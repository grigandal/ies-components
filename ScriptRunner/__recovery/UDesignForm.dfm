object DesignForm: TDesignForm
  Left = 239
  Top = 159
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1084#1087#1086#1085#1077#1085#1090#1072' '#1074#1099#1079#1086#1074#1072' '#1074#1085#1077#1096#1085#1080#1093' '#1055#1055#1055
  ClientHeight = 365
  ClientWidth = 576
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 473
    Height = 361
    Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1086#1085#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 164
      Height = 13
      Caption = #1071#1079#1099#1082' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080' '#1084#1086#1089#1090#1072' '#1089' '#1055#1055#1055':'
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 262
      Height = 13
      Caption = #1042#1088#1077#1084#1103' '#1086#1078#1080#1076#1072#1085#1080#1103' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1089#1082#1088#1080#1087#1090#1072' ('#1074' '#1089#1077#1082#1091#1085#1076#1072#1093'):'
    end
    object Label3: TLabel
      Left = 16
      Top = 120
      Width = 375
      Height = 13
      Caption = 
        #1048#1089#1087#1086#1083#1100#1079#1091#1077#1084#1099#1077' '#1084#1086#1076#1091#1083#1080' ('#1084#1086#1076#1091#1083#1080' '#1076#1086#1083#1078#1085#1099' '#1073#1099#1090#1100' '#1085#1072#1087#1080#1089#1072#1085#1099' '#1085#1072' '#1086#1076#1085#1086#1084' '#1103#1079#1099#1082#1077')' +
        ':'
    end
    object LangCB: TComboBox
      Left = 368
      Top = 24
      Width = 89
      Height = 21
      ItemIndex = 0
      TabOrder = 0
      Text = 'VBScript'
      Items.Strings = (
        'VBScript'
        'JavaScript')
    end
    object TimeoutEdit: TSpinEdit
      Left = 368
      Top = 56
      Width = 89
      Height = 22
      MaxValue = 30000
      MinValue = -1
      TabOrder = 1
      Value = -1
    end
    object UnitsLB: TListBox
      Left = 16
      Top = 144
      Width = 337
      Height = 201
      ItemHeight = 13
      TabOrder = 2
    end
    object AddB: TButton
      Left = 368
      Top = 216
      Width = 89
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 3
      OnClick = AddBClick
    end
    object DelB: TButton
      Left = 368
      Top = 248
      Width = 89
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100
      TabOrder = 4
      OnClick = DelBClick
    end
    object CreateB: TButton
      Left = 368
      Top = 184
      Width = 89
      Height = 25
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1085#1086#1074#1099#1081
      TabOrder = 5
      OnClick = CreateBClick
    end
    object ErrorCB: TCheckBox
      Left = 16
      Top = 88
      Width = 257
      Height = 17
      Caption = #1042#1099#1074#1086#1076#1080#1090#1100' '#1085#1072' '#1101#1082#1088#1072#1085' '#1089#1086#1086#1073#1097#1077#1085#1080#1103' '#1086#1073' '#1086#1096#1080#1073#1082#1072#1093
      TabOrder = 6
    end
  end
  object OKB: TButton
    Left = 496
    Top = 16
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBClick
  end
  object OpenDlg: TOpenDialog
    Filter = 'VBScript (*.vb)|*.vb|JavaScript (*.js)|*.js|All files (*.*)|*.*'
    Left = 48
    Top = 168
  end
end
