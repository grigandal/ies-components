unit UDesignForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, XMLDoc, XMLIntf;

type
  TDesignForm = class(TForm)
    GroupBox1: TGroupBox;
    LangCB: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    TimeoutEdit: TSpinEdit;
    Label3: TLabel;
    UnitsLB: TListBox;
    AddB: TButton;
    DelB: TButton;
    OpenDlg: TOpenDialog;
    OKB: TButton;
    CreateB: TButton;
    ErrorCB: TCheckBox;
    procedure CreateBClick(Sender: TObject);
    procedure AddBClick(Sender: TObject);
    procedure DelBClick(Sender: TObject);
    procedure OKBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ConfigDoc: IXMLDocument;
  end;

var
  DesignForm: TDesignForm;

implementation

{$R *.dfm}


procedure TDesignForm.FormCreate(Sender: TObject);
begin
  ConfigDoc := TXMLDocument.Create(nil);
end;


procedure TDesignForm.CreateBClick(Sender: TObject);
var
  sl: TStringList;
  FN: string;
begin
  FN := InputBox('�������� ������ ������', '������� ��� ������:', 'Global');

  sl := TStringList.Create();
  if LangCB.ItemIndex = 0 then begin
    // vbScript
    FN := FN + '.vb';

    sl.Add(''' ��� VB');
    sl.Add('');
    sl.Add('Dim CurDir');
    sl.Add('Dim bb');
    sl.Add('');
    sl.Add('Sub SetGlobals(aCurDir, aBB)');
    sl.Add('  CurDir = aCurDir');
    sl.Add('  Set bb = aBB');
    sl.Add('End Sub');
    sl.Add('');
  end
  else begin
    // JavaScipt
    FN := FN + '.js';

    sl.Add('// ��� JavaScript');
    sl.Add('');
    sl.Add('var CurDir;');
    sl.Add('var bb;');
    sl.Add('');
    sl.Add('function SetGlobals(aCurDir, aBB)');
    sl.Add('{');
    sl.Add('  CurDir = aCurDir;');
    sl.Add('  bb = aBB;');
    sl.Add('}');
    sl.Add('');
  end;
  sl.SaveToFile(FN);
  sl.Free;

  UnitsLB.Items.Add(FN);
end;

procedure TDesignForm.AddBClick(Sender: TObject);
begin
  if OpenDlg.Execute then UnitsLB.Items.Add(OpenDlg.FileName);
end;

procedure TDesignForm.DelBClick(Sender: TObject);
begin
  if UnitsLB.ItemIndex >= 0 then UnitsLB.DeleteSelected;
end;

procedure TDesignForm.OKBClick(Sender: TObject);
begin
  // �������� ���������������� ����������

end;



end.
