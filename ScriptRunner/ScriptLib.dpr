library ScriptLib;

uses
  ComServ,
  ScriptLib_TLB in 'ScriptLib_TLB.pas',
  URunnerImpl in 'URunnerImpl.pas' {Runner: CoClass},
  UDesignForm in 'UDesignForm.pas' {DesignForm};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
