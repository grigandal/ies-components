unit ScriptLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 21.09.2004 11:44:48 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Dd\Delphi\ScriptRunner\ScriptLib.tlb (1)
// LIBID: {5784FA18-C2D5-417E-9943-07C02B9C4D64}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ScriptLibMajorVersion = 1;
  ScriptLibMinorVersion = 0;

  LIBID_ScriptLib: TGUID = '{5784FA18-C2D5-417E-9943-07C02B9C4D64}';

  IID_IRunner: TGUID = '{68345324-4E05-4992-8BEF-91106F782EE0}';
  CLASS_Runner: TGUID = '{C0508C0B-3E87-42CE-915F-B13E4FF376B6}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IRunner = interface;
  IRunnerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Runner = IRunner;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PPSafeArray1 = ^PSafeArray; {*}


// *********************************************************************//
// Interface: IRunner
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {68345324-4E05-4992-8BEF-91106F782EE0}
// *********************************************************************//
  IRunner = interface(IDispatch)
    ['{68345324-4E05-4992-8BEF-91106F782EE0}']
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function  Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName: WideString; const Msg: WideString; 
                             out Output: OleVariant); safecall;
    procedure Stop; safecall;
    function  AddModule(const FilePath: WideString): Integer; safecall;
    procedure Reset; safecall;
    function  RunFunction(const Name: WideString; const Module: WideString; var Params: PSafeArray; 
                          out ReturnValue: OleVariant): Integer; safecall;
    function  Get_LastError: WideString; safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
    property LastError: WideString read Get_LastError;
  end;

// *********************************************************************//
// DispIntf:  IRunnerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {68345324-4E05-4992-8BEF-91106F782EE0}
// *********************************************************************//
  IRunnerDisp = dispinterface
    ['{68345324-4E05-4992-8BEF-91106F782EE0}']
    property Name: WideString dispid 1;
    property Broker: OleVariant dispid 2;
    procedure Configurate(const Config: WideString); dispid 3;
    procedure ProcessMessage(const SenderName: WideString; const Msg: WideString; 
                             out Output: OleVariant); dispid 4;
    procedure Stop; dispid 5;
    function  AddModule(const FilePath: WideString): Integer; dispid 6;
    procedure Reset; dispid 7;
    function  RunFunction(const Name: WideString; const Module: WideString; 
                          var Params: {??PSafeArray}OleVariant; out ReturnValue: OleVariant): Integer; dispid 8;
    property LastError: WideString readonly dispid 9;
  end;

// *********************************************************************//
// The Class CoRunner provides a Create and CreateRemote method to          
// create instances of the default interface IRunner exposed by              
// the CoClass Runner. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRunner = class
    class function Create: IRunner;
    class function CreateRemote(const MachineName: string): IRunner;
  end;

implementation

uses ComObj;

class function CoRunner.Create: IRunner;
begin
  Result := CreateComObject(CLASS_Runner) as IRunner;
end;

class function CoRunner.CreateRemote(const MachineName: string): IRunner;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Runner) as IRunner;
end;

end.
