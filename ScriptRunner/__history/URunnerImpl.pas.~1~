unit URunnerImpl;

{$WARN SYMBOL_PLATFORM OFF}

{******************************************************************************
 *   ��������� ScriptLib.Runner ������������ ��� ���������� �������� � �������,
 * ������������� �� ������ JavaScript � VBScript, � ������ �������������.
 * ��������� ������������ ����������� ������������� Windows MS Script Engine,
 * ����� ���������������� ������� ����������� � ������, ������� �����
 * ����������� ������������ � ������ ����������.
 *
 *   ����������. � ������ ����������� ������ (�����) ������ ���� ���������
 * ��������� ���������� ����������:
 *  - CurDir, � ������� ����� ���������� ���� � ������� ���������� ���
 *  �������� ������ � ������.
 *  - bb, � ������� ����� ���������� ������ �� �������� �����
 *  ����� ��������� ������� ������ SetGlobals, ���������������� ��������
 * ���������� ���������� CurDir � bb.
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 21 ��������, 2004 >>
 ******************************************************************************}

interface

uses
  ComObj, ActiveX, ScriptLib_TLB, StdVcl, XMLDoc, XMLIntf, MSScriptControl_TLB;

type
  TVarArr = array of variant;

  TRunner = class(TAutoObject, IRunner)
  protected
    Name: WideString;
    Broker: Variant;
    Error: WideString;
    Silent: boolean;
    function Get_Broker: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName, Msg: WideString;
      out Output: OleVariant); safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Stop; safecall;
    function AddModule(const FilePath: WideString): Integer; safecall;
    procedure Reset; safecall;
    function RunFunction(const Name, Module: WideString;
      var Params: PSafeArray; out ReturnValue: OleVariant): integer;
      safecall;
    function Get_LastError: WideString; safecall;
    procedure Set_LastError(const Value: WideString); safecall;
    { Protected declarations }
  private
    ConfigDoc: IXMLDocument;
    SE: TScriptControl;
    procedure ProcessError(Err: WideString);
    procedure Design;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  end;

implementation

uses ComServ, Dialogs, Variants, SysUtils, Forms, Classes, UDesignForm, Controls;

const
  DEF_MODULE = 'Global';
  LANG_VBS = 'VBScript';
  LANG_JS = 'JavaScript';
  NO_TIME_OUT = -1;
  CRLF = Chr(13) + Chr(10);


procedure TRunner.Initialize;
begin
  inherited;
  ConfigDoc := TXMLDocument.Create(nil);
  ConfigDoc.LoadFromXML('<config><script Timeout="-1" Language="VBScript" Silent="true" /></config>');
  SE := TScriptControl.Create(Application);
end;


destructor TRunner.Destroy;
begin
  SE.Free;
  ConfigDoc := nil;
  inherited;
end;

procedure TRunner.ProcessError(Err: WideString);
begin
  Error := Err;
  if not Silent then ShowMessage(Error);
end;


// ����������� ������ IESComponent


function TRunner.Get_Broker: OleVariant;
begin
  Result := Broker;
end;


function TRunner.Get_Name: WideString;
begin
  Result := Name;
end;


procedure TRunner.Set_Broker(Value: OleVariant);
begin
  Broker := Value;
end;


procedure TRunner.Set_Name(const Value: WideString);
begin
  Name := Value;
end;


function TRunner.Get_LastError: WideString;
begin
  Result := Error;
end;

procedure TRunner.Set_LastError(const Value: WideString);
begin
end;


procedure TRunner.Configurate(const Config: WideString);
var
  n: IXMLNode;
  nodes: IXMLNodeList;
  path, filename: WideString;
begin
  { ������ ���������������� ����������
  <config>
    <script Timeout="0" Language="VBScript" Silent="false"/>
    <file>unit1.vb</file>
    <file>unit2.vb</file>
  </config>
  }

  SE.TimeOut := NO_TIME_OUT;
  SE.Language := LANG_VBS;
  SE.Reset;
  Silent := false;
  
  // ���� ������� ������ �� ������������� xml
  try
    ConfigDoc.LoadFromXML(Config);
  except
    on E:Exception do begin
      ProcessError('Scripter: ������������ ���������������� ����������' + #10#13 + E.Message);
      exit;
    end;
  end;

  try
    n := ConfigDoc.DocumentElement.ChildNodes.FindNode('script');
    if n = nil then exit;
    if n.HasAttribute('Timeout') then
      SE.TimeOut := StrToInt(n.Attributes['Timeout']);
    if n.HasAttribute('Language') then
      SE.Language := n.Attributes['Language'];
    SE.Reset;

    if n.HasAttribute('Silent') then
      Silent := AnsiCompareStr(n.Attributes['Silent'], 'true') = 0;

    nodes := ConfigDoc.DocumentElement.ChildNodes;
    n := nodes.First;
    while n <> nil do begin
      if n.NodeName = 'file' then begin
        filename := n.Text;
        // ���� ���� �������������, �� ������� ����
        if AnsiPos(':\', filename) = 0 then begin
          try
            path := Broker.Path;
          except
            // ���� ��� �������� Path ��� ��� �������
            path := ExtractFilePath(Application.ExeName)
          end;
          filename := path + filename;
        end;
        AddModule(filename);
      end;
      if n = nodes.Last then n := nil
      else n := n.NextSibling;
    end;
  except
    on E:Exception do ProcessError('Scripter: ������������ ���������������� ����������' + #10#13 + E.Message);
  end;
end;

{
������ �������������� ���������:
        AddModule       -       �������� ������ (���������� ����� �� ��������)
        Reset           -       �������� ���� �������
        Run             -       ������ ���������

        Design          -       ���������� ���������� � �� ��� design-time
        DesignMathcad   -       ���������� ������ ��� ������ � Mathcad
        DesignDatabase  -       ���������� ������ ��� ������ � ��
        IsComponentConfigured
}
procedure TRunner.ProcessMessage(const SenderName, Msg: WideString; out Output: OleVariant);
var
  MessageDoc: IXMLDocument;
  n, p: IXMLNode;
  Params: PSafeArray;
  i: integer;
  param: Variant;
  bounds: TSafeArrayBound;
  ProcName, s: string;
begin
  Error := '';
  MessageDoc := TXMLDocument.Create(nil);
  MessageDoc.LoadFromXML(Msg);
  ProcName := MessageDoc.DocumentElement.Attributes['ProcName'];

  // �������� ������ (���������� ����� �� ��������)
  {
  <message ProcName="AddModule">
    <file>Global.vb</file>
  </message>
  }

  if AnsiCompareText(ProcName, 'AddModule') = 0 then begin
    n := MessageDoc.DocumentElement.ChildNodes['file'];
    OutPut := AddModule(n.Text);
  end;

  // �������� ���� �������
  if AnsiCompareText(ProcName, 'Reset') = 0 then begin
    Reset;
  end;

  // ������ ���������
  {
  <message ProcName="Run">
    <func name="function1" module="Global">
      <param type="number">13</param>
      <param type="string">hello</param>
      <param type="bool">-1</param>
    </func>
  </message>
  }
  if AnsiCompareText(ProcName, 'Run') = 0 then begin
    n := MessageDoc.DocumentElement.ChildNodes['func'];

    // ���������� � ����� �������
    bounds.lLbound := 0;
    bounds.cElements := n.ChildNodes.Count;
    Params := SafeArrayCreate(VT_VARIANT, 1, bounds);

    // ���������� ��������
    for i := 0 to n.ChildNodes.Count-1 do begin
      p := n.ChildNodes[i];
      if p.Attributes['type'] = 'number' then
        param := StrToInt(p.Text)
      else if p.Attributes['type'] = 'bool' then
        param := boolean(StrToInt(p.Text))
      else
        param := WideString(p.Text);
      SafeArrayPutElement(Params, i, param);
    end;

    RunFunction(n.Attributes['name'], n.Attributes['module'], Params, OutPut);
    SafeArrayDestroy(Params);
  end;

  // ��� ��������
  // <message ProcName='Mig81'><text>sadf</text></message>
  if AnsiCompareText(ProcName, 'Mig81') = 0 then begin
    if MessageDoc.DocumentElement.ChildNodes.Count > 0 then begin
      p := MessageDoc.DocumentElement.ChildNodes[0];
      s := p.Text;
    end
    else
      s := '';
    if InputQuery('ProcName="Mig81"', '������, ����! �� ������ ��� ������� ���� ��� ������:', s) then
      Output := WideString(s)
    else
      Output := Unassigned;
    exit;
  end;

  // ���������������� � ���������� ��������
  if AnsiCompareText(ProcName, 'Design') = 0 then begin
    Design;
    Output := ConfigDoc.DocumentElement.XML;
    ShowMessage(Output);
    exit;
  end;

  // ��� Mathcad
  if AnsiCompareText(ProcName, 'DesignMathcad') = 0 then begin
    Design;
    Output := ConfigDoc.DocumentElement.XML;
    //ShowMessage(Output);
    exit;
  end;

  // ��� ��
  if AnsiCompareText(ProcName, 'DesignDatabase') = 0 then begin
    Design;
    Output := ConfigDoc.DocumentElement.XML;
    //ShowMessage(Output);
    exit;
  end;

  // IsComponentConfigured
  if AnsiCompareText(ProcName, 'IsComponentConfigured') = 0 then begin
    // �������� ���������������� �� ��������� �����;
    Output := true;
    exit;
  end;

end;


procedure TRunner.Stop;
begin

end;



// ��������� ������


function TRunner.AddModule(const FilePath: WideString): Integer;
var
  Str, Module: WideString;
  prev: OleVariant;
  obj: IScriptModule;

  Params: PSafeArray;
  param: Variant;
  bounds: TSafeArrayBound;
  i: integer;
begin
  Error := '';
  // �������� ��� ������ �� ����
  Module := ChangeFileExt(ExtractFileName(FilePath), '');

  Str := FilePath;
  
  with TStringList.Create do
  try
    try
      // ���� ����� ������ ��� ����, �� ������ �� ������
      SE.Modules.Item[Module]; // ���� ��� ���, �� �� ��������� ������ exit
      exit;
    except
    end;

    if FileExists(Str) then LoadFromFile(Str) // �������� ������ � ������
    else begin
      Result := -1;
      ProcessError('Scripter: �� ���� ������� ���� "'+Str+'".');
      Exit;
    end;

    prev := SE.Modules.Item[1]; // ���������� ������
    obj := SE.Modules.Add(Module, prev);
    try
      obj.AddCode(Text);

      // �������������� ���������� ���������� CurDir � BB
      bounds.lLbound := 0;
      bounds.cElements := 2;
      Params := SafeArrayCreate(VT_VARIANT, 1, bounds);

      i := 0;
      param := WideString(ExtractFilePath(Application.ExeName));
      SafeArrayPutElement(Params, i, param);

      i := 1;
      param := Unassigned;
      if not VarIsEmpty(Broker) then param := Broker.Blackboard;
      SafeArrayPutElement(Params, i, param);

      obj.Run('SetGlobals', Params);
      SafeArrayDestroy(Params);
    except
      on E: Exception do begin
        Result := -1;
        ProcessError(e.Message);
        exit;
      end;
    end;
  finally
    Free;
  end;

  Result := 0;
end;


procedure TRunner.Reset;
begin
  Error := '';
  try
    SE.Reset;
  except
    on E: Exception do ProcessError(e.Message);
  end;
end;


function TRunner.RunFunction(const Name, Module: WideString;
  var Params: PSafeArray; out ReturnValue: OleVariant): Integer;
var
  pModule: IScriptModule;
begin
  Error := '';
  try
    pModule := SE.Modules.Item[Module];
  except
    Result := -1;
    ProcessError('������ "'+Module+'" �� ������.');
    exit;
  end;

  try
    if pModule = nil then
      ReturnValue := SE.Run(Name, params)
    else
      ReturnValue := pModule.Run(Name, params);
  except
    on E: Exception do begin
      Result := -1;
      ProcessError(e.Message);
    end;
  end;
end;


procedure TRunner.Design;
var
  n: IXMLNode;
  nodes: IXMLNodeList;
  i: integer;
begin
  DesignForm := TDesignForm.Create(nil);
  {<config>
    <script Timeout="0" Language="VBScript" Silent="false"/>
    <file>unit1.vb</file>
    <file>unit2.vb</file>
  </config>}

  n := ConfigDoc.DocumentElement.ChildNodes['script'];
  if not VarIsNull(n) then begin
    if n.HasAttribute('Timeout') then
      DesignForm.TimeoutEdit.Text := n.Attributes['Timeout'];
    if n.HasAttribute('Language') and (AnsiCompareStr(n.Attributes['Language'], LANG_VBS) = 0) then
      DesignForm.LangCB.ItemIndex := 0
    else
      DesignForm.LangCB.ItemIndex := 1;
    if n.HasAttribute('Silent') then
      DesignForm.ErrorCB.Checked := not (AnsiCompareStr(n.Attributes['Silent'], 'true') = 0);
  end;

  nodes := ConfigDoc.DocumentElement.ChildNodes;
  n := nodes.First;
  while n <> nil do begin
    if n.NodeName = 'file' then DesignForm.UnitsLB.Items.Add(n.Text);
    if n = nodes.Last then n := nil
    else n := n.NextSibling;
  end;

  if DesignForm.ShowModal = mrOK then begin
    ConfigDoc.LoadFromXML('<config />');
    n := ConfigDoc.DocumentElement.AddChild('script');
    n.Attributes['Timeout'] := DesignForm.TimeoutEdit.Text;
    n.Attributes['Language'] := DesignForm.LangCB.Text;
    if DesignForm.ErrorCB.Checked then
      n.Attributes['Silent'] := 'false'
    else
      n.Attributes['Silent'] := 'true';

    for i:=0 to DesignForm.UnitsLB.Count-1 do begin
      n := ConfigDoc.DocumentElement.AddChild('file');
      n.Text := DesignForm.UnitsLB.Items[i];
    end;
  end;

  DesignForm.Free;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TRunner, Class_Runner,
    ciMultiInstance, tmApartment);
end.
