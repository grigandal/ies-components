# Компоненты ИЭС комплекса АТ-ТЕХНОЛОГИЯ

## Среда разработки

Рекомендуемая среда разработки - ***Embarcadero RAD Studio 10***

---

Используемая на момент 2020 года среда - ***Embarcadero RAD Studio 10.3.3 Rio Architect Version 26.0.36039.7899 [2019, MULTILANG]***

Установщик и "таблэтку" можно взять [здесь](https://drive.google.com/open?id=1rzpr0rZ6OAH_ptgn7GxO8uN1QvGXbxXv)

### Установка и patch Embarcadero RAD Studio 10.3.3 Rio

!!!`На время установки отключить антивирус`!!!

1. Скачиваем архив по ссылке выше и распаковываем его

2. Если нет программы ***DAEMON Tools Lite***, скачиваем и устанавливаем ее из интернета

3. Запускаем ***DAEMON Tools Lite***, нажимаем ***Быстрое Монтирование***, выбираем файл ***delphicbuilder10_3_3_7899_nt.iso***

<details>
  <summary>Изображения к 3</summary>

  ![DTWindow](./README/DTWindow.png)

  ![ISO](./README/ISO.png)

</details>
<br>

4. После этого появится виртуальный диск BD-ROM, в имени которого будет содержаться RADStudio

<details>
  <summary>Изображения к 4</summary>

![Disk](./README/Disk.png)
</details>
<br>

5. Открываем этот диск, заходим в папку ***Install***, находим и запускаем файл ***Setup.exe***

<details>
  <summary>Изображения к 5</summary>

![Setup](./README/Setup.png)
</details>
<br>

6. Открылся установщик, соглашаемся с лицензией, нажимаем next

<details>
  <summary>Изображения к 6</summary>

![SU1](./README/SU1.png)
</details>
<br>

7. **!!!ВНИМАНИЕ!!!** - Перешли на страницу выбора метода активации. **!!!НАЖИМАЕМ BACK!!!**

<details>
  <summary>Изображения к 7</summary>

![SU2](./README/SU2.png)
</details>
<br>

8. Переходим в папку, куда распаковали изначальный скачанный архив, и, **ОТКЛЮЧИВ АНТИВИРУС** запускаем ***RADStudioKeyPatch_Lite.exe*** (на Windows 10 может возникнуть синее окошко Windows SmartScreen, нажимаем подробнее и выполнить в любом случае)

<details>
  <summary>Изображения к 8</summary>

![SUPatch](./README/SUPatch.png)

![SS1](./README/SS1.png)

![SS2](./README/SS2.png)

</details>
<br>

9. В открывшемся окне ***RADStudioKeyPatch_Lite.exe*** выбираем ***RAD Studio 10.3.3 Rio Update 3*** и нажимаем ***Generate*** , RADStudioKeyPatch_Lite ***НЕ ЗАКРЫВАЕМ.***

<details>
  <summary>Изображения к 9</summary>

![KG1](./README/KG1.png)

</details>
<br>

10. Переходим обратно к установщику и нажимаем next. Попадаем уже на другую страницу. Обязательно выбираем ***Delphi*** для установки, а С++ Builder - по желанию, и проходим установку до конца.

11. После завершения установки переходим снова к ***RADStudioKeyPatch_Lite*** и нажимаем ***Patch RAD Studio***

<details>
  <summary>Изображения к 9</summary>

![KG2](./README/KG2.png)

</details>
<br>

12. Переходим к установленной RAD Studio, обычно это папка `C:\Program Files (x86)\Embarcadero\Studio\XX.X\bin`, где `XX.X` - номер версии (в нашем случае 20.0), и запускаем ***bds.exe*** ***ОТ ИМЕНИ АДМИНИСТРАТОРА***. Установка завершена.

## Компоненты

- ### [Решатель](./USP/SolverX/)
- ### [Диалоговый компонент](./prg/Dialoger/)