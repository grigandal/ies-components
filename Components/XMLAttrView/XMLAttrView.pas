unit XMLAttrView;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, ComCtrls, ValEdit,
  XMLDoc, XMLIntf, Variants, Dialogs;

type
  TXMLAttrView = class(TValueListEditor)
  public
    XMLNode: IXMLNode; // ����, ��������� � treeview, �������� �������� ���� ��������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ShowAttributes;
    procedure PostAttribute(AKey, AValue: string);
    procedure SelectOnMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure GetAttrOnSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  private
    CanEdit: boolean; // ����� �� ����������� OnSetEditText
    XMLAttr: IXMLNode; // ��� �������, ������� ������ ������
  published
    property KeyOptions;
  end;

function RussianInName(Name: string): boolean;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DD Pack', [TXMLAttrView]);
end;

{=============================== TXMLAttrView ============================}

function RussianInName(Name: string): boolean;
var i: integer;
begin
  // ����� ���������� ��� ������������ XML
  i:=1;
  while i<=Length(Name) do begin
    case Name[i] of
    '�'..'�','�'..'�','�'..'�': break;
    end;
    Inc(i);
  end;
  Result := (i<length(Name)+1);
end;

constructor TXMLAttrView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  TitleCaptions.Text := '';
  TitleCaptions.Add('�������');
  TitleCaptions.Add('��������');
  KeyOptions := [keyEdit, keyAdd, keyDelete, keyUnique];
  CanEdit := True;
  OnMouseUp := SelectOnMouseUp;
  OnSelectCell := GetAttrOnSelectCell;
  XMLNode := nil;
  XMLAttr := nil;
end;

destructor TXMLAttrView.Destroy;
begin
  inherited Destroy;
end;

procedure TXMLAttrView.ShowAttributes;
var i: integer;
begin
  if XMLNode <> NIL then begin
    CanEdit := False; // ����� SetEdit �� �������
    Strings.Clear;
    for i:=0 to XMLNode.AttributeNodes.Count-1 do begin
      // � ��������� ����� ������������ �������� Text ������ NodeValue
      Values[XMLNode.AttributeNodes.Nodes[i].NodeName] :=
        XMLNode.AttributeNodes.Nodes[i].Text;
    end;
    CanEdit := True;
  end;
end;

procedure TXMLAttrView.PostAttribute(AKey, AValue: string);
begin
  if (XMLNode = NIL) or (AKey = '') or RussianInName(AKey) then exit;
  // ���� ���������� ��� ��������, ���� ������ �������
  if (XMLAttr <> NIL) and (Col = 0) then XMLNode.AttributeNodes.Delete(XMLAttr.NodeName);
  // � ������ ��������� ����� ���� ��� ������ �������� ������������� ����
  XMLNode.Attributes[AKey] := AValue;
end;

procedure TXMLAttrView.SelectOnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ACol, ARow: integer;
begin
  // OnMouseUp: select �� ������ �����
  MouseToCell(X, Y, ACol, ARow);
  if (ARow > 0) and (ARow < RowCount) then begin
    Col := ACol;
    Row := ARow;
  end;
end;

procedure TXMLAttrView.GetAttrOnSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  // �������� �������, ��������������� ��������� ������
  if (ARow > 0) and (ARow < RowCount) then begin
    XMLAttr := XMLNode.AttributeNodes.FindNode(Keys[ARow]);
    XMLNode := XMLNode;
  end;
end;

end.
