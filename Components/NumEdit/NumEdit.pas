//---------------------------------------------
// ��������� ��� ����� ����� c ��������� ������
// ������: The DimOS Arts corp.
//---------------------------------------------

unit NumEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TNumEdit = class(TEdit)
   private
   FloatNum: Double;
   Function GetNum: Double;
   Procedure SetNum(Value: Double);
   protected
      Procedure KeyPress(var Key: char); Override;
   published
      Property Number: Double Read GetNum Write SetNum;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DD Pack', [TNumEdit]);
end;

//============================================================================
{ TNumEdit }

Function TNumEdit.GetNum: Double;
var
   s: string;
begin
   s := Text;
   s := StringReplace(s, '.', DecimalSeparator, [rfReplaceAll]);
   s := StringReplace(s, ',', DecimalSeparator, [rfReplaceAll]);
   try Result := StrToFloat(s);
   except
      on EConvertError do begin
         Result := 0;
         Text := '';
      end;
   end;
end;

Procedure TNumEdit.SetNum(Value: Double);
begin
   FloatNum := Value;
   Text := FloatToStr(Value);
end;

Procedure TNumEdit.KeyPress(var Key: char);
begin
   case Key of
   '0'..'9',#8,#13: ;
   '-': begin
           if Pos(Key,Text)=0 then //������ ������ ������ ������
      	      Text := Concat('-',Text); //����� �������� ������ � ������
           Key := #0;
      	end;
   '.',',':
        if Pos(Key,Text)>0 then
          Key := #0;//������ ��������� ������ �����
        else Key := #0;
   end;
   inherited KeyPress(Key);
end;

{---------------------------------------------}

end.
