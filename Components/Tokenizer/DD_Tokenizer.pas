unit DD_Tokenizer;

{******************************************************************************
 * ��������� TDDTokenizer ������������ ��� ������� �������� ������ �� �������
 * ������ � ����� IE_pack.bpl � �� ������� �� ������ ����������� ������.
 * �����: << ������� �. �. >>    ���� ��������: << 12 �������, 2001 >>
 * ��������� ����������: << 18 ��������, 2001 >>
 * Copyright (�) The DimOS Arts corp., 2001
 ******************************************************************************}

interface

uses Classes;

type
  TSpaces = set of (Tab, Space, NL, CR); // Tab = 9, Space = 32, NL = 10, CR = 13

  TDDTokenizer = class(TComponent)
  private
  {��������� ����}
    DelimCount: integer;   // ����� ������������ (��� ���������� ��������)
    Position: integer;     // ������� ������� � ������ FText ��� {} �������
    TextLen: integer;      // ����� �������� ������
    ReadingQuote: boolean; // ������ �� �� ������ ������ � �������� ��� ������� �����

  {���� ��� �������}
    FText: string;       // ������� ����� ��� �����������
    FDelims: string;     // ������ � ������������� (���������� - ���� �����������)
    FBAsS: TSpaces;      // ��� ���������� ������� ������������ ��� ������� (blanks as symbols)
    FDAsT: boolean;      // ���������� ����������� � ������ / ������������ ����������� (delimiters as tokens)
    FTokens: TStrings;   // ������ ������� ����� ������ ��������� Tokenize
    FQChar: char;       // ������, �������������� ������ ����� - �����������

  {������, ��������������� �������� � ����������� ����}
    procedure setText(const S: string);     // ������������� ������� �����
    procedure setDelims(const S: string);   // ������������� �������������� �������
    function IsEOT: boolean;                // ���������, ���� �� ��� ������ � ������

  {��������������� ������}
    function IsBlank(ch: char): boolean;    // ���������, �������� �� ������ ����������
    function IsDelim(ch: char): boolean;    // --||-- ������������
    function IsSepar(ch: char): boolean;    // --||-- ������������ ��� ����������

  {������, �������������� ���������� ������� ������}
    {function ParseComment: integer;         // ���������� �����������
    function ParseString: integer;          // ���������� ������ � ��������
    function ParseNumber: integer;          // ���������� ������ �����
    function ParseIdent: integer;}           // ���������� ��������������

  public
  {��� ������ �������� ������������}
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    // ���������� ��� ������ Text �����, ����� ������������ ��������� � ������� ����� �������
    procedure Tokenize;
    // ����� ���������� � ����� ��� {} ������� � �������, ����� ������������ ����� ��������
    procedure NextToken(var Token: string);  // NB! �������� ��������� Tokenize
    // ����� ��������� � Text �� ���������� ����� � ���������� Success/Fail
    function  LoadTextFromFile(FileName: string): boolean;
  published
    property Text: string           read FText   write setText;
    property Delimiters: string     read FDelims write setDelims;
    property BlankAsSymbol: TSpaces read FBAsS   write FBAsS;
    property DelimAsToken: boolean  read FDAsT   write FDAsT;
    property Tokens: TStrings       read FTokens;
    property EOT: boolean           read IsEOT;
    property QuoteChar: char        read FQChar  write FQChar;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DD Pack', [TDDTokenizer]);
end;

//============================================================================

           {===== Public ������ =====}

constructor TDDTokenizer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FTokens := TStringList.Create;
  {��������� �� ���������}
  FBAsS := [];             // ���������� ������� �������� �������������
  FDAsT := False;          // ����������� �������������
  FDelims := '';           // ������ ������������ ��� (������ ����������)
  FText := '';             // ������� ������ ������
  FQChar := '"';           // ������ �� ��������� �������������� �������� ���������
  Position := 1;           // ������������� ������ ������� �� ������ ������
  ReadingQuote := False;
end;

destructor TDDTokenizer.Destroy;
begin
  FTokens.Free;
  inherited Destroy;
end;

procedure TDDTokenizer.Tokenize;
var
  b,i: integer;
  str: string;
begin
  FTokens.Clear;
  ReadingQuote := false;
  //���� �� ����� ������������, �� �������� ��� ������
  if (FDelims = '')                                        // ������ ������������ ����
  and (FBAsS = [Tab,Space,NL,CR])                          // ��� ���������� - �� �����������
  then FTokens.Append(Text)                                // �� ! ����� = ��� ������
  else begin
    i := 1;
    while (i<=TextLen) do begin
      while (i<=TextLen) and IsSepar(FText[i]) do begin         // �������� ��� �����������
        if FDAsT and not IsBlank(FText[i])                      // ������� ��� �� �����
        then FTokens.Append(FText[i]);                          // � ����������� ���� �����
        if FText[i] = FQChar then begin                         // ����� ������ � ��������
          if ReadingQuote then
            ReadingQuote := False
          else begin
            ReadingQuote := True;
            str := copy(FText,i+1,length(FText)-i); //������� ������ ���������
            b := pos(FQChar,str)-1;  //���� � ������� ����������� �������
            if b>0 then begin
              FTokens.Append(copy(str,1,b));
              i := i+b;
            end;
          end
        end;
        i := i+1;
      end;
      if i>TextLen then break;                                  // ����� ��������
      b := i;                                                   // �������� ������ ������
      while (i<=TextLen) and not IsSepar(FText[i]) do i := i+1; // ���� � ����� ������
      FTokens.Append(copy(FText, b, i-b))
    end;
  end;
  ReadingQuote := False;
end;

procedure TDDTokenizer.NextToken(var Token: string);
var
  b,i: integer;
  OldSpaces: TSpaces;
begin
  if (FDelims = '')                                        // ������ ������������ ����
  and (FBAsS = [Tab,Space,NL,CR])                          // ��� ���������� - �� �����������
  then begin
    Token := Copy(FText,Position,TextLen-Position+1);      // �� ! ����� - ������� ������
    FTokens.Append(Token);
    Position := TextLen+1;                                 // ����� EOT ��� True
  end
  else begin                                               // ����� ������� ��������� �����
    i := Position;
    while (i<=TextLen) and IsSepar(FText[i]) do begin      // ���������� ��� �����������
      if FText[i] = FQChar then begin
        if ReadingQuote then begin
          ReadingQuote := False;
          FBAsS := OldSpaces;
        end
        else begin
          ReadingQuote := True;
          OldSpaces := FBAsS;
          FBAsS := [Space,Tab];
        end;
      end;
      if FDAsT and not IsBlank(FText[i])
      then begin
        Token := FText[i];
        FTokens.Append(Token);
        Position := i+1;
        exit;
      end;
      i := i+1;
    end;
    if i<=TextLen then begin
      b := i;                                                   // �������� ������ ������
      while (i<=TextLen) and not IsSepar(FText[i]) do i := i+1; // ���� � ����� ������
      Token := copy(FText, b, i-b);
      FTokens.Append(Token);
      Position := i;
    end;
  end;
end;

function TDDTokenizer.LoadTextFromFile(FileName: string): boolean;
var
  f: TextFile;
  buf: string;
begin
  {$I-}
  AssignFile(f, FileName);
  Reset(f);
  {$I+}
  Result := (IOResult = 0) and (FileName <> '');
  if Result then begin                // ���� ����������
    Text := '';
    while not EOF(f) do begin
      ReadLn(f, buf);
      Text := Text + Chr(13) + buf; // \n �� �������� ReadLn
    end;
  end;
  CloseFile(f);
  ReadingQuote := False;
end;

{===== Private ������ =====}

procedure TDDTokenizer.setDelims(const S: string);
begin
  FDelims := S;
  DelimCount := Length(FDelims); // ��� ����� � ������� ������� write
end;

procedure TDDTokenizer.setText(const S: string);
begin
  FText := S;
  TextLen := Length(FText);
end;

function TDDTokenizer.IsBlank(ch: char): boolean;
begin
  Result := False;
  case ch of  // � ����� �� if'�
  chr(32): if not (Space in FBAsS) then Result := True;
  chr(13): if not (CR    in FBAsS) then Result := True;
  chr(10): if not (NL    in FBAsS) then Result := True;
  chr(9) : if not (Tab   in FBAsS) then Result := True;
  end;
end;

function TDDTokenizer.IsDelim(ch: char): boolean;
var i: integer;
begin
  Result := False;
  i := 1;
  if (ch = FQChar) then Result := True
  else begin
    while (i<=DelimCount) and (ch<>Delimiters[i])do i:=i+1;
    if i<=DelimCount then Result := True;
  end;
end;

function TDDTokenizer.IsSepar(ch: char): boolean;
begin
  if IsBlank(ch) or IsDelim(ch)
  then Result := True
  else Result := False;
end;

function TDDTokenizer.isEOT: boolean;
begin
  if Position > TextLen
  then begin
    Position := 1;       // ������ �������������� Position ��� ���������� ��������
    Result := True;
  end
  else Result := False;
end;

//============================================================================
end.
