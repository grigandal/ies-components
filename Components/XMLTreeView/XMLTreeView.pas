unit XMLTreeView;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, ComCtrls,
  XMLDoc, XMLIntf, Dialogs, StdActns, Forms, Variants;

type
  TNodeOption = (
    noAttrsAsNodes,     // ���������� �� � ������ �������� ����� ��� ����
    noAttrsAsString,    // ���������� �� � ������ �������� ����� ��� ������ ����� ����� ����
    noShowTextNodes,    // ���������� �� ��������� ����
    noTextNodesAsText,  // ������ #text ������������ ��� ����� ����
    noReadOnly          // �������������� ���������
  );
  TNodeOptions = set of TNodeOption;

  TXMLTreeView = class(TTreeView)
  private
    FNodeOptions: TNodeOptions;
    FOnHint: TNotifyEvent;
  public
    XMLDocument: IXMLDocument;
    HoverXMLNode: IXMLNode; // ����, �� ������� ��� ���� ���������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetAttributes(XMLNode: IXMLNode; TreeNode: TTreeNode);
    procedure RecurseInto(XMLNode: IXMLNode; TreeNode: TTreeNode);
    procedure SynchronizeTree(XMLNode: IXMLNode);
    function GetXMLNode(Node: TTreeNode): IXMLNode;
    function GetTreeNode(Node: IXMLNode): TTreeNode;
    procedure NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure TVMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  published
    property NodeOptions: TNodeOptions read FNodeOptions write FNodeOptions;
    property OnHint: TNotifyEvent read FOnHint write FOnHint;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DD Pack', [TXMLTreeView]);
end;

{=============================== TXMLTreeView ============================}

constructor TXMLTreeView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  XMLDocument := TXMLDocument.Create(NIL);
  Self.ReadOnly := True;
  Self.RightClickSelect := True;
  Self.HotTrack := True;
  Self.NodeOptions := [noAttrsAsNodes, noShowTextNodes, noTextNodesAsText, noReadOnly];
  Self.OnMouseUp := NodeMouseUp;
  Self.OnMouseMove := TVMouseMove;
end;

destructor TXMLTreeView.Destroy;
begin
  inherited Destroy;
end;

procedure TXMLTreeView.NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Node: TTreeNode;
begin
  // ��� ����� ���������� ������ ������� ���� ���� ���� �������� OnChange
  if Button = mbRight then begin
    Node := Self.Selected; // �������� ��� �������� ������ �������
    Node.Selected := True; // �����������, �.�. ����� ��������� ��������� ��, ��� ����
    Node.Focused := True;
    if Assigned(Self.OnChange) then Self.OnChange(Sender, Self.Selected);
  end;
end;

procedure TXMLTreeView.TVMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  xn: IXMLNode;
begin
  xn := Self.GetXMLNode(Self.GetNodeAt(X, Y));
  if (xn <> nil) and (HoverXMLNode <> xn) then begin
    HoverXMLNode := xn;
    if Assigned(OnHint) then OnHint(Self);
  end;
end;

procedure TXMLTreeView.SynchronizeTree(XMLNode: IXMLNode);
var Node: TTreeNode;
begin
  if (not XMLDocument.Active)or(XMLDocument.DocumentElement = nil) then exit;
  if XMLNode = XMLDocument.DocumentElement then begin
    Self.Items.Clear;
    Node := Self.Items.AddChild(NIL, XMLNode.NodeName);
  end
  else begin
    Node := GetTreeNode(XMLNode);
    Node.Text := XMLNode.NodeName;
    Node.DeleteChildren;
  end;
  RecurseInto(XMLNode, Node);
  Self.FullExpand;
end;


procedure TXMLTreeView.GetAttributes(XMLNode: IXMLNode; TreeNode: TTreeNode);
var
  i: integer;
  XMLAttr: IXMLNode;
  s: WideString;
begin
  // ���� �������� ��� ������ ����������, ��
  if noAttrsAsString in NodeOptions then begin
    s := XMLNode.NodeName;
    for i:=0 to XMLNode.AttributeNodes.Count-1 do begin
      XMLAttr := XMLNode.AttributeNodes.Nodes[i];
      s := s + ' ' + XMLAttr.NodeName + '="' + XMLAttr.Text + '"';
    end;
    TreeNode.Text := s;
  end;
  // ���� �������� ��� ���� ����������, ��
  if noAttrsAsNodes in NodeOptions then
  for i:=0 to XMLNode.AttributeNodes.Count-1 do begin
    XMLAttr := XMLNode.AttributeNodes.Nodes[i];
    Self.Items.AddChild(TreeNode, XMLAttr.NodeName + '=' + XMLAttr.Text);
  end;
end;

procedure TXMLTreeView.RecurseInto(XMLNode: IXMLNode; TreeNode: TTreeNode);
var
  i: integer;
  XMLChild: IXMLNode;
  TreeChild: TTreeNode;
begin
  GetAttributes(XMLNode, TreeNode);
  for i:=0 to xmlNode.ChildNodes.Count-1 do begin
    XMLChild := xmlNode.ChildNodes.Nodes[i];
    if xmlChild.NodeType = ntText then begin
      if (noShowTextNodes in NodeOptions) then // ���� ���� ���������� ��������� ����
        if (noTextNodesAsText in NodeOptions) then // ���� ���������� ��� �����
          Self.Items.AddChild(TreeNode,xmlChild.Text)
        else
          Self.Items.AddChild(TreeNode,xmlChild.NodeName)
    end
    else begin // ����� �������� ��� ��� � ����� ������
      TreeChild := Self.Items.AddChild(TreeNode, xmlChild.NodeName);
      RecurseInto(XMLChild, TreeChild);
    end;
  end;
end;


function TXMLTreeView.GetXMLNode(Node: TTreeNode): IXMLNode;
var XMLParent: IXMLNode;
begin
  if Node = NIL then
    Result := NIL
  else if Node = Items.GetFirstNode then // ����� �� ��������
    Result := XMLDocument.DocumentElement
  else begin
    XMLParent := GetXMLNode(Node.Parent);
    Result := XMLParent.ChildNodes.Nodes[Node.Index];
  end;
end;

function TXMLTreeView.GetTreeNode(Node: IXMLNode): TTreeNode;
var Parent: TTreeNode;
begin
  // ���� ��������������� ���� � treeview �� �������, �� �� ����� (����� ����� ���������)!!!
  if Node = NIL then
    Result := NIL
  else if Node = XMLDocument.DocumentElement then
    Result := Items.GetFirstNode
  else begin
    Parent := GetTreeNode(Node.ParentNode);
    Result := Parent.Item[Node.ParentNode.ChildNodes.IndexOf(Node)];
  end;
end;



end.
