unit GAPerformer;

interface

uses
  Windows, Messages, SysUtils, Classes, Dialogs;

type
  PChromosome = ^TChromosome;
  TChromosome = record
    binary: PByteArray; // ���������
    FV: integer;
  end;

  TChromosomes = class(TPersistent)
  private
    FCount: integer;
    FLength: integer;
  published
    property Count: integer read FCount write FCount;
    property Length: integer read FLength write FLength;
  end;

  TExitConditions = class(TPersistent)
  private
    FGenNum: integer;
    FEpsilon: real;
  published
    property MaxGenerations: integer read FGenNum write FGenNum;
    property Epsilon: real read FEpsilon write FEpsilon;
  end;

  TStats = record
    MaxFV: integer;                     // �������� ������� �����������������
    MinFV: integer;                     // ������� ������� �����������������
    AveFV: integer;                     // ������� ������� �����������������
    Generation: integer;                // ����� ���������
  end;

  TGoal = (gMaximize, gMinimize);
  TSelectionType = (stRandom, stElite, stRange);
  TNext = (ngRandom, ngElite, ngKidsOnly);
  TGetFitnessValue = function (Chromosome: PChromosome): integer of object;

  TGAPerformer = class(TComponent)
  private
    FChromosomes: TChromosomes;         // �������� �������� (�������� ��������� � �����)
    FStop: TExitConditions;             // ������� ������ (�������� � ������� ���������)
    FGoal: TGoal;                       // ������������ / ����������� ������� �����������������
    FSelectionType: TSelectionType;     // ������ ������ ������������ ���
    FMutationProbability: real;         // ����������� ������� ����
    FNext: TNext;                       // ������ ������ � ��������� ���������
    FGetFitnessValue: TGetFitnessValue; // ������� ��� ���������� ������� �����������������
    FStats: TStats;                     // ���������� �� ��: Min, Max, Ave
    Population: TList;                  // ������� ��������� ��������
    Parents: TList;                     // ������������ ��������� ��� �����������
    Pool: TList;                        // ����� ��������� + ������
    procedure SelectParents;            // ����� ������������ ��� �� ���������
    procedure CrossOverParents;         // ����������� ��������
    procedure SelectNextGeneration;     // ����� ��� ���������� ���������
    procedure Mutate;                   // ������� ��������
    procedure ClearList(L: TList);      // ������� ������ ��������
  protected
  public
    function Stats: TStats;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Init;
    procedure CalcStats;                // ������� ���������� �� ��������� ��
    procedure StepIt;                   // ������� � ���������� ���������
    function TimeToStop: boolean;       // ��������� ������� ������
    function Best: PChromosome;         // ������ ���������
    procedure GeneratePopulation;       // ������ ��������� ��������
  published
    property Chromosomes: TChromosomes read FChromosomes write FChromosomes;
    property Stop: TExitConditions read FStop write FStop;
    property Goal: TGoal read FGoal write FGoal;
    property SelectionType: TSelectionType read FSelectionType write FSelectionType;
    property MutationProbability: real read FMutationProbability write FMutationProbability;
    property NextGeneration: TNext read FNext write FNext;
    property OnGetFitnessValue: TGetFitnessValue read FGetFitnessValue write FGetFitnessValue;
  end;

  function CMP(Item1, Item2: Pointer): Integer;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DD Pack', [TGAPerformer]);
end;

//============================================================================

constructor TGAPerformer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Chromosomes := TChromosomes.Create;
  Chromosomes.Count := 10;
  Stop := TExitConditions.Create;
  Goal := gMinimize;
  MutationProbability := 0.01;
  NextGeneration := ngRandom;
  Population := TList.Create;
  Parents := TList.Create;
  Pool := TList.Create;
end;

procedure TGAPerformer.ClearList(L: TList);
var i: integer;
begin
  for i:=0 to L.Count-1 do begin
    FreeMem(PChromosome(L[i]).binary);
    FreeMem(L[i]);
  end;
  L.Clear;
end;

procedure TGAPerformer.Init;
begin
  ClearList(Parents);
  ClearList(Pool);
  ClearList(Population);
end;

destructor TGAPerformer.Destroy;
begin
  Init;
  Parents.Free;
  Pool.Free;
  Population.Free;
  Stop.Free;
  Chromosomes.Free;
  inherited Destroy;
end;

//---------------------------- ���������� -------------------------------

function TGAPerformer.Stats: TStats;
begin
  Result := FStats;
end;

procedure TGAPerformer.CalcStats;
var Sum, i: integer;
begin
  FStats.MaxFV := PChromosome(Population[Population.Count-1]).FV;
  FStats.MinFV := PChromosome(Population[0]).FV;
  Sum := 0;
  for i := 0 to Population.Count-1 do begin
    Sum := Sum + PChromosome(Population[i]).FV;
  end;
  FStats.AveFV := Sum div Population.Count;
end;

function TGAPerformer.Best: PChromosome;
begin
  if Population.Count > 0 then begin
    if Goal = gMaximize then
      Result := Population[Population.Count-1]
    else
      Result := Population[0]
  end
  else Result := NIL;
end;

//----------------------------- ��������� -------------------------------

procedure TGAPerformer.GeneratePopulation;       // ������ ��������� ��������
var
  i,j: integer;
  p: PChromosome;
begin
  Randomize;
  ClearList(Population);
  for i:=0 to Chromosomes.Count-1 do begin
    getmem(p, sizeof(TChromosome));
    getmem(p.binary, Chromosomes.Length);
    for j:=0 to Chromosomes.Length-1 do
      p.binary[j] := random(2);
    p.FV := OnGetFitnessValue(p);
    Population.Add(p);
  end;
  Population.Sort(@CMP);
  FStats.Generation := 1;
  CalcStats; // ��������� MinFV, MaxFV, AveFV
end;

function CMP(Item1, Item2: Pointer): Integer;
begin
  Result := PChromosome(Item1).FV - PChromosome(Item2).FV;
end;

//----------------------------- ������� -------------------------------

procedure TGAPerformer.SelectParents;            // ����� ������������ ��� �� ���������
var i: integer;
begin
  for i:=0 to Population.Count-1 do begin
    Parents.Add(Population[i]);
  end;
end;

procedure TGAPerformer.CrossOverParents;         // ����������� ��������
var
  i,j, where: integer;
  p: PChromosome;
begin
  i:=0;
  while i<Parents.Count-1 do begin
    where := random(Chromosomes.Length-1)+1;
    // �������� ��� ���������
    getmem(p, sizeof(TChromosome));
    getmem(p.binary, Chromosomes.Length);
    for j:=0 to where do
      p.binary[j] := PChromosome(Parents[i]).binary[j];
    for j:=where+1 to Chromosomes.Length-1 do
      p.binary[j] := PChromosome(Parents[i+1]).binary[j];
    p.FV := OnGetFitnessValue(p);
    Pool.Add(p);
    // � ������
    getmem(p, sizeof(TChromosome));
    getmem(p.binary, Chromosomes.Length);
    for j:=0 to where do
      p.binary[j] := PChromosome(Parents[i+1]).binary[j];
    for j:=where+1 to Chromosomes.Length-1 do
      p.binary[j] := PChromosome(Parents[i]).binary[j];
    p.FV := OnGetFitnessValue(p);
    Pool.Add(p);
    i := i+2;
  end;
  // ������� ������� ��������� � Pool
  for i:=0 to Population.Count-1 do
    Pool.Add(Population[i]);
  // ������� ������ �� Population �� Parents
  for i:=0 to Parents.Count-1 do Parents[i] := nil;
  Parents.Clear;
  // ������� ������ �� Population, ������ ��� ��������� � Pool
  for i:=0 to Population.Count-1 do Population[i] := nil;
  Population.Clear;
  Pool.Sort(@CMP);
end;

procedure TGAPerformer.SelectNextGeneration;    // ����� ��� ���������� ���������
var
  i,j,k, Sum: integer;
  ProbArr: array of real;
  IntArr: array of real;
  CheckSum, prob: real;
  p: PChromosome;
  s: string;
begin
  // �������� �����: ��� ����������� p(i) = 2*(N-i+1)/(N*(N+1)), ��� 1 - ���, N - ����
  // ��� ������������ p(i) = 2*i/(N*(N+1)), � ��� ������ i ������������� i+1
  // ����������� ��������� -> ����� ���� ��� ��������� (������� ������ �� ����� ��������)
  SetLength(ProbArr, Pool.Count);
  SetLength(IntArr, Pool.Count);
  ProbArr[0] := 2/(Pool.Count+1);
  {sum := PChromosome(Pool[Pool.Count-1]).FV * Pool.Count;
  for i:=0 to Pool.Count-1 do sum := sum - PChromosome(Pool[i]).FV;
  ProbArr[0] := (PChromosome(Pool[Pool.Count-1]).FV - PChromosome(Pool[0]).FV) / Sum;}
  CheckSum := ProbArr[0];
  IntArr[0] := ProbArr[0];
  // ������� ����������� ������ ��������� � ��������� ���������
  s := '[0; '+FloatToStr(IntArr[0])+']';
  for i:=1 to Pool.Count-1 do begin
    ProbArr[i] := 2*(Pool.Count-i)/((Pool.Count+1)*Pool.Count);
    //ProbArr[i] := (PChromosome(Pool[Pool.Count-1]).FV - PChromosome(Pool[i]).FV) / Sum;
    CheckSum := CheckSum + ProbArr[i];
    IntArr[i] := ProbArr[i]+IntArr[i-1];
    s:=s+#13+'['+FloatToStr(IntArr[i-1])+'; '+FloatToStr(IntArr[i])+']';
  end;
  //ShowMessage(s);
  // ������������� �����
  for j:=1 to Chromosomes.Count do begin
    prob := random;
    i := 1;
    while i<Length(IntArr) do begin
      if (IntArr[i-1] < prob) and (prob <= IntArr[i]) then break;
      Inc(i);
    end;
    if i = Length(IntArr) then i := 0;
    // �������� i-�� ��������� �������� � ����� ���������
    getmem(p, sizeof(TChromosome));
    getmem(p.binary, Chromosomes.Length);
    for k:=0 to Chromosomes.Length-1 do
      p.binary[k] := PChromosome(Pool[i]).binary[k];
    p.FV := OnGetFitnessValue(p);
    Population.Add(p);
  end;
  // �������� �� Pool � Population ������ 50%, ��������� �������
  {for i:=0 to Chromosomes.Count-1 do begin
    getmem(p, sizeof(TChromosome));
    getmem(p.binary, Chromosomes.Length);
    for j:=0 to Chromosomes.Length-1 do
      p.binary[j] := PChromosome(Pool[i]).binary[j];
    p.FV := OnGetFitnessValue(p);
    Population.Add(p);
  end;}
  for i:=0 to Pool.Count-1 do begin
    FreeMem(PChromosome(Pool[i]).binary);
    FreeMem(Pool[i]);
  end;
  Pool.Clear;
end;

procedure TGAPerformer.Mutate;                  // ������� ��������
var i,j: integer;
begin
  // �������������� ����� � ������������ MutationProbability
  // //������ � ��������� ��������� �� �������
  if (Stats.Generation mod 100) = 0 then // �������� �� ��������
    MutationProbability := MutationProbability * 4/5;
  for i:=1 to Population.Count-2 do begin
    for j:=0 to Chromosomes.Length-1 do
      if random <= MutationProbability then
        PChromosome(Population[i]).binary[j] := 1-PChromosome(Population[i]).binary[j];
    PChromosome(Population[i]).FV := OnGetFitnessValue(Population[i]);
  end;
end;

//----------------------------- ��� ������������� ��������� -------------

procedure TGAPerformer.StepIt;
begin
  SelectParents;
  CrossOverParents;
  SelectNextGeneration;
  FStats.Generation := FStats.Generation + 1;
  if FStats.Generation < Stop.MaxGenerations then
    Mutate;
  CalcStats;
end;

function TGAPerformer.TimeToStop: boolean;
begin
  Result := False;
  if Goal = gMaximize then begin
    if (Stop.MaxGenerations > 0) and (FStats.Generation >= Stop.MaxGenerations) then Result := True;
  end
  else begin
    if (Stop.MaxGenerations > 0) and (FStats.Generation >= Stop.MaxGenerations) then Result := True;
    if Stats.MinFV = 0 then Result := True;
  end;
end;


end.
