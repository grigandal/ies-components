unit UDesignForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XMLDoc, StdCtrls,XMLIntf, ExtCtrls, ToolWin, ComCtrls, ImgList,
  CheckLst, Buttons, xmldom, msxmldom, Aspr95, KBCompon, KBTypes, UExplUtils;

type

  TDesignForm = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    NewTB: TToolButton;
    ImageList1: TImageList;
    OpenTB: TToolButton;
    SaveTB: TToolButton;
    GroupBox2: TGroupBox;
    GroupBox5: TGroupBox;
    ThenMemo: TMemo;
    Splitter3: TSplitter;
    OpenDlg: TOpenDialog;
    SaveDlg: TSaveDialog;
    GroupBox7: TGroupBox;
    RuleIDEdit: TEdit;
    GroupBox6: TGroupBox;
    ElseMemo: TMemo;
    GroupBox4: TGroupBox;
    RuleTV: TTreeView;
    Splitter2: TSplitter;
    GroupBox3: TGroupBox;
    RulesCLB: TListBox;
    SaveExplRecB: TButton;
    GenerateB: TButton;
    procedure FormShow(Sender: TObject);
    procedure NewTBClick(Sender: TObject);
    procedure OpenTBClick(Sender: TObject);
    procedure SaveTBClick(Sender: TObject);
    procedure RulesCLBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SaveExplRecBClick(Sender: TObject);
    procedure GenerateBClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillControls;
    procedure ShowRuleStructure(r: record_rule);
    procedure ShowRecordedExpl(r: record_rule);
    function ParseLogicExpr(s:string): string;
  public
    { Public declarations }
    ExplFile: string;
    RulesDoc: IXMLDocument;
    KBControl: TKBControl;
  end;

//var DesignForm: TDesignForm;

implementation

{$R *.dfm}

// ---------------------------------------------------------------------

procedure TDesignForm.FormCreate(Sender: TObject);
begin
  // ��������� � ������������� �� �����������
  RulesDoc := nil;
  KBControl := nil;
end;

procedure TDesignForm.FormDestroy(Sender: TObject);
begin
  RulesDoc := nil;
  KBControl := nil;
end;

procedure TDesignForm.FormShow(Sender: TObject);
begin
  FillControls;
  // ������� ���������� ������ ������� ���� ��� ����
  if RulesCLB.Count > 0 then begin
    RulesCLB.ItemIndex := 0;
    RulesCLB.OnClick(Sender);
  end;
end;

//---------------------------------------------------------------------

procedure TDesignForm.FillControls;
var
  i: integer;
  r: record_rule;
begin
  // �� ������ ���� ������ ��������� ������ ������ � RulesCLB
  RuleTV.Items.Clear;
  for i:=0 to KBControl.KB.ListRules.Count-1 do begin
   r := record_rule(KBControl.KB.ListRules.Items[i]);
   RulesCLB.Items.Add(IntToStr(r.n_rule) + '. ' + r.comment);
  end;
end;

// ---------------------------------------------------------------------

procedure TDesignForm.NewTBClick(Sender: TObject);
begin
  RulesDoc.Active:=true;
  if RulesDoc.ChildNodes.Nodes[0].ChildNodes.Count<>0 then begin
    if MessageDlg('������ �� �� ��������� ������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      SaveTBClick(Sender);
  end;
  RulesDoc.XML.Clear;
  RulesDoc.Active:=true;
  RulesDoc.AddChild('rules');
end;

procedure TDesignForm.OpenTBClick(Sender: TObject);
begin
  // ���� ���� ��� ���������, ��...
  if RulesDoc.ChildNodes.Nodes[0].ChildNodes.Count<>0 then begin
    if MessageDlg('������ �� �� ��������� ������� ��������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      SaveTBClick(Sender);
  end;

  if OpenDlg.Execute then begin
    RulesDoc.LoadFromFile(OpenDlg.FileName);
  end;

  FillControls;
end;

procedure TDesignForm.SaveTBClick(Sender: TObject);
begin
  RulesDoc.SaveToFile(ExplFile);
  //if SaveDlg.Execute then RulesDoc.SaveToFile(SaveDlg.FileName);
end;


function TDesignForm.ParseLogicExpr(s:string): string;
var
  i: integer;
  sret:string;
begin
  sret := '';
  i := 1;
  while i<=Length(s) do begin
    case s[i] of
    'P': ;
    '&': sret := sret + '�';
    '|': sret := sret + '���';
    '~': sret := sret + '��';
    else
      sret := sret + s[i];
    end;
    inc(i);
  end;
  sret := sret + ' ';
  Result := sret;
end;

procedure TDesignForm.ShowRuleStructure(r: record_rule);
var
  nif, nthen, nelse: TTreeNode;
  i,j, h,Pos: integer;
  K1, Code: integer;
  assert: Tassertion;
  obj: record_obj;
  attr: record_attr;
  s,incident, snum, sgn: string;
begin
  RuleTV.Items.Clear;

  // �������� �������
  nif := RuleTV.Items.AddChild(nil,'����');
  incident := ParseLogicExpr(r.incident);
  i:=1;
  s:='';
  while i<=length(incident) do begin
    case incident[i] of
    '0'..'9':
     begin
       for j:=i to length(incident) do begin
         case incident[j] of
         '0'..'9':
         else
           begin
            Pos:=j;
            break;
           end;
         end;
       end;
       snum:='';
       For h:=i to Pos-1 do snum := snum + incident[h];

      Val(snum, K1, Code);
      assert := Tassertion(r.list_attr_if.Items[K1-1]);
      obj := record_obj(KBControl.KB.ListObjs.Items[assert.n_obj-1]);
      attr := record_attr(obj.ListAttrs.Items[assert.n_attr-1]);
      case assert.predicat of
        0: sgn := ' = ';
        1: sgn := ' > ';
        2: sgn := ' < ';
        3: sgn := ' >= ';
        4: sgn := ' <= ';
        5: sgn := ' <> ';
      else sgn := ' - ';
      end;
      s := s + obj.comment + '.' + attr.comment + sgn + assert.znach + ' ';
      i := Pos;
     end
    else
      begin
        s := s + incident[i];
        i := i+1;
      end;
    end;
  end;

  RuleTV.Items.AddChild(nif, s);

  // �������� ��������� then
  nthen := RuleTV.Items.AddChild(nil,'��');

  for i:=0 to r.list_attr_then.Count-1 do begin
    assert := Tassertion(r.list_attr_then.Items[i]);
    obj := record_obj(KBControl.KB.ListObjs.Items[assert.n_obj-1]);
    attr := record_attr(obj.ListAttrs.Items[assert.n_attr-1]);
    s := obj.comment + '.' + attr.comment + ' = ' + assert.znach;
    RuleTV.Items.AddChild(nthen, s);
  end;

  // �������� ��������� else
  if r.list_attr_else.Count = 0 then begin
    ElseMemo.Enabled := false;
    RuleTV.FullExpand;
    exit;
  end;

  ElseMemo.Enabled := true;
  nelse := RuleTV.Items.AddChild(nil, '�����');
  for i:=0 to r.list_attr_else.Count-1 do begin
    assert := Tassertion(r.list_attr_else.Items[i]);
    obj := record_obj(KBControl.KB.ListObjs.Items[assert.n_obj-1]);
    attr := record_attr(obj.ListAttrs.Items[assert.n_attr-1]);
    s := obj.comment + '.' + attr.comment + ' = ' + assert.znach;
    RuleTV.Items.AddChild(nelse, s);
  end;

  RuleTV.FullExpand;
end;

procedure TDesignForm.ShowRecordedExpl(r: record_rule);
var
  i: integer;
  XMLNode,n: IXMLNode;
  found: boolean;
begin
  ThenMemo.Clear;
  ElseMemo.Clear;

  // ����� ���������� ���������� � RuleDoc, ���� ��� ��� ����
  found := false;
  i := 0;
  while (i < RulesDoc.DocumentElement.ChildNodes.Count) and not found do begin
    XMLNode := RulesDoc.DocumentElement.ChildNodes[i];
    // � RuleIDEdit.Tag ������������ ����� ������� � ���� ������
    found := XMLNode.ChildNodes['name'].Text = IntToStr(RuleIDEdit.Tag);
    Inc(i);
  end;

  if found then begin
    // ���� �����, �� ������� �� �����
    n := XMLNode.ChildNodes.FindNode('thentext');
    if n <> nil then ThenMemo.Lines.Text := n.Text;
    n := XMLNode.ChildNodes.FindNode('elsetext');
    if n <> nil then ElseMemo.Lines.Text := n.Text;
  end
  else begin
    // � ���� ��� ������ ����, �� ��� ������ ����� �������, �������� ��� ���� ����
    XMLNode := RulesDoc.DocumentElement.AddChild('rule');
    n := XMLNode.AddChild('name');
    n.Text := IntToStr(RuleIDEdit.Tag);
    XMLNode.AddChild('thentext');
    XMLNode.AddChild('elsetext');
  end;
end;

procedure TDesignForm.RulesCLBClick(Sender: TObject);
var
  r: record_rule;
begin
  r := record_rule(KBControl.KB.ListRules.Items[RulesCLB.ItemIndex]);
  RuleIDEdit.Text := IntToStr(r.n_rule) + '. ' + r.comment;
  RuleIDEdit.Tag := r.n_rule;

  ShowRuleStructure(r);
  ShowRecordedExpl(r);
end;

procedure TDesignForm.SaveExplRecBClick(Sender: TObject);
var
  i: integer;
  XMLNode,n: IXMLNode;
  found: boolean;
begin
  // �������� � RulesDoc ���������. ������ ���� ��� ��� ������������� ����
  found := false;
  i := 0;
  while (i < RulesDoc.DocumentElement.ChildNodes.Count) and not found do begin
    XMLNode := RulesDoc.DocumentElement.ChildNodes[i];
    // � RuleIDEdit.Tag ������������ ����� ������� � ���� ������
    found := XMLNode.ChildNodes['name'].Text = IntToStr(RuleIDEdit.Tag);
    Inc(i);
  end;

  if found then begin
    // ���� �����, �� ��������
    n := XMLNode.ChildNodes.FindNode('thentext');
    if n <> nil then n.Text := ThenMemo.Lines.Text;
    n := XMLNode.ChildNodes.FindNode('elsetext');
    if n <> nil then n.Text := ElseMemo.Lines.Text;
  end
end;

procedure TDesignForm.GenerateBClick(Sender: TObject);
var
  r: record_rule;
begin
  // ����������� ���������� ����������
  r := record_rule(KBControl.KB.ListRules.Items[RulesCLB.ItemIndex]);

  ThenMemo.Lines.Text := '��������� ' + list2txt(KBControl, r.list_attr_if) +
           ', �� ' + list2txt(KBControl, r.list_attr_then);

  if r.list_attr_else.Count > 0 then
    ElseMemo.Lines.Text := '��� ��� �� ����� ��, ��� ' + list2txt(KBControl, r.list_attr_if) +
             ', �� ' + list2txt(KBControl, r.list_attr_else);
end;

end.
