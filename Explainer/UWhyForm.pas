unit UWhyForm;

interface

uses
 Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  StdCtrls, XMLIntf, XMLDoc, Aspr95, KBTypes;

type

  TWhyForm = class(TForm)
    FactMemo: TMemo;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  //  ExplDoc: IXMLDocument;
    KBControl: TKBControl;
    procedure Reset;

    procedure AddFact(n_obj, n_attr: integer; znach: widestring);
    procedure main( p1,p2: widestring);
    function getcomment( n_obj,n_attr: integer):widestring;
  end;

var
  WhyForm: TWhyForm;
  lst: Tstringlist;
implementation

{$R *.dfm}

procedure TWhyForm.FormCreate(Sender: TObject);
begin
 // ExplDoc := TXMLDocument.Create(nil);
end;

procedure TWhyForm.FormDestroy(Sender: TObject);
begin
  //ExplDoc := nil;
end;

procedure TWhyForm.Reset;
begin
  FactMemo.Lines.Clear;
  lst:= TStringlist.create;
end;


function TWhyForm.getcomment( n_obj,n_attr: integer):widestring;
var
i,j: integer;
 Obj: record_obj;
  Attr: record_attr;
  begin
  i:=0;
  while i<KBControl.KB.ListObjs.Count do begin
    j:=0;
    Obj := record_obj(KBControl.KB.ListObjs.Items[i]);
    if Obj.n_obj = n_obj then begin
      while j<Obj.ListAttrs.Count do begin
        Attr := record_attr(Obj.ListAttrs.Items[j]);
        if Attr.n_attr = n_attr then begin
          Result := Attr.comment;
          break;
        end;
        Inc(j);
      end;
      break;
    end;
    Inc(i);
 end;
end;

procedure TWhyForm.main( p1,p2: widestring);
var
  rec: record_rule;
  n_attr,n_obj,i,j:integer;
  s: string;
begin
 Reset;
 n_obj:=strtoint(p1);
 n_attr:=strtoint(p2);

  s := '   �������������� ������ ����� � ����� � ���, ��� �������� ��������� "' + getcomment(n_obj, n_attr) +
       '" ����� �������� �� ��� �����������. ';
  FactMemo.Lines.Add(s);
  s := '   �� ������� ��������� ������� �������� ��������� ����������, ����������� ��� ����������� ������ �������:';
  FactMemo.Lines.Add(s);
  FactMemo.Lines.Add('');

  for i:=0 to KBControl.KB.ListRules.count-1 do
 begin

    rec:= record_rule(KBControl.KB.ListRules.Items[i]);
  for j:=0 to rec.Flist_attr_if.Count-1 do

     if n_obj =  list(rec.Flist_attr_if.Items[j]).fn_obj then


         if n_attr =  list(rec.Flist_attr_if.Items[j]).fn_attr then

             AddFact( list(rec.Flist_attr_then.Items[0]).fn_obj,list(rec.Flist_attr_then.Items[0]).fn_attr, list(rec.Flist_attr_then.Items[0]).znach)  ;
           if (rec.Flist_attr_else.Count = 1) and (list(rec.Flist_attr_else.Items[0]).fn_attr <> list(rec.Flist_attr_then.Items[0]).fn_attr) then
             AddFact( list(rec.Flist_attr_else.Items[0]).fn_obj,list(rec.Flist_attr_else.Items[0]).fn_attr,list(rec.Flist_attr_else.Items[0]).znach)    ;

  end;
 end;
procedure TWhyForm.AddFact(n_obj, n_attr: integer; znach: widestring);
var
 i, flag: integer;
 begin
   if lst.Count = 0 then
   begin
     lst.add(getcomment(n_obj,n_attr));
     FactMemo.Lines.Add('  ' + getcomment(n_obj,n_attr));
     FactMemo.Lines.Add('');
   end;
   flag:=0;
   for i:=0 to lst.count-1 do
    begin
     If getcomment(n_obj,n_attr) = lst[i] then flag:= -1;
    end;
     If flag = 0 then
     Begin
      lst.add(getcomment(n_obj,n_attr));
      FactMemo.Lines.Add('  ' + getcomment(n_obj,n_attr));
      FactMemo.Lines.Add('');
     End;
 end;

procedure TWhyForm.Button1Click(Sender: TObject);
begin
 close;
end;

end.
