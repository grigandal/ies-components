unit GraphModule;

interface

uses Types, Dialogs, SysUtils, ExtCtrls, Classes, Graphics, RulesAndFacts;

type
Node = class
  private
    radius: integer;
    coord: TRect;
    next: TList;
    previous : TList;
    data: FactInfo;
    level: integer;

  public
    constructor Create; overload;
    constructor Create(r: integer; c: TRect; d: FactInfo; l: integer); overload;
    constructor Create(r: integer; x, y: integer; d: FactInfo; l: integer); overload;
    procedure draw(Canvas: TCanvas; x0, y0: integer; maxLevel: integer);
    function getRadius: integer;
    function getCoord: TRect;
    function getNext: TList;
    function getPrevious: TList;
    function getData: FactInfo;
    procedure setRadius(r: integer);
    procedure setCoord(c: TRect); overload;
    procedure setCoord(x, y: integer); overload;
    procedure setNext(n: TList);
    procedure setPrevious(p: TList);
    procedure setData(d: FactInfo);
end;

Arch = class
  private
    arrowSize: integer;
    coord: TRect;
    previous: Node;
    next: Node;
    data: RuleInfo;

    procedure getArrowCoord(var p1: TPoint; var p2: TPoint);
    procedure getArchCoord(var p1: TPoint; var p2:TPoint);

  public
    constructor Create; overload;
    constructor Create(s: integer; p, n: Node; d: RuleInfo); overload;
    function getArrowSize: integer;
    function getCoord: TRect;
    function getPrevious: Node;
    function getNext: Node;
    function getData: RuleInfo;
    procedure setArrowSize(s: integer);
    procedure setCoord(c: TRect);
    procedure setPrevious(p: Node);
    procedure setNext(n: Node);
    procedure setData(d: RuleInfo);
    procedure draw(Canvas: TCanvas; x0, y0: integer);
end;

Graph = class
  private
    nodes: TList;
    arches: TList;
    maxLevel: integer;
    maxX, maxY: integer;

  public
    constructor Create; overload;
    constructor Create(rfInfo: RulesAndFactsInfo); overload;
    function getNodes: TList;
    function getArches: TList;
    function getMaxLevel: integer;
    function getMaxX: integer;
    function getMaxY: integer;
    procedure setMaxLevel(l: integer);
    procedure setMaxX(x: integer);
    procedure setMaxY(y: integer);
    procedure addNode(r: integer; data: FactInfo; previous, next: TList; x, y: integer); overload;
    procedure addNode(n: Node); overload;
    procedure addArch(a: Arch);
    procedure draw(Canvas: TCanvas; x0, y0: integer);
end;

function pieceLength(p1, p2: TPoint): integer;
function isInRect(p: TPoint; r: TRect): boolean;
function isInLine(p, b, e: TPoint): boolean;

implementation

constructor Node.Create;
begin
  next := TList.Create;
  previous := TList.Create;
  data := FactInfo.Create;
  coord.Left := 0;
  coord.Right := 0;
  coord.Top := 0;
  coord.Bottom := 0;
  level := 0;
end;

constructor Node.Create(r: integer; c: TRect; d: FactInfo; l: integer);
begin
  next := TList.Create;
  previous := TList.Create;
  data := d;
  coord := c;
  level := l;
end;

constructor Node.Create(r: integer; x, y: integer; d: FactInfo; l: integer);
begin
  next := TList.Create;
  previous := TList.Create;
  data := d;
  coord.Left := x;
  coord.Top := y;
  coord.Right := x + 2 * r;
  coord.Bottom := y + 2 * r;
  radius := r;
  level := l;
end;

procedure Node.draw(Canvas: TCanvas; x0, y0: integer; maxLevel: integer);
var
  i: integer;
begin
  if level = 1 then
    Canvas.Brush.Color := clRed;
  if level = maxLevel then
    Canvas.Brush.Color := clGreen;
  if (level > 1) and (level < maxLevel) then
    Canvas.Brush.Color := clBlue;
  Canvas.Ellipse(coord.Left - x0, coord.Top - y0, coord.Right - x0,
                coord.Bottom - y0);
  Canvas.Brush.Color := clWhite;
  for i := 0 to data.value.Count - 1 do
  begin
    Canvas.TextOut(round(coord.Left + radius - length(data.value.Strings[i]) * 3) - x0,
                  coord.Bottom + 13 * (i + 1) - y0, data.value.Strings[i]);
  end;
  //Canvas.TextOut(round(coord.Left + radius - length(data.value) * 4 / 2) - x0,
  //                coord.Bottom + 10 - y0, data.value);
end;

function Node.getRadius: integer;
begin
  getRadius := radius;
end;

function Node.getCoord: TRect;
begin
  getCoord := coord;
end;

function Node.getNext: TList;
begin
  getNext := next;
end;

function Node.getPrevious: TList;
begin
  getPrevious := previous;
end;

function Node.getData: FactInfo;
begin
  getData := data;
end;

procedure Node.setRadius(r: integer);
begin
  radius := r;
end;

procedure Node.setCoord(c: TRect);
begin
  coord := c;
end;

procedure Node.setCoord(x, y: integer);
begin
  coord.Left := x;
  coord.Top := y;
  coord.Right := x + 2 * radius;
  coord.Bottom := y + 2 * radius;
end;

procedure Node.setNext(n: TList);
begin
  next := n;
end;

procedure Node.setPrevious(p: TList);
begin
  previous := p;
end;

procedure Node.setData(d: FactInfo);
begin
  data := d;
end;

//////////////////////////////////////////////////////

constructor Arch.Create;
begin
  arrowSize := 0;
  previous := Node.Create;
  next := Node.Create;
  coord.Left := 0;
  coord.Top := 0;
  coord.Right := 0;
  coord.Bottom := 0;
  data := RuleInfo.Create;
end;

constructor Arch.Create(s: integer; p, n: Node; d: RuleInfo);
var
  p1, p2: TPoint;
begin
  arrowSize := s;
  previous := p;
  next := n;
  data := d;
  getArchCoord(p1, p2);
  coord.TopLeft := p1;
  coord.BottomRight := p2;
end;

function Arch.getArrowSize: integer;
begin
  getArrowSize := arrowSize;
end;

function Arch.getCoord: TRect;
begin
  getCoord := coord;
end;

function Arch.getPrevious: Node;
begin
  getPrevious := previous;
end;

function Arch.getNext: Node;
begin
  getNext := next;
end;

function Arch.getData: RuleInfo;
begin
  getData := data;
end;

procedure Arch.setArrowSize(s: integer);
begin
  arrowSize := s;
end;

procedure Arch.setCoord(c: TRect);
begin
  coord := c;
end;

procedure Arch.setPrevious(p: Node);
begin
  previous := p;
end;

procedure Arch.setNext(n: Node);
begin
  next := n;
end;

procedure Arch.setData(d: RuleInfo);
begin
  data := d;
end;

procedure Arch.draw(Canvas: TCanvas; x0, y0: integer);
var
  p1, p2: TPoint;
  p: array [1..3] of TPoint;
begin
  Canvas.MoveTo(coord.Left - x0, coord.Top - y0);
  Canvas.LineTo(coord.Right - x0, coord.Bottom - y0);
  getArrowCoord(p1, p2);
  p[1].X := p1.X - x0;
  p[1].Y := p1.Y - y0;
  p[2].X := p2.X - x0;
  p[2].Y := p2.Y - y0;
  p[3].X := coord.Right - x0;
  p[3].Y := coord.Bottom - y0;
  Canvas.Brush.Color := clBlack;
  Canvas.Polygon(p);
  Canvas.Brush.Color := clWhite;
  {Canvas.LineTo(p1.X - x0, p1.Y - y0);
  Canvas.LineTo(p2.X - x0, p2.Y - y0);
  Canvas.LineTo(coord.Right - x0, coord.Bottom - y0); }
end;

procedure Arch.getArrowCoord(var p1: TPoint; var p2: TPoint);
var
  c1, c2, c: TPoint;
  k: real;
begin
  if coord.Left = coord.Right then
  begin
    p1.X := round(coord.Right + arrowSize / sqrt(2));
    p1.Y := round(coord.Bottom - arrowSize / sqrt(2));
    p2.X := round(coord.Right - arrowSize / sqrt(2));
    p2.Y := p1.Y;
  end
  else
  begin
    k := (coord.Bottom - coord.Top) / (coord.Right - coord.Left);
    if k = 0 then
    begin
      c1.X := round(coord.Right + arrowSize / sqrt(2));
      c1.Y := coord.Bottom;
      c2.X := round(coord.Right - arrowSize / sqrt(2));
      c2.Y := coord.Bottom;
      if pieceLength(c1, coord.TopLeft) < pieceLength(coord.BottomRight, coord.TopLeft) then
        p1.X := round(coord.Right + arrowSize / sqrt(2))
      else
        p1.X := round(coord.Right - arrowSize / sqrt(2));
      p1.Y := round(coord.Bottom - arrowSize / sqrt(2));
      p2.X := p1.X;
      p2.Y := round(coord.Bottom + arrowSize / sqrt(2));
    end
    else
    begin
      c1.X := round(coord.Right + arrowSize / (sqrt(2 * (sqr(k) + 1))));
      c1.Y := round(k * c1.X + coord.Bottom - k * coord.Right);
      c2.X := round(coord.Right - arrowSize / (sqrt(2 * (sqr(k) + 1))));
      c2.Y := round(k * c2.X + coord.Bottom - k * coord.Right);
      if (pieceLength(c1, coord.TopLeft) < pieceLength(coord.BottomRight, coord.TopLeft)) then
        c := c1
      else
        c := c2;
      p1.X := round(c.X + arrowSize / (sqrt(2 * (1 / sqr(k) + 1))));
      p1.Y := round(-p1.X / k + c.Y + c.X / k);
      p2.X := round(c.X - arrowSize / (sqrt(2 * (1 / sqr(k) + 1))));
      p2.Y := round(-p2.X / k + c.Y + c.X / k);
    end;
  end;
end;

procedure Arch.getArchCoord(var p1: TPoint; var p2:TPoint);
var
  c1, c2, p11, p12, p21, p22: TPoint;
  k: real;
begin
  c1.X := (previous.coord.Left + previous.coord.Right) div 2;
  c1.Y := (previous.coord.Top + previous.coord.Bottom) div 2;
  c2.X := (next.coord.Left + next.coord.Right) div 2;
  c2.Y := (next.coord.Top + next.coord.Bottom) div 2;

  if c1.X = c2.X then
  begin
    p1.X := c1.X;
    p1.Y := c1.Y + previous.radius;
    p2.X := c2.X;
    p2.Y := c2.Y - next.radius;
  end
  else
  begin
    k := (c2.Y - c1.Y) / (c2.X - c1.X);
    p11.X := c1.X + round(previous.radius / (Sqrt(Sqr(k) + 1)));
    p11.Y := round(k * p11.X + c1.Y - k * c1.X);
    p12.X := c1.X - round(previous.radius / (Sqrt(Sqr(k) + 1)));
    p12.Y := round(k * p12.X + c1.Y - k * c1.X);
    if pieceLength(p11, c2) < pieceLength(p12, c2) then
    begin
      p1 := p11;
    end
    else
    begin
      p1 := p12;
    end;

    p21.X := c2.X + round(next.radius / (Sqrt(Sqr(k) + 1)));
    p21.Y := round(k * p21.X + c2.Y - k * c2.X);
    p22.X := c2.X - round(next.radius / (Sqrt(Sqr(k) + 1)));
    p22.Y := round(k * p22.X + c2.Y - k * c2.X);
    if pieceLength(p21, c1) < pieceLength(p22, c1) then
      p2 := p21
    else
      p2 := p22;
  end;
end;

//////////////////////////////////////////////////////

constructor Graph.Create;
begin
  nodes := TList.Create;
  arches := TList.Create;
  maxLevel := 0;
end;

constructor Graph.Create(rfInfo: RulesAndFactsInfo);
var
  level: integer;
  y: integer;
  i, j, k: integer;
  fInfo: FactInfo;
  rInfo: RuleInfo;
  n, n1: Node;
  a: Arch;
  radius, arrowSize: integer;
begin
  radius := 15;
  arrowSize := 10;
  nodes := TList.Create;
  arches := TList.Create;
  maxLevel := rfInfo.maxLevel;

  {for level := 1 to rfInfo.maxLevel do
  begin
    if (level mod 2) <> 0 then
      y := 10
    else
      y := 60;
    for i := 0 to rfInfo.facts.Count - 1 do
    begin
      fInfo := rfInfo.facts.Items[i];
      if fInfo.level = level then
      begin
        nilList := TList.Create;
        n := Node.Create(radius, 150 * level - 80, y, nilList, nilList, fInfo, fInfo.level);
        nodes.Add(n);
        y := y + 110;
      end;
    end;
    if maxY < y then
      maxY := y;
  end;}

  for level := 1 to rfInfo.maxLevel do
  begin
    if (level mod 2) <> 0 then
      y := 10
    else
      y := 60;
    for i := 0 to rfInfo.facts.Count - 1 do
    begin
      fInfo := rfInfo.facts.Items[i];
      if fInfo.level = level then
      begin
        n := Node.Create(radius, 150 * level - 80, y, fInfo, fInfo.level);
        // ���� ���� ���� �� ���� ��������� � ������� �������, �� �������
        for j := 0 to rfInfo.rules.Count - 1 do
        begin
          rInfo := rfInfo.rules.Items[j];
          if rInfo.isInIf(n.getData) or rInfo.isInThen(n.getData) or rInfo.isInElse(n.getData) then begin
            nodes.Add(n);
            y := y + 110;
            break;
          end;
        end;
      end;
    end;
    if maxY < y then
      maxY := y;
  end;

  for i := 0 to nodes.Count - 1 do
  begin
    n := nodes.Items[i];
    for j := 0 to rfInfo.rules.Count - 1 do
    begin
      rInfo := rfInfo.rules.Items[j];
      if rInfo.isInIf(n.getData) then
      begin
        for k := 0 to nodes.Count - 1 do
        begin
          n1 := nodes.Items[k];
          if (rInfo.isInThen(n1.getData) and (rInfo.oznach = 0)) or
             (rInfo.isInElse(n1.getData) and (rInfo.oznach = 1)) then
          begin
            a := Arch.Create(arrowSize, n, n1, rInfo);
            arches.Add(a);
            n.next.Add(a);
            n1.previous.Add(a);
          end;
        end;
      end;
    end;
  end;

  maxX := 150 * (maxLevel + 1) - 80;
end;

function Graph.getNodes: TList;
begin
  getNodes := nodes;
end;

function Graph.getArches: TList;
begin
  getArches := arches;
end;

function Graph.getMaxLevel: integer;
begin
  getMaxLevel := maxLevel;
end;

function Graph.getMaxX: integer;
begin
  getMaxX := maxX;
end;

function Graph.getMaxY: integer;
begin
  getMAxY := maxY;
end;

procedure Graph.setMaxLevel(l: integer);
begin
  maxLevel := l;
end;

procedure Graph.setMaxX(x: integer);
begin
  maxX := x;
end;

procedure Graph.setMaxY(y: integer);
begin
  maxY := y;
end;

procedure Graph.addNode(r: integer; data: FactInfo; previous, next: TList; x, y: integer);
var
  new: Node;
begin
  new := Node.Create;
  new.data := data;
  new.next := next;
  new.previous := previous;
  new.coord.Left := x;
  new.coord.Top := y;
  new.coord.Right := new.coord.Left + 2 * r;
  new.coord.Bottom := new.coord.Top + 2 * r;
  nodes.Add(new);
end;

procedure Graph.addNode(n: Node);
begin
  nodes.Add(n);
end;

procedure Graph.addArch(a: Arch);
begin
   arches.Add(a);
end;

procedure Graph.draw(Canvas: TCanvas; x0, y0: integer);
var
  i: integer;
  n: Node;
  a: Arch;
begin
  for i := 0 to nodes.Count - 1 do
  begin
    n := nodes.Items[i];
    //if n.next.Count > 0 then
      n.draw(Canvas, x0, y0, maxLevel);
  end;
  for i := 0 to arches.Count - 1 do
  begin
    a := arches.Items[i];
    a.draw(Canvas, x0, y0);
  end;
end;

////////////////////////////////////////////////////////////////////////////////

function pieceLength(p1, p2: TPoint): integer;
begin
  pieceLength := round(sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)));
end;

function isInRect(p: TPoint; r: TRect): boolean;
begin
  if (p.X > r.Left) and (p.X < r.Right) and (p.Y > r.Top) and (p.Y < r.Bottom) then
    isInRect:=true
  else
    isInRect:=false;
end;

function isInLine(p, b, e: TPoint): boolean;
var
  k: real;
begin
    if b.X = e.X then
    begin
      if (((p.Y > b.Y) and (p.Y < e.Y)) or ((p.Y < b.Y) and (p.Y > e.Y))) and (abs(b.X - p.X) < 10) then
        isInLine := true
      else
        isInLine :=false;
    end
    else
    begin
      k := (b.Y - e.Y) / (b.X - e.X);
      if k = 0 then
      begin
        if (((p.X > b.X) and (p.X < e.X)) or ((p.X < b.X) and (p.X > e.X))) and (abs(b.Y - p.Y) < 10) then
          isInLine := true
        else
          isInLine := false;
      end
      else
      begin
        if(((p.X >= b.X) and (p.X <= e.X)) or ((p.X <= b.X) and (p.X >= e.X)))
        and (((p.Y >= b.Y) and (p.Y <= e.Y)) or ((p.Y <= b.Y) and (p.Y >= e.Y)))
        and (abs(p.Y - k * p.X - b.Y + k * b.X) < 10) then
          isInLine := true
        else
          isInLine := false;
      end;
    end;
end;

end.
