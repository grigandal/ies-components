library ExplLib;

uses
  ComServ,
  ExplLib_TLB in 'ExplLib_TLB.pas',
  Explainer in 'Explainer.pas' {Explainer: CoClass},
  UMainForm in 'UMainForm.pas' {MainForm},
  UDesignForm in 'UDesignForm.pas' {DesignForm},
  UWhyForm in 'UWhyForm.pas' {WhyForm},
  GraphModule in 'GraphModule.pas',
  RulesAndFacts in 'RulesAndFacts.pas',
  UExplUtils in 'UExplUtils.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
