unit Explainer;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, ExplLib_TLB, StdVcl, XMLIntf, XMLDoc, SysUtils, Forms,
  Classes, Dialogs, UMainForm, UWhyForm, UDesignForm, Aspr95, KBTypes,
  RulesAndFacts, GraphModule, UExplUtils;

type
  TExplainer = class(TAutoObject, IExplainer)
  protected
    Name: WideString;
    Broker: Variant;

    function Get_Broker: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName, MessageText: WideString;
      out Output: OleVariant); safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Stop; safecall;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  private
    ConfigDoc: IXMLDocument;  // ���������������� ����������
    MainForm: TMainForm;
    WhyForm: TWhyForm;
    DesignForm: TDesignForm;
    TmpFolder: WideString;
    KBControl: TKBControl;
    ExplFile: WideString;
    KBFile: WideString;
    procedure Run;
    procedure Design();
    procedure Generate(KBFileName, ExplFileName: WideString);
    function GetAttrName(AttrPath: WideString; var AttrName: WideString): boolean;
    procedure GenerateDBfile;
    procedure RunWhy(obj_n,attr_n:widestring);
  end;

implementation

uses ComServ;

procedure TExplainer.Initialize;
begin
  inherited Initialize;
  // ����� ����� ��������� ������ �������
  KBControl := TKBControl.Create(NIL);
  KBControl.KB := TKnowledgeBase.Create(NIL);
  KBControl.KB.KBInterface := TKBInterface.Create(nil);

  ConfigDoc := TXMLDocument.Create(nil);

  MainForm := TMainForm.Create(Application);
  MainForm.TmpFolder := TmpFolder;
  MainForm.KBControl := KBControl;

  WhyForm := TWhyForm.Create(Application);
  WhyForm.KBControl := KBControl;
end;

destructor TExplainer.Destroy;
begin
  // � ����� �������
  KBControl.KB.KBInterface.Free;
  KBControl.KB.Free;
  KBControl.Free;

  MainForm.Free;
  WhyForm.Free;
  ConfigDoc := nil;
  inherited Destroy;
end;

function TExplainer.Get_Broker: OleVariant;
begin
  Result := Broker;
end;

function TExplainer.Get_Name: WideString;
begin
  Result := Name;
end;

procedure TExplainer.Set_Broker(Value: OleVariant);
begin
  Broker := Value;
end;

procedure TExplainer.Set_Name(const Value: WideString);
begin
  Name := Value;
end;

procedure TExplainer.Configurate(const Config: WideString);
begin
  { <config>
      <FileName>exp.xml</FileName>
      <kb>es.kbs</kb>
    </config> }
  try
    ConfigDoc.LoadFromXML(Config);

    try
      TmpFolder := Broker.Path;
    except
      // ���� ��� �������� Path ��� ��� �������
      TmpFolder := ExtractFilePath(Application.ExeName);
    end;
    TmpFolder := TmpFolder + 'expl\';
    MainForm.TmpFolder := TmpFolder;

    // �������� ����������
    ExplFile := ConfigDoc.DocumentElement.ChildNodes['FileName'].Text;
    // ���� ���� �������������, �� ������� ����
    if AnsiPos(':\', ExplFile) = 0 then begin
      try
        ExplFile := Broker.Path + ExplFile;
      except
        // ���� ��� �������� Path ��� ��� �������
        ExplFile := ExtractFilePath(Application.ExeName) + ExplFile;
      end;
    end;

    MainForm.RecordedDoc.LoadFromFile(ExplFile);

    // �������� ���� ��
    KBFile := ConfigDoc.DocumentElement.ChildNodes['kb'].Text;
    // ���� ���� �������������, �� ������� ����
    if AnsiPos(':\', KBFile) = 0 then begin
      try
        KBFile := Broker.Path + KBFile;
      except
        // ���� ��� �������� Path ��� ��� �������
        KBFile := ExtractFilePath(Application.ExeName) + KBFile;
      end;
    end;
    KBControl.ClearKB(KBControl.KB.KBOwner);
    KBControl.KB.FileName := KBFile;
    KBControl.KB.Load;
  except
    on E: Exception do ShowMessage('Explainer: ' + e.message);
  end;

end;

procedure TExplainer.ProcessMessage(const SenderName,
  MessageText: WideString; out Output: OleVariant);
var
  MessageDoc: IXMLDocument;
  n: IXMLNode;
  obj_n,attr_n,s: WideString;

begin
  MessageDoc := TXMLDocument.Create(nil);
  MessageDoc.LoadFromXML(MessageText);

  // ��������������� �� �����������
  n := MessageDoc.DocumentElement;
  if AnsiCompareText(n.Attributes['ProcName'], 'IsComponentConfigured') = 0 then begin
    // ����� ���� �������� �������� �� ��, ��� ������������ ������ ������ (��� design-time)
    Output := 1;
  end;

  // ���������������� ���������� ����������� � design-time
  if AnsiCompareText(n.Attributes['ProcName'], 'ConfigureExplainer') = 0 then begin
    // �������� ���� ������, �� �������� ����������� ��������� KBControl:
    // ���������� ������ ���������� ����� � ��������� ����. ���������� ���-������ ��� ����� ��� ���������� ������ � ���������������� ����������
    // ��������� ��� ���������������� ���������� � ���������� Output
    Output := ConfigDoc.DocumentElement.XML;                        
  end;

  // ������� ������������ �����������
  if AnsiCompareText(n.Attributes['ProcName'], 'ComponentConfiguration') = 0 then begin
    Output := ConfigDoc.DocumentElement.XML;
  end;

  // ��������� �����������
  if AnsiCompareText(n.Attributes['ProcName'], 'Run') = 0 then begin
    Run; // ��. ����� Run
  end;
      // ���������� �� ������?
 if MessageDoc.DocumentElement.Attributes['ProcName']='Why' then begin
        obj_n:=  MessageDoc.DocumentElement.ChildNodes[0].text ;
        attr_n:=  MessageDoc.DocumentElement.ChildNodes[1].text ;

    RunWhy(obj_n,attr_n); // �� ����� RunP
  end;

  // �������� ������������ ����������
  if AnsiCompareText(n.Attributes['ProcName'], 'Design') = 0 then begin
    Design();
  end;

  // �������������� ��������� ���������� ���������� �� ��
  if AnsiCompareText(n.Attributes['ProcName'], 'Generate') = 0 then begin
    n := MessageDoc.DocumentElement;
    Generate(n.ChildNodes['KBFileName'].Text, n.ChildNodes['ExplFileName'].Text);
  end;

  // �� ���� � �������� ������� ��� ���
  if AnsiCompareText(n.Attributes['ProcName'], 'GetName') = 0 then begin
    n := MessageDoc.DocumentElement;
    s := '';
    GetAttrName(n.Text, s);
    Output := s;
  end;

end;

procedure TExplainer.Stop;
begin
  // ���������� ������ ����������� (�������� ������� ���)
end;


{---------------------------- private --------------------------------------}
//������� ��������������� ����  ��� ���������� �������������� ��
procedure TExplainer.GenerateDBfile;
  var f, nf: textfile;
  temp, q, r, s, dsfname: string;
  i, x, k: integer;
  config: IXMLDocument;
  node: IXMLNode;
  flag: integer;
begin


  config := TXMLDocument.Create(nil);
  config.LoadFromFile(ExtractFilePath(Application.ExeName)+'config.xml');
    for k:=0 to config.DocumentElement.ChildNodes.Count-1 do begin
      node:= config.DocumentElement.ChildNodes[k];
      if node.Attributes['Name'] = 'Dialoger' then
        dsfname:= node.ChildNodes.nodes['config'].ChildNodes['FileName'].Text;
   end;
  assignfile(f, ExtractFilePath(Application.ExeName)+dsfname);
  Reset(f);
  temp:= '<?xml version="1.0" encoding="utf-8"?><db>';
  while not EOF(f) do
  begin
  readln(f,s);
    If (system.pos('message',s)<>0) and (system.pos('Scripter',s)<>0) and (system.pos('about',s)<>0) then
     begin
      flag:=1;
      x:=system.pos('#������',s);
      delete(s,1,x+6);
      x:=system.pos('#',s);
      r:=copy(s,1,x-1);
      x:=system.pos('.�������',r);
      delete(r,x+1, 7);


        For i:=1 to 5 do  begin
           readln(f,s);
            if (system.pos('module="database"',s)<>0)
            or (system.pos('module="db"',s)<>0) then
           begin
            if flag = 1 then
             begin
             temp:= temp+ '<'+'a'+r+'>';
             flag:=0;
             end;
              while  (system.pos('/func',s)=0) do
              begin
                readln(f,s);
                if system.pos('#',s)<>0 then
                begin
                 x:= system.pos('#������',s);
                 delete(s,1,x+6);
                 x:=system.pos('#',s);
                 q:=copy(s,1,x-1);
                 x:=system.pos('.�������',q);
                 delete(q,x+1, 7);
                 temp:= temp+'<'+'n'+q+'/>';
                end;
              end;
           temp:=temp + '</'+'a'+r+'>' ;
           break;
           end;
        end;
     end;
  end;
  temp:= temp + '</db>';

  AssignFile(nf, TmpFolder + 'dbtemp.xml');
  rewrite(nf);
  Write(nf,temp);
  Closefile(nf);
  CloseFile(f);
end;

procedure TExplainer.RunWhy(obj_n,attr_n: widestring); //������ ������ �� ������?
begin
  WhyForm.main(obj_n, attr_n);
  WhyForm.ShowModal;
end;

procedure TExplainer.Run;
var
  Value, Exists, Count: OleVariant;
  i: integer;
  asked, ThenPart, goal: boolean;
  ap, v, RuleId: WideString;
  pD: PDouble;
  rules: Tlist;
  fInfo: FactInfo; // by Sidorkevich M.
  rInfo: RuleInfo; // by Sidorkevich M.
begin
  MainForm.Reset;

  // �������� ����� �� Blackboard �� 'bb.wm.facts'
  Broker.Blackboard.GetChildCount('bb.wm.facts', 'fact', Count, Exists);
  if (Exists = true) and (Count > 0) then
  for i:=0 to Count-1 do begin
    Broker.Blackboard.GetParamValue('bb.wm.facts.fact['+IntToStr(i)+']', 'AttrPath', Value, Exists);
    ap := Value;
    Broker.Blackboard.GetParamValue('bb.wm.facts.fact['+IntToStr(i)+']', 'Value', Value, Exists);
    if not Exists then v := '' else v := Value;
    Broker.Blackboard.GetParamValue('bb.wm.facts.fact['+IntToStr(i)+']', 'Asked', Value, Exists);
    Asked := Value = 'T';
    Broker.Blackboard.GetParamValue('bb.wm.facts.fact['+IntToStr(i)+']', 'Status', Value, Exists);
    Goal := Value = 'T';
    MainForm.AddFact(ap, v, Asked, Goal);
  end;

  // �������� ������ �� 'bb.wm.rules'
  Broker.Blackboard.GetChildCount('bb.wm.rules', 'rule', Count, Exists);
  if (Exists = true) and (Count > 0) then

  rules:= Tlist.Create;
  for i:=0 to Count-1 do begin
    New(pD);
    Broker.Blackboard.GetParamValue('bb.wm.rules.rule['+IntToStr(i)+']', 'name', Value, Exists);
    RuleId := Value;
    pD^ := Value;
    rules.Add(pD);
    Broker.Blackboard.GetParamValue('bb.wm.rules.rule['+IntToStr(i)+']', 'oznach', Value, Exists);
    ThenPart := Value = '0';
    MainForm.AddRule(RuleId, ThenPart, rules);
  end;


  // by Sidorkevich M.
  Broker.Blackboard.SaveToFile(ExtractFilePath(Application.ExeName) + 'bb.xml');
  MainForm.rfInfo := RulesAndFactsInfo.Create(MainForm.KBControl.KB,
            ExtractFilePath(Application.ExeName) + 'bb.xml', MainForm.RecordedDoc);

  MainForm.g := Graph.Create(MainForm.rfInfo);
  MainForm.g.draw(MainForm.Image1.Canvas, 0, 0);
  // end Sidorkevich M.

  // �������� �����������
  GenerateDBfile;
  MainForm.ExplToMemo;
  MainForm.ShowModal;
end;

procedure TExplainer.Design;
begin
  DesignForm := TDesignForm.Create(nil);
  DesignForm.Visible := false; // ����� ����� ���� ������� ��� ���������
  DesignForm.KBControl := KBControl;
  DesignForm.RulesDoc := MainForm.RecordedDoc;
  DesignForm.ExplFile := ExplFile;
  DesignForm.ShowModal;
  DesignForm.Free;
end;

{---------------------------------------------------------------------------}
procedure TExplainer.Generate(KBFileName, ExplFileName: WideString);
var
  KBC: TKBControl;
  GenDoc: IXMLDocument;
  i: integer;
  r: record_rule;
  n, m: IXMLNode;
  txt: WideString;
begin
  // ��������� ���� � ������, ���� ����
  if System.Pos(':', KBFileName) = 0 then
    KBFileName := ExtractFilePath(Application.ExeName) + KBFileName;
  if System.Pos(':', ExplFileName) = 0 then
    ExplFileName := ExtractFilePath(Application.ExeName) + ExplFileName;

  // �������� ���� ������ � �������� �������� � ������������
  KBC := TKBControl.Create(NIL);
  KBC.KB := TKnowledgeBase.Create(NIL);
  KBC.KB.KBInterface := TKBInterface.Create(nil);
  KBC.KB.FileName := KBFileName;
  KBC.KB.LoadFromFile;

  GenDoc := TXMLDocument.Create(NIL);
  GenDoc.LoadFromXML('<rules/>');
  GenDoc.Encoding := 'utf-8';

  // ������� ��� �������
  for i:=0 to KBC.KB.ListRules.Count-1 do
  begin
    r := record_rule(KBC.KB.ListRules.Items[i]);
    n := GenDoc.DocumentElement.AddChild('rule');
    m := n.AddChild('name');
    m.Text := IntToStr(r.n_rule);
    // ������� �������� ������������ then �����
    m := n.AddChild('thentext');
    txt := '��������� ' + list2txt(KBC, r.list_attr_if) +
           ', �� ' + list2txt(KBC, r.list_attr_then);
    m.Text := txt;
    // ������� �������� ������������ else �����, ���� ��� ����
    if r.list_attr_else.Count > 0 then begin
      m := n.AddChild('elsetext');
      txt := '��� ��� �� ����� ��, ��� ' + list2txt(KBC, r.list_attr_if) +
             ', �� ' + list2txt(KBC, r.list_attr_else);
      m.Text := txt;
    end;
  end;

  GenDoc.SaveToFile(ExplFileName);

  KBC.KB.KBInterface.Free;
  KBC.KB.Free;
  KBC.Free;
end;


function TExplainer.GetAttrName(AttrPath: WideString; var AttrName: WideString): boolean;
var
  n_obj, n_attr: integer;
  ObjName: WideString;
begin
  Result := MainForm.ParseAttrPath(AttrPath, n_obj, ObjName, n_attr, AttrName);
end;

{---------------------------------------------------------------------------}

initialization
  TAutoObjectFactory.Create(ComServer, TExplainer, Class_Explainer,
    ciMultiInstance, tmApartment);
end.
