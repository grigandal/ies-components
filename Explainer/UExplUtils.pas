unit UExplUtils;

interface

uses
  Classes, Aspr95, KBTypes;

function GetAttr(KBC: TKBControl; n_obj, n_attr: integer): record_attr;
function list2txt(KBC: TKBControl; list: TCollection): string;


implementation

function GetAttr(KBC: TKBControl; n_obj, n_attr: integer): record_attr;
var
  j: integer;
  o: record_obj;
  a: record_attr;
begin
  Result := NIL;
  o := NIL;
  j:=0;
  while j<KBC.KB.ListObjs.Count do begin
    o := record_obj(KBC.KB.ListObjs.Items[j]);
    if o.n_obj = n_obj then break;
    Inc(j);
  end;
  if o = NIL then exit;
  a := NIL;
  j:=0;
  while j<o.ListAttrs.Count do begin
    a := record_attr(o.ListAttrs.Items[j]);
    if a.n_attr = n_attr then break;
    Inc(j);
  end;
  if a = NIL then exit;
  Result := a;
end;

function list2txt(KBC: TKBControl; list: TCollection): string;
var
  i: integer;
  a: TAssertion;
  attr: record_attr;
  txt, pr: string;
begin
  txt := '';
  for i:=0 to list.Count-1 do begin
    a := TAssertion(list.Items[i]);
    attr := GetAttr(KBC, a.n_obj, a.n_attr);
    case a.predicat of
      0: pr := '=';
      1: pr := '>';
      2: pr := '<';
      3: pr := '>=';
      4: pr := '<=';
      5: pr := '<>';
    end;
    txt := txt + attr.comment + pr + a.GetZnach + ' ';
  end;
  Result := txt;
end;


end.
 