unit ExplLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 17.01.2007 12:33:10 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Documents and Settings\�������������\������� ����\����� �����\ExplainerAll\ExplLib.tlb (1)
// LIBID: {BEACB127-D49D-45F9-BBF1-22ED9EFAED09}
// LCID: 0
// Helpfile: 
// HelpString: ExplLib Library
// DepndLst: 
//   (1) v2.0 stdole, (D:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
//{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ExplLibMajorVersion = 1;
  ExplLibMinorVersion = 0;

  LIBID_ExplLib: TGUID = '{BEACB127-D49D-45F9-BBF1-22ED9EFAED09}';

  IID_IExplainer: TGUID = '{81CA22D9-455F-4B27-98FB-04C280316B5D}';
  CLASS_Explainer: TGUID = '{26E5265E-204A-4305-A63E-A2C99E2CF6ED}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IExplainer = interface;
  IExplainerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Explainer = IExplainer;


// *********************************************************************//
// Interface: IExplainer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {81CA22D9-455F-4B27-98FB-04C280316B5D}
// *********************************************************************//
  IExplainer = interface(IDispatch)
    ['{81CA22D9-455F-4B27-98FB-04C280316B5D}']
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); safecall;
    procedure Stop; safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IExplainerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {81CA22D9-455F-4B27-98FB-04C280316B5D}
// *********************************************************************//
  IExplainerDisp = dispinterface
    ['{81CA22D9-455F-4B27-98FB-04C280316B5D}']
    property Name: WideString dispid 1;
    property Broker: OleVariant dispid 3;
    procedure Configurate(const Config: WideString); dispid 4;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); dispid 5;
    procedure Stop; dispid 6;
  end;

// *********************************************************************//
// The Class CoExplainer provides a Create and CreateRemote method to          
// create instances of the default interface IExplainer exposed by              
// the CoClass Explainer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoExplainer = class
    class function Create: IExplainer;
    class function CreateRemote(const MachineName: string): IExplainer;
  end;

implementation

uses ComObj;

class function CoExplainer.Create: IExplainer;
begin
  Result := CreateComObject(CLASS_Explainer) as IExplainer;
end;

class function CoExplainer.CreateRemote(const MachineName: string): IExplainer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Explainer) as IExplainer;
end;

end.
