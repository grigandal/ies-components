unit RulesAndFacts;

interface

uses Classes, Aspr95, SysUtils, XMLIntf, XMLDoc, ComCtrls, Variants;

type
  FactInfo = class
    private
      FnumObj: integer;
      FnumAttr: integer;
      Flevel: integer;
      Fvalue: TStrings;
    public
      constructor Create;
      function getNumObj: integer;
      function getNumAttr: integer;
      function getLevel: integer;
      function getValue: TStrings;
      procedure setLevel(l: integer);
      procedure setValue(v: TStrings);
      procedure setNumObj(n: integer);
      procedure setNumAttr(n: integer);
    published
      property numObj: integer read getNumObj write setNumObj;
      property numAttr: integer read getNumAttr write setNumAttr;
      property level: integer read getLevel write setLevel;
      property value: TStrings read getValue write setValue;
  end;

  RuleInfo = class
    private
      FnumRule: integer;
      FifFactList: TList;
      FthenFactList: TList;
      FelseFactList: TList;
      Fvalue: TStrings;
      Foznach: integer;
    public
      constructor Create;
      function isInIf(fact: FactInfo): boolean; overload;
      //function isInIf(fact: String): boolean; overload;
      function isInThen(fact: FactInfo): boolean; overload;
      //function isInThen(fact: String): boolean; overload;
      function isInElse(fact: FactInfo): boolean; overload;
      //function isInElse(fact: String): boolean; overload;
      function getNumRule: integer;
      function getIfFactList: TList;
      function getThenFactList: TList;
      function getElseFactList: TList;
      function getValue: TStrings;
      function getOznach: integer;
      procedure setNumRule(n: integer);
      procedure setIfFactList(factList: TList);
      procedure setThenFactList(factList: TList);
      procedure setElseFactList(factList: TList);
      procedure setValue(value: TStrings);
      procedure setOznach(o: integer);
    published
      property numRule: integer read getNumRule write setNumRule;
      property ifFactList: TList read getIfFactList write setIfFactList;
      property thenFactList: TList read getThenFactList write setThenFactList;
      property elseFactList: TList read getElseFactList write setElseFactList;
      property value: TStrings read getValue write setValue;
      property oznach: integer read getOznach write setOznach;
  end;

  RulesAndFactsInfo = class
    private
      FFacts: TList;
      FRules: TList;
      FmaxLevel: integer;
    public
      constructor Create; overload;
      constructor Create(KB: TKnowledgeBase; bbFileName: String;
                        explDoc: IXMLDocument); overload;
      procedure addRule(r: RuleInfo); overload;
      procedure addRule(n: integer; ifList: TList; thenList: TList;
                        elseList:TList; v: TStrings; o: integer); overload;
      procedure addFact(f: FactInfo); overload;
      procedure addFact(nObj: integer; nAttr: integer; v: TStrings); overload;
      function getFacts: TList;
      function getRules: TList;
      procedure setFacts(f: TList);
      procedure setRules(r: TList);
      function getMaxLevel: integer;
    published
      property facts: TList read getFacts write setFacts;
      property rules: TList read getRules write setRules;
      property maxLevel: integer read getMaxLevel;
  end;

implementation

function findChar(str: String; c: char): integer;
var
  i: integer;
begin
  findChar := 0;
  for i := 1 to length(str) do
  begin
    if str[i] = c then
    begin
      findChar := i;
      break;
    end;
  end;
end;

function findSubStr(str: String; subStr: String): integer;
var
  i, j: integer;
  b: boolean;
begin
  findSubStr := 0;
  for i := 1 to length(str) do
  begin
      b := false;
      for j := 1 to length(subStr) do
      begin
        if str[i + j - 1] <> subStr[j] then
        begin
          b := false;
          break;
        end
        else
          b := true;
      end;
      if b then
      begin
        findSubStr := i;
        break;
      end;
  end;
end;

constructor RulesAndFactsInfo.Create;
begin
  FFacts := TList.Create;
  FRules := TList.Create;
end;

constructor RulesAndFactsInfo.Create(KB: TKnowledgeBase; bbFileName: String;
                                      explDoc: IXMLDocument);
var
  i, j, k, l, m, z: integer;
  Obj: record_obj;
  Attr: record_attr;
  rule: record_rule;
  ifList, thenList, elseList: TList;
  Assertion: TAssertion;
  newLevel: boolean;
  fInfo: FactInfo;
  rInfo: RuleInfo;
  maxLevel: integer;
  cont: boolean;
  n, n1, n2, rulesNode, ruleNode: IXMLNode;
  bbDoc: IXMLDocument;
  numRule, oznach: integer;
  attrPath: String;
  posOfPoint: integer;
  numObj, numAttr: String;
  factValue: TStrings;
  value: String;
  ruleStrings: TStrings;
  pos1, pos2: integer;
  tempStr: String;
  variantValue: OleVariant;
begin
  FFacts := TList.Create;
  FRules := TList.Create;

  bbDoc := TXMLDocument.Create(bbFileName);
  //explDoc := TXMLDocument.Create(ExtractFilePath(Application.ExeName) + 'exp.xml');
  n := bbDoc.DocumentElement.ChildNodes['wm'];
  n1 := n.ChildNodes['rules'];
  for i := 0 to n1.ChildNodes.Count - 1 do
  begin
    n2 := n1.ChildNodes.Nodes[i];
    //RichEdit2.Lines.Add(n2.Attributes['name']);
    numRule := n2.Attributes['name'];
    oznach := n2.Attributes['oznach'];
    for j := 0 to KB.ListRules.Count - 1 do
    begin
      rule := record_rule(KB.ListRules.items[j]);
      if rule.n_rule = numRule then
      begin
        ifList := TList.Create;
        thenList := TList.Create;
        elseList := TList.Create;
        for k := 0 to rule.list_attr_if.Count - 1 do
        begin
          Assertion := TAssertion(rule.list_attr_if.Items[k]);
          for l := 0 to KB.ListObjs.Count - 1 do
          begin
            Obj := record_obj(KB.ListObjs.Items[l]);
            if Assertion.n_obj = Obj.n_obj then
            begin
              for m := 0 to Obj.ListAttrs.Count - 1 do
              begin
                Attr := record_attr(Obj.ListAttrs.Items[m]);
                if Assertion.n_attr = Attr.n_attr then
                begin
                  fInfo := FactInfo.Create;
                  fInfo.numObj := Obj.n_obj;
                  fInfo.numAttr := Attr.n_attr;
                  tempStr := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  z := 1;
                  while z < length(tempStr) do
                  begin
                    fInfo.value.Add(copy(tempStr, z, 10));
                    z := z + 10;
                  end;
                  //fInfo.value := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  ifList.Add(fInfo);
                end;
              end;
            end;
          end;
        end;
        ///////////////
        for k := 0 to rule.list_attr_then.Count - 1 do
        begin
          Assertion := TAssertion(rule.list_attr_then.Items[k]);
          for l := 0 to KB.ListObjs.Count - 1 do
          begin
            Obj := record_obj(KB.ListObjs.Items[l]);
            if Assertion.n_obj = Obj.n_obj then
            begin
              for m := 0 to Obj.ListAttrs.Count - 1 do
              begin
                Attr := record_attr(Obj.ListAttrs.Items[m]);
                if Assertion.n_attr = Attr.n_attr then
                begin
                  fInfo := FactInfo.Create;
                  fInfo.numObj := Obj.n_obj;
                  fInfo.numAttr := Attr.n_attr;
                  tempStr := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  z := 1;
                  while z < length(tempStr) do
                  begin
                    fInfo.value.Add(copy(tempStr, z, 10));
                    z := z + 10;
                  end;
                  //fInfo.value := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  thenList.Add(fInfo);
                  //thenList.Add(Obj.comment + '.' + Attr.comment + '=' + Assertion.znach);
                end;
              end;
            end;
          end;
        end;
        ////////////////
        for k := 0 to rule.list_attr_else.Count - 1 do
        begin
          Assertion := TAssertion(rule.list_attr_else.Items[k]);
          for l := 0 to KB.ListObjs.Count - 1 do
          begin
            Obj := record_obj(KB.ListObjs.Items[l]);
            if Assertion.n_obj = Obj.n_obj then
            begin
              for m := 0 to Obj.ListAttrs.Count - 1 do
              begin
                Attr := record_attr(Obj.ListAttrs.Items[m]);
                if Assertion.n_attr = Attr.n_attr then
                begin
                  fInfo := FactInfo.Create;
                  fInfo.numObj := Obj.n_obj;
                  fInfo.numAttr := Attr.n_attr;
                  tempStr := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  z := 1;
                  while z < length(tempStr) do
                  begin
                    fInfo.value.Add(copy(tempStr, z, 10));
                    z := z + 10;
                  end;
                  //fInfo.value := Obj.comment + '.' + Attr.comment + '=' + Assertion.znach;
                  elseList.Add(fInfo);
                  //elseList.Add(Obj.comment + '.' + Attr.comment + '=' + Assertion.znach);
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    rulesNode := explDoc.DocumentElement;
    ruleStrings := TStringList.Create;
    for j := 0 to rulesNode.ChildNodes.Count - 1 do
    begin
      ruleNode := rulesNode.ChildNodes[j];
      if numRule = StrToInt(ruleNode.ChildNodes[0].Text) then
      begin
        ruleStrings.Clear;
        ruleStrings.Add('������� ' + IntToStr(numRule));
        if oznach = 0 then
          ruleStrings.Add(ruleNode.ChildNodes[1].Text)
        else
          ruleStrings.Add(ruleNode.ChildNodes[2].Text);
      end;
     // memo.Lines.Add('name = ' + IntToStr(ruleNode.Attributes['name']));
      //memo.Lines.Add('thentext = ' + ruleNode.Attributes['thentext']);
      //memo.Lines.Add('elsetext = ' + ruleNode.Attributes['elsetext']);
    end;
    addRule(numRule, ifList, thenList, elseList, ruleStrings,
           oznach);
  end;

  n1 := n.ChildNodes['facts'];
  for i := 0 to n1.ChildNodes.Count - 1 do
  begin
    n2 := n1.ChildNodes.Nodes[i];
    attrPath := n2.Attributes['AttrPath'];
    if  (findSubStr(attrPath, '������') <> 0) and (findSubStr(attrPath, '�������') <> 0) then
    begin
      factValue := TStringList.Create;
      delete(attrPath, 1, 6);
      posOfPoint := findChar(attrPath, '.');
      if posOfPoint = 0 then
        continue;
      numObj := '';
      for j := 1 to posOfPoint - 1 do
      begin
        numObj := numObj + attrPath[j];
      end;
      //RichEdit1.Lines.Add(numObj);
      delete(attrPath, 1, posOfPoint + 7);
      numAttr := '';
      for j := 1 to length(attrPath) do
      begin
        numAttr := numAttr + attrPath[j];
      end;
      //RichEdit1.Lines.Add(numAttr);

      //value := n2.Attributes['Value'];     // bug when null - Demidov
      variantValue := n2.Attributes['Value'];
      if VarIsNull(variantValue) then continue;
      value := variantValue;
      // end demidov
      if value = '' then continue;

      for j := 0 to KB.ListObjs.Count - 1 do
      begin
        Obj := record_obj(KB.ListObjs.Items[j]);
        if Obj.n_obj = StrToInt(numObj) then
        begin
          for k := 0 to Obj.ListAttrs.Count - 1 do
          begin
            Attr := record_attr(Obj.ListAttrs.Items[k]);
            if Attr.n_attr = StrToInt(numAttr) then
            begin
              tempStr := Obj.comment + '.' + Attr.comment + '=' + value;
              z := 1;
              while z < length(tempStr) do
              begin
                factValue.Add(copy(tempStr, z, 20));
                z := z + 20;
              end;
              //factValue := Obj.comment + '.' + Attr.comment + '=' + value;
            end;
          end;
        end;
      end;
      addFact(StrToInt(numObj), StrToInt(numAttr), factValue);
    end
    else
      continue;
  end;

  //��������� ���� level � ������� �����
  for i := 0 to facts.Count - 1 do
  begin
    newLevel := true;
    fInfo := facts.Items[i];
    for j := 0 to rules.Count - 1 do
    begin
      rInfo := rules.Items[j];
      if rInfo.isInThen(fInfo) then
      begin
        newLevel := false;
        break;
      end;
    end;
    if newLevel then
    begin
      fInfo.level := 1;
    end;
  end;
  maxLevel := 1;
  cont := true;
  while cont = true do
  begin
    cont := false;
    for i := 0 to facts.Count - 1 do
    begin
      fInfo := facts.Items[i];
      if fInfo.level = maxLevel then
        for j := 0 to rules.Count - 1 do
        begin
          rInfo := rules.Items[j];
          if rInfo.isInIf(fInfo) then
          begin
            cont := true;
            for k := 0 to facts.Count - 1 do
            begin
              fInfo := facts.Items[k];
              if rInfo.isInThen(fInfo) or rInfo.isInElse(fInfo) then
                fInfo.level := maxLevel + 1;
            end;
          end;
        end;
    end;
    inc(maxLevel);
  end;
  FmaxLevel := maxLevel - 1; 
end;

procedure RulesAndFactsInfo.addRule(r: RuleInfo);
begin
  FRules.Add(r);
end;

procedure RulesAndFactsInfo.addRule(n: integer; ifList: TList;
          thenList: TList; elseList:TList; v: TStrings; o: integer);
var
  r: RuleInfo;
begin
  r := RuleInfo.Create;
  r.numRule := n;
  r.ifFactList := ifList;
  r.thenFactList := thenList;
  r.elseFactList := elseList;
  r.value := v;
  r.oznach := o;
  addRule(r);
end;

procedure RulesAndFactsInfo.addFact(f: FactInfo);
begin
  FFacts.Add(f);
end;

procedure RulesAndFactsInfo.addFact(nObj: integer; nAttr: integer; v: TStrings);
var
  f: FactInfo;
  i: integer;
  add: boolean;
begin
  add := true;
  {for i := 0 to facts.Count - 1 do
  begin
    f := facts.Items[i];
    if v = f.value then
      add := false;
  end;
  if add then
  begin}
    f := FactInfo.Create;
    f.numObj := nObj;
    f.numAttr := nAttr;
    f.value := v;
    addFact(f);
  //end;
end;

function RulesAndFactsInfo.getFacts: TList;
begin
  getFacts := FFacts;
end;

function RulesAndFactsInfo.getRules: TList;
begin
  getRules := FRules;
end;

procedure RulesAndFactsInfo.setFacts(f: TList);
begin
  FFacts := f;
end;

procedure RulesAndFactsInfo.setRules(r: TList);
begin
  FRules := r;
end;

function RulesAndFactsInfo.getMaxLevel: integer;
begin
  getMaxLevel := FmaxLevel;
end;

//////////////////////////////////////

constructor FactInfo.Create;
begin
  FnumObj := 0;
  FnumAttr := 0;
  Flevel := 0;
  Fvalue := TStringList.Create;
end;

function FactInfo.getNumObj: integer;
begin
  getNumObj := FnumObj;
end;

function FactInfo.getNumAttr: integer;
begin
  getNumAttr := FnumAttr;
end;

function FactInfo.getLevel: integer;
begin
  getLevel := FLevel;
end;

function FactInfo.getValue: TStrings;
begin
  getValue := FValue;
end;

procedure FactInfo.setLevel(l: integer);
begin
  FLevel := l;
end;

procedure FactInfo.setValue(v: TStrings);
begin
  FValue := v;
end;

procedure FactInfo.setNumObj(n: integer);
begin
  FnumObj := n;
end;

procedure FactInfo.setNumAttr(n: integer);
begin
  FnumAttr := n;
end;

/////////////////////////////////////////////////

constructor RuleInfo.Create;
begin
  FnumRule := 0;
  FifFactList := TList.Create;
  FthenFactList := TList.Create;
  FelseFactList := TList.Create;
end;

function RuleInfo.isInIf(fact: FactInfo): boolean;
var
  res: boolean;
  i: integer;
  f: FactInfo;
begin
  res := false;
  for i := 0 to FifFactList.Count - 1 do
  begin
    f := FifFactList.Items[i];
    if (f.numObj = fact.numObj) and (f.numAttr = fact.numAttr) then
    begin
      res := true;
      break;
    end;
  end;
  isInIf := res;
end;

{function RuleInfo.isInIf(fact: String): boolean;
var
  res: boolean;
  i: integer;
begin
  res := false;
  for i := 0 to FifFactList.Count - 1 do
  begin
    if FifFactList.Strings[i] = fact then
    begin
      res := true;
      break;
    end;
  end;
  isInIf := res;
end;}

function RuleInfo.isInThen(fact: FactInfo): boolean;
var
  res: boolean;
  i: integer;
  f: FactInfo;
begin
  res := false;
  for i := 0 to FthenFactList.Count - 1 do
  begin
    f := FthenFactList.Items[i];
    if (f.numObj = fact.numObj) and (f.numAttr = fact.numAttr) then
    begin
      res := true;
      break;
    end;
  end;
  isInThen := res;
end;

{function RuleInfo.isInThen(fact: String): boolean;
var
  res: boolean;
  i: integer;
begin
  res := false;
  for i := 0 to FthenFactList.Count - 1 do
  begin
    if FthenFactList.Strings[i] = fact then
    begin
      res := true;
      break;
    end;
  end;
  isInThen := res;
end;   }

function RuleInfo.isInElse(fact: FactInfo): boolean;
var
  res: boolean;
  i: integer;
  f: FactInfo;
begin
  res := false;
  for i := 0 to FelseFactList.Count - 1 do
  begin
    f := FelseFactList.Items[i];
    if (f.numObj = fact.numObj) and (f.numAttr = fact.numAttr) then
    begin
      res := true;
      break;
    end;
  end;
  isInElse := res;
end;

{function RuleInfo.isInElse(fact: String): boolean;
var
  res: boolean;
  i: integer;
begin
  res := false;
  for i := 0 to FelseFactList.Count - 1 do
  begin
    if FelseFactList.Strings[i] = fact then
    begin
      res := true;
      break;
    end;
  end;
  isInElse := res;
end;    }

function RuleInfo.getNumRule: integer;
begin
  getNumRule := FnumRule;
end;

function RuleInfo.getIfFactList: TList;
begin
  getIfFactList := FifFactList;
end;

function RuleInfo.getThenFactList: TList;
begin
  getThenFactList := FthenFactList;
end;

function RuleInfo.getElseFactList: TList;
begin
  getElseFactList := FelseFactList;
end;

function RuleInfo.getValue: TStrings;
begin
  getValue := Fvalue;
end;

function RuleInfo.getOznach: integer;
begin
  getOznach := Foznach;
end;

procedure RuleInfo.setNumRule(n: integer);
begin
  FnumRule := n;
end;

procedure RuleInfo.setIfFactList(factList: TList);
begin
  FifFactList := factList;
end;

procedure RuleInfo.setThenFactList(factList: TList);
begin
  FthenFactList := factList;
end;

procedure RuleInfo.setElseFactList(factList: TList);
begin
  FelseFactList := factList;
end;

procedure RuleInfo.setValue(value: TStrings);
begin
  Fvalue := value;
end;

procedure RuleInfo.setOznach(o: integer);
begin
  Foznach := o;
end;

end.
