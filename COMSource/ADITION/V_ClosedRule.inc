constructor TVerificatorClosedRule.Create(base: TKnowledgeBase);
begin
 ErrorType := 4;
 inherited Create(base);
end;

procedure ShiftCycle(chain: TStringList);
var
 i, min, n: integer;
 s: string;
begin
 if chain.Count < 2 then exit;
 min := StrToInt(chain[0]);
 for i := 1 to chain.Count - 1 do begin
  n := StrToInt(chain[i]);
  if n < min then min := n;
 end;

 repeat
  s := chain[0];
  chain.Delete(0);
  chain.Add(s);
  n := StrToInt(chain[0]);
 until n = min;
end;

procedure TVerificatorClosedRule.FindErrors;
var
 ThenIf: TIntMatrix;
 i, j, t: integer;
 s: string;
 chains, tmp: TStringList;
begin
 inherited FindErrors;

 SetLength(ThenIf, KB.ListRules.Count, KB.ListRules.Count);
 for i := Low(ThenIf) to High(ThenIf) do
  for j := Low(ThenIf[i]) to High(ThenIf[i]) do begin
   if CmpIfThen(record_rule(KB.ListRules.Items[i]), record_rule(KB.ListRules.Items[j])) then
    ThenIf[i, j] := 1
  end;

 for i := Low(ThenIf) to High(ThenIf) do
  for j := i to High(ThenIf[i]) do
   if ThenIf[i, j] = 1 then begin
    if i = j then begin
     ThenIf[i, j] := 2;
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>��������� �������:</B> <A href="R%0:d" n_rule="%0:d">%1:s</A>', [i + 1, record_rule(KB.ListRules.Items[i]).comment]);
     AddError(s);
    end else
    if ThenIf[j, i] = 1 then begin
     ThenIf[i, j] := 2;
     ThenIf[j, i] := 2;
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>��������� ���� ������:</B> <A href="R%0:d" n_rule="%0:d">%1:s</A><B> � </B><A href="R%2:d" n_rule="%2:d">%3:s</A>', [i + 1, record_rule(KB.ListRules.Items[i]).comment, j + 1, record_rule(KB.ListRules.Items[j]).comment]);
     AddError(s);
    end;
   end;

 //ShowMatrix(ThenIf);

 chains := TStringList.Create;
 tmp := TStringList.Create;

 for i := Low(ThenIf) to High(ThenIf) do begin
  chains.Clear;
  s := '';
  GetCycledChains(i, ThenIf, chains, s);

  for j := 0 to chains.Count - 1 do begin
   tmp.CommaText := chains[j];
   ShiftCycle(tmp);
   if tmp.Count > 2 then begin
    s := '<IMG src="idx:0" align="middle"><B>��������� ������� ������:</B> ';
    for t := 0 to tmp.Count - 1 do begin
     s := s + Format('<A href="R%0:d" n_rule="%0:d">R%0:d</A>', [StrToInt(tmp[t])]);
     if t <> tmp.Count - 1 then s := s + '<B>�</B>';
    end;
    AddError(s);
   end;
  end;

 end;

 tmp.Free;
 chains.Free;
end;

procedure TVerificatorClosedRule.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
 s: string;
 i: integer;
begin
 i := 1;
 while true do begin
  s := GetTagValue(Errors[n], 'A', i);
  if s = '' then break;
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '������� ������� ' + '''' + s + '''';
  Inc(i);
 end;
end;

function TVerificatorClosedRule.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 n_rule, i: integer;
 currule: record_rule;
begin
 Result := false;
 s := Errors[n];
 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule', action + 1)) - 1;

 currule := record_rule(KB.ListRules.Items[n_rule]);
 if (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;

  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;

  Result := true;
 end;
end;
