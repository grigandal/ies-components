constructor TVerificatorAttrNotUsed.Create(base: TKnowledgeBase);
begin
 ErrorType := 1;
 inherited Create(base);
end;

procedure TVerificatorAttrNotUsed.FindErrors;
var
 cur_a: record_attr;
 cur_r: record_rule;
 cur_t: record_type;
 cur_l: list;
 cur_v: value;
 i, j, k: integer;
 s: string;
 {usedtypes: TList;
 usedvals: TList;}
begin
 inherited FindErrors;

 {usedtypes := TList.Create;
 GetUsedTypesList(KB, usedtypes);
 usedvals := TList.Create;
 GetUsedValsInRuleIfs(KB, usedvals);

 usedvals.Free;
 usedtypes.Free;}

 for i := 0 to KB.ListRules.Count - 1 do begin
  cur_r := record_rule(KB.ListRules.Items[i]);
  for j := 0 to cur_r.list_attr_if.Count - 1 do begin
   cur_l := list(cur_r.list_attr_if.Items[j]);
   cur_a := record_attr(record_obj(KB.ListObjs.Items[cur_l.n_obj - 1]).ListAttrs.Items[cur_l.n_attr - 1]);
   cur_t := record_type(KB.ListTypes.Items[cur_a.n_type - 1]);
   if cur_t.list_znach.Count = 0 then cur_t.Comm := '1'
   else
    for k := 0 to cur_t.list_znach.Count - 1 do begin
     cur_v := value(cur_t.list_znach.Items[k]);
     if CompareStr(cur_v.znach, cur_l.znach) = 0 then cur_v.Comm := '1';
    end;
  end;
  for j := 0 to cur_r.list_attr_then.Count - 1 do begin
   cur_l := list(cur_r.list_attr_then.Items[j]);
   cur_a := record_attr(record_obj(KB.ListObjs.Items[cur_l.n_obj - 1]).ListAttrs.Items[cur_l.n_attr - 1]);
   cur_t := record_type(KB.ListTypes.Items[cur_a.n_type - 1]);
   if cur_t.list_znach.Count = 0 then cur_t.Comm := '1'
   else
    for k := 0 to cur_t.list_znach.Count - 1 do begin
     cur_v := value(cur_t.list_znach.Items[k]);
     if CompareStr(cur_v.znach, cur_l.znach) = 0 then cur_v.Comm := '1';
    end;
  end;
  for j := 0 to cur_r.list_attr_else.Count - 1 do begin
   cur_l := list(cur_r.list_attr_else.Items[j]);
   cur_a := record_attr(record_obj(KB.ListObjs.Items[cur_l.n_obj - 1]).ListAttrs.Items[cur_l.n_attr - 1]);
   cur_t := record_type(KB.ListTypes.Items[cur_a.n_type - 1]);
   if cur_t.list_znach.Count = 0 then cur_t.Comm := '1'
   else
    for k := 0 to cur_t.list_znach.Count - 1 do begin
     cur_v := value(cur_t.list_znach.Items[k]);
     if CompareStr(cur_v.znach, cur_l.znach) = 0 then cur_v.Comm := '1';
    end;
  end;
 end;

 for i := 0 to KB.ListTypes.Count - 1 do begin
  cur_t := record_type(KB.ListTypes.Items[i]);
  if cur_t.Comm = '1' then cur_t.Comm := ''
  else
   for j := 0 to cur_t.list_znach.Count - 1 do begin
    cur_v := value(cur_t.list_znach.Items[j]);
    if cur_v.Comm = '1' then cur_v.Comm := ''
    else begin
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>�������� ��� ������:</B> <A href="T%0:d:V%1:d" n_type="%0:d" n_value="%1:d">%2:s.%3:s</A>', [i + 1, j + 1, cur_t.comment, cur_v.znach]);
     AddError(s);
    end;
   end;
 end;
end;

procedure TVerificatorAttrNotUsed.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
begin
 tmp := TMenuItem.Create(Menu);
 Menu.Items.Add(tmp);
 tmp.Caption := '������� ��������';

 tmp := TMenuItem.Create(Menu);
 Menu.Items.Add(tmp);
 tmp.Caption := '������� ��� ��������';
end;

function TVerificatorAttrNotUsed.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 n_type, n_value: integer;
 curtype: record_type;
 curvalue: value;
begin
 Result := false;

 s := Errors[n];
 if (action = 0) and (ThisForm.MessageYesNo('������� ��������?','������� ��������') = IDYES) then begin
  n_type := StrToInt(GetTagParam(s, 'A', 'n_type')) - 1;
  n_value := StrToInt(GetTagParam(s, 'A', 'n_value')) - 1;

  curtype := record_type(KB.ListTypes.Items[n_type]);
  curvalue := value(curtype.list_znach.Items[n_value]);
  curvalue.Destroy;
  curtype.k_znach := curtype.list_znach.Count;

  Result := true;
 end else
 if (action = 1) and (ThisForm.MessageYesNo('������� ��� ��������?','������� ��� ��������') = IDYES) then begin
  FixErrors;
  Result := true;
 end;
end;

procedure TVerificatorAttrNotUsed.FixErrors;
var
 s: string;
 n_type, n_value, i: integer;
 curtype: record_type;
 curvalue: value;
 listval: TList;
begin
 listval := TList.Create;

 for i := 0 to Errors.Count - 1 do begin
  s := Errors[i];
  if s[15] <> '0' then continue;
  n_type := StrToInt(GetTagParam(s, 'A', 'n_type')) - 1;
  n_value := StrToInt(GetTagParam(s, 'A', 'n_value')) - 1;

  curtype := record_type(KB.ListTypes.Items[n_type]);
  curvalue := value(curtype.list_znach.Items[n_value]);
  listval.Add(curvalue);
 end;

 for i := 0 to listval.Count - 1 do begin
  curvalue := value(listval.Items[i]);
  curvalue.Destroy;
 end;

 listval.Free;

 for i := 0 to KB.ListTypes.Count - 1 do begin
  curtype := record_type(KB.ListTypes.Items[i]);
  curtype.k_znach := curtype.list_znach.Count;
 end;
end;
