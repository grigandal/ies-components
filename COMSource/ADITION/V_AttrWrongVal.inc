constructor TVerificatorAttrWrongVal.Create(base: TKnowledgeBase);
begin
 ErrorType := 2;
 inherited Create(base);
end;

procedure TVerificatorAttrWrongVal.FindErrors;
var
 cur_a: record_attr;
 cur_o: record_obj;
 cur_r: record_rule;
 cur_t: record_type;
 tmp: TKBOwnedCollection;
 cur_l: list;
 cur_v: value;
 i, j, k, l: integer;
 //minv, maxv, v: extended;
 Wrong: boolean;
 s: string;
begin
 inherited FindErrors;

 tmp := nil;
 for i := 0 to KB.ListRules.Count - 1 do begin
  cur_r := record_rule(KB.ListRules.Items[i]);
  for l := 1 to 3 do begin
   case l of
    1: tmp := cur_r.list_attr_if;
    2: tmp := cur_r.list_attr_then;
    3: tmp := cur_r.list_attr_else;
   end;
   for j := 0 to tmp.Count - 1 do begin
    cur_l := list(tmp.Items[j]);
    cur_o := record_obj(KB.ListObjs.Items[cur_l.n_obj - 1]);
    cur_a := record_attr(cur_o.ListAttrs.Items[cur_l.n_attr - 1]);
    cur_t := record_type(KB.ListTypes.Items[cur_a.n_type - 1]);

    Wrong := true;
    if cur_t.type_znach = 1 then begin
     //try
      //minv := StrToFloat(cur_t.min_znach);
      //maxv := StrToFloat(cur_t.max_znach);
      //v := StrToFloat(cur_l.znach);
      //if (v >= minv) and (v <= maxv) then Wrong := false;
     //except
     //end;
     Wrong := false;
    end else
     for k := 0 to cur_t.list_znach.Count - 1 do begin
      cur_v := value(cur_t.list_znach.Items[k]);
      if CompareStr(cur_v.znach, cur_l.znach) = 0 then Wrong := false;
     end;

    if Wrong then begin
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>������������ ��������</B> <FONT color="clmaroon">%0:s</FONT> <B>��������</B> <A href="R%1:d:O%4:d:A%5:d" n_rule="%1:d" n_type="%3:d" value="%0:s">%2:s</A>', [cur_l.znach, i + 1, cur_a.comment, cur_a.n_type, cur_l.n_obj, cur_l.n_attr]);
     AddError(s);
    end;
   end;
  end;
 end;
end;

procedure TVerificatorAttrWrongVal.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
begin
 tmp := TMenuItem.Create(Menu);
 Menu.Items.Add(tmp);
 tmp.Caption := '������� �������';

 tmp := TMenuItem.Create(Menu);
 Menu.Items.Add(tmp);
 tmp.Caption := '�������� ��������';
end;

function TVerificatorAttrWrongVal.PopupClick(n: integer; action: integer): boolean;
var
 s, s_value: string;
 i, n_rule, n_type: integer;
 currule: record_rule;
 curtype: record_type;
 curvalue: value;
begin
 Result := false;

 s := Errors[n];

 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule')) - 1;
 currule := record_rule(KB.ListRules.Items[n_rule]);
 n_type := StrToInt(GetTagParam(s, 'A', 'n_type')) - 1;
 curtype := record_type(KB.ListTypes.Items[n_type]);
 s_value := GetTagParam(s, 'A', 'value');

 if (action = 0) and (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;
  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;
  Result := true;
 end else
 if (action = 1) then begin
  curvalue := value.Create(curtype.list_znach);
  curvalue.znach := s_value;
  curtype.k_znach := curtype.list_znach.Count;
  Result := true;
 end;
end;
