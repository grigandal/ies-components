unit VerifyPrint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Buttons;

type
  TFormVerifyPrint = class(TForm)
    PanelRight: TPanel;
    RichEditPrint: TRichEdit;
    ButtonPrint: TSpeedButton;
    PrinterSetupDialog: TPrinterSetupDialog;
    ButtonSetup: TSpeedButton;
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonSetupClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  FormVerifyPrint: TFormVerifyPrint;

implementation

{$R *.DFM}

procedure TFormVerifyPrint.ButtonPrintClick(Sender: TObject);
begin
 RichEditPrint.Print('Протокол верификации');
end;

procedure TFormVerifyPrint.ButtonSetupClick(Sender: TObject);
begin
 PrinterSetupDialog.Execute;
end;

end.
