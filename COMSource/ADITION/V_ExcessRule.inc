constructor TVerificatorExcessRule.Create(base: TKnowledgeBase);
begin
 ErrorType := 6;
 inherited Create(base);
end;

procedure TVerificatorExcessRule.FindErrors;
var
 i, j, k, l, t: integer;
 IfIf, IfThen: TIntMatrix;
 s, ch1, ch2: string;
 Chains1, Chains2, tmp: TStringList;
begin
 inherited FindErrors;

 SetLength(IfIf, KB.ListRules.Count, KB.ListRules.Count);
 for i := Low(IfIf) to High(IfIf) do
  for j := Low(IfIf[i]) to High(IfIf[i]) do begin
   IfIf[i, j] := 1;
   if (IfIf[j, i] = 0) then begin

    if CmpIncidents(record_rule(KB.ListRules.Items[i]), record_rule(KB.ListRules.Items[j])) then
     IfIf[i, j] := 2;

    if CmpListsAttr(record_rule(KB.ListRules.Items[i]).list_attr_then, record_rule(KB.ListRules.Items[j]).list_attr_then) then
     IfIf[i, j] := IfIf[i, j] + 2;

    if (IfIf[i, j] = 4) then begin
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>���������� �������:</B> <A href="R%0:d" n_rule="%0:d">%2:s</A><B> � </B><A href="R%1:d" n_rule="%1:d">%3:s</A>', [i + 1, j + 1, record_rule(KB.ListRules.Items[i]).comment, record_rule(KB.ListRules.Items[j]).comment]);
     AddError(s);
    end;

   end else IfIf[i, j] := IfIf[j, i];
  end;

 SetLength(IfThen, KB.ListRules.Count, KB.ListRules.Count);
 for i := Low(IfThen) to High(IfThen) do
  for j := Low(IfThen[i]) to High(IfThen[i]) do begin
   if CmpIfThen(record_rule(KB.ListRules.Items[i]), record_rule(KB.ListRules.Items[j])) then
    IfThen[i, j] := 1
  end;

 //ShowMatrix(IfIf);

 Chains1 := TStringList.Create;
 Chains2 := TStringList.Create;
 tmp := TStringList.Create;

 for i := Low(IfIf) to High(IfIf) do
  for j := i to High(IfIf[i]) do begin
   if IfIf[i, j] = 2 then begin
    s := '';
    Chains1.Clear;
    GetRulesChains(i, IfThen, Chains1, s);
    Chains2.Clear;
    GetRulesChains(j, IfThen, Chains2, s);

    //ShowMessage(Chains1.Text + #13#10 + #13#10 + Chains2.Text);
    for k := 0 to Chains1.Count - 1 do
     for l := 0 to Chains2.Count - 1 do begin
      if FindConflict(Chains1[k], Chains2[l], IfIf, ch1, ch2) then begin
       //ShowMessage(ch1 + ' conflicts with ' + ch2);

       s := '<IMG src="idx:0" align="middle"><B>���������� ������� ������:</B> ';

       tmp.CommaText := ch1;
       for t := 0 to tmp.Count - 1 do begin
        s := s + Format('<A href="R%0:d" n_rule="%0:d">R%0:d</A>', [StrToInt(tmp[t])]);
        if t <> tmp.Count - 1 then s := s + '<B>�</B>';
       end;

       s := s + '<B> � </B>';

       tmp.CommaText := ch2;
       for t := 0 to tmp.Count - 1 do begin
        s := s + Format('<A href="R%0:d" n_rule="%0:d">R%0:d</A>', [StrToInt(tmp[t])]);
        if t <> tmp.Count - 1 then s := s + '<B>�</B>';
       end;

       AddError(s);

      end;
     end;

   end;
  end;

 tmp.Free;
 Chains2.Free;
 Chains1.Free;
end;

procedure TVerificatorExcessRule.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
 s: string;
 i: integer;
begin
 i := 1;
 while true do begin
  s := GetTagValue(Errors[n], 'A', i);
  if s = '' then break;
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '������� ������� ' + '''' + s + '''';
  Inc(i);
 end;
end;

function TVerificatorExcessRule.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 n_rule, i: integer;
 currule: record_rule;
begin
 Result := false;
 s := Errors[n];
 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule', action + 1)) - 1;

 currule := record_rule(KB.ListRules.Items[n_rule]);
 if (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;

  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;

  Result := true;
 end;
end;
