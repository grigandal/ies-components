unit VerifyStart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, VerifyTypes;

type
  TFormVerifyStart = class(TForm)
    GroupBoxVerify: TGroupBox;
    RadioAll: TRadioButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    ImageVerifyStart: TImage;
    procedure CheckBox1Click(Sender: TObject);
    procedure RadioAllClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    VerifyWhat: set of TErrorTypes;
    { Public declarations }
  end;

//var
  //FormVerifyStart: TFormVerifyStart;

implementation

{$R *.DFM}

procedure TFormVerifyStart.CheckBox1Click(Sender: TObject);
var
 i, s: integer;
 c: TComponent;
begin
 s := 0;
 for i := 0 to ComponentCount - 1 do begin
  c := Components[i];
  if (c is TCheckBox) and (c.Tag > 0) then
   if TCheckBox(c).Checked then s := s + TCheckBox(c).Tag;
 end;

 if s = (1 + 2 + 3 + 4 + 5 + 6 + 7) then RadioAll.Checked := true
 else RadioAll.Checked := false;

 if s = 0 then ButtonOk.Enabled := false
 else ButtonOk.Enabled := true;
end;

procedure TFormVerifyStart.RadioAllClick(Sender: TObject);
var
 i: integer;
 c: TComponent;
begin
 for i := 0 to ComponentCount - 1 do begin
  c := Components[i];
  if (c is TCheckBox) and (c.Tag > 0) then TCheckBox(c).Checked := true;
 end;
end;

procedure TFormVerifyStart.ButtonOkClick(Sender: TObject);
var
 i: integer;
 c: TComponent;
begin
 VerifyWhat := [];
 for i := 0 to ComponentCount - 1 do begin
  c := Components[i];
  if (c is TCheckBox) and (c.Tag > 0) then
   if TCheckBox(c).Checked then
    case c.Tag of
     1: VerifyWhat := VerifyWhat + [etAttrNotUsed];
     2: VerifyWhat := VerifyWhat + [etAttrWrongVal];
     3: VerifyWhat := VerifyWhat + [etImpossibleThen];
     4: VerifyWhat := VerifyWhat + [etClosedRule];
     5: VerifyWhat := VerifyWhat + [etOddIf];
     6: VerifyWhat := VerifyWhat + [etExcessRule];
     7: VerifyWhat := VerifyWhat + [etConflictRule];
    end;
 end;
end;

end.
