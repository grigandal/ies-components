constructor TVerificatorOddIf.Create(base: TKnowledgeBase);
begin
 ErrorType := 5;
 inherited Create(base);
end;

function IsEqualOddBranches(b1, b2: TStrings): integer;
var
 i, j, t, n, k: integer;
 bs1, bs2: TStringList;
 s: string;
begin
 bs1 := TStringList.Create;
 bs2 := TStringList.Create;

 n := 0;
 for i := 0 to b1.Count - 1 do begin
  bs1.CommaText := StringReplace(b1[i], ' & ', ',', [rfReplaceAll]);
  for j := 0 to b2.Count - 1 do begin
   bs2.CommaText := StringReplace(b2[j], ' & ', ',', [rfReplaceAll]);
   //ShowMessage(bs1.CommaText + ' - ' + bs2.CommaText);
   if (bs1.Count = 1) and (bs2.Count = 1) then begin
    if (CompareStr('~' + b1[i], b2[j]) = 0) or (CompareStr(b1[i], '~' + b2[j]) = 0) then begin
     n := 1;
    end;
   end
   else if (bs1.Count = 1) and (bs2.Count > 1) then begin
    s := bs1[0];
    if (s[1] = '~') then Delete(s, 1, 1)
    else s := '~' + s;
    if bs2.IndexOf(s) <> -1 then n := 2
   end
   else if (bs1.Count > 1) and (bs2.Count = 1) then begin
    s := bs2[0];
    if (s[1] = '~') then Delete(s, 1, 1)
    else s := '~' + s;
    if bs1.IndexOf(s) <> -1 then n := 2
   end
   else if (bs1.Count = bs2.Count) then begin
    //ShowMessage(bs1.CommaText + ' - ' + bs2.CommaText);
    k := 0;
    for t := 0 to bs1.Count - 1 do begin
     if bs2.IndexOf(bs1[t]) = -1 then begin
      inc(k);
      s := bs1[t];
     end;
     if k > 1 then break;
    end;
    //ShowMessage(IntToStr(k));
    if k = 1 then n := 3;
   end;

   if n <> 0 then break;
  end;
  if n <> 0 then break;
 end;

 Result := n;
 bs1.Free;
 bs2.Free;
end;

function CmpIncidentsOdd(r1, r2: record_rule): integer;
var
 b1, b2: TStringList;
 inc1, inc2: string;
 not1, not2: boolean;
 i, j, k: integer;
begin
 b1 := TStringList.Create;
 b2 := TStringList.Create;

 inc1 := r1.incident;
 RemoveBrackets(inc1);
 inc1 := ApplyDeMorgan(inc1, false);
 inc2 := r2.incident;
 RemoveBrackets(inc2);
 inc2 := ApplyDeMorgan(inc2, false);

 k := 0; not1 := false; not2 := false;
 for i := 0 to r1.list_attr_if.Count - 1 do begin
  //if (System.Pos('~P' + IntToStr(i + 1), inc1) <> 0) then not1 := true else not1 := false;
  for j := 0 to r2.list_attr_if.Count - 1 do begin
   //if (System.Pos('~P' + IntToStr(j + 1), inc2) <> 0) then not2 := true else not2 := false;
   if CmpAttr(list(r1.list_attr_if.Items[i]), list(r2.list_attr_if.Items[j]), not1, not2) then begin
    inc(k);
    inc1 := StringReplace(inc1, 'P' + IntToStr(i + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
    inc2 := StringReplace(inc2, 'P' + IntToStr(j + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
   end;
  end;
 end;

 for i := 0 to r1.list_attr_if.Count - 1 do begin
  inc(k);
  inc1 := StringReplace(inc1, 'P' + IntToStr(i + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
 end;

 for j := 0 to r2.list_attr_if.Count - 1 do begin
  inc(k);
  inc2 := StringReplace(inc2, 'P' + IntToStr(j + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
 end;

 //inc1 := StringReplace(inc1, '~', '', [rfReplaceAll]);
 //inc2 := StringReplace(inc2, '~', '', [rfReplaceAll]);

 GetDisForm(inc1, b1);
 GetDisForm(inc2, b2);

 Result := IsEqualOddBranches(b1, b2);

 b2.Free;
 b1.Free;
end;

procedure TVerificatorOddIf.FindErrors;
var
 i, j, t: integer;
 cur_r1, cur_r2: record_rule;
 s: string;
begin
 inherited FindErrors;

 for i := 0 to KB.ListRules.Count - 1 do begin
  cur_r1 := record_rule(KB.ListRules.Items[i]);
  for j := i to KB.ListRules.Count - 1 do if (i <> j) then begin
   cur_r2 := record_rule(KB.ListRules.Items[j]);
   if CmpListsAttr(cur_r1.list_attr_then, cur_r2.list_attr_then) then begin
    t := CmpIncidentsOdd(cur_r1, cur_r2);
    case t of
     1: begin
      FmtStr(s, '<IMG src="idx:0" align="middle"><B>������� � ������ IF-��������:</B> <A href="R%0:d" n_rule="%0:d" n_case="1">%2:s</A><B> � </B><A href="R%1:d" n_rule="%1:d">%3:s</A>', [i + 1, j + 1, cur_r1.comment, cur_r2.comment]);
      AddError(s);
     end;
     2: begin
      FmtStr(s, '<IMG src="idx:0" align="middle"><B>������� � ������ IF-��������:</B> <A href="R%0:d" n_rule="%0:d" n_case="2">%2:s</A><B> � </B><A href="R%1:d" n_rule="%1:d">%3:s</A>', [i + 1, j + 1, cur_r1.comment, cur_r2.comment]);
      AddError(s);
     end;
     3: begin
      FmtStr(s, '<IMG src="idx:0" align="middle"><B>������� � ������ IF-��������:</B> <A href="R%0:d" n_rule="%0:d" n_case="3">%2:s</A><B> � </B><A href="R%1:d" n_rule="%1:d">%3:s</A>', [i + 1, j + 1, cur_r1.comment, cur_r2.comment]);
      AddError(s);
     end;
    end;
   end;
  end;
 end;

end;

procedure TVerificatorOddIf.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
 s: string;
 i, n_case: integer;
begin
 i := 1;
 while true do begin
  s := GetTagValue(Errors[n], 'A', i);
  if s = '' then break;
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '������� ������� ' + '''' + s + '''';
  Inc(i);
 end;

 s := Errors[n];
 n_case := StrToInt(GetTagParam(s, 'A', 'n_case'));

 if (n_case = 2) or (n_case = 3) then begin
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '-';
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '�������������� ���������';
 end;
end;

function TVerificatorOddIf.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 n_rule, n_case, i: integer;
 currule: record_rule;
begin
 Result := false;
 s := Errors[n];
 n_case := StrToInt(GetTagParam(s, 'A', 'n_case'));
 if (n_case <> 1) then exit;
 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule', action + 1)) - 1;

 currule := record_rule(KB.ListRules.Items[n_rule]);
 if (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;

  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;

  Result := true;
 end;
end;
