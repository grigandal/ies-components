constructor TVerificatorImpossibleThen.Create(base: TKnowledgeBase);
begin
 ErrorType := 3;
 inherited Create(base);
end;

procedure TVerificatorImpossibleThen.FindErrors;
var
 cur_r_then, cur_r_if: record_rule;
 list_then, list_if: list;
 i, j, ii, jj: integer;
 imp: boolean;
 s: string;
begin
 inherited FindErrors;

 for i := 0 to KB.ListRules.Count - 1 do begin
  cur_r_then := record_rule(KB.ListRules.Items[i]);
  for j := 0 to cur_r_then.list_attr_then.Count - 1 do begin
   list_then := list(cur_r_then.list_attr_then.Items[j]);
   imp := true;
   for ii := 0 to KB.ListRules.Count - 1 do begin
    cur_r_if := record_rule(KB.ListRules.Items[ii]);
    for jj := 0 to cur_r_if.list_attr_if.Count - 1 do begin
     list_if := list(cur_r_if.list_attr_if.Items[jj]);
     if CmpAttr(list_then, list_if) then begin
      imp := false;
      break;
     end;
    end;
    if not imp then break;
   end;
   if imp then begin
    FmtStr(s, '<IMG src="idx:0" align="middle"><B>��������������� ���������� � �������</B> <A href="R%1:d:O%2:d:A%3:d" n_rule="%1:d">%0:s</A>', [cur_r_then.comment, i + 1, list_then.n_obj, list_then.n_attr]);
    AddError(s);
   end;
  end;
 end;
end;

procedure TVerificatorImpossibleThen.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
begin
 tmp := TMenuItem.Create(Menu);
 Menu.Items.Add(tmp);
 tmp.Caption := '������� �������';
end;

function TVerificatorImpossibleThen.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 i, n_rule: integer;
 currule: record_rule;
begin
 Result := false;

 s := Errors[n];
 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule')) - 1;
 currule := record_rule(KB.ListRules.Items[n_rule]);

 if (action = 0) and (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;
  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;
  Result := true;
 end;
end;
