unit VerifyUtils;

interface

uses SysUtils, Classes, VerifyTypes;

function RemoveTag(var s: string; tag: string): boolean;
function GetTagValue(s: string; tag: string; vn: integer = 1): string;
function GetTagValuePos(s: string; tag: string; var pos: integer; var len: integer; vn: integer = 1): boolean;
function GetTagParam(s: string; tag: string; param: string; pn: integer = 1): string;
function GetArgsNum(s: string; var op: char): integer;
procedure RemoveBrackets(var s: string);
function GetArg(s: string; num: integer): string;
function FindBranches(s: string; slist: TStrings): string;
procedure GetDisForm(s: string; slist: TStrings);
function IsEqualForms(b1, b2: TStrings): boolean;
function ApplyDeMorgan(s: string; isnot: boolean): string;
procedure GetCycledChains(n: integer; ThenIf: TIntMatrix; chains: TStringList; chain: string);
procedure GetRulesChains(n: integer; ThenIf: TIntMatrix; chains: TStringList; chain: string);
function FindConflict(chain1, chain2: string; ThenThen: TIntMatrix; var ch1: string; var ch2: string): boolean;

implementation

function RemoveTag(var s: string; tag: string): boolean;
var
 p1, p2: integer;
 tmp: string;
begin
 Result := false;

 p1 := System.Pos('<' + tag, s);
 if p1 = 0 then exit;
 tmp := Copy(s, p1, Length(s) - p1 + 1);
 p2 := System.Pos('>', tmp);
 if (p2 = 0) then exit;
 Delete(s, p1, p2);

 p1 := System.Pos('<' + '/' + tag, s);
 if p1 = 0 then exit;
 tmp := Copy(s, p1, Length(s) - p1 + 1);
 p2 := System.Pos('>', tmp);
 if (p2 = 0) then exit;
 Delete(s, p1, p2);

 RemoveTag(s, tag);
 Result := True;
end;

function GetTagValue(s: string; tag: string; vn: integer = 1): string;
var
 p1, p2, v1, v2: integer;
 tmp: string;
begin
 Result := '';

 tmp := s;

 while vn > 0 do begin
  p1 := System.Pos('<' + tag, tmp);
  if p1 = 0 then exit;
  tmp := Copy(tmp, p1 + 1, Length(tmp) - p1);
  Dec(vn);
 end;

 p2 := System.Pos('>', tmp);
 if (p2 = 0) then exit;
 Inc(p2);
 v1 := p2;

 v2 := System.Pos('<' + '/' + tag, tmp);
 if v2 = 0 then exit;

 Result := Copy(tmp, v1, v2 - p2);
end;

function GetTagValuePos(s: string; tag: string; var pos: integer; var len: integer; vn: integer = 1): boolean;
var
 p1, p2, v1, v2: integer;
 tmp: string;
begin
 Result := false;
 pos := 0;
 len := 0;

 tmp := s;

 while vn > 0 do begin
  p1 := System.Pos('<' + tag, tmp);
  if p1 = 0 then exit;
  tmp := Copy(tmp, p1 + 1, Length(tmp) - p1);
  pos := pos + p1;
  Dec(vn);
 end;

 p2 := System.Pos('>', tmp);
 if (p2 = 0) then exit;
 Inc(p2);
 v1 := p2;

 v2 := System.Pos('<' + '/' + tag, tmp);
 if v2 = 0 then exit;

 Result := true;
 pos := pos + v1;
 len := v2 - p2;
end;

function GetTagParam(s: string; tag: string; param: string; pn: integer = 1): string;
var
 p1, p2: integer;
 tmp: string;
begin
 Result := '';

 tmp := s;

 while pn > 0 do begin
  p1 := System.Pos('<' + tag, tmp);
  if p1 = 0 then exit;
  tmp := Copy(tmp, p1 + 1, Length(tmp) - p1);
  Dec(pn);
 end;

 p2 := System.Pos('>', tmp);
 if (p2 = 0) then exit;
 tmp := Copy(tmp, 1, p2);

 p1 := System.Pos(param + '="', tmp);
 if p1 = 0 then exit;
 p1 := p1 + 2 + Length(param);
 tmp := Copy(tmp, p1, Length(tmp) - p1 + 1);
 p2 := System.Pos('"', tmp);
 if (p2 = 0) then exit;

 Result := Copy(tmp, 1, p2 - 1);
end;

function GetArgsNum(s: string; var op: char): integer;
var
 i,n,b: integer;
begin
 n := 0;
 b := 0;
 for i := 1 to Length(s) do begin
  if s[i] = '(' then inc(b)
  else if s[i] = ')' then dec(b)
  else if (((s[i] = '&') or (s[i] = '|') or (s[i] = '~')) and (b = 0)) then begin
   if (s[i] <> '~') then inc(n);
   op := s[i];
  end;
 end;
 Result := n + 1;
end;

procedure RemoveBrackets(var s: string);
var
 i: integer;
 n: integer;
begin
 s := Trim(s);
 if Length(s) < 1 then exit;
 if (s[1] = '(') and (s[Length(s)] = ')') then begin
  n := 0;
  for i := 1 to Length(s) do begin
   if s[i] = '(' then inc(n) else if s[i] = ')' then dec(n);
   if (n = 0) and (i <> Length(s)) then exit;
  end;
  if n = 0 then begin
   Delete(s,1,1);
   Delete(s,Length(s),1);
   Trim(s);
  end;
 end;
end;

function GetArg(s: string; num: integer): string;
var
 i,n,b,p1,p2: integer;
 arg: string;
begin
 n := 0;
 b := 0;
 arg := '';
 p1 := 1;
 p2 := 0;
 for i := 1 to Length(s) do begin
  if s[i] = '(' then inc(b)
  else if s[i] = ')' then dec(b)
  else if (((s[i] = '&') or (s[i] = '|')) and (b = 0)) then begin
   inc(n);
   if n = num - 1 then p1 := i + 1
   else if n = num then p2 := i - 1;
  end else if ((s[i] = '~') and (b = 0)) then begin
   Result := GetArg(Copy(s, i + 1, Length(s) - i), 1);
   exit;
  end;
 end;

 if p2 < p1 then p2 := Length(s);
 arg := Copy(s,p1,p2 - p1 + 1);
 RemoveBrackets(arg);
 Trim(arg);

 Result := arg;
end;

function ApplyDeMorgan(s: string; isnot: boolean): string;
var
 op: char;
 i, n: integer;
 tmp, opp: string;
begin
 Result := '';
 n := GetArgsNum(s, op);

 if op = '~' then begin
  for i := 1 to n do begin
   tmp := ApplyDeMorgan(GetArg(s, i), not isnot);
  end;
  Result := tmp;
 end else begin
  if n > 1 then begin
   if (isnot) then begin
    if (op = '&') then opp := ' | '
    else if (op = '|') then opp := ' & ';
   end else opp := ' ' + op + ' ';
   for i := 1 to n do begin
    tmp := ApplyDeMorgan(GetArg(s, i), isnot);
    if i = n then opp := '';
    Result := Result + '(' + tmp + ')' + opp;
   end
  end else begin
   if isnot then Result := '~' + GetArg(s, 1) + ''
   else Result := GetArg(s, 1);
  end;
 end;

end;

function FindBranches(s: string; slist: TStrings): string;
var
 op: char;
 i, n: integer;
begin
 Result := '';
 n := GetArgsNum(s, op);

 if op = '|' then begin
  for i := 1 to n do
   slist.Add(GetArg(s, i));
  Result := s;
 end else begin
  if n > 1 then
   for i := 1 to n do begin
    Result := FindBranches(GetArg(s, i), slist);
    if Result <> '' then exit;
   end;
 end;
end;

procedure GetDisForm(s: string; slist: TStrings);
var
 where, t: string;
 what, tmp: TStringList;
 i, n: integer;
begin
 tmp := TStringList.Create;
 what := TStringList.Create;

 RemoveBrackets(s);
 //s := ApplyDeMorgan(s, false);
 //slist.Add(s);
 tmp.Add(s);

 while tmp.Count > 0 do begin
  what.Clear;
  where := FindBranches(tmp[0], what);
  if where <> '' then begin
   n := System.Pos(where,tmp[0]);
   for i := 0 to what.Count - 1 do begin
    t := tmp[0];
    Delete(t, n, Length(where));
    Insert(what[i], t, n);
    tmp.Add(t);
   end;
  end else begin
   t := tmp[0];
   t := StringReplace(t, '(', '', [rfReplaceAll]);
   t := StringReplace(t, ')', '', [rfReplaceAll]);
   t := StringReplace(t, '&', ',', [rfReplaceAll]);
   what.CommaText := t;
   what.Sort;
   t := what.CommaText;
   t := StringReplace(t, ',', ' & ', [rfReplaceAll]);
   slist.Add(t);
  end;
  tmp.Delete(0);
 end;

 what.Free;
 tmp.Free; 
end;

function IsEqualForms(b1, b2: TStrings): boolean;
var
 i, j: integer;
begin
 Result := false;

 for i := 0 to b1.Count - 1 do
  for j := 0 to b2.Count - 1 do
   if CompareStr(b1[i], b2[j]) = 0 then begin
    Result := true;
    exit;
   end;
end;

procedure GetCycledChains(n: integer; ThenIf: TIntMatrix; chains: TStringList; chain: string);
var
 i, p: integer;
 flag: boolean;
begin
 flag := false;
 chain := chain + IntToStr(n + 1);

 for i := Low(ThenIf) to High(ThenIf) do
  if ThenIf[n, i] >= 1 then begin
   if chain[Length(chain)] <> ',' then chain := chain + ',';
   p := System.Pos(',' + IntToStr(i + 1) + ',', ',' + chain);
   if (p = 0) then begin
    //flag := true;
    GetCycledChains(i, ThenIf, chains, chain);
   end else
   if (p = 1) then flag := true;
  end;

 if flag then begin
  if chain[Length(chain)] = ',' then Delete(chain, Length(chain), 1);
  if chains.IndexOf(chain) = -1 then chains.Add(chain);
 end;
end;

procedure GetRulesChains(n: integer; ThenIf: TIntMatrix; chains: TStringList; chain: string);
var
 i: integer;
 flag: boolean;
begin
 flag := false;
 chain := chain + IntToStr(n + 1);

 for i := Low(ThenIf) to High(ThenIf) do
  if ThenIf[n, i] = 1 then begin
   if chain[Length(chain)] <> ',' then chain := chain + ',';
   if (System.Pos(',' + IntToStr(i + 1) + ',', ',' + chain) = 0) then begin
    flag := true;
    GetRulesChains(i, ThenIf, chains, chain);
   end;
  end;

 if not flag then begin
  if chain[Length(chain)] = ',' then Delete(chain, Length(chain), 1);
  if chains.IndexOf(chain) = -1 then chains.Add(chain);
 end;
end;

function FindConflict(chain1, chain2: string; ThenThen: TIntMatrix; var ch1: string; var ch2: string): boolean;
var
 lst1, lst2: TStringList;
 i, j: integer;
begin
 Result := false;

 lst1 := TStringList.Create;
 lst2 := TStringList.Create;

 lst1.CommaText := chain1;
 lst2.CommaText := chain2;

 ch1 := '';
 for i := 0 to lst1.Count - 1 do begin
  if ch1 <> '' then ch1 := ch1 + ',';
  ch1 := ch1 + lst1[i];
  ch2 := '';
  for j := 0 to lst2.Count - 1 do begin
   if ch2 <> '' then ch2 := ch2 + ',';
   ch2 := ch2 + lst2[j];

   if ThenThen[StrToInt(lst1[i]) - 1, StrToInt(lst2[j]) - 1] > 2 then begin
    Result := true;
    break;
   end;
  end;
  if Result = true then break;
 end;

 lst1.Free;
 lst2.Free;
end;

end.
