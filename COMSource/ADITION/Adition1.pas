unit Adition1;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, StdCtrls, Paths, KBTypes,
  Clipbrd, DsgnIntf, Aspr95, ComCtrls, CommCtrl, ExtCtrls, Wizards;
  
  {$R Adition1.res}

const
  ProgName='ADITION.EXE';
  _BUF_STREAM=16000;
  Q           = 6;

  mk:         PChar=(#126#126#0);
  Succ='������������ ��������';

type
  TKBTestForm = class(TForm)
    MainMenu1: TMainMenu;
    N10: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ListBox1: TListBox;
    N20: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Numeric: TMenuItem;
    FindNumeric: TMenuItem;
    N4: TMenuItem;
    TreeView1: TTreeView;
    Splitter1: TSplitter;
    PopupMenu1: TPopupMenu;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N11: TMenuItem;
    procedure N18Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure NumericClick(Sender: TObject);
    procedure FindNumericClick(Sender: TObject);
    procedure ClearTree;
    procedure ShowTestPlan;
    procedure FormShow(Sender: TObject);
    procedure N8Click(Sender: TObject);
  private
    { Private declarations }
  public
    VerifyWizard: TWizardComponent;
  end;


  TAdition=class(TKBTool)
    private
    { Private declarations }
    protected
    { Protected declarations }
      FVerifyWizard: TWizardComponent;
    public
      function Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject; override;
    published
    property VerifyWizard: TWizardComponent read FVerifyWizard write FVerifyWizard;
  end;

  TAdd=class(TKBTool)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
    function Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject; override;
    published
  end;

{  TAditionEditor = class(TComponentEditor)
    protected

    public
      procedure Edit; override;
  end;

  TAddEditor = class(TComponentEditor)
    protected

    public
      procedure Edit; override;
  end;
}
function CompareRules(r1: record_rule; r2: record_rule; Mode: Integer): String; export;
procedure Optimize3(KB_: TKnowledgeBase); far; Export;

procedure Register;

implementation

uses Reper, AddFrag, ContrKB;

var
  KBTestForm: TKBTestForm;
  Det: TKBControl;
  KB_: TKnowledgeBase;

{function SetKB: TKnowledgeBase; far; external Cons95;}

{$R *.DFM}

procedure Register;
begin
  RegisterComponents('KB Access', [TAdition,TAdd]);
  KBTypes._RegistryMethod(TAdition,@TAdition.Run,
  'Run',[nil],nil);
  KBTypes._RegistryMethod(TAdd,@TAdd.Run,
  'Run',[TypeInfo(_TVariants)],TypeInfo(Variant));
//  RegisterComponentEditor(TAdition, TAditionEditor);
//  RegisterComponentEditor(TAdd, TAddEditor);

end;

{G+}
function TAdition.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
  Verify(NIL);
  if IsVerified then begin
    if not Assigned(Sender) then begin
      SetStage(etAnalize);
      StartTestKF(KBControl);
    end else if Sender=Self then begin
      SetStage(etDesign);
      StartTestKB(KBControl);
    end else begin
      Det:=KBControl;
      KB_:=Det.KB;

      Application.CreateForm(TKBTestForm, KBTestForm);
      KBTestForm.VerifyWizard:=VerifyWizard;
      KBTestForm.ShowModal;
      KBTestForm.Free;
    end;
    RestStage;
  end;
end;

function TAdd.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
  Verify(NIL);
  if IsVerified then begin
    SetStage(etAnalize);
    AddFrag.StartAdd(KBControl);
    RestStage;
  end;
end;

function CompareRules(r1: record_rule; r2: record_rule; Mode: Integer): String;
begin
  CompareRules:='';
  if (r1=NIL) or (r2=NIL) then begin
    CompareRules:='��������� ������: ���������� ������!';
    Exit;
  end;
  if r1.n_rule=r2.n_rule then
    CompareRules:=
    '������� � ���������� ������� R'+IntToStr(r1.n_rule) else
  Case Mode of
    1: begin
      if (r1.k_znach_if=r2.k_znach_if) and
       (r1.k_znach_then=r2.k_znach_then) and
       (r1.k_znach_else=r2.k_znach_else) and
       (StrComp(r1.incident,r2.incident)=0) and
       (StrComp(r1.consequent,r2.consequent)=0) and
       (StrComp(r1.consequent_else,r2.consequent_else)=0) and
       CompareListsAttr(r1.list_attr_if,r2.list_attr_if) and
       (not (CompareListsAttr(r1.list_attr_then,r2.list_attr_then)) or
        not (CompareListsAttr(r1.list_attr_else,r2.list_attr_else))) then
      CompareRules:=
      '������� R'+IntToStr(r1.n_rule)+'� R'+IntToStr(r2.n_rule)+' �����������';
      end;
    2: begin
      if (r1.k_znach_if=r2.k_znach_if) and
       (r1.k_znach_then=r2.k_znach_then) and
       (r1.k_znach_else=r2.k_znach_else) and
       (StrComp(r1.incident,r2.incident)=0) and
       (StrComp(r1.consequent,r2.consequent)=0) and
       (StrComp(r1.consequent_else,r2.consequent_else)=0) and
       CompareListsAttr(r1.list_attr_if,r2.list_attr_if) and
       CompareListsAttr(r1.list_attr_then,r2.list_attr_then) and
       CompareListsAttr(r1.list_attr_else,r2.list_attr_else) then
      CompareRules:=
      '������� R'+IntToStr(r1.n_rule)+' � R'+IntToStr(r2.n_rule)+' ���������';
      end;
   end;
end;

function CompareConns(l1: record_conn; l2: record_conn; Mode: Integer): String;
begin
  CompareConns:='';
  if l1.n_conn=l2.n_conn then
    CompareConns:=
    '����� � ���������� ������� L'+IntToStr(l1.n_conn) else
  Case Mode of
    1: begin
      if (l1.k_znach=l2.k_znach) and
       ((l1.n_source_obj<>l2.n_source_obj) or
        (l2.n_sink_obj<>l2.n_sink_obj)) and
       CompareListsAttr(l1.list_conn,l2.list_conn) then
      CompareConns:=
      '����� L'+IntToStr(l1.n_conn)+'� L'+IntToStr(l2.n_conn)+' �����������';
      end;
    2: begin
     if (l1.k_znach=l2.k_znach) and
       ((l1.n_source_obj=l2.n_source_obj) or
        (l2.n_sink_obj=l2.n_sink_obj)) and
       CompareListsAttr(l1.list_conn,l2.list_conn) then
     CompareConns:=
      '����� L'+IntToStr(l1.n_conn)+' � L'+IntToStr(l2.n_conn)+' ���������';
      end;
   end;
end;

function Crossing: Boolean;
var
  Find: Boolean;
  Res1,Res2: Boolean;
  I,J: Integer;
  cur_r,find_r: record_rule;
  cur_c,find_c: record_conn;

begin
  Crossing:=True;
  for I:=1 to KB_.ListRules.Count-1 do begin
    cur_r:=record_rule(KB_.ListRules.Items[I-1]);
    for J:=I+1 to KB_.ListRules.Count do begin
      find_r:=record_rule(KB_.ListRules.Items[J-1]);
      if find_r.n_rule<>cur_r.n_rule then begin
        Res1:=False;
        Res2:=False;
        if CompareCrossing(cur_r.list_attr_if,find_r.list_attr_if,1,1) and
           Not CompareListsAttr(cur_r.list_attr_if,find_r.list_attr_if)
           then Res1:=True;
        if Res1 then
          with KBTestForm.ListBox1 do begin
            Items.Add('������� R'+IntToStr(cur_r.n_rule)+
                      ' ������������ � �������� R'+ IntToStr(find_r.n_rule));
            Crossing:=False;
          end else
        if Not Res1 and Res2 then
          with KBTestForm.ListBox1 do begin
            Items.Add('������� R'+IntToStr(cur_r.n_rule)+
                      ' ������������ � �������� R'+ IntToStr(find_r.n_rule));
            Crossing:=False;
          end;
      end;
    end;
  end;

  for I:=1 to KB_.ListConns.Count-1 do begin
    cur_c:=record_conn(KB_.ListConns.Items[I-1]);
    for J:=I+1 to KB_.ListConns.Count do begin
      find_c:=record_conn(KB_.ListConns.Items[J-1]);
      if find_c.n_conn<>cur_c.n_conn then begin
        Res1:=False;
        Res2:=False;
        if CompareCrossing(cur_c.list_conn,find_c.list_conn,1,1) and
           Not CompareListsAttr(cur_c.list_conn,find_c.list_conn)
           then Res1:=True;
        if Res1 then
          with KBTestForm.ListBox1 do begin
            Items.Add('����� L'+IntToStr(cur_c.n_conn)+
                      ' ������������ �� ������ L'+ IntToStr(find_c.n_conn));
            Crossing:=False;
          end
      end;
    end;
  end;

end;

procedure TKBTestForm.N18Click(Sender: TObject);
begin
  Optimize1(KB_);
end;

procedure ShowMutants;
begin
{
  with KBTestForm.ListBox1 do begin
    Visible:=True;
    Left:=0;
    Top:=0;
    Height:=KBTestForm.Height-46;
    Width:=KBTestForm.Width-8;
  end;}
end;

procedure TKBTestForm.FormResize(Sender: TObject);
begin
//  if ListBox1.Visible then ShowMutants
end;

{ Conflicts }
procedure TKBTestForm.N13Click(Sender: TObject);
var
  Success: Boolean;
  ptr1,ptr2: record_rule;
  ptr1_,ptr2_: record_conn;
  Res: String;
  I,J: Integer;
begin
  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    Success:=True;
    for I:=1 to KB_.ListRules.Count-1 do begin
      ptr1:=record_rule(KB_.ListRules.Items[I-1]);
      for J:=I+1 to KB_.ListRules.Count do begin
        ptr2:=record_rule(KB_.ListRules.Items[J-1]);
        if I<>J then begin
          Res:=CompareRules(ptr1,ptr2,1);
          if Res<>'' then begin
           Items.Add(Res);
           Success:=False;
          end;
        end;
      end;
    end;
    if not Success then Items.Add('');

    for I:=1 to KB_.ListConns.Count-1 do begin
      ptr1_:=record_conn(KB_.ListConns.Items[I-1]);
      for J:=I+1 to KB_.ListConns.Count do begin
        ptr2_:=record_conn(KB_.ListConns.Items[J-1]);
        if I<>J then begin
          Res:=CompareConns(ptr1_,ptr2_,1);
          if Res<>'' then begin
           Items.Add(Res);
           Success:=False;
          end;
        end;
      end;
    end;


    if Success then Items.Add(Succ);
  end;
  ShowMutants;
end;

{ Abundance }
procedure TKBTestForm.N14Click(Sender: TObject);
var
  Success: Boolean;
  ptr1,ptr2: record_rule;
  ptr1_,ptr2_: record_conn;
  ptr3,ptr4: record_type;
  Res: String;
  I,J: Integer;
begin
  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    Success:=True;

    for I:=1 to KB_.ListTypes.Count-1 do begin
      ptr3:=record_type(KB_.ListTypes.Items[I-1]);
      for J:=I+1 to KB_.ListTypes.Count do begin
        ptr4:=record_type(KB_.ListTypes.Items[J-1]);
        if I<>J then begin
          Res:=CompareTypes(ptr3,ptr4);
          if Res<>'' then begin
            Items.Add(Res);
            Success:=False;
          end;
        end;
      end;
    end;
    if not Success then Items.Add('');

    for I:=1 to KB_.ListRules.Count-1 do begin
      ptr1:=record_rule(KB_.ListRules.Items[I-1]);
      for J:=I+1 to KB_.ListRules.Count do begin
        ptr2:=record_rule(KB_.ListRules.Items[J-1]);
        if I<>J then begin
          Res:=CompareRules(ptr1,ptr2,2);
          if Res<>'' then begin
           Items.Add(Res);
           Success:=False;
          end;
        end;
      end;
    end;
    if not Success then Items.Add('');

    for I:=1 to KB_.ListConns.Count-1 do begin
      ptr1_:=record_conn(KB_.ListConns.Items[I-1]);
      for J:=I+1 to KB_.ListConns.Count do begin
        ptr2_:=record_conn(KB_.ListConns.Items[J-1]);
        if I<>J then begin
          Res:=CompareConns(ptr1_,ptr2_,2);
          if Res<>'' then begin
           Items.Add(Res);
           Success:=False;
          end;
        end;
      end;
    end;

    if Success then Items.Add(Succ);
  end;
  ShowMutants;
end;

{ Crossing }
procedure TKBTestForm.N15Click(Sender: TObject);
begin
  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    if Crossing then Items.Add(Succ);
  end;
  ShowMutants;
end;

{ Gaps among attributes }
procedure TKBTestForm.N17Click(Sender: TObject);
begin
  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    if References(KB_,1,ListBox1) then Items.Add(Succ);
  end;
  ShowMutants;  {}
end;

{ Gaps among rules }
procedure TKBTestForm.N16Click(Sender: TObject);
begin
  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    Items.Add(Succ);
  end;
  ShowMutants;
end;

{Protokol Test}
procedure TKBTestForm.N22Click(Sender: TObject);
begin
{  ListBox1.Clear;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    if references(1) then Items.Add(Succ);
  end;
  ShowMutants;}
  StartTestKF(Det);
end;

procedure TKBTestForm.N19Click(Sender: TObject);
begin
  References(KB_,2,NIL);
end;

procedure TKBTestForm.N20Click(Sender: TObject);
begin
  Optimize2(KB_);
end;

procedure TKBTestForm.N23Click(Sender: TObject);
{const
  ProgName1='REPERT.EXE';
  ConvDir='../ELENA/';}

//var
//  CmdLine: array[0..255] of Char;

begin
  {ChDir(Copy(ConvDir,1,Length(ConvDir)-1));
  StrPCopy(CmdLine,ConvDir+ProgName1);
  WinExec(CmdLine,SW_SHOWNORMAL);}
  Reper.StartRepertImport(Det);
end;

procedure TKBTestForm.N1Click(Sender: TObject);
begin
  Optimize3(KB_);
end;

procedure TKBTestForm.N3Click(Sender: TObject);
begin
  {Det.StartAdd;}
  StartAdd(Det);
end;

{procedure TAditionEditor.Edit;
begin
  TAdition(Component).Run(Nil);
end;

procedure TAddEditor.Edit;
begin
  TAdd(Component).Run(Nil);
end;
}
procedure TKBTestForm.NumericClick(Sender: TObject);
var
  I,J,N,K,L,M: Integer;
  ptr1: record_type;
  ptr2: value;
  ptr3: list;
  ptr4: record_rule;
  ptr5: TCollection;
  S: array[0..255] of Char;
  C: Char;
  Success: Boolean;
  F: Boolean;
begin
  ListBox1.Clear;
  Success:=True;
  with ListBox1 do begin
    Items.Add('       �������� ������������');
    Items.Add('');
    for I:=1 to KB_.ListTypes.Count do begin
      ptr1:=record_type(KB_.ListTypes.Items[I-1]);
      if ptr1.type_znach<>1 then begin
        for J:=1 to ptr1.list_znach.Count do begin
          ptr2:=value(ptr1.list_znach.Items[J-1]);
          if (ord(ptr2.znach[1])>=ord('0')) and
             (ord(ptr2.znach[1])<=ord('9')) then begin
            case ptr1.type_znach of
            0,2: Items.Add(
              '��� T'+IntToStr(ptr1.n_type)+' �������� ��������, � ��������� ��� ����������');
            3: Items.Add(
              '��� T'+IntToStr(ptr1.n_type)+' �������� ��������, � ��������� ��� ��������');
            else
              Items.Add(
              '��� T'+IntToStr(ptr1.n_type)+' �� ���������');
            end;
            Success:=False;
            break;
          end;
        end;
      end;
    end;

    for I:=1 to KB_.ListRules.Count-1 do begin
      ptr4:=record_rule(KB_.ListRules.Items[I-1]);
      F:=False;
      for N:=1 to 3 do begin
        case N of
        1: ptr5:=ptr4.list_attr_if;
        2: ptr5:=ptr4.list_attr_then;
        3: ptr5:=ptr4.list_attr_else;
        end;
        for J:=1 to ptr5.Count do begin
          ptr3:=list(ptr5.Items[J-1]);
          if (ord(ptr3.znach[1])>=ord('0')) and
             (ord(ptr3.znach[1])<=ord('9')) then begin
            StrPCopy(S,ptr3.znach);
            L:=StrLen(S);
            M:=0;
            for K:=L downto L do begin
              C:=S[K-1];
              if ((ord(C)<Ord('0')) or
                 (ord(C)>Ord('9'))) then begin
              Items.Add(
              '������� R'+IntToStr(ptr4.n_rule)+' �������� �������� ��������� ����');
                Success:=False;
                F:=True;
                M:=K;
                break;
              end;
              if F then break;
            end;
            if F then break;
          end;
          if F then break;
        end;
      end;
    end;

    if Success then Items.Add(Succ);
  end;
end;


procedure TKBTestForm.FindNumericClick(Sender: TObject);
begin
  CorrTypes(KB_);
end;

procedure Optimize3(KB_: TKnowledgeBase);
var
  ptr1_,ptr2_: record_conn;
  Res: String;
  I,J: Integer;
begin
  {������� ��������� ������}
  I:=1;
  while I<=KB_.ListConns.Count-1 do begin
    ptr1_:=record_conn(KB_.ListConns.Items[I-1]);
    J:=I+1;
    while J<=KB_.ListConns.Count do begin
      ptr2_:=record_conn(KB_.ListConns.Items[J-1]);
      if I<>J then begin
        Res:=CompareConns(ptr1_,ptr2_,2);
        if Res<>'' then begin
          {KB_.ListConns.Delete(J-1);}
          ptr2_.Destroy;
          Dec(J);
        end;
      end;
      Inc(J);
    end;
    Inc(I);
  end;
end;

procedure TKBTestForm.ClearTree;
begin
  TreeView1.Items.Clear;
end;

procedure TKBTestForm.ShowTestPlan;
var
  LastItemId: HTREEITEM;
  i: Integer;
  ItemName: String;
begin
  ClearTree;
  if VerifyWizard is TKBVerifyWizard then begin
    LastItemId:=NIL;
    TreeView1.Selected:=(TreeView1.Items.GetNode(LastItemId));
    LastItemId:=TreeView1.Items.Add(TreeView1.Selected,'����').ItemId;
    TreeView1.Selected:=(TreeView1.Items.GetNode(LastItemId));
    for I:=1 to TKBVerifyWizard(VerifyWizard).Steps.Count do begin
      ItemName:=TKBVerifyStepsItem(TKBVerifyWizard(VerifyWizard).Steps.Items[I-1]).TaskName;
      if ItemName<>'' then begin
        LastItemId:=TreeView1.Items.AddChild(TreeView1.Selected,ItemName).ItemId;
        TreeView1.Selected:=(TreeView1.Items.GetNode(LastItemId));
        TreeView1.Selected:=(TreeView1.Selected.Parent);
      end;
    end;
  end;
end;

procedure TKBTestForm.FormShow(Sender: TObject);
begin
  ShowTestPlan;
end;

procedure TKBTestForm.N8Click(Sender: TObject);
var
  i: Integer;
begin
  ListBox1.Items.Clear;
  for I:=1 to TKBVerifyWizard(VerifyWizard).Steps.Count do begin
//    ItemName:=TKBVerifyStepsItem(TKBVerifyWizard(VerifyWizard).Steps.Items[I-1]).TaskName;
//    if ItemName<>'' then begin
    TKBVerifyStepsItem(TKBVerifyWizard(VerifyWizard).Steps.Items[I-1]).Run(ListBox1.Items);
//    end;
  end;
end;

end.
