constructor TVerificatorConflictRule.Create(base: TKnowledgeBase);
begin
 ErrorType := 7;
 inherited Create(base);
end;

procedure ShowMatrix(m: TIntMatrix);
var
 i, j: integer;
 s: string;
begin
 for i := Low(m) to High(m) do begin
  for j := Low(m) to High(m) do begin
   if (m[i, j] <> 0) then s := s + Format('%3d', [m[i, j]])
   else s := s + '  .';
  end;
  s := s + #13#10;
 end;
 ShowMessage(s);
end;

function CmpIncidents(r1, r2: record_rule): boolean;
var
 b1, b2: TStringList;
 inc1, inc2: string;
 not1, not2: boolean;
 i, j, k: integer;
begin
 Result := false;

 b1 := TStringList.Create;
 b2 := TStringList.Create;

 inc1 := r1.incident;
 RemoveBrackets(inc1);
 inc1 := ApplyDeMorgan(inc1, false);
 inc2 := r2.incident;
 RemoveBrackets(inc2);
 inc2 := ApplyDeMorgan(inc2, false);

 k := 0;
 for i := 0 to r1.list_attr_if.Count - 1 do begin
  if (System.Pos('~P' + IntToStr(i + 1), inc1) <> 0) then not1 := true else not1 := false;
  for j := 0 to r2.list_attr_if.Count - 1 do begin
   if (System.Pos('~P' + IntToStr(j + 1), inc2) <> 0) then not2 := true else not2 := false;
   if CmpAttr(list(r1.list_attr_if.Items[i]), list(r2.list_attr_if.Items[j]), not1, not2) then begin
    inc(k);
    inc1 := StringReplace(inc1, 'P' + IntToStr(i + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
    inc2 := StringReplace(inc2, 'P' + IntToStr(j + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
   end;
  end;
 end;

 for i := 0 to r1.list_attr_if.Count - 1 do begin
  inc(k);
  inc1 := StringReplace(inc1, 'P' + IntToStr(i + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
 end;

 for j := 0 to r2.list_attr_if.Count - 1 do begin
  inc(k);
  inc2 := StringReplace(inc2, 'P' + IntToStr(j + 1), 'Q' + IntToStr(k), [rfReplaceAll]);
 end;

 inc1 := StringReplace(inc1, '~', '', [rfReplaceAll]);
 inc2 := StringReplace(inc2, '~', '', [rfReplaceAll]);

 GetDisForm(inc1, b1);
 GetDisForm(inc2, b2);

 if IsEqualForms(b1, b2) then Result := true;

 b2.Free;
 b1.Free;
end;

function ConflictAttr(a1, a2: list): boolean;
begin
 if (((CompareStr(a1.znach, a2.znach) <> 0) or (a1.predicat <> a2.predicat)
       or (a1.factor1 <> a2.factor1) or (a1.factor2 <> a2.factor2)
       or (a1.accuracy <> a2.accuracy))
       //and (a1.n_layer = a2.n_layer)
       and (a1.n_obj = a2.n_obj) and (a1.n_attr = a2.n_attr))
 then
  Result := true
 else
  Result := false;
end;

function ConflictLists(ptr1: TCollection; ptr2: TCollection): boolean;
var
 i, j: Integer;
begin
 Result := false;
 for i := 0 to ptr1.Count - 1 do
  for j := 0 to ptr2.Count - 1 do
   if ConflictAttr(list(ptr1.Items[i]), list(ptr2.Items[j])) then begin
    Result := True;
    break;
   end;
end;

function CmpIfThen(r1, r2: record_rule): boolean;
var
 i, j: integer;
 s: string;
begin
 Result := false;
 s := r2.incident;
 RemoveBrackets(s);
 s := ApplyDeMorgan(s, false);
 
 for i := 0 to r1.list_attr_then.Count - 1 do
  for j := 0 to r2.list_attr_if.Count - 1 do begin
   if (System.Pos('~P' + IntToStr(j + 1), s) <> 0) then begin
    Result := CmpAttr(list(r2.list_attr_if.Items[j]), list(r1.list_attr_then.Items[i]), true)
   end else
    Result := CmpAttr(list(r2.list_attr_if.Items[j]), list(r1.list_attr_then.Items[i]));
   if Result = true then exit;
  end;
end;

procedure TVerificatorConflictRule.FindErrors;
var
 i, j, k, l, t: integer;
 IfIf, IfThen: TIntMatrix;
 s, ch1, ch2: string;
 Chains1, Chains2, tmp: TStringList;
begin
 inherited FindErrors;

 SetLength(IfIf, KB.ListRules.Count, KB.ListRules.Count);
 for i := Low(IfIf) to High(IfIf) do
  for j := Low(IfIf[i]) to High(IfIf[i]) do begin
   IfIf[i, j] := 1;
   if (IfIf[j, i] = 0) then begin

    if CmpIncidents(record_rule(KB.ListRules.Items[i]), record_rule(KB.ListRules.Items[j])) then
     IfIf[i, j] := 2;

    if ConflictLists(record_rule(KB.ListRules.Items[i]).list_attr_then, record_rule(KB.ListRules.Items[j]).list_attr_then) then
     IfIf[i, j] := IfIf[i, j] + 2;

    if (IfIf[i, j] = 4) then begin
     FmtStr(s, '<IMG src="idx:0" align="middle"><B>����������� �������:</B> <A href="R%0:d" n_rule="%0:d">%2:s</A><B> � </B><A href="R%1:d" n_rule="%1:d">%3:s</A>', [i + 1, j + 1, record_rule(KB.ListRules.Items[i]).comment, record_rule(KB.ListRules.Items[j]).comment]);
     AddError(s);
    end;

   end else IfIf[i, j] := IfIf[j, i];
  end;

 SetLength(IfThen, KB.ListRules.Count, KB.ListRules.Count);
 for i := Low(IfThen) to High(IfThen) do
  for j := Low(IfThen[i]) to High(IfThen[i]) do begin
   if CmpIfThen(record_rule(KB.ListRules.Items[i]), record_rule(KB.ListRules.Items[j])) then
    IfThen[i, j] := 1
  end;

 //ShowMatrix(IfIf);
 //ShowMatrix(IfThen);

 Chains1 := TStringList.Create;
 Chains2 := TStringList.Create;
 tmp := TStringList.Create;

 for i := Low(IfIf) to High(IfIf) do
  for j := i to High(IfIf[i]) do begin
   if IfIf[i, j] = 2 then begin
    s := '';
    Chains1.Clear;
    GetRulesChains(i, IfThen, Chains1, s);
    Chains2.Clear;
    GetRulesChains(j, IfThen, Chains2, s);

    //ShowMessage(Chains1.Text + #13#10 + #13#10 + Chains2.Text);
    for k := 0 to Chains1.Count - 1 do
     for l := 0 to Chains2.Count - 1 do begin
      if FindConflict(Chains1[k], Chains2[l], IfIf, ch1, ch2) then begin
       //ShowMessage(ch1 + ' conflicts with ' + ch2);

       s := '<IMG src="idx:0" align="middle"><B>����������� ������� ������:</B> ';

       tmp.CommaText := ch1;
       for t := 0 to tmp.Count - 1 do begin
        s := s + Format('<A href="R%0:d" n_rule="%0:d">R%0:d</A>', [StrToInt(tmp[t])]);
        if t <> tmp.Count - 1 then s := s + '<B>�</B>';
       end;

       s := s + '<B> � </B>';

       tmp.CommaText := ch2;
       for t := 0 to tmp.Count - 1 do begin
        s := s + Format('<A href="R%0:d" n_rule="%0:d">R%0:d</A>', [StrToInt(tmp[t])]);
        if t <> tmp.Count - 1 then s := s + '<B>�</B>';
       end;

       AddError(s);

      end;
     end;

   end;
  end;

 tmp.Free;
 Chains2.Free;
 Chains1.Free;
end;

procedure TVerificatorConflictRule.FillPopup(Menu: TPopupMenu; n: integer);
var
 tmp: TMenuItem;
 s: string;
 i: integer;
begin
 i := 1;
 while true do begin
  s := GetTagValue(Errors[n], 'A', i);
  if s = '' then break;
  tmp := TMenuItem.Create(Menu);
  Menu.Items.Add(tmp);
  tmp.Caption := '������� ������� ' + '''' + s + '''';
  Inc(i);
 end;
end;

function TVerificatorConflictRule.PopupClick(n: integer; action: integer): boolean;
var
 s: string;
 n_rule, i: integer;
 currule: record_rule;
begin
 Result := false;
 s := Errors[n];
 n_rule := StrToInt(GetTagParam(s, 'A', 'n_rule', action + 1)) - 1;

 currule := record_rule(KB.ListRules.Items[n_rule]);
 if (ThisForm.MessageYesNo('������� ������� ' + '''' + currule.comment + ''''+ '?','������� �������') = IDYES) then begin
  currule.Destroy;

  for i := 0 to KB.ListRules.Count - 1 do begin
   currule := record_rule(KB.ListRules.Items[i]);
   currule.n_rule := i + 1;
  end;

  Result := true;
 end;
end;
