unit VerifyReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, slstbox, Buttons, ComCtrls, ExtCtrls, ImgList, VerifyTypes,
  KBTypes, Aspr95, EditKB, grids, Menus, RA_YesNoDlg, KBCompon, VerifyUtils,
  VerifyPrint;

type
  TFormVerifyReport = class(TForm)
    PanelTop: TPanel;
    PanelCenter: TPanel;
    PanelLeft: TPanel;
    BoxReport: TSectionListBox;
    ButtonExit: TSpeedButton;
    ButtonPrint: TSpeedButton;
    PanelPic: TPanel;
    ImageReport: TImage;
    PanelInfo: TPanel;
    PanelLegend: TPanel;
    LabelErrorsFound: TLabel;
    ImageErrorsFound: TImage;
    ImageErrorsNone: TImage;
    LabelErrorsNone: TLabel;
    ImageListSections: TImageList;
    ImageListItems: TImageList;
    ImageDiscovered: TImage;
    LabelDiscovered: TLabel;
    ImageFixed: TImage;
    LabelFixed: TLabel;
    PanelBottom: TPanel;
    StatusBar: TStatusBar;
    PanelProgress: TPanel;
    ProgressBar: TProgressBar;
    ButtonEditor: TSpeedButton;
    PopupMenuError: TPopupMenu;
    LabelSolution: TLabel;
    procedure ButtonExitClick(Sender: TObject);
    procedure BoxReportClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BoxReportAnchorEnter(Sender: TObject; index: Integer;
      anchor: String);
    procedure BoxReportAnchorExit(Sender: TObject; index: Integer;
      anchor: String);
    procedure BoxReportAnchorClick(Sender: TObject; index: Integer;
      anchor: String);
    procedure ButtonEditorClick(Sender: TObject);
    procedure PopupMenuErrorPopup(Sender: TObject);
    procedure BoxReportSubItemRClick(Sender: TObject; sectionidx,
      subitemidx: Integer);
    procedure PopupMenuErrorClick(Sender: TObject);
    function MessageYesNo(Text,Caption: string): integer;
    procedure ImageReportClick(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
  private
    { Private declarations }
    YesNoDlg: TYesNoDlg;
    Verificators: TList;
  public
    VerifyWhat: set of TErrorTypes;
    KB: TKnowledgeBase;
    KBEditor: TKBEditor;
    FormPrint: TFormVerifyPrint;
    procedure ShowSolutionText(n: integer);
    procedure FindErrors;
    { Public declarations }
  end;

  TVerificator = class(TObject)
   ErrorType: integer;
   Errors: TStringList;
   ErrorString: string;
   SolutionString: string;
   KB: TKnowledgeBase;
   constructor Create(base: TKnowledgeBase);
   destructor Destroy; override;
   procedure FindErrors; virtual;
   procedure FixErrors; virtual;
   function ErrorsCount: integer;
   procedure FillPopup(Menu: TPopupMenu; n: integer); virtual;
   function PopupClick(n: integer; action: integer): boolean; virtual;
  private
   FirstRun: boolean;
   ErrorsOld: TStringList;
   procedure AddError(err: string); //virtual;
   procedure CheckFixed; virtual;
   function FindError(err: string): integer;
  end;

  TVerificatorAttrNotUsed = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FindErrors; override;
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
   procedure FixErrors; override;
  end;

  TVerificatorAttrWrongVal = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
   procedure FindErrors; override;
  end;

  TVerificatorImpossibleThen = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
   procedure FindErrors; override;
  end;

  TVerificatorClosedRule = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FindErrors; override;
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
  end;

  TVerificatorOddIf = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FindErrors; override;
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
  end;

  TVerificatorExcessRule = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FindErrors; override;
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
  end;

  TVerificatorConflictRule = class(TVerificator)
   constructor Create(base: TKnowledgeBase);
   procedure FindErrors; override;
   procedure FillPopup(Menu: TPopupMenu; n: integer); override;
   function PopupClick(n: integer; action: integer): boolean; override;
  end;

var
  ThisForm: TFormVerifyReport;
  ErrCount: integer;

implementation

{$R *.DFM}

//------------------------------------------------------------------------------
constructor TVerificator.Create(base: TKnowledgeBase);
begin
 inherited Create;

 Errors := TStringList.Create;
 ErrorsOld := TStringList.Create;

 ErrorString := ErrorStrings[ErrorType];
 SolutionString := SolutionStrings[ErrorType];

 FirstRun := true;
 KB := base;
end;

destructor TVerificator.Destroy;
var
 i: integer;
begin
 for i := 0 to Errors.Count - 1 do begin
  if (Errors.Objects[i] <> nil) then Errors.Objects[i].Free;
 end;
 Errors.Clear;
 Errors.Free;
 ErrorsOld.Clear;
 ErrorsOld.Free;
 inherited Destroy;
end;

function TVerificator.FindError(err: string): integer;
var
 i: integer;
 s: string;
begin
 Result := -1;
 RemoveTag(err, 'A');
 for i := 0 to Errors.Count - 1 do begin
  s := Errors.Strings[i];
  RemoveTag(s, 'A');
  if CompareStr(err, s) = 0 then begin
   Result := i;
   exit;
  end;
 end;
end;

procedure TVerificator.AddError(err: string);
begin
 if (Errors.IndexOf(err) = -1) then Errors.Add(err)
end;

procedure TVerificator.FindErrors;
var
 i: integer;
begin
 for i := 0 to ErrorsOld.Count - 1 do begin
  if (ErrorsOld.Objects[i] <> nil) then ErrorsOld.Objects[i].Free;
 end;
 ErrorsOld.Clear;
 ErrorsOld.Assign(Errors);
 for i := 0 to Errors.Count - 1 do begin
  if (Errors.Objects[i] <> nil) then Errors.Objects[i].Free;
 end;
 Errors.Clear;
 //Sleep(500);
end;

procedure TVerificator.CheckFixed;
var
 i, f: integer;
 tmp: string;
begin
 for i := 0 to ErrorsOld.Count - 1 do begin
  tmp := ErrorsOld.Strings[i];
  if FindError(tmp) = -1 then begin
   tmp[15] := '1';
   RemoveTag(tmp, 'A');
   Errors.Add(tmp);
  end;
 end;
 i := 0;
 while i < Errors.Count do begin
  tmp := Errors[i];
  if tmp[15] = '1' then begin
   tmp[15] := '0';
   f := FindError(tmp);
   if f = -1 then inc(i)
   else Errors.Delete(i);
  end else inc(i);
 end;
 Errors.Sort;
 FirstRun := false;
end;

procedure TVerificator.FixErrors;
begin
end;

function TVerificator.ErrorsCount: integer;
var
 i: integer;
 tmp: string;
begin
 Result := 0;
 for i := 0 to Errors.Count - 1 do begin
  tmp := Errors.Strings[i];
  if (tmp[15] = '0') then inc(Result);
 end;
end;

procedure TVerificator.FillPopup(Menu: TPopupMenu; n: integer);
begin
end;

function TVerificator.PopupClick(n: integer; action: integer): boolean;
begin
 Result := false;
end;
//------------------------------------------------------------------------------
procedure GetUsedTypesList(KB: TKnowledgeBase; lst: TList);
var
 i, j: integer;
 cur_a: record_attr;
 cur_o: record_obj;
begin
 for i := 0 to KB.ListObjs.Count - 1 do begin
  cur_o := record_obj(KB.ListObjs.Items[i]);
  for j := 0 to cur_o.ListAttrs.Count - 1 do begin
   cur_a := record_attr(cur_o.ListAttrs.Items[j]);
   lst.Add(record_type(KB.ListTypes.Items[cur_a.n_type - 1]));
  end;
 end;
end;

procedure GetUsedValsInRuleIfs(KB: TKnowledgeBase; lst: TList);
var
 i, j: integer;
 cur_r: record_rule;
begin
 for i := 0 to KB.ListRules.Count - 1 do begin
  cur_r := record_rule(KB.ListRules.Items[i]);
  for j := 0 to cur_r.list_attr_if.Count - 1 do
   lst.Add(list(cur_r.list_attr_if.Items[j]));
 end;
end;

function CmpAttr(a1, a2: list; not1: boolean = false; not2: boolean = false): boolean;
const
 PrNotTab: array [0..5] of integer = (5, 4, 3, 2, 1, 0);
var
 p1, p2: integer;
begin
 p1 := a1.predicat;
 p2 := a2.predicat;
 if not1 then p1 := PrNotTab[p1];
 if not2 then p2 := PrNotTab[p2];
 if (CompareStr(a1.znach, a2.znach) = 0) and
//    (a1.n_layer = a2.n_layer) and
    (a1.n_obj = a2.n_obj) and
    (a1.n_attr = a2.n_attr) and
    (a1.factor1 = a2.factor1) and
    (a1.factor2 = a2.factor2) and
    (a1.accuracy = a2.accuracy) and
    (p1 = p2) then
  CmpAttr := true
 else
  CmpAttr := false;
end;

function CmpListsAttr(ptr1: TCollection; ptr2: TCollection): boolean;
var
 I1, I2: Integer;
begin
 if ptr1.Count <> ptr2.Count then CmpListsAttr := false else begin
  CmpListsAttr := true;
  I1 := 1;
  I2 := 1;
  while (I1 <= ptr1.Count) and (I2 <= ptr2.Count) do begin
   if CmpAttr(list(ptr1.Items[I1-1]), list(ptr2.Items[I2-1])) then begin
    Inc(I1);
    Inc(I2);
   end else begin
    CmpListsAttr := false;
    break;
   end;
  end;
 end;
end;

//------------------------------------------------------------------------------
{$INCLUDE V_AttrNotUsed.inc}
{$INCLUDE V_AttrWrongVal.inc}
{$INCLUDE V_ImpossibleThen.inc}
{$INCLUDE V_OddIf.inc}
{$INCLUDE V_ConflictRule.inc}
{$INCLUDE V_ExcessRule.inc}
{$INCLUDE V_ClosedRule.inc}
//==============================================================================

procedure TFormVerifyReport.ButtonExitClick(Sender: TObject);
begin
 Close;
end;

procedure TFormVerifyReport.BoxReportClick(Sender: TObject);
begin
 if (BoxReport.GetActiveSection <> nil) then
  ShowSolutionText(BoxReport.GetActiveSection.Tag)
 else
  ShowSolutionText(0);
end;

procedure TFormVerifyReport.ShowSolutionText(n: integer);
begin
 LabelSolution.Caption := SolutionStrings[n];
end;

procedure TFormVerifyReport.FormShow(Sender: TObject);
var
 i: integer;
 tmp: TVerificator;
 sec: TListSection;
begin
 Verificators.Clear;

 if (etAttrNotUsed in VerifyWhat) then Verificators.Add(TVerificatorAttrNotUsed.Create(KB));
 if (etAttrWrongVal in VerifyWhat) then Verificators.Add(TVerificatorAttrWrongVal.Create(KB));
 if (etImpossibleThen in VerifyWhat) then Verificators.Add(TVerificatorImpossibleThen.Create(KB));
 if (etClosedRule in VerifyWhat) then Verificators.Add(TVerificatorClosedRule.Create(KB));
 if (etOddIf in VerifyWhat) then Verificators.Add(TVerificatorOddIf.Create(KB));
 if (etExcessRule in VerifyWhat) then Verificators.Add(TVerificatorExcessRule.Create(KB));
 if (etConflictRule in VerifyWhat) then Verificators.Add(TVerificatorConflictRule.Create(KB));

 BoxReport.Clear;
 for i := 0 to Verificators.Count - 1 do begin
  tmp := TVerificator(Verificators.Items[i]);
  sec := BoxReport.Sections.Add;
  with sec do begin
   Caption := tmp.ErrorString;
   Font.Assign(Self.Font);
   FontUsage := fuBoth;
   ImageIndex := 0;
   ItemHeight := 24;
   ReadOnly := true;
   Tag := tmp.ErrorType;
   VAlignment := vtaCenter;
   AutoSize := true;
   Autoedit := false;
   Color := clWindow;
  end;
 end;

 if (BoxReport.GetActiveSection <> nil) then
  ShowSolutionText(BoxReport.GetActiveSection.Tag)
 else
  ShowSolutionText(0);
end;

procedure TFormVerifyReport.FormCreate(Sender: TObject);
begin
 ThisForm := Self;
 VerifyWhat := [];
 Verificators := TList.Create;
 YesNoDlg := TYesNoDlg.Create(Self);
end;

procedure TFormVerifyReport.FormHide(Sender: TObject);
var
 i: integer;
begin
 VerifyWhat := [];
 for i := 0 to Verificators.Count - 1 do TVerificator(Verificators.Items[i]).Free;
 Verificators.Clear;
 BoxReport.Clear;
end;

procedure TFormVerifyReport.FormDestroy(Sender: TObject);
begin
 Verificators.Free;
 YesNoDlg.Free;
end;

procedure TFormVerifyReport.FindErrors;
var
 i, c: integer;
 tmp: TVerificator;
begin
 Screen.Cursor := crHourGlass;
 ProgressBar.Max := Verificators.Count;
 ProgressBar.Position := 0;
 ErrCount := 0;

 //AnimateVerify.Active := True;
 Application.ProcessMessages;
 BoxReport.BeginUpdate;
 for i := 0 to Verificators.Count - 1 do begin
  tmp := TVerificator(Verificators.Items[i]);

  StatusBar.SimpleText := '����� ������ ����: ''' + tmp.ErrorString + '''...';
  Application.ProcessMessages;

  tmp.FindErrors;
  tmp.CheckFixed;
  BoxReport.Sections.Items[i].SubItems.Clear;
  BoxReport.Sections.Items[i].SubItems.Assign(tmp.Errors);

  c := tmp.ErrorsCount;
  if c > 0 then begin
   BoxReport.Sections.Items[i].ImageIndex := 1;
   //BoxReport.Sections.Items[i].State := lssExpanded;
   ErrCount := ErrCount + c;
  end else
   BoxReport.Sections.Items[i].ImageIndex := 0;

  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;
 BoxReport.EndUpdate;

 //AnimateVerify.Active := False;
 StatusBar.SimpleText := '������ �������: ' + IntToStr(ErrCount);
 ProgressBar.Position := 0;
 Screen.Cursor := crDefault;
end;

procedure TFormVerifyReport.FormActivate(Sender: TObject);
begin
 FindErrors;
end;

procedure TFormVerifyReport.BoxReportAnchorEnter(Sender: TObject;
  index: Integer; anchor: String);
var
 n: integer;
begin
 if (anchor[1] = 'R') and (System.Pos(':', anchor) = 0) then begin
  n := StrToInt(Copy(anchor, 2, Length(anchor) - 1));
  StatusBar.SimpleText := '�������� ������� ''' + record_rule(KB.ListRules.Items[n-1]).comment + ''' � ���������';
 end else
  StatusBar.SimpleText := '�������� ' + anchor + ' � ���������';
end;

procedure TFormVerifyReport.BoxReportAnchorExit(Sender: TObject;
  index: Integer; anchor: String);
begin
 StatusBar.SimpleText := '';
end;

procedure TFormVerifyReport.BoxReportAnchorClick(Sender: TObject;
  index: Integer; anchor: String);
begin
 KBEditor.ShowWhat := anchor;
 KBEditor.Run(Self);
 KBEditor.ShowWhat := '';
 FindErrors;
end;

procedure TFormVerifyReport.ButtonEditorClick(Sender: TObject);
begin
 KBEditor.Run(Self);
 FindErrors;
end;

procedure TFormVerifyReport.PopupMenuErrorPopup(Sender: TObject);
var
 seci, itemi, i: integer;
 s: string;
begin
 while PopupMenuError.Items.Count > 0 do
  PopupMenuError.Items.Delete(0);

 if (BoxReport.GetSelection(seci, itemi, s)) and (itemi <> -1) then begin
  if s[15] = '0' then
   TVerificator(Verificators[seci]).FillPopup(PopupMenuError, itemi);
 end;

 for i := 0 to PopupMenuError.Items.Count - 1 do begin
  PopupMenuError.Items[i].OnClick := PopupMenuErrorClick;
 end;

end;

procedure TFormVerifyReport.BoxReportSubItemRClick(Sender: TObject;
  sectionidx, subitemidx: Integer);
begin
 BoxReport.SetSelection(sectionidx, subitemidx);
end;

procedure TFormVerifyReport.PopupMenuErrorClick(Sender: TObject);
var
 seci, itemi: integer;
 s: string;
begin
 Screen.Cursor := crHourGlass;

 if (BoxReport.GetSelection(seci, itemi, s)) and (itemi <> -1) then begin
  with Sender as TMenuItem do begin
   if TVerificator(Verificators[seci]).PopupClick(itemi, MenuIndex) then FindErrors;
  end;
 end;

 Screen.Cursor := crDefault;
end;

function TFormVerifyReport.MessageYesNo(Text,Caption: string): integer;
begin
 YesNoDlg.Caption := Caption;
 YesNoDlg.LabelText.Caption := Text;

 if YesNoDlg.ShowModal = mrYes then Result := IDYES
 else Result := IDNO;
end;


procedure TFormVerifyReport.ImageReportClick(Sender: TObject);
begin
 FindErrors;
end;

function SelectTagValue(Rich: TRichEdit; tag: string): boolean;
var
 p1, p2: integer;
begin
 Result := false;
 p1 := Rich.FindText('<' + tag, Rich.SelStart, Length(Rich.Text) - Rich.SelStart, [stMatchCase]);
 if p1 = -1 then exit;
 p1 := Rich.FindText('>', p1, Length(Rich.Text) - p1, [stMatchCase]);
 if p1 = -1 then exit;
 inc(p1);
 p2 := Rich.FindText('</' + tag + '>', p1, Length(Rich.Text) - p1, [stMatchCase]);
 if p2 = -1 then exit;

 Rich.SelStart := p1;
 Rich.SelLength := p2 - p1;
 Result := true;
end;

function ClearTag(Rich: TRichEdit; tag: string): boolean;
var
 p1, p2: integer;
begin
 Result := false;

 p1 := Rich.FindText('<' + tag, Rich.SelStart, Length(Rich.Text) - Rich.SelStart, [stMatchCase]);
 if p1 = -1 then exit;
 p2 := Rich.FindText('>', p1, Length(Rich.Text) - p1, [stMatchCase]);
 if p2 = -1 then exit;
 inc(p2);
 Rich.SelStart := p1;
 Rich.SelLength := p2 - p1;
 Rich.ClearSelection;

 Result := true;

 p1 := Rich.FindText('</' + tag + '>', p1, Length(Rich.Text) - p1, [stMatchCase]);
 if p1 = -1 then exit;

 Rich.SelStart := p1;
 Rich.SelLength := Length('</' + tag + '>');
 Rich.ClearSelection;
end;

procedure TFormVerifyReport.ButtonPrintClick(Sender: TObject);
var
 i, j: integer;
 //FormPrint: TFormVerifyPrint;
 s: string;
begin
 Screen.Cursor := crHourGlass;
 StatusBar.SimpleText := '���������� � ������...';
 ProgressBar.Max := ErrCount * 10;
 ProgressBar.Position := 0;

 //FormPrint := TFormVerifyPrint.Create(NIL);
 FormPrint.RichEditPrint.Clear;

 for i := 0 to BoxReport.Sections.Count - 1 do begin
  s := BoxReport.Sections.Items[i].Caption + ':';
  FormPrint.RichEditPrint.Lines.Add(s);
  FormPrint.RichEditPrint.SelStart := FormPrint.RichEditPrint.SelStart - Length(s) - 2;
  FormPrint.RichEditPrint.SelLength := Length(s);
  FormPrint.RichEditPrint.SelAttributes.Style := [fsUnderline, fsBold];
  FormPrint.RichEditPrint.SelAttributes.Color := clGreen;
  for j := 0 to BoxReport.Sections.Items[i].SubItems.Count - 1 do begin
   s := BoxReport.GetSectionSubItem(i, j);
   if s[15] = '0' then s := '�������� - ' + s
   else s := '���������� - ' + s;
   RemoveTag(s, 'IMG');
   //RemoveTag(s, 'A');
   //RemoveTag(s, 'B');
   FormPrint.RichEditPrint.Lines.Add(s);
   ProgressBar.StepIt;
   Application.ProcessMessages;
  end;
 end;

 FormPrint.RichEditPrint.SelStart := 0;
 while SelectTagValue(FormPrint.RichEditPrint, 'A') do with FormPrint.RichEditPrint do begin
  SelAttributes.Color := clBlue;
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;
 FormPrint.RichEditPrint.SelStart := 0;
 while ClearTag(FormPrint.RichEditPrint, 'A') do begin
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;

 FormPrint.RichEditPrint.SelStart := 0;
 while SelectTagValue(FormPrint.RichEditPrint, 'B') do with FormPrint.RichEditPrint do begin
  SelAttributes.Style := [fsBold];
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;
 FormPrint.RichEditPrint.SelStart := 0;
 while ClearTag(FormPrint.RichEditPrint, 'B') do begin
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;

 FormPrint.RichEditPrint.SelStart := 0;
 while SelectTagValue(FormPrint.RichEditPrint, 'FONT') do with FormPrint.RichEditPrint do begin
  SelAttributes.Color := clMaroon;
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;
 FormPrint.RichEditPrint.SelStart := 0;
 while ClearTag(FormPrint.RichEditPrint, 'FONT') do begin
  ProgressBar.StepIt;
  Application.ProcessMessages;
 end;

 Screen.Cursor := crDefault;
 ProgressBar.Position := 0;
 StatusBar.SimpleText := '';

 FormPrint.ShowModal;
 //FormPrint.Free;
end;

end.
