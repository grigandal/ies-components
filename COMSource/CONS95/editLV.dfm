object editLVForm: TeditLVForm
  Left = 172
  Top = 94
  BorderStyle = bsSingle
  Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1083#1080#1085#1075#1074#1080#1089#1090#1080#1095#1077#1089#1082#1086#1081' '#1087#1077#1088#1077#1084#1077#1085#1085#1086#1081
  ClientHeight = 345
  ClientWidth = 786
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 81
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1051#1055
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 67
    Width = 136
    Height = 13
    Caption = #1054#1073#1083#1072#1089#1090#1100' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 160
    Top = 56
    Width = 84
    Height = 13
    Caption = #1053#1080#1078#1085#1103#1103' '#1075#1088#1072#1085#1080#1094#1072
  end
  object Label4: TLabel
    Left = 280
    Top = 56
    Width = 86
    Height = 13
    Caption = #1042#1077#1088#1093#1085#1103#1103' '#1075#1088#1072#1085#1080#1094#1072
  end
  object Label5: TLabel
    Left = 8
    Top = 96
    Width = 79
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1051#1055
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 786
    Height = 2
    Align = alTop
  end
  object Bevel2: TBevel
    Left = 0
    Top = 49
    Width = 393
    Height = 2
  end
  object Bevel3: TBevel
    Left = 392
    Top = 8
    Width = 2
    Height = 289
  end
  object edtName: TEdit
    Left = 8
    Top = 24
    Width = 369
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
  end
  object edtLow: TEdit
    Left = 160
    Top = 72
    Width = 97
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    Text = '0'
    OnChange = CheckValues
  end
  object edtHigh: TEdit
    Left = 280
    Top = 72
    Width = 97
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    Text = '10'
    OnChange = CheckValues
  end
  object btnAdd: TButton
    Left = 128
    Top = 304
    Width = 75
    Height = 17
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 3
    OnClick = btnAddClick
  end
  object btnChange: TButton
    Left = 216
    Top = 304
    Width = 75
    Height = 17
    Caption = #1048#1079#1084#1077#1085#1080#1090#1100
    Enabled = False
    TabOrder = 4
    OnClick = btnChangeClick
  end
  object btnDel: TButton
    Left = 304
    Top = 304
    Width = 75
    Height = 17
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Enabled = False
    TabOrder = 5
    OnClick = btnDelClick
  end
  object lbLVvalue: TListBox
    Left = 8
    Top = 120
    Width = 369
    Height = 177
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    OnClick = lbLVvalueClick
  end
  object btnOK: TButton
    Left = 608
    Top = 304
    Width = 75
    Height = 17
    Caption = 'OK'
    TabOrder = 7
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 696
    Top = 304
    Width = 75
    Height = 17
    Caption = 'Cancel'
    TabOrder = 8
    OnClick = btnCancelClick
  end
  object Chart1: TChart
    Left = 408
    Top = 8
    Width = 369
    Height = 289
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    BackWall.Color = clBlack
    LeftWall.Brush.Color = clWhite
    Title.Font.Charset = RUSSIAN_CHARSET
    Title.Font.Color = clBlue
    Title.Font.Height = -11
    Title.Font.Name = 'Times New Roman'
    Title.Font.Style = []
    Title.Text.Strings = (
      #1060#1091#1085#1082#1094#1080#1103' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080)
    BackColor = clBlack
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Axis.Style = psDot
    BottomAxis.Axis.Width = 1
    BottomAxis.ExactDateTime = False
    BottomAxis.Increment = 1.000000000000000000
    BottomAxis.Maximum = 10.000000000000000000
    BottomAxis.Title.Caption = 'X'
    BottomAxis.Title.Font.Charset = DEFAULT_CHARSET
    BottomAxis.Title.Font.Color = clBlack
    BottomAxis.Title.Font.Height = -16
    BottomAxis.Title.Font.Name = 'Arial'
    BottomAxis.Title.Font.Style = []
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Width = 1
    LeftAxis.ExactDateTime = False
    LeftAxis.Increment = 0.100000000000000000
    LeftAxis.LabelsSize = 18
    LeftAxis.Maximum = 1.000000000000000000
    LeftAxis.MinorTickCount = 2
    LeftAxis.TickLength = 3
    LeftAxis.Title.Angle = 0
    LeftAxis.Title.Caption = 'Y'
    LeftAxis.Title.Font.Charset = DEFAULT_CHARSET
    LeftAxis.Title.Font.Color = clBlack
    LeftAxis.Title.Font.Height = -16
    LeftAxis.Title.Font.Name = 'Arial'
    LeftAxis.Title.Font.Style = []
    LeftAxis.TitleSize = 1
    Legend.LegendStyle = lsValues
    Legend.TextStyle = ltsXValue
    Legend.Visible = False
    RightAxis.Visible = False
    TopAxis.Visible = False
    View3D = False
    BevelOuter = bvNone
    BevelWidth = 0
    BorderStyle = bsSingle
    Color = clSilver
    TabOrder = 9
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Style = smsValue
      Marks.Visible = False
      SeriesColor = clRed
      LinePen.Color = clRed
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object sbError: TStatusBar
    Left = 0
    Top = 326
    Width = 786
    Height = 19
    BiDiMode = bdLeftToRight
    Panels = <>
    ParentBiDiMode = False
  end
  object SaveDialog1: TSaveDialog
    Left = 544
    Top = 64
  end
  object OpenDialog1: TOpenDialog
    Left = 592
    Top = 56
  end
  object MainMenu1: TMainMenu
    Left = 640
    Top = 56
    object File1: TMenuItem
      Caption = 'File'
      object Save1: TMenuItem
        Caption = 'Save...'
        OnClick = Save1Click
      end
      object Load1: TMenuItem
        Caption = 'Load...'
        OnClick = Load1Click
      end
      object Clear1: TMenuItem
        Caption = 'Clear...'
        OnClick = Clear1Click
      end
    end
  end
end
