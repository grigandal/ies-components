{**************************************************************
                    ����  CONVERT1.PAS
               �������� ���������� �����
         ��� ��������� ������������� ����������,
        ���������� � ��������  ���������� ������
          ��� ������� ������ (���� ������� 22)
          Copyright (c) Smirnov V.V.  23.10.97
**************************************************************}
unit CONVERT1;

interface

uses
  VCL.Dialogs, SysUtils, WinTypes, Edit_str, Conv_dat, Classes;

const
  BUF_STREAM_=16000;
  MAXSTR=1024;

type
  my_buf=array [0..BUF_STREAM_-1] of Char;

const
  strnumber_Format: Integer=0;
  
var
  FormatItems: TList;

  buf_stream: ^my_buf;
  buf: TStr;
  fp: Text;
  lengthf: Longint;


function OpenFormat(fname: String): Boolean;
procedure LoadFormat(fname: String; strnumber: PInteger);
procedure FreeFormat(strnumber: PInteger);
procedure CloseFormat;


implementation


function OpenFormat(fname: String): Boolean;
var
  CurDir: String;
begin
  OpenFormat:=False;
  if FileExists(fname) then begin
    FormatItems:=TList.Create;
    {$I-}
    Assign(fp,fname);
    Reset(fp);
    {$I+}
    if IOResult<>0 then ShowMessage('�� ���� ������� ���� �������:'+fname);
    New(buf_stream);
    SetTextBuf(fp,buf_stream^);
    OpenFormat:=True;
  end else begin
    ShowMessage('�� ���� ����� ���� �������:'+fname);
  end;
end;

procedure CloseFormat;
begin
  Flush(fp);
  Dispose(buf_stream);
  Close(fp);
end;

procedure LoadFormat(fname: String; strnumber: PInteger);
var
  ptr: TFormatItem;
  i,l,j: Integer;

begin
  if OpenFormat(fname) and (not(eof(fp))) then begin
    i:=0;
    Inc(i);
    Readln(fp,buf);
    if buf[0]=#0 then begin
      buf[0]:=#32;
      buf[1]:=#0;
    end;
    {j:=0;
    while (buf[j]<>#0) do begin if buf[j]=#255 then buf[j]:=#32; Inc(j); end;}
    FormatItems.Add(TFormatItem.Create);
    ptr:=TFormatItem(FormatItems.Items[(FormatItems.Count-1)]);
    StrCopy(ptr.str,buf);
    ptr.n := i;
    buf[0]:=#0;
    while (not(eof(fp))) do begin
      Inc(i);
      Readln(fp,buf);
      if IOResult <> 0 then begin
        buf[0]:=#0;
      end;
      if buf[0]=#0 then begin
        buf[0]:=#32;
        buf[1]:=#0;
      end;
      {j:=0;
      while (buf[j]<>#0) do begin if buf[j]=#255 then buf[j]:=#32; Inc(j); end;}
      {if(buf[0]=#0) then begin strnumber^:=i;CloseFormat;exit;end;}
      FormatItems.Add(TFormatItem.Create);
      ptr:=TFormatItem(FormatItems.Items[(FormatItems.Count-1)]);
      ptr.n := i;
      StrCopy(ptr.str,buf);
      buf[0]:=#0;
    end;{end while}
    CloseFormat;
    strnumber^:=i;
  end else begin
    CloseFormat;
    strnumber^:=0;
  end;{end if}
end;

procedure FreeFormat(strnumber: PInteger);
var
  ptr: TFormatItem;

begin
  strnumber^ := 0;
  while (FormatItems.Count <> 0) do begin
    ptr  := FormatItems.Items[0];
    FormatItems.Delete(0);
    ptr.Destroy;
  end;
  if FormatItems<>NIL then FormatItems.Destroy;
end;

end.
