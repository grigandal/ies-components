unit KB_Types;

 {******************************************************************************
 * ������ KB_Types.pas �������� ����������� ����������� ������ TKBType:
 *
 * TNumericType - ��������� ��� ��������� ����
 * TSymbolicType - ��������� ��� ����������� ����
 * TFuzzyType - ��������� ��� ��������� ����
 * TArrayType - ��������� ��� �������
 * TReferenceType - ��������� ��� ����-������
 * TCOMWrapper - ��������� IDispatch ��� ������ � COM
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, ActiveX, Windows, ComObj,
  KB_Containers, XML_Consts;

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TNumericType �������� ����������� ��������� ����
  ////////////////////////////////////////////////////////////////////////////

  TNumericType = class(TKBType)
  public
    MinValue: double;           // ����������� �������� ���������� ������� ���� (���� ����� MaxValue, �� ����������� ���)
    MaxValue: double;           // ������������ �������� ���������� ������� ����
    IsInteger: boolean;         // True, ���� ���������� ����� ���� �.�. ������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    function GetXML: {Wide}String; override;
    function Valid(Value: TValue): boolean; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TSymbolicType �������� ����������� ����������� ����
  ////////////////////////////////////////////////////////////////////////////

  TSymbolicType = class(TKBType)
  public
    Values: TStringList;        // ���������� �������� ���������� ����� ����
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TArrayType �������� ����������� �������
  ////////////////////////////////////////////////////////////////////////////

  TArrayType = class(TKBType)
  public
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TFuzzyType �������� ����������� ��������� ����
  ////////////////////////////////////////////////////////////////////////////

  TFuzzyType = class(TKBType)
  public
    MFList: TList;
    Units: TList;
    Synonyms: TStringList;
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TReferenceType �������� ����������� ����-������
  ////////////////////////////////////////////////////////////////////////////

  TReferenceType = class(TKBType)
  public
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TCOMWrapper �������� ��������� ���������� IDispatch ��� ������ � COM
  ////////////////////////////////////////////////////////////////////////////

  TCOMWrapper = class(TKBClass)
  public
    Obj: IDispatch;             // ������ �� COM-������
    ProgID: WideString;         // ������ � ProgID ������� COM-�������
    function GetXML: {Wide}String; override;
    function CreateInstance: TComponent; override; // ������� ��������� ������
    function Connect: boolean;
    function Invoke(ProcName: WideString; Params: TList): boolean;
  private
    function OleMethod(MethodName: WideString; var Params: DISPPARAMS): OleVariant;
    function OlePropertyGet(PropertyName: WideString; var Params: DISPPARAMS): OleVariant;
    procedure OlePropertySet(PropertyName: WideString; var Params: DISPPARAMS);
  end;


implementation

uses KB_Fuzzy;

/////////////////////////////////////////
// ������ ������ TNumericType
/////////////////////////////////////////

constructor TNumericType.Create(ACollection: TCollection);
begin
  inherited;
  MinValue := 0;
  MaxValue := 0;
  IsInteger := false;
end;

function TNumericType.GetXML: {Wide}String;
var
  Doc: IXMLDocument;
  Val: Variant;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<'+sType+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sMeta] := sNumber;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  Val := MinValue;
  Doc.DocumentElement.AddChild(sFrom);
  Doc.DocumentElement.ChildNodes[sFrom].Text := Val;

  Val := MaxValue;
  Doc.DocumentElement.AddChild(sTo);
  Doc.DocumentElement.ChildNodes[sTo].Text := Val;

  Result := Doc.DocumentElement.XML;
end;

function TNumericType.Valid(Value: TValue): boolean;
begin
  if MaxValue >= MinValue then Result := True
  else Result := False;
end;


/////////////////////////////////////////
// ������ ������ TSymbolicType
/////////////////////////////////////////

constructor TSymbolicType.Create(ACollection: TCollection);
begin
  inherited;
  Values := TStringList.Create;
end;

destructor TSymbolicType.Destroy;
begin
  Values.Free;
  inherited;
end;

function TSymbolicType.GetXML: {Wide}String;
var
  Doc: IXMLDocument;
  i: integer;
  n,m: IXMLNode;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<'+sType+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sMeta] := sString;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  n := Doc.DocumentElement;
  for i:=0 to Values.Count-1 do begin
    m := n.AddChild(sValue);
    m.Text := Values[i];
  end;

  Result := Doc.DocumentElement.XML;
end;


/////////////////////////////////////////
// ������ ������ TFuzzyType
/////////////////////////////////////////

constructor TFuzzyType.Create(ACollection: TCollection);
begin
  inherited;
  MFList := TList.Create;
  Units := TList.Create;
  Synonyms := TStringList.Create;
end;

destructor TFuzzyType.Destroy;
begin
  Synonyms.Free;
  Units.Free;
  MFList.Free;
  inherited;
end;

function TFuzzyType.GetXML: {Wide}String;
var
  Doc, Child: IXMLDocument;
  Parameter: IXMLNode;
  i: integer;
  MF: TMembershipFunction;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sType+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sMeta] := sFuzzy;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  // ������� ��������������
  for i:=0 to MFList.Count-1 do begin
    MF := MFList[i];
    Parameter := Doc.DocumentElement.AddChild(sParameter);
    Parameter.Attributes[sMinValue] := MF.XMin;
    Parameter.Attributes[sMaxValue] := MF.XMax;
    Parameter.AddChild(sValue);
    Parameter.ChildNodes[sValue].Text := MF.MFName;
    Child.LoadFromXML(MF.XML);
    Parameter.ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  // ������� ���������

  Result := Doc.DocumentElement.XML;
end;


/////////////////////////////////////////
// ������ ������ TReferenceType
/////////////////////////////////////////


/////////////////////////////////////////
// ������ ������ TCOMWrapper
/////////////////////////////////////////

function TCOMWrapper.GetXML: {Wide}String;
var Doc: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sClass+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sExt] := sTrue;

  if AncestorID <> '' then
    Doc.DocumentElement.Attributes[sAncestor] := AncestorID;

  Doc.DocumentElement.Attributes[sDesc] := Desc;

  Doc.DocumentElement.AddChild(sProgID);
  Doc.DocumentElement.ChildNodes[sProgID].Text := ProgID;

  Result := Doc.DocumentElement.XML;
end;

function TCOMWrapper.CreateInstance: TComponent;
begin
  Result := inherited CreateInstance;
  Connect;
end;

function TCOMWrapper.Connect: boolean;
begin
  try
    Obj := CreateOleObject(ProgID);
    Result := True;
  except
    Result := False;
  end
end;

function TCOMWrapper.OleMethod(MethodName: WideString; var Params: DISPPARAMS): OleVariant;
var
  MethodDispID: integer;
begin
  Obj.GetIDsOfNames(GUID_NULL, @MethodName, 1, LOCALE_USER_DEFAULT, @MethodDispID);
  Obj.Invoke(MethodDispID, GUID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, Params, nil, nil, nil);
end;

function TCOMWrapper.OlePropertyGet(PropertyName: WideString; var Params: DISPPARAMS): OleVariant;
var
  PropertyDispID: integer;
  VarResult: TVarData;
begin
  Obj.GetIDsOfNames(GUID_NULL, @PropertyName, 1, LOCALE_USER_DEFAULT, @PropertyDispID);
  Obj.Invoke(PropertyDispID, GUID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, Params, @VarResult, nil, nil);
end;

procedure TCOMWrapper.OlePropertySet(PropertyName: WideString; var Params: DISPPARAMS);
var
  PropertyDispID: integer;
begin
  Obj.GetIDsOfNames(GUID_NULL, @PropertyName, 1, LOCALE_USER_DEFAULT, @PropertyDispID);
  Obj.Invoke(PropertyDispID, GUID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, Params, nil, nil, nil);
end;

function TCOMWrapper.Invoke(ProcName: WideString; Params: TList): boolean;
begin
  // �� TList of TValue ������� DISPPARAMS
  Result := True;
end;


end.
