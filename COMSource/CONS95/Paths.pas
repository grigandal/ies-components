unit Paths;

interface

uses SysUtils, VCL.Forms, VCL.Dialogs;

const

  FilePath     =  'Lingv\';
  ResPath      =  'Cons95\';

var
  DefaultPath: String  =  '';

  function GetFilePath(s: String): String;

implementation
{$G+}
  function GetFilePath(s: String): String;
  var
    s1,s2: array[0..255] of Char;
    p: Integer;
    OpenDialog: TOpenDialog;
    res: String;
  begin

    StrPCopy(s1,ExtractFilePath(Application.ExeName));
    StrPCopy(s2,s);
    if (Pos('DELPHI',UpperCase(StrPas(s1)))>0) then begin
      while DefaultPath='' do begin
        OpenDialog:=TOpenDialog.Create(Application);
        OpenDialog.Title:='����� ��������������� ���� ��� '+s;
        OpenDialog.FileName:='*.*';
        if OpenDialog.Execute then begin
          DefaultPath:=ExtractFilePath(OpenDialog.FileName);
        end;
        OpenDialog.Destroy;
      end;
      StrPCopy(s1,DefaultPath);
//      StrCat(s1,s2);
    end else
      StrCat(s1,s2);
    Res:=StrPas(s1);
    GetFilePath:=Res;
  
  end;

end.
