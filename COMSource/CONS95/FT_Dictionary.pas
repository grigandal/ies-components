//**************************************************************************//
//*                                                                        *//
//*  ������, ���������� �������� ������, ����������� �� ������ ���������-  *//
//*  ������ ���������� ��� ������������ ������� ��.                        *//
//*  ����������� � ���� ����������.                                        *//
//*                                                                        *//
//*  ������:                                                               *//
//*          TMFDictionaryComponent,                                       *//
//*          TMFDictionary_Editor,                                         *//
//*          TDictionary_Creator                                           *//
//*                                                                        *//
//*  �����: ������ �.�.                                                    *//
//*  ������������: ������� �.�.                                             *//
//*  ������: 2.1                                                           *//
//*  ���� ������ ����������: 03.12.99                                      *//
//*                                                                        *//
//**************************************************************************//
unit FT_Dictionary;

//----------------------------------------------------------------------------
interface

{$INCLUDE Defs.inc}

uses
{$IFDEF _DESIGNTIME}
  DesignEditors,
{$ENDIF}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {Designintf,} Buttons, ExtCtrls, StdCtrls,
  editLV,
  MSXML_TLB,
  TypInfo,
  KBCompon,
  Paths,
  LVstr,    // class  TLinguisticVariable ��������������� ����������
  KB_Fuzzy; // class TMembershipFunction ������� ��������������
            // class TFuzzyPoint �����

//**************************************************************************//
//*                                                                        *//
//*  ����� TMFDictionaryComponent ������������ ��� �������� ������� �� �   *//
//*  ���� ����������.                                                      *//
//*                                                                        *//
//**************************************************************************//
type TMFDictionaryComponent = class (TAtDictionary)
  private
    FFileName: String;
  public
    FMFTypes:   TMFTypes; { ���� ������� �������������� }
    FListLV:    TKBOwnedCollection; { ������ ��������������� ����������}
    function Edit: Integer;
    function EditMFTypes: Integer;
    procedure Save;   // ������ ������� � ����.
    procedure Load;   // ���������� ������� �� �����.
    procedure LoadXML(XML: String);
    function GetXML: String;
//    procedure GoodAssign (iValue : TMFDictionary); // ����������� �������� ���� � �����.
    function FindLVbyName (LVName : string) : TLinguisticVariable; // ����� � ������� LV �� �����.
    function FindMFbyName (LVName,MFName : string) : TMembershipFunction; // ����� � ������� �� �� �����.
    function FindMFTypebyName (MFName : string) : TMembershipFunction; // ����� � ������� �� �� �����.
    constructor Create (AOwner: TComponent); override; // �����������.
    destructor Destroy; override;                      // ����������.

//    procedure StoreDictionary (iValue : TMFDictionary); // ������ �������.
  published
//    property Dictionary : TMFDictionary read GetDictionary write StoreDictionary; // ��������, ���������� �� ������� ��.
    property FileName : string read FFileName write FFileName; // ��������, ���������� �� ��� ����� �� ��������.
  property ListLV: TKBOwnedCollection read  FListLV write FListLV;
  property MFTypes: TMFTypes read FMFTypes write FMFTypes;
end;

//**************************************************************************//
//*                                                                        *//
//*  ����� TMFDictionaryEditor ������������ ��� �������� ��������� ������  *//
//*  ��������������� ����������.                                           *//
//*                                                                        *//
//**************************************************************************//
{$IFDEF _DESIGNTIME}
type TMFDictionaryEditor = class (TComponentEditor)
  public
    procedure Edit; override;                          // ��������� �������������� ����������.
    procedure ExecuteVerb (Index : integer); override; // ��������� ������� �������������� ���������� �� ������� ������ ������ ����.

    function GetVerb (Index : integer) : string; override; // �� ����, ����� ������ ����� ��� �������.
    function GetVerbCount : integer; override;             // ���� �� �����-�� ����� �����.
end;
{$ENDIF}
//**************************************************************************//
//*                                                                        *//
//*  ����� TDictionary_Creator ��������� ���������� ���� ��������� �����-  *//
//*  ����� ��� ������ ������� ��.                                          *//
//*                                                                        *//
//**************************************************************************//
type TDictionary_Creator = class (TForm)
    //*** ���� ***//
    Btn_Ok       : TBitBtn;       // ������ "��".
    Btn_Cancel   : TBitBtn;       // ������ "������".
    ToolBar      : TPanel;        // ������ ������������.
    ActiveBar    : TPanel;        // ������� ������.
    Btn_Plus     : TSpeedButton;  // ������ ���������� ��������������� ���������� (��).
    Btn_Minus    : TSpeedButton;  // ������ �������� ��.
    Btn_Save     : TSpeedButton;  // ������ ������ ������� ��.
    Btn_Open     : TSpeedButton;  // ������ �������� ����� �������.
    Btn_View     : TSpeedButton;  // ������ ��������������.
    Lbl_Name     : TLabel;        // "��� (�������)".
    Lbl_LVList   : TLabel;        // "������ ��".
    Lbl_LV       : TLabel;        // "��� ��".
    Lbl_MFList   : TLabel;        // "������ ��".
    Txt_Name     : TEdit;         // ��� �������.
    Txt_Lingv    : TEdit;         // ��� ������� �� - ������ ������.
    Ling_LVList  : TListBox;      // ������ ��.
    Ling_MFList  : TListBox;      // ������ ��.
    Dlg_Open     : TOpenDialog;   // ���������� ���� �������� �������.
    Dlg_Save     : TSaveDialog;   // ���������� ���� ������ �������.

    //*** ����������� ������� ***//
    procedure Btn_OpenClick (Sender : TObject);
    procedure Btn_SaveClick (Sender : TObject);
    procedure Btn_PlusClick (Sender : TObject);
    procedure Ling_LVListClick (Sender : TObject);
    procedure Btn_MinusClick (Sender : TObject);
    procedure Btn_ViewClick (Sender : TObject);
    procedure Txt_NameChange (Sender : TObject);

  public
    FListLV:    TKBOwnedCollection;  // ������ �� �������
    fEditor     : TeditLVForm;      // �������� ��� ����������� ���������� ����� ��������������� ����������.
    FMFDictionaryComponent: TMFDictionaryComponent;


    procedure RedrawList; // ��������� ���������� ������ ��������������� ����������.

end;

//*** ��������� � ������� ***//
procedure Register;

//----------------------------------------------------------------------------
implementation
{$G+}
{$R FT_Dictionary.dcr}
{$R *.DFM}

procedure Register;
begin
  RegisterComponents ('Fuzzy Tools', [TMFDictionaryComponent]);
{$IFDEF _DESIGNTIME}
  RegisterComponentEditor (TMFDictionaryComponent, TMFDictionaryEditor);
{$ENDIF}
end;

//*** TMFDictionaryComponent ***//

{procedure TMFDictionaryComponent.GoodAssign (iValue : TMFDictionary);
var
  i           : Integer;      // ������� ��� ����� For.
  Temp_MFList : TMFListOwner; // ��������� ���������� ��� ������� � ��������� ������-�����������.
  ValueOwner  : TMFListOwner; // ��������� ���������� ��� ������� � ��������� ������-���������.
begin
  if (iValue.Count>0) then begin                   // ���� � ��� ���� ���� �� ���� ������� � ������-���������, ��...
    Clear;                                         //   �������� ������-����������.
    for i:=0 to iValue.Count-1 do begin            //   ��� ���� ��������� ������-���������...
      Temp_MFList:=Add as TMFListOwner;            //     ��������� ����� ������� � ������-����������.
      ValueOwner:=iValue.Items[i] as TMFListOwner; //     ������� ��������� ������� � ������-���������.
      Temp_MFList.GoodAssign (ValueOwner.MFList);  //     �������� �������� ���� � �����.
    end;
    FileName:=iValue.FileName;                     // �������� �����.
  end;
end;}

function TMFDictionaryComponent.FindLVbyName (LVName : string) : TLinguisticVariable;
var
  i           : integer;      // ������� ��� ����� For.
  j           : integer;      // ������� ��� ����� For.
  Temp_LV     : TLinguisticVariable; // ��������� ���������� ��� ������� � ��������� �������.
  Temp_MF     : TMembershipFunction;     // ��������� ���������� ��� ������� � ���-����������.
begin
  with FListLV do begin
    if (Count<>0) then begin                                       // ���� � ��� ���� ��������������� ����������, ��...
      for i:=0 to Count-1 do begin                                 //   ��� ���� ��������������� ����������...
        Temp_LV:=Items[i] as TLinguisticVariable;                  //     ������� ��������� ��������������� ����������.
        if (Temp_LV.NameLV=LVName ) then begin
          FindLVbyName:=Temp_LV;                                     //           ���������� ��������� �� ��� ��.
          Exit;                                                    //           ����������� ������.
        end;
      end;
    end;
  end;
  FindLVbyName:=nil;                                               // ���� ������ �� �����, �� ���������� ������ ���������.
end;

function TMFDictionaryComponent.FindMFbyName (LVName,MFName : string) : TMembershipFunction;
var
  i           : integer;      // ������� ��� ����� For.
  j           : integer;      // ������� ��� ����� For.
  Temp_LV     : TLinguisticVariable; // ��������� ���������� ��� ������� � ��������� �������.
  Temp_MF     : TMembershipFunction;     // ��������� ���������� ��� ������� � ���-����������.
begin
  with FListLV do begin
    if (Count<>0) then begin                                       // ���� � ��� ���� ��������������� ����������, ��...
      for i:=0 to Count-1 do begin                                 //   ��� ���� ��������������� ����������...
        Temp_LV:=Items[i] as TLinguisticVariable;                  //     ������� ��������� ��������������� ����������.
        if (Temp_LV.NameLV='') or (Temp_LV.NameLV=LVName ) then begin
          if (Temp_LV.MFList.Count<>0) then begin                  //     ���� � ��� ���� ����-���������, ��...
            for j:=0 to Temp_LV.MFList.Count-1 do begin            //       ��� ���� ����-��������...
              Temp_MF:=Temp_LV.MFList.Items[j];                    //         ������� ��������� ��.
              if (Temp_MF.mfName=MFName) then begin                //         ���� �� ��� ����� ���������, ��...
                FindMFbyName:=Temp_MF;                             //           ���������� ��������� �� ��� ��.
                Exit;                                              //           ����������� ������.
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  FindMFbyName:=nil;                                               // ���� ������ �� �����, �� ���������� ������ ���������.
end;

function TMFDictionaryComponent.FindMFTypebyName (MFName : string) : TMembershipFunction;
var
  j           : integer;      // ������� ��� ����� For.
  Temp_MF     : TMembershipFunction;     // ��������� ���������� ��� ������� � ���-����������.
begin
  if (FMFTypes.MFList.Count<>0) then begin                  //     ���� � ��� ���� ����-���������, ��...
    for j:=0 to FMFTypes.MFList.Count-1 do begin            //       ��� ���� ����-��������...
      Temp_MF:=FMFTypes.MFList.Items[j];                    //         ������� ��������� ��.
      if (Temp_MF.mfName=MFName) then begin                 //         ���� �� ��� ����� ���������, ��...
        FindMFTypebyName:=Temp_MF;                          //           ���������� ��������� �� ��� ��.
        Exit;                                               //           ����������� ������.
      end;
    end;
  end;
  FindMFTypebyName:=nil;                                               // ���� ������ �� �����, �� ���������� ������ ���������.
end;

constructor TMFDictionaryComponent.Create (AOwner: TComponent);
begin
  inherited Create (AOwner); // �������� �������������� �����������.
  FMFTypes:=TMFTypes.Create(Nil); // ������� ������ �����
  FMFTypes.NameLV:='������� ������� ��������������';
  FListLV:=TKBOwnedCollection.Create(Self,TLinguisticVariable);
      // ������� ������� � ����� ��������� TLinguisticVariable.
end;

destructor TMFDictionaryComponent.Destroy;
begin
  FListLV.Destroy;  // ������� �������.
  FMFTypes.Destroy; // ������� ������ �����
  inherited Destroy; // �������� �������������� ����������.
end;

function TMFDictionaryComponent.Edit: Integer;
var
  CreatorForm    : TDictionary_Creator; // ����� ��� ��������� ����������.
begin
  Result:=0;
  CreatorForm:=TDictionary_Creator.Create (Application);
  CreatorForm.FMFDictionaryComponent:=Self;
  CreatorForm.RedrawList;
  if (CreatorForm.ShowModal=mrOk) then begin
     CreatorForm.Free;
  end
  else begin
    CreatorForm.Free;
  end;
end;

function TMFDictionaryComponent.EditMFTypes: Integer;
var
  fEditor: TeditLVForm;
begin
  Result:=0;
  fEditor:=TeditLVForm.Create (Application);                                      // ������� ���������� ���� ��������� ��������������� ����������.
  fEditor.edtName.Text:=MFTypes.NameLV;
  fEditor.LV:=MFTypes;

  fEditor.Left:=fEditor.Left+10;                                                   // �������� ��� �� 10 �������� ������.
  fEditor.Top:=fEditor.Top+10;                                                     // �������� ��� �� 10 �������� ����.

  fEditor.ShowModal;                                                               // ���������� ��� ������������.
  fEditor.Free;                                                                    // ������� ���������� ����.
end;

procedure TMFDictionaryComponent.Save;   // ������ ������� � ����.
var
  X: IXMLDOMDocument;
  S: String;
begin
  X:=CoDOMDocument.Create;
  S:=GetXML;
  X.LoadXML(S);
  X.save(FFileName);
end;

function TMFDictionaryComponent.GetXML: String;
var
  X,X1: IXMLDOMDocument;
  Element, PropElement{, Child}: IXMLDOMElement;
  j: Integer;
  Collect: TCollection;
  CollectItem: TCollectionItem;
  PropInfo: PPropInfo;
  GotXMLString: String;
begin
  X:=CoDOMDocument.Create;
  X1:=CoDOMDocument.Create;
  Element:=X.createElement(Self.ClassName);
  X.appendChild(Element);
  
  {������ ����� ������� ��������������}
  PropElement:=X.createElement('MFTypes');
  Element.appendChild(PropElement);
  GotXMLString:=FMFTypes.GetXML;
  X1.loadXML(GotXMLString);
  PropElement.appendChild(X1.documentElement);

 { ������ ��������������� ����������}
  PropElement:=X.createElement('ListLV');
  Element.appendChild(PropElement);
  PropInfo:=GetPropInfo(Self.ClassInfo,'ListLV');
  Collect:=TCollection(VarToObj(GetOrdProp(Self,PropInfo)));
  for j:=1 to Collect.Count do begin
    if Collect.Items[j-1] is TCollectionItem then begin
      CollectItem:=TCollectionItem(Collect.Items[j-1]);
      GotXMLString:='';
      if CollectItem is TLinguisticVariable then
        GotXMLString:=TLinguisticVariable(CollectItem).GetXML;
      if GotXMLString<>'' then begin
        X1.loadXML(GotXMLString);
        PropElement.appendChild(X1.selectSingleNode(CollectItem.ClassName));
      end;
    end;
  end;
  Result:=X.xml;
end;

procedure TMFDictionaryComponent.Load;   // ���������� ������� �� �����.
var
  X: IXMLDOMDocument;
begin
  if FileExists(FileName) then begin
    X:=CoDOMDocument.Create;
    X.load(FFileName);
    LoadXML(X.xml);
  end else
    ShowMessage('����' + FileName + ' �� ������!');
end;

procedure TMFDictionaryComponent.LoadXML(XML: String);
var
  PropInfo: PPropInfo;
  AProp: Variant;
  APropStr: String;
  X1: IXMLDOMDocument;
  Element, AttributeItem, PropElement: IXMLDOMNode;
  ElementAttributes: IXMLDOMNamedNodeMap;
  Nodes, ElementNodes: IXMLDOMNodeList;
  i,j: Integer;
  PersistentClass: TClass;
  Collect: TCollection;
begin
  X1:=CoDOMDocument.Create;
  X1.LoadXML(XML);
  Element:=X1.documentElement;
  if Element=NIL then Exit;
  if Element.nodeName=Self.ClassName then begin
    ElementNodes:=Element.childNodes;
    for i:=1 to ElementNodes.length do begin
      AttributeItem:=ElementNodes.item[i-1];
      PropInfo:=GetPropInfo(Self.ClassInfo,AttributeItem.nodeName);
      APropStr:=AttributeItem.text;
      AProp:=APropStr;
      case PropInfo^.PropType^.Kind of
          tkClass: begin
            PersistentClass:=FindClass(PropInfo^.PropType^.Name);
            Element:=ElementNodes.item[i-1];
            if Element.nodeName='MFTypes' then begin
              FMFTypes.LoadXML(Element.firstChild.xml);
            end else
            if PersistentClass.ClassNameIs('TKBCollection') or
              PersistentClass.ClassNameIs('TKBOwnedCollection') then begin
              Collect:=TCollection(VarToObj(GetOrdProp(Self,PropInfo)));
              Collect.Clear;
              Nodes:=AttributeItem.selectNodes(Collect.ItemClass.ClassName);
              for j:=1 to Nodes.length do begin
                PropElement:=Nodes.item[j-1];
                TLinguisticVariable.LoadXML(PropElement.xml,Collect);
              end;
            end;
          end;
      end;
    end;
  end;
end;

//*** TMFDictionaryEditor ***//
{$IFDEF _DESIGNTIME}
procedure TMFDictionaryEditor.Edit;
begin
  MFDictionaryComponent(Self.Component).Edit;
end;
{$ENDIF}

//*** TDictionary_Creator ***//

procedure TDictionary_Creator.Btn_OpenClick (Sender : TObject);
var
  DictFile : TextFile; // ���������� ����� �������.
begin
  Dlg_Open.FileName:=FMFDictionaryComponent.FFileName;
  if (Dlg_Open.Execute) then begin            // ���� ��� ������ ����, ��...
    FMFDictionaryComponent.FFileName:=Dlg_Open.FileName;
    FMFDictionaryComponent.Load;
    RedrawList;                               //   �������������� ������ ��������������� ����������.
  end;
end;

procedure TDictionary_Creator.Btn_SaveClick (Sender : TObject);
var
  DictFile : TextFile; // ���������� ����� �������.
begin
  Dlg_Save.FileName:=FMFDictionaryComponent.FFileName;
  if (Dlg_Save.Execute) then begin            // ���� ��� ������ ����, ��...
    FMFDictionaryComponent.FFileName:=Dlg_Save.FileName;
    FMFDictionaryComponent.Save;
  end;
end;

procedure TDictionary_Creator.RedrawList;
var
  i           : integer;         // ������� ��� ����� For.
  Temp_LV : TLinguisticVariable; // ��������� ���������� ��� ������� � ��������� �������.
begin
//  Txt_Name.Text:=FMFDictionaryComponent.FFileName;                     // ���������� � ��������������� ���� ��� �������.
  if (FMFDictionaryComponent.FListLV.Count<>0) then begin                     // ���� ������� �� ����, ��...
    Ling_LVList.Clear;                                     //   ������� ������ ��������������� ����������.
    for i:=0 to FMFDictionaryComponent.FListLV.Count-1 do begin               //   ��� ���� ��������������� ���������� �������...
      Temp_LV:=FMFDictionaryComponent.FListLV.Items[i] as TLinguisticVariable;   //     ������� ��������� ��������������� ����������.
      Ling_LVList.Items.Add (Temp_LV.NameLV); //     ���������� �� ��� � ������.
    end;
  end;
end;

procedure TDictionary_Creator.Btn_PlusClick (Sender : TObject);
var
  PtrLV: TLinguisticVariable;
begin
  Btn_Minus.Enabled:=False;
  Btn_View.Enabled:=False;

  PtrLV:=FMFDictionaryComponent.FListLV.Add as TLinguisticVariable;
  PtrLV.NameLV:=Txt_Name.Text;

  fEditor:=TeditLVForm.Create (Application);                                      // ������� ���������� ���� ��������� ��������������� ����������.
  fEditor.edtName.Text:=PtrLV.NameLV;
  fEditor.LV:=PtrLV;

  fEditor.Left:=fEditor.Left+10;                                                   // �������� ��� �� 10 �������� ������.
  fEditor.Top:=fEditor.Top+10;                                                     // �������� ��� �� 10 �������� ����.
  fEditor.ShowModal;                                                               // ���������� ��� ������������.
//  if (fEditor.ModalResult=mrOk) then begin                                         // ���� ������������ ����� ������ "��", ��...
//  RedrawList;
//  end;
  RedrawList;
  fEditor.Free;                                                                    // ������� ���������� ����.
end;

procedure TDictionary_Creator.Ling_LVListClick (Sender : TObject);
var
  i              : integer;      // ������� ��� ����� For.
  Temp_ListOwner : TLinguisticVariable; // ��������� ���������� ��� ������� � ��������������� ����������.
  Temp_MFOwner   : TMembershipFunction;     // ��������� ���������� ��� ������� � �������� ��������������.
begin
  if (Ling_LVList.ItemIndex<>-1) then begin                                   // ���� ���� ���������� �������, ��...
    Btn_Minus.Enabled:=True;                                                  //   ������ ��������� ������ ��������.
    Btn_View.Enabled:=True;                                                   //   ������ ��������� ������ ��������������.
    Temp_ListOwner:=FMFDictionaryComponent.FListLV.
    Items[Ling_LVList.ItemIndex] as TLinguisticVariable;                                                    //   ������� ���������� ��������������� ����������.
    Txt_Lingv.Text:=Temp_ListOwner.NameLV;                                    //   ������ �� ��� � ��������������� ����.
    if (Temp_ListOwner.MFList.Count<>0) then begin                            //   ���� ������ �� �� �� ����, ��...
      Ling_MFList.Clear;                                                      //     ������� ������ �� � ����.
      for i:=0 to Temp_ListOwner.MFList.Count-1 do begin                      //     ��� ���� ��...
        Temp_MFOwner:=TMembershipFunction(Temp_ListOwner.MFList.Items[i]);
        Ling_MFList.Items.Add (Temp_MFOwner.MFName);                          //     ����� �� ��� � ����.
      end;
    end;
  end;
end;

procedure TDictionary_Creator.Btn_MinusClick (Sender : TObject);
begin
  if (Ling_LVList.ItemIndex<>-1) then begin                                // ���� ���� ���������� �������, ��...
    FMFDictionaryComponent.FListLV.Delete(Ling_LVList.ItemIndex);
    Txt_Lingv.Text:='';                                                    //   ������� �� ��� �� ���� �����.
    Ling_MFList.Clear;                                                     //   ������� ������ �� ��.
    Btn_Minus.Enabled:=False;                                              //   ������ ����������� ������ ��������.
    Btn_View.Enabled:=False;                                               //   ������ ����������� ������ ��������������.
    Ling_LVList.ItemIndex;
    RedrawList;
  end;
end;

procedure TDictionary_Creator.Btn_ViewClick (Sender : TObject);
var
  PtrLV: TLinguisticVariable;
begin
  if Ling_LVList.ItemIndex<0 then Exit;
  Btn_Minus.Enabled:=False;
  Btn_View.Enabled:=False;

  fEditor:=TeditLVForm.Create (Application);                                      // ������� ���������� ���� ��������� ��������������� ����������.
  PtrLV:=FMFDictionaryComponent.FListLV.Items[Ling_LVList.ItemIndex]
    as TLinguisticVariable;
  fEditor.edtName.Text:=PtrLV.NameLV;
  fEditor.LV:=PtrLV;

  fEditor.Left:=fEditor.Left+10;                                                   // �������� ��� �� 10 �������� ������.
  fEditor.Top:=fEditor.Top+10;                                                     // �������� ��� �� 10 �������� ����.
  fEditor.ShowModal;                                                               // ���������� ��� ������������.
  if (fEditor.ModalResult=mrOk) then begin                                         // ���� ������������ ����� ������ "��", ��...
    RedrawList;
  end;
  fEditor.Free;                                                                    // ������� ���������� ����.
end;

procedure TDictionary_Creator.Txt_NameChange (Sender : TObject);
begin
//  FMFDictionaryComponent.FileName:=Txt_Name.Text; // ������ ����������� ���.
end;

//----------------------------------------------------------------------------
end.
