unit editLV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, editMF, Menus, Buttons, ExtCtrls,
  KB_Fuzzy, TeeProcs, TeEngine, Chart, Series, LVstr,
  ComCtrls;

type
  TeditLVForm = class(TForm)
    edtName: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtLow: TEdit;
    edtHigh: TEdit;
    Label5: TLabel;
    btnAdd: TButton;
    btnChange: TButton;
    btnDel: TButton;
    lbLVvalue: TListBox;
    btnOK: TButton;
    btnCancel: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    sbError: TStatusBar;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Save1: TMenuItem;
    Load1: TMenuItem;
    Bevel1: TBevel;
    Clear1: TMenuItem;
    Bevel2: TBevel;
    Bevel3: TBevel;
    procedure lbLVvalueClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnChangeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Draw();
    procedure CheckValues(Sender: TObject);
    procedure Load1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure Clear1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ShowLV;
//    procedure ShowMF;
 
  private
    { Private declarations }
  public
    { Public declarations }
    editMFForm: TeditMFForm;
    LV : TLinguisticVariable;
    Flag: Boolean;
  end;

var
  editLVForm: TeditLVForm;

implementation

{$R *.dfm}

procedure TeditLVForm.lbLVvalueClick(Sender: TObject);
begin
  if lbLVvalue.ItemIndex <> -1 then
  begin
    btnChange.Enabled:= True;
    btnDel.Enabled   := True;
    Draw();
  end
  else
  begin
    btnChange.Enabled:= False;
    btnDel.Enabled   := False;
  end;
end;

procedure TeditLVForm.btnAddClick(Sender: TObject);
begin
 Application.CreateForm(TeditMFForm, editMFForm);
 editMFForm.editLVFormPtr:=Self;
 editMFForm.MF:=TMembershipFunction.Create(NIL);
 LV.mfList.Add(editMFForm.MF);
// ShowMF;
 try
  if (edtLow.Text<>'') and (edtHigh.Text<>'') then
  begin
    if StrToFloat(edtLow.Text)>editMFForm.Chart1.BottomAxis.Maximum then
    begin
      editMFForm.Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
      editMFForm.Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
    end
    else
    begin
      editMFForm.Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
      editMFForm.Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
    end;
  end
  else
  begin
    sbError.SimpleText:='������� �������� ������� �����������';
    exit;
  end;
 except
    exit;
 end;
  editMFForm.Series1.Clear;
  editMFForm.Series2.Clear;
  editMFForm.ShowModal;
  editMFForm.Destroy;
end;

procedure TeditLVForm.btnCancelClick(Sender: TObject);
begin
  try
//    LV.Free;
  except
  end;
  Close;
end;

procedure TeditLVForm.btnDelClick(Sender: TObject);
var
  numbofpoint : Integer;
begin
  numbofpoint :=lbLVvalue.ItemIndex;
  LV.mfList.Delete(lbLVvalue.ItemIndex);
  lbLVvalue.Items.Delete(lbLVvalue.ItemIndex);
  lbLVvalue.ItemIndex:=numbofpoint;
  if lbLVvalue.ItemIndex = -1 then
  begin
    btnDel.Enabled:=False;
    btnChange.Enabled:=False;
    Series1.Clear;
  end
  else
    Draw();
end;

procedure TeditLVForm.btnOKClick(Sender: TObject);
begin
  try
//    LV.Free;
  except
  end;
  LV.NameLV := edtName.Text;
  LV.dmin:=StrToInt(edtLow.Text);
  LV.dmax:=StrToInt(edtHigh.Text);
  Close;
end;

procedure TeditLVForm.btnChangeClick(Sender: TObject);
var
  k : Integer;
begin
 Application.CreateForm(TeditMFForm, editMFForm);
 editMFForm.editLVFormPtr:=Self;
// ShowMF;
 try
  if StrToFloat(edtLow.Text)>editMFForm.Chart1.BottomAxis.Maximum then
  begin
    editMFForm.Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
    editMFForm.Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
  end
  else
  begin
    editMFForm.Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
    editMFForm.Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
  end;
  editMFForm.MF:=LV.mfList.Items[lbLVvalue.ItemIndex];
  editMFForm.edtNameMF.Text:=editMFForm.MF.MFName;
  for k:=0 to editMFForm.MF.Points.Count-1 do
  begin
    editMFForm.lbPointList.Items.Add('X = ' + FloatToStr(TFuzzyPoint(editMFForm.MF.Points.Items[k]).X) + '; Y = ' + FloatToStr(TFuzzyPoint(editMFForm.MF.Points.Items[k]).Y));
  end;
 except
//  ShowMessage('Error, send msg to developer');
 end;
 editMFForm.ShowModal;
 editMFForm.Destroy;
end;

procedure TeditLVForm.FormCreate(Sender: TObject);
begin
//  Application.CreateForm(TeditMFForm, editMFForm);
  LV:=nil;
  Flag:=False;
end;

procedure TeditLVForm.Draw();
var
  i : Integer;
  CurrMF: TMembershipFunction;
begin
  Series1.Clear;
//  editMFForm.MF:=LV.mfList.Items[lbLVvalue.ItemIndex];
  if (lbLVvalue.ItemIndex>=0) and (LV.mfList.Count>lbLVvalue.ItemIndex) then begin
    CurrMF:=LV.mfList.Items[lbLVvalue.ItemIndex];
    for i:=0 to CurrMF.Points.Count-1 do
    begin
      Series1.AddXY(TFuzzyPoint(CurrMF.Points.Items[i]).X,TFuzzyPoint(CurrMF.Points.Items[i]).Y);
    end;
  end;
end;

procedure TeditLVForm.CheckValues;
begin
  sbError.SimpleText:='';
  try
  if (edtHigh.Text<>'') or (edtLow.Text<>'') then
  begin
    if (edtLow.Text<>'') then
    try
      StrToFloat(edtLow.Text);
    except
      sbError.SimpleText:='������� �� ���������� �������� ������ �������';
      exit;
    end;
    if (edtHigh.Text<>'') then
    try
      StrToFloat(edtHigh.Text);
    except
      sbError.SimpleText:='������� �� ���������� �������� ������� �������';
      exit;
    end;
    if StrToFloat(edtLow.Text)>StrToFloat(edtHigh.Text) then
    begin
      sbError.SimpleText:='�������� ������ ������� ������ �������� �������';
    end
    else
    begin
      if StrToFloat(edtLow.Text)>Chart1.BottomAxis.Maximum then
      begin
        Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
        Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
      end
      else
      begin
        Chart1.BottomAxis.Minimum:=StrToFloat(edtLow.Text);
        Chart1.BottomAxis.Maximum:=StrToFloat(edtHigh.Text);
      end;
    end;
  end;
  except
  end;
end;


//======================================================

procedure TeditLVForm.Save1Click(Sender: TObject);
begin
  if edtName.Text<>'' then
    LV.NameLV:=edtName.Text
  else
  begin
    Showmessage('������� ��� ��');
    exit;
  end;
  if (edtLow.Text<>'') and (edtHigh.Text<>'') then
  begin
    LV.dMin:=StrToInt(edtLow.Text);
    LV.dMax:=StrToInt(edtHigh.Text);
  end
  else
  begin
    ShowMessage('������� �������� ������� �����������');
    exit;
  end;
  SaveDialog1.InitialDir:= ExtractFilePath(Application.ExeName);
  if SaveDialog1.Execute then
  begin
    LV.Save(SaveDialog1.FileName);
  end;

end;

procedure TeditLVForm.ShowLV;
var
  i : Integer;
begin
  Series1.Clear;
  lbLVvalue.Clear;
  for i:=0 to LV.mfList.Count-1 do
  begin
    lbLVvalue.Items.Add(TMembershipFunction(LV.mfList.Items[i]).MFName);
  end;
  edtName.Text:=LV.NameLV;
  edtLow.Text:=IntToStr(LV.dMin);
  edtHigh.Text:=IntToStr(LV.dMax);
  Chart1.BottomAxis.Maximum:=StrToFloat(IntToStr(LV.dMax));
  Chart1.BottomAxis.Minimum:=StrToFloat(IntToStr(LV.dMin));
end;

procedure TeditLVForm.Load1Click(Sender: TObject);
begin
  if LV.mfList.Count<>0 then
    LV.Clear;
  OpenDialog1.InitialDir:=ExtractFilePath(Application.ExeName);
  if OpenDialog1.Execute then
    LV.Load(OpenDialog1.FileName)
  else
  begin
    exit;
  end;
  ShowLV;
end;


procedure TeditLVForm.Clear1Click(Sender: TObject);
begin
  if LV.mfList.Count<>0 then
    LV.mfList.Clear;
  edtName.Text:='';
  edtLow.Text:='';
  edtHigh.Text:='';
  lbLVvalue.Clear;
  Series1.Clear;
end;

procedure TeditLVForm.FormDestroy(Sender: TObject);
begin
//  editMFForm.Destroy;
  if Flag then
    LV.Destroy;
end;

procedure TeditLVForm.FormShow(Sender: TObject);
begin
  if not Assigned(LV) then begin
    LV:=TlinguisticVariable.Create(nil);
    Flag:=True;
  end else
    ShowLV;
end;

{procedure TeditLVForm.ShowMF;
var
  i: Integer;
begin
  for i:=0 to LV.mfList.Count-1 do
  begin
    editMFForm.MF:=LV.mfList.Items[i];
    lbLVvalue.Items.Add(editMFForm.MF.MFName);
  end;
end;}

end.
