unit Solve;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes,
  Dialogs, Aspr95,  DmpShf, Paths, Calc, KbTypes;
  //DesignIntf;

  {$R Solve.res}

type
  TRuleState=class(TObject)
    ptr_rule: record_rule;
  end;

  TRulesList = class (TList);

  TSolve=class(TInterpreter)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
      {FKB: TKnowledgeBase;}
      Stop: Boolean;
      procedure StartSolve;
      function Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject; override;
      procedure MakeConsult(KB: TKnowledgeBase;
                      GoalsList: TList;
                      Style: Integer);
      constructor Create(AOwner: TComponent); override;
      procedure Verify(Protocol: TStrings); override;
    published
      {property KB: TKnowledgeBase read FKB write FKB;}
  end;

  TExternalInterpreter=class(TInterpreter)
  public
    FExternal: TExternal{TPersistent};
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject; override;
  published
    property External: TExternal{TPersistent} read FExternal write FExternal;
  end;


{  TSolveEditor = class(TComponentEditor)
    protected

    public
      procedure Edit; override;
  end;
}

procedure StartSolve(Detachment: TKBControl; var Stop: Boolean); far; export;
procedure Consult(KB: TKnowledgeBase; var Stop: Boolean); far; export;
procedure MakeConsult(KB: TKnowledgeBase;
                      GoalsList: TList;
                      Style: Integer; var Stop: Boolean); far; export;

procedure Register;

implementation

uses ESKernel;

procedure Register;
begin
  RegisterComponents('KB Access', [TSolve, TExternalInterpreter]);
  KBTypes._RegistryMethod(TSolve,@TSolve.StartSolve,
  'StartSolve',[nil],nil);
  KBTypes._RegistryMethod(TSolve,@TSolve.Run,
  'Run',[TypeInfo(_TVariants)],TypeInfo(Variant));
  KBTypes._RegistryMethod(TSolve,@TSolve.MakeConsult,
  'MakeConsult',[TypeInfo(TKnowledgeBase),TypeInfo(TList),TypeInfo(Integer)],nil);
//  RegisterComponentEditor(TSolve, TSolveEditor);
end;

procedure TSolve.StartSolve;
begin
  Solve.StartSolve(KBControl, Self.Stop);
end;

function TSolve.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
  Verify(NIL);
  if IsVerified then begin
    SetStage(etTesting);
//    Self.Stop:=False;
    Solve.Consult(KBControl.KB,Self.Stop);
    RestStage;
  end;
end;

procedure TSolve.MakeConsult(KB: TKnowledgeBase;
                      GoalsList: TList;
                      Style: Integer);
begin
  Solve.MakeConsult(KB, GoalsList, Style, Self.Stop);
  Self.Stop:=False;
end;

procedure StartSolve(Detachment: TKBControl; var Stop: Boolean);
begin
  {Det:=Detachment;}
  {Application.CreateForm(TTestForm,TestForm);
  TestForm.Show;}
  Solve.Consult(Detachment.KB, Stop );
  {TestForm.Free;}
end;

function FindRule(Rules: TList; r: record_rule): Boolean;
var
  i: Integer;
begin
  FindRule:=False;
  for i:=1 to Rules.Count do begin
    if r.n_rule=record_rule(TRuleState(Rules.Items[i-1]).ptr_rule).n_rule then begin
      FindRule:=True;
      break;
    end;
  end;
end;

function NotOznachCount(KB: TKnowledgeBase; r: record_rule): Integer;
var
  ptr_list_attr: TCollection;
  ptr_list: list;
  J,K,N,M: Integer;
begin
  ptr_list_attr:=r.list_attr_if;
  M:=0;
  for J:=1 to ptr_list_attr.Count do begin
    ptr_list:=list(ptr_list_attr.Items[J-1]);
    for K:=1 to KB.ListObjs.Count do begin
      for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
        if (list(ptr_list_attr.Items[J-1]).n_obj
           =record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).n_obj) and
        (list(ptr_list_attr.Items[J-1]).n_attr
         =record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).n_attr) and
         (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status<=2)
        then begin
          Inc(M);
        end;
      end;
    end;
  end;
  NotOznachCount:=M;
end;

procedure SortRules(KB: TKnowledgeBase; RulesList: TList);
var
  i,j,k: Integer;
  P1,P2: Pointer;
  ptr_list_attr: TCollection;
  ptr_list: list;
  r1,r2,r3,r4: Real;
begin
  for i:=1 to RulesList.Count do begin
    for j:=i+1 to RulesList.Count do begin
      if KB.FCalcMethod=Bajes then begin
        ptr_list_attr:=TRuleState(RulesList.Items[i-1]).ptr_rule.list_attr_then;
        r1:=0;
        for k:=1 to ptr_list_attr.Count do begin
          ptr_list:=list(ptr_list_attr.Items[k-1]);
          r1:=ds_And(Lower,Independent,r1,ptr_list.factor1/100);
        end;
        ptr_list_attr:=TRuleState(RulesList.Items[j-1]).ptr_rule.list_attr_then;
        r2:=0;
        for k:=1 to ptr_list_attr.Count do begin
          ptr_list:=list(ptr_list_attr.Items[k-1]);
          r2:=ds_And(Lower,Independent,r2,ptr_list.factor1/100);
        end;
        if (r1=r2) and (
          (NotOznachCount(KB,TRuleState(RulesList.Items[i-1]).ptr_rule)>
           NotOznachCount(KB,TRuleState(RulesList.Items[j-1]).ptr_rule)) {or
          (record_rule(TRuleState(RulesList.Items[i-1]).ptr_rule).
            list_attr_then.count<
          record_rule(TRuleState(RulesList.Items [j-1]).ptr_rule).
            list_attr_then.count)}) or (r1<r2) then begin
          P1:=TRuleState(RulesList.Items[i-1]).ptr_rule;
          P2:=TRuleState(RulesList.Items[j-1]).ptr_rule;
          TRuleState(RulesList.Items[i-1]).ptr_rule:=P2;
          TRuleState(RulesList.Items[j-1]).ptr_rule:=P1;
        end;
      end else begin
        ptr_list_attr:=TRuleState(RulesList.Items[i-1]).ptr_rule.list_attr_then;
        r1:=0;
        r3:=0;
        for k:=1 to ptr_list_attr.Count do begin
          ptr_list:=list(ptr_list_attr.Items[k-1]);
          r1:=ds_And(Lower, Independent,r1,ptr_list.factor1/100);
          r3:=ds_And(Height,Independent,r3,ptr_list.factor2/100);
        end;
        ptr_list_attr:=TRuleState(RulesList.Items[j-1]).ptr_rule.list_attr_then;
        r2:=0;
        r4:=0;
        for k:=1 to ptr_list_attr.Count do begin
          ptr_list:=list(ptr_list_attr.Items[k-1]);
          r2:=ds_And(Lower, Independent,r2,ptr_list.factor1/100);
          r4:=ds_And(Height,Independent,r4,ptr_list.factor2/100);
        end;
        if (r1=r2) and (
          (NotOznachCount(KB,TRuleState(RulesList.Items[i-1]).ptr_rule)>
           NotOznachCount(KB,TRuleState(RulesList.Items[j-1]).ptr_rule)) {or
          (record_rule(TRuleState(RulesList.Items[i-1]).ptr_rule).
            list_attr_then.count<
          record_rule(TRuleState(RulesList.Items [j-1]).ptr_rule).
            list_attr_then.count)}) or ((r3-r1)<(r4-r2)) then begin
          P1:=TRuleState(RulesList.Items[i-1]).ptr_rule;
          P2:=TRuleState(RulesList.Items[j-1]).ptr_rule;
          TRuleState(RulesList.Items[i-1]).ptr_rule:=P2;
          TRuleState(RulesList.Items[j-1]).ptr_rule:=P1;
        end;
      end;
    end;
  end;
end;

procedure Consult(KB: TKnowledgeBase; var Stop: Boolean);
var
  GoalsList: TList;
  K,N: Integer;
begin
//  ShowMessage('������������');

  {��������� ��������� ���������}
  KB.SetPointers;

  {�������� ��������� ����������}
  {for K:=1 to KB.ListObjs.Count do begin
    with record_obj(KB.ListObjs.Items[K-1]) do begin
      if OnKBInterface=NIL then begin
        OnKBInterface:=KB.KBInterface;
        FOnKBInterface.OnInput:=InputDemonRun;
        FOnKBInterface.OnOutput:=OutputDemonRun;
      end;
    end;
        for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
      with record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]) do begin
        if OnKBInterface=NIL then begin
          OnKBInterface:=KB.KBInterface;
          FOnKBInterface.OnInput:=InputDemonRun;
          FOnKBInterface.OnOutput:=OutputDemonRun;
        end;
      end;
    end;
  end;
  }
  {������� ������ �����}
  GoalsList:=TList.Create;
  if KB.Style=1 then
    for K:=1 to KB.ListObjs.Count do begin
      for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
        if (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status=1) or
           (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status=2)
          then
           GoalsList.Add(record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]));
      end;
    end;

  {�������� �����������}
  MakeConsult(KB,GoalsList,KB.Style, Stop);

  {������� ������ �����}
  GoalsList.Clear;
  GoalsList.Destroy;

end;

procedure AddToList(SubGoalsList:TList; Attr: record_attr );
var
  i: Integer;
  Find: Boolean;
begin
  Find:=False;
  for i:=1 to SubGoalsList.Count do begin
    if SubGoalsList.Items[i-1]=Attr then begin
      Find:=True;
      break;
    end;
  end;
  if not Find then begin
    SubGoalsList.Add(Attr);
  end;
end;

procedure MakeConsult(KB: TKnowledgeBase;
                      GoalsList: TList;
                      Style: Integer;var Stop: Boolean);
var
  I,J,K,M,N,D: Integer;
  {PtrObj :  record_obj;
  PtrAttr:  record_attr;
  PtrType:  record_type;
  PtrRule:  record_rule;}
  ptr_list_attr: TCollection;
  RulesList:     TRulesList;
  RuleState:     TRuleState;
  SubGoalsList:  TList;
  Oznach: Integer;
  LogRes: Real;
  LastRes: Integer;
  { Oznach:
    0 - ������� �� ��������;
    1 - ������� �������� � ��������� ��������� ����� "��",
    2 - ������� �������� � ��������� ��������� ����� "�����"}

  StrIncident,
  StrFactor1,
  StrFactor2,
  Str2, Str3,
  Str1: PChar;
  InterfaceValue:Variant;
  InterfaceSuccess:Boolean;
  MaxResult: Real;// �������� ������ ��������������
  ResTestAssertion: Real;
  ESKernel: TESKernel;
begin
  ESKernel := NIL;
  if KB.Owner is TESKernel then ESKernel := TESKernel(KB.Owner);


  if (Style=1) and (GoalsList.Count=0) then Exit;

  StrIncident:=AllocMem(MAX_LINE+1);
  StrFactor1:=AllocMem(MAX_LINE+1);
  StrFactor2:=AllocMem(MAX_LINE+1);
  Str2:=AllocMem(MAX_LINE+1);
  Str3:=AllocMem(MAX_LINE+1);
  Str1:=AllocMem(256);

  RulesList:=TRulesList.Create;

  if Style=1 then begin
  {�������� �������� �����}

    {������� ������ �������� ������}
    M:=GoalsList.Count;
    for K:=1 to M do begin
      for I:=1 to KB.ListRules.Count do begin
        ptr_list_attr:=record_rule(KB.ListRules.Items[I-1]).list_attr_then;
        for J:=1 to ptr_list_attr.Count do begin
          if not FindRule(RulesList,record_rule(KB.ListRules.Items[I-1])) and
             (list(ptr_list_attr.Items[J-1]).n_obj=
               record_attr(GoalsList.Items[K-1]).n_obj) and
             (list(ptr_list_attr.Items[J-1]).n_attr=
               record_attr(GoalsList.Items[K-1]).n_attr) then begin
             RuleState:=TRuleState.Create;
             RuleState.ptr_rule:=record_rule(KB.ListRules.Items[I-1]);
             RulesList.Add(RuleState);
          end;
        end;
        ptr_list_attr:=record_rule(KB.ListRules.Items[I-1]).list_attr_else;
        for J:=1 to ptr_list_attr.Count do begin
          if not FindRule(RulesList,record_rule(KB.ListRules.Items[I-1])) and
             (list(ptr_list_attr.Items[J-1]).n_obj=
               record_attr(GoalsList.Items[K-1]).n_obj) and
             (list(ptr_list_attr.Items[J-1]).n_attr=
               record_attr(GoalsList.Items[K-1]).n_attr) then begin
             RuleState:=TRuleState.Create;
             RuleState.ptr_rule:=record_rule(KB.ListRules.Items[I-1]);
             RulesList.Add(RuleState);
           end;
        end;
      end;
    end;
    SortRules(KB,RulesList);
  end;

  if Style=1 then begin
    {���������, ����� ������� ����� ��������� ����������� ������� �
     ���� ������� ��� �� ��� ����������, �� ���������� �������}
    SubGoalsList:=TList.Create;
    M:=RulesList.Count;
    for I:=1 to M do begin
      RuleState:=TRuleState(RulesList.Items[I-1]);
      ptr_list_attr:=RuleState.ptr_rule.list_attr_if;
      for J:=1 to ptr_list_attr.Count do begin
        for K:=1 to KB.ListObjs.Count do begin
          for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
            if (list(ptr_list_attr.Items[J-1]).n_obj
               =record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).n_obj) and
            (list(ptr_list_attr.Items[J-1]).n_attr
             =record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).n_attr) and
             (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status<=2)
            then begin
              if (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status=0) then
                record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status:=2;
//              SubGoalsList.Add(record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]));
              AddToList(SubGoalsList,record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]));
              {ShowMessage(record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).comment);}


              {�������� ����� �� �������}
              if SubGoalsList.Count>0 then begin
                MakeConsult( KB, SubGoalsList, 1, Stop );
              end;

              {�������� � ������������, ���� �� ������� ��������}
              if //((record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status=0) or
                 (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status=2)  //)
              then begin
                KB.DemonData.ptr_obj:=record_obj(KB.ListObjs.Items[K-1]);
                KB.DemonData.ptr_attr:=record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                     FListAttrs.Items[N-1]);
                {Det.OnInput(Det);}
    //            ShowMessage('SetVal');
               if ESKernel.Tag = 1 then begin
                if Assigned(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                         FListAttrs.Items[N-1]).OnKBInterface) then
                record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                         FListAttrs.Items[N-1]).OnKBInterface.SetAttrValue(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                         FListAttrs.Items[N-1]),InterfaceValue,InterfaceSuccess)
                else begin
                  ShowMessage('������!!');
                end;
                {record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status:=4;}
              end
              else begin
              //---------------------- �� ������ ���������� ----------------------
              if Assigned(ESKernel) then begin
                if ESKernel.Subdialogs then begin
                  ESKernel.RequestValue('������'+IntToStr(record_obj(KB.ListObjs.Items[K-1]).n_obj)+'.'+
                                        '�������'+IntToStr(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                        ListAttrs.Items[N-1]).n_attr));
                  if ESKernel.StopFlag then Stop := True;

                  // ����� �������� � ������ ���������� �������
                  with KB.DemonData do
                    ESKernel.AddToTrace(ptr_obj.n_obj, ptr_attr.n_attr, -1, 0);
                end;
              end;
              //------------------------------------------------------------------
              end;
                if record_attr(record_obj(KB.ListObjs.Items[K-1]).
                             FListAttrs.Items[N-1]).OnKBInterface.Stop then Stop:=True;
              end;
              SubGoalsList.Clear;
            end;
            if Stop then break;
          end;
          if Stop then break;
        end;
        if Stop then break;
      end;
      if Stop then break;
    end;

    {������� ������ ��������}
    SubGoalsList.Clear;
    SubGoalsList.Destroy;
  end;

  if Style=2 then begin
    SubGoalsList:=TList.Create;
    for K:=1 to KB.ListObjs.Count do begin
      if Stop then break;
      for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
        if (record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status<=2)
        then begin
          record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]).status:=2;
//          SubGoalsList.Add(record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]));
          AddToList(SubGoalsList,record_attr(record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Items[N-1]));
        end;
      end;
    end;

    if SubGoalsList.Count>0 then begin
      MakeConsult( KB, SubGoalsList, 1, Stop );
    end;

    SubGoalsList.Clear;
    SubGoalsList.Destroy;
  end;

  if Style=1 then begin
    {��������, ����� ������� ��� �������� � ��������� ������ �����������}
      M:=RulesList.Count;
    for I:=1 to M do begin
      if Stop then break;
      RuleState:=TRuleState(RulesList.Items[I-1]);
      ptr_list_attr:=RuleState.ptr_rule.list_attr_if;
      StrCopy(StrIncident,RuleState.ptr_rule.incident);
      StrCopy(StrFactor1,RuleState.ptr_rule.incident);
      StrCopy(StrFactor2,RuleState.ptr_rule.incident);
//      Oznach:=1;
      for J:=1 to ptr_list_attr.Count do begin
        for K:=1 to KB.ListObjs.Count do begin
          for N:=1 to record_obj(KB.ListObjs.Items[K-1]).
                                    FListAttrs.Count do begin
            if (list(ptr_list_attr.Items[J-1]).n_obj
               =record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                          FListAttrs.Items[N-1]).n_obj) and
               (list(ptr_list_attr.Items[J-1]).n_attr
               =record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                          FListAttrs.Items[N-1]).n_attr)
            then begin

             {�������� �������}
             if (record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                           FListAttrs.Items[N-1]).status<=2)
             then begin
//               Oznach:=0;
//               break;
                StrPCopy(Str1,'P'+IntToStr(J));
                StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
                StrLCopy(Str3,StrIncident,
                     StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
                StrCopy(StrIncident,Str3);
                StrCat(StrIncident,PChar(FloatToStr(2)));
                StrCat(StrIncident,Str2);
                LastRes:=2;
             end else begin
              {�������� ���������� �����������}
              ResTestAssertion:=
                TestAssertion(TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                            FListAttrs.Items[N-1]).
                                            FAssertionCollection.Items[0]),
                  list(ptr_list_attr.Items[J-1]), KB);
              if (record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                            FListAttrs.Items[N-1]).
                                            status>=4) and
                 (ResTestAssertion>=FireThreshold)
              then begin
                StrPCopy(Str1,'P'+IntToStr(J));
                StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
                StrLCopy(Str3,StrIncident,
                     StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
                StrCopy(StrIncident,Str3);
                StrCat(StrIncident,PChar(FloatToStr(ResTestAssertion)));
                StrCat(StrIncident,Str2);
                LastRes:=1;
              end;
              {�������� �������� �����������}
              if (record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                          FListAttrs.Items[N-1]).
                                          status>=4) and
                 (ResTestAssertion<FireThreshold)
              then begin
                StrPCopy(Str1,'P'+IntToStr(J));
                StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
                StrLCopy(Str3,StrIncident,
                     StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
                StrCopy(StrIncident,Str3);
                StrCat(StrIncident,PChar(FloatToStr(ResTestAssertion)));
                StrCat(StrIncident,Str2);
                {Oznach:=2;
                break;}
                LastRes:=0;
              end;
             end;

              {������������ ������ ��� �������1}
              StrPCopy(Str1,'P'+IntToStr(J));
              StrCopy(Str2,StrPos(StrFactor1,Str1)+StrLen(Str1));
              StrLCopy(Str3,StrFactor1,
                   StrLen(StrFactor1)-StrLen(Str2)-StrLen(Str1));
              StrCopy(StrFactor1,Str3);
              if LastRes<>2 then
                StrPCopy(Str1,FloatToStr(TAssertion(record_attr(
                            record_obj(KB.ListObjs.Items[K-1]).
                                        FListAttrs.Items[N-1]).
                                        FAssertionCollection.Items[0]).factor1/100))
              else
                StrPCopy(Str1,FloatToStr(2));
              StrCat(StrFactor1,Str1);
              StrCat(StrFactor1,Str2);

             {������������ ������ ��� �������2}
              StrPCopy(Str1,'P'+IntToStr(J));
              StrCopy(Str2,StrPos(StrFactor2,Str1)+StrLen(Str1));
              StrLCopy(Str3,StrFactor2,
                   StrLen(StrFactor2)-StrLen(Str2)-StrLen(Str1));
              StrCopy(StrFactor2,Str3);
              if LastRes<>2 then
                StrPCopy(Str1,FloatToStr(TAssertion(record_attr(
                            record_obj(KB.ListObjs.Items[K-1]).
                                        FListAttrs.Items[N-1]).
                                        FAssertionCollection.Items[0]).factor2/100))
              else
                StrPCopy(Str1,FloatToStr(2));
              StrCat(StrFactor2,Str1);
              StrCat(StrFactor2,Str2);
//             end else begin
//             end;
            end;
          end;
//          if Oznach=0 then break
        end;
//        if  Oznach=0 then break;
      end;

//      if (Oznach>0) then begin
        LogRes:=Logic(StrIncident,MaxResult);
//      end;
      if {(Oznach>0) and} (LogRes=1) then Oznach:=1 else
      if {(Oznach>0) and} (LogRes=0)
         and (record_rule(KB.ListRules.Items[I-1]).list_attr_else.Count>0)
         then Oznach:=2 else Oznach:=0;

       {ShowMessage('R'+IntToStr(RuleState.ptr_rule.n_rule)+
              '='+IntToStr(Oznach)+' '+StrIncident);}

      if Oznach>0 then begin
        if Oznach=1 then
         ptr_list_attr:=RuleState.ptr_rule.list_attr_then;
        if Oznach=2 then
          ptr_list_attr:=RuleState.ptr_rule.list_attr_else;
          for J:=1 to ptr_list_attr.Count do begin
            for K:=1 to KB.ListObjs.Count do begin
              for N:=1 to record_obj(KB.ListObjs.Items[K-1]).FListAttrs.Count do begin
                if (list(ptr_list_attr.Items[J-1]).n_obj
                  =record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                             FListAttrs.Items[N-1]).n_obj) and
                  (list(ptr_list_attr.Items[J-1]).n_attr
                  =record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                             FListAttrs.Items[N-1]).n_attr) and
                  ((record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                             FListAttrs.Items[N-1]).status<=2) {or
                   (record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                             ListAttrs.Items[N-1]).status=4)})
                                              then begin

                   TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                 FListAttrs.Items[N-1]).FAssertionCollection.Items[0]).MaximalPoint:=MaxResult;
                   if  record_attr(record_obj(KB.ListObjs.Items[K-1]).
                         FListAttrs.Items[N-1]).PtrType.type_znach<>1 then
                     TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                       FListAttrs.Items[N-1]).FAssertionCollection.Items[0]).znach:=
                       list(ptr_list_attr.Items[J-1]).znach else
                     TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                       FListAttrs.Items[N-1]).FAssertionCollection.Items[0]).znach:=
                       FloatToStr(CalcStr(list(ptr_list_attr.Items[J-1]).znach));

                   {record_attr(record_obj(KB.ListObjs.Items[K-1]).
                              ListAttrs.Items[N-1]).assertion.factor1:=
                    Round(LogicF(StrFactor1,1)*100);
                  record_attr(record_obj(KB.ListObjs.Items[K-1]).
                               ListAttrs.Items[N-1]).assertion.factor2:=
                    Round(LogicF(StrFactor2,2)*100);}

                  case KB.FCalcMethod of
                   Demster: begin

                    United(
                      Then_Else_F(StrFactor1,StrFactor2,1,
                        list(ptr_list_attr.Items[J-1]).n_obj,
                        list(ptr_list_attr.Items[J-1]).n_attr,
                        RuleState.ptr_rule.list_attr_then,
                        RuleState.ptr_rule.list_attr_else),
                      Then_Else_F(StrFactor1,StrFactor2,2,
                        list(ptr_list_attr.Items[J-1]).n_obj,
                        list(ptr_list_attr.Items[J-1]).n_attr,
                        RuleState.ptr_rule.list_attr_then,
                        RuleState.ptr_rule.list_attr_else),
                      record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                FListAttrs.Items[N-1]));

                      {record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                   ListAttrs.Items[N-1]).assertion.factor1:=
                        Round(Then_Else_F(StrFactor1,StrFactor2,1,
                           list(ptr_list_attr.Items[J-1]).n_obj,
                          list(ptr_list_attr.Items[J-1]).n_attr,
                          RuleState.ptr_rule..list_attr_then,
                          RuleState.ptr_rule..list_attr_else)*100);

                      record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                   ListAttrs.Items[N-1]).assertion.factor2:=
                        Round(Then_Else_F(StrFactor1,StrFactor2,2,
                          list(ptr_list_attr.Items[J-1]).n_obj,
                          list(ptr_list_attr.Items[J-1]).n_attr,
                          record_rule(KB.ListRules.Items[I-1]).list_attr_then,
                          record_rule(KB.ListRules.Items[I-1]).list_attr_else)*100);}
                   end;

                   Bajes: begin

                     TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                   FListAttrs.Items[N-1]).FAssertionCollection.Items[0]).factor1:=
                       Round(Then_Else_F(StrFactor1,StrFactor2,3,
                         list(ptr_list_attr.Items[J-1]).n_obj,
                         list(ptr_list_attr.Items[J-1]).n_attr,
                         RuleState.ptr_rule.list_attr_then,
                        RuleState.ptr_rule.list_attr_else)*100);

                   end;
                  end;

                  TAssertion(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                        FListAttrs.Items[N-1]).FAssertionCollection.Items[0]).accuracy:=
                   list(ptr_list_attr.Items[J-1]).accuracy;

                  record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                         FListAttrs.Items[N-1]).status:=4;

                  {������� �������� ����� �������� ������������}
                  KB.DemonData.ptr_obj:=record_obj(KB.ListObjs.Items[K-1]);
                  KB.DemonData.ptr_attr:=record_attr(record_obj(KB.ListObjs.Items[K-1]).
                                          FListAttrs.Items[N-1]);
                  KB.DemonData.ptr_rule:=RuleState.ptr_rule;
                  
                  // ����� �������� � ������ ����� ������� � �������
                  with KB.DemonData do
                    ESKernel.AddToTrace(ptr_obj.n_obj, ptr_attr.n_attr, ptr_rule.n_rule, oznach);
		  // ����� �� ���� ��������
                  {
//                  ShowMessage('GetAttr');
                  if Assigned(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                             FListAttrs.Items[N-1]).OnKBInterface) then
                  record_attr(record_obj(KB.ListObjs.Items[K-1]).
                             FListAttrs.Items[N-1]).OnKBInterface.GetAttrValue(record_attr(record_obj(KB.ListObjs.Items[K-1]).
                             FListAttrs.Items[N-1]),InterfaceValue,InterfaceSuccess)
                  else begin
                    ShowMessage('������!!');
                  end;


//                  ShowMessage('OnOutput');
                  KB.OnOutput(KB,InterfaceValue,InterfaceSuccess);}

                  if record_attr(record_obj(KB.ListObjs.Items[K-1]).
                             FListAttrs.Items[N-1]).OnKBInterface.Stop then Stop:=True;

                  break;
                end;
              end;
              if Stop then break;
            end;
            if Stop then break;
          end;
      end;
      if Stop then break;
    end;
  end;

  {��������� ������ �������� ������}
   while RulesList.Count>0 do begin
     RuleState:=TRuleState(RulesList.Items[0]);
     RulesList.Delete(0);
     RuleState.Destroy;
   end;

   RulesList.Destroy;

   FreeMem(StrIncident);
   FreeMem(StrFactor1);
   FreeMem(StrFactor2);
   FreeMem(Str2);
   FreeMem(Str3);
   FreeMem(Str1);

end;

constructor TSolve.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Stop:=False;
end;

procedure TSolve.Verify(Protocol: TStrings);
begin
  inherited Verify(Protocol);
  if not IsVerified then Exit;
  if Assigned(KBControl) then
    if Assigned(KBControl.KB) then KBControl.KB.Verify(Protocol);
end;

{procedure TSolveEditor.Edit;
begin
  TSolve(Component).Run(Nil);
end;
}

constructor TExternalInterpreter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FExternal:={TPersistent(}TExternal.Create{(AOwner))};
end;

destructor TExternalInterpreter.Destroy;
begin
  {TExternal(}FExternal{)}.Destroy;
  inherited Destroy;
end;

function TExternalInterpreter.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
//  if @{TExternal(}FExternal{)}.Run<>NIL then
//    Run:={TExternal(}FExternal{)}.Run(Parms);
  FExternal.RunExe(Nil);
end;

initialization
  RegisterClasses([TSolve]);

end.
