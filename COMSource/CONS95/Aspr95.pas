unit Aspr95;

interface

uses
  Classes, SysUtils, VCL.Dialogs, VCL.Forms, DB, VCL.StdCtrls,
  Aspr_str, Conv_dat, Convert1,Clean, VCL.Controls,
  Math, Aspr_, Edit_str, TypInfo, KBCompon, Paths;
  {FT_Point, FT_MembershipFunction, FT_LinguisticVariable, FT_Dictionary}
  //DsgnIntf, {in Delphi 4}
  //DesignEditors; {in Delphi 6}

 {$R Aspr95.res}

const
  MAXSTR    =    1024;
  LENNAME   =     256;
  RASH_OBJ  =  '.OBT';
  RASH_ATTR =  '.ATR';
  RASH_DICT =  '.DIC';
  RASH_TYPE =  '.TYP';
  RASH_RULE =  '.RUL';
  RASH_SAY  =  '.SAY';
  RASH_SESS =  '.SAS';

 { �������� �������: }
  w_obj     =  '������';
  w_group   =  '������';
  w_attrs   =  '��������';
  w_input   =  '�������� �����';
  w_output  =  '��������� �����';
  w_comment =  '�����������';

  { �������� ������: }
  w_conn    =  '�����';
  w_source  =  '��������';
  w_sink    =  '��������';
  w_list    =  '�����������';
  {w_comment =  '�����������';}

  { �������� ��������: }
  w_attr    =  '�������';
  w_type    =  '���';
  {w_comment =  '�����������';}

  { �������� ����: }
  {w_type   =  '���';}
  w_num    =  '�����';
  w_str    =  '������';
  w_fuzzy  =  '��������';
  w_from   =  '��';
  w_to     =  '��';
  {w_comment = '�����������;}

  { �������� �����������: }
  w_factor   = '�����������';
  w_accuracy = '��������';

  { �������� �������:}
  w_rule    = '�������';
  w_if      = '����';
  w_then    = '��';
  w_else    = '�����';
  {w_comment = '�����������;}

  w_dict = '�������';

  Defaultfactor1  = 50;
  Defaultfactor2  = 100;
  Defaultaccuracy =  0;

  {FontName='Courier New Cyr';}
  FontName='MS Sans Serif';

  {��������� ����������}
  MAX_GEN_LEN = 2048;

var
  flag_separate: Integer = 1;
  flag_eko: Integer = 0;
  n_str: Integer = 1;
  n_str_err: Longint = 0;

  {InputErrors}
  GetNumError: Integer=0;
  LastTypeNumber: Integer=0;
  LastObjectNumber: Integer=0;
  LastRuleNumber: Integer=0;



type

  {TatCollection = class(TCollection)
  public
     constructor Create(ItemClass: TCollectionItemClass);
     function GetNamePath: string; override;
  end;}

  TatDemon = class(TKBComponent)
     private
        {....}
     public
       FActive: Boolean;   // ������ ��� ���������� �����������,
                           // � ���������� ���������� ����� �����������
                           // � ������ ���������� �������� ���
       FOnInput: TPtrProc;
       FOnOutput: TPtrProc;
       InterfaceClass: TClass;
       procedure SetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean ); virtual;
       procedure GetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean ); virtual;
     published
       property Active:Boolean read FActive write FActive;
       property OnInput: TPtrProc read FOnInput write FOnInput;
       property OnOutput: TPtrProc read FOnOutput write FOnOutput;
  end;

  {TPtrProc=TNotifyEvent;}
  TTextStr=array[0..MAXSTR] of Char;

  TCalcMethod=(Demster,Bajes);

  TKBItem = class(TKBCollectionItem)
  public
     FSessionID: Integer;
     Fname:    String;            { - ���                        }
     Fcomment: String;            { - �����������                }
     FControl: String;            { - ����������, ������������ � ��������
                                 ������������ ������ � � ������ ������������ }
     FMFControl: String;            { - ����������, ������������ � ��������
                                 ������������ ������ � � ������ ������������ }
     FComm: String;                { - ����������, ������������ � ��������
                                 ������������ ������ }
     procedure SetDefault;
   published
     property SessionID: Integer read FSessionID write FSessionID;
     property name:    String read Fname write Fname;
     property comment: String read Fcomment write Fcomment;
     property Control: String read FControl write FControl;
     property Comm: String read FComm write FComm;

     {procedure InputDemonRun(Sender: TObject);
     procedure OutputDemonRun(Sender: TObject);}
//     procedure InputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
//     procedure OutputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
     constructor Create(ACollection: TCollection); override;
     procedure Assign(KBItem: TKBItem); virtual;

  end;

  TKBInterface = class(TatDemon)
  public
    FStop: Boolean;
    procedure Design(Sender: TObject); virtual;
    constructor Create(AOwner: TComponent); override;
  published
    property Stop: Boolean read FStop write FStop;
    procedure InputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
    procedure OutputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
  end;

  TKBTestFormInterface = class(TKBInterface)
  public
//    PtrObj: record_obj;
    FForm: TForm;
//    FKB: TKnowledgeBase;
    procedure SetFormValue(Value: TForm);
    function GetFormValue: TForm;
    constructor Create(AOwner: TComponent); override;
    procedure SetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean );virtual;
    procedure GetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean );virtual;
    procedure Design(Sender: TObject); override;
    procedure ReplaceValues(Sender: TObject);
  published
    property Form: TForm read GetFormValue write SetFormValue;
//    property KB: TKnowledgeBase read FKB write FKB;
  end;


{  TTestFormPropertyEditor = class(TPropertyEditor)
    protected
    public
      function GetValue: String; override;
      procedure GetValues(Proc: TGetStrProc); override;
      function GetAttributes: TPropertyAttributes; override;
//      procedure Edit; override;
      procedure SetValue(const Value: string); override;
  end;}

  {TKBInterface = class(TCollectionItem)
  public
    FOnInput:    TPtrProc;
    FOnOutput:   TPtrProc;
  published
    property OnInput:  TPtrProc read FOnInput write FOnInput;
    property OnOutput: TPtrProc read FOnOutput write FOnOutput;
  end;}

record_dict = class(TObject){ �������� ��������:           }
  n_name : Integer;             { - ����� ��������             }
  d_name:  String;              { - ��������                   }
  procedure SetDefault;
  constructor Load(var F: Text);
  procedure Save(var F: Text);
  constructor Create;
  destructor Destroy; override;
end;

record_type=class(TKBItem)    { ��������� ���� ���������:    }
  private
    function GetK_Znach: Integer;
  public
   Fn_type: Integer;              { - ����� ����                 }
   Fk_znach: Integer;             { - ���������� ��������        }
   Ftype_znach: Integer;          { - ��� ��������
                                0,2 - ����������;
                                1   - ��������;
                                3   - ��������������� ����������
                                4   - �������}
   Fmin_znach: String;{ - ����������� ��������       }
   Fmax_znach: String;{ - ������������ ��������      }
   Flist_znach: TKBOwnedCollection;{ - ������ ��������            }
   //FMFList:     TMFList;
        {TKBOwnedCollection} { ������ ������� �������������� }
   FCombo: String;                { - ����������, ������������ � ��������
                                 ������������ ������ }
   FMFControl: String;       { - ����������, ������������ � ��������
                                 ������������ ������ }
  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection);
  constructor Import(N: Integer; ACollection: TCollection);
  procedure Export(N: Integer);
  procedure Save(var F: Text);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;
  procedure Assign(KBItem: TKBItem); override;
  published
  property n_type: Integer read Fn_type write Fn_type;
  property type_znach: Integer read Ftype_znach write Ftype_znach;
  property min_znach: String read Fmin_znach write Fmin_znach;
  property max_znach: String read Fmax_znach write Fmax_znach;
  property Combo: String read FCombo write FCombo;
  property MFControl: String read FMFControl write FMFControl;
  property list_znach: TKBOwnedCollection read Flist_znach write Flist_znach;
  //property MFList: TMFList
//  {TKBOwnedCollection} read FMFList write FMFList;
  property k_znach: Integer read GetK_Znach write Fk_znach;
end;

{  TMFListPropertyEditor = class(TPropertyEditor)
    protected

    public
      function GetAttributes: TPropertyAttributes; override;
      function GetValue: String; override;
      procedure Edit; override;
  end;
}
record_conn=class(TKBItem)     { �������� ������:           }
  public
  n_conn: Integer;              { - ����� �����                }
  k_znach: Integer;             { - ���������� �����������     }
  n_source_obj: Integer;        { - ����� �������-���������    }
  n_sink_obj: Integer;          { - ����� �������-���������    }
  type_: Integer;               { - ��� �����                  }
  Flist_conn: TKBOwnedCollection;       { - ������ �����������         }
  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection);
  procedure Save(var F: Text);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;
  procedure Assign(KBItem: TKBItem); override;
  published
    property list_conn: TKBOwnedCollection read Flist_conn write Flist_conn;
end;

record_obj=class(TKBItem)   { ��������� �������:           }
  public
   Fn_obj:    Integer;            { - ����� �������              }
   Fn_group:  Integer;            { - ����� ������               }
   Froot:     Integer;            { - ����� ������� ��������
                                                        ������ }
   Flast:     Integer;            { - ����� ������� �������
                                    ��������� �� ������ ������ }
   Fk_attr:   Integer;            { - ���������� ���������       }
   Fx, Fy:    Integer;            { - ���������� ������� �� �����}
   Fflag:     Integer;            { - flag=1 - ��������� ������  }
   Fk_input:  Integer;            { - ���������� ������ �� ����� }
   Fk_output: Integer;            { - ���������� ������ �� ������}
   FOnKBInterface: TKBInterface;  { - ��������� � �������� ����������� }
   FListAttrs: TKBOwnedCollection;
   FActiveDesign: Boolean;
  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection );
  constructor Import(N: Integer; ACollection: TCollection );
  procedure Export(N: Integer);
  procedure Save(var F: Text);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;
  procedure Assign(KBItem: TKBItem); override;
  procedure Design(AValue: TKBInterface); virtual;
  procedure SetActiveDesign(AValue: Boolean); virtual;

   published
    property n_obj:   Integer read Fn_obj    write Fn_obj;
    property n_group: Integer read Fn_group  write Fn_group;
    property root:    Integer read Froot     write Froot;
    property last:    Integer read Flast     write Flast;
    property k_attr:  Integer read Fk_attr   write Fk_attr;
    property x:       Integer read Fx        write Fx;
    property y:       Integer read Fy        write Fy;
    property flag:    Integer read Fflag     write Fflag;
    property k_input: Integer read Fk_input  write Fk_input;
    property k_output:Integer read Fk_output write Fk_output;
    property OnKBInterface: TKBInterface read FOnKBInterface write FOnKBInterface{Design};
    property ListAttrs: TKBOwnedCollection read FListAttrs write FListAttrs;
    property ActiveDesign: Boolean read FActiveDesign write SetActiveDesign;
end;

TAssertion=class(TKBItem)        { �����������,                     }
  public
  Fn_obj:    Integer;            { - ����� �������              }
  Fn_attr:   Integer;            { - ����� ��������             }
  Fznach:    String;             { - ��������                   }
  Fn_layer:  Integer;            { - ������� �����������        }
  Ffactor1:  Integer;            { - ������ �����������         }
  Ffactor2:  Integer;            { - ������ �����������         }
  Faccuracy: Integer;            { - ��������                   }
  Fpredicat: Integer;            { - ���������� ��������,
                                 ����������� ������� � ��������:
                                 0 - '='
                                 1 - '>'
                                 2 - '<'
                                 3 - '>='
                                 4 - '<='
                                 5 - '<>'}
  Flast_znach: String;
  Fflag_del: Boolean;
  Fflag_end: Boolean;
  FScale1: String;                { - ����������, ������������ � ��������
                                 ������������ ������ }
  FScale2: String;                { - ����������, ������������ � ��������
                                 ������������ ������ }
  FScale3: String;                { - ����������, ������������ � ��������
                                 ������������ ������ }
  FSkip: Boolean;
  FStep: Integer;
  FLastStep: Integer;

  Ptr_attr : TKBItem; //for GetDefMF

  //*** ��������� �������� �. �. ***//
  fMaximalPoint       : real;                // ������������ �������� �����-�� ��� �� - ������������ ���������.
//  fMembershipFunction : TMembershipFunction; // ��, ������������ � Assertion'�.
  //*** ------------------------ ***//

  procedure SetDefault;
  procedure Assign(KBItem: TKBItem); override;
  constructor Load(var F: Text; ACollection: TCollection);
  constructor LoadFromStr(Str: String; Pos: Integer; ACollection: TCollection);
  procedure Save(var F: Text; Bot: String);
  procedure AddTrassa(Trassa: TStrings; Bot: String);
  function GetZnach: String;
  procedure SetZnach(Value: String);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;

  //*** ��������� �������� �. �. ***//
//  procedure SetMF (iValue : TMembershipFunction);
//  function GetMF : TMembershipFunction;
  //*** ------------------------ ***//

  published
  property n_obj:    Integer read Fn_obj write Fn_obj;
  property n_attr:   Integer read Fn_attr write Fn_attr;
  property znach:    String read GetZnach write SetZnach;
  property n_layer:  Integer read Fn_layer write Fn_layer;
  property factor1:  Integer read Ffactor1 write Ffactor1;
  property factor2:  Integer read Ffactor2 write Ffactor2;
  property accuracy: Integer read Faccuracy write Faccuracy;
  property predicat: Integer read Fpredicat write Fpredicat;
  property last_znach: String read Flast_znach write Flast_znach;
  property flag_del: Boolean read Fflag_del write Fflag_del;
  property flag_end: Boolean read Fflag_end write Fflag_end;
  property Scale1: String read FScale1 write FScale1;
  property Scale2: String read FScale2 write FScale2;
  property Scale3: String read FScale3 write FScale3;
  property Skip: Boolean read FSkip write FSkip;
  property Step: Integer read FStep write FStep;
  property LastStep: Integer read FLastStep write FLastStep;

  //*** ��������� �������� �. �. ***//
  property MaximalPoint       : real read fMaximalPoint write fMaximalPoint;
//  property MembershipFunction : TMembershipFunction read GetMF write SetMF;
  //*** ------------------------ ***//

end;

record_attr=class(TKBItem)  { ��������� ��������:          }
  public
   Fn_obj:  Integer;              { - ����� �������              }
   Fn_attr: Integer;              { - ����� ��������             }
   Fn_type: Integer;              { - ����� ����                 }

   {F}PtrType: record_type;         { - �������� �� ��� ��������   }
   {F}PtrObj:  record_obj;           { - �������� �� ������         }

  { ������, ����������� � �������� ������: }
   FOnKBInterface: TKBInterface;    { - ��������� � �������� ����������� }
   FAssertionCollection: TKBOwnedCollection; { - ������ ����������� }
   Fstatus:     Integer;          { - ������ ��������:           }
  {0 - ���������� ��������� �������� � ������������;
   1 - ��� �������������, ������� �������� ���������� ��������,
              �� ���������� ��� � ������������;
   2 - ������� �������� �������, �� ��� ��������
              ��������� �������;
   3 - ������� �������� ������� � �������� ��� ��������,
              �� ��� �������� ��������� �����������;
   4 - ������� �������� �������, � ��� �������� ��� ��������;
   5 - ������� �������� ������� � ��������� ���������,
              � �������� ��� �������������;
   6 - ������� �� �������� �������, �� ��� ��������
              ��� �������� ��� ������ �������������            }

   FFactor1Control: String;            { - ����������, ������������ � ��������
                                 ������������ ������ � � ������ ������������ }
   FFactor2Control: String;            { - ����������, ������������ � ��������
                                 ������������ ������ � � ������ ������������ }
   FAccuracyControl: String;            { - ����������, ������������ � ��������
                                 ������������ ������ � � ������ ������������ }

  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection);
  procedure Save(var F: Text);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;

  published
    property n_obj:  Integer read Fn_obj write  Fn_obj;
    property n_attr: Integer read Fn_attr write Fn_attr;
    property n_type: Integer read Fn_type write Fn_type;

//    property PtrType: record_type read FPtrType write FPtrType;
//    property PtrObj:  record_obj read FPtrObj write FPtrObj;
    property OnKBInterface: TKBInterface read FOnKBInterface write FOnKBInterface;
    property AssertionCollection: TKBOwnedCollection read FAssertionCollection write FAssertionCollection;
    property status:     Integer read Fstatus write Fstatus;
    property Factor1Control: String read Ffactor1Control write Ffactor1Control;
    property Factor2Control: String read Ffactor2Control write Ffactor2Control;
    property AccuracyControl: String read FaccuracyControl write FaccuracyControl;
end;

list=class(TAssertion)
  public
   {F}PtrAttr: record_attr;         { - ��������� �� �������       }
   {F}last: list;
  procedure Assign(KBItem: TKBItem); override;
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;
  published
//   property PtrAttr: record_attr read FPtrAttr write FPtrAttr;
//   property last: list read Flast write Flast;

end;

value=class(TKBItem)        { �������� ��������:           }
  public
    Fznach:    String;         { - ��������          }
  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection);
  procedure Save(var F: Text);
  constructor Create(ACollection: TCollection);
  destructor Destroy; override;
  published
    property znach: String read Fznach write Fznach;
end;

record_rule=class(TKBItem)  { ��������� �������:           }
  private
    function GetK_Znach_if: Integer;
    function GetK_Znach_then: Integer;
    function GetK_Znach_else: Integer;
  public
    Fn_rule: Integer;              { - ����� �������              }
    Fk_znach_if: Integer;          { - ���������� �����������
                                   � ������� �������           }
    Fk_znach_then: Integer;        { - ���������� �����������
                                   � �������� �������          }
    Fk_znach_else: Integer;        { - ���������� �����������
                                   � �������� ������� (�����)  }
  incident:   TTextStr;         { - ������� �������            }
  consequent: TTextStr;         { - �������� �������           }
  consequent_else: TTextStr;    { - �������� ������� (�����)   }
  Flist_attr_if : TKBOwnedCollection;       { - ������ ���������,
                               �������������� � ������� �������}
  Flist_attr_then: TKBOwnedCollection;      { - ������ ���������,
                              �������������� � �������� �������}
  Flist_attr_else:TKBOwnedCollection;      { - ������ ���������,
                              �������������� � �������� �������
                                        (�����)                }
  procedure SetDefault;
  constructor Load(var F: Text; ACollection: TCollection);
  constructor Import(N: Integer; ACollection: TCollection);
  procedure Export(PtrObjs,PtrTypes: TCollection; N: Integer);
  procedure Save(var F: Text);
  procedure AddTrassa(Trassa: TStrings);
  procedure AddTrace(Trace: TStrings);
  constructor Create(ACollection: TCollection); override;
  destructor Destroy; override;
  procedure Assign(KBItem: TKBItem); override;
  function GetIncident: String;
  procedure PutIncident(Value: String);
  function GetConsequent: String;
  procedure PutConsequent(Value: String);
  function GetConsequent_else: String;
  procedure PutConsequent_else(Value: String);
  procedure Verify(Protocol: TStrings); override;

  published
  property n_rule: Integer read Fn_rule write Fn_rule;
  property k_znach_if: Integer read Getk_znach_if write Fk_znach_if;
  property k_znach_then: Integer read Getk_znach_then write Fk_znach_then;
  property k_znach_else: Integer read Getk_znach_else write Fk_znach_else;
  property Pincident: String read GetIncident write PutIncident;
  property Pconsequent: String read GetConsequent write PutConsequent;
  property Pconsequent_else: String read GetConsequent_else write PutConsequent_else;
  property list_attr_if: TKBOwnedCollection  read Flist_attr_if write Flist_attr_if;
  property list_attr_then: TKBOwnedCollection read Flist_attr_then write Flist_attr_then;
  property list_attr_else: TKBOwnedCollection read Flist_attr_else write Flist_attr_else;

end;

record_constr=class(TObject){ ��������� �����������:       }
  constraint: String;           { - ����� �����������          }
  {procedure SetDefault;
  constructor Load(var F: Text);
  procedure Save(var F: Text);
  constructor Create; override;}
end;

record_relation=class(TObject){ ��������� ���������:       }
  n_rel:    Integer;            { - ����� ���������            }
  rel_type: Integer;            { - ��� ���������              }
  comment:  String;             { - �����������                }
  {procedure SetDefault;
  constructor Load(var F: Text);
  procedure Save(var F: Text);
  constructor Create;}
end;

record_say=class(TObject)
  _row1: Integer;
  _colm1: Integer;
  _attr: Integer;
  _fon: Integer;
  say_string: String;
  procedure SetDefault;
  {constructor Load(var F: Text);
  procedure Save(var F: Text);}
  constructor Create;
  destructor Destroy; override;
end;

{ �������� ���������� � ������ ���������������� }
record_stek=class(TObject)
  step:            Integer;
  n_cur_obj:       Integer;
  n_cur_attr:      Integer;
  n_cur_type:      Integer;
  input_or_output: Integer;
  cur_n_input:     Integer;
  cur_n_output:    Integer;
  {procedure SetDefault;
  constructor Load(var F: Text);
  procedure Save(var F: Text);
  constructor Create;}
end;

last_state=class(TObject)
  _session_path:   array[0..LENNAME] of Char;
  _session_name:   array[0..LENNAME] of Char;
  _comment:        array[0..LENNAME] of Char;
  _scn_name:       array[0..LENNAME] of Char;
  last_step:       Integer;
  _row1:           Integer;
  _colm1:          Integer;
  _struct:         Integer;
  max1:            Integer;
  max2:            Integer;
  dx1:             Integer;
  dy1:             Integer;
  dx2:             Integer;
  dy2:             Integer;
  n_cur_obj:       Integer;
  n_cur_attr:      Integer;
{n_cur_list_attr: Integer;}
  n_cur_dict:      Integer;
  n_cur_type:      Integer;
  n_cur_rule:      Integer;
  cur_n_znach:     Integer;
  cur_n_znach_if:  Integer;
  cur_n_znach_then:Integer;
  cur_n_znach_else:Integer;
  cur_n_input:     Integer;
  cur_n_output:    Integer;
  if_or_then:      Integer;
  input_or_output: Integer;
  obj_x:           Integer;
  obj_y:           Integer;
  state_session:   Integer;
  may_save_say:    Integer;
  len_stek:        Integer;
  alt:             Integer;
  _stek: record_stek;
  {procedure SetDefault;
  constructor Load(var F: Text);
  procedure Save(var F: Text);
  constructor Create; override;}
end;

TDemonData=class(TObject)
  ptr_obj: record_obj;
  ptr_attr: record_attr;
  ptr_rule: record_rule;
end;

TKnowledgeBase=class(TKBComponent)
  private
    {FActive:Boolean;}
    {procedure SetActive(const Value: Boolean);}
    procedure DefineKBInterface(const Value: TKBInterface);
  public
  FSessionID: Integer;
  FListObjs:  TKBOwnedCollection; { ������ �������� � ����������  }
  FListConns: TKBOwnedCollection; { ������ ������ ����� ��������� }
  FListTypes: TKBOwnedCollection; { ������ ����� ���������        }
  FListRules: TKBOwnedCollection; { ������ ������                 }
  FListList:  TKBOwnedCollection; { ������ �����������            }

  {���������� ���������}
  FProblemName: String;               { ��� ����������� ��     }
  FFileName: String;                   { ���� � ����� ��   }
  FStyle: Integer;                    { ����� ��������� ������ }
  FCalcMethod: TCalcMethod;           { ����� ������ �������   }

  DemonData: TDemonData;              { ������ ��� ������}
  FOnInput:    TPtrProc;              { ����� ����� �������� }
  FOnOutput:   TPtrProc;              { ����� ������ �������� }
  KBOwner: TComponent;
  {FKBInterfaceCollection: TKBOwnedCollection;  ������ ������� }
  FKBInterface: TKBInterface;         {����� }
  //FMFDictionary: TMFDictionaryComponent;
  FTrassa: TStrings;
  FTrace: TStrings;
  published
  property SessionID: Integer read FSessionID write FSessionID;
  {property Active:Boolean read FActive write SetActive;}
  property ProblemName: String read FProblemName write FProblemName;
  property FileName: String read FFileName write FFileName;
  property Style: Integer read FStyle write FStyle;
  property CalcMethod: TCalcMethod read FCalcMethod write FCalcMethod;
  property OnInput:  TPtrProc read FOnInput write FOnInput;
  property OnOutput: TPtrProc read FOnInput write FOnInput;
  {property KBInterfaceCollection: TKBOwnedCollection read FKBInterfaceCollection
                                                write FKBInterfaceCollection;}
  property KBInterface: TKBInterface read FKBInterface
                                                write DefineKBInterface;
  property ListObjs: TKBOwnedCollection read FListObjs
                                 write FListObjs;
  property ListConns: TKBOwnedCollection read FListConns
                                  write FListConns;
  property ListTypes: TKBOwnedCollection read FListTypes
                             write FListTypes;
  property ListRules: TKBOwnedCollection read  FListRules
                             write FListRules;
  property ListList: TKBOwnedCollection read  FListList
                             write FListList;
//  property MFDictionary: TMFDictionaryComponent read FMFDictionary
//                             write   FMFDictionary;
  property Trassa: TStrings read FTrassa write FTrassa;
  //��������� ��������� �.�., ������ ������ ����������� ������
  property Trace: TStrings read FTrace write FTrace;

  procedure SetDefault;
  constructor Create(AOwner: TComponent);override;
  destructor Destroy; override;
  procedure ClearLists; virtual;
  procedure LoadFromFile;
  procedure SetPointers;
  procedure Load;
  procedure Import(SESPath: String);
  procedure Export(SESPath: String);
  function Save: String;
  function Convert(frmt_name: String): String;
  procedure transl(Current: CURRENT_);
  {procedure AddFromStrings(KBStrings:TKB; KB1: TKnowledgeBase);}
  {procedure TestDemon(Sender: TObject);}
  procedure Add(KBAddPath: String);
  procedure CreateListList;
//  procedure ListClear;
  procedure SaveStep(StepPath: String);
  procedure LoadStep(StepPath: String);
  procedure ClearWorkMemory;
  procedure TestDemon(Sender: TObject; var AValue:Variant; var Success:Boolean );
  procedure Verify(Protocol: TStrings);  override;
end;

STEK = record_stek;
LAST = last_state;

{  TFileNameEditor = class(TPropertyEditor)
    protected

    public
      function GetAttributes: TPropertyAttributes; override;
      function GetValue:string; override;
      procedure Edit; override;
  end;}

var
  Str: String;
  FlagCreate:  Boolean;
  FlagCreate1: Boolean;
  FlagRead:    Boolean;
  Pos: Integer;
  SetDefaultKBInterface: TKBInterface{TCollectionItem};

  {KB: TKnowledgeBase;
  KB1: TKnowledgeBase;}

  {���������� ����������}
  gen_buf: array [0..MAX_GEN_LEN-1] of Char;
  CurObj :  record_obj;
  CurAttr:  record_attr;
  CurType:  record_type;
  CurRule:  record_rule;
  CurDict:  value;
  CurNZnach: Integer;
  CurIf  :  list;
  CurThen:  list;
  CurElse:  list;
  CurList:  list;
  ptr_a: record_attr;
  ptr_t: record_type;
  ptr_d: value;
  F_fp: Text;
  frmt_name: String;
  _pref:  String;
  _and:   String;
  _or:    String;
  _not:   String;
  _begin: String;
  _end:   String;
  _m1:    String;
  _m2:    String;
  _m3:    String;
  _m4:    String;
  _iform: String;
  ptr_curr: CURRENT_;

  var
  _S: String;

  {procedure InitKB; far; export;}
  {procedure ClearKB; far; export;}
  {function SetKB: TKnowledgeBase; far; export;}
  {procedure AddKB(FileName: String; var KB,KB1: TKnowledgeBase); far; export;}
  {procedure LetKB(KBPtr: TKnowledgeBase); export;}
  function iformat(i: Integer;Q :Integer): String; far; export;
  function eko_str(s: String): String; far; export;
  function DelSpaces(s: String): PChar; far; export;
  procedure ShowTrassa(ptr_rule: record_rule); far; export;
  function GetNumFromWord(S: String): Integer; far; export;
  procedure SetPtrObjs(KB: TKnowledgeBase); far; export;
  procedure SetPtrAttrs(KB: TKnowledgeBase); far; export;
  procedure SetPtrTypes(KB: TKnowledgeBase); far; export;
  procedure SetPtrAttr(KB: TKnowledgeBase; ptr_list_attr: TCollection); far; export;
  procedure ReadStr(var F: Text; var Str: String; var Pos: Integer);
  function LastWord(S: String; var Pos: Integer): String; 
  function ScanStr(S: String; var Pos: Integer):  String; 
  function TestWord(S: String; L: array of String; SCount: Integer): Boolean;

  procedure Register;

implementation

uses
  //FT_Add,
  Calc;

  {function ScanSkob(S: String; var Pos: Integer): String; forward;}

  {��������� � ������� ��� ����������}
  procedure next_str(Current: CURRENT_);           forward;
  function Cmp(n: Integer; s: PChar): Boolean;     forward;
  procedure is_err1(Current: CURRENT_);            forward;
  procedure is_err2(Current: CURRENT_);            forward;
  procedure is_err3(n: Integer);                   forward;
  procedure find_begin;                            forward;

  {procedure init_session;                        far;external Aspr;
  procedure load_session(name: String);          far;external Aspr;
  procedure save_session(name: String);          far;external Aspr;
  procedure set_prog_name(name: String);         far;external Aspr;
  procedure load_base(name: String);             far;external Aspr;
  procedure save_base(name: String);             far;external Aspr;

  function set_obj :OBJECT_;                     far;external Aspr;
  function set_attr:ATTRIBUTE;                   far;external Aspr;
  function set_dict:LIST_ZNACH;                  far;external Aspr;
  function set_type:TYPE_;                       far;external Aspr;
  function set_rule:RULE;                        far;external Aspr;
  function set_session:LAST;                     far;external Aspr;

  function set_obj_count :Integer;               far;external Aspr;
  function set_attr_count:Integer;               far;external Aspr;
  function set_dict_count:Integer;               far;external Aspr;
  function set_type_count:Integer;               far;external Aspr;
  function set_rule_count:Integer;               far;external Aspr;

  function find_attr(ptr: ATTRIBUTE; n: Integer): ATTRIBUTE;far;external Aspr;
  function find_type_n(ptr: TYPE_ ; n: Integer): TYPE_;     far;external Aspr;

  function new_record_obj: OBJECT_;    far;external Aspr;
  function new_record_attr: ATTRIBUTE; far;external Aspr;
  function new_record_type: TYPE_;     far;external Aspr;
  function new_record_cur_dict(cur_type: TYPE_): LIST_ZNACH; far;external Aspr;
  function new_record_rule: RULE;      far;external Aspr;
  function insert_record_znach(cur_rule: RULE;if_then: Integer): LIST_ATTR;
    far;external Aspr;}

procedure Register;
begin
  RegisterComponents('Engine',
         [TKnowledgeBase, TKBInterface, TKBTestFormInterface]);
  RegisterClasses([TKnowledgeBase, TKBItem,
         record_type,record_obj, record_attr, record_rule]);
{  RegisterPropertyEditor(TypeInfo(TForm),
         TKBTestFormInterface,
         'Form',
         TTestFormPropertyEditor);
  RegisterPropertyEditor(TypeInfo(string),
         TKnowledgeBase,
         'FileName',
         TFileNameEditor);}
end;


  procedure TKBItem.SetDefault;
  begin
    Fname:='';
    Fcomment:='';
    FControl:='';
    Fname:='';
    Fcomment:='';
    FControl:='';
    FComm:='';
  end;

  constructor TKBItem.Create(ACollection: TCollection);
  begin
    inherited Create(ACollection);
    SessionID:=GetOrdProp(TKBOwnedCollection(ACollection).Owner,
      GetPropInfo(TKBOwnedCollection(ACollection).Owner.ClassInfo,'SessionID'));
  end;

  procedure record_dict.SetDefault;
  begin
  end;

  constructor record_dict.Load(var F: Text);
  begin
  end;

  procedure record_dict.Save(var F: Text);
  begin
  end;

  constructor record_dict.Create;
  begin
    Inherited Create;
    SetDefault;
  end;

  destructor record_dict.Destroy;
  begin
    Inherited Destroy;
  end;

  procedure record_obj.SetDefault;
  begin
    Inherited SetDefault;
    n_obj   :=  0;
    n_group :=  0;
    root    :=  0;
    last    :=  0;
    k_attr  :=  0;
    x       :=  0;
    y       :=  0;
    flag    :=  0;
    k_input :=  0;
    k_output:=  0;

    {if input<>NIL
    then input.Destroy;
    else input:=TList.Create;

    if output<>NIL
    then output.Destroy;
    else output:=TList.Create;
    }
    FOnKBInterface:={TKBInterface(}SetDefaultKBInterface{)};
    if FOnKBInterface<>NIL then begin
//      FOnKBInterface.Design(Self);
      FOnKBInterface.OnInput:=FOnKBInterface.InputDemonRun;
      FOnKBInterface.OnOutput:=FOnKBInterface.OutputDemonRun;
    end;
    FActiveDesign:=False;
  end;

  constructor record_obj.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: record_obj;
    PtrAttr: record_attr;
    Flag: Boolean;

  begin
    FlagCreate:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if LastWord(Str,Pos)=w_type then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_rule then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_dict)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_obj then begin
         Self:=record_obj(ACollection.Add);
         n_obj:=GetNumFromWord(LastWord(Str,Pos));
         LastObjectNumber:=n_obj;
         if GetNumError<>0 then begin
           ShowMessage('������ � ������ �������!');
           n_obj:=ACollection.Count;
         end;
         comment:=w_obj+IntToStr(n_obj);
         if eof(F) then break;
         FlagRead:=True;
         ReadStr(F,Str,Pos);
         if LastWord(Str,Pos)=w_group then begin
           n_group:=GetNumFromWord(LastWord(Str,Pos));
           if GetNumError<>0 then begin
             ShowMessage('������ '+IntToStr(LastObjectNumber)+
               ': ������ � ������ ������!');
           end;
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         if LastWord(Str,Pos)=w_attrs then begin

           {ListAttrs:=TList.Create;}

           Flag:=False;
           FlagCreate1:=False;
           FlagRead:=True;
           PtrAttr:=record_attr.Load(F,ListAttrs);
           if FlagCreate1 then  begin
              PtrAttr.n_obj:=n_obj;
              TAssertion(PtrAttr.AssertionCollection.Items[0]).n_obj:=n_obj;
            end;
           while not(Eof(F)) and FlagCreate1 do begin
             FlagCreate1:=False;
             {ListAttrs.Add(PtrAttr);}
             PtrAttr:=record_attr.Load(F,ListAttrs);
             if FlagCreate1 then  begin
               PtrAttr.n_obj:=n_obj;
               TAssertion(PtrAttr.AssertionCollection.Items[0]).n_obj:=n_obj;
             end;
             k_attr:=k_attr+1;
             Flag:=True;
           end;
           {if FlagCreate1 then ListAttrs.Add(PtrAttr);}
           Pos:=1;
         end else Pos:=1;
        if LastWord(Str,Pos)=w_input then begin
            FlagRead:=True;
         end else Pos:=1;
         if LastWord(Str,Pos)=w_output then begin
           FlagRead:=True;
         end else Pos:=1;
         if LastWord(Str,Pos)=w_comment then begin
           comment:=Copy(Str,Pos+1,Length(Str));
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         break;
      end;
      FlagRead:=True;
    end;
 end;

  constructor record_obj.Import(N: Integer; ACollection: TCollection);
  var
    PtrObj :OBJECT_;
    PtrAttr: ATTRIBUTE;
    Ptr: record_attr;
    I: Integer;
  begin
    Self:=record_obj(ACollection.Add);
    PtrObj    :=   set_obj;
    for i:=1 to N-1 do PtrObj:=PtrObj^.next;
    n_obj     :=   PtrObj^.n_obj;
    n_group   :=   0;
    root      :=   PtrObj^.root;
    last      :=   PtrObj^.last;
    k_attr    :=   0;
    x         :=   PtrObj^.x;
    y         :=   PtrObj^.y;
    flag      :=   PtrObj^.flag;
    k_input   :=   PtrObj^.k_input;
    k_output  :=   PtrObj^.k_output;
    comment   :=   StrPas(DelSpaces(PtrObj^.o_comment));
    {input :
    output:}
    {if k_attr>0 then ListAttrs:=TList.Create;}
    PtrAttr:=set_attr;
    I:=0;
    while (PtrAttr<>NIL) {and (I<PtrObj.k_attr)} do begin
      if PtrAttr^.n_obj=n_obj then begin
        Inc(I);
        {Ptr:=record_attr.Create(ListAttrs);}
        Ptr:=record_attr(ListAttrs.Add);
        with Ptr do begin
          n_obj   := PtrAttr^.n_obj ;
          n_attr  := PtrAttr^.n_attr;
          n_type  := PtrAttr^.n_type;
          comment := PtrAttr^.a_comment;
          {comment := 'AA'+IntToStr(n_attr);}
        end;
        {ListAttrs.Add(Ptr);}
      end;
      k_attr:=I;
      PtrAttr:=PtrAttr^.next;
    end;
  end;

  procedure record_obj.Export(N: Integer);
  var
    PtrObj :OBJECT_;
    PtrAttr: ATTRIBUTE;
    I: Integer;
  begin
    PtrObj:=new_record_obj;
    PtrObj^.n_obj    := n_obj;
    PtrObj^.root     := root;
    PtrObj^.last     := last;
    PtrObj^.k_attr   := k_attr;
    PtrObj^.x        := x;
    PtrObj^.y        := y;
    PtrObj^.flag     := flag;
    PtrObj^.k_input  := k_input;
    PtrObj^.k_output := k_output;
    PtrObj^.o_comment:= comment;
    for I:=1 to ListAttrs.Count do begin
      PtrAttr:=new_record_attr;
      PtrAttr^.n_obj     :=  record_attr(ListAttrs.Items[I-1]).n_obj;
      PtrAttr^.n_attr    :=  record_attr(ListAttrs.Items[I-1]).n_attr;
      PtrAttr^.n_type    :=  record_attr(ListAttrs.Items[I-1]).n_type;
      PtrAttr^.a_comment :=  record_attr(ListAttrs.Items[I-1]).comment;
    end;
  end;

  procedure record_obj.Save(var F: Text);
  var
    i: Integer;
  begin
    Writeln(F,w_obj+' '+w_obj,n_obj);
    Writeln(F,w_group+' '+w_group,n_group);
    Writeln(F,w_attrs);
    for i:=1 to ListAttrs.Count do begin
       record_attr(ListAttrs.Items[i-1]).Save(F);
    end;
    Writeln(F,w_comment+' '+comment);
  end;

  procedure record_type.Assign(KBItem: TKBItem);
  var
    S,D: record_type;
    I: Integer;
    v: value;
  begin
    Inherited Assign(KBItem);
    S:=record_type(KBItem);
    D:=Self;
    D.n_type:=       S.n_type;
    D.k_znach:=      S.k_znach;
    D.type_znach:=   S.type_znach;
    D.min_znach:=    S.min_znach;
    D.max_znach:=    S.max_znach;
    D.Flist_znach.Clear;
    for I:=1 to S.Flist_znach.Count do begin
      v:=value(D.Flist_znach.Add);
      v.Fznach:=value(S.Flist_znach.Items[I-1]).Fznach;
    end;
//    D.FMFList.GoodAssign(S.FMFList);
    D.Combo:=        S.Combo;
  end;

  constructor record_obj.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     RegistryTestableProperty('ListAttrs');
     {Self:=record_obj(ACollection.Add);}
     FListAttrs:=TKBOwnedCollection.Create(Self,record_attr);
     SetDefault;
     FlagCreate:=True;
  end;

  destructor record_obj.Destroy;
  var
    cur_a: record_attr;
    I: Integer;
  begin
    if ListAttrs<>NIL then begin
      I:=1;
      while ListAttrs.Count>0 do begin
        cur_a:=record_attr(ListAttrs.Items[I-1]);
        {ListAttrs.Delete(I-1);}
        cur_a.Destroy;
      end;
      ListAttrs.Destroy;
    end;
    Inherited Destroy;
  end;

  procedure record_obj.Assign(KBItem: TKBItem);
  var
    S,D: record_obj;
  begin
    Inherited Assign(KBItem);
    S:=record_obj(KBItem);
    D:=Self;
    D.n_obj:=     S.n_obj;
    D.n_group:=   S.n_group;
    D.root:=      S.root;
    D.last:=      S.last;
    D.k_attr:=    S.k_attr;
    D.x:=         S.x;
    D.y:=         S.y;
    D.flag:=      S.flag;
    D.k_input:=   S.k_input;
    D.k_output:=  S.k_output;
    D.OnKBInterface:= S.OnKBInterface;
    D.ListAttrs:={.Assign(}S.ListAttrs{)};
  end;

  procedure record_conn.SetDefault;
  begin
    Inherited SetDefault;
    n_conn:=0;
    k_znach:=0;

    if list_conn<>NIL
    then list_conn.Destroy;
    Flist_conn:=TKBOwnedCollection.Create(Self,list);

  end;

  constructor record_conn.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: record_rule;
    PtrValue: list;
    Flag: Boolean;
  begin
    FlagCreate:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if LastWord(Str,Pos)=w_type then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_obj)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_rule)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_dict)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_conn then begin
         FlagCreate:=True;
         Self:=record_conn(ACollection.Add);
         n_conn:=GetNumFromWord(LastWord(Str,Pos));
         if GetNumError<>0 then begin
           ShowMessage('������ � ������ �����!');
           n_conn:=ACollection.Count;
         end;
         comment:=w_conn+IntToStr(n_conn);
         if eof(F) then break;
         FlagRead:=True;
         ReadStr(F,Str,Pos);

         if LastWord(Str,Pos)=w_source then begin
           n_source_obj:=GetNumFromWord(LastWord(Str,Pos));
           if GetNumError<>0 then begin
             ShowMessage('������ � ������ �������� �������!');
           end;
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;

         if LastWord(Str,Pos)=w_sink then begin
           n_sink_obj:=GetNumFromWord(LastWord(Str,Pos));
           if GetNumError<>0 then begin
             ShowMessage('������ � ������ ��������������� �������!');
           end;
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;

         if LastWord(Str,Pos)=w_list then begin
           if list_conn=NIL then
             Flist_conn:=TKBOwnedCollection.Create(Self,list);

           Flag:=False;
           FlagRead:=True;
           PtrValue:=list.Load(F,list_conn);
           while not(Eof(F)) and FlagCreate1 do begin
             {list_conn.Add(PtrValue);}
             Flag:=False;
             PtrValue:=list.Load(F,list_conn);
             Flag:=True;
           end;
           {if FlagCreate1 then begin
             list_conn.Add(PtrValue);
           end;}
           Pos:=1;
         end else Pos:=1;

         if LastWord(Str,Pos)=w_comment then begin
           comment:=Copy(Str,Pos+1,Length(Str));
          FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         break;
      end;
      FlagRead:=True;
    end;
  end;

  procedure record_conn.Save(var F: Text);
  var
    i: Integer;
    Bot: String;
  begin
    Writeln(F,w_conn+' L',n_conn);
    Writeln(F,w_source,' ', n_source_obj);
    Writeln(F,w_sink,  ' ', n_sink_obj);
    if list_conn.Count>0 then Writeln(F,w_list);
    Bot:='';
    for i:=1 to list_conn.Count do begin
      list(list_conn.Items[i-1]).Save(F,Bot);
    end;
    Writeln(F,w_comment+' '+comment);
 end;

  constructor record_conn.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     {Self:=record_conn(ACollection.Add);}
     SetDefault;
     FlagCreate:=True;
     Flist_conn:=TKBOwnedCollection.Create(Self,list);
  end;

  destructor record_conn.Destroy;
  begin
    list_conn.Destroy;
    Inherited Destroy;
  end;

  procedure record_conn.Assign(KBItem: TKBItem);
  var
    S,D: record_conn;
  begin
    Inherited Assign(KBItem);
    S:=record_conn(KBItem);
    D:=Self;
    D.n_conn:=S.n_conn;
    D.k_znach:=S.k_znach;
    D.n_source_obj:=S.n_source_obj;
    D.n_sink_obj:=S.n_sink_obj;
    D.type_:=S.type_;
    D.list_conn:={Assign(}S.list_conn{)};
  end;

  procedure record_attr.SetDefault;
  begin
    Inherited SetDefault;
    n_obj   :=  0;
    n_attr  :=  0;
    n_type  :=  0;
    Status  :=  0;
    FOnKBInterface:=SetDefaultKBInterface;
    if FOnKBInterface<>NIL then begin
      FOnKBInterface.OnInput:=FOnKBInterface.InputDemonRun;
      FOnKBInterface.OnOutput:=FOnKBInterface.OutputDemonRun;
    end;
    {F}PtrType:=NIL;
    {F}PtrObj:=NIL;
  end;

  constructor record_attr.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: record_attr;
    Pos: Integer;
  begin
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if (LastWord(Str,Pos)=w_obj) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_type) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_rule) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_comment) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_attr then begin
         Self:=record_attr(ACollection.Add);
         n_attr:=GetNumFromWord(LastWord(Str,Pos));
         if GetNumError<>0 then begin
           ShowMessage('������ '+IntToStr(LastObjectNumber)+
             ': ������ � ������ ��������!');
           n_obj:=ACollection.Count;
         end;
         TAssertion(AssertionCollection.Items[0]).n_attr:=n_attr;
         comment:=w_attr+IntToStr(n_attr);
         if eof(F) then break;
         FlagRead:=True;
         ReadStr(F,Str,Pos);
         if LastWord(Str,Pos)=w_type then begin
           n_type:=GetNumFromWord(LastWord(Str,Pos));
           if GetNumError<>0 then begin
             ShowMessage('������ '+IntToStr(LastObjectNumber)+
               ': ������ � ������ ����!');
           end;
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         if LastWord(Str,Pos)=w_comment then begin
           comment:=Copy(Str,Pos+1,Length(Str));
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         break;
      end else Pos:=1;
      break;
    end;
  end;

  procedure record_attr.Save(var F: Text);
  begin
    Writeln(F,w_attr+' '+w_attr,n_attr);
    Writeln(F,w_type+' '+w_type,n_type);
    Writeln(F,w_comment+' '+comment);
  end;

  constructor record_attr.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     RegistryTestableProperty('OnKBInterface');
     {Self:=record_attr(ACollection.Add);}
     AssertionCollection:=TKBOwnedCollection.Create(Self,TAssertion);
     AssertionCollection.Add;
     TAssertion (AssertionCollection.Items[0]).Ptr_attr:=Self;
   //  Fassertion:=TAssertion.Create(AssertionCollection);
     SetDefault;
     FlagCreate1:=True;
  end;

  destructor record_attr.Destroy;
  begin
//    if FAssertion<>NIL then
//      FAssertion.Destroy;
    if AssertionCollection<>NIL then
      AssertionCollection.Destroy;
    Inherited Destroy;
  end;

  procedure value.SetDefault;
  begin
    Fznach:='';
  end;

  constructor value.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: value;
    Pos: Integer;
    Pos1: Integer;
  begin
    FlagCreate1:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if (LastWord(Str,Pos)=w_obj) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_type) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_rule) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_attr) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_comment) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_fuzzy) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      Pos1:=System.Pos('"',Str);
      if Pos1>0 then begin
         FlagCreate1:=True;
         Self:=value(ACollection.Add);
         Fznach:=Copy(Str,Pos1+1,System.Pos('"',
                Copy(Str,Pos1+1,Length(Str)))-1);
         FlagRead:=True;
         ReadStr(F,Str,Pos);
         break;
      end;
    end;
  end;

  procedure value.Save(var F: Text);
  begin
    Writeln(F,'"',Fznach,'"');
  end;

  constructor value.Create(ACollection: TCollection);
  begin
     Inherited Create(ACollection);
     SetDefault;
     FlagCreate1:=True;
  end;

  destructor value.Destroy;
  begin
    Inherited Destroy;
  end;

  procedure TAssertion.SetDefault;
  begin
    Inherited SetDefault;
    n_obj    :=  0;
    n_attr   :=  0;
    Fznach   := '';
    {znach    := '';}
    n_layer  :=  0;
    factor1  := Defaultfactor1;
    factor2  := Defaultfactor2;
    accuracy := Defaultaccuracy;
    predicat :=  0;
    flag_del:=False;
  end;

  procedure TAssertion.Assign(KBItem: TKBItem);
  begin
    inherited Assign(KBItem);
    Fn_obj       := TAssertion(KBItem).Fn_obj;
    Fn_attr      := TAssertion(KBItem).Fn_attr;
    Fznach       := TAssertion(KBItem).Fznach;
    Fn_layer     := TAssertion(KBItem).Fn_layer;
    Ffactor1     := TAssertion(KBItem).Ffactor1;
    Ffactor2     := TAssertion(KBItem).Ffactor2;
    Faccuracy    := TAssertion(KBItem).Faccuracy;
    Fpredicat    := TAssertion(KBItem).Fpredicat;
    Flast_znach  := TAssertion(KBItem).Flast_znach;
    FLastStep    := TAssertion(KBItem).FLastStep;
    Fflag_del    := TAssertion(KBItem).Fflag_del;
    Fflag_end    := TAssertion(KBItem).Fflag_end;
    FScale1      := TAssertion(KBItem).FScale1;
    FScale2      := TAssertion(KBItem).FScale2;
    FScale3      := TAssertion(KBItem).FScale3;
    FSkip        := TAssertion(KBItem).FSkip;
    Ptr_attr     := TAssertion(KBItem).Ptr_attr;
    fMaximalPoint:= TAssertion(KBItem).fMaximalPoint;
//    fMembershipFunction.Assign(
//      TAssertion(KBItem).fMembershipFunction);
  end;

  procedure list.Assign(KBItem: TKBItem);
  begin
    inherited Assign(KBItem);
    PtrAttr:=list(KBItem).PtrAttr;
    last:=list(KBItem).last;
  end;

  constructor TAssertion.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: list;
    Pos,Pos1,I: Integer;
    W: String;
  begin
    FlagCreate1:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if (LastWord(Str,Pos)=w_then) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_else) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_comment) then begin
         Pos:=1; break;
      end else
         Pos:=1;

      FlagCreate1:=True;
      Self:=TAssertion(ACollection.Add);
      n_obj :=GetNumFromWord(ScanStr(Str,Pos));
      if GetNumError<>0 then begin
        ShowMessage('������� '+IntToStr(LastRuleNumber)+
          ': ������ � ������ �������!');
      end;
      ScanStr(Str,Pos);
      n_attr:=GetNumFromWord(ScanStr(Str,Pos));
      if GetNumError<>0 then begin
        ShowMessage('������� '+IntToStr(LastRuleNumber)+
          ': ������ � ������ �������!');
      end;
      predicat:=-1;
      if  Str[Pos]='='  then predicat:=0;
      if  Str[Pos]='>'  then predicat:=1;
      if  Str[Pos]='<'  then predicat:=2;
      if (Str[Pos]='>') and (Str[Pos+1]='=') then predicat:=3;
      if (Str[Pos]='<') and (Str[Pos+1]='=') then predicat:=4;
      if (Str[Pos]='<') and (Str[Pos+1]='>') then predicat:=5;
      if predicat=-1 then begin
        ShowMessage('������� '+IntToStr(LastRuleNumber)+
          ': �������� ���� "=", ">" ��� "<"');
        predicat:=0;
      end;
      ScanStr(Str,Pos);
      ScanStr(Str,Pos);
      Pos1:=System.Pos('"',Str);
      if Pos1>0 then begin
         Fznach:=Copy(Str,Pos1+1,System.Pos('"',
                Copy(Str,Pos1+1,Length(Str)))-1);
      end else
        ShowMessage('����������� ������: "');
      {Fznach:=ScanStr(Str,Pos);}
      {ScanStr(Str,Pos);}

      Pos1:=Pos+Length(Fznach)+2;
      if ScanStr(Str,Pos1)=w_factor then begin
        I:=Pos1;
        while (Str[I]<>'[') and
          not ((Str[I]>='0') and
              (Str[I]<='9')) do Inc(I);
        if (Str[I]>='0') and
           (Str[I]<='9') then
          factor1:=GetNumFromWord(ScanStr(Str,Pos1));
          if GetNumError<>0 then begin
            ShowMessage('������� '+IntToStr(LastRuleNumber)+
              ': ������ � ������� �����������!');
          end;
        if (Str[I]='[') then begin
          Pos1:=I+1;
          factor1:=GetNumFromWord(ScanStr(Str,Pos1));
          if GetNumError<>0 then begin
            ShowMessage('������� '+IntToStr(LastRuleNumber)+
              ': ������ � ������� �����������!');
          end;
           I:=Pos1;
          while not ((Str[I]>='0') and
                    (Str[I]<='9')) do Inc(I);
          Pos1:=I;
          factor2:=GetNumFromWord(ScanStr(Str,Pos1));
          if GetNumError<>0 then begin
            ShowMessage('������� '+IntToStr(LastRuleNumber)+
              ': ������ � ������� �����������!');
          end;
           Inc(Pos1);
        end;
        Pos:=Pos1;
      end;
      Pos1:=Pos;
      if ScanStr(Str,Pos1)=w_accuracy then begin
        accuracy:=GetNumFromWord(ScanStr(Str,Pos1));
        if GetNumError<>0 then begin
            ShowMessage('������� '+IntToStr(LastRuleNumber)+
              ': ������ � ��������!');
        end;
      end;

//      Step:=Step;

      FlagRead:=True;
      ReadStr(F,Str,Pos);
      break;

    end;
  end;

  constructor TAssertion.LoadFromStr(Str: String; Pos: Integer; ACollection: TCollection);
  var
    Ptr: list;
    Pos1,I: Integer;
    W: String;
  begin
      FlagCreate1:=True;
      Self:=TAssertion(ACollection.Add);
      n_obj :=GetNumFromWord(ScanStr(Str,Pos));
      LastObjectNumber:=n_obj;
      if GetNumError<>0 then begin
         ShowMessage('������� '+IntToStr(LastRuleNumber)+
           ': ������ � ������ �������!');
        n_obj:=ACollection.Count;
      end;
      ScanStr(Str,Pos);
      n_attr:=GetNumFromWord(ScanStr(Str,Pos));
      if GetNumError<>0 then begin
        ShowMessage('������� '+IntToStr(LastRuleNumber)+
          ': ������ � ������ ��������!');
      end;
      predicat:=-1;
      if  Str[Pos]='='  then predicat:=0;
      if  Str[Pos]='>'  then predicat:=1;
      if  Str[Pos]='<'  then predicat:=2;
      if (Str[Pos]='>') and (Str[Pos+1]='=') then predicat:=3;
      if (Str[Pos]='<') and (Str[Pos+1]='=') then predicat:=4;
      if (Str[Pos]='<') and (Str[Pos+1]='>') then predicat:=5;
      if predicat=-1 then begin
        ShowMessage('������� '+IntToStr(LastRuleNumber)+
          ': �������� ���� "=", ">" ��� "<"');
        predicat:=0;
      end;
      ScanStr(Str,Pos);
      ScanStr(Str,Pos);
      Pos1:=System.Pos('"',Str);
      if Pos1>0 then begin
         Fznach:=Copy(Str,Pos1+1,System.Pos('"',
                Copy(Str,Pos1+1,Length(Str)))-1);
       end;
      {Fznach:=ScanStr(Str,Pos);}
      {ScanStr(Str,Pos);}

      Pos1:=Pos+Length(Fznach)+2;
      if ScanStr(Str,Pos1)=w_factor then begin
        I:=Pos1;
        while (Str[I]<>'[') and
          not ((Str[I]>='0') and
              (Str[I]<='9')) do Inc(I);
        if (Str[I]>='0') and
           (Str[I]<='9') then
          factor1:=GetNumFromWord(ScanStr(Str,Pos1));
          if GetNumError<>0 then begin
            ShowMessage('������� '+IntToStr(LastRuleNumber)+
              ': ������ � ������� �����������!');
          end;
          if (Str[I]='[') then begin
            Pos1:=I+1;
            factor1:=GetNumFromWord(ScanStr(Str,Pos1));
            if GetNumError<>0 then begin
              ShowMessage('������ � ������� �����������!');
            end;
            I:=Pos1;
            while not ((Str[I]>='0') and
                      (Str[I]<='9')) do Inc(I);
            Pos1:=I;
            factor2:=GetNumFromWord(ScanStr(Str,Pos1));
            if GetNumError<>0 then begin
              ShowMessage('������� '+IntToStr(LastRuleNumber)+
                ': ������ � ������� �����������!');
            end;
            Inc(Pos1);
          end;
          Pos:=Pos1;
      end;
      Pos1:=Pos;
      if ScanStr(Str,Pos1)=w_accuracy then begin
        accuracy:=GetNumFromWord(ScanStr(Str,Pos1));
        if GetNumError<>0 then begin
          ShowMessage('������� '+IntToStr(LastRuleNumber)+
            ': ������ � ��������!');
        end;
      end;
  end;


  procedure TAssertion.Save(var F: Text; Bot: String);
  var
    p: String;
  begin
    case predicat of
      0:p:='=';
      1:p:='>';
      2:p:='<';
      3:p:='>=';
      4:p:='<=';
      5:p:='<>';
    else
      p:='=';
    end;
    Writeln(F,w_obj,n_obj,'.',w_attr,n_attr,p,'"',
      Fznach,'" ',w_factor,' [',factor1,';',factor2,'] ',
    w_accuracy,' ',accuracy,Bot);
  end;

  procedure TAssertion.AddTrassa(Trassa: TStrings; Bot: String);
  var
    s: array[0..255] of Char;
  begin
    StrPCopy(s,w_obj+IntToStr(n_obj)+StrPas('.')+
    StrPAs(w_attr)+IntToStr(n_attr)+StrPas('="')+
      Fznach+StrPas('" ')+StrPas(w_factor)+StrPas(' [')+IntToStr(factor1)+
    ';'+IntToStr(factor2)+StrPas('] ')+
    StrPas(w_accuracy)+StrPas(' ')+IntToStr(accuracy)+Bot);
    Trassa.Add(StrPas(s));
  end;

  function TAssertion.GetZnach: String;
  begin
    //if Ptr_attr is record_attr then
      //if record_attr(Ptr_attr).PtrType.type_znach=3 then GetDefMF(Self);
    GetZnach:=Fznach;
  end;

  procedure TAssertion.SetZnach(Value: String);
  begin
    Fznach:=Value;
//    if Ptr_attr is record_attr then
//      if record_attr(Ptr_attr).PtrType.type_znach=3 then GetFuzMF(Self);
  end;

  constructor TAssertion.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     {Self:=TAssertion(ACollection.Add);}
     SetDefault;
     FlagCreate:=True;

     //*** ��������� �������� �. �. ***//
//     fMembershipFunction:=TMembershipFunction.Create (Self, TMFPoint);
     //*** ------------------------ ***//
     Step:=0;

  end;

  destructor TAssertion.Destroy;
  begin
    //*** ��������� �������� �. �. ***//
//    fMembershipFunction.Destroy;
    //*** ------------------------ ***//
    Inherited Destroy;
  end;

  //*** ��������� �������� �. �. ***//

{  procedure TAssertion.SetMF (iValue : TMembershipFunction);
  begin
    if (iValue<>Nil) then begin
      fMembershipFunction.Clear;
      fMembershipFunction.GoodAssign (iValue);
    end
    else begin
      // ��������� �������������� ��������
    end;
  end;

  function TAssertion.GetMF : TMembershipFunction;
  begin
    GetMF:=fMembershipFunction;
  end;
}
  //*** ------------------------ ***//

  constructor list.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     {Self:=list(ACollection.Add);}
     {F}PtrAttr:=NIL;
     {F}last:=NIL;
  end;

  destructor list.Destroy;
  begin
    Inherited Destroy;
  end;

  procedure record_type.SetDefault;
  begin
    Inherited SetDefault;
    n_type     := 0;
    k_znach    := 0;
    type_znach := 0;
    min_znach  := StrPas('00000000');
    max_znach  := StrPas('00000000');

    if Flist_znach<>NIL
    then Flist_znach.Clear
    else Flist_znach:=TKBOwnedCollection.Create(Self,Value);

//   if FMFList<>NIL then FMFList.Clear
//   else FMFList:=TMFList{TKBOwnedCollection}.Create(Self,TMFOwner);

  end;

  constructor record_type.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: record_type;
    PtrValue: value;
    {CountZnach: Integer;}
    Flag: Boolean;
  begin
    FlagCreate:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if (LastWord(Str,Pos)=w_obj)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_rule) then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_dict)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_type then begin
         Self:=record_type(ACollection.Add);
         n_type:=GetNumFromWord(LastWord(Str,Pos));
         LastTypeNumber:=n_type;
         if GetNumError<>0 then begin
           ShowMessage('������ � ������ ����!');
           n_type:=ACollection.Count;
         end;
         comment:=w_type+IntToStr(n_type);
         if eof(F) then break;
         FlagRead:=True;
         ReadStr(F,Str,Pos);
         if LastWord(Str,Pos)=w_num then begin
           type_znach:=1;
           FlagRead:=True;
           ReadStr(F,Str,Pos);
           if LastWord(Str,Pos)=w_from then begin
             min_znach:=LastWord(Str,Pos);
             FlagRead:=True;
             ReadStr(F,Str,Pos);
           end;
           if LastWord(Str,Pos)=w_to then begin
             max_znach:=LastWord(Str,Pos);
             FlagRead:=True;
             ReadStr(F,Str,Pos);
           end;
         end else Pos:=1;
         if LastWord(Str,Pos)=w_str then begin

           type_znach:=2;
           Flist_znach:=TKBOwnedCollection.Create(Self,value);

           Flag:=False;
           {CountZnach:=0;}
           FlagRead:=True;
           PtrValue:=value.Load(F,Flist_znach);
           {PtrValue.n_znach:=CountZnach;}
           while not(Eof(F)) and FlagCreate1 do begin
             {Inc(CountZnach);
             PtrValue.n_znach:=CountZnach;}
//             list_znach.Add(PtrValue);
             Flag:=False;
             PtrValue:=value.Load(F,Flist_znach);
             Flag:=True;
           end;
//           if FlagCreate1 then begin
             {Inc(CountZnach);
             PtrValue.n_znach:=CountZnach;}
//             list_znach.Add(PtrValue);
//           end;
           Pos:=1;
         end else Pos:=1;
         if LastWord(Str,Pos)=w_fuzzy then begin
//           MFList.Load(F,comment);
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         if LastWord(Str,Pos)=w_comment then begin
           comment:=Copy(Str,Pos+1,Length(Str));
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         break;
      end;
      FlagRead:=True;
    end;
  end;

  constructor record_type.Import(N: Integer; ACollection: TCollection);
  var
    PtrType: TYPE_;
    PtrValue: Aspr_str.LIST_ZNACH;
    Ptr: value;
    I: Integer;
  begin
    Self:=record_type(ACollection.Add);
    PtrType    :=  set_type;
    for i:=1 to N-1 do PtrType:=PtrType^.next;
    n_type     :=  PtrType^.n_type;
    k_znach    :=  0;
    type_znach :=  PtrType^.type_znach;
    min_znach:=PtrType^.min_znach;
    max_znach:=PtrType^.max_znach;
    comment    :=  StrPas(DelSpaces(PtrType^.t_comment));

    PtrValue   :=  PtrType^.list_znach;
    I:=0;
    while PtrValue<>NIL do begin
      Inc(I);
      Ptr:=value.Create(Flist_znach);
      with Ptr do begin
        Fznach:=StrPas(DelSpaces(PtrValue^.znach));
      end;
      PtrValue:=PtrValue^.next;
    end;
    k_znach:=I;
  end;

  procedure record_type.Export(N: Integer);
  var
    PtrType: TYPE_;
    PtrValue: Aspr_str.LIST_ZNACH;
    I: Integer;
  begin
    PtrType:=new_record_type;
    PtrType^.n_type     := n_type;
    PtrType^.k_znach    := k_znach;
    PtrType^.type_znach := type_znach;
    PtrType^.min_znach  := min_znach;
    PtrType^.max_znach  := max_znach;
    PtrType^.t_comment  := comment;
    for I:=1 to Flist_znach.Count do begin
      PtrValue:=new_record_cur_dict(PtrType);
      PtrValue^.n_znach := I;
      PtrValue^.znach   := value(Flist_znach.Items[I-1]).Fznach;
      PtrValue^.type_znach :=0;
      PtrValue^.k_types    :=0;
    end;
  end;

  procedure record_type.Save(var F: Text);
  var
    i: Integer;
  begin
    Writeln(F,w_type+' '+w_type,n_type);
    if type_znach=1 then begin
       Writeln(F,w_num);
       Writeln(F,w_from,' ',min_znach);
       Writeln(F,w_to  ,' ',max_znach);
    end else begin
      Writeln(F,w_str);
      for i:=1 to Flist_znach.Count do begin
        value(Flist_znach.Items[i-1]).Save(F);
      end;
    end;
{    if MFList.Count>0 then begin
      Writeln(F,w_fuzzy);
      MFList.Save(F);
    end;}
    Writeln(F,w_comment+' '+comment);
  end;

  constructor record_type.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     {Self:=record_type(ACollection.Add);}
     SetDefault;
     FlagCreate:=True;
  end;

  destructor record_type.Destroy;
  var
    cur_v: value;
    I:Integer;
  begin
    if Flist_znach<>NIL then begin
      Flist_znach.Destroy;
//       FMFList.Destroy;
    end;
    Inherited Destroy;
  end;

  function record_type.GetK_Znach: Integer;
  begin
    GetK_Znach:=Flist_znach.Count;
  end;

  procedure record_rule.Assign(KBItem: TKBItem);
  var
    S,D: record_rule;
    i: Integer;
    Ptr: list;
  begin
    Inherited Assign(KBItem);
    S:=record_rule(KBItem);
    D:=Self;
    D.n_rule:=S.n_rule;
    D.k_znach_if:=S.k_znach_if;
    D.k_znach_then:=S.k_znach_then;
    D.k_znach_else:=S.k_znach_else;
    StrCopy(D.incident,S.incident);
    StrCopy(D.consequent,S.consequent);
    StrCopy(D.consequent_else,S.consequent_else);
    if D.Flist_attr_if<>NIL
    then D.Flist_attr_if.Clear else
      D.Flist_attr_if:=TKBOwnedCollection.Create(Self,list);

    if D.Flist_attr_then<>NIL
    then D.Flist_attr_then.Clear else
      D.Flist_attr_then:=TKBOwnedCollection.Create(Self,list);

    if D.Flist_attr_else<>NIL
    then D.Flist_attr_else.Clear else
      D.Flist_attr_else:=TKBOwnedCollection.Create(Self,list);

    for i:=1 to S.Flist_attr_if.Count do begin
      Ptr:=list(D.Flist_attr_if.Add);
      Ptr.Assign(list(S.Flist_attr_if.Items[i-1]));
    end;
    for i:=1 to S.Flist_attr_then.Count do begin
      Ptr:=list(D.Flist_attr_then.Add);
      Ptr.Assign(list(S.Flist_attr_then.Items[i-1]));
    end;
    for i:=1 to S.Flist_attr_else.Count do begin
      Ptr:=list(D.Flist_attr_else.Add);
      Ptr.Assign(list(S.Flist_attr_else.Items[i-1]));
    end;
  end;

  procedure record_rule.SetDefault;
  begin
    Inherited SetDefault;
    n_rule:=0;
    k_znach_if:=0;
    k_znach_then:=0;
    k_znach_else:=0;
    incident[0]:=#0;
    consequent[0]:=#0;

    if list_attr_if<>NIL
    then list_attr_if.Destroy;
    Flist_attr_if:=TKBOwnedCollection.Create(Self,list);

    if list_attr_then<>NIL
    then list_attr_then.Destroy;
    Flist_attr_then:=TKBOwnedCollection.Create(Self,list);

    if list_attr_else<>NIL
    then list_attr_else.Destroy;
    Flist_attr_else:=TKBOwnedCollection.Create(Self,list);

  end;

  function record_rule.GetIncident: String;
  begin
    GetIncident:=String(Incident);
  end;

  procedure record_rule.PutIncident(Value: String);
  begin
    StrPCopy(Incident,Value);
  end;

  function record_rule.GetConsequent: String;
  begin
    GetConsequent:=String(Consequent);
  end;

  procedure record_rule.PutConsequent(Value: String);
  begin
    StrPCopy(Consequent,Value);
  end;

  function record_rule.GetConsequent_else: String;
  begin
    GetConsequent_else:=String(Consequent_else);
  end;

  procedure record_rule.PutConsequent_else(Value: String);
  begin
    StrPCopy(Consequent_else,Value);
  end;

  {procedure gen_incident(list_attr_if: TList;
                         incident: PChar);
  var
    PtrValue: list;
    p: PChar;
    Flag: Byte;
    i,n,l: Integer;
  begin
    if (list_attr_if<>NIL) and (list_attr_if.Count>0) then begin
      l:=StrLen(incident)-1;
      for i:=0 to l do incident[i]:=#0;
      l:=list_attr_if.Count-1;
      for i:=0 to 0 do begin
        PtrValue:=list(list_attr_if.Items[i]);
        n:=1;
        p:=incident;
        if ( PtrValue.n_layer=1 ) then begin
          StrPCopy(p,'P'+IntToStr(n));
          p:=p+StrLen(p);
          flag:=1;
        end else begin
          StrPCopy(p,'(P'+IntToStr(n)+')');
          p:=p+StrLen(p);
          flag:=2;
        end;
      end;
      for i:=1 to l do begin
        PtrValue:=list(list_attr_if.Items[i]);
        Inc(n);
        if ( PtrValue.n_layer=1 ) then begin
          if ( flag=2 ) then begin
            StrPCopy(p,')');
            p:=p+StrLen(p);
          end;
          StrPCopy(p,' .and. P'+IntToStr(n));
          p:=p+StrLen(p);
          flag:=1;
        end else begin
          if ( flag=1 ) then begin
            StrPCopy(p,' .and. (P'+IntToStr(n));
            p:=p+StrLen(p);
            flag:=2;
          end else begin
            StrPCopy(p,' .or. P'+IntToStr(n));
            p:=p+StrLen(p);
            flag:=2;
          end;
        end;
        if ( flag=2 ) then begin
            StrPCopy(p,')');
            p:=p+StrLen(p);
        end;
      end;
    end;
  end;}

  {procedure LoadIncident(var F: Text;
                           list_attr_if: TList;
                           incident: PChar);
  var
    PtrValue: list;
    Flag: Boolean;

  begin

    Flag:=False;
    FlagRead:=True;
    PtrValue:=list.Load(F);
    while not(Eof(F)) and FlagCreate1 do begin
      PtrValue.n_layer:=1;
      list_attr_if.Add(PtrValue);
      Flag:=False;
      PtrValue:=list.Load(F);
      Flag:=True;
    end;
    if FlagCreate1 then begin
      PtrValue.n_layer:=1;
      list_attr_if.Add(PtrValue);
    end;

    gen_incident(list_attr_if,incident);

  end;}

  procedure LoadIncident1(var F: Text;
                           list_attr_if: TCollection;
                           incident: PChar; ACollection: TCollection);
  var
    incid: TTextStr;
    Pos,SPos,i,j,j1,j2: Integer;
    Str1,Str5: String;
    Str2: array[0..255] of Char;
    Str3,Str4: PChar;
    PtrValue: list;
    PosBound1, PosBound2: Integer;
    FlagSpace: Boolean;
  begin
    i:=0;
    Str3:=incident;
    j1:=0;
    j2:=0;
    FlagCreate1:=True;
    FlagRead:=True;
    FlagSpace:=True;
    while not(Eof(F)) and FlagCreate1 do  begin
      Pos:=1;
      ReadStr(F,Str,Pos);
      Str1:=Str;
      Pos:=1;
      j:=0;
      PosBound1:=System.Pos('"',Str1);
      StrPCopy(Str2,Str1);
      PosBound2:=System.Pos('"',StrPas(Str2+PosBound1))+PosBound1;
      while Pos<=Length(Str1) do begin
        case Str1[Pos] of
          {'+','-','*','/',}'(',')',' '{,'<','>','='},'&','|','~': begin
            if (Pos<PosBound1) or (Pos>PosBound2) then begin
              if Str1[Pos]='(' then begin Inc(J1); end;
              if Str1[Pos]=')' then Inc(J2);
              if FlagSpace or (Str1[Pos]<>' ') then begin
                Str2[j]:=Str1[Pos];
                Inc(j);
              end;
              if Str1[Pos]=' '
              then FlagSpace:=False
              else FlagSpace:=True;
            end;
            Inc(Pos);
          end;
          else begin
            Inc(i);
            Str4:=Str2+j;
            StrPCopy(Str4,' P'+IntToStr(i));
            j:=j+StrLen(Str4);
            SPos:=Pos;
            Str5:=LastWord(Str1,Pos);
            if (Str5<>w_then) and
               (Str5<>w_else) and
               (Str5<>w_comment) then begin
              PtrValue:=list.LoadFromStr(Str1,SPos,list_attr_if);
              PtrValue.n_layer:=J1-J2;
              {list_attr_if.Add(PtrValue);}
              FlagRead:=True;
            end else begin
              FlagRead:=False;
              FlagCreate1:=False;
              break;
            end;
            while Pos<=Length(Str1) do begin
              case Str1[Pos] of
                {'+','-','*','/',}'(',')',' '{,'<','>','='},'&','|','~': begin
                  if (Pos<PosBound1) or (Pos>PosBound2) then begin
                    if FlagSpace or (Str1[Pos]<>' ') then begin
                      Str2[j]:=Str1[Pos];
                      if Str1[Pos]=' '
                      then FlagSpace:=False
                      else FlagSpace:=True;
                      Inc(j);
                    end;
                    if Str1[Pos]='(' then Inc(J1);
                    if Str1[Pos]=')' then Inc(J2);
                  end;
                  Inc(Pos);
                end;
                else begin
                  Inc(Pos);
                end;
            end;
          end;
        end;
      end;
      Str2[j]:=#0;
      {Pos:=1;
      Str5:=LastWord(Str1,Pos);}
      if (Str5<>w_then) and
         (Str5<>w_else) and
         (Str5<>w_comment) then begin
         StrCat(Str3,Str2);
         Str3:=Str3+StrLen(Str2);
         j:=0;
      end else begin
        FlagRead:=False;
        Exit;
      end;
      end;
    end;
  end;

  constructor record_rule.Load(var F: Text; ACollection: TCollection);
  var
    Ptr: record_rule;
    PtrValue: list;
    Flag: Boolean;
  begin
    FlagCreate:=False;
    while not(eof(F)) do begin
      ReadStr(F,Str,Pos);
      if LastWord(Str,Pos)=w_type then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_obj)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_conn)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if (LastWord(Str,Pos)=w_dict)  then begin
         Pos:=1; break;
      end else
         Pos:=1;
      if LastWord(Str,Pos)=w_rule then begin
         FlagCreate:=True;
         Self:=record_rule(ACollection.Add);
         n_rule:=GetNumFromWord(LastWord(Str,Pos));
         LastRuleNumber:=n_rule;
         if GetNumError<>0 then begin
           ShowMessage('������ � ������ �������!');
           n_rule:=ACollection.Count;
         end;
         comment:=w_rule+IntToStr(n_rule);
         if eof(F) then break;
         FlagRead:=True;
         ReadStr(F,Str,Pos);

         if LastWord(Str,Pos)=w_if then begin
           if list_attr_if=NIL then
             Flist_attr_if:=TKBOwnedCollection.Create(Self,list);
           LoadIncident1(F,list_attr_if,incident,ACollection);
           Pos:=1;
         end else Pos:=1;

         if LastWord(Str,Pos)=w_then then begin
           if list_attr_then=NIL then
             Flist_attr_then:=TKBOwnedCollection.Create(Self,list);

           Flag:=False;
           FlagRead:=True;
           PtrValue:=list.Load(F, list_attr_then);
           while not(Eof(F)) and FlagCreate1 do begin
             {list_attr_then.Add(PtrValue);}
             Flag:=False;
             PtrValue:=list.Load(F, list_attr_then);
             Flag:=True;
           end;
           {if FlagCreate1 then begin
             list_attr_then.Add(PtrValue);
           end;}
           Pos:=1;
         end else Pos:=1;

         if LastWord(Str,Pos)=w_else then begin
           if list_attr_else=NIL then
             Flist_attr_else:=TKBOwnedCollection.Create(Self,list);

           Flag:=False;
           FlagRead:=True;
           PtrValue:=list.Load(F, list_attr_else);
           while not(Eof(F)) and FlagCreate1 do begin
             {list_attr_else.Add(PtrValue);}
             Flag:=False;
             PtrValue:=list.Load(F, list_attr_else);
             Flag:=True;
           end;
           {if FlagCreate1 then begin
             list_attr_else.Add(PtrValue);
           end;}
           Pos:=1;
         end else Pos:=1;

         if LastWord(Str,Pos)=w_comment then begin
           comment:=Copy(Str,Pos+1,Length(Str));
           FlagRead:=True;
           ReadStr(F,Str,Pos);
         end else Pos:=1;
         break;
      end;
      FlagRead:=True;
    end;
  end;

  function record_rule.GetK_Znach_if: Integer;
  begin
    GetK_Znach_if:=Flist_attr_if.Count;
  end;

  function record_rule.GetK_Znach_then: Integer;
  begin
    GetK_Znach_then:=Flist_attr_then.Count;
  end;

  function record_rule.GetK_Znach_else: Integer;
  begin
    GetK_Znach_else:=Flist_attr_else.Count;
  end;

procedure DecodeIncident(Rule : record_rule;
      IncidentText:     TStringList); // Developed by Lisa
var
   i,j,k,n : Integer;
   t: PChar;
   S: array[0..255] of Char;
   p: String;

begin
  j := 0;
  t:=Rule.Incident;
  IncidentText.Add('');
  while (t[j]<>#0) do begin
    if (t[j]=')') or
       (t[j]='(') or
       (t[j]=#32) then begin
      IncidentText.Strings[IncidentText.Count-1] :=
        IncidentText.Strings[IncidentText.Count-1]+t[j];
      if (t[j]=')') and (t[j+1]<>#0) then
        IncidentText.Strings[IncidentText.Count-1] :=
          IncidentText.Strings[IncidentText.Count-1]+' ';
     Inc(j);
     while (t[j]= #32) do begin
        Inc(j);
      end;
    end else
    if (t[j]='|') or
       (t[j]='&') or
       (t[j]='~') then begin
      IncidentText.Strings[IncidentText.Count-1] :=
        IncidentText.Strings[IncidentText.Count-1]+t[j];
      if (t[j]<>'~') then IncidentText.Add('');
      Inc(j);
    end else
    if (t[j]='P') then begin
      i:=j+1;
      k:=0;
      while (t[i]>= '0' ) and
            (t[i]<= '9') do begin
        S[k]:=t[i];
        Inc(i);
        Inc(k);
      end;
      S[k]:=#0;
      n:=StrToInt(StrPas(S));
      with list(Rule.list_attr_if.Items[n-1]) do begin
        case predicat of
          0:p:='=';
          1:p:='>';
          2:p:='<';
          3:p:='>=';
          4:p:='<=';
          5:p:='<>';
          else
          p:='=';
        end;
      IncidentText.Strings[IncidentText.Count-1] :=
        IncidentText.Strings[IncidentText.Count-1]+
        w_obj+IntToStr(n_obj)+'.'+
        w_attr+IntToStr(n_attr)+p+'"'+
        Fznach+'" '+
        w_factor+' ['+
        IntToStr(factor1)+';'+
        IntToStr(factor2)+'] '+
        w_accuracy+' '+IntToStr(accuracy);
      end;
      j:=j+1+StrLen(S);
    end;
  end;
end;

procedure SaveIncident(var F: Text; Rule : record_rule);
var
  IncidentText:     TStringList;
  i: Integer;
begin
  IncidentText:=TStringList.Create;
  DecodeIncident(Rule,IncidentText);
  for i:=1 to IncidentText.Count do Writeln(F, IncidentText.Strings[i-1]);
  IncidentText.Destroy;
end;

  procedure record_rule.Save(var F: Text);
  var
    i: Integer;
    Bot: String;
  begin
    Writeln(F,w_rule+' '+w_rule,n_rule);
    if list_attr_if.Count>0 then Writeln(F,w_if);
     {for i:=1 to list_attr_if.Count do begin
      if i<>list_attr_if.Count then Bot:=' &' else Bot:='';
      list(list_attr_if.Items[i-1]).Save(F,Bot);
    end;}
    SaveIncident(F, Self);
    Bot:='';
    if list_attr_then.Count>0 then Writeln(F,w_then);
    for i:=1 to list_attr_then.Count do begin
      list(list_attr_then.Items[i-1]).Save(F,Bot);
    end;
    if list_attr_else.Count>0 then Writeln(F,w_else);
    for i:=1 to list_attr_else.Count do begin
      list(list_attr_else.Items[i-1]).Save(F,Bot);
    end;
    Writeln(F,w_comment+' '+comment);
  end;

  procedure record_rule.AddTrace(Trace: TStrings);
  begin
    Trace.Add(IntToStr(n_rule));
  end;

  procedure record_rule.AddTrassa(Trassa: TStrings);
  var
    i: Integer;
    Bot: String;
    s: array[0..255] of Char;
    IncidentText:     TStringList;
  begin
    StrPCopy(s,w_rule+StrPas(' R')+IntToStr(n_rule));
    Trassa.Add(StrPas(s));
    if list_attr_if.Count>0 then begin
      Trassa.Add(w_if);
    end;
    IncidentText:=TStringList.Create;
    DecodeIncident(Self,IncidentText);
    Trassa.AddStrings(IncidentText);
    IncidentText.Destroy;
    Bot:='';
    if list_attr_then.Count>0 then begin
      Trassa.Add(w_then);
    end;
    for i:=1 to list_attr_then.Count do begin
      list(list_attr_then.Items[i-1]).AddTrassa(Trassa,Bot);
    end;
    if list_attr_else.Count>0 then begin
      Trassa.Add(StrPas(w_else));
    end;
    for i:=1 to list_attr_else.Count do begin
      list(list_attr_else.Items[i-1]).AddTrassa(Trassa,Bot);
    end;
    StrPCopy(s,w_comment+' '+comment);
    Trassa.Add(StrPas(s));
  end;

  function set_znach(PtrList: LIST_ATTR): String;
  var
    PtrType: TYPE_;
    I: Integer;
    Ptr: LIST_ZNACH;
  begin
    PtrType:=find_type_n(set_type,
          find_attr(set_attr, PtrList^.n_attr)^.n_type);
    Ptr:=PtrType^.list_znach;
    for I:=2 to PtrList^.n_znach do Ptr:=Ptr^.next;
    set_znach:=Ptr^.znach;
  end;

  function ret_dict_n(PtrObjs, PtrTypes: TCollection; n_obj,n_attr: Integer; s: String): Integer;
  var
    PtrObj:  record_obj;
    PtrType: record_type;
    I,J: Integer;
    Ptr: value;
    n_type: Integer;
  begin
    for I:=1 to PtrObjs.Count do begin
      PtrObj:=record_obj(PtrObjs.Items[I-1]);
      if PtrObj.n_obj=n_obj then begin
        for J:=1 to PtrObj.ListAttrs.Count do begin
          if record_attr(PtrObj.ListAttrs.Items[J-1]).n_attr=n_attr then begin
            n_type:=record_attr(PtrObj.ListAttrs.Items[J-1]).n_type;
            break;
          end;
        end;
        break;
      end;
    end;
    PtrType:=NIL;
    for I:=1 to PtrTypes.Count do begin
      if record_type(PtrTypes.Items[I-1]).n_type=n_type then begin
        PtrType:=record_type(PtrTypes.Items[I-1]);
        break;
      end;
    end;
    if PtrType<>NIL then
      for I:=1 to PtrType.Flist_znach.Count do begin
        if value(PtrType.Flist_znach.Items[I-1]).Fznach=s then begin
          ret_dict_n:=I;
          break;
        end;
      end
    else
      ret_dict_n:=0;
  end;

  constructor record_rule.Import(N: Integer; ACollection: TCollection);
  var
    PtrRule: RULE;
    PtrList: LIST_ATTR;
    Ptr: list;
    I: Integer;
  begin
    Self:=record_rule(ACollection.Add);
    PtrRule:=set_rule;
    for i:=1 to N-1 do PtrRule:=PtrRule^.next;
    n_rule         := PtrRule^.n_rule;
    k_znach_if     := 0;
    k_znach_then   := 0;
    k_znach_else   := 0;
    StrCopy(incident,    PtrRule^.incident  );
    StrCopy(consequent,  PtrRule^.consequent);
    StrCopy(consequent_else,'');
    comment        := '';

    PtrList   :=  PtrRule^.list_attr_if;
    I:=0;
    while PtrList<>NIL do begin
      Inc(I);
      Ptr:=list.Create(list_attr_if);
      with Ptr do begin
        n_obj     :=  find_attr(set_attr, PtrList^.n_attr)^.n_obj;
        n_attr    :=  PtrList^.n_attr;
        znach     :=  StrPas(DelSpaces(set_znach(PtrList)));
        n_layer   :=  PtrList^.n_layer;
        factor1   :=  PtrList^.factor1;
        factor2   :=  PtrList^.factor2;
        accuracy  :=  PtrList^.accuracy;
        predicat  :=  PtrList^.predicat;
      end;
      k_znach_if:=I;
      {list_attr_if.Add(Ptr);}
      PtrList:=PtrList^.next;
    end;

    PtrList   :=  PtrRule^.list_attr_then;
    I:=0;
    while PtrList<>NIL do begin
      Inc(I);
      Ptr:=list.Create(list_attr_then);
      with Ptr do begin
        n_obj     :=  find_attr(set_attr, PtrList^.n_attr)^.n_obj;
        n_attr    :=  PtrList^.n_attr;
        znach     :=StrPas(DelSpaces(set_znach(PtrList)));
        n_layer   :=  PtrList^.n_layer;
        factor1   :=  PtrList^.factor1;
        factor2   :=  PtrList^.factor2;
        accuracy  :=  PtrList^.accuracy;
        predicat  :=  PtrList^.predicat;
      end;
      k_znach_then:=I;
      {list_attr_then.Add(Ptr);}
      PtrList:=PtrList^.next;
    end;

  end;

  procedure record_rule.Export(PtrObjs,PtrTypes: TCollection; N: Integer);
  var
    PtrRule: RULE;
    PtrList: LIST_ATTR;
    Ptr: list;
    I: Integer;
  begin
    PtrRule:=new_record_rule;
    PtrRule^.n_rule        := n_rule;
    PtrRule^.k_znach_if    := k_znach_if;
    PtrRule^.k_znach_then  := k_znach_then;
    StrCopy(PtrRule^.incident  ,incident  );
    StrCopy(PtrRule^.consequent,consequent);

    for I:=1 to list_attr_if.Count do begin
      Ptr:=list(list_attr_if.Items[I-1]);
      PtrList:=insert_record_znach(PtrRule,1);
      with Ptr do begin
        PtrList^.n_attr     := n_attr;
        PtrList^.n_znach    :=
           ret_dict_n(PtrObjs,PtrTypes,n_obj,n_attr,Fznach);
        PtrList^.n_layer    := n_layer;
        PtrList^.factor1    := factor1;
        PtrList^.factor2    := factor2;
        PtrList^.accuracy   := accuracy;
        PtrList^.predicat   := predicat;
      end;
    end;

    for I:=1 to list_attr_then.Count do begin
      Ptr:=list(list_attr_then.Items[I-1]);
      PtrList:=insert_record_znach(PtrRule,2);
      with Ptr do begin
        PtrList^.n_attr     := n_attr;
        PtrList^.n_znach    :=
          ret_dict_n(PtrObjs,PtrTypes,n_obj,n_attr,Fznach);
        PtrList^.n_layer    := n_layer;
        PtrList^.factor1    := factor1;
        PtrList^.factor2    := factor2;
        PtrList^.accuracy   := accuracy;
        PtrList^.predicat   := predicat;
      end;
    end;

  end;

  constructor record_rule.Create(ACollection: TCollection);
  begin
     inherited Create(ACollection);
     {Self:=record_rule(ACollection.Add);}
     list_attr_if  :=NIL;
     list_attr_then:=NIL;
     list_attr_else:=NIL;
     SetDefault;
     FlagCreate:=True;
  end;

  destructor record_rule.Destroy;
  var
    cur_l: list;
    I:Integer;
  begin
    I:=1;
    if list_attr_if<>NIL then begin
      while list_attr_if.Count>0 do begin
        cur_l:=list(list_attr_if.Items[I-1]);
        {list_attr_if.Delete(I-1);}
        cur_l.Destroy;
      end;
      list_attr_if.Destroy;
    end;
    if list_attr_then<>NIL then begin
      while list_attr_then.Count>0 do begin
        cur_l:=list(list_attr_then.Items[I-1]);
        {list_attr_then.Delete(I-1);}
        cur_l.Destroy;
      end;
      list_attr_then.Destroy;
    end;
    if list_attr_else<>NIL then begin
      while list_attr_else.Count>0 do begin
        cur_l:=list(list_attr_else.Items[I-1]);
        {list_attr_else.Delete(I-1);}
        cur_l.Destroy;
      end;
      list_attr_else.Destroy;
    end;
    Inherited Destroy;
  end;

  procedure record_rule.Verify(Protocol: TStrings);
  var
    i,j,k: Integer;
    cur_lif: list;
    cur_l: list;
    ProtocolStr: String;
    CurProtocol: TStrings;
    StrIncident: PChar;
    Str2, Str3,
    Str1: PChar;
    Number: Real;
    MaxResult: Real;// �������� ������ ��������������
  begin
     IsVerified:=True;
     if not Assigned(Protocol) then CurProtocol:=TStringList.Create else
       CurProtocol:=Protocol;
     inherited Verify(CurProtocol);
     if not IsVerified then Exit;
     if (list_attr_if.Count=0) then begin
          ProtocolStr:='� ������� ������� ��� �����������:'+
          ' R'+IntToStr(Self.n_rule)+
            '('+Self.Comment+')';
       if Assigned(CurProtocol) then begin
         if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
           CurProtocol.Add(ProtocolStr);
           if MessageDlg(ProtocolStr,
             mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
         end;
       end else begin
       if MessageDlg(ProtocolStr,
         mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
       end;
     end;
     if (list_attr_then.Count=0) then begin
          ProtocolStr:='� �������� ������� ��� �����������:'+
          ' R'+IntToStr(Self.n_rule)+
            '('+Self.Comment+')';
       if Assigned(CurProtocol) then begin
         if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
           CurProtocol.Add(ProtocolStr);
           if MessageDlg(ProtocolStr,
           mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
         end;
       end else begin
       if MessageDlg(ProtocolStr,
         mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
       end;
     end;
     for i:=1 to list_attr_if.Count do begin
      cur_lif:=list(list_attr_if.Items[i-1]);
      for j:=1 to list_attr_then.Count do begin
        cur_l:=list(list_attr_then.Items[j-1]);
        if (cur_lif.n_obj=cur_l.n_obj) and
           (cur_lif.n_attr=cur_l.n_attr) then begin
          ProtocolStr:='������� '+'A'+IntToStr(cur_lif.n_attr)+
            ' ����������� � ������� � � �������� �������: '+
            ' R'+IntToStr(Self.n_rule)+
            '('+Self.Comment+')';
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              CurProtocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end;
      end;
    end;
    for i:=1 to list_attr_if.Count do begin
      cur_lif:=list(list_attr_if.Items[i-1]);
      for j:=1 to list_attr_else.Count do begin
        cur_l:=list(list_attr_else.Items[j-1]);
        if (cur_lif.n_obj=cur_l.n_obj) and
           (cur_lif.n_attr=cur_l.n_attr) then begin
          ProtocolStr:='������� '+'A'+IntToStr(cur_lif.n_attr)+
            ' ����������� � ������� � � �������� �������: '+
            ' R'+IntToStr(Self.n_rule)+
            '('+Self.Comment+')';
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              CurProtocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end;
      end;
    end;
    StrIncident:=AllocMem(MAX_LINE+1);
    Str2:=AllocMem(MAX_LINE+1);
    Str3:=AllocMem(MAX_LINE+1);
    Str1:=AllocMem(256);
    StrCopy(StrIncident,incident);
    Number:=1;
    for j:=1 to list_attr_if.Count do begin
      StrPCopy(Str1,'P'+IntToStr(J));
      StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
      StrLCopy(Str3,StrIncident,
      StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
      StrCopy(StrIncident,Str3);
      StrCat(StrIncident,PChar(FloatToStr(Number)));
      StrCat(StrIncident,Str2);
    end;
    if list_attr_if.Count>0 then Logic(StrIncident,MaxResult);
    FreeMem(StrIncident);
    FreeMem(Str2);
    FreeMem(Str3);
    FreeMem(Str1);
    if not Assigned(Protocol) then CurProtocol.Destroy;
  end;

  procedure record_say.SetDefault;
  begin
    _row1      := 0;
    _colm1     := 0;
    _attr      := 0;
    _fon       := 0;
    say_string := '';
  end;

  constructor record_say.Create;
  begin
     Inherited Create;
     SetDefault;
     FlagCreate:=True;
  end;

  destructor record_say.Destroy;
  begin
     Inherited Destroy;
  end;

  procedure TKnowledgeBase.SetDefault;
  begin
    FListTypes.Clear;
    FListObjs.Clear;
    FListRules.Clear;
    FListConns.Clear;
    FListList.Clear;
    {KBInterfaceCollection.Clear;}
    CalcMethod:=Demster;
    Style:=2;
    {if FKBInterfaceCollection.Count=0 then
      SetDefaultKBInterface:=FKBInterfaceCollection.Add
    else
      SetDefaultKBInterface:=FKBInterfaceCollection.Items[0];}
    {FKBInterface:=TKBInterface.Create(KBOwner);}
    SetDefaultKBInterface:=FKBInterface;
    OnInput:=TestDemon;
    OnOutput:=TestDemon;
  end;

  procedure SetPtrObjs(KB: TKnowledgeBase);
  var
    PtrObj: record_obj;
    PtrAttr: record_attr;
    i,j: Integer;
  begin
    with KB do begin
      for i:=1 to ListObjs.Count do begin
        PtrObj:=record_obj(ListObjs.Items[i-1]);
        for j:=1 to PtrObj.ListAttrs.Count do begin
          PtrAttr:=record_attr(PtrObj.ListAttrs.Items[j-1]);
          PtrAttr.{F}PtrObj:=PtrObj;
        end;
      end;
    end;
  end;

  procedure SetPtrTypes(KB: TKnowledgeBase);
  var
    I,J,K: Integer;
  begin
    with KB do begin
     for I:=1 to ListObjs.Count do
       for J:=1 to record_obj(ListObjs.Items[I-1]).ListAttrs.Count do begin
         for K:=1 to ListTypes.Count do begin
           if record_attr(record_obj(ListObjs.Items[I-1]).
                                      ListAttrs.Items[J-1]).n_type=
                    record_type(ListTypes.Items[K-1]).n_type then
             record_attr(record_obj(ListObjs.Items[I-1]).
                                      ListAttrs.Items[J-1]).{F}PtrType:=
             record_type(ListTypes.Items[K-1]);
         end;
       end;
    end;
  end;

  procedure SetPtrAttr(KB: TKnowledgeBase; ptr_list_attr: TCollection);
  var
    ptr_list: Aspr95.list;
    J,K,N: Integer;
  begin
    for J:=1 to ptr_list_attr.Count do begin
      ptr_list:=list(ptr_list_attr.Items[J-1]);
      for K:=1 to KB.ListObjs.Count do begin
        for N:=1 to record_obj(KB.ListObjs.Items[K-1]).ListAttrs.Count do begin
          if (list(ptr_list_attr.Items[J-1]).n_obj
             =record_attr(record_obj(KB.ListObjs.Items[K-1]).ListAttrs.Items[N-1]).n_obj) and
             (list(ptr_list_attr.Items[J-1]).n_attr
             =record_attr(record_obj(KB.ListObjs.Items[K-1]).ListAttrs.Items[N-1]).n_attr)
          then begin
            ptr_list.PtrAttr:=record_attr(record_obj(KB.ListObjs.Items[K-1]).ListAttrs.Items[N-1]);
          end;
        end;
      end;
    end;
  end;

  procedure SetPtrAttrs(KB: TKnowledgeBase);
  var
    PtrRule: record_rule;
    I: Integer;
  begin
    with KB do begin
      for I:=1 to ListRules.Count do begin
        PtrRule:=record_rule(ListRules.Items[I-1]);
        {Set PtrAttr}
        SetPtrAttr(KB, PtrRule.list_attr_if);
        SetPtrAttr(KB, PtrRule.list_attr_then);
        SetPtrAttr(KB, PtrRule.list_attr_else);
      end;
    end;  
  end;

  procedure TKnowledgeBase.LoadFromFile;
  var
    T:  System.Text;
    PtrType: record_type;
    PtrObj:  record_obj;
    PtrRule: record_rule;
    PtrList: record_conn;
    Flag: Boolean;
  begin
     if FileName='' then begin
       ShowMessage('�� ������ ��� �����');
       {Active:=False;}
       Exit;
     end;
     SetDefaultKBInterface:=FKBInterface;
     System.AssignFile(T, FileName);
     try
       System.Reset(T);
     except
       on e: Exception do begin
         ShowMessage('KB: �� ���� ��������� ����: '+FileName + #10#13 + e.Message);
         {Active:=False;}
         Exit;
       end;
     end;
     FlagRead:=True;
     Str:='';
     Pos:=1;

     Flag:=False;
     PtrType:=record_type.Load(T, ListTypes);
     while not(Eof(T)) and FlagCreate do begin
      {ListTypess.Add(PtrType);}
       PtrType:=record_type.Load(T, ListTypes);
       Flag:=True;
     end;
     if FlagCreate then begin
       {ListTypes.Add(PtrType);}
       FlagRead:=True;
     end;

     Flag:=False;
     PtrObj:=record_obj.Load(T, ListObjs);
     while not(Eof(T)) and FlagCreate do begin
       {ListObjs.Add(PtrObj);}
       PtrObj:=record_obj.Load(T, ListObjs);
       Flag:=True;
     end;
     if FlagCreate then begin
       {ListObjs.Add(PtrObj);}
       FlagRead:=True;
     end;

     Flag:=False;
     PtrRule:=record_rule.Load(T, ListRules);
     while not(Eof(T)) and FlagCreate do begin
       {ListRules.Add(PtrRule);}
       {Set PtrAttr}
       {SetPtrAttr(Self, PtrRule.list_attr_if);
       SetPtrAttr(Self, PtrRule.list_attr_then);
       SetPtrAttr(Self, PtrRule.list_attr_else);}
       PtrRule:=record_rule.Load(T, ListRules);
       Flag:=True;
     end;
     if FlagCreate then begin
       {ListRules.Add(PtrRule);}
       FlagRead:=True;
       {Set PtrAttr}
       {SetPtrAttr(Self, PtrRule.list_attr_if);
       SetPtrAttr(Self, PtrRule.list_attr_then);
       SetPtrAttr(Self, PtrRule.list_attr_else);}
     end;

     {SetPtrObjs(Self);}

     Flag:=False;
     PtrList:=record_conn.Load(T, ListConns);
     while not(Eof(T)) and FlagCreate do begin
       {ListConns.Add(PtrList);}
       PtrList:=record_conn.Load(T, ListConns);
       Flag:=True;
     end;
     if FlagCreate then begin
        {ListConns.Add(PtrList);}
        FlagRead:=True;
     end;

{     if Assigned(MFDictionary) then begin
       if not (Eof(T)) and (Pos=1) then
         if (LastWord(Str,Pos)=w_dict)  then begin
           MFDictionary.Dictionary.LoadFromFile(T);
         end;
     end;
}
     System.Close(T);

  end;

  procedure TKnowledgeBase.SetPointers;
  var
    PtrType: record_type;
    PtrObj:  record_obj;
    PtrRule: record_rule;
    PtrList: record_conn;
    I,J,K: Integer;
  begin
    SetPtrTypes(Self);
    SetPtrAttrs(Self);
    SetPtrObjs(Self);
  end;

  procedure TKnowledgeBase.Load;
  begin
    LoadFromFile;
    SetPointers;
  end;

  procedure TKnowledgeBase.Import(SESPath: String);
  var
    T:  System.Text;
    PtrObj:  record_obj;
    PtrType: record_type;
    PtrRule: record_rule;
    Flag: Boolean;
    obj_count :Integer;
    {attr_count:Integer;
    dict_count:Integer;}
    type_count:Integer;
    rule_count:Integer;
    I,J,K,L: Integer;
    Name: String;
    Name1: array[0..256] of Char;
  begin
    {�������� ���� ������}
    SetDefaultKBInterface:=FKBInterface;
    Name:=SESPath;
    set_prog_name(Name);
    init_session;
    L:=Length(Name);
    StrPCopy(Name1,Copy(Name,1,L-4));
    load_session(StrPas(Name1));
    load_base(StrPas(Name1));

    obj_count :=set_obj_count;
    {attr_count:=set_attr_count;
    dict_count:=set_dict_count;}
    type_count:=set_type_count;
    rule_count:=set_rule_count;

    for I:=1 to type_count do begin
      PtrType:=record_type.Import(I, ListTypes);
       {ListTypes.Add(PtrType);}
    end;

    for I:=1 to obj_count do begin
      PtrObj:=record_obj.Import(I, ListObjs);
      {ListObjs.Add(PtrObj);}
    end;

     for I:=1 to ListObjs.Count do
       for J:=1 to record_obj(ListObjs.Items[I-1]).ListAttrs.Count do begin
         for K:=1 to ListTypes.Count do begin
           if record_attr(record_obj(ListObjs.Items[I-1]).
                                      ListAttrs.Items[J-1]).n_type=
                    record_type(ListTypes.Items[K-1]).n_type then
             record_attr(record_obj(ListObjs.Items[I-1]).
                                      ListAttrs.Items[J-1]).{F}PtrType:=
             record_type(ListTypes.Items[K-1]);
         end;
       end;

    for I:=1 to rule_count do begin
      PtrRule:=record_rule.Import(I, ListRules);
      {ListRules.Add(PtrRule);}
    end;

  end;

  procedure TKnowledgeBase.Export(SESPath: String);
  var
    I,L: Integer;
    Name: String;
    Name1: array[0..256] of Char;
  begin
    Name:=SESPath;
    set_prog_name(Name);
    init_session;
    for I:=1 to ListTypes.Count do begin
      record_type(ListTypes.Items[I-1]).Export(I);
    end;
    for I:=1 to ListObjs.Count do begin
      record_obj(ListObjs.Items[I-1]).Export(I);
    end;
    for I:=1 to ListRules.Count do begin
      record_rule(ListRules.Items[I-1]).Export(ListObjs,ListTypes,I);
    end;
    L:=Length(Name);
    StrPCopy(Name1,{Copy(}Name{,1,L-4)});
    save_base(StrPas(Name1));
    save_session(StrPas(Name1));
  end;

  function TKnowledgeBase.Save: String;
  var
    F:  System.Text;
    i: Integer;
  begin
    System.AssignFile(F,FileName);
    try
      System.Rewrite(F);
    except
       on e: Exception do begin
         ShowMessage('�� ���� ��������� ����: '+FileName + #10#13 + e.Message);
         {Active:=False;}
         Exit;
       end;
    end;

    ShowMessage('������ ����: '+FileName);
    for i:=1 to ListTypes.Count do begin
      record_type(ListTypes.Items[i-1]).Save(F);
      Writeln(F);
    end;
    for i:=1 to ListObjs.Count do begin
      record_obj(ListObjs.Items[i-1]).Save(F);
      Writeln(F);
    end;
    for i:=1 to ListRules.Count do begin
      record_rule(ListRules.Items[i-1]).Save(F);
      Writeln(F);
    end;
    for i:=1 to ListConns.Count do begin
      record_conn(ListConns.Items[i-1]).Save(F);
      Writeln(F);
    end;
{    if Assigned(MFDictionary) then begin
      Writeln(F,w_dict);
      MFDictionary.Dictionary.SaveToFile(F);
      Writeln(F);
    end;
}
    System.Close(F);
    Save:=FileName;

  end;

  function TKnowledgeBase.Convert(frmt_name: String): String;
  var
    i: Integer;
    res_name: Array [0..MAXSTR] of Char;
    s,p: PChar;
    s1: Array[0..10] of Char;
    res: String;
    s_name: String;

  begin
    if frmt_name='' then begin
      ShowMessage('�� ������ ��� ���������� �����');
      Exit;
    end;

    res:='�������� ����������';
    s_name:=FProblemName;
    LoadFormat(frmt_name,@strnumber_Format);

    ptr_curr:=CURRENT_.Create;
    ptr_curr.L:=TFormatItem(FormatItems[0]);
    ptr_curr.c:=TFormatItem(FormatItems[0]).str;
    StrPCopy(res_name,GetFilePath(ResPath+s_name));
    p:=res_name+Strlen(res_name);
    find_begin;

    while ptr_curr.L.n<FormatItems.Count do begin

      if( ptr_curr.c^='$') then begin
        Inc(ptr_curr.c);
        s:=p;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_pref);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
           Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_and);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_or);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_begin);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_end);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_m1);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^='$'  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_m2);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^=','  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_m3);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^=','  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_m4);
        s:=s1;
        while( ptr_curr.c^<>',') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^=','  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      ',');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

        Inc(ptr_curr.c);
        StrPCopy(s1,_iform);
        s:=s1;
        while( ptr_curr.c^<>'$') do begin
        if( ptr_curr.c^= #10  ) or (
            ptr_curr.c^= #0   ) or (
            ptr_curr.c^= #13  ) or (
            ptr_curr.c^=','  ) then begin
          ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                      frmt_name+
                      ' : ��������  '+
                      '$');
          Close(F_fp);
          {Application.Terminate;}Exit;
        end;
        s^:=ptr_curr.c^;
        Inc(s);
        Inc(ptr_curr.c);
        end;
        s^:=#0;

      end;

      {$I-}
      System.AssignFile(F_fp,res_name);
      System.Rewrite(F_fp);
      {$I+}
      if IOResult<>0 then begin
        ShowMessage('�� ���� ������� ����: '+res_name);
        Exit;
      end;
      ShowMessage('������ ����: '+StrPas(res_name));
      {SetTextBuf(F_fp,buf_stream);}

      {Inc(ptr_curr.c); ��������� '$'}

      {ShowMessage( 'First:'+StrPas(ptr_curr.c)+#10+KeyWord[BEGIN_] );}

       while not(Cmp(BEGIN_,ptr_curr.c)) do begin{��������� �� ������ �����}
        Inc(ptr_curr.c);
        next_str(ptr_curr);  {ShowMessage( 'next1:'+StrPas(ptr_curr.c) );}
        is_err1(ptr_curr);
      end;{while}
      {ShowMessage( 'next2:'+StrPas(ptr_curr.c) );}

      transl(ptr_curr);

      {Flush(F_fp);}
      Close(F_fp);
      break;
      find_begin;

    end;{while ptr_curr}

    ptr_curr.Destroy;
    FreeFormat(@strnumber_Format);

    {convert:=res;!!!!}
    Convert:=StrPas(res_name);

  end;

  constructor TKnowledgeBase.Create(AOwner: TComponent);
  begin
    Inherited Create(AOwner);
    RegistryTestableProperty('KBInterface');
    RegistryTestableProperty('ListTypes');
    RegistryTestableProperty('ListObjs');
    RegistryTestableProperty('ListRules');
    RegistryTestableProperty('MFDictionary');
    FListTypes:=TKBOwnedCollection.Create(Self,record_type);
    FListObjs:=TKBOwnedCollection.Create(Self,record_obj);
    FListRules:=TKBOwnedCollection.Create(Self,record_rule);
    FListConns:=TKBOwnedCollection.Create(Self,record_conn);
    FListList:=TKBOwnedCollection.Create(Self,list);
    {FKBInterfaceCollection:=TKBOwnedCollection.Create(Self,TKBInterface);}
    DemonData:=TDemonData.Create;
    KBOwner:=AOwner;
    SetDefault;
    Trace := TStringList.Create;
  end;

  destructor TKnowledgeBase.Destroy;
//  var
//    cur_t: record_type;
//    cur_o: record_obj;
//    cur_a: record_attr;
//    cur_r: record_rule;
//    cur_l: record_conn;
//    cur_v: list;
//    I,J,C: Integer;
  begin
//    ListClear;
//    ListList.Clear;

//    I:=1;
//    while ListTypes.Count>0 do begin
//      cur_t:=record_type(ListTypes.Items[I-1]);
//      {ListTypes.Delete(I-1);}
//      cur_t.Destroy;
//    end;
//    while ListObjs.Count>0 do begin
//      cur_o:=record_obj(ListObjs.Items[I-1]);
//      {ListObjs.Delete(I-1);}
//      cur_o.Destroy;
//    end;
//    while ListRules.Count>0 do begin
//      cur_r:=record_rule(ListRules.Items[I-1]);
//      {ListRules.Delete(I-1);}
//      cur_r.Destroy;
//    end;
//    while ListConns.Count>0 do begin
//      cur_l:=record_conn(ListConns.Items[I-1]);
//      {ListConns.Delete(I-1);}
//      cur_l.Destroy;
//    end;
    Trace.Free;
    if FListTypes<>NIL then begin
      FListTypes.Clear;
      FListTypes.Destroy;
    end;
    if FListObjs<>NIL then begin
      FListObjs.Clear;
      FListObjs.Destroy;
    end;
    if FListRules<>NIL then begin
      FListRules.Clear;
      FListRules.Destroy;
    end;
    if FListConns<>NIL then begin
      FListConns.Clear;
      FListConns.Destroy;
    end;
    if FListList<>NIL then begin
      FListList.Clear;
      FListList.Destroy;
    end;
    {if FKBInterfaceCollection<>NIL then
      FKBInterfaceCollection.Destroy;}
    {if FKBInterface<>NIL then
      FKBInterface.Destroy;}
    if DemonData<>NIL then begin
      DemonData.Destroy;
    end;
    Inherited Destroy;
  end;

  procedure TKnowledgeBase.ClearLists;
  begin
    ShowMessage('�������'); {Exit;}
    FListTypes.Clear;
    FListObjs.Clear;
    FListRules.Clear;
    FListConns.Clear;
//    ListClear;
    FListList.Clear;
  end;

  procedure TKnowledgeBase.Add(KBAddPath: String);
  var
    I,J: Integer;
    t_count,o_count,r_count,l_count: Integer;
    dnt,dno,dnr,dnl: Integer;
    cur_t: record_type;
    cur_o: record_obj;
    cur_a: record_attr;
    cur_r: record_rule;
    cur_l: record_conn;
    p: TKBOwnedCollection;
    SaveKBAddPath: String;

  begin

    t_count:=ListTypes.Count;
    o_count:=ListObjs.Count;
    r_count:=ListRules.Count;
    l_count:=ListConns.Count;

    dnt:=ListTypes.Count;
    dno:=ListObjs.Count;
    dnr:=ListRules.Count;
    dnl:=ListConns.Count;

    SaveKBAddPath:=FileName;
    FileName:=KBAddPath;
    LoadFromFile;
    FileName:=SaveKBAddPath;

    {������ ������� �����}
    for I:=t_count+1 to Self.ListTypes.Count do begin
      cur_t:=record_type(Self.ListTypes.Items[I-1]);
      if cur_t.n_type  >0 then cur_t.n_type:=cur_t.n_type+dnt;
    end;

    {������ ������� o�������}
    for I:=o_count+1 to Self.ListObjs.Count do begin
      cur_o:=record_obj(Self.ListObjs.Items[I-1]);
      if cur_o.n_obj   >0 then cur_o.n_obj:=cur_o.n_obj+dno;
      if cur_o.root    >0 then cur_o.root:=cur_o.n_obj+dno;
      if cur_o.last    >0 then cur_o.last:=cur_o.n_obj+dno;

      {������ ������� � ���������}
      for J:=1 to cur_o.ListAttrs.Count do begin
        cur_a:=record_attr(cur_o.ListAttrs.Items[J-1]);
       if cur_a.n_obj   >0 then cur_a.n_obj:=cur_a.n_obj+dno;
       if cur_a.n_type  >0 then cur_a.n_type:=cur_a.n_type+dnt;
      end;
    end;

    {������ ������� ������}
    for I:=r_count+1 to Self.ListRules.Count do begin
      cur_r:=record_rule(Self.ListRules.Items[I-1]);
      if cur_r.n_rule  >0 then cur_r.n_rule:=cur_r.n_rule+dnr;
      {if cur_r.k_znach_if >0 then begin}
         p:=cur_r.list_attr_if;
         for J:=1 to p.Count do begin
           list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
         end;
      {end;}
      {if cur_r.k_znach_then >0 then begin}
        p:=cur_r.list_attr_then;
         for J:=1 to p.Count do begin
          list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
        end;
      {end;}
      {if cur_r.k_znach_else >0 then begin}
        p:=cur_r.list_attr_else;
         for J:=1 to p.Count do begin
          list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
        end;
      {end;}
    end;

    {������ ������� ������}
    for I:=l_count+1 to Self.ListConns.Count do begin
      cur_l:=record_conn(Self.ListConns.Items[I-1]);
      if cur_l.n_conn  >0 then cur_l.n_conn:=cur_l.n_conn+dnl;
      if cur_l.k_znach >0 then begin
         p:=cur_l.list_conn;
         for J:=1 to cur_l.list_conn.Count do begin
           list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
         end
      end;
    end;

    SetPointers;

  end;

  {procedure InitKB;
  begin
    KB:=TKnowledgeBase.Create;
    KB1:=TKnowledgeBase.Create;
  end;}

  {procedure ClearKB;
  begin
    KB.Destroy;
    KB1.Destroy;
    KB:=TKnowledgeBase.Create;
    KB1:=TKnowledgeBase.Create;
  end;}

  {function SetKB: TKnowledgeBase;
  begin
    SetKB:=KB;
  end;}

  procedure AddKB(FileName: String; var KB{,KB1}: TKnowledgeBase);
  begin
    {KB.KB1:=KB1;}
    KB.Add(FileName);
  end;

  {procedure LetKB(KBPtr: TKnowledgeBase);
  var
    I: Integer;
  begin
    for I:=1 to KB.ListObjs.Count do begin
      KBPtr.ListObjs.Add(record_obj(KB.ListObjs.Items[I-1]));
    end;
    for I:=1 to KB.ListAttrs.Count do begin
      KBPtr.ListAttrs.Add(record_attr(KB.ListAttrs.Items[I-1]));
    end;
    for I:=1 to KB.ListTypes.Count do begin
      KBPtr.ListTypes.Add(record_type(KB.ListTypes.Items[I-1]));
    end;
    for I:=1 to KB.ListRules.Count do begin
      KBPtr.ListRules.Add(record_rule(KB.ListRules.Items[I-1]));
    end;
  end;}

  procedure ReadStr(var F: Text; var Str: String; var Pos: Integer);
  begin
    if FlagRead then begin
      ReadLn(F,Str);
      while not eof(F) and (Length(Str)=0) do ReadLn(F,Str);
    end;
    Pos:=1;
    FlagRead:=False;
  end;

  function LastWord(S: String; var Pos: Integer): String;
  var
    L,I,J: Integer;
    Res: String;
  begin
    L:=Length(S);
    I:=0;
    while (S[Pos+I]=' ') and (Pos+I<L) do Inc(I);
    if (S[Pos+I]=' ') and (Pos+I=L) then LastWord:=''
    else begin
      J:=Pos+I;
      I:=0;
      while (S[J+I]<>' ') and (J+I<L) do Inc(I);
      Pos:=J+I;
      if J+I=L then Inc(I);
      LastWord:=Copy(S,J,I);
    end;
  end;

  function TestWord(S: String; L: array of String; SCount: Integer): Boolean;
  var
    i: Integer;
    F: Boolean;
  begin
    F:=True;
    for i:=1 to SCount do begin
      if S=L[i-1] then begin
        F:=False;
        break;
      end;
    end;
    TestWord:=F;
  end;

  function GetNumFromWord(S: String): Integer;
  var
    L,I,J: Integer;
  begin
    GetNumError:=0;
    L:=Length(S);
    I:=1;
    while ((S[I]<'0') or (S[I]>'9'))
      and (I<L) do Inc(I);
    if (S[I]>='0') and (S[I]<='9') then begin
      J:=I;
      while ((S[J]>='0') and (S[J]<='9'))
        and (J<L) do Inc(J);
      if (J>I) and ((S[J]<'0') or (S[J]>'9')) then Dec(J);
      GetNumFromWord:=StrToInt(Copy(S,I,J-I+1));
    end else begin
     GetNumFromWord:=0;
     GetNumError:=1;
    end;
  end;

  function ScanStr(S: String; var Pos: Integer): String;
  var
    L,I,J: Integer;
    Res: String;
  begin
    L:=Length(S);
    I:=0;
    while (S[Pos+I]=' ') and (Pos+I<L) do Inc(I);
    if (S[Pos+I]=' ') and (Pos+I=L) then ScanStr:=''
    else begin
      J:=Pos+I;
      I:=0;
      while((S[J+I]>='0') and (S[J+I]<='9') or
            (S[J+I]>='A') and (S[J+I]<='Z') or
            (S[J+I]>='a') and (S[J+I]<='z') or
            (S[J+I]>='�') and (S[J+I]<='�') or
            (S[J+I]>='�') and (S[J+I]<='�') or
            (S[J+I]='_')) and (J+I<L) do Inc(I);

      if not((S[J+I]>='0') and (S[J+I]<='9') or
             (S[J+I]>='A') and (S[J+I]<='Z') or
             (S[J+I]>='a') and (S[J+I]<='z') or
             (S[J+I]>='�') and (S[J+I]<='�') or
             (S[J+I]>='�') and (S[J+I]<='�') or
             (S[J+I]='_')) and (I>0) then Dec(I);
      Pos:=J+I+1;
      if J+I=L then Inc(I);
      ScanStr:=Copy(S,J,I+1);
    end;
  end;

  {function ScanSkob(S: String; var Pos: Integer): String;
  var
    L,I,J1,J2: Integer;
    Res: String;
  begin
    L:=Length(S);
    I:=1;
    J1:=1;
    J2:=0;
    while (Pos+I<L) do begin
      if S[Pos+I]='(' then Inc(J1);
      if S[Pos+I]=')' then Inc(J2);
      if J1=J2 then break;
      Inc(I);
    end;
    if J1=J2 then begin
      ScanSkob:=Copy(S,Pos+1,I-1);
    end else
      ShowMessage('Error');
  end;}
{****************************************************************}
 function iformat(i: Integer;Q :Integer): String;
var
  j: Integer;
  Res: String;
begin
  Res:=SysUtils.Format('%'+IntToStr(Q)+'d',[i]);
  j:=Length(Res);
  while j>0 do begin
   if Res[j]=#32 then Res[j]:='0';
   Dec(j);
  end;
  iformat:=Res;
end;

function Cmp(n: Integer; s: PChar): Boolean;
{ ��������� � �������� ������ }
var
  Sc,S1: array[0..MAXSTR] of Char;
  i,j: Integer;

begin
  Cmp:=False;
//  ShowMessage(StrPas(S));
  if (s=NIL) then Exit;
  if (s='') then begin
    ShowMessage('���������� ������ ������');
    Exit;
  end;
  StrPCopy(Sc,KeyWord[n]);
  if StrLen(s)<StrLen(Sc) then Exit;

  j:=0;
  while(s[j]=#32) do Inc(j);
  StrCopy(S1,s+j);

  if(n>= NUMBER_obj  ) and ( n<= NUMBER_rule ) or (
     n = OBJ_x ) or  (
     n = OBJ_y ) or  (
     n = NUMBER_cond ) or  (
     n = NUMBER_perf ) or  (
     n = NUMBER_str  ) then
    begin
      if StrLComp(Sc,S1,3)=0 then Cmp:= True;
      i:=0;
      while (i<3) and (Sc[i]=S1[i]) do Inc(i);
      if (i=3)
        then Cmp:=True
        else Cmp:=False;
    end
  else begin
    i:=0;
    while (Sc[i]<>#0) and (Sc[i]=S1[i]) do Inc(i);
    if (Sc[i]=#0)
      then Cmp:=True
      else Cmp:=False;
    {ShowMessage(StrPas(S1));
    if (Sc[i]=#0) then ShowMessage('True');}
  end;
end;

procedure is_err1(Current: CURRENT_);
begin
  if(Current.L =NIL) then begin
    ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                  frmt_name+
                  ' : ��������  '+
                  KeyWord[BEGIN_]);
    Close(F_fp);
    {Application.Terminate;}Exit;
  end;
end;

procedure is_err2(Current: CURRENT_);
begin
  if(Current.L =NIL) then begin
    ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                  frmt_name+
                  ' : ��������  '+
                  KeyWord[END_]);
    Close(F_fp);
    {Application.Terminate;}Exit;
  end;
end;

procedure is_err3(n: Integer);
begin
  ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                  frmt_name+
                  ' : �������� ������� ����� '+
                  KeyWord[n]);
  Close(F_fp);
  {Application.Terminate;}Exit;
end;

procedure next_str(Current: CURRENT_);
{ �������� � ������� �� ��������� ������ }
begin
  if(Current.L<>NIL) and (Current.c<>NIL) then begin
    if Current.L.n<FormatItems.Count then
    if( Current.c^ =#0) or
        ( Current.c^ =#10) or
        ( Current.c^ =#13) then begin
           Current.L:=TFormatItem(FormatItems.Items[Current.L.n]); {Next Item}
           Current.c:=Current.L.str;
    end;
//    if Current.L.str<>_S then begin
//      ShowMessage(Current.L.str);
//      _S:=Current.L.str;
//    end;
    n_str_err:=Current.L.n;
  end{ else ShowMessage('EOF!')};

  {if Current.c^=#255 then ShowMessage('2:'+StrPas(Current.L^.str^)); }
end;

{������ �� ����� �����:}
procedure find_end(Current: CURRENT_);
var
  i: Integer;
begin
  i:=1;
  while(i>0) do begin
    Inc(Current.c);
    next_str(Current);
    is_err2(Current);
    if(Cmp(BEGIN_,Current.c)) then Inc(i);
    if(Cmp(END_  ,Current.c)) then Dec(i);
  end;
  Current.c:=Current.c+Length(KeyWord[END_]);
end;


procedure put_n(Current: CURRENT_; n: Integer; t: Integer);
var
  s: String;
  p: PChar;
  r: array[0..9] of Char;
  k: Integer;
  f: Boolean;
begin
  if(Current.c[3] ='~') then
  begin
    s:=IntToStr(n);
    Current.c:=Current.c+Length(KeyWord[t]);
    if ( Current.c^ =#10) or
       ( Current.c^ =#0)  or
       ( Current.c^ =#13)
        then f:=True else f:=False;
  end else
  begin
    p:=Current.c+3;
    k:=0;
    while( p^<>'~') and ( k<=8) do begin r[k]:=p^; Inc(p); Inc(k);end;
    if( p^<>'~' ) or ( k =8) then begin
      ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                  frmt_name+
                  ' : �������� '+
                  '~');
      Close(F_fp);
      {Application.Terminate;}Exit;
    end;
    r[k]:=#0;
    s:=iformat(n,StrToInt(StrPas(r)));
    Current.c:=Current.c+Length(KeyWord[t])+k;
    if ( Current.c^ =#10) or
       ( Current.c^ =#0)  or
       ( Current.c^ =#13)
        then f:=True else f:=False;
  end;
  next_str(Current);
  if f then begin
     Inc(n_str);
     Writeln(F_fp,s);
  end else begin
    Write(F_fp,s);
  end;
end;

procedure put_i(Current: CURRENT_; i: Integer; t: Integer);
var
  s: String;
  f: Boolean;
begin
  s:=IntToStr(i);
  Current.c:=Current.c+Length(KeyWord[t]);
    if ( Current.c^ =#10) or
       ( Current.c^ =#0)  or
       ( Current.c^ =#13)
        then f:=True else f:=False;
  next_str(Current);
  if f then begin
     Inc(n_str);
     Writeln(F_fp,s);
  end else begin
    Write(F_fp,s);
  end;
end;

function eko_str(s: String): String;
var
  i: Integer;
  ss: array[0..255] of Char;
begin
  i:=Length(s);
  StrPCopy(ss,s);
  Dec(i);
  while(ss[i] =#32 ) and ( i>0) do Dec(i);{���������� �������}
  ss[i+1]:=#0;
  while(i>=0) do begin
    if(ss[i] =#32 ) or (
       ss[i] ='!' ) or (
       ss[i] ='?' ) or (
       ss[i] ='.' ) or (
       ss[i] =',' ) or (
       ss[i] =';' ) or (
       ss[i] =#34 ) or (
       ss[i] =#39 ) or (
       ss[i] ='&' ) or (
       ss[i] ='|' ) then ss[i]:='_';
    Dec(i);
    end;
  {�������� ������}
  if(Length(ss)>30) then begin
    {ShowMessage('������ ������: '+ss);}
    ss[30]:=#0;
    {ShowMessage('��������: '+ss);}
  end;
  eko_str:=StrPas(ss);
end;

procedure put_s(Current: CURRENT_; s: String; t: Integer);
var
  s1: String;
  ss: array[0..255] of Char;
  i: Integer;
begin
  if flag_eko=0 then begin
    i:=Length(s)-1;
    StrPCopy(ss,s);
    while(ss[i] =#32 ) and ( i>0) do Dec(i);
    ss[i+1]:=#0;
    s1:=StrPas(ss);
    Write(F_fp,s1);
  end else begin
    StrPCopy(ss,eko_str(s));
    Write(F_fp,ss);
  end;
  Current.c:=Current.c+Length(KeyWord[t]);
  next_str(Current);
end;

procedure GenIncident(Current: CURRENT_; ptr: record_rule; t: Integer);
var
  ss: String;
  p: PChar;
  ptr1: list;
  flag: Integer;

  {ptr_obj:  record_obj;
  ptr_attr: record_attr;
  ptr_type: record_type;
  ptr_rule: record_rule;}

  I,J,K: Integer;

begin
  Flag:=0;

  Current.c:=Current.c+Length(KeyWord[t]);
  next_str(Current);
  if ( ptr <> NIL ) then begin
    {if( ptr.incident <> NIL ) then  Dispose(ptr.incident);???}
    for I:=1 to ptr.list_attr_if.Count do begin
      ptr1:=list(ptr.list_attr_if.Items[I-1]);
      p:=gen_buf;
      if ( ptr1.n_layer  =1 ) then begin
        {for J:=1 to KB.ListObjs.Count do begin
          ptr_obj:=record_obj(KB.ListObjs.Items[J-1]);
          if ptr_obj.n_obj=ptr1.n_obj then begin
            for K:=1 to ptr_obj.ListAttrs.Count do begin
              ptr_attr:=record_attr(ptr_obj.ListAttrs.Items[K-1]);
              if ptr_attr.n_attr=ptr1.n_attr then begin
                ptr_type:=record_type(KB.ListTypes[ptr_attr.n_type-1]);
                break;
              end;
            end;
            break;
          end;
        end;}
        if flag_eko=0 then begin
          StrPCopy(p,_m1+_pref+iformat(ptr1.n_attr,StrToInt(_iform))+'='+
                                      _m3+ptr1.Fznach+_m4+_m2);
        end else begin
          ss:=eko_str(ptr1.Fznach);
          StrPCopy(p,_m1+_pref+iformat(ptr1.n_attr,StrToInt(_iform))+'='+
                                      _m3+ss+_m4+_m2);
        end;
        p:=p+Length(StrPas(p));
        flag:=1;
      end else begin
        {for J:=1 to KB.ListObjs.Count do begin
          ptr_obj:=record_obj(KB.ListObjs.Items[J-1]);
          if ptr_obj.n_obj=ptr1.n_obj then begin
            for K:=1 to ptr_obj.ListAttrs.Count do begin
              ptr_attr:=record_attr(ptr_obj.ListAttrs.Items[K-1]);
              if ptr_attr.n_attr=ptr1.n_attr then begin
                ptr_type:=record_type(KB.ListTypes[ptr_attr.n_type-1]);
                break;
              end;
            end;
            break;
          end;
        end;}
        if flag_eko=0 then begin
          StrPCopy(p,_begin+_m1+_pref+iformat(ptr1.n_attr,StrToInt(_iform))+'='+
                                      _m3+ptr_d.Fznach+_m4+_m2);
        end else begin
          ss:=eko_str(ptr_d.Fznach);
          StrPCopy(p,_begin+_m1+_pref+iformat(ptr1.n_attr,StrToInt(_iform))+'='+
                                      _m3+ss+_m4+_m2);
        end;
        p:=p+Length(StrPas(p));
        flag:=2;
      end;
      if ( flag =2 ) then begin
        StrPCopy(p,_end);
        p:=p+Length(StrPas(p));
      end;
    end;
    Write(F_fp,gen_buf);
  end
end;

procedure TKnowledgeBase.transl(Current: CURRENT_);
var
    ptr1:      record_attr;
    ptr2:      record_type;
    ptr3:      record_rule;
    ptr4:      TKBOwnedCollection;
    ptr5:      value;
    ptr6:      record_obj;
    save_obj:  record_obj;
    save_attr: record_attr;
    save_type: record_type;
    save_dict: value;
    save_list: list;
    save_n_znach: Integer;
    ptr_curr: CURRENT_;

  ptr_obj:  record_obj;
  ptr_attr: record_attr;
  ptr_type: record_type;
  ptr_rule: record_rule;

  I,J,K,T: Integer;

begin
  if (Current.L=NIL) or (Current.c=NIL) then begin
//    ShowMessage('�������������� ������ � ������:'+IntToStr(n_str));
    Exit;
  end;

  ptr_curr:=CURRENT_.Create;

  if(Cmp(BEGIN_,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[BEGIN_]);
    if ( Current.c^ =#10) or
       ( Current.c^ =#0)  or
       ( Current.c^ =#13)
        then begin
       Inc(n_str);
       Writeln(F_fp);
    end;
    next_str(Current);
    if(( Current.c^ ='~')) and (( not Cmp(END_,Current.c))) then begin
//      if flag_separate=1 then
        transl(Current);
    end;{if}
    if (Current.L=NIL) or (Current.c=NIL) then begin
//      ShowMessage('�������������� ������ � ������:'+IntToStr(n_str));
      ptr_curr.Destroy;
      Exit;
    end;
    while ( not Cmp(END_,Current.c) )
         and ( Current.L.str[0]<>#0)
         and ( Current.L.n<FormatItems.Count) do begin
      if ( Current.c^ =#10) or
         ( Current.c^ =#0)  or
         ( Current.c^ =#13)
           then begin
             Inc(n_str);
             Writeln(F_fp);
      end else begin
        {if Current.c^=#255 then ShowMessage('1:'+StrPas(Current.L^.str^));}
        Write(F_fp,Current.c^);
        Inc(Current.c);
        if ( Current.c^ =#10) or
           ( Current.c^ =#0)  or
           ( Current.c^ =#13)
           then begin
          Inc(n_str);
          Writeln(F_fp);
        end;
      end;
      next_str(Current);
      if(( Current.c^ ='~')) and ( not(Cmp(END_,Current.c))) then begin
//        if flag_separate=1 then
          transl(Current);
      end;{if}
      if (Current.L=NIL) or (Current.c=NIL) then begin
//        ShowMessage('�������������� ������ � ������:'+IntToStr(n_str));
        ptr_curr.Destroy;
        Exit;
      end;
    end;{while}
    is_err2(Current);
    Current.c:=Current.c+Length(KeyWord[END_]);
    next_str(Current);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_obj,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_obj]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if ListObjs.Count<>0 then begin
      for I:=1 to ListObjs.Count do begin
        ptr6:=record_obj(ListObjs.Items[I-1]);
        CurObj:=ptr6;
        ptr_curr.L:=Current.L;
        ptr_curr.c:=Current.c;
        transl(ptr_curr);
      end;{for ptr6}
      CurObj:=NIL;
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end else
      find_end(Current);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_obj_obj,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_obj_obj]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    save_obj:=CurObj;
    for I:=1 to ListObjs.Count do begin
      ptr6:=record_obj(ListObjs.Items[I-1]);
      CurObj:=ptr6;
      if(CurObj.last =save_obj.n_obj) then begin
        ptr_curr.L:=Current.L;
        ptr_curr.c:=Current.c;
        transl(ptr_curr);
      end;
    end;{for ptr6}
    CurObj:=save_obj;
    if ListObjs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_obj_root,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_obj_obj]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    for I:=1 to ListObjs.Count do begin
      ptr6:=record_obj(ListObjs.Items[I-1]);
      CurObj:=ptr6;
      if(CurObj.root =save_obj.n_obj) then begin
        ptr_curr.L:=Current.L;
        ptr_curr.c:=Current.c;
        transl(ptr_curr);
      end;
    end;{for ptr6}
    CurObj:=save_obj;
    if ListObjs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_obj_attr,Current.c)) then begin {??????}
    Current.c:=Current.c+Length(KeyWord[LOOP_obj_attr]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurAttr=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    for I:=1 to ListObjs.Count do begin
      ptr6:=record_obj(ListObjs.Items[I-1]);
      CurObj:=ptr6;
      for J:=1 to CurObj.ListAttrs.Count do begin
        if CurAttr=CurObj.ListAttrs.Items[J-1] then begin
          ptr_curr.L:=Current.L;
          ptr_curr.c:=Current.c;
          transl(ptr_curr);
        end;
      end;
    end;{for ptr6}
    CurObj:=NIL;
    if ListObjs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_attr,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_attr]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin {��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    for I:=1 to ListObjs.Count do begin
      ptr_obj:=record_obj(ListObjs.Items[I-1]);
      for J:=1 to ptr_obj.ListAttrs.Count do begin
        ptr1 := record_attr(ptr_obj.ListAttrs.Items[J-1]);
        CurAttr:=ptr1;
        ptr_curr.L:=Current.L;
        ptr_curr.c:=Current.c;
        transl(ptr_curr);
      end;
    end;
    CurAttr:=NIL;
    if ListObjs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_attr_obj,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_attr_obj]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurObj=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    for I:=1 to CurObj.ListAttrs.Count do begin
      ptr1 := record_attr(CurObj.ListAttrs.Items[I-1]);
      CurAttr:=ptr1;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
    end;{for ptr1}
    CurAttr:=NIL;
    if CurObj.ListAttrs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_attr_type,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_attr_type]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurType=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    for I:=1 to ListObjs.Count do begin
      ptr_obj:=record_obj(ListObjs.Items[I-1]);
      for J:=1 to ptr_obj.ListAttrs.Count do begin
        ptr1 := record_attr(ptr_obj.ListAttrs.Items[J-1]);
        CurAttr:=ptr1;
        if(CurAttr.n_type =CurType.n_type) then begin
          ptr_curr.L:=Current.L;
          ptr_curr.c:=Current.c;
          transl(ptr_curr);
        end;
      end;
    end;{for ptr1}
    CurAttr:=NIL;
    if ListObjs.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_type,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_type]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    for I:=1 to ListTypes.Count do begin
      ptr2 := record_type(ListTypes.Items[I-1]);
      CurType:=ptr2;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
    end;{for ptr2}
    CurType:=NIL;
    if ListTypes.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_type_attr,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_type_attr]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
     end;{while}
    if(CurAttr=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    for I:=1 to ListTypes.Count do begin
      ptr2 := record_type(ListTypes.Items[I-1]);
      CurType:=ptr2;
      if(CurType.n_type =CurAttr.n_type) then begin
        ptr_curr.L:=Current.L;
        ptr_curr.c:=Current.c;
        transl(ptr_curr);
      end;
    end;{for ptr2}
    CurType:=NIL;
    if ListTypes.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_rule,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_rule]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    for I:=1 to ListRules.Count do begin
      ptr3 := record_rule(ListRules.Items[I-1]);
      CurRule:=ptr3;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
      if(CurRule =NIL) then is_err3(LOOP_rule);
    end;{while ptr3}
    CurRule:=NIL;
    if ListRules.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_list,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_list]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurType=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    for I:=1 to CurType.Flist_znach.Count do begin
      ptr5 := value(CurType.Flist_znach.Items[I-1]);
      CurDict:=ptr5;
      CurNZnach:=I;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
    end;{for ptr5}
    CurDict:=NIL;
    CurNZnach:=0;
    if CurType.Flist_znach.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;  
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_cond,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_cond]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurRule = NIL) then is_err3(LOOP_cond);
    if(CurRule = NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    ptr4 := CurRule.list_attr_if;
    save_obj:=CurObj;
    save_attr:=CurAttr;
    save_dict:=CurDict;
    save_n_znach:=CurNZnach;
    if(ptr4.Count<>0) then flag_separate:=1;
    for I:=1 to ptr4.Count do begin
      CurIf:=list(ptr4.Items[I-1]);
      for J:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[J-1]);
        if ptr_obj.n_obj=CurIf.n_obj then begin
          for K:=1 to ptr_obj.ListAttrs.Count do begin
            ptr_attr:=record_attr(ptr_obj.ListAttrs.Items[K-1]);
            if (ptr_attr.n_obj=CurIf.n_obj) and
               (ptr_attr.n_attr=CurIf.n_attr) then begin
              CurObj:=ptr_obj;
              CurAttr:=ptr_attr;
              for T:=1 to ListTypes.Count do begin
                ptr_type:=record_type(ListTypes.Items[T-1]);
                if ptr_type.n_type=CurAttr.n_type then break;
              end;
              for T:=1 to ptr_type.Flist_znach.Count do begin
                if CompareStr(value(ptr_type.Flist_znach.Items[T-1]).Fznach,
                  CurIf.Fznach)=0 then begin
                  CurDict:=value(ptr_type.Flist_znach.Items[T-1]);
                  CurNZnach:=T;
                  break;
                end;
              end;
              break;
            end;
          end;
          break;
        end;
      end;
      if(I=ptr4.Count) then flag_separate:=0;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
//      if CurDict=NIL then ShowMessage(CurAttr.Comment);
      transl(ptr_curr);
    end;{for ptr4}
    CurObj:=save_obj;
    CurAttr:=save_attr;
    CurDict:=save_dict;
    CurNZnach:=save_n_znach;
    CurIf:=NIL;
    if ptr4.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_perf,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_perf]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurRule = NIL) then is_err3(LOOP_perf);
    if(CurRule = NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    ptr4 := CurRule.list_attr_then;
    save_obj:=CurObj;
    save_attr:=CurAttr;
    save_dict:=CurDict;
    save_n_znach:=CurNZnach;
    if(ptr4.Count<>0) then flag_separate:=1;
    for I:=1 to ptr4.Count do begin
      CurThen:=list(ptr4.Items[I-1]);
      for J:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[J-1]);
        if ptr_obj.n_obj=CurThen.n_obj then begin
          for K:=1 to ptr_obj.ListAttrs.Count do begin
            ptr_attr:=record_attr(ptr_obj.ListAttrs.Items[K-1]);
            if (ptr_attr.n_obj=CurThen.n_obj) and
               (ptr_attr.n_attr=CurThen.n_attr) then begin
              CurObj:=ptr_obj;
              CurAttr:=ptr_attr;
              for T:=1 to ListTypes.Count do begin
                ptr_type:=record_type(ListTypes.Items[T-1]);
                if ptr_type.n_type=CurAttr.n_type then break;
              end;
              for T:=1 to ptr_type.Flist_znach.Count do begin
                if CompareStr(value(ptr_type.Flist_znach.Items[T-1]).Fznach,
                  CurThen.Fznach)=0 then begin
                  CurDict:=value(ptr_type.Flist_znach.Items[T-1]);
                  CurNZnach:=T;
                  break;
                end;
              end;
              break;
            end;
          end;
          break;
        end;
      end;
      if(I=ptr4.Count) then flag_separate:=0;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
    end;{for ptr4}
    CurObj:=save_obj;
    CurAttr:=save_attr;
    CurDict:=save_dict;
    CurNZnach:=save_n_znach;
    CurThen:=NIL;
    if ptr4.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_else,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_else]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(CurRule = NIL) then is_err3(LOOP_else);
    if(CurRule = NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    ptr4 := CurRule.list_attr_if;
    save_obj:=CurObj;
    save_attr:=CurAttr;
    save_dict:=CurDict;
    save_n_znach:=CurNZnach;
    if(ptr4.Count<>0) then flag_separate:=1;
    for I:=1 to ptr4.Count do begin
      CurElse:=list(ptr4.Items[I-1]);
      for J:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[J-1]);
        if ptr_obj.n_obj=CurElse.n_obj then begin
          for K:=1 to ptr_obj.ListAttrs.Count do begin
            ptr_attr:=record_attr(ptr_obj.ListAttrs.Items[K-1]);
            if ptr_attr.n_attr=ptr1.n_attr then begin
              CurObj:=ptr_obj;
              CurAttr:=ptr_attr;
              for T:=1 to ListTypes.Count do begin
                ptr_type:=record_type(ListTypes.Items[T-1]);
                if ptr_type.n_type=CurAttr.n_type then break;
              end;
                for T:=1 to ptr_type.Flist_znach.Count do begin
                if CompareStr(value(ptr_type.Flist_znach.Items[T-1]).Fznach,
                  CurElse.Fznach)=0 then begin
                  CurDict:=value(ptr_type.Flist_znach.Items[T-1]);
                  CurNZnach:=T;
                  break;
                end;
              end;
              break;
            end;
          end;
          break;
        end;
      end;
      if(I=ptr4.Count) then flag_separate:=0;
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
    end;{for ptr4}
    CurObj:=save_obj;
    CurAttr:=save_attr;
    CurDict:=save_dict;
    CurNZnach:=save_n_znach;
    CurElse:=NIL;
    if ptr4.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_obj,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_n(Current,CurObj.n_obj,NUMBER_obj);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_attr,Current.c)) then begin
    if(CurAttr =NIL) then is_err3(LOOP_attr) else
      put_n(Current,CurAttr.n_attr,NUMBER_attr);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_type,Current.c)) then begin
    if(CurType =NIL) then is_err3(LOOP_type) else
      put_n(Current,CurType.n_type,NUMBER_type);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_list,Current.c)) then begin
    if(CurDict =NIL) then is_err3(LOOP_list) else
      put_n(Current,CurNZnach,NUMBER_list);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_rule,Current.c)) then begin
    if(CurRule =NIL) then is_err3(LOOP_rule) else
      put_n(Current,CurRule.n_rule,NUMBER_rule);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(SEPARATE,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[SEPARATE]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if flag_separate=1 then begin
      ptr_curr.L:=Current.L;
      ptr_curr.c:=Current.c;
      transl(ptr_curr);
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end else begin
      find_end(Current);
      flag_separate:=1;
    end;
    {find_end(Current);}
//    Current.L:=ptr_curr.L;
//    Current.c:=ptr_curr.c;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_root,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.root,OBJ_root);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_last,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.last,OBJ_last);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_k_attr,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.k_attr,OBJ_k_attr);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_x,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.x,OBJ_x);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_y,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.y,OBJ_y);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_flag,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_i(Current,CurObj.flag,OBJ_flag);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(OBJ_comm,Current.c)) then begin
    if(CurObj =NIL) then is_err3(LOOP_obj) else
      put_s(Current,CurObj.comment,OBJ_comm);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(ATTR_demon,Current.c)) then begin
    if(CurAttr =NIL) then is_err3(LOOP_attr) else
      put_i(Current,0{CurAttr.demon},ATTR_demon);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(ATTR_comm,Current.c)) then begin
    if(CurAttr =NIL) then is_err3(LOOP_attr) else
      put_s(Current,CurAttr.comment,ATTR_comm);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(TYPE_k_znach,Current.c)) then begin
    if(CurType =NIL) then is_err3(LOOP_type) else
      put_i(Current,CurType.k_znach,TYPE_k_znach);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(TYPE_comm,Current.c)) then begin
    if(CurType =NIL) then is_err3(LOOP_type) else
      put_s(Current,CurType.comment,TYPE_comm);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(RULE_k_znach_if,Current.c)) then begin
    if(CurRule =NIL) then is_err3(LOOP_rule) else
      put_i(Current,CurRule.k_znach_if,RULE_k_znach_if);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(RULE_k_znach_then,Current.c)) then begin
    if(CurRule =NIL) then is_err3(LOOP_rule) else
      put_i(Current,CurRule.k_znach_then,RULE_k_znach_then);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  {if(Cmp(RULE_predicate,Current.c)) then begin
    if(CurRule =NIL) then is_err3(LOOP_rule);
    put_i(Current,CurRule.predicate,RULE_predicate);
  end;
  }
  if(Cmp(LIST_name,Current.c)) then begin
    if(CurDict =NIL) then is_err3(LOOP_list) else
      put_s(Current,CurDict.Fznach,LIST_name);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_cond,Current.c)) then begin
    if(CurIf =NIL) then is_err3(LOOP_cond) else
      put_n(Current,CurNZnach{CurIf.n_znach},NUMBER_cond);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_perf,Current.c)) then begin
    if(CurThen =NIL) then is_err3(LOOP_perf) else
      put_n(Current,CurNZnach{CurThen.n_znach},NUMBER_perf);
  end;{if}

  if(Cmp(S_name,Current.c)) then begin
    put_s(Current,FProblemName,S_name);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(EKO,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[EKO]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
     end;{while}
    flag_eko:=1;
    ptr_curr.L:=Current.L;
    ptr_curr.c:=Current.c;
    transl(ptr_curr);
    flag_eko:=0;
    {find_end(Current);}
    Current.L:=ptr_curr.L;
    Current.c:=ptr_curr.c;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(NUMBER_str,Current.c)) then begin
    put_n(Current,n_str,NUMBER_str);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(GEN_incident,Current.c)) then begin
    GenIncident(Current,CurRule,GEN_incident);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_utv,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_utv]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(ListList=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    if(ListList.Count>0) then for I:=1 to ListList.Count do begin
      CurList := list(ListList.Items[I-1]);
      for K:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[K-1]);
        CurObj:=ptr_obj;
        if ptr_obj.n_obj=CurList.n_obj then
        for J:=1 to ptr_obj.ListAttrs.Count do begin
          ptr1 := record_attr(ptr_obj.ListAttrs.Items[J-1]);
          if ptr1.n_attr=CurList.n_attr then begin
            CurAttr:=ptr1;
           ptr_curr.L:=Current.L;
           ptr_curr.c:=Current.c;
           transl(ptr_curr);
          end;
        end;
      end;
    end;{for ptr1}
    CurList:=NIL;
    CurObj:=NIL;
    CurAttr:=NIL;
    if ListList.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_utv_utv,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_utv_utv]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(ListList=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    save_list:=CurList;
    save_obj:=CurObj;
    save_attr:=CurAttr;
    if(ListList.Count>0) then for I:=1 to ListList.Count do begin
      CurList := list(ListList.Items[I-1]);
      if CurList.last<>NIL then
      if (CurList.last.n_attr=save_list.n_attr) and
         (CurList.last.n_obj=save_list.n_obj) and
         (CurList.last.Fznach=save_list.Fznach) then
      for K:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[K-1]);
        CurObj:=ptr_obj;
        if ptr_obj.n_obj=CurList.n_obj then
        for J:=1 to ptr_obj.ListAttrs.Count do begin
          ptr1 := record_attr(ptr_obj.ListAttrs.Items[J-1]);
          if ptr1.n_attr=CurList.n_attr then begin
            CurAttr:=ptr1;
            ptr_curr.L:=Current.L;
            ptr_curr.c:=Current.c;
            transl(ptr_curr);
          end;
        end;
      end;
    end;{for ptr1}
    CurList:=save_list;
    CurObj:=save_obj;
    CurAttr:=save_attr;
    if ListList.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(LOOP_utv_attr,Current.c)) then begin
    Current.c:=Current.c+Length(KeyWord[LOOP_utv_attr]);
    next_str(Current);
    while not(Cmp(BEGIN_,Current.c)) do begin{��������� �� ������ �����}
      Inc(Current.c);
      next_str(Current);
      is_err1(Current);
    end;{while}
    if(ListList=NIL) then begin
      find_end(Current);
      ptr_curr.Destroy;
      Exit;
    end;
    save_list:=CurList;
    save_obj:=CurObj;
    save_attr:=CurAttr;
    if(ListList.Count>0) then for I:=1 to ListList.Count do begin
      CurList := list(ListList.Items[I-1]);
      if CurList.last<>NIL then
      if (CurList.last.n_obj = save_attr.n_obj ) and
         (CurList.last.n_attr= save_attr.n_attr) then
      for K:=1 to ListObjs.Count do begin
        ptr_obj:=record_obj(ListObjs.Items[K-1]);
        CurObj:=ptr_obj;
        if ptr_obj.n_obj=CurList.n_obj then
        for J:=1 to ptr_obj.ListAttrs.Count do begin
          ptr1 := record_attr(ptr_obj.ListAttrs.Items[J-1]);
          if ptr1.n_attr=CurList.n_attr then begin
            CurAttr:=ptr1;
            ptr_curr.L:=Current.L;
            ptr_curr.c:=Current.c;
            transl(ptr_curr);
          end;
        end;
      end;
    end;{for ListList}
    CurList:=save_list;
    CurObj:=save_obj;
    CurAttr:=save_attr;
    if ListList.Count=0 then
      find_end(Current)
    else begin
      Current.L:=ptr_curr.L;
      Current.c:=ptr_curr.c;
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(UTV_name,Current.c)) then begin
    if(CurList =NIL) then is_err3(LOOP_utv) else begin
      if CurList.flag_end then
        put_s(Current,CurList.Fznach,UTV_name) else
        put_s(Current,StrPas(''),UTV_name);
    end;
    ptr_curr.Destroy;
    Exit;
  end;{if}

  if(Cmp(UTV_last,Current.c)) then begin
    if(CurList =NIL) then begin
      is_err3(LOOP_utv);
    end else
      put_s(Current,CurList.last_znach,UTV_last);
    ptr_curr.Destroy;
    Exit;
  end;{if}

  ptr_curr.Destroy;

end;

procedure find_begin;
begin
  while( ptr_curr.c^=#32  ) or (
         ptr_curr.c^='-' ) or (
         ptr_curr.c^=#10 ) do begin{��������� �� ������ �����}
    if( ptr_curr.c^='-') then begin{�����������}
         ptr_curr.L:=TFormatItem(FormatItems[ptr_curr.L.n]);
         ptr_curr.c:=ptr_curr.L.str;
    end else begin
     ShowMessage(ptr_curr.c);
     Inc(ptr_curr.c);
     next_str(ptr_curr);
    end;
  end;{while}
  if(ptr_curr.L.n<FormatItems.Count ) and (
     ptr_curr.c^<>'$') then begin
    ShowMessage(IntToStr(n_str_err)+' ������ � ����� '+
                  frmt_name+
                  ' : ��������  '+
                  '$');
    Close(F_fp);
    {Application.Terminate;}Exit;
  end;
end;

procedure ShowTrassa(ptr_rule: record_rule);
var
  TestMemo: TMemo;
  OKButton: TButton;
begin
  Application.CreateForm(TTestForm,TestForm);
  with TestForm do begin
    Left := 4;
    Top := 3;
    Width := 637;
    Height := 268;
    Caption := '����������';
    Font.Height := -11;
    Font.Name := FontName;
    PixelsPerInch := 96;
  end;
  TestMemo:=TMemo.Create(TestForm);
  with TestMemo do begin
    Left := 8;
    Top := 8;
    Width := 617;
    Height := 193;
    TabOrder := 0;
  end;
  TestForm.InsertControl(TestMemo);
  OKButton:=TButton.Create(TestForm);
  with OKButton do begin
    Left := 296;
    Top := 208;
    Width := 100;
    Height := 25;
    Font.Name:=FontName;
    Font.Size:=9;
    Caption := '����������';
    TabOrder := 1;
    OnClick:=TestForm.OKButtonClick;
  end;
  TestForm.InsertControl(OKButton);
  ptr_rule.AddTrassa(TestMemo.Lines);
  if TKBOwnedCollection(ptr_rule.Collection).Owner is TKnowledgeBase then begin
    if Assigned(TKnowledgeBase(TKBOwnedCollection(ptr_rule.Collection).Owner).Trassa) then
      ptr_rule.AddTrassa(TKnowledgeBase(TKBOwnedCollection(ptr_rule.Collection).Owner).Trassa);
    if Assigned(TKnowledgeBase(TKBOwnedCollection(ptr_rule.Collection).Owner).Trace) then
      ptr_rule.AddTrace(TKnowledgeBase(TKBOwnedCollection(ptr_rule.Collection).Owner).Trace);
  end;
  TestForm.ShowModal;
  {TestMemo.Clear;}
  TestMemo.Lines.Clear;
  TestForm.RemoveControl(TestMemo);
  TestForm.RemoveControl(OKButton);
  TestMemo.Destroy;
  OKButton.Destroy;
  TestForm.Destroy;
end;

{procedure TKnowledgeBase.TestDemon(Sender: TObject);
  var
    DemonData: TDemonData;
  begin
    DemonData:=TDemonData(Sender);
    if DemonData.ptr_attr.status=4 then begin
      ShowTrassa(Trassa);
    end;
  end;}

  procedure TKnowledgeBase.CreateListList;
  var
    i,j,k: Integer;
    last_list, find_list: list;
    PtrValue, PtrValue1: TPersistent;
    list_attr: TKBOwnedCollection;
    last_znach: String;
    LastStep: Integer;
    FlagFind: Boolean;
  begin
//    ListClear;
    ListList.Clear;
    for i:=1 to ListRules.Count do begin
      list_attr:=record_rule(ListRules.Items[i-1]).list_attr_if;
      last_list:=NIL;
      last_znach:='';
      LastStep:=0;
      for j:=1 to list_attr.Count do begin
        PtrValue:=list(list_attr.Items[j-1]);
        list(PtrValue).last:=last_list;
        list(PtrValue).last_znach:=last_znach;
        list(PtrValue).LastStep:=LastStep;
        list(PtrValue).flag_end:=False;
        PtrValue1:=ListList.Add;
        with list(PtrValue1) do begin
          name:=        list(PtrValue).name;
          comment:=     list(PtrValue).comment;
          Control:=     list(PtrValue).Control;
          Comm:=        list(PtrValue).Comm;
          n_obj:=       list(PtrValue).n_obj;
          n_attr:=      list(PtrValue).n_attr;
          znach:=       list(PtrValue).znach;
          n_layer:=     list(PtrValue).n_layer;
          factor1:=     list(PtrValue).factor1;
          factor2:=     list(PtrValue).factor2;
          accuracy:=    list(PtrValue).accuracy;
          predicat:=    list(PtrValue).predicat;
          last_znach:=  list(PtrValue).last_znach;
          LastStep:=    list(PtrValue).LastStep;
          flag_del:=    list(PtrValue).flag_del;
          flag_end:=    list(PtrValue).flag_end;
          Scale1:=      list(PtrValue).Scale1;
          Scale2:=      list(PtrValue).Scale2;
          Scale3:=      list(PtrValue).Scale3;
          Skip:=        list(PtrValue).Skip;
          PtrAttr:=     list(PtrValue).PtrAttr;
          Last:=        list(PtrValue).Last;
        end;
        {PtrValue1.Assign(PtrValue);}
        last_list:=list(PtrValue);
        last_znach:=list(PtrValue).Fznach;
        LastStep:=list(PtrValue).LastStep;
      end;
      list_attr:=record_rule(ListRules.Items[i-1]).list_attr_then;
      for j:=1 to list_attr.Count do begin
        list(PtrValue):=list(list_attr.Items[j-1]);
        list(PtrValue).last:=last_list;
        list(PtrValue).last_znach:=last_znach;
        list(PtrValue).LastStep:=LastStep;
        list(PtrValue).flag_del:=False;
        list(PtrValue).flag_end:=False;
        PtrValue1:=ListList.Add;
        with list(PtrValue1) do begin
          name:=        list(PtrValue).name;
          comment:=     list(PtrValue).comment;
          Control:=     list(PtrValue).Control;
          Comm:=        list(PtrValue).Comm;
          n_obj:=       list(PtrValue).n_obj;
          n_attr:=      list(PtrValue).n_attr;
          znach:=       list(PtrValue).znach;
          n_layer:=     list(PtrValue).n_layer;
          factor1:=     list(PtrValue).factor1;
          factor2:=     list(PtrValue).factor2;
          accuracy:=    list(PtrValue).accuracy;
          predicat:=    list(PtrValue).predicat;
          last_znach:=  list(PtrValue).last_znach;
          LastStep:=    list(PtrValue).LastStep;
          flag_del:=    list(PtrValue).flag_del;
          flag_end:=    list(PtrValue).flag_end;
          Scale1:=      list(PtrValue).Scale1;
          Scale2:=      list(PtrValue).Scale2;
          Scale3:=      list(PtrValue).Scale3;
          Skip:=        list(PtrValue).Skip;
          PtrAttr:=     list(PtrValue).PtrAttr;
          Last:=        list(PtrValue).Last;
        end;
        {PtrValue1.Assign(PtrValue);}
        last_list:=list(PtrValue);
        last_znach:=list(PtrValue).Fznach;
        LastStep:=list(PtrValue).Step;
        if j=list_attr.Count then begin
          PtrValue:=list(ListList.Add);
          list(PtrValue).PtrAttr:=last_list.PtrAttr;
          list(PtrValue).n_obj  := last_list.n_obj;
          list(PtrValue).n_attr := last_list.n_attr;
          list(PtrValue).Fznach:=last_list.znach;
          list(PtrValue).last:=last_list;
          list(PtrValue).last_znach:=last_znach;
          list(PtrValue).LastStep:=LastStep;
          list(PtrValue).flag_del:=True;
          list(PtrValue).flag_end:=False;
        end;
      end;
    end;

    i:=1;
    while i<ListList.Count do begin
      PtrValue:=list(ListList.Items[i-1]);
      if list(PtrValue).last<>NIL then begin
        j:=i+1;
        while j<=ListList.Count do begin
          FlagFind:=False;
          find_list:=list(ListList.Items[j-1]);
          if find_list.last<>NIL then
          if (list(PtrValue).last.n_obj=find_list.last.n_obj) and
             (list(PtrValue).last.n_attr=find_list.last.n_attr) and
             (CompareStr(list(PtrValue).last_znach,find_list.last_znach)=0)
             //(list(PtrValue).LastStep=find_list.LastStep)
          then begin
            FlagFind:=True;

            k:=1;
            while k<=ListList.Count do begin
              if (list(ListList.Items[i-1]).last<>find_list) and
               (list(ListList.Items[i-1]).last.n_obj=find_list.n_obj) and
               (list(ListList.Items[i-1]).last.n_attr=find_list.n_attr) and
               (CompareStr(list(ListList.Items[i-1]).last_znach,find_list.Fznach)=0)
               //(list(PtrValue).LastStep=find_list.LastStep)
            then
              list(ListList.Items[i-1]).last:=list(PtrValue);
              Inc(k);
            end;

            ListList.Items[j-1].Destroy;

          end;
          if not FlagFind then Inc(j);
        end;
      end;{ else begin
        j:=i+1;
        while j<=ListList.Count do begin
          FlagFind:=False;
          find_list:=list(ListList.Items[j-1]);
          if find_list.last=NIL then begin
            FlagFind:=True;
            ListList.Delete(j-1);
          end;
          if not FlagFind then Inc(j);
        end;
      end;}
      Inc(i);
    end;

    {i:=1;
    while i<ListList.Count do begin
      PtrValue:=list(ListList.Items[i-1]);
      if PtrValue.last<>NIL then begin
        j:=i+1;
        while j<=ListList.Count do begin
          FlagFind:=False;
          find_list:=list(ListList.Items[j-1]);
          if find_list<>NIL then
          if (PtrValue.n_obj=find_list.n_obj) and
             (PtrValue.n_attr=find_list.n_attr) and
             (CompareStr(PtrValue.Fznach,find_list.Fznach)=0) then begin
            FlagFind:=True;

            k:=1;
            while k<=ListList.Count do begin
              if (list(ListList.Items[i-1]).n_obj=find_list.n_obj) and
               (list(ListList.Items[i-1]).n_attr=find_list.n_attr) and
               (CompareStr(list(ListList.Items[i-1]).Fznach,find_list.Fznach)=0) then
              list(ListList.Items[i-1]).last:=PtrValue;
              Inc(k);
            end;

            ListList.Delete(j-1);

          end;
          if not FlagFind then Inc(j);
        end;
      end else begin
        j:=i+1;
        while j<=ListList.Count do begin
          FlagFind:=False;
          find_list:=list(ListList.Items[j-1]);
          if find_list.last=NIL then begin
            FlagFind:=True;
            ListList.Delete(j-1);
          end;
          if not FlagFind then Inc(j);
        end;
      end;
      Inc(i);
    end;}

    {PtrValue:=list.Create;
    PtrValue.flag_del:=True;
    PtrValue.n_obj:=0;
    PtrValue.n_attr:=0;
    StrCopy(PtrValue.Fznach,'Begin');
    ListList.Add(PtrValue);
    for k:=1 to ListList.Count do begin
      find_list:=list(ListList.Items[k-1]);
      if find_list.last=NIL then begin
        find_list.last:=PtrValue;
        StrCopy(find_list.last_znach,'Begin');
      end;
    end;}
  end;

//  procedure TKnowledgeBase.ListClear;
//  var
//   I,J,C: Integer;
//   cur_v: list;
//  begin
    {cur_v:=list(ListList.Items[ListList.Count]);
    ListList.Delete(ListList.Count-1);
    cur_v.Destroy;}

//    I:=1;
//    C:=ListList.Count;
//    J:=1;
//    while J<=C do begin
//      cur_v:=list(ListList.Items[J-1]);
      {if cur_v.flag_del then begin}
        {ListList.Items[j-1].Destroy;}
//        cur_v.Destroy;
//        Dec(C);
      {end else Inc(J);}
//    end;

//  end;

  function DelSpaces(s: String): PChar;
  var
    res: array[0..255] of Char;
    l: Integer;
  begin
    StrPCopy(res,s);
    l:=StrLen(res)-1;
    while l>=0 do begin
      if res[l]=#32 then res[l]:=#0 else break;
      Dec(l);
    end;
    Delspaces:=res;
  end;

  procedure SkipSpaces(var fp: TextFile; var c: Char);
  begin
    if (c<>#10) and (c<>#13) and (c<>#32) then Exit;
    if (c=#10) or (c=#13) then begin
      Read(fp,c);
      Read(fp,c);
    end else
      Read(fp,c);
    while c=#32 do Read(fp,c);
    if (c=#10) or (c=#13) then begin Read(fp,c); Read(fp,c); end;
  end;

  procedure ReadWord(var fp: TextFile; var c: Char; var s: PChar);
  var
    i: Integer;
  begin
    i:=0;
    SkipSpaces(fp,c);

    if c=#39 then begin
      Read(fp,c);
      while c<>#39 do begin
        s[i]:=c;
        Inc(i);
        Read(fp,c);
      end;
      Read(fp,c);
      if (c=#10) or (c=#13) then begin SkipSpaces(fp,c); end;
    end else
    if c='[' then begin
      Read(fp,c);
      while c<>']' do begin
        s[i]:=c;
        Inc(i);
        Read(fp,c);
      end;
      Read(fp,c);
      if (c=#10) or (c=#13) then begin SkipSpaces(fp,c); end;
    end else
    while not(eof(fp)) and (c<>#32)
          and (c<>#10) and (c<>#13) do begin
      if (c=#10) or (c=#13) then begin Read(fp,c); SkipSpaces(fp,c); break; end;
      if (c<>#32) then s[i]:=c;
      Inc(i);
      Read(fp,c);
    end;
    s[i]:=#0;
  end;

  procedure TKnowledgeBase.SaveStep(StepPath: String);
  var
    Fp: TextFile;
    i,j: Integer;
    ptr_list: TKBOwnedCollection;
  begin
    System.AssignFile(Fp,StepPath);
    Rewrite(Fp);
    for i:=1 to ListTypes.Count do begin
      {if record_type(ListTypes.Items[i-1])).Control<>'' or
         record_type(ListTypes.Items[i-1])).Combo<>'' or
         record_type(ListTypes.Items[i-1])).Comm<>'' then}
      WriteLn(Fp,'[T'+IntToStr(record_type(ListTypes.Items[i-1]).n_type),'][',
                 record_type(ListTypes.Items[i-1]).Control,'][',
                 record_type(ListTypes.Items[i-1]).Combo,'][',
                 record_type(ListTypes.Items[i-1]).Comm,']');
    end;
    for i:=1 to ListObjs.Count do begin
      {if record_obj(ListObjs.Items[i-1])).Control<>'' or
         record_obj(ListObjs.Items[i-1])).Comm<>'' then}
      WriteLn(Fp,'[O'+IntToStr(record_obj(ListObjs.Items[i-1]).n_obj),'][',
                 record_obj(ListObjs.Items[i-1]).Control,'][',
                 record_obj(ListObjs.Items[i-1]).Comm,']');
    end;
    for i:=1 to ListRules.Count do begin
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_if;
      for j:=1 to ptr_list.Count do begin
        WriteLn(Fp,list(ptr_list.Items[j-1]).Control,'][',
                 list(ptr_list.Items[j-1]).Scale1,']');
      end;
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_then;
      for j:=1 to ptr_list.Count do begin
        WriteLn(Fp,list(ptr_list.Items[j-1]).Control,'][',
                 list(ptr_list.Items[j-1]).Scale1,']');
      end;
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_else;
      for j:=1 to ptr_list.Count do begin
        WriteLn(Fp,list(ptr_list.Items[j-1]).Control,'][',
                 list(ptr_list.Items[j-1]).Scale1,']');
      end;
    end;
    Close(Fp);
  end;

  procedure TKnowledgeBase.LoadStep(StepPath: String);
  var
    Fp: TextFile;
    i,j: Integer;
    Str: Array[0..255] of Char;
    S: PChar;
    c: Char;
    ptr_list: TKBOwnedCollection;

  begin
    S:=Str;
    System.AssignFile(Fp,StepPath);
    Reset(Fp);
    for i:=1 to ListTypes.Count do begin
      ReadWord(Fp,c,S);
      ReadWord(Fp,c,S); record_type(ListTypes.Items[i-1]).Control:=StrPas(S);
      ReadWord(Fp,c,S); record_type(ListTypes.Items[i-1]).Combo:=StrPas(S);
      ReadWord(Fp,c,S); record_type(ListTypes.Items[i-1]).Comm:=StrPas(S);
    end;
    for i:=1 to ListObjs.Count do begin
      ReadWord(Fp,c,S);
      ReadWord(Fp,c,S); record_obj(ListObjs.Items[i-1]).Control:=StrPas(S);
      ReadWord(Fp,c,S); record_obj(ListObjs.Items[i-1]).Comm:=StrPas(S);
    end;
    for i:=1 to ListRules.Count do begin
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_if;
      for j:=1 to ptr_list.Count do begin
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Control:=StrPas(S);
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Scale1:=StrPas(S);
      end;
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_then;
      for j:=1 to ptr_list.Count do begin
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Control:=StrPas(S);
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Scale1:=StrPas(S);
      end;
      ptr_list:=record_rule(ListRules.Items[i-1]).list_attr_else;
      for j:=1 to ptr_list.Count do begin
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Control:=StrPas(S);
        ReadWord(Fp,c,S); list(ptr_list.Items[j-1]).Scale1:=StrPas(S);
      end;
    end;
    Close(Fp);
  end;

{rocedure TKnowledgeBase.AddFromStrings(KBStrings:TKB; KB1: TKnowledgeBase);
var
  KFType     : TKFType;
  KFObject   : TKFObject;
  KFAttribute: TKFAttribute;
  KFRule     : TKFRule;

  PtrType     : record_type;
  PtrObj      : record_obj;
  PtrAttr     : record_attr;
  PtrRule     : record_rule;
  PtrValue    : value;
  i,j: Integer;
  dnt,dno,dnr,dnl: Integer;
  cur_t: record_type;
  cur_o: record_obj;
  cur_a: record_attr;
  cur_r: record_rule;
  cur_l: record_conn;
  p: TList;

begin
  with KB1 do begin
    dnt:=ListTypes.Count;
    dno:=ListObjs.Count;
    dnr:=ListRules.Count;
    with KBStrings do begin
      for i:=1 to TypeList.Count do begin
        KFType  := (TypeList.Objects[i-1] as TKFType);
        PtrType:=record_type.Create;
        ListTypes.Add(PtrType);
        PtrType.name:=KFType.TName;
        PtrType.comment:=KFType.Comments;
        PtrType.type_znach:=KFType.Kind;
        StrPCopy(PtrType.min_znach,FloatToStr(KFType.BoundUp));
        StrPCopy(PtrType.max_znach,FloatToStr(KFType.BoundDown));
        PtrType.n_type:=GetNumFromWord(KFType.TName);
        for j:=1 to KFType.ValueList.Count do begin
          PtrValue:=value.Create;
          PtrType.list_znach.Add(PtrValue);
          StrPCopy(PtrValue.Fznach,KFType.ValueList.Strings[j-1]);
        end;
      end;
      for i:=1 to ObjectList.Count do begin
        KFObject:= (ObjectList.Objects[i-1] as TKFObject);
        PtrObj:=record_obj.Create;
        ListObjs.Add(PtrObj);
        PtrObj.name:=KFObject.name;
        PtrObj.comment:=KFObject.comment;
        PtrObj.n_obj:=GetNumFromWord(KFObject.name);
        PtrObj.n_group:=1;
        for j:=1 to KFObject.AttributeList.Count do begin
          KFAttribute:= (KFObject.AttributeList.Objects[j-1] as TKFAttribute);
          PtrAttr:=record_attr.Create;
          PtrObj.ListAttrs.Add(PtrAttr);
          PtrAttr.name:=KFAttribute.AName;
          PtrAttr.comment:=KFAttribute.comments;
          PtrAttr.n_attr:=GetNumFromWord(KFAttribute.Aname);
        end;
      end;
      for i:=1 to RuleList.Count do begin
        KFRule  := (RuleList.Objects[i-1] as TKFRule);
        PtrRule:=record_rule.Create;
        ListRules.Add(PtrRule);
        PtrRule.name:=KFRule.name;
        PtrRule.comment:=KFRule.comment;
        PtrRule.n_rule:=GetNumFromWord(KFRule.name);
      end;
    end;
  end;

    for I:=1 to KB1.ListTypes.Count do begin
      cur_t:=record_type(KB1.ListTypes.Items[I-1]);
      if cur_t.n_type  >0 then cur_t.n_type:=cur_t.n_type+dnt;
    end;

    for I:=1 to KB1.ListObjs.Count do begin
      cur_o:=record_obj(KB1.ListObjs.Items[I-1]);
      if cur_o.n_obj   >0 then cur_o.n_obj:=cur_o.n_obj+dno;
      if cur_o.root    >0 then cur_o.root:=cur_o.n_obj+dno;
      if cur_o.last    >0 then cur_o.last:=cur_o.n_obj+dno;

      for J:=1 to cur_o.ListAttrs.Count do begin
        cur_a:=record_attr(cur_o.ListAttrs.Items[J-1]);
       if cur_a.n_obj   >0 then cur_a.n_obj:=cur_a.n_obj+dno;
       if cur_a.n_type  >0 then cur_a.n_type:=cur_a.n_type+dnt;
      end;
    end;

    for I:=1 to KB1.ListRules.Count do begin
      cur_r:=record_rule(KB1.ListRules.Items[I-1]);
      if cur_r.n_rule  >0 then cur_r.n_rule:=cur_r.n_rule+dnr;
      if cur_r.k_znach_if >0 then begin
         p:=cur_r.list_attr_if;
         for J:=1 to cur_r.k_znach_if do begin
           list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
         end
      end;
      if cur_r.k_znach_then >0 then begin
        p:=cur_r.list_attr_then;
        for J:=1 to cur_r.k_znach_then do begin
          list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
        end
      end;
      if cur_r.k_znach_else >0 then begin
        p:=cur_r.list_attr_else;
        for J:=1 to cur_r.k_znach_then do begin
          list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
        end
      end;
    end;

    for I:=1 to KB1.ListConns.Count do begin
      cur_l:=record_conn(KB1.ListConns.Items[I-1]);
      if cur_l.n_conn  >0 then cur_l.n_conn:=cur_l.n_conn+dnl;
      if cur_l.k_znach >0 then begin
         p:=cur_l.list_conn;
         for J:=1 to cur_l.list_conn.Count do begin
           list(p.Items[J-1]).n_obj:=list(p.Items[J-1]).n_obj+dno;
         end
      end;
    end;

    for I:=1 to KB1.ListTypes.Count do begin
      ListTypes.Add(record_type(KB1.ListTypes.Items[I-1]));
    end;
    for I:=1 to KB1.ListObjs.Count do begin
      ListObjs.Add(record_obj(KB1.ListObjs.Items[I-1]));
    end;
    for I:=1 to KB1.ListRules.Count do begin
      ListRules.Add(record_rule(KB1.ListRules.Items[I-1]));
    end;
    for I:=1 to KB1.ListConns.Count do begin
      ListConns.Add(record_conn(KB1.ListConns.Items[I-1]));
    end;


    KB1.ClearLists;

end;}

  procedure record_obj.Design(AValue: TKBInterface);
  begin
    FOnKBInterface:=AValue;
    if AValue<>NIL then AValue.Design(Self);
  end;

  procedure record_obj.SetActiveDesign(AValue: Boolean);
  begin
    if AValue then begin
      if FOnKBInterface<>NIL then
        FOnKBInterface.Design(Self);
      FActiveDesign:=False;
    end;
  end;


  constructor TKBInterface.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    OnInput:=InputDemonRun;
    OnOutput:=OutputDemonRun;
    FStop:=False;
  end;

  procedure TKBInterface.InputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
  var
    KBTestFormInterface: TKBTestFormInterface;
  begin
    KBTestFormInterface:=TKBTestFormInterface.Create(Self);
    KBTestFormInterface.SetAttrValue(Sender,AValue,Success);
    KBTestFormInterface.Destroy;
  end;

  procedure TKBInterface.OutputDemonRun(Sender: TObject; var AValue:Variant; var Success:Boolean );
  var
    KBTestFormInterface: TKBTestFormInterface;
  begin
    KBTestFormInterface:=TKBTestFormInterface.Create(Self);
    KBTestFormInterface.GetAttrValue(Sender,AValue,Success);
    KBTestFormInterface.Destroy;
  end;

  procedure TKBTestFormInterface.Design(Sender: TObject);
  var
    PtrLabel: TLabel;
    PtrEdit: TEdit;
    PtrComboBox: TComboBox;
    CurAttr: record_attr;
    OkButton: TButton;
    i,m,MaxLen,MaxLen1,h,l,k: Integer;
    Comp: TComponent;

  begin
//     if PtrType=NIL then begin
        Comp:=TComponent(TKBOwnedCollection(record_obj(Sender).Collection).Owner);
        if Comp is TKnowledgeBase then begin
          TKnowledgeBase(Comp).SetPointers;
        end else begin
          ShowMessage('������ ��������� �� ���');
        end;
//     end;

     with record_obj(Sender) do begin
        if ListAttrs.Count<=0 then Exit;
        with Form do begin
          {Set Data}
          Caption:=Comment;
          Top:=0;
          Left:=0;
{G+}
          Width:=Screen{.ActiveForm}.Width;
          Height:=Screen{.ActiveForm}.Height;
{G-}
          {Insert Controls}
          m:=0;
          MaxLen1:=0;
          h:=1;
          for i:=1 to ListAttrs.Count do begin
            CurAttr:=record_attr(ListAttrs.Items[i-1]);
            CurAttr.OnKBInterface:=FOnKBInterface;
            Inc(m);
            PtrLabel:=TLabel.Create({TKBTestFormInterface(FOnKBInterface).}Form);
            InsertControl(PtrLabel);
            with PtrLabel do begin
              Font.Name:=FontName;
              Font.Size:=9;
//              Caption:=CurAttr.PtrType.Comment;
              if CurAttr.Comment='' then
                Caption:='?' else
                Caption:=CurAttr.Comment;
              h:=Height+15;
              Top:=m*h;
              Left:=20;
              l:=ClientWidth+Left;
            end;
            PtrComboBox:=TComboBox.Create({TKBTestFormInterface(FOnKBInterface).}Form);
            InsertControl(PtrComboBox);
            with PtrComboBox do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='ComboBox_'+IntToStr(i);
              CurAttr.Control:=Name;
              Text:=TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach;
              Top:=m*h;
              Left:=l+20;
              MaxLen:=1;
              for k:=1 to CurAttr.{F}PtrType.list_znach.Count do begin
                Items.Add(value(CurAttr.{F}PtrType.list_znach.Items[k-1]).Fznach);
                MaxLen:=Max(MaxLen,Length(value(CurAttr.{F}PtrType.list_znach.Items[k-1]).Fznach));
              end;
              if CurAttr.{F}PtrType.type_znach<>1
              then Width:=Round(Font.Size*MaxLen)+40
              else Width:=50;
            end;
            PtrLabel:=TLabel.Create(Form);
            InsertControl(PtrLabel);
            with PtrLabel do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Caption:='����.';
              Top:=m*h;
              Left:=PtrComboBox.Left+PtrComboBox.Width+20;
            end;
            PtrEdit:=TEdit.Create(Form);
            InsertControl(PtrEdit);
            with PtrEdit do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='EditF1_'+IntToStr(i);
              CurAttr.Factor1Control:=Name;
              Text:=IntToStr(100);//TAssertion(CurAttr.AssertionCollection.Items[0]).factor1);
              Top:=m*h;
              Left:=PtrLabel.Left+40;
              Width:=50;
            end;
            PtrEdit:=TEdit.Create(Form);
            InsertControl(PtrEdit);
            with PtrEdit do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='EditF2_'+IntToStr(i);
              CurAttr.Factor2Control:=Name;
              Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).factor2);
              Top:=m*h;
              Left:=PtrLabel.Left+90;
              Width:=50;
              l:=Left+Width;
            end;
            if CurAttr.{F}PtrType.type_znach=1 then begin
              PtrLabel:=TLabel.Create(Form);
              InsertControl(PtrLabel);
              with PtrLabel do begin
                Font.Name:=FontName;
                Font.Size:=9;
                Caption:='����.';
                Top:=m*h;
                Left:=PtrEdit.Left+70;
              end;
              PtrEdit:=TEdit.Create(Form);
              InsertControl(PtrEdit);
              with PtrEdit do begin
                Font.Name:=FontName;
                Font.Size:=9;
                Name:='EditT_'+IntToStr(i);
                CurAttr.AccuracyControl:=Name;
                Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).accuracy);
                Top:=m*h;
                Left:=PtrLabel.Left+50;
                Width:=50;
                l:=Left+Width;
              end;
            end;
            MaxLen1:=Max(MaxLen1,l);
          end;

          Width:=MaxLen1+20;

          if {TKBTestFormInterface(FOnKBInterface).}Form is TTestForm   then begin
            OKButton:=TButton.Create({TKBTestFormInterface(FOnKBInterface).}Form);
            with OKButton do begin
              Left := Round((MaxLen*9+l+40)/2);
              Top:=(m+1)*h;
              Width := 100;
              Height := 25;
              Font.Name:=FontName;
              Font.Size:=9;
              Caption := '����������';
              TabOrder := 1;
              OnClick:=TTestForm({TKBTestFormInterface(FOnKBInterface).}Form).OKButtonClick;
            end;
            InsertControl(OKButton);
            Height:=Min(Screen.Height,OKButton.Top+2*OKButton.Height+40);
            OKButton.Left := Round((Width-OKButton.Width)/2);
          end;

        end;
    end;
  end;

  procedure TKBTestFormInterface.ReplaceValues(Sender: TObject);
  var
    CurAttr: record_attr;
    i,j,k: Integer;
  begin
    with record_obj(Sender) do begin
     with {TKBTestFormInterface(FOnKBInterface).}Form do begin
     {Replace Values}
      for j:=1 to ControlCount do begin
        for i:=1 to ListAttrs.Count do begin
          CurAttr:=record_attr(ListAttrs.Items[i-1]);
          if (Controls[j-1].Name=CurAttr.Control) and
             (CurAttr.Status<=2) then begin
             if (TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach=TComboBox(Controls[j-1]).Text) then
               CurAttr.Status:=1 else begin
               TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach:=TComboBox(Controls[j-1]).Text;
               for k:=1 to ControlCount do begin
                 if (Controls[k-1].Name=CurAttr.Factor1Control) and (CurAttr.Factor1Control<>'') then begin
                   TAssertion(CurAttr.AssertionCollection.Items[0]).factor1:=StrToInt(TEdit(Controls[k-1]).Text);
                 end;
                 if (Controls[k-1].Name=CurAttr.Factor2Control) and (CurAttr.Factor2Control<>'')then begin
                   TAssertion(CurAttr.AssertionCollection.Items[0]).factor2:=StrToInt(TEdit(Controls[k-1]).Text);
                 end;
                 if (Controls[k-1].Name=CurAttr.AccuracyControl)and (CurAttr.AccuracyControl<>'') then begin
                   TAssertion(CurAttr.AssertionCollection.Items[0]).accuracy:=StrToInt(TEdit(Controls[k-1]).Text);
                 end;
               end;
               CurAttr.Status:=6;
             end;
          end;
        end;
      end;
     end;
    end;
  end;

  procedure TKBTestFormInterface.SetFormValue(Value: TForm);
  begin
    FForm:=Value;
  end;

  function TKBTestFormInterface.GetFormValue: TForm;
  begin
    GetFormValue:=FForm;
  end;

  constructor TKBTestFormInterface.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    Form:=TForm(AOwner);
  end;

  procedure TKBTestFormInterface.SetAttrValue(Source: TObject; var AValue: Variant;
    var Success: Boolean);
  var
//    i,j: Integer;
//    CurAttr: record_attr;
    Frm: TForm;
  begin
      Frm:={TKBTestFormInterface(Self).}Form;
      Application.CreateForm(TTestForm,TestForm);
      {TKBTestFormInterface(Self).}Form:=TestForm;
      Design(record_attr(Source).PtrObj);
      {TKBTestFormInterface(Self).}Form.ShowModal;
      ReplaceValues(record_attr(Source).PtrObj);
      {TKBTestFormInterface(Self).}Form:=Frm;
      TestForm.Destroy;
  end;

  procedure TKBTestFormInterface.GetAttrValue(Source: TObject; var AValue: Variant;
    var Success: Boolean);
  var
    i,j: Integer;
    CurAttr: record_attr;
    Frm: TForm;
  begin
      Frm:={TKBTestFormInterface(Self).}Form;
      Application.CreateForm(TTestForm,TestForm);
      {TKBTestFormInterface(Self).}Form:=TestForm;
      Design(record_attr(Source).PtrObj);
      {TKBTestFormInterface(Self).}Form.ShowModal;
//      ReplaceValuesKBInterface(Self);
      {TKBTestFormInterface(Self).}Form:=Frm;
      TestForm.Destroy;
  end;

  procedure TKBItem.Assign(KBItem: TKBItem);
  begin
    inherited Assign(KBItem);
    name:=    KBItem.name;
    comment:= KBItem.comment;
    Control:= KBItem.Control;
    Comm:=    KBItem.Comm;
  end;

  procedure TKnowledgeBase.ClearWorkMemory;
  var
    K,N: Integer;
  begin
    if Assigned(Trassa) then Trassa.Clear;
    if Assigned(Trace) then Trace.Clear;
    for K:=1 to ListObjs.Count do begin
     for N:=1 to record_obj(ListObjs.Items[K-1]).ListAttrs.Count do begin
       TAssertion(record_attr(record_obj(ListObjs.Items[K-1]).ListAttrs.Items[N-1]).AssertionCollection.Items[0]).Fznach:='';
       TAssertion(record_attr(record_obj(ListObjs.Items[K-1]).ListAttrs.Items[N-1]).AssertionCollection.Items[0]).Ffactor1  := Defaultfactor1;
       TAssertion(record_attr(record_obj(ListObjs.Items[K-1]).ListAttrs.Items[N-1]).AssertionCollection.Items[0]).Ffactor2  := Defaultfactor2;
       TAssertion(record_attr(record_obj(ListObjs.Items[K-1]).ListAttrs.Items[N-1]).AssertionCollection.Items[0]).Faccuracy := Defaultaccuracy;
       record_attr(record_obj(ListObjs.Items[K-1]).ListAttrs.Items[N-1]).status:=0;
     end;
    end;
  end;

  procedure TKnowledgeBase.TestDemon(Sender: TObject; var AValue:Variant; var Success:Boolean );
  begin
    {TDetachment(Sender).KB.TestDemon(TDetachment(Sender).DemonData);}
    if TKnowledgeBase(Sender).DemonData.ptr_attr.status=4 then begin
      ShowTrassa(TKnowledgeBase(Sender).DemonData.ptr_rule);
    end;
  end;

{ TatCollectionItem }

{constructor TatCollection.Create(ItemClass: TCollectionItemClass);
begin
    inherited Create(ItemClass);
    Add;
end;

function TatCollection.GetNamePath: string;
begin
    Result:= ItemClass.ClassName;
end;}


{procedure TKnowledgeBase.SetActive(const Value: Boolean);
begin
  FActive := Value;
  If Value=True Then Load Else SetDefault;
end;}

{procedure TFileNameEditor.Edit;
var
  OpenDialog: TOpenDialog;
//  AOwner: TComponent;
begin
  OpenDialog:=TOpenDialog.Create(Application);
  OpenDialog.Title:='�������';
  OpenDialog.FileName:=GetStrValue;
  OpenDialog.Filter := '����� ���� ������ (*.kbs)|*.KBS|��� ����� (*.*)|*.*';
  if OpenDialog.Execute then begin
    SetStrValue(OpenDialog.FileName);
  end;
  OpenDialog.Destroy;
end;

function TFileNameEditor.GetValue:string;
begin
  Result:=GetStrValue;
end;

function TFileNameEditor.GetAttributes: TPropertyAttributes;
begin
  GetAttributes:=inherited GetAttributes +[paDialog];
end;}

{
procedure TMFListPropertyEditor.Edit;
var
  Component: TPersistent;
  j: Integer;

  i               : Integer;      // ������� ��� ����� For.
  List_EditorForm : TList_Editor; // �������� ����������.
  Temp_MFOwner    : TMFOwner;

  List: TMFList;

begin
  Component:=GetComponent(0);
  if Component is record_type then begin
    List:=TMFList(record_type(Component).MFList);
    List_EditorForm:=TList_Editor.Create (Application);
  //*** ���� �� ���������� ����� ��������������. ***//
    List_EditorForm.Ling_Name.Text:=List.LingName;
    if (List.Count>0) then begin
      List_EditorForm.fList.GoodAssign (List as TMFList);
      for i:=0 to List.Count-1 do begin
        Temp_MFOwner:=List.Items[i] as TMFOwner;
      end;
    end;
    if (List_EditorForm.ShowModal=mrOk) then begin
    //*** ���� �� ���������� ������ TMFListComponent. ***//
      List.LingName:=List_EditorForm.Ling_Name.Text;
      if (List_EditorForm.fList.Count>0) then begin
        List.GoodAssign (List_EditorForm.fList as TMFList);
      end;
      List_EditorForm.Free;
    end else begin
      List_EditorForm.Free;
    end;
  end;
end;

function TMFListPropertyEditor.GetValue: String ;
begin
  Result:='('+record_type(GetComponent(0)).MFList.ClassName+')';
end;

function TMFListPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  GetAttributes:=inherited GetAttributes +[paDialog];
end;
}

procedure TKnowledgeBase.DefineKBInterface(const Value: TKBInterface);
begin
  FKBInterface:=Value;
  SetDefaultKBInterface:=Value;
end;

{ TatDemon }

procedure TatDemon.GetAttrValue(Source: TObject; var AValue: Variant;
  var Success: Boolean);
begin
   {ShowMessage('get value:'+Source.ClassName);}
   If Assigned(FOnOutput) then
      FOnOutput(Source, AValue, Success);
end;

procedure TatDemon.SetAttrValue(Source: TObject; var AValue: Variant;
  var Success: Boolean);
begin
   //ShowMessage('Set value:'+Source.ClassName);
   If Assigned(FOnInput) then
      FOnInput(Source, AValue, Success);
end;


  procedure TKBInterface.Design(Sender: TObject);
  begin
    ShowMessage('Designing...');
  end;

{ function TTestFormPropertyEditor.GetValue: String;
  var
    Value: TObject;
  begin
    Value:=VarToObj(GetOrdValue);
    if Value<>NIL then
      Result:=TForm(GetOrdValue).Name
    else
      Result:='';
//    Result:=GetStrValue;
  end;

  procedure TTestFormPropertyEditor.GetValues(Proc: TGetStrProc);
  var
    i:Integer;
    loInterface: TKBTestFormInterface;
//    loInterfaceClass: TClass;
  begin
    loInterface:=TKBTestFormInterface(Self.GetComponent(0));
//    loInterfaceClass:= loInterface.InterfaceClass;
    with Screen do
      For i:=0 To FormCount-1 do
//         if Forms[i].InheritsFrom(loInterfaceClass) then
           Proc(Forms[i].Name);
  end;

  function TTestFormPropertyEditor.GetAttributes: TPropertyAttributes;
  begin
    GetAttributes:=[paValueList];
  end;

//  procedure TFormPropertyEditor.Edit;
//  begin
//  end;

  procedure TTestFormPropertyEditor.SetValue(const Value: String);
  var
    loForm: TForm;
    i: Integer;
    loInterface: TKBTestFormInterface;
  begin
    loInterface:=TKBTestFormInterface(Self.GetComponent(0));
    if Value='' then begin
      loInterface.InterfaceClass:=NIL;
      loForm:=nil;
    end else begin
      with Screen do
      for i:=0 to FormCount-1 do
        if CompareText(Forms[i].Name, Value)=0 then begin
          loForm:= Forms[i];
          Break;
        end;
    if (not Assigned(loForm)) and (Value<>'') then
      ShowMessage( Value+' �� �������� ������!' )
    else begin
//      SetStrValue(Value);
      loInterface.InterfaceClass:=GetClass(loForm.ClassName);
      SetOrdValue( LongInt(loForm) );
    end;
  end;
end;
}
procedure TKnowledgeBase.Verify(Protocol: TStrings);
var
  CurProtocol: TStrings;
begin
  if not Assigned(Protocol) then CurProtocol:=TStringList.Create else
      CurProtocol:=Protocol;
  inherited Verify(Protocol);
//  if not IsVerified then
//     if MessageDlg(CurProtocol[0],
//         mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
  if not Assigned(Protocol) then CurProtocol.Destroy;
end;

end.
