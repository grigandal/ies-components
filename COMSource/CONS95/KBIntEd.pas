unit KBIntEd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, Aspr95, SelComp, Types,
  KBInt, UForms;

type
  TKBFormInterfaceDesign = class(TForm)
    FormPanel: TPanel;
    Splitter1: TSplitter;
    TablePanel: TPanel;
    ButtonPanel: TPanel;
    Splitter2: TSplitter;
    FormComboBox: TComboBox;
    TextLabel: TLabel;
    ComponentStringGrid: TStringGrid;
    OKBitBtn: TBitBtn;
    SelectButton: TButton;
    ObjectLabel: TLabel;
    ObjectComboBox: TComboBox;
    GenerateButton: TButton;
    ClearButton: TButton;
    CreateButton: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FindObjects;
    procedure FindForms;
    procedure FindComponents;
    procedure FindControls;
    procedure FindAttr;
    procedure OKBitBtnClick(Sender: TObject);
    procedure SelectButtonClick(Sender: TObject);
    procedure ObjectComboBoxChange(Sender: TObject);
    procedure FormComboBoxChange(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    procedure GenerateButtonClick(Sender: TObject);
    procedure CreateButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    Activate: Boolean;
    Numbers: TStrings;
  end;

var
  KB: TKnowledgeBase;
  KBFormInterface: TKBFormInterface;
  CurrentForm: TForm;
  CurrentComponent: TComponent;
  CurrentObject: record_obj;
  SaveContextObj: TObject;

implementation

{$R *.DFM}

procedure TKBFormInterfaceDesign.FormActivate(Sender: TObject);
begin
  if Activate then begin
    Activate:=False;
    ComponentStringGrid.Cells[0,0]:='�������';
    ComponentStringGrid.Cells[1,0]:='���� ��������';
    ComponentStringGrid.Cells[2,0]:='���� ���.�������';
    ComponentStringGrid.Cells[3,0]:='���� ���.�������';
    ComponentStringGrid.Cells[4,0]:='���� ��������';
    FindForms;
    FindObjects;
    FindAttr;
  end;
end;

procedure TKBFormInterfaceDesign.FormCreate(Sender: TObject);
begin
  Activate:=True;
  KB:=NIL;
  CurrentForm:=NIL;
  CurrentComponent:=NIL;
  CurrentObject:=NIL;
  Numbers:=TStringList.Create;
end;

procedure TKBFormInterfaceDesign.FindObjects;
var
  i: Integer;
begin
  ObjectComboBox.Items.Clear;
  if Assigned(KB) then begin
    CurrentObject:=NIL;
    with KB do
      for i:=1 to ListObjs.Count do begin
        if i=1 then begin
          ObjectComboBox.Text:=
          record_obj(ListObjs.Items[i-1]).Comment;
          CurrentObject:=record_obj(ListObjs.Items[i-1]);
        end;
        ObjectComboBox.Items.Add(record_obj(ListObjs.Items[i-1]).
          Comment);
      end;
  end;
end;

procedure TKBFormInterfaceDesign.FindAttr;
var
  i: Integer;
begin
  if Assigned(CurrentObject) then begin
    ComponentStringGrid.RowCount:=CurrentObject.ListAttrs.Count+1;
    Numbers.Clear;
    with CurrentObject do
      for i:=1 to ListAttrs.Count do begin
        Numbers.Add(IntToStr(i));
        ComponentStringGrid.Cells[0,i]:=
          record_attr(ListAttrs.Items[i-1]).FComment;
        ComponentStringGrid.Cells[1,i]:=
          record_attr(ListAttrs.Items[i-1]).FControl;
        ComponentStringGrid.Cells[2,i]:=
          record_attr(ListAttrs.Items[i-1]).Ffactor1Control;
        ComponentStringGrid.Cells[3,i]:=
          record_attr(ListAttrs.Items[i-1]).Ffactor2Control;
        ComponentStringGrid.Cells[4,i]:=
          record_attr(ListAttrs.Items[i-1]).FaccuracyControl;
      end;
  end;
end;

procedure TKBFormInterfaceDesign.FindForms;
var
  i,j: Integer;
begin
  FormComboBox.Items.Clear;
  with Screen do
    for i:=1 to FormCount do begin
      if Forms[i-1] is TAtFrmDesigner then begin
        FormComboBox.Items.Add(Forms[i-1].Caption);
      end;
    end;
    if Assigned(KBFormInterface.Form.Designer) then
      if FormComboBox.Items.Count>0 then begin
        FormComboBox.Text:=KBFormInterface.Form.Designer.Caption;
        CurrentForm:=KBFormInterface.Form.Designer;
      end;
end;

procedure TKBFormInterfaceDesign.FindComponents;
var
  i: Integer;
begin
  if Assigned(CurrentForm) then
    with SelectComponentForm do begin
      SelectComponentComboBox.Items.Clear;
      for i:=1 to CurrentForm.ComponentCount do begin
        if i=1 then begin
          SelectComponentComboBox.Text:=
            CurrentForm.Components[i-1].Name;
        end;
        SelectComponentComboBox.Items.
          Add(CurrentForm.Components[i-1].Name);
      end;
    end;
end;

procedure TKBFormInterfaceDesign.FindControls;
var
  i: Integer;
begin
  if Assigned(CurrentForm) then
    with SelectComponentForm do begin
      SelectComponentComboBox.Items.Clear;
      for i:=1 to CurrentForm.ControlCount do begin
        if CurrentForm.Controls[i-1].Name<>'' then
          SelectComponentComboBox.Items.
            Add(CurrentForm.Controls[i-1].Name);
      end;
      if SelectComponentComboBox.Items.Count>0 then
          SelectComponentComboBox.Text:=
            SelectComponentComboBox.Items[0];
    end;
end;


procedure TKBFormInterfaceDesign.OKBitBtnClick(Sender: TObject);
var
  i,j: Integer;
begin
  if Assigned(CurrentObject) then
    for i:=1 to CurrentObject.ListAttrs.Count do begin
      for j:=1 to ComponentStringGrid.RowCount-1 do begin
        if StrToInt(Numbers[j-1])=i then begin
           record_attr(CurrentObject.ListAttrs.Items[i-1]).Control:=
          ComponentStringGrid.Cells[1,j];
           record_attr(CurrentObject.ListAttrs.Items[i-1]).Factor1Control:=
          ComponentStringGrid.Cells[2,j];
           record_attr(CurrentObject.ListAttrs.Items[i-1]).Factor2Control:=
          ComponentStringGrid.Cells[3,j];
           record_attr(CurrentObject.ListAttrs.Items[i-1]).AccuracyControl:=
          ComponentStringGrid.Cells[4,j];
       end;  
      end;
    end;
  ModalResult:=mrOK;
end;

procedure TKBFormInterfaceDesign.SelectButtonClick(Sender: TObject);
var
  i: Integer;
begin
  if ComponentStringGrid.Col>0 then begin
    Application.CreateForm(TSelectComponentForm,
                     SelectComponentForm);
    FindControls;
    SelectComponentForm.ShowModal;
    if Assigned(CurrentForm) then
      with SelectComponentForm do
        for i:=1 to CurrentForm.ControlCount do begin
          if SelectComponentComboBox.Text=
              CurrentForm.Controls[i-1].Name then begin
            CurrentComponent:=CurrentForm.Controls[i-1];
            ComponentStringGrid.
              Cells[ComponentStringGrid.Col,ComponentStringGrid.Row]:=
              CurrentForm.Controls[i-1].Name;
            break;
          end;
        end;
    SelectComponentForm.Free;
  end;  
end;

procedure TKBFormInterfaceDesign.ObjectComboBoxChange(Sender: TObject);
var
 i: Integer;
begin
  if Assigned(KB) then begin
    with KB do
      for i:=1 to ListObjs.Count do begin
        if ObjectComboBox.Text=
          record_obj(ListObjs.Items[i-1]).Comment then begin
          CurrentObject:=record_obj(ListObjs.Items[i-1]);
          FindAttr;
          break;
        end;
      end;
  end;
end;

procedure TKBFormInterfaceDesign.FormComboBoxChange(Sender: TObject);
var
  i: Integer;
begin
  with Screen do
    for i:=1 to FormCount do begin
      if Forms[i-1].Caption=FormComboBox.Text then begin
        CurrentForm:=Forms[i-1];
        if Forms[i-1] is TAtFrmDesigner then
          KBFormInterface.Form.Designer:=TAtFrmDesigner(CurrentForm);
        break;
      end;
    end;
end;

procedure TKBFormInterfaceDesign.ClearButtonClick(Sender: TObject);
var
  i: Integer;
begin
  if Assigned(KBFormInterface.Form.Designer) then begin
    FindObjects;
    FindAttr;
    while KBFormInterface.Form.Designer.ComponentCount>0 do begin
      if Assigned(CurrentObject) then begin
        with CurrentObject do
          for i:=1 to ListAttrs.Count do begin
            if record_attr(ListAttrs.Items[i-1]).FControl=
              KBFormInterface.Form.Designer.Components[0].Name then
              record_attr(ListAttrs.Items[i-1]).FControl:='';
          end;
      end;
      KBFormInterface.Form.Designer.Components[0].Free;
    end;
    if Assigned(CurrentObject) then
      FindAttr;
  end;
end;

procedure TKBFormInterfaceDesign.GenerateButtonClick(Sender: TObject);
var
  SaveDesignMode: TDesignMode;
  i: Integer;
begin
  if Assigned(KBFormInterface.Form.Designer) then begin
    SaveDesignMode:=KBFormInterface.DesignMode;
    KBFormInterface.DesignMode:=dmDefault;
    for i:=1 to KB.ListObjs.Count do begin
      if ObjectComboBox.Text=record_obj(KB.ListObjs.Items[i-1]).Comment then begin
        KBFormInterface.Design(record_obj(KB.ListObjs.Items[i-1]));
        break;
      end;
    end;
    KBFormInterface.DesignMode:=SaveDesignMode;
    FindObjects;
    FindAttr;
  end;
end;

procedure TKBFormInterfaceDesign.CreateButtonClick(Sender: TObject);
begin
  ModalResult:=50;
end;

procedure TKBFormInterfaceDesign.FormDestroy(Sender: TObject);
begin
  Numbers.Destroy;
end;

end.
