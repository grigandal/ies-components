//**************************************************************************//
//*                                                                        *//
//*  ������, ���������� ����������� ��������� � �������, ��� ��������� ��- *//
//*  �������� � �������������� ���������.                                  *//
//*                                                                        *//
//*  �����: ������ �.�.                                                    *//
//*  ������: 2.1                                                           *//
//*  ���� ������ ����������: 30.11.99                                      *//
//*  ����� ������ �������� ������� �.�. 27.11.00                           *//
//*  ������������ ������: �������� ������� �������� ���������              *//
//**************************************************************************//
unit DR_Calculus;

//----------------------------------------------------------------------------
interface

uses SysUtils, VCL.Dialogs;

//**************************************************************************//
//*                                                                        *//
//*  ���������� ������� ���������:                                         *//
//*                                                                        *//
//*   Input      ::= Arithmetic | Logic                                    *//
//*  --------------------------------------------------------------------- *//
//*   Arithmetic ::= Mult | Mult + Arithmetic | Mult - Arithmetic          *//
//*   Mult       ::= Term | Term * Mult | Term / Mult                      *//
//*   Term       ::= ( Arithmetic ) | Number                               *//
//*  --------------------------------------------------------------------- *//
//*   Logic      ::= Conj | Conj OR Logic                                  *//
//*   Conj       ::= Fuzz | Fuzz AND Conj                                  *//
//*   Fuzz       ::= ( Logic ) | NOT ( Logic ) | Bool_Digit                *//
//*  --------------------------------------------------------------------- *//
//*   Number     ::= /< �������������� ����� >/                            *//
//*   Bool_Digit ::= /< �������������� ����� �� ��������� [0;1] />         *//
//*                                                                        *//
//**************************************************************************//

//*** ��������� ������� ����������� ��������� ***//
procedure ParseLogic (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� ��������������� ��������� ***//
procedure ParseArithmetic (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� �������������� ���������������� ***//
procedure ParseMult (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� �������������� ������ ***//
procedure ParseTerm (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� ���������� ��������� ***//
procedure ParseConj (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� ���������� �������� ���������� ***//
procedure ParseFuzz (iValue : string; var Result_x : real; var RestValue : string);

//*** ��������� ������� �������������� ����� ***//
procedure ParseNumber (iValue : string; var Result_x : real; var RestValue : string);

//*** ������� ����������� ���� ��������� ***//
function DetectLogic (iValue : string) : Boolean;

//*** ������� ������� ������������ ������� � �������� �������� ***//
function Calculate (iValue : string) : real;

//*** ������� ����������� ������� ������ iValue � ������� Pos ***//
function GetRestString (Pos : Integer; iValue : string) : string;

//*** ������� �������� �������� �� ���� �������� ***//
function Minimum (iX, iY : real) : real;

//*** ������� �������� ��������� �� ���� �������� ***//
function Maximum (iX, iY : real) : real;

//----------------------------------------------------------------------------
implementation

function DetectLogic (iValue : string) : Boolean;
var
  Pos_OR   : Integer; // ������� ����� "OR" � ������.
  Pos_AND  : Integer; // ������� ����� "AND" � ������.
  Pos_NOT  : Integer; // ������� ����� "NOT" � ������.
  Result_x : Boolean; // ������������ ��������: True = Logic | False = Arithmetic.
begin
  Pos_OR:=Pos (' OR ', iValue);                             // ���������� ������� ��������� "OR".
  Pos_AND:=Pos (' AND ', iValue);                           // ���������� ������� ��������� "AND".
  Pos_NOT:=Pos (' NOT ', iValue);                           // ���������� ������� ��������� "NOT".
  if ((Pos_OR>0) or (Pos_AND>0) or (Pos_NOT>0)) then begin  // ���� ���� �� ���� ��������� ������������, ��...
    Result_x:=True;                                         //   ��������� ������.
  end
  else begin                                                // �����...
    Result_x:=False;                                        //   ��������� ����.
  end;
  DetectLogic:=Result_x;                                    // ������� ��������, ����� �� �������.
end;

function Calculate (iValue : string) : real;
var
  Result_x : real;   // ������������ ��������.
  Temp_str : string; // ������� ������� ������.
begin
  if (DetectLogic (AnsiUpperCase (iValue))) then begin             // ���� ������������� ���������� ���������, ��...
    ParseLogic (AnsiUpperCase (iValue), Result_x, Temp_str);       //   ��������� ���������� ���������.
  end
  else begin                                                       // �����...
    ParseArithmetic (AnsiUpperCase (iValue), Result_x, Temp_str);  //   ��������� �������������� ���������.
  end;
  Calculate:=Result_x;                                             // ������� ��������, ����� �� �������.
end;

procedure ParseLogic (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_OR    : Integer; // ������� ����� "OR"
  GetConj   : real;    // ������������ �������� ���������.
  GetLog    : real;    // ������������ �������� ����������� ����������� ���������.
  Temp_str  : string;  // ��������� ���������� ��� �������� ������� ������.
  Temp_res  : string;  // ������� ������� ������.
begin
  ParseConj (iValue, GetConj, Temp_res);           // ��������� ��������� ���������.
  Pos_OR:=Pos ('OR ', TrimLeft(Temp_res));                  // ������� ������� ������ "OR".
  if (Pos_OR=1) then begin                         // ���� ���������� ��� ���������, ��...
    Temp_str:=GetRestString (Pos_OR+3, TrimLeft(Temp_res));  //   ������� ������� ������ ��� ������� "OR".
    ParseLogic (Temp_str, GetLog, RestValue);      //   ��������� ���������� ���������� ���������.
    Result_x:=Maximum (GetConj, GetLog);           //   ������� ��������� ���������� (��������).
  end
  else begin                                       // �����...
    Result_x:=GetConj;                             //   ��������� ����� ���������.
    RestValue:=Temp_res;                           //   ����� ������ ������������.
  end;
end;

procedure ParseArithmetic (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_Plus  : Integer; // ������� ����� "+".
  Pos_Minus : Integer; // ������� ����� "-".
  GetMult   : real;    // ������������ �������� ���������������.
  GetArith  : real;    // ������������ �������� ����������� ��������������� ���������.
  Temp_res  : string;  // ������� ������� ������.
  Temp_str  : string;  // ��������� ���������� ��� �������� ������� ������.
begin
  ParseMult (iValue, GetMult, Temp_res);                                    // ��������� ��������������.
  Pos_Plus:=Pos ('+', Temp_res);                                            // ������� ������� ����� "+".
  Pos_Minus:=Pos ('-', Temp_res);                                           // ������� ������� ����� "-".
  if (Pos_Plus=1) then begin                                                // ���� ������ ����� ���� "+", ��...
    Temp_str:=GetRestString (Pos_Plus+1, Temp_res);                         //   ������� ������ ����� ����� "+".
    ParseArithmetic (Temp_str, GetArith, RestValue);                        //   ��������� �������������� ���������.
    Result_x:=GetMult+GetArith;                                             //   ���������� �������������� � ��������� ���������.
  end;
  if (Pos_Minus=1) then begin                                               // ���� ������ ����� ���� "-", ��...
    Temp_str:=GetRestString (Pos_Minus+1, Temp_res);                        //   ������� ������ ����� ����� "-".
    ParseArithmetic (Temp_str, GetArith, RestValue);                        //   ��������� �������������� ���������.
    Result_x:=GetMult-GetArith;                                             //   �������� �� ��������������� ��������� ��������.
  end;
  if ((Pos_Plus<>1) and (Pos_Minus<>1)) then begin                          // ���� �� ����� "+" ��� "-" ������� � ����������, ��...
    Result_x:=GetMult;                                                      //   ��������� ����� ���������������.
    RestValue:=Temp_res;                                                    //   ����� ������ ������������.
  end;
end;

procedure ParseMult (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_Mult : Integer; // ������� ����� "*".
  Pos_Div  : Integer; // ������� ����� "/".
  GetTerm  : real;    // ���������� ��� ��������� �������� �����.
  GetMult  : real;    // ���������� ��� �������� �������� ���������� ���������������.
  Temp_str : string;  // ��������� ���������� ��� �������� ������� ������.
  Temp_res : string;  // ��������� ���������� ��� �������� ������� ������.
begin
  ParseTerm (iValue, GetTerm, Temp_str);                                // ��������� ����.
  Pos_Mult:=Pos ('*', Temp_str);                                        // ������� ������� ����� "*".
  Pos_Div:=Pos ('/', Temp_str);                                         // ������� ������� ����� "/".
  Temp_res:=Temp_str;                                                   //   ������� ������� ������ �� ����������.
  if (Pos_Mult=1) then begin                                            // ���� ������ ����� ���� "*", ��...
    Temp_str:=GetRestString (Pos_Mult+1, Temp_str);                     //   ������� ������ ����� ����� "*".
    ParseMult (Temp_str, GetMult, Temp_res);                            //   ��������� ��������������.
    GetTerm:=GetTerm*GetMult;                                           //   ����������� ���� � ����������������.
    Temp_str:=Temp_res;                                                 //   �������� ��������, �� ��� ����������.
  end;
  if (Pos_Div=1) then begin                                             // ���� ������ ����� ���� "/", ��...
    Temp_str:=GetRestString (Pos_Div+1, Temp_str);                      //   ������� ������ ����� ����� "/".
    ParseMult (Temp_str, GetMult, Temp_res);                            //   ��������� ��������������.
    GetTerm:=GetTerm/GetMult;                                           //   ����� ���� �� ��������������.
    Temp_str:=Temp_res;                                                 //   �������� ��������, �� ��� ����������.
  end;
  if ((Pos_Mult=0) and (Pos_Div=0)) then begin                          // ���� ������ "*" ��� "/" ���, ��...
    Temp_res:=Temp_str;                                                 //   ������� ������� ������ �� ����������.
  end;
  Temp_str:=Temp_res;                                                   //   �������� ��������, �� ��� ����������.
  Result_x:=GetTerm;                                                    // ��������� ����� �����.
  RestValue:=Temp_res;                                                  // ������� ������� ������ ������������.
end;

procedure ParseTerm (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_Bracket : Integer; // ������� ����� '('.
  Temp_str    : string;  // ��������� ���������� ��� �������� ������� ������.
  Temp_res    : string;  // ��������� ���������� ��� �������� ������� ������.
  GetArith    : real;    // ���������� ��� ��������� �������� ����������� ��������������� ���������.
begin
  Pos_Bracket:=Pos ('(', iValue);                       // ������� ������� ����� "(".
  if (Pos_Bracket=1) then begin                         // ���� ����� ���� ����, ��...
    Temp_str:=GetRestString (Pos_Bracket+1, iValue);    //   ������� ������ �� ���� ������.
    ParseArithmetic (Temp_str, GetArith, Temp_res);     //   ��������� �������������� ���������.
    Pos_Bracket:=Pos (')', Temp_res);                   //   ������� ������� ����� ")".
    if (Pos_Bracket<>1) then begin                      //   ���� ����������� ������ ���, ��...
                                                        //     �������������� �������� - ��� ����������� ������.
    end;
    Temp_res:=GetRestString (Pos_Bracket+1, Temp_res);  //   ������� ������� ������ ����� ����������� ������.
    Result_x:=GetArith;                                 //   ��������� ����� ��������������� ���������.
    RestValue:=Temp_res;                                //   ������� ������ ������������.
  end
  else begin                                            // �����...
    ParseNumber (iValue, Result_x, RestValue);          //   ��������� �����, � ��� ����� ����������.
  end;
end;

procedure ParseConj (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_AND  : Integer; // ������� ����� "AND".
  GetFuzz  : real;    // ���������� ��� ��������� �������� ��������� �����.
  GetConj  : real;    // ���������� ��� �������� �������� ���������� ���������.
  Temp_str : string;  // ��������� ���������� ��� �������� ������� ������.
  Temp_res : string;  // ��������� ���������� ��� �������� ������� ������.
begin
  ParseFuzz (iValue, GetFuzz, Temp_str);                               // ��������� �������� ����.
  Pos_AND:=Pos ('AND ', TrimLeft(Temp_str));                                    // ������� ������� ����� "AND".
  if (Pos_AND=1) then begin                                            // ���� ���������� ��� ���������, ��...
    Temp_str:=GetRestString (Pos_AND+4, TrimLeft(Temp_str));                     //   ������� ������ ����� ����� "*".
    ParseConj (Temp_str, GetConj, Temp_res);                           //   ��������� ��������������.
    Result_x:=Minimum (GetFuzz, GetConj);                              //   ������� ����������� ���� � ����������������.
    RestValue:=Temp_res;                                               //   ������������ ������� ������.
  end
  else begin                                                           // ���� ���� "AND" ���-�� �����, ��...
    Result_x:=GetFuzz;                                                 //   ��������� ����� ��������� �����.
    RestValue:=Temp_str;                                               //   ������� ������� ������ ������������.
  end;
end;

procedure ParseFuzz (iValue : string; var Result_x : real; var RestValue : string);
var
  Pos_Bracket : Integer; // ������� ����� '('.
  Pos_NOT     : Integer; // ������� ����� 'NOT'.
  Temp_str    : string;  // ��������� ���������� ��� �������� ������� ������.
  Temp_res    : string;  // ��������� ���������� ��� �������� ������� ������.
  GetLogic    : real;    // ���������� ��� ��������� �������� ����������� ��������� ���������.
begin
  Pos_Bracket:=Pos ('(', iValue);                       // ������� ������� ����� "(".
  Pos_NOT:=Pos ('NOT ', TrimLeft(iValue));                       // ������� ������� ����� "NOT".
  if (Pos_Bracket=1) then begin                         // ���� ����� ���� ����, ��...
    Temp_str:=GetRestString (Pos_Bracket+1, iValue);    //   ������� ������ �� ���� ������.
    ParseLogic (Temp_str, GetLogic, Temp_res);          //   ��������� �������� ���������.
    Pos_Bracket:=Pos (')', Temp_res);                   //   ������� ������� ����� ")".
    if (Pos_Bracket=0) then begin                       //   ���� ����������� ������ ���, ��...
                                                        //     �������������� �������� - ��� ����������� ������.
    end;
    Temp_res:=GetRestString (Pos_Bracket+1, Temp_res);  //   ������� ������� ������ ����� ����������� ������.
    Result_x:=GetLogic;                                 //   ��������� ����� ��������������� ���������.
    RestValue:=Temp_res;                                //   ������� ������ ������������.
  end;
  if (Pos_NOT=1) then begin                             // ���� ���� ���� "NOT", ��...
    Temp_str:=GetRestString (Pos_NOT+4, TrimLeft(iValue));        //   ������� ������ ����� ����� �����.
    ParseLogic (Temp_str, Result_x, RestValue);         //   ��������� �����.
    Result_x:=1-Result_x;
  end;
  if ((Pos_Bracket<>1) and (Pos_NOT<>1)) then begin     // �����...
    ParseNumber (iValue, Result_x, RestValue);          //   ��������� �����, � ��� ����� ����������.
  end;
end;

function GetRestString (Pos : Integer; iValue : string) : string;
var
  i        : Integer; // ������� ����� For.
  Result_x : string;  // ������������ ���������.
begin
  Result_x:='';                           // �������� ���������.
  for i:=Pos to Length (iValue) do begin  // ��� ���� �������� � �������� �� ����� ������...
    Result_x:=Result_x+iValue[i];         //   � ���������� ���������� �� �������.
  end;
  GetRestString:=Result_x;                // ���������� ���������.
end;

function Minimum (iX, iY : real) : real;
var
  Result_x :  real; // ������������ ���������.
begin
  if (iX<=iY) then begin // ����������� ������� ��������� ��������.
    Result_x:=iX;
  end
  else begin
    Result_x:=iY;
  end;
  Minimum:=Result_x;
end;

function Maximum (iX, iY : real) : real;
var
  Result_x :  real; // ������������ ���������.
begin
  if (iX<=iY) then begin // ����������� ������� ��������� ���������.
    Result_x:=iY;
  end
  else begin
    Result_x:=iX;
  end;
  Maximum:=Result_x;
end;

procedure ParseNumber (iValue : string; var Result_x : real; var RestValue : string);
var
  Stop     : Boolean; // ��������� �������� �������� �������.
  Minus    : Boolean; // ��������� ����������� ����� "-".
  Plus     : Boolean; // ��������� ����������� ����� "+".
  E        : Boolean; // ��������� ����������� ����� "E".
  Point    : Boolean; // ��������� ����������� ����� ".".
  i        : Integer; // ������� ��� ����� For.
  Temp_res : string;  // ������ � ������.
  Temp_str : string;  // ��������� ���������� ��� ��������� ������� ������.
begin
  Stop:=False;
  Minus:=False;
  Plus:=False;
  E:=false;
  Point:=False;
  Temp_res:='';
  Temp_str:='';
  for i:=1 to Length (iValue) do begin
    case iValue[i] of
      '-': //*** ���� "-" ***//
        begin
          if (not Minus) then begin
            Minus:=True;
            Temp_res:=Temp_res+iValue[i];
          end
          else begin
            Stop:=True;
          end;
        end;
      '+': //*** ���� "+" ***//
        begin
          if (not Plus) then begin
            Plus:=True;
            Temp_res:=Temp_res+iValue[i];
          end
          else begin
            Stop:=True;
          end;
        end;
      '0': //*** ���� "0" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '1': //*** ���� "1" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '2': //*** ���� "2" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '3': //*** ���� "3" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '4': //*** ���� "4" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '5': //*** ���� "5" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '6': //*** ���� "6" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '7': //*** ���� "7" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '8': //*** ���� "8" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '9': //*** ���� "9" ***//
        begin
          Minus:=True;
          Plus:=True;
          Temp_res:=Temp_res+iValue[i];
        end;
      '.': //*** ���� "." ***//
        begin
          if (not Point) then begin
            Minus:=True;
            Plus:=True;
            Point:=True;
            Temp_res:=Temp_res+iValue[i];
          end
          else begin
            Stop:=True;
          end;
        end;
      'e': //*** ���� "e" ***//
        begin
          if (not E) then begin
            E:=True;
            Minus:=False;
            Plus:=False;
            Temp_res:=Temp_res+iValue[i];
          end
          else begin
            Stop:=True;
          end;
        end;
      'E': //*** ���� "E" ***//
        begin
          if (not E) then begin
            E:=True;
            Minus:=False;
            Plus:=False;
            Temp_res:=Temp_res+iValue[i];
          end
          else begin
            Stop:=True;
          end;
        end;
    else
      begin
        Stop:=True;
      end;
    end;
    if Stop then Break;
  end;
  if Temp_res='' then begin
    ShowMessage('������ � �����');
    Result_x:=0;
  end else
    Result_x:=StrToFloat (Temp_res);
  for i:=Length (Temp_res)+1 to Length (iValue) do begin
    Temp_str:=Temp_str+iValue[i];
  end;
  RestValue:=Temp_str;
end;

//----------------------------------------------------------------------------
end.
