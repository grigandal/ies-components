unit KBCompon;

interface

uses
  Classes, SysUtils, TypInfo, VCL.Dialogs;
  //DsgnIntf, {in Delphi 4}
  //DesignEditors; {in Delphi 6}

{const
  ENDING_FILE      = 'ENDING.MRF';
  WORDS_FILE       = 'NEWLEX.VKL';
  SEM_FILE         = 'SWORDS.SEM';
  MOD_FILE         = 'MODELS.SEM';
  SNT_FILE         = 'SENTENCE.TXT';
  LEX_FILE         = 'LEXF.VKL';}

type

TPtrProc = procedure( Source:TObject; var AValue:Variant; var Success:Boolean ) of object;

TTestableCollection=class(TCollection)
    private
      { Private declarations }
    protected
      { Protected declarations }
    public
      procedure RegistryTestableProperty(PropName: String); virtual;
      procedure Verify(Protocol: TStrings);  virtual;
   published
end;

TTestableOwnedCollection=class(TOwnedCollection)
    private
      { Private declarations }
    protected
      { Protected declarations }
    public
      procedure RegistryTestableProperty(PropName: String); virtual;
      procedure Verify(Protocol: TStrings);  virtual;
   published
end;

TTestableCollectionItem=class(TCollectionItem)
    private
      { Private declarations }
    protected
      { Protected declarations }
    public
      procedure RegistryTestableProperty(PropName: String); virtual;
      procedure Verify(Protocol: TStrings);  virtual;
   published
end;

TTestableComponent=class(TComponent)
    private
//      FLeft: Integer;
//      FTop: Integer;
    protected
      { Protected declarations }
    public
      procedure RegistryTestableProperty(PropName: String); virtual;
      procedure Verify(Protocol: TStrings);  virtual;
   published
//     property Left: Integer read FLeft write FLeft;
//     property Top: Integer read FTop write FTop;
end;

TKBCollection=class(TTestableCollection)
  private
    { Private declarations }
    Registed: TStringList;
  protected
    { Protected declarations }
  public
    IsVerified: Boolean;
    constructor Create(ItemClass: TCollectionItemClass); {override;}
    destructor Destroy; override;
    procedure RegistryTestableProperty(PropName: String); override;
    procedure Verify(Protocol: TStrings);  override;
  published
end;

TKBOwnedCollection=class(TTestableOwnedCollection)
  private
    { Private declarations }
    Registed: TStringList;
  protected
    { Protected declarations }
  public
    IsVerified: Boolean;
    Owner: TPersistent;
    constructor Create(AOwner: TPersistent;ItemClass: TCollectionItemClass); {override;}
    destructor Destroy; override;
    procedure RegistryTestableProperty(PropName: String); override;
    procedure Verify(Protocol: TStrings);  override;
  published
end;

TKBCollectionItem=class(TTestableCollectionItem)
  private
    { Private declarations }
    Registed: TStringList;
  protected
    { Protected declarations }
  public
    IsVerified: Boolean;
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure RegistryTestableProperty(PropName: String); virtual;
    procedure Verify(Protocol: TStrings);  override;
    procedure Assign(KBCollectionItem: TKBCollectionItem); virtual;
  published
end;

TKBComponent=class(TTestableComponent)
  private
    { Private declarations }
    Registed: TStringList;
  protected
    { Protected declarations }
  public
    IsVerified: Boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure RegistryTestableProperty(PropName: String); override;
    procedure Verify(Protocol: TStrings);  override;
  published
end;

TDictionaryEntryItemType=(WordHead,WordEnd,WordPred,WordAttr,WordList,WordCateg);

TAtDictionary=class(TKBComponent)
private
  FDictionaryHeader: String;
  FDictionaryEntry: TOwnedCollection;
  FFileName: String;
  FindCondition: TOwnedCollection;
public
  function Add: Integer; virtual; abstract; // �������� ������ (DictionaryEntry) � �������. ���������� ��� ������
  function Delete: Integer; virtual; abstract; // ������� (DictionaryEntry) �� �������. ���������� ��� ������
  function Edit: Integer; virtual; abstract; // ������� �������� ������ �/��� ����� �������. ���������� ��� ������
  function FindFirst(Condition:TKBOwnedCollection): Integer; virtual; // ����� ������ �� ��������� ���������. ���������� ��� ������
  function FindNext: Integer; virtual; abstract; // ������� �� ��������� ��������� ������. ���������� ��� ������
  function Open: Integer; virtual; abstract; // ������� ���� �������. ���������� ��� ������
  function Close: Integer; virtual; abstract; // ������� ���� �������. ���������� ��� ������
//  constructor Create(AOwner: TComponent); override;
//  destructor Destroy; override;
published
  property DictionaryHeader: String
    read FDictionaryHeader write FDictionaryHeader; // ��������� �������
  property DictionaryEntry: TOwnedCollection
    read FDictionaryEntry write FDictionaryEntry; // ������� ������,
        // ������� ��������� ��� ����������� ���������, ��������� � ������� ��� ��������� �� �������
  property FileName: String
    read FFileName write FFileName; // ��� ����� �������
end;

TAtDictionaryEntryItem=class(TKBCollectionItem) // �������� ��������, � ������� ������ �������
private
  FItemType: TDictionaryEntryItemType;
  FItemName: String;
  FItemText: String;
public
  property ItemType: TDictionaryEntryItemType
    read FItemType write FItemType; // ��� ��������
 property ItemName: String
    read FItemName write FItemName; // ��� ��������
 property ItemText: String // �������� ��������
    read FItemText write FItemText;
end;

function VarToObj(V:Variant):TObject;
function FindProtocolMessage(Protocol: TStrings; Msg: String): Boolean;

implementation

Function VarToObj(V:Variant):TObject;
var P:Pointer;
begin
  P:=TVarData(V).VPointer;
  Result:=P;
end;

procedure TTestableCollection.Verify(Protocol: TStrings);
begin
end;

procedure TTestableOwnedCollection.Verify(Protocol: TStrings);
begin
end;

procedure TTestableCollection.RegistryTestableProperty(PropName: String);
begin
end;

procedure TTestableOwnedCollection.RegistryTestableProperty(PropName: String);
begin
end;

procedure TTestableCollectionItem.Verify(Protocol: TStrings);
begin
end;

procedure TTestableCollectionItem.RegistryTestableProperty(PropName: String);
begin
end;

procedure TTestableComponent.Verify(Protocol: TStrings);
begin
end;

procedure TTestableComponent.RegistryTestableProperty(PropName: String);
begin
end;

constructor TKBCollection.Create(ItemClass: TCollectionItemClass);
begin
  inherited Create(ItemClass);
  Registed:=TStringList.Create;
  IsVerified:=True;
end;

destructor TKBCollection.Destroy;
begin
  Registed.Destroy;
  inherited Destroy;
end;

constructor TKBOwnedCollection.Create(AOwner: TPersistent;ItemClass: TCollectionItemClass);
begin
  inherited Create(AOwner,ItemClass);
  Registed:=TStringList.Create;
  IsVerified:=True;
  Owner:=AOwner;
end;

destructor TKBOwnedCollection.Destroy;
begin
  Registed.Destroy;
  inherited Destroy;
end;

constructor TKBCollectionItem.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  Registed:=TStringList.Create;
  IsVerified:=True;
end;

destructor TKBCollectionItem.Destroy;
begin
  Registed.Destroy;
  inherited Destroy;
end;

procedure TKBCollectionItem.Assign(KBCollectionItem: TKBCollectionItem);
begin
  Registed.AddStrings(KBCollectionItem.Registed);
  IsVerified:=KBCollectionItem.IsVerified;
end;

constructor TKBComponent.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Registed:=TStringList.Create;
  IsVerified:=True;
end;

destructor TKBComponent.Destroy;
begin
  Registed.Destroy;
  inherited Destroy;
end;

procedure TKBCollection.Verify(Protocol: TStrings);
var
  i: Integer;
begin
  IsVerified:=True;
  for i:=1 to Count do begin
    if Items[i-1] is TKBCollectionItem then
      TKBCollectionItem(Items[i-1]).Verify(Protocol);
      if not TKBCollectionItem(Items[i-1]).IsVerified then IsVerified:=False;
  end;
end;

procedure TKBOwnedCollection.Verify(Protocol: TStrings);
var
  i: Integer;
begin
  IsVerified:=True;
  for i:=1 to Count do begin
    if Items[i-1] is TKBCollectionItem then begin
      TKBCollectionItem(Items[i-1]).Verify(Protocol);
      if not TKBCollectionItem(Items[i-1]).IsVerified then IsVerified:=False;
    end;
  end;
end;

procedure TKBCollectionItem.Verify(Protocol: TStrings);
var
  PropInfo: PPropInfo;
  i: Integer;
  AObject: TObject;
  AString: String;
  ProtocolStr: String;
  CurProtocol: TStrings;
begin
  IsVerified:=True;
  if not Assigned(Protocol) then CurProtocol:=TStringList.Create else
    CurProtocol:=Protocol;
  for i:=1 to Registed.Count do begin
    PropInfo:=GetPropInfo(Self.ClassInfo,Registed[i-1]);
    Case PropInfo^.PropType^.Kind of
      tkClass: begin
        AObject:=VarToObj(GetOrdProp(Self,PropInfo));
        if AObject=NIL then begin
          ProtocolStr:='�� ������ ��������: '{+Self.Owner.Name+'.'}+Self.ClassName{Name}+'.'+Registed[i-1];
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              CurProtocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end else
        if AObject is TKBCollection then begin
          TKBCollection(AObject).Verify(CurProtocol);
          if not TKBCollection(AObject).IsVerified then IsVerified:=False;
        end else
        if AObject is TKBOwnedCollection then begin
          TKBOwnedCollection(AObject).Verify(CurProtocol);
          if not TKBOwnedCollection(AObject).IsVerified then IsVerified:=False;
        end else
        if AObject is TKBComponent then begin
          TKBComponent(AObject).Verify(CurProtocol);
          if not TKBComponent(AObject).IsVerified then IsVerified:=False;
        end;
      end;
      tkString,tkLString: begin
        if GetStrProp(Self,PropInfo)='' then begin
          ProtocolStr:='�� ������ ��������: '{+Self.Owner.Name+'.'}+Self.ClassName{Name}+'.'+Registed[i-1];
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              CurProtocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end;
      end;
    end;
  end;
  if not Assigned(Protocol) then CurProtocol.Destroy;
end;

procedure TKBComponent.Verify(Protocol: TStrings);
var
  PropInfo: PPropInfo;
  i: Integer;
  AObject: TObject;
  ProtocolStr: String;
  CurProtocol: TStrings;
begin
  IsVerified:=True;
  if not Assigned(Protocol) then CurProtocol:=TStringList.Create else
    CurProtocol:=Protocol;
  if Self.Name='' then begin
    IsVerified:=False;
    Exit;
  end;
  for i:=1 to Registed.Count do begin
    PropInfo:=GetPropInfo(Self.ClassInfo,Registed[i-1]);
    Case PropInfo^.PropType^.Kind of
      tkClass: begin
        AObject:=VarToObj(GetOrdProp(Self,PropInfo));
        if AObject=NIL then begin
          ProtocolStr:='�� ������ ��������: '+Self.Owner.Name+'.'+Self.Name+'.'+Registed[i-1];
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              CurProtocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;    
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end else
        if AObject is TKBCollection then begin
          TKBCollection(AObject).Verify(CurProtocol);
          if not TKBCollection(AObject).IsVerified then IsVerified:=False;
        end else
        if AObject is TKBOwnedCollection then begin
          TKBOwnedCollection(AObject).Verify(CurProtocol);
          if not TKBOwnedCollection(AObject).IsVerified then IsVerified:=False;
        end else
        if AObject is TKBComponent then begin
          TKBComponent(AObject).Verify(CurProtocol);
          if not TKBComponent(AObject).IsVerified then IsVerified:=False;
          if (not IsVerified) and
             (TKBComponent(AObject).Name='') then begin
            ProtocolStr:='�� ������ ��������: '+Self.Owner.Name+'.'+Self.Name+'.'+Registed[i-1];
            if Assigned(CurProtocol) then begin
              if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
                CurProtocol.Add(ProtocolStr);
                if MessageDlg(ProtocolStr,
                  mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
              end;
            end else begin
                if MessageDlg(ProtocolStr,
                  mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;
          end;
        end;
      end;
      tkString,tkLString: begin
        if GetStrProp(Self,PropInfo)='' then begin
          ProtocolStr:='�� ������ ��������: '{+Self.Owner.Name+'.'}+Self.ClassName{Name}+'.'+Registed[i-1];
          if Assigned(CurProtocol) then begin
            if not FindProtocolMessage(CurProtocol,ProtocolStr) then begin
              Protocol.Add(ProtocolStr);
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
            end;    
          end else begin
              if MessageDlg(ProtocolStr,
                mtWarning,[mbOK, mbCancel],0)=2 then IsVerified:=False;
          end;
        end;
      end;
    end;
  end;
  if not Assigned(Protocol) then CurProtocol.Destroy;
end;

procedure TKBCollection.RegistryTestableProperty(PropName: String);
begin
  Registed.Add(PropName);
end;

procedure TKBOwnedCollection.RegistryTestableProperty(PropName: String);
begin
  Registed.Add(PropName);
end;

procedure TKBCollectionItem.RegistryTestableProperty(PropName: String);
begin
  Registed.Add(PropName);
end;

procedure TKBComponent.RegistryTestableProperty(PropName: String);
begin
  Registed.Add(PropName);
end;

function TAtDictionary.FindFirst(Condition: TKBOwnedCollection): Integer;
begin
  FindCondition:=Condition;
  Result:=0;
end;

function FindProtocolMessage(Protocol: TStrings; Msg: String): Boolean;
var
  i: Integer;
begin
  FindProtocolMessage:=False;
  if not Assigned(Protocol) then Exit;
  for i:=1 to Protocol.Count do begin
    if Msg=Protocol[i-1] then begin
      FindProtocolMessage:=True;
      break;
    end;
  end;
end;

end.
