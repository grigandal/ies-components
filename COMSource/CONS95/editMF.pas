unit editMF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, KB_Fuzzy, ExtCtrls, TeeProcs, TeEngine, Chart, Series,
  ComCtrls;

type
  TeditMFForm = class(TForm)
    Label1: TLabel;
    edtNameMF: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtX: TEdit;
    edtY: TEdit;
    btnAddPoint: TButton;
    btnDelPoint: TButton;
    Label4: TLabel;
    lbPointList: TListBox;
    btnOk: TButton;
    btnCancel: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    sbError: TStatusBar;
    Series2: TLineSeries;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure btnAddPointClick(Sender: TObject);
    procedure lbPointListClick(Sender: TObject);
    procedure btnDelPointClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure AnalizeStr( str: string; var X:Double; var Y:Double);
    procedure FormShow(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure Redraw(memfunc : TMembershipFunction);
    procedure AddToListBox (var X: Double; var Y: Double);
    procedure CheckValues(Sender: TObject);
    procedure edtNameMFChange(Sender: TObject);
    procedure Series1Click(Sender: TChartSeries; ValueIndex: Integer;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
{    procedure Chart1DblClick(Sender: TObject);
    procedure DotToComma (var str: string);
}   procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    editLVFormPtr: TForm;
    MFCollection: TCollection;
    MF: TMembershipFunction;
    Flag: Boolean;
    MFbackup : TMembershipFunction;
    Error, XSame : Boolean;
  end;

implementation

uses editLV;
{$R *.dfm}

procedure TeditMFForm.btnAddPointClick(Sender: TObject);
var
  num : Integer;
  X,Y:Double;
begin
  if (edtX.Text<>'') and (edtY.Text<>'') and (Error=False) then
  begin
    if btnAddPoint.Caption='�������� � ������' then
    begin
      X:=StrToFloat(edtX.Text);
      Y:=StrToFloat(edtY.Text);
      AddToListBox(X,Y);
      lbPointList.ItemIndex:=-1;
      btnDelPoint.Enabled:=False;
      Redraw(MFbackup);
    end
    else
    begin
      num := lbPointList.ItemIndex;
      MFbackup.Points.Delete(num);
      lbPointList.DeleteSelected;
      lbPointList.ItemIndex:=num;
      X:=StrToFloat(edtX.Text);
      Y:=StrToFloat(edtY.Text);
      AddToListBox(X,Y);
      lbPointList.ItemIndex:=-1;
      btnDelPoint.Enabled:=False;
      btnAddPoint.Caption:='�������� � ������';
      Redraw(MFbackup);
    end;
  edtX.Text:='';
  edtY.Text:='';
  XSame:=False;
  end;
end;

procedure TeditMFForm.lbPointListClick(Sender: TObject);
var X,Y: Double;
begin
  if lbPointList.ItemIndex<>-1 then
  begin
    btnDelPoint.Enabled:=True;
    btnAddPoint.Caption:='�������� �����';
  end
  else
  begin
    btnDelPoint.Enabled:=False;
  end;
  AnalizeStr(lbPointList.Items.Strings[lbPointList.ItemIndex],X,Y);
  edtX.Text:=FloatToStr(X);
  edtY.Text:=FloatToStr(Y);
  Redraw(MFbackup);
end;

procedure TeditMFForm.btnDelPointClick(Sender: TObject);
var
  numbofpoint: Integer;
  X,Y:Double;
begin
  numbofpoint:=lbPointList.ItemIndex;
  MFbackup.Points.Delete(lbPointList.ItemIndex);
  lbPointList.Items.Delete(lbPointList.ItemIndex);
  lbPointList.ItemIndex:=numbofpoint;
  edtX.Text:='';
  edtY.Text:='';
  Redraw(MFbackup);
  if lbPointList.ItemIndex = -1 then
  begin
    btnDelPoint.Enabled:=False;
    btnAddPoint.Caption:='�������� � ������';
  end
  else
  begin
    AnalizeStr(lbPointList.Items.Strings[lbPointList.ItemIndex],X,Y);
    edtX.Text:=FloatToStr(X);
    edtY.Text:=FloatToStr(Y);
  end;
end;

procedure TeditMFForm.btnCancelClick(Sender: TObject);
begin
  edtNameMF.Text:='';
  edtX.Text:='';
  edtY.Text:='';
  lbPointList.Clear;
  btnAddPoint.Caption:='�������� � ������';
  btnDelPoint.Enabled:=False;
  MFbackup.Free;
  Close;
end;

procedure TeditMFForm.btnOkClick(Sender: TObject);
begin
  if edtNameMF.Text<>'' then
  begin
    MF:=MFbackup;
    if MF.MFName<>edtNameMF.Text then
    begin
      TeditLVForm(editLVFormPtr).lbLVvalue.Items.Add(edtNameMF.Text);
      MF.MFName:=edtNameMF.Text;
//      TeditLVForm(editLVFormPtr).LV.mfList.Add(MF);
    end
    else
    begin
      TeditLVForm(editLVFormPtr).LV.mfList.Delete(TeditLVForm(editLVFormPtr).lbLVvalue.ItemIndex);
      TeditLVForm(editLVFormPtr).LV.mfList.Insert(TeditLVForm(editLVFormPtr).lbLVvalue.ItemIndex, MF);
      TeditLVForm(editLVFormPtr).Draw;
    end;

    edtNameMF.Text:='';
    edtX.Text:='';
    edtY.Text:='';
    btnAddPoint.Caption:='�������� � ������';
    btnDelPoint.Enabled:=False;
    lbPointList.Clear;
    Close;
  end
  else sbError.SimpleText:='������� ��� �������� ����������';

end;

procedure TeditMFForm.AnalizeStr (str : string; var X:Double; var Y:Double);
var
  k,l: Integer;
begin
  for k:=1 to Length(str) do
  begin
    if str[k] = '=' then
    begin
      for l:=k+1 to Length(str) do
      begin
        if str[l] = ';' then
        begin
          X:=StrToFloat(Copy(str,k+1,l-k-1));
          break;
        end;
      end; //for l
      l:=l-1;
      if l=Length(str) then
      begin
        Y:=StrToFloat(Copy(str,k+1,l-k));
        exit;
      end;
    end; // if str[k] = '=';
  end; //for k

end;

procedure TeditMFForm.FormShow(Sender: TObject);
begin
  Error:=False;
  XSame:=False;
  MFbackup:=MF{.CreateCopy};
  MFbackup.MFName:=MF.MFName;
  Redraw(MFbackup);

  if not Assigned(MF) then begin
    MF:=TMembershipFunction(MFCollection.Add);
    Flag:=True;
  end;
end;

procedure TeditMFForm.FormClick(Sender: TObject);
begin
  btnAddPoint.Caption:='�������� � ������';
  lbPointList.ItemIndex:=-1;
  btnDelPoint.Enabled:=False;
  Series2.Clear;
end;

procedure TeditMFForm.Redraw(memfunc : TMembershipFunction);
var
  i : Integer;
begin
  Series1.Clear;
  Series2.Clear;
  for i:=0 to memfunc.Points.Count-1 do
  begin
    Series1.AddXY(TFuzzyPoint(memfunc.Points.Items[i]).X,TFuzzyPoint(memfunc.Points.Items[i]).Y);
  end;
  if lbPointList.ItemIndex<>-1 then
  begin
    Series2.AddXY(TFuzzyPoint(memfunc.Points.Items[lbPointList.ItemIndex]).X,TFuzzyPoint(memfunc.Points.Items[lbPointList.ItemIndex]).Y);
  end;
end;

procedure TeditMFForm.AddToListBox (var X: Double; var Y:Double);
var
  k : Integer;
  Xt, Yt: Double;
  FPtemp: TFuzzyPoint;
begin
  FPtemp:=TFuzzyPoint.Create(nil);
  if lbPointList.Items.Count <> 0 then
  begin
    for k:=1 to lbPointList.Items.Count do
    begin
      AnalizeStr(lbPointList.Items.Strings[k-1],Xt,Yt);
      if X = Xt then
      begin
        sbError.SimpleText:='������ �������� ���������� X ��� ������� � ������';
        XSame:=True;
        FPtemp.Free;
        exit;
      end;
      if X < Xt then
      begin
        lbPointList.Items.Insert(k-1,'X = ' + FloatToStr(X) + '; Y = ' + FloatToStr(Y));
        FPtemp.X:=X;
        FPtemp.Y:=Y;
        MFbackup.Points.Insert(k-1,FPtemp);
        exit;
      end;
    end;
  end
  else
  begin
    lbPointList.Items.Add('X = ' + FloatToStr(X) + '; Y = ' + FloatToStr(Y));
    FPtemp.X:=X;
    FPtemp.Y:=Y;
    MFbackup.Points.Add(FPtemp);
    exit;
  end;
  lbPointList.Items.Add('X = ' + FloatToStr(X) + '; Y = ' + FloatToStr(Y));
  FPtemp.X:=X;
  FPtemp.Y:=Y;
  MFbackup.Points.Add(FPtemp);
end;

procedure TeditMFForm.CheckValues;
begin
  if XSame=False then sbError.SimpleText:='';
  try
  if (edtX.Text<>'')  then
  begin
    try
      StrToFloat(edtX.Text);
    except
      sbError.SimpleText:='������� �� ���������� �������� X';
      Error:=True;
      exit;
    end;
    if (StrToFloat(edtX.Text)<StrToFloat(TeditLVForm(editLVFormPtr).edtLow.Text)) or
       (StrToFloat(edtX.Text)>StrToFloat(TeditLVForm(editLVFormPtr).edtHigh.Text))  then
    begin
      sbError.SimpleText:='�������� X ������ ���� � ��������� ['+TeditLVForm(editLVFormPtr).edtLow.Text+';'+TeditLVForm(editLVFormPtr).edtHigh.Text+']';
      Error:=True;
      exit;
    end;
  end;
  if edtY.Text<>'' then
  begin
    try
      StrToFloat(edtY.Text);
    except
      sbError.SimpleText:='������� �� ���������� �������� Y';
      Error:=True;
      exit;
    end;
    if (StrToFloat(edtY.Text)<0) or
       (StrToFloat(edtY.Text)>1)  then
    begin
      sbError.SimpleText:='�������� Y ������ ���� � ��������� [0;1]';
      Error:=True;
      exit;
    end
  end;
  Error:=False;
  except
  end;

end;

procedure TeditMFForm.edtNameMFChange(Sender: TObject);
begin
  if sbError.SimpleText = '������� ��� �������� ����������' then
    sbError.SimpleText:='';
end;

procedure TeditMFForm.Series1Click(Sender: TChartSeries; ValueIndex: Integer;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i : Integer;
  Xt, Yt : Double;
begin
  for i:=0 to lbPointList.Items.Count-1 do
  begin
    AnalizeStr(lbPointList.Items.Strings[i],Xt,Yt);
    if Xt=Sender.XValues[ValueIndex] then
    begin
      lbPointList.ItemIndex:=i;
      lbPointListClick(TeditLVForm(editLVFormPtr));
      exit;
    end;
  end;
end;


{
procedure TForm2.Chart1DblClick(Sender: TObject);
var Point :  TPoint;
  newX, newY: Double;
  newXstr, newYstr :string;
begin
  Point:=Chart1.GetCursorPos;
  newX:=(Point.X-34)/((Chart1.ChartXCenter-34)/(Chart1.BottomAxis.Maximum/2));
  newY:=(229-Point.Y)/((229-Chart1.ChartYCenter)/(Chart1.LeftAxis.Maximum/2));
  Str(newX:2:3,newXstr);
  Str(newY:2:3,newYstr);
  DotToComma(newXstr);
  DotToComma(newYstr);
  if lbPointList.ItemIndex<>-1 then
  begin
    edtX.Text:=newXstr;
    edtY.Text:=newYstr;
    Form2.btnAddPointClick(Form2);
  end;
end;

procedure TForm2.DotToComma(var str:string);
var
  i: Integer;
begin
  for i:=1 to Length(str) do
  begin
    if str[i]='.' then
      str[i]:=','
  end;
end;
}

procedure TeditMFForm.FormCreate(Sender: TObject);
begin
  Flag:=False;
  MFCollection:=TCollection.Create(TMembershipFunction);
end;

procedure TeditMFForm.FormDestroy(Sender: TObject);
begin
  MFCollection.Destroy;
end;

end.

