unit Calc;

interface

uses
  SysUtils, VCL.Dialogs, Classes, Aspr95,  DmpShf, KBTypes,
  DR_Calculus;
  //FT_Add;

const
  MAX_LINE=1024;

  FireThreshold: Real=0.7;

function Logic(Str: PChar; var X : real): Integer;
function LogicF(Str: PChar; T: Integer): Real; export;
function Then_Else_F(Str1,Str2: PChar;T: Integer;
         n_obj,n_attr: Integer;
         ptr_list_attr_then,
         ptr_list_attr_else: TCollection): Real; export;
function TestAssertion(Ass1,Ass2: TAssertion;
           KB: TKnowledgeBase): Real; export;
procedure United(N2,P2: Real; Attr: record_attr); export;
function CalcStr(S: String): Real; export;

procedure ParseLogicCorr (iValue : string; var Result_x : real; var RestValue : string);

implementation

function Calc_(line: PChar): Real; forward;
function CalcFactor(Factor: TFactor;line: PChar): Real; forward;

function CalcStr(S: String): Real;
var
  P: array[0..256] of Char;
begin
  if S<>'' then begin
    P[0]:=' ';
    if System.Pos('-',TrimLeft(S))>0 then
      StrPCopy(P+1,'0'+S) else
      StrPCopy(P+1,S);
    CalcStr:=Calc_(P);
  end;  
end;

function Logic(Str: PChar; var X : real): Integer;
var
  StrIncident, Str2, Str3: Array[0..MAX_LINE] of Char;
  Str1: Array[0..255] of Char;
  Temp_Str : string;
  Result_x : real;

begin
//  ShowMessage(StrPas(Str));
//  StrIncident[0]:=' ';
  StrCopy(StrIncident,Str);
  while StrPos(StrIncident,'&')<>NIL do begin
    StrCopy(Str1,'&');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident, StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'AND');
    StrCat(StrIncident,Str2);
  end;
  while StrPos(StrIncident,'|')<>NIL do begin
    StrCopy(Str1,'|');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident,
    StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'OR');
    StrCat(StrIncident,Str2);
  end;
  while StrPos(StrIncident,'~')<>NIL do begin
    StrCopy(Str1,'~');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident,
    StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'NOT');
    StrCat(StrIncident,Str2);
  end;
//  ShowMessage(StrPas(StrIncident));
//  ParseLogic (TrimLeft (StrIncident), Result_x, Temp_str);
  ParseLogicCorr(StrIncident, Result_x, Temp_str);
  if Length(Trim(Temp_str))>0 then ShowMessage('�� ���������� ����� ���������: '+
    Temp_str);
  if Result_x=2 then
    Logic:=2
  else
  if Result_x>=FireThreshold then begin
    Logic:=1
//    ShowMessage('True');
  end
  else begin
    Logic:=0
//    ShowMessage('False');
  end;
  X:=Result_x;
end;

function LogicF(Str: PChar; T: Integer): Real;
var
  StrIncident, Str2, Str3: Array[0..MAX_LINE] of Char;
  Str1: Array[0..255] of Char;
  R: Real;
begin
  StrIncident[0]:=' ';
  StrCopy(StrIncident+1,Str);
  while StrPos(StrIncident,'.and.')<>NIL do begin
    StrCopy(Str1,'.and.');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident,
    StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'&');
    StrCat(StrIncident,Str2);
  end;
  while StrPos(StrIncident,'.or.')<>NIL do begin
    StrCopy(Str1,'.or.');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident,
    StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'|');
    StrCat(StrIncident,Str2);
  end;
  while StrPos(StrIncident,'.not.')<>NIL do begin
    StrCopy(Str1,'.not.');
    StrCopy(Str2,StrPos(StrIncident,Str1)+StrLen(Str1));
    StrLCopy(Str3,StrIncident,
    StrLen(StrIncident)-StrLen(Str2)-StrLen(Str1));
    StrCopy(StrIncident,Str3);
    StrCat(StrIncident,'~');
    StrCat(StrIncident,Str2);
  end;
  if T=1 then R:=CalcFactor(Lower ,StrIncident);
  if T=2 then R:=CalcFactor(Height,StrIncident);
  if T>2 then R:=CalcFactor(BajesF,StrIncident);
  if R=2 then
    LogicF:=0
  else
    LogicF:=R;
end;

function Calc_(line: PChar): Real;
label
  e,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,
  m20,m21;
var
  masv :array[1..MAX_LINE] of Real;
  masop:array[1..MAX_LINE] of Char;
  tmpn,i,j,l,r,m,n: Integer;
  z,tmpv: Real;
  g: Boolean;
  Stepen, t: Integer;
begin
{--------------------------���� ������--------------------------------------}

  for i:=1 to MAX_LINE do begin
      masv[i]:=0;
      masop[i]:=' ';
  end;
  n:=StrLen(line);
  m:=1;
  i:=1;
  repeat
    case line[i] of
      '+','-','*','/','(',')',' ','<','>','=','&','|','~': begin
        masop[m]:=line[i];
        Inc(i);
        Inc(m);
      end;
      '0'..'9','E','.',',': begin
        while ((line[i]>='0') and (line[i]<='9')) or
              (line[i]='.') or
              (line[i]=',') or
              (line[i]='E') do begin
          if (line[i]='.') or
             (line[i]=',') then begin
            Inc(i);
            z:=0.1;
            while (line[i]>='0') and (line[i]<='9') do begin
              masv[m]:=masv[m]+(ord(line[i])-ord('0'))*z;
              z:=z*0.1;
              Inc(i);
            end;
          end else
          if (line[i]<>'E') then begin
            masv[m]:=masv[m]*10+ord(line[i])-ord('0');
            Inc(i);
          end else begin
            if line[i+3]='-' then Stepen:=-ord(line[i+4]) else
            if line[i+3]='+' then Stepen:=+ord(line[i+4]) else
            begin
              ShowMessage(StrPas(line)+' << ������������ ���������!') ;
              Exit;
            end;
            for t:=1 to Abs(Stepen) do begin
              if Stepen>0 then masv[m]:=masv[m]*10;
              if Stepen<0 then masv[m]:=masv[m]*0.1;
            end;
            i:=i+5;
          end;
        end;
        Inc(m);
      end
      else begin
       ShowMessage(StrPas(line)+' << ������������ ���������!') ;
       Exit;
       {goto e}
      end;
    end;
  until i>=n;
{---------------------------����������--------------------------------------}
{---------------------------���������� ( * � / )----------------------------}
  g:=False;
  repeat
    for i:=1 to m do
      if (masop[i]=')') then begin
        r:=i;
        masop[i]:=' ';
        for j:=r downto 1 do
          if (masop[j]='(') then begin
            l:=j;
            masop[j]:=' ';
          end;
     g:=False;
     break;
     {goto m1;}
    end else begin
        g:=True;
        l:=1;
        r:=m;
      end;
m1: for i:=l to r do begin
      case masop[i] of
        '*': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m2;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m2:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m3;}
          end;
m3:       masv[tmpn]:=tmpv*masv[j];
          masv[j]:=0;
          masop[i]:=' ';
        end;
        '/': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m4;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m4:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              ShowMessage('Calc: ������� �� 0!') ;
              Exit;
              {goto e;}
            end;
m5:       masv[tmpn]:=tmpv/masv[j];
          masv[j]:=0;
          masop[i]:=' ';
        end;
      end;
    end;
{---------------------------����������( + � - )-----------------------------}
    for i:=l to r do begin
      case masop[i] of
        '+': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m6;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m6:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m7;}
            end;
m7:       masv[tmpn]:=tmpv+masv[j];
          masv[j]:=0;
          masop[i]:=' ';
        end;
        '-': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m8;
          end;
          tmpv:=masv[j];
          tmpn:=j;
m8:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m9;}
            end;
m9:       masv[tmpn]:=tmpv-masv[j];
          masv[j]:=0;
          masop[i]:=' ';
        end;
      end;
    end;
{---------------------------����������( < , > � = )-------------------------}
    for i:=l to r do begin
      case masop[i] of
        '=': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m10;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m10:      j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m11;}
          end;
m11:      if (tmpv=masv[j]) then masv[tmpn]:=1 else masv[tmpn]:=0;
            masv[j]:=0;
            masop[i]:=' ';
        end;
        '<': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m12;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m12:      j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m13;}
            end;
m13:      if (tmpv<masv[j]) then masv[tmpn]:=1 else masv[tmpn]:=0;
          masv[j]:=0;
          masop[i]:=' ';
        end;
        '>': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m14;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m14:      j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m15;}
            end;
m15:      if (tmpv>masv[j]) then masv[tmpn]:=1 else masv[tmpn]:=0;
            masv[j]:=0;
            masop[i]:=' ';
        end;
      end;
    end;
{---------------------------����������( & � | )-----------------------------}
    for i:=l to r do begin
      case masop[i] of
        '&': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m16;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m16:      j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m17;}
          end;
m17:      if (tmpv<>0) and (masv[j]<>0) then masv[tmpn]:=1 else masv[tmpn]:=0;
          masv[j]:=0;
          masop[i]:=' ';
        end;
        '|': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m18;
            end;
            tmpv:=masv[j];
            tmpn:=j;
m18:        j:=i+1;
            while (masv[j]=0) and (masop[j+1]=' ') do
              if (j<(m+1)) then Inc(j) else begin
                masv[j]:=0;
                j:=m;
                break;
                {goto m19;}
              end;
m19:        if (tmpv<>0) or (masv[j]<>0) then masv[tmpn]:=1 else masv[tmpn]:=0;
              masv[j]:=0;
              masop[i]:=' ';
        end;
        '~': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m20;
          end;
          tmpv:=masv[j];
          tmpn:=j;
m20:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m21;}
            end;
        end;
      end;
    end;
  until g;
  i:=1;
  while (masv[i]=0) do
    if (i<m) then Inc(i) else begin
       Calc_:=masv[1];
     goto e;
  end;
  Calc_:=masv[i];
e:
End;

function CalcFactor(Factor: TFactor; line: PChar): Real;
label
  e,m1,m2,m3,m6,m7;
var
  masv :array[1..MAX_LINE] of Real;
  masop:array[1..MAX_LINE] of Char;
  tmpn,i,j,l,r,m,n: Integer;
  z,tmpv: Real;
  g: Boolean;
begin
{--------------------------���� ������--------------------------------------}

  for i:=1 to MAX_LINE do begin
      masv[i]:=0;
      masop[i]:=' ';
  end;
  n:=StrLen(line);
  m:=1;
  i:=1;
  repeat
    case line[i] of
      '(',')',' ','&','|','~': begin
        masop[m]:=line[i];
        Inc(i);
        Inc(m);
      end;
      '0'..'9','.',',': begin
        while ((line[i]>='0') and (line[i]<='9')) or
              (line[i]='.') or
              (line[i]=',')
               do begin
          if (line[i]='.') or
             (line[i]=',') then begin
            Inc(i);
            z:=0.1;
            while (line[i]>='0') and (line[i]<='9') do begin
              masv[m]:=masv[m]+(ord(line[i])-ord('0'))*z;
              z:=z*0.1;
              Inc(i);
            end;
          end else begin
            masv[m]:=masv[m]*10+ord(line[i])-ord('0');
            Inc(i);
          end;
        end;
        Inc(m);
      end
      else begin
       ShowMessage(StrPas(line)+' << ������������ ���������!') ;
       Exit;
       {goto e}
      end;
    end;
  until i>=n;
{---------------------------����������--------------------------------------}
{---------------------------���������� (  &  )----------------------------}
  g:=False;
  repeat
    for i:=1 to m do
      if (masop[i]=')') then begin
        r:=i;
        masop[i]:=' ';
        for j:=r downto 1 do
          if (masop[j]='(') then begin
            l:=j;
            masop[j]:=' ';
          end;
     g:=False;
     break;
     {goto m1;}
    end else begin
        g:=True;
        l:=1;
        r:=m;
      end;
m1: for i:=l to r do begin
      case masop[i] of
        '&': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m2;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m2:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m3;}
          end;
m3:       masv[tmpn]:=DmpShf.CalcFactor(Factor,
                     AndOp,
                     Independent,
                     tmpv, masv[j]);
          masv[j]:=0;
          masop[i]:=' ';
        end;
      end;
    end;
{---------------------------����������( | )-----------------------------}
    for i:=l to r do begin
      case masop[i] of
        '|': begin
          j:=i-1;
          while (masv[j]=0) and (masop[j-1]=' ') do
            if (j>0) then Dec(j) else begin
              tmpv:=0;
              tmpn:=1;
              goto m6;
            end;
          tmpv:=masv[j];
          tmpn:=j;
m6:       j:=i+1;
          while (masv[j]=0) and (masop[j+1]=' ') do
            if (j<(m+1)) then Inc(j) else begin
              masv[j]:=0;
              j:=m;
              break;
              {goto m7;}
            end;
m7:       masv[tmpn]:=DmpShf.CalcFactor(Factor,
                     OrOp,
                     Independent,
                     tmpv, masv[j]);
          masv[j]:=0;
          masop[i]:=' ';
        end;
      end;
    end;
  until g;
  i:=1;
  while (masv[i]=0) do
    if (i<m) then Inc(i) else begin
       CalcFactor:=masv[1];
     goto e;
  end;
  CalcFactor:=masv[i];
e:
end;

function Then_Else_F(Str1,Str2: PChar;T: Integer;
         n_obj,n_attr: Integer;
         ptr_list_attr_then,
         ptr_list_attr_else: TCollection): Real;
var
  S_{Lower},T_{Height},A_{Bajes}: Real;
  N1, P1 : Real;
  N2, P2 : Real;
  i: Integer;
begin
  S_:=0;
  T_:=0;
  A_:=0;
  N1:=0;
  P1:=1;
  N2:=0;
  P2:=1;
  if T<3 then
    S_:=LogicF(Str1,1);
  if T<3 then
    T_:=LogicF(Str2,2);
  if T>2 then
    A_:=LogicF(Str1,3);
  for I:=1 to ptr_list_attr_then.Count do begin
    if (list(ptr_list_attr_then.Items[I-1]).n_obj=n_obj) and
       (list(ptr_list_attr_then.Items[I-1]).n_attr=n_attr) then begin
      N1:=list(ptr_list_attr_then.Items[I-1]).factor1/100;
      P1:=list(ptr_list_attr_then.Items[I-1]).factor2/100;
      break;
    end;
  end;
  for I:=1 to ptr_list_attr_else.Count do begin
    if (list(ptr_list_attr_else.Items[I-1]).n_obj=n_obj) and
       (list(ptr_list_attr_else.Items[I-1]).n_attr=n_attr) then begin
      N2:=list(ptr_list_attr_else.Items[I-1]).factor1/100;
      P2:=list(ptr_list_attr_else.Items[I-1]).factor2/100;
      break;
    end;
  end;
  if T<3 then begin
    if (S_<=T_) and (N1<=P1) and (N2<=P2) then begin
      if T=1 then begin
        if (N1>=N2) then Then_Else_F:=N1*S_+N2*(1-S_)
                    else Then_Else_F:=N1*T_+N2*(1-T_);
      end;
      if T=2 then begin
        if (P1<=P2) then Then_Else_F:=P1*S_+P2*(1-S_)
                    else Then_Else_F:=P1*T_+P2*(1-T_);
      end;
    end else
      Then_Else_F:=0;
  end;
  if T=3 then
    Then_Else_F:=N1*A_/((N1-N2)*A_+N2);
  if T=4 then
    Then_Else_F:=N2*A_/((N2-N1)*A_+N1);
end;

{
����������� �����������:

����� ������ ������� ���� ��� � ������ [n1, p1],
� ������ [n2, p2].
����� ����� ������, ���������� � ������� ������, ����� ���:
 n = (n1 * p2 + n2 * p1 � n1 * n2) / (1 � n1 * (1 � p2) � n2 * (1 � p1)),
 p = p1 * p2 / (1 � n1 * (1 � p2) � n2 * (1 � p1)).
}

procedure United(N2,P2: Real; Attr: record_attr);
var
  N1,P1: Real;
begin
  if Attr.status>=4 then begin
    N1:=TAssertion(Attr.FAssertionCollection.Items[0]).factor1/100;
    P1:=TAssertion(Attr.FAssertionCollection.Items[0]).factor2/100;
    TAssertion(Attr.FAssertionCollection.Items[0]).factor1:=
      Round((N1*P2+N2*P1-N1*N2)/(1-N1*(1-P2)-N2*(1-P1))*100);
    TAssertion(Attr.FAssertionCollection.Items[0]).factor2:=
      Round(P1*P2/(1-N1*(1-P2)-N2*(1-P1))*100);
  end else begin
    TAssertion(Attr.FAssertionCollection.Items[0]).factor1:=Round(N2*100);
    TAssertion(Attr.FAssertionCollection.Items[0]).factor2:=Round(P2*100);
  end;
end;

{     ������� eq, gte, lte �������� ���������� �������������� ������-
 ��� "�����",  "������ ��� �����" � "������ ��� �����". ��� ��������-
 �� ��������� �������:

     eq(X,Y,E)  - ����� X � Y ����� � ��������� �� E;
     gte(X,Y,E) - ����� X ������ ��� ����� Y � ��������� �� E;
     lte(X,Y,E) - ����� X ������ ��� ����� Y � ��������� �� E,
 ��� 0.00 < E < 1.00.

     �������� ������� ����������� �� ��������:

 eq(X,Y,E) = (  1.0, ����   abs(X-Y) < E * max[abs(X),abs(Y)];
                0.0 �����)

 gte(X,Y,E) = ( 1.0, ����   X >= Y ��� 0 < Y-X < E * max[abs(X),abs(Y)];
                0.0 �����)

 lte(X,Y,E) = ( 1.0, ����   Y >= X ��� 0 < X-Y < E * max[abs(X),abs(Y)];
                0.0 �����)
 }

function eq (X,Y,E: Real): Boolean; {=}
begin
  eq:=False;
  if (E=0)  and (X = Y) then eq:=True;
  if (E>0) and (Abs(X-Y)<E*Max(Abs(X),Abs(Y))) then eq:=True;
end;

function gte(X,Y,E: Real): Boolean; {>=}
begin
  gte:=False;
  if (E=0)  and (X >= Y) then gte:=True;
  if (E>0) and ((X>=Y) or (0<(Y-X)) and ((Y-X)<E*Max(Abs(X),Abs(Y))))
    then gte:=True;
end;

function lte(X,Y,E: Real): Boolean; {<=}
begin
  lte:=False;
  if (E=0)  and (Y >= X) then lte:=True;
  if (E>0) and ((Y>=X) or (0<(X-Y)) and ((X-Y)<E*Max(Abs(X),Abs(Y))))
    then lte:=True;
end;

function CompareAssertion(Ass1,Ass2: TAssertion): Boolean;
begin
  CompareAssertion:=False;
  if (Ass1.predicat=0) and (Ass2.predicat=0) then {=}
    CompareAssertion:=eq(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
  if (Ass1.predicat=0) and (Ass2.predicat=5) then {<>}
    CompareAssertion:=Not(eq(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat=0) and (Ass2.predicat=1) then {>}
    CompareAssertion:=Not(lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat=0) and (Ass2.predicat=2) then {<}
    CompareAssertion:=Not(gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat=0) and (Ass2.predicat=3) then {>=}
    CompareAssertion:=gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
  if (Ass1.predicat=0) and (Ass2.predicat=4) then {<=}
    CompareAssertion:=lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
  if (Ass1.predicat=5) and (Ass2.predicat=0) then {=}
    CompareAssertion:=Not(eq(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat=5) and (Ass2.predicat=0) then {>}
    CompareAssertion:=Not(lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat=5) and (Ass2.predicat=0) then {<}
    CompareAssertion:=Not(gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  if (Ass1.predicat>0) and (Ass1.predicat<5) and
     (Ass2.predicat>0) and (Ass2.predicat<5) then begin
    if (Ass1.predicat=Ass2.predicat) then
      CompareAssertion:=eq(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
    if (Ass1.predicat=1) and (Ass2.predicat=1) then {>}
      CompareAssertion:=Not(lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
    if (Ass1.predicat=3) and (Ass2.predicat=3) then {>=}
      CompareAssertion:=gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
    if (Ass1.predicat=1) and (Ass2.predicat=3) then {>}
      CompareAssertion:=Not(lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
    if (Ass1.predicat=2) and (Ass2.predicat=2) then {<}
      CompareAssertion:=Not(gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
    if (Ass1.predicat=4) and (Ass2.predicat=4) then {<=}
      CompareAssertion:=lte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100);
    if (Ass1.predicat=2) and (Ass2.predicat=4) then {<}
      CompareAssertion:=Not(gte(CalcStr(Ass1.znach),CalcStr(Ass2.znach),
                        Ass2.accuracy/100));
  end;

end;

function TestAssertion(Ass1,Ass2: TAssertion;
           KB: TKnowledgeBase): Real; export;
const
  Bound=0.1;
var
  type_znach1, type_znach2: Integer;
  i,j: Integer;
  PtrObj: record_obj;
  PtrAttr: record_attr;
  TestMFAssertionRes: Real;
begin

  type_znach1:=0;
  type_znach2:=0;
  for i:=1 to KB.ListObjs.Count do begin
    PtrObj:=record_obj(KB.ListObjs.Items[i-1]);
    for j:=1 to PtrObj.FListAttrs.Count do begin
      PtrAttr:=record_attr(PtrObj.FListAttrs.Items[j-1]);
      if (Ass1.n_obj=PtrAttr.n_obj) and
         (Ass1.n_attr=PtrAttr.n_attr) then
        type_znach1:=PtrAttr.{F}PtrType.type_znach;
      if (Ass2.n_obj=PtrAttr.n_obj) and
         (Ass2.n_attr=PtrAttr.n_attr) then
        type_znach2:=PtrAttr.{F}PtrType.type_znach;
    end;
  end;
{
  if (type_znach1=3) and
     (type_znach2=3) then begin
    TestMFAssertionRes:=TestMFAssertion(Ass1,Ass2,KB);
    if (TestMFAssertionRes>Bound) then begin
      TestAssertion:=TestMFAssertionRes;
      Exit;
    end;
  end;
}
  case KB.FCalcMethod of
  Bajes:
    {�������� �� �����}
    if {((Ass1.znach[1]>='0') and (Ass1.znach[1]<='9') or
      (Ass1.znach[1]='_') or (Ass1.znach[1]='+') or
      (Ass1.znach[1]='.')) and
      ((Ass2.znach[1]>='0') and (Ass2.znach[1]<='9') or
        (Ass2.znach[1]='_') or (Ass2.znach[1]='+') or
      (Ass2.znach[1]='.'))}
      (type_znach1=1) and
      (type_znach2=1)
       then begin
      {���������� �����}
      if CompareAssertion(Ass1,Ass2) and
        (Ass1.factor1>=Ass2.factor1) and
        (Ass1.accuracy<=Ass2.accuracy)
        then TestAssertion:=1
        else TestAssertion:=0;
    end else begin
      {���������� ������}
      if (CompareStr(Ass1.znach,Ass2.znach)=0) and
        (Ass1.factor1>=Ass2.factor1)
        then TestAssertion:=1
        else TestAssertion:=0;
    end;
  Demster:
    {�������� �� �����}
    if {((Ass1.znach[1]>='0') and (Ass1.znach[1]<='9') or
      (Ass1.znach[1]='_') or (Ass1.znach[1]='+') or
      (Ass1.znach[1]='.')) and
      ((Ass2.znach[1]>='0') and (Ass2.znach[1]<='9') or
        (Ass2.znach[1]='_') or (Ass2.znach[1]='+') or
      (Ass2.znach[1]='.'))}
      (type_znach1=1) and
      (type_znach2=1)
       then begin
      {���������� �����}
      if CompareAssertion(Ass1,Ass2) and
        (Ass1.factor1>=Ass2.factor1) and
        (Ass1.factor2<=Ass2.factor2) and
        (Ass1.accuracy<=Ass2.accuracy)
        then begin
          TestAssertion:=1;
//          ShowMessage('True');
        end else begin
          TestAssertion:=0;
//          ShowMessage('False');
        end;
    end else begin
      {���������� ������}
      if (CompareStr(Ass1.znach,Ass2.znach)=0) and
        (Ass1.factor1>=Ass2.factor1) and
        (Ass1.factor2<=Ass2.factor2)
        then begin
          TestAssertion:=1;
//          ShowMessage('True');
        end else begin
          TestAssertion:=0;
//          ShowMessage('False');
        end;
    end;
  end;

end;

function ParseBounds(InStr: String; var Err: Integer): Real;
var
  S,S1: String;
  i,l,p1,p2,p3: Integer;
  n,r: Integer;
  Res,Res0: Real;
begin
  Err:=0;
  Res:=0;
  S:=TrimRight(InStr);
  l:=Length(S);
  p1:=0;
  p2:=0;
  p3:=0;
  r:=0;
  if S[l]=')' then begin
    n:=1;
    for i:=l-1 downto 1 do begin
      p1:=i;
      if S[i]='(' then Dec(n) else
      if S[i]=')' then Inc(n);
      if n=0 then begin
        p2:=i;
        p3:=l-1;
        break;
      end;
    end;
    if n=0 then begin
      r:=4;
      Res:=ParseBounds(Copy(S,p2+1,p3-(p2+1)+1),Err);
    end else
      Err:=3;
  end else
  if (Ord(S[l])>=Ord('0')) and (Ord(S[l])<=Ord('9')) then begin
    for i:=l downto 1 do begin
      p1:=i;
      S1:=Copy(S,i,l);
      if System.Pos('NOT',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=1;
        break;
      end else
      if System.Pos('AND',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=2;
        break;
      end else
      if System.Pos('OR',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=3;
        break;
      end;
    end;
    if r=0 //���� �����, ���� ������
    then begin
      ParseNumber(Trim(S), Res, S1);
      if S1<>'' then
        Err:=2;
    end;
  end else
    Err:=1;
  if Err=0 then begin
    if ((r=0) or (r=4)) and (p1=1)//����� ��� ������
    then begin
      ParseBounds:=Res;
      Exit;
    end else
    if (r=1) and (p1=1) //NOT �����
    then begin
      ParseNumber(Trim(Copy(S,p2+3,l)), Res, S1);
      if S1='' then begin
        if Res<>2 then
          ParseBounds:=1-Res
        else
          ParseBounds:=Res;
      end else
        Err:=5;
      Exit;
    end else
    if (r=2) and (p1>1) //AND �����
    then begin
      ParseNumber(Trim(Copy(S,p2+3,l)), Res, S1);
      if S1='' then begin
        Res0:=ParseBounds(Copy(S,1,p2-1),Err);
        if (Res0=2) and (Res=0) or (Res0=0) and (Res=2) then begin
          ParseBounds:=0;
        end else
        if (Res0=2) or (Res=2) then begin
          ParseBounds:=2
        end else
          ParseBounds:=Minimum(Res0,Res)
      end else
        Err:=6;
      Exit;
    end else
    if (r=3) and (p1>1) //OR �����
    then begin
      ParseNumber(Trim(Copy(S,p2+2,l)), Res, S1);
      Res0:=ParseBounds(Copy(S,1,p2-1),Err);
      if (Res0=2) or (Res=2) then
        ParseBounds:=Minimum(Res0,Res)
      else
        ParseBounds:=Maximum(Res0,Res);
      Exit;
    end else
    if (r=4) and (p1>0) then begin
      if S1='' then
        ParseBounds:=ParseBounds(Copy(S,1,p2-1)+
          FloatToStr(Res),Err)
      else
        Err:=7;
      Exit;
    end; {else
      Err:=4;}
  end else
    ParseBounds:=Res;
end;

type

  TExprTreeItem=class(TCollectionItem)
  public
    _Not: Boolean;
    Name: String; // Pi
    ChildList: TCollection; // OR List
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
  end;

  constructor TExprTreeItem.Create(Collection: TCollection);
  begin
    inherited Create(Collection);
    ChildList:=TCollection.Create(TExprTreeItem);
  end;

  destructor TExprTreeItem.Destroy;
  begin
    ChildList.Destroy;
    inherited Destroy;
  end;

procedure ParseP(iValue : string; var Res: String; var RestValue : string); // ��������, ��� iValue ���� Pi,
begin
end;
// Res - Pi
// RestValue - ������������� ������� ������, ���� ������

// ����������� ���������, ������� ������ ��������� ���������� ��������� � ���������
// ������ ����:
// (P1 AND P2) OR (P3 AND P4) =>

//1: TopItem.ChildList: Item1,Item2
//2: Item1.ChildList: P2; Item1.Collection: TopItem
//3: Item2.ChildList: P1: Item2.Collection: Item1
//4: Item3.ChildList: P4; Item1.Collection: TopItem
//5: Item4.ChildList: P3: Item2.Collection: Item3

// �.�. ����� ����� ������ ������� �� ������ {Pi}, ����������� ��������� AND

procedure ParseBoundsP(InStr: String; var Err: Integer; Top: TExprTreeItem );
var
  S,S1,Res: String;
  i,l,p1,p2,p3: Integer;
  n,r: Integer;
  ExprTreeItem: TExprTreeItem;
begin
  Err:=0;
  S:=TrimRight(InStr);
  l:=Length(S);
  p1:=0;
  p2:=0;
  p3:=0;
  r:=0;
  if S[l]=')' then begin
    n:=1;
    for i:=l-1 downto 1 do begin
      p1:=i;
      if S[i]='(' then Dec(n) else
      if S[i]=')' then Inc(n);
      if n=0 then begin
        p2:=i;
        p3:=l-1;
        break;
      end;
    end;
    if n=0 then begin
      r:=4;
      ParseBoundsP(Copy(S,p2+1,p3-(p2+1)+1),Err,Top);
    end else
      Err:=3;
  end else
  if (S[l]='P') then begin
    for i:=l downto 1 do begin
      p1:=i;
      S1:=Copy(S,i,l);
      if System.Pos('NOT',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=1;
        break;
      end else
      if System.Pos('AND',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=2;
        break;
      end else
      if System.Pos('OR',S1)=1 then begin
        p2:=i;
        p3:=l;
        r:=3;
        break;
      end;
    end;
    if r=0 //���� Pi, ���� ������
    then begin
      ParseP(Trim(S), Res, S1);
      if S1='' then begin
        ExprTreeItem:=TExprTreeItem(Top.ChildList.Add);
        ExprTreeItem._NOT:=False;
        ExprTreeItem.Name:=Res;
      end else
        Err:=2;
    end;
  end else
    Err:=1;
  if Err=0 then begin
    if ((r=0) or (r=4)) and (p1=1)//Pi ��� ������
    then begin
      Exit;
    end else
    if (r=1) and (p1=1) //NOT Pi
    then begin
      ParseP(Trim(Copy(S,p2+3,l)), Res, S1);
      if S1='' then begin
        ExprTreeItem:=TExprTreeItem(Top.ChildList.Add);
        ExprTreeItem._NOT:=True;
        ExprTreeItem.Name:=Res;
      end else
        Err:=5;
      Exit;
    end else
    if (r=2) and (p1>1) //AND Pi
    then begin
      ParseP(Trim(Copy(S,p2+3,l)), Res, S1);
      if S1='' then begin
        ExprTreeItem:=TExprTreeItem(Top.ChildList.Add);
        ExprTreeItem._NOT:=False;
        ExprTreeItem.Name:=Res;
        ParseBoundsP(Copy(S,1,p2-1),Err,ExprTreeItem);
      end else
        Err:=6;
      Exit;
    end else
    if (r=3) and (p1>1) //OR Pi
    then begin
      ParseP(Trim(Copy(S,p2+2,l)), Res, S1);
      if S='' then begin
        ExprTreeItem:=TExprTreeItem(Top.ChildList.Add);
        ExprTreeItem._NOT:=False;
        ExprTreeItem.Name:=Res;
        ParseBoundsP(Copy(S,1,p2-1),Err,Top);
      end;
      Exit;
    end else
    if (r=4) and (p1>0) then begin
      if S1='' then begin
        ExprTreeItem:=TExprTreeItem(Top.ChildList.Add);
        ExprTreeItem._NOT:=False;
        ExprTreeItem.Name:=Res;
        ParseBoundsP(Copy(S,1,p2-1),Err,ExprTreeItem)
      end else
        Err:=7;
      Exit;
    end; //else
      //Err:=4;
  end;
end;

procedure ParseLogicCorr (iValue : string; var Result_x : real; var RestValue : string);
var
  Err: Integer;
begin
  Result_x:=ParseBounds(TrimLeft(UpperCase(iValue)), Err);
  if Err>0 then begin
    RestValue:=iValue;
    Result_x:=2;
  end else
    RestValue:='';
end;

end.
