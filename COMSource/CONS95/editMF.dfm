object editMFForm: TeditMFForm
  Left = 156
  Top = 108
  BorderStyle = bsSingle
  Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1092#1091#1085#1082#1094#1080#1080' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080
  ClientHeight = 338
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClick = FormClick
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 216
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1092#1091#1085#1082#1094#1080#1080' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 75
    Width = 24
    Height = 13
    Caption = 'X = '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 136
    Top = 75
    Width = 24
    Height = 13
    Caption = 'Y = '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 128
    Width = 81
    Height = 13
    Caption = #1057#1087#1080#1089#1086#1082' '#1090#1086#1095#1077#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 0
    Top = 56
    Width = 241
    Height = 2
  end
  object Bevel2: TBevel
    Left = 240
    Top = 0
    Width = 2
    Height = 289
  end
  object edtNameMF: TEdit
    Left = 8
    Top = 24
    Width = 225
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    OnChange = edtNameMFChange
  end
  object edtX: TEdit
    Left = 32
    Top = 72
    Width = 81
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    OnChange = CheckValues
  end
  object edtY: TEdit
    Left = 160
    Top = 72
    Width = 73
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    OnChange = CheckValues
  end
  object btnAddPoint: TButton
    Left = 64
    Top = 97
    Width = 121
    Height = 17
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1089#1087#1080#1089#1086#1082
    TabOrder = 3
    OnClick = btnAddPointClick
  end
  object btnDelPoint: TButton
    Left = 64
    Top = 272
    Width = 121
    Height = 17
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1089#1087#1080#1089#1082#1072
    Enabled = False
    TabOrder = 4
    OnClick = btnDelPointClick
  end
  object lbPointList: TListBox
    Left = 8
    Top = 144
    Width = 225
    Height = 121
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 5
    OnClick = lbPointListClick
  end
  object btnOk: TButton
    Left = 456
    Top = 296
    Width = 75
    Height = 17
    Caption = 'OK'
    TabOrder = 6
    OnClick = btnOkClick
  end
  object btnCancel: TButton
    Left = 552
    Top = 296
    Width = 75
    Height = 17
    Caption = 'Cancel'
    TabOrder = 7
    OnClick = btnCancelClick
  end
  object Chart1: TChart
    Left = 256
    Top = 8
    Width = 377
    Height = 281
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    LeftWall.Brush.Color = clWhite
    Title.Font.Charset = RUSSIAN_CHARSET
    Title.Font.Color = clBlue
    Title.Font.Height = -11
    Title.Font.Name = 'Times New Roman'
    Title.Font.Style = []
    Title.Text.Strings = (
      #1060#1091#1085#1082#1094#1080#1103' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080)
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Axis.Style = psDot
    BottomAxis.Axis.Width = 1
    BottomAxis.ExactDateTime = False
    BottomAxis.Increment = 1.000000000000000000
    BottomAxis.Maximum = 10.000000000000000000
    BottomAxis.Title.Caption = 'X'
    BottomAxis.Title.Font.Charset = DEFAULT_CHARSET
    BottomAxis.Title.Font.Color = clBlack
    BottomAxis.Title.Font.Height = -16
    BottomAxis.Title.Font.Name = 'Arial'
    BottomAxis.Title.Font.Style = []
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Width = 1
    LeftAxis.ExactDateTime = False
    LeftAxis.Increment = 0.100000000000000000
    LeftAxis.LabelsSize = 18
    LeftAxis.Maximum = 1.000000000000000000
    LeftAxis.MinorTickCount = 2
    LeftAxis.TickLength = 3
    LeftAxis.Title.Angle = 0
    LeftAxis.Title.Caption = 'Y'
    LeftAxis.Title.Font.Charset = DEFAULT_CHARSET
    LeftAxis.Title.Font.Color = clBlack
    LeftAxis.Title.Font.Height = -16
    LeftAxis.Title.Font.Name = 'Arial'
    LeftAxis.Title.Font.Style = []
    LeftAxis.TitleSize = 1
    Legend.LegendStyle = lsValues
    Legend.TextStyle = ltsXValue
    Legend.Visible = False
    RightAxis.Visible = False
    TopAxis.Visible = False
    View3D = False
    BevelOuter = bvNone
    BevelWidth = 0
    BorderStyle = bsSingle
    Color = clSilver
    TabOrder = 8
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Style = smsValue
      Marks.Visible = False
      SeriesColor = clRed
      OnClick = Series1Click
      LinePen.Color = clRed
      LinePen.Width = 2
      Pointer.Brush.Color = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Style = smsValue
      Marks.Visible = False
      SeriesColor = clRed
      LinePen.Color = clRed
      LinePen.Width = 2
      Pointer.Brush.Color = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object sbError: TStatusBar
    Left = 0
    Top = 319
    Width = 640
    Height = 19
    BiDiMode = bdLeftToRight
    Panels = <>
    ParentBiDiMode = False
  end
end
