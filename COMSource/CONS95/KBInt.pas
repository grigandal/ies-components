unit KBInt;

interface

uses
  Classes, SysUtils, Aspr95, Forms, DB, StdCtrls, ExtCtrls,
  Controls, DsgnIntf, Dialogs, KBCompon, KBTypes, Clean,
  UObjInsp, Types, BaseDgrm, Parser, RepTypes, Windows,
  UForms;

type

  TDesignMode=(dmDefault,dmDialog,dmManual);

  TKBFormInterface = class(TKBInterface)
  public
//    PtrObj: record_obj;
//    FForm: TForm;
//    FFormName: String;
    FForm: TDesignerReference;
    FDesignMode: TDesignMode;

//    FKB: TKnowledgeBase;
    procedure SetFormValue(Value: TDesignerReference);
    function GetFormValue: TDesignerReference;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean );override;
    procedure GetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean );override;
    procedure Design(Sender: TObject); override;
    procedure ReplaceValues(Sender: TObject);
  published
//    property Form: TForm read GetFormValue write SetFormValue;
//    property FormName: String read FFormName write FFormName;
    property Form: TDesignerReference read GetFormValue write SetFormValue;
    property DesignMode: TDesignMode read FDesignMode write FDesignMode;
//    property KB: TKnowledgeBase read FKB write FKB;
  end;

{  TKBDBInterface = class(TKBInterface)
  public
    FDataSource: TDataSource;
//    FKB: TKnowledgeBase;
    procedure SetDataSourceValue(Value: TDataSource);
    function GetDataSourceValue: TDataSource;
  published
    property DataSource: TDataSource read GetDataSourceValue write SetDataSourceValue;
//    property KB: TKnowledgeBase read FKB write FKB;
  end;
}
  TKellyFormInterface = class(TKellyInterface)
  public
    FForm: TForm;
    procedure SetFormValue(Value: TForm);
    function GetFormValue: TForm;
    constructor Create(AOwner: TComponent); override;
  published
    property Form: TForm read GetFormValue write SetFormValue;
  end;

  TKBAtDiagramInterface = class(TKBInterface)
  public
    FAtDiagram: TAtDiagram;
  published
    property AtDiagram: TAtDiagram read FAtDiagram write FAtDiagram;
  end;

  TKBProgElemInterface = class(TKBInterface)
  public
    FProgElem: TProgElem;
  published
    property ProgElem: TProgElem read FProgElem write FProgElem;
  end;

  TKBParserInterface = class(TKBInterface)
  public
    FDataToSolve:   TTextProgramm;
    FDataFromSolve: TTextProgramm;
  published
    property DataToSolve:   TTextProgramm read FDataToSolve write FDataToSolve;
    property DataFromSolve: TTextProgramm read FDataFromSolve write FDataFromSolve;
  end;

  TFormPropertyEditor = class(TPropertyEditor)
    protected

    public
      function GetValue: String; override;
      procedure GetValues(Proc: TGetStrProc); override;
      function GetAttributes: TPropertyAttributes; override;
//      procedure Edit; override;
      procedure SetValue(const Value: string); override;
  end;

  TCustomKBDialog = class(TMemo {TCustomPanel} )
  private
    { Private declarations }
  public
//    function Run(Parms:array of Variant):Variant;
  end;

  TKBDialog = class(TCustomKBDialog)
  public
    Stop: Boolean;
//    FDialog: TPersistent;{TMemo}
    FOpenDialog: TPersistent; {TOpenDialog}
    FSaveDialog: TPersistent; {TSaveDialog}
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Save;
    procedure Open;
    procedure KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
  published
//    property Dialog: TPersistent read FDialog write FDialog;
  end;

  TKBConfigFormInterfaces = class(TKBTool)
  public
    function Run( Sender: TObject): TObject; override;
  end;

  procedure Register;


implementation
uses KBIntEd;
{$G+}
procedure Register;
begin
  RegisterComponents('KB Interface', [TKBFormInterface,
    {TKBDBInterface,} TKellyFormInterface,
    TKBAtDiagramInterface, TKBProgElemInterface, TKBParserInterface,
    TKBConfigFormInterfaces,
    TKBDialog]);
  RegisterClasses([TCustomKBDialog,TKBDialog]);
  RegisterPropertyEditor(TypeInfo(TForm),
         TKellyFormInterface,
         'Form',
         TFormPropertyEditor);
end;

  procedure TKBFormInterface.Design(Sender: TObject);
  var
    PtrLabel: TLabel; {�����������}
    PtrComboBox: TComboBox;{��������}
    PtrEdit: TEdit; {����������� � ��������}
    CustomKBDialog: TCustomKBDialog{TCustomPanel};
    CurAttr: record_attr;
    OkButton: TButton;
    i,j,m,MaxLen,MaxLen1,h,l,k: Integer;
    Comp: TComponent;
    KBFormInterfaceDesign: TKBFormInterfaceDesign;
    ExitFlag: Boolean;
    D:  TAtFrmDesigner;
    Mr: TModalResult;
  begin
   if not Assigned(Form) then Exit;
   if not Assigned(Form.Designer) then Exit;

   case DesignMode of
   dmDefault:begin

       if Sender is TKnowledgeBase then Exit;

//     if PtrType=NIL then begin
        Comp:=TComponent(TKBOwnedCollection(record_obj(Sender).Collection).Owner);
        if Comp is TKnowledgeBase then begin
          TKnowledgeBase(Comp).SetPointers;
        end else begin
          ShowMessage('������ ��������� �� ���');
        end;
//     end;

      with record_obj(Sender) do begin
        if ListAttrs.Count<=0 then Exit;
        with Form.Designer do begin
          {Set Data}
          Caption:=Comment;
          Top:=0;
          Left:=0;
          Width:=Screen{.ActiveForm}.Width;
          Height:=Screen{.ActiveForm}.Height;

          {Insert Controls}
          m:=0;
          MaxLen1:=0;
          h:=1;
          for i:=1 to ListAttrs.Count do begin
            CurAttr:=record_attr(ListAttrs.Items[i-1]);
            CurAttr.OnKBInterface:=FOnKBInterface;
            Inc(m);
            PtrLabel:=TLabel.Create(Form.Designer);
            InsertControl(PtrLabel);
            with PtrLabel do begin
              Font.Name:=FontName;
              Font.Size:=9;
//              Caption:=CurAttr.PtrType.Comment;
              if CurAttr.Comment='' then
                Caption:='?' else
                Caption:=CurAttr.Comment;
              h:=Height+15;
              Top:=m*h;
              Left:=20;
              l:=ClientWidth+Left;
            end;
            PtrComboBox:=TComboBox.Create(Form.Designer);
            InsertControl(PtrComboBox);
            with PtrComboBox do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='ComboBox_'+IntToStr(i);
              CurAttr.Control:=Name;
              Text:=TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach;
              Top:=m*h;
              Left:=l+20;
              MaxLen:=1;
              for k:=1 to CurAttr.{F}PtrType.list_znach.Count do begin
                Items.Add(value(CurAttr.{F}PtrType.list_znach.Items[k-1]).Fznach);
                MaxLen:=Max(MaxLen,Length(value(CurAttr.{F}PtrType.list_znach.Items[k-1]).Fznach));
              end;
              if CurAttr.{F}PtrType.type_znach<>1
              then Width:=Round(Font.Size*MaxLen)+40
              else Width:=50;
            end;
            PtrLabel:=TLabel.Create(Form.Designer);
            InsertControl(PtrLabel);
            with PtrLabel do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Caption:='����.';
              Top:=m*h;
              Left:=PtrComboBox.Left+PtrComboBox.Width+20;
            end;
            PtrEdit:=TEdit.Create(Form.Designer);
            InsertControl(PtrEdit);
            with PtrEdit do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='EditF1_'+IntToStr(i);
              CurAttr.Factor1Control:=Name;
              Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).factor1);
              Top:=m*h;
              Left:=PtrLabel.Left+40;
              Width:=50;
            end;
            PtrEdit:=TEdit.Create(Form.Designer);
            InsertControl(PtrEdit);
            with PtrEdit do begin
              Font.Name:=FontName;
              Font.Size:=9;
              Name:='EditF2_'+IntToStr(i);
              CurAttr.Factor2Control:=Name;
              Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).factor2);
              Top:=m*h;
              Left:=PtrLabel.Left+90;
              Width:=50;
              l:=Left+Width;
            end;
            if CurAttr.{F}PtrType.type_znach=1 then begin
              PtrLabel:=TLabel.Create(Form.Designer);
              InsertControl(PtrLabel);
              with PtrLabel do begin
                Font.Name:=FontName;
                Font.Size:=9;
                Caption:='����.';
                Top:=m*h;
                Left:=PtrEdit.Left+70;
              end;
              PtrEdit:=TEdit.Create(Form.Designer);
              InsertControl(PtrEdit);
              with PtrEdit do begin
                Font.Name:=FontName;
                Font.Size:=9;
                Name:='EditT_'+IntToStr(i);
                CurAttr.AccuracyControl:=Name;
                Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).accuracy);
                Top:=m*h;
                Left:=PtrLabel.Left+50;
                Width:=50;
                l:=Left+Width;
              end;
            end;
            MaxLen1:=Max(MaxLen1,l);
          end;

          Width:=MaxLen1+20;

          if TForm(Form.Designer) is TTestForm   then begin
            OKButton:=TButton.Create(Form.Designer);
            with OKButton do begin
              Left := Round((MaxLen*9+l+40)/2);
              Top:=(m+2)*h;
              Width := 100;
              Height := 25;
              Font.Name:=FontName;
              Font.Size:=9;
              Caption := '����������';
              TabOrder := 1;
              OnClick:=TTestForm(Form.Designer).OKButtonClick;
            end;
            InsertControl(OKButton);
            Height:=Min(Screen.Height,OKButton.Top+2*OKButton.Height+40);
            OKButton.Left := Round((Width-OKButton.Width)/2);
          end else
            Form.Designer.Height:=(m+1)*h;
        end;
     end;
    end;
   dmDialog: begin

     if not (Sender is TKnowledgeBase) then begin

       Comp:=TComponent(TKBOwnedCollection(record_obj(Sender).Collection).Owner);
       if Comp is TKnowledgeBase then begin
         TKnowledgeBase(Comp).SetPointers;
       end else begin
         ShowMessage('������ ��������� �� ���');
       end;
     end;

     with Form.Designer do begin
       CustomKBDialog:=TKBDialog.Create(Form.Designer);
       InsertControl(CustomKBDialog);
//       CustomKBDialog:=TPanel.Create(Form.Designer);
//       InsertControl(CustomKBDialog);
     end;
   end;
   dmManual: begin
     ExitFlag:=False;
     with Screen do begin
       for i:=1 to FormCount do begin
            with Forms[i-1] do begin
              for j:=1 to ComponentCount do begin
                if Components[j-1] is TKnowledgeBase then begin
                  if TKnowledgeBase(Components[j-1]).KBInterface=Self then begin
                    Application.CreateForm(TKBFormInterfaceDesign,
                      KBFormInterfaceDesign);
                    KBIntEd.SaveContextObj:=AtKernel.Context.Obj;
                    KBIntEd.KB:=TKnowledgeBase(Components[j-1]);
                    KBIntEd.KBFormInterface:=Self;
                    Mr:=KBFormInterfaceDesign.ShowModal;
                    if Mr=50 then begin
                      D:= TAtFrmDesigner.Create(Application);
                      AtKernel.Context.Obj:=KBIntEd.SaveContextObj;
                      with TatForm(AtKernel.Context.Obj) do begin
                        Edit;
                      end;
                      KBIntEd.CurrentForm:=D;
                      KBIntEd.KBFormInterface.Form.Designer:=D;
                      //KBFormInterfaceDesign.FindForms;
                    end;
                    KBFormInterfaceDesign.Free;
                    ExitFlag:=True;
                    break;
                  end;
                end;
              end;
            end;
         if ExitFlag then break;
       end;
     end;
   end;
   end; {Case}
  end;

  function FindValue(var Value: String; CurAttr: record_attr; Memo: TCustomMemo): Boolean;
  var
    I,J,P: Integer;
    F: Boolean;
  begin
    F:=False;
    Value:='';
//    ShowMessage('FindBegin');
    with Memo.Lines do begin
      for I:=1 to Count do begin
        P:=System.Pos(CurAttr.PtrType.Comment,Strings[I-1]);
        if P<>0 then begin
          Value:=TrimLeft(Copy(Strings[I-1],P+
            Length(CurAttr.PtrType.Comment),Length(Strings[I-1])));
          F:=True;
          break;
        end;
        for J:=1 to CurAttr.PtrType.list_znach.Count do begin
          P:=System.Pos(Aspr95.value(CurAttr.PtrType.Flist_znach.Items[J-1]).znach,
          Strings[I-1]);
          if P<>0 then begin
            Value:=Aspr95.value(CurAttr.PtrType.Flist_znach.Items[J-1]).znach;
            F:=True;
            break;
          end;
        end;
      end;
    end;
//    ShowMessage('FindEnd');
    FindValue:=F;
  end;

  function GetControlValue(Control: TComponent; CurAttr: record_attr;
    var Res: String): Boolean;
  var
    i: Integer;
  begin
    GetControlValue:=False;
    Res:='';
    if Assigned(Control) then begin
      if Control is TComboBox then begin
        Res:=TComboBox(Control).Text;
        GetControlValue:=True;
      end else
      if Control is TEdit then begin
        Res:=TEdit(Control).Text;
        GetControlValue:=True;
      end else
      if Control is TRadioGroup then begin
        for i:=1 to TRadioGroup(Control).Items.Count do begin
          if TRadioGroup(Control).ItemIndex=i-1 then begin
            Res:=TRadioGroup(Control).Items[TRadioGroup(Control).ItemIndex];
            GetControlValue:=True;
            break;
          end;
        end;
      end else
      if Control is TCheckBox then begin
        if TCheckBox(Control).Checked then begin
          if Assigned(CurAttr.PtrType) then begin
            if CurAttr.PtrType.list_znach.Count>0 then begin
              Res:=Value(CurAttr.PtrType.list_znach.Items[0]).Fznach;
              GetControlValue:=True;
            end;
          end;
        end;
      end;
    end;
  end;

  function SetControlValue(Control: TComponent; CurAttr: record_attr;
    V: String): Boolean;
  var
    i: Integer;
    CurType: record_type;
  begin
    SetControlValue:=False;
    if Assigned(Control) then begin
      if Control is TComboBox then begin
        TComboBox(Control).Text:=V;
        SetControlValue:=True;
      end else
      if Control is TEdit then begin
        TEdit(Control).Text:=V;
        SetControlValue:=True;
      end else
      if Control is TRadioGroup then begin
        for i:=1 to TRadioGroup(Control).Items.Count do begin
          if TRadioGroup(Control).Items[i-1]=V then begin
            TRadioGroup(Control).ItemIndex:=i-1;
            SetControlValue:=True;
            break;
          end;
        end;
      end else
      if Control is TCheckBox then begin
        TCheckBox(Control).Checked:=False;
        if CurAttr.Status>2 then begin
          if Assigned(CurAttr.PtrType) then begin
            if CurAttr.PtrType.list_znach.Count>0 then begin
              if Value(CurAttr.PtrType.list_znach.Items[0]).Fznach=V then begin
                TCheckBox(Control).Checked:=True;
                SetControlValue:=True;
              end;
            end;
          end else begin
            TCheckBox(Control).Checked:=True;
            SetControlValue:=True;
          end;
        end;
      end;
    end;
  end;

  procedure TKBFormInterface.ReplaceValues(Sender: TObject);
  var
    CurAttr: record_attr;
    i,j,n,k: Integer;
    KB: TKnowledgeBase;
    Value: String;
    CurForm: TForm;
  begin
//    ShowMessage('Interf');
    KB:=NIL;
    with record_obj(Sender) do begin
      if TKBOwnedCollection(Collection).Owner is TKnowledgeBase then
        KB:=TKnowledgeBase(TKBOwnedCollection(Collection).Owner);
     CurForm:=Form.Designer;
     with CurForm do begin
     {Replace Values}
      for j:=1 to ComponentCount do begin
//        if Components[j-1] is TComboBox then ShowMessage(TComboBox(Components[j-1]).Text);
        for i:=1 to ListAttrs.Count do begin
          CurAttr:=record_attr(ListAttrs.Items[i-1]);
//          ShowMessage(IntToStr(CurAttr.n_attr)+' '+CurAttr.Control+
//          '->'+Component[j-1].Name);
          if (Components[j-1].Name=CurAttr.Control) and
             (CurAttr.Control<>'') and
             (CurAttr.Status<=2) then begin
             if (TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach=TComboBox(Components[j-1]).Text) then
               CurAttr.Status:=1 else begin
//               ShowMessage(TComboBox(Component[j-1]).Text);
               if GetControlValue(Components[j-1], CurAttr,
                    TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach) then begin
                 for k:=1 to ComponentCount do begin
                   if (Components[k-1].Name=CurAttr.Factor1Control) and (CurAttr.Factor1Control<>'') then begin
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor1:=StrToInt(TEdit(Components[k-1]).Text);
                   end;
                   if (Components[k-1].Name=CurAttr.Factor2Control) and (CurAttr.Factor2Control<>'')then begin
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor2:=StrToInt(TEdit(Components[k-1]).Text);
                   end;
                   if (Components[k-1].Name=CurAttr.AccuracyControl)and (CurAttr.AccuracyControl<>'') then begin
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Faccuracy:=StrToInt(TEdit(Components[k-1]).Text);
                   end;
                 end;
                 CurAttr.Status:=6;
               end;
             end;
          end;
        end;
        if UpperCase(Components[j-1].ClassName)=UpperCase('TKBDialog') then begin
//          with TKBDialog(Component[j-1]) do begin
//            for k:=1 to {TMemo(Dialog).} Lines.Count do begin
              for n:=1 to KB.ListObjs.Count do begin
                for i:=1 to record_obj(KB.ListObjs.Items[n-1]).ListAttrs.Count do begin
                  CurAttr:=record_attr(record_obj(KB.ListObjs.Items[n-1]).
                    ListAttrs.Items[i-1]);
                  if FindValue(Value,CurAttr,TKBDialog(Components[j-1]){TMemo(Dialog)} ) and
                    (CurAttr.Status<=2) then begin
                    if (TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach=TComboBox(Components[j-1]).Text) then
                     CurAttr.Status:=1 else begin
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach:=Value;
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor1:=100;
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor1:=100;
                     TAssertion(CurAttr.AssertionCollection.Items[0]).Faccuracy:=0;
                     CurAttr.Status:=6;
                    end;
                  end;
                end;
//              end;
              if TKBDialog(Components[j-1]).Stop then begin
                TKBDialog(Components[j-1]).Stop:=False;
                Self.Stop:=True;
              end;
            end;
//          end;
        end;
      end;
     end;
    end;
  end;

  procedure TKBFormInterface.SetFormValue(Value:  TDesignerReference);
  begin
    FForm:=Value;
//    if Assigned(FForm) then
//      if Assigned(FForm.Designer) then
//        FFormName:=FForm.Designer.Name;
  end;

  function TKBFormInterface.GetFormValue:  TDesignerReference;
//  var
//    FormName1: String;
//    CurForm: TForm;
//    i: Integer;
  begin
{    FormName1:='';
    if Assigned(FForm) then
      if Assigned(FForm.Designer) then
        FormName1:=FForm.Designer.Name;
    if FFormName<>FormName1 then begin
      if FFormName<>'' then begin
        for i:=1 to Screen.FormCount do begin
          CurForm:=Screen.Forms[i-1];
          if FFormName=CurForm.Name then begin
            if CurForm is TAtForm then
              FForm.Designer:=TAtForm(CurForm);
          end;
        end;
      end else
        FFormName:=FormName1;
    end;}
    GetFormValue:=FForm;
  end;

  constructor TKBFormInterface.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    Form:= TDesignerReference.Create(TATForm);
    Form.Designer:=NIL;
    DesignMode:=dmDefault;
//    RegistryTestableProperty('FormName');
  end;

  destructor TKBFormInterface.Destroy;
  begin
    Form.Destroy;
    inherited Destroy;
  end;

  procedure TKBFormInterface.SetAttrValue(Source: TObject; var AValue: Variant;
    var Success: Boolean);
  begin
   if {False}Form.Designer is TAtForm then begin
    GetAttrValue(Source,AValue,Success);
   {Replace Values}
   ReplaceValues(record_attr(Source).PtrObj);
    {with record_attr(Source).PtrObj do begin
    with Form do begin
      for j:=1 to ControlCount do begin
        if Controls[j-1] is TComboBox then
        for i:=1 to ListAttrs.Count do begin
          CurAttr:=record_attr(ListAttrs.Items[i-1]);
          if (TComboBox(Controls[j-1]).Name=CurAttr.Control) and
             (CurAttr.Status<=2) then begin
             if (CurAttr.Assertion.Fznach=TComboBox(Controls[j-1]).Text) then
               CurAttr.Status:=1 else begin
               CurAttr.Assertion.Fznach:=TComboBox(Controls[j-1]).Text;
               CurAttr.Assertion.factor1:=100;
               CurAttr.Assertion.factor1:=100;
               CurAttr.Assertion.accuracy:=0;
               CurAttr.Status:=6;
             end;
          end;
        end;
      end;
    end;
    end;}
   end else begin
     inherited SetAttrValue(Source, AValue, Success);
   end;
  end;

  procedure TKBFormInterface.GetAttrValue(Source: TObject; var AValue: Variant;
    var Success: Boolean);
  var
    i,j,n,k: Integer;
    CurAttr: record_attr;
    KB: TKnowledgeBase;
    Value: String;
    CurForm: TAtForm;
  begin
    KB:=NIL;
    if {False}TKBFormInterface(Self).Form.Designer is {TAtFrmDesigner}TAtForm then begin

   {View Values}
    with record_attr(Source).PtrObj do begin
      if TKBOwnedCollection(Collection).Owner is TKnowledgeBase then
        KB:=TKnowledgeBase(TKBOwnedCollection(Collection).Owner);
      CurForm:=Form.Designer as TAtForm;
      with CurForm do begin
        for i:=1 to ListAttrs.Count do begin
          CurAttr:=record_attr(ListAttrs.Items[i-1]);
          for j:=1 to ComponentCount do begin
//            if Components[j-1] is TComboBox then begin
            if (TComboBox(Components[j-1]).Name=CurAttr.Control) and
                 (CurAttr.Control<>'') then begin
              if SetControlValue(Components[j-1], CurAttr,
                   TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach) then begin
//                 TComboBox(Components[j-1]).Text:=TAssertion(CurAttr.AssertionCollection..Items[0]).Fznach;
                for k:=1 to ComponentCount do begin
                  if (Components[k-1].Name=CurAttr.Factor1Control) and (CurAttr.Factor1Control<>'') then begin
                    TEdit(Components[k-1]).Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor1);
                  end;
                  if (Components[k-1].Name=CurAttr.Factor2Control) and (CurAttr.Factor2Control<>'')then begin
                    TEdit(Components[k-1]).Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).Ffactor2);
                  end;
                  if (Components[k-1].Name=CurAttr.AccuracyControl)and (CurAttr.AccuracyControl<>'') then begin
                    TEdit(Components[k-1]).Text:=IntToStr(TAssertion(CurAttr.AssertionCollection.Items[0]).Faccuracy);
                  end;
                end;
              end;
            end;
//            end;
          end;
        end;

        for j:=1 to ComponentCount do begin
          if Components[j-1] is TKBDialog then begin
            for n:=1 to KB.ListObjs.Count do begin
              for i:=1 to record_obj(KB.ListObjs.Items[n-1]).ListAttrs.Count do begin
                CurAttr:=record_attr(record_obj(KB.ListObjs.Items[n-1]).
                ListAttrs.Items[i-1]);
                if not FindValue(Value,CurAttr,TKBDialog(Components[j-1])) and
                  (CurAttr.Status>2) then begin
                  TKBDialog(Components[j-1]).Lines.
                  Add(CurAttr.PtrType.Comment+': '+
                  TAssertion(CurAttr.AssertionCollection.Items[0]).Fznach);
                end;
              end;
            end;
            if TKBDialog(Components[j-1]).Stop then begin
              TKBDialog(Components[j-1]).Stop:=False;
              Self.Stop:=True;
            end;
            break;
          end;
        end;
      end;
//      ShowMessage('??');
      try
        frmEditor.FormEditors.Active := False;
        frmEditor.FormEditors.DesignedComponent:= nil;
        {frmEditor.FormEditors.DesignedComponent:= CurForm;
        Include(AtKernel.State, stRunning);
        Exclude(AtKernel.State, stEditing);}
        CurForm.ShowModal;
      finally
{        Exclude(AtKernel.State, stRunning);
        Include(AtKernel.State, stEditing);}
      end;
//      ShowMessage('???');
    end;
   end else begin
     inherited GetAttrValue(Source, AValue, Success);
   end;
  end;

  {procedure TKBDBInterface.SetDataSourceValue(Value: TDataSource);
  begin
    FDataSource:=Value;
  end;

  function TKBDBInterface.GetDataSourceValue: TDataSource;
  begin
    GetDataSourceValue:=FDataSource;
  end;
  }
  function TFormPropertyEditor.GetValue: String;
  var
    Value: TObject;
  begin
    Value:=VarToObj(GetOrdValue);
    if Value<>NIL then
      Result:=TForm(GetOrdValue).Name
    else
      Result:='';
//    Result:=GetStrValue;
  end;

  procedure TFormPropertyEditor.GetValues(Proc: TGetStrProc);
  var
    i:Integer;
    loInterface: TKBFormInterface;
//    loInterfaceClass: TClass;
  begin
    loInterface:=TKBFormInterface(Self.GetComponent(0));
//    loInterfaceClass:= loInterface.InterfaceClass;
    with Screen do
      For i:=0 To FormCount-1 do
//         if Forms[i].InheritsFrom(loInterfaceClass) then
           Proc(Forms[i].Name);
  end;

  function TFormPropertyEditor.GetAttributes: TPropertyAttributes;
  begin
    GetAttributes:=[paValueList];
  end;

//  procedure TFormPropertyEditor.Edit;
//  begin
//  end;

  procedure TFormPropertyEditor.SetValue(const Value: String);
  var
    loForm: TForm;
    i: Integer;
    loInterface: TKBFormInterface;
  begin
    loInterface:=TKBFormInterface(Self.GetComponent(0));
    if Value='' then begin
      loInterface.InterfaceClass:=NIL;
      loForm:=nil;
    end else begin
      with Screen do
      for i:=0 to FormCount-1 do
        if CompareText(Forms[i].Name, Value)=0 then begin
          loForm:= Forms[i];
          Break;
        end;
    if (not Assigned(loForm)) and (Value<>'') then
      ShowMessage( Value+' �� �������� ������!' )
    else begin
//      SetStrValue(Value);
      loInterface.InterfaceClass:=GetClass(loForm.ClassName);
      SetOrdValue( LongInt(loForm) );
    end;
  end;
end;

  procedure TKellyFormInterface.SetFormValue(Value: TForm);
  begin
    FForm:=Value;
  end;

  function TKellyFormInterface.GetFormValue: TForm;
  begin
    GetFormValue:=FForm;
  end;

  constructor TKellyFormInterface.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    Form:=TForm(AOwner);
  end;


  procedure TKBDialog.KeyDown (Sender: TObject; var Key: Word; Shift: TShiftState);
  begin
    if (Key=VK_ESCAPE) and
       ((ssShift in Shift) or
        (ssAlt   in Shift) or
        (ssLeft  in Shift)) then begin
      Stop:=True;
      if Owner is TForm then TForm(Owner).Close;
    end;
  end;

  constructor TKBDialog.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);                  
      Stop:=False;
//    FDialog:=TPersistent(TMemo.Create(Self));
//    FOpenDialog:=TPersistent(TOpenDialog.Create(Self));
//    FSaveDialog:=TPersistent(TSaveDialog.Create(Self));
//    Self.InsertControl(TMemo(FDialog));
//    with TMemo(FDialog) do begin
//      Name:=Self.Name+'�����';
      Align:=alClient;
      ScrollBars:=ssVertical;
      OnKeyDown:=KeyDown;
//    end;
   end;

  destructor TKBDialog.Destroy;
  begin
//    RemoveControl(TMemo(FDialog));
//    TMemo(FDialog).Destroy;
//    TOpenDialog(FOpenDialog).Destroy;
//    TSaveDialog(FSaveDialog).Destroy;
    inherited Destroy;
  end;

  procedure TKBDialog.Save;
  begin
    TSaveDialog(FSaveDialog).Execute;
  end;

  procedure TKBDialog.Open;
  begin
    TOpenDialog(FOpenDialog).Execute;
  end;

//  function TCustomKBDialog.Run(Parms:array of Variant):Variant;
//  begin
//  end;

  function TKBConfigFormInterfaces.Run( Sender: TObject): TObject;
  var
    i,j: Integer;
    KBFormInterface: TKBFormInterface;
    SaveDesignMode: TDesignMode;
  begin
   KBFormInterface:=NIL;
   with Screen do begin
      for i:=1 to FormCount do begin
            with Forms[i-1] do begin
              for j:=1 to ComponentCount do begin
                if Components[j-1] is TKBFormInterface then begin
                  KBFormInterface:=TKBFormInterface(Components[j-1]);
                  SaveDesignMode:=KBFormInterface.DesignMode;
                  KBFormInterface.DesignMode:=dmManual;
                  KBFormInterface.Design(Sender);
                  KBFormInterface.DesignMode:=SaveDesignMode;
                end;
              end;
            end;
      end;
    end;
    if not Assigned(KBFormInterface) then
      ShowMessage('��������� ���������������� ���������� �� ��������');
  end;

end.


