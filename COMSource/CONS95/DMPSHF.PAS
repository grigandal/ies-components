unit DmpShf;

Interface

type
  TFactor=(Lower,Height,BajesF);
  TOperation=(NotOp,AndOp,OrOp);
  TFacts=(Independent,       {����� ����������}
          NotKnownDependens, {��� ���������� � ����������� ������}
          Exclusive,         {����� ������� ��������� ���� �����}
          Following,         {���� ���� ��������� ������ ����}
          Bajes_);           {������ �� ������� ������}

function CalcFactor(Factor: TFactor;
                     Operation: TOperation;
                     Facts: TFacts;
                     N_P1, N_P2: Real): Real;        export;

function Max (X, Y : Real) : Real;                   export;
function Min (X, Y : Real) : Real;                   export;
function ds_Not (Factor: TFactor; N_P : Real): Real; export;
function ds_Or (Factor: TFactor; Facts: TFacts;
                           N_P1, N_P2 : Real): Real; export;
function ds_And (Factor: TFactor; Facts: TFacts;
                           N_P1, N_P2 : Real): Real; export;

Implementation

function CalcFactor(Factor: TFactor;
                     Operation: TOperation;
                     Facts: TFacts;
                     N_P1, N_P2: Real): Real;
begin
  if Factor=BajesF then Facts:=Bajes_;
  case Operation of
    NotOp: CalcFactor:=ds_Not (Factor, N_P1);
    AndOp: CalcFactor:=ds_And (Factor, Facts, N_P1, N_P2);
    OrOp : CalcFactor:=ds_Or  (Factor, Facts, N_P1, N_P2);
  end;
end;

function ds_Not (Factor: TFactor; N_P : Real): Real;
begin
  ds_Not:=1-N_P;
end;

function ds_Or (Factor: TFactor; Facts: TFacts;
                                 N_P1, N_P2 : Real): Real;
begin
  case Facts of
    Independent,Bajes_: begin
         ds_Or:=N_P1*N_P2;
       end;
    NotKnownDependens: begin
         case Factor of
           Lower : ds_Or:=Max (0, N_P1+N_P2-1);
           Height: ds_Or:=Min (1, N_P1+N_P2);
         end;
       end;
    Exclusive: begin
         ds_Or:=0;
       end;
    Following: begin
         ds_Or:=Min (N_P1, N_P2);
       end;
  end;
end;

function ds_And (Factor: TFactor; Facts: TFacts;
                                  N_P1, N_P2 : Real): Real;
begin
  case Facts of
    Independent,Bajes_: begin
         ds_And:=N_P1+N_P2-N_P1*N_P2;
       end;
    NotKnownDependens: begin
         case Factor of
           Lower : ds_And:=Max (N_P1, N_P2);
           Height: ds_And:=Min (1, N_P1+N_P2);
         end;
       end;
    Exclusive: begin
         ds_And:=N_P1+N_P2;
       end;
    Following: begin
         ds_And:=Max(N_P1, N_P2);
       end;
  end;
end;

function Max (X, Y : Real) : Real;
begin
  if (X>=Y) then Max:=X
            else Max:=Y;
end;

function Min (X, Y : Real) : Real;
begin
  if (X<=Y) then Min:=X
            else Min:=Y;
end;

end.

