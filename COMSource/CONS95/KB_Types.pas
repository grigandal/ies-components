unit KB_Types;

 {******************************************************************************
 * ������ KB_Types.pas �������� ����������� ����������� ������ TKBType:
 *
 * TNumericType - ��������� ��� ��������� ����
 * TSymbolicType - ��������� ��� ����������� ����
 * TFuzzyType - ��������� ��� ��������� ����
 * TReferenceType - ��������� ��� ����-������
 * TKBClass - ��������� ��� ������ ���� ������, �������� ��� IDispatch
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, ComObj, SysUtils,
  KB_Containers, KB_Common;

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TNumericType �������� ����������� ��������� ����
  ////////////////////////////////////////////////////////////////////////////

  TNumericType = class(TKBType) // ������������� ���� DTD 'type', ��� 'meta' = 'number'
  public
    MinValue: double;           // KB, ����������� �������� ���������� ������� ���� (���� ����� MaxValue, �� ����������� ���)
    MaxValue: double;           // KB, ������������ �������� ���������� ������� ����
    IsInteger: boolean;         // True, ���� ���������� ����� ���� �.�. ������
    constructor Create(AOwner: TComponent); override;
    procedure XML(Parent: IXMLNode); override;
    function Valid(Value: TAbstractValue): boolean; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TSymbolicType �������� ����������� ����������� ����
  ////////////////////////////////////////////////////////////////////////////

  TSymbolicType = class(TKBType)// ������������� ���� DTD 'type', ��� 'meta' = 'string'
  public
    Values: TStringList;        // KB, ���������� �������� ���������� ����� ����
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TFuzzyType �������� ����������� ��������� ����
  ////////////////////////////////////////////////////////////////////////////

  TFuzzyType = class(TKBType)   // ������������� ���� DTD 'type', ��� 'meta' = 'fuzzy'
  public
    MFList: TList;              // KB
    Units: TList;               // KB
    Synonyms: TStringList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML( Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TReferenceType �������� ����������� ����-������
  ////////////////////////////////////////////////////////////////////////////

  TReferenceType = class(TKBType)
  public
    //ReturnValue: TAbstractValue;
    //ParamDecl: TParamDecl;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TKBClass �������� ����������� ��� ������ ���� ������,
  // ������������ ����� �������� ��������� ���������� IDispatch ��� ������ � COM
  ////////////////////////////////////////////////////////////////////////////

  TKBClass = class(TKBType)     // ������������� ���� DTD 'class'
  public
    AncestorMeta: TAncestorMeta;// KB, ��� ������ ������
    AncestorID: WideString;     // KB, ������ � (ProgID | CLSID) COM-������� ��� � KBID ������ ��
    Properties: TList;          // KB, �������� ������
    Rules: TList;               // KB, ������� ������
    Methods: TList;             // KB, ������ ������
    ClassesRef: TList;          // ��������� �� ������ ������� � KBContainer
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function CreateInstance: TInstance; // ������� ��������� ������ TInstance
    procedure CreateObjectProperties(Env: TInstance; KB: TKBContainer); // ������� ��������-������� � ������ AutoCreate
    procedure FireInitialRules(Env: TInstance);
  private
    // ���� ��� ������ ID, �� ���������� nil
    function GetProp(ID: WideString): TNamedItem;       // TClassProperty
    function GetRule(ID: WideString): TNamedItem;       // TClassRule
    function GetMethod(ID: WideString): TNamedItem;     // TClassMethod
  public
    property Prop[ID: WideString]: TNamedItem read GetProp;
    property Rule[ID: WideString]: TNamedItem read GetRule;
    property Method[ID: WideString]: TNamedItem read GetMethod;
  end;


implementation

uses KB_Fuzzy, OM_Containers, KB_Instructions, KB_Values;

/////////////////////////////////////////
// ������ ������ TNumericType
/////////////////////////////////////////

constructor TNumericType.Create(AOwner: TComponent);
begin
  inherited;
  MinValue := 0;
  MaxValue := 0;
  IsInteger := false;
end;

procedure TNumericType.XML(Parent: IXMLNode);
var
  N: IXMLNode;
  Val: Variant;
begin
  N := Parent.AddChild(sType);
  N.Attributes[sID] := ID;
  N.Attributes[sMeta] := sNumber;
  N.Attributes[sDesc] := Desc;

  Val := MinValue;
  N.AddChild(sFrom);
  N.ChildNodes[sFrom].Text := Val;

  Val := MaxValue;
  N.AddChild(sTo);
  N.ChildNodes[sTo].Text := Val;
end;

function TNumericType.Valid(Value: TAbstractValue): boolean;
begin
  if MaxValue >= MinValue then Result := True
  else Result := False;
end;


/////////////////////////////////////////
// ������ ������ TSymbolicType
/////////////////////////////////////////

constructor TSymbolicType.Create(AOwner: TComponent);
begin
  inherited;
  Values := TStringList.Create;
end;

destructor TSymbolicType.Destroy;
begin
  Values.Free;
  inherited;
end;

procedure TSymbolicType.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sType);
  N.Attributes[sID] := ID;
  N.Attributes[sMeta] := sString;
  N.Attributes[sDesc] := Desc;

  for i:=0 to Values.Count-1 do begin
    M := N.AddChild(sValue);
    M.Text := Values[i];
  end;
end;


/////////////////////////////////////////
// ������ ������ TFuzzyType
/////////////////////////////////////////

constructor TFuzzyType.Create(AOwner: TComponent);
begin
  inherited;
  MFList := TList.Create;
  Units := TList.Create;
  Synonyms := TStringList.Create;
end;

destructor TFuzzyType.Destroy;
begin
  Synonyms.Free;
  Units.Free;
  MFList.Free;
  inherited;
end;

procedure TFuzzyType.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
  MF: TMembershipFunction;
begin
  N := Parent.AddChild(sType);
  N.Attributes[sID] := ID;
  N.Attributes[sMeta] := sFuzzy;
  N.Attributes[sDesc] := Desc;

  // ������� ��������������
  for i:=0 to MFList.Count-1 do begin
    MF := MFList[i];
    M := N.AddChild(sParameter);
    M.Attributes[sMinValue] := MF.XMin;
    M.Attributes[sMaxValue] := MF.XMax;
    M.AddChild(sValue);
    M.ChildNodes[sValue].Text := MF.MFName;
    MF.XML(M);
  end;

  // ������� ���������

  // ���������
end;


/////////////////////////////////////////
// ������ ������ TReferenceType
/////////////////////////////////////////



/////////////////////////////////////////
// ������ ������ TKBClass
/////////////////////////////////////////

constructor TKBClass.Create(AOwner: TComponent);
begin
  inherited;
  AncestorMeta := amNone;
  Desc := '';
  Properties := TList.Create;
  Rules := TList.Create;
  Methods := TList.Create;
end;

destructor TKBClass.Destroy;
begin
  Properties.Free;
  Rules.Free;
  Methods.Free;
  inherited;
end;

procedure TKBClass.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sClass);
  N.Attributes[sID] := ID;

  if AncestorMeta <> amNone then begin
    M := N.AddChild(sClassInfo);
    M.Text := AncestorID;
    if AncestorMeta = amKBID then M.Attributes[sMeta] := sKBID
    else if AncestorMeta = amProgID then M.Attributes[sMeta] := sProgID
    else if AncestorMeta = amCLSID then M.Attributes[sMeta] := sCLSID;
  end;

  N.Attributes[sDesc] := Desc;

  M := N.AddChild(sProperties);
  for i := 0 to Properties.Count-1 do
    TClassProperty(Properties[i]).XML(M);

  M := N.AddChild(sRules);
  for i := 0 to Rules.Count-1 do
    TClassRule(Rules[i]).XML(M);

  M := N.AddChild(sMethods);
  for i := 0 to Methods.Count-1 do
    TClassMethod(Methods[i]).XML(M);
end;

// �������� ���������� ������, (������������ � XMLLoader, ��������)
function TKBClass.CreateInstance: TInstance;
var
  Ancestor: TKBClass;
  cp: TClassProperty;
  i: integer;
begin
  Result := nil;

  case AncestorMeta of
  amProgID:     // ����� �������� ����������� �OM-�������, ��������� � ���� ProgID
    begin
      Result := TInstance.Create(nil);
      Result.ClassID := Self.ID;
      Result.SetClassRef(Self);
      try
        Result.Obj := CreateOleObject(AncestorID);
      except
        ErrorHandler.HandleError(eInvalidGUID, '������ ��� �������� ���������� ������ "'+Self.ID+'"');
      end;
    end;
  amCLSID:      // ����� �������� ����������� �OM-�������, ��������� � ���� CLSID
    begin
      Result := TInstance.Create(nil);
      Result.ClassID := Self.ID;
      Result.SetClassRef(Self);
      try
        Result.Obj := CreateCOMObject(StringToGUID(AncestorID)) as IDispatch;
      except
        ErrorHandler.HandleError(eInvalidGUID, '������ ��� �������� ���������� ������ "'+Self.ID+'"');
      end;
    end;
  amKBID:       // ����� �������� ����������� ������ ��
    begin
      Ancestor := GlbFindItem(ClassesRef, AncestorID) as TKBClass;
      if Ancestor <> nil then begin
        Result := Ancestor.CreateInstance;
        Result.ClassID := Self.ID;
        Result.SetClassRef(Self);
      end
      else begin
        ErrorHandler.HandleError(eInvalidID, '�� ������ ������ "'+AncestorID+'" ��� �������� ���������� ������ "'+Self.ID+'"');
      end;
    end;
  amNone:       // ����� �������� �������
    begin
      Result := TInstance.Create(nil);
      Result.ClassID := Self.ID;
      Result.SetClassRef(Self); // ������� ��������� ������
    end
  end;

  // �������� �������� ���������� �� ��������� (����� ����� ��� �������� �������, � ����� �� ���� ����)
  for i:=0 to Properties.Count-1 do begin
    cp := Properties[i];
    if cp.Default <> nil then begin
      Result.SetProperty(cp.ID, cp.Default, false);
    end;
  end;
end;

procedure TKBClass.CreateObjectProperties(Env: TInstance; KB: TKBContainer);
var
  cp: TClassProperty;
  i: integer;
  Instr: TCreateInstruction;
begin
  // c������� ��������-�������, � ������� ��������� ������ Create
  // !!! ������ ������������� �������� !!!

  for i := 0 to Properties.Count-1 do begin
    cp := Properties[i];
    // �������, ��� ������ Create ����� ��������� ������ � ��������
    if cp.ShallCreate and (cp.TypeRef is TKBClass) then begin
      // Result - ��� Env
      Instr := TCreateInstruction.Create(nil);
      Instr.ClassMeta := amKBID;
      Instr.ClassesRef := ClassesRef;
      Instr.ClassGUID := (cp.TypeRef as TKBClass).ID;
      Instr.VarRef := TReference.Create(Instr);
      Instr.VarRef.ID := cp.ID;
      Instr.VarRef.TypeRef := cp.TypeRef;
      Instr.Interprete(Env, KB, nil, nil);
      Instr.Free;
    end;
  end;
end;

procedure TKBClass.FireInitialRules(Env: TInstance);
var
  i: integer;
  r: TClassRule;
  Attrs: TAttrList;
begin
  // ����� ����� ��������� ��� ������� ���� initially
  Attrs := TAttrList.Create(Self);
  for i:=0 to self.Rules.Count-1 do begin
    r := Rules[i];
    if r.Meta = rmInitial then
      r.Interprete(Env, Attrs, true);
  end;
  Attrs.Free;

  // !!! � ��� ���� � ������ ��� ��� ���������...
end;


function TKBClass.GetProp(ID: WideString): TNamedItem;
begin
  Result := GlbFindItem(Properties, ID);
end;

function TKBClass.GetRule(ID: WideString): TNamedItem;
begin
  Result := GlbFindItem(Rules, ID);
end;

function TKBClass.GetMethod(ID: WideString): TNamedItem;
begin
  Result := GlbFindItem(Methods, ID);
  // ��� ������ � COM-��������
end;

end.
