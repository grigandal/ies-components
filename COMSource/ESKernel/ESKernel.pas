unit ESKernel;

{******************************************************************************
 * ��������� TESKernel ��������� ���� ��.
 * ������ � ����� ESKernelPack.bpl.
 * �����: << ������� �. �. >>    ���� ��������: << 8 ������, 2002 >>
 * ��������� ����������: << 10 ������, 2002 >>
 * Copyright (�) The DimOS Arts corp., 2002
 ******************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Classes, XMLIntf;

type
  TRequestValueEvent = procedure(Sender: TObject; AttrPath: string) of object;
  TErrorEvent = procedure(Sender: TObject; Description: string) of object;

  TESKernel = class(TComponent)
  private
    FKBFileName: string;
    FOnRequestValue: TRequestValueEvent;
    FOnError: TErrorEvent;
    FStyle: string;
    FSubdialogs: boolean;
  protected
    procedure SetStyle(Style: string); virtual; abstract;
  public
    StopFlag: boolean;
    // basic routines
    procedure StartInference; virtual; abstract;
    procedure StopInference; virtual; abstract;
    // to work with operating memory
    procedure SetAttrValue(ObjName: string; AttrName, Value: string; CF, CF2, Acc: integer); virtual; abstract;
    function  GetAttrValueCF(ObjName: string; AttrName: string; var CF, CF2, Acc: integer): string; virtual; abstract;
    function  GetAttrValue(ObjName: string; AttrName: string): string; virtual; abstract;
    procedure RetrieveFact(ObjName: string; AttrName: string); virtual; abstract;
    procedure AddGoal(ObjName: string; Goal: string); virtual; abstract;
    // ���������� ���������, ����� ���� ������
    procedure RequestValue(AttrPath: string); virtual; abstract;
    // ���������� ����� ��� ������� � ��������������� ��������
    procedure ProcessError(Description: string); virtual; abstract;
    // �������� ���� ������
    procedure LoadKB(FileName: string); virtual; abstract;
    procedure AddToTrace(n_obj, n_attr, n_rule, oznach: integer); virtual; abstract;
    function GetTrace: WideString; virtual; abstract;
    procedure RuleToXML(RuleInd,oznach: integer; XMLRule: IXMLNode); virtual; abstract;
  published
    property KBFileName: string read fKBFileName write fKBFileName;
    property Style: string read fStyle write SetStyle;
    property Subdialogs: boolean read fSubdialogs write fSubdialogs;
    property OnRequestValue: TRequestValueEvent read FOnRequestValue write FOnRequestValue;
    property OnError: TErrorEvent read FOnError write FOnError;
  end;

function UpString(Str : String) : String;

implementation

{==============================================================================}
  function UpString(Str : String) : String;
  var
    i : Integer;
  begin
    Result := '';
    for i := 1 to Length(Str) do
      case Str[i] of
        'a'..'z' : Result := Result + UpperCase(Str[i]);
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        else Result := Result + Str[i];
      end;
  end;
{==============================================================================}

end.




