unit ATESKernel;

{******************************************************************************
 * ��������� TATESKernel ��������� ���� ��.
 * ������ � ����� Shell.bpl � ���������� ��� ���������� ����.
 * �����: << ������� �. �. >>    ���� ��������: << 2 ������, 2001 >>
 * ��������� ����������: << 2 ������, 2001 >>
 * Copyright (�) The DimOS Arts corp., 2001
 ******************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Solve, Aspr95, KBTypes, KBCompon, ESKernel, XMLdoc, XMLIntf , DD_Sentential, DD_Tokenizer;

type
  TATESKernel = class(TESKernel)
  private
    // kernel components
    Solver: TSolve;
    KBControl: TKBControl;
    Trace: IXMLDocument;
    procedure InitComponents;
    function IsNumber(AttrName: string): boolean;
    function GetNumber(S: string): integer;
    procedure InitInference;
  public
    // basic routines
    procedure StartInference; override;
    procedure StopInference; override;
    // to work with operating memory
    procedure SetAttrValue(ObjName: string; AttrName, Value: string; CF, CF2, Acc: integer); override;
    function  GetAttrValueCF(ObjName: string; AttrName: string; var CF, CF2, Acc: integer): string; override;
    function  GetAttrValue(ObjName: string; AttrName: string): string; override;
    procedure RetrieveFact(ObjName: string; AttrName: string); override;
    procedure AddGoal(ObjName: string; Goal: string); override;
    // ���������� ���������, ����� ���� ������
    procedure RequestValue(AttrPath: string); override;
    // ���������� ����� ��� ������� � ��������������� ��������
    procedure ProcessError(Description: string); override;
    // �������� ���� ������
    procedure LoadKB(FileName: string); override;
    procedure SetStyle(Style: string); override;
    procedure AddToTrace(n_obj, n_attr, n_rule, oznach: integer); override;
    function GetTrace: widestring; override;
    procedure RuleToXML(RuleInd, oznach: integer; XMLRule: IXMLNode); override;
    procedure StructureToXML(list: TKBOwnedCollection; prefixed: string; var Node: IXMLNode);
    // constructor & destructor
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{=========================================================}
constructor TATESKernel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  KBFileName := '';
  StopFlag := False;
  Solver := TSolve.Create(Self);
  KBControl := TKBControl.Create(Self);
  KBControl.KB := TKnowledgeBase.Create(Self);
  KBControl.KB.KBInterface := TKBInterface.Create(Self);
  Trace := TXMLDocument.Create(NIL);
  InitComponents;
end;

destructor TATESKernel.Destroy;
begin
  Solver.Free;
  KBControl.KB.KBInterface.Free;
  KBControl.KB.Free;
  KBControl.Free;
  inherited Destroy;
end;

{--------------------------------------------------------}

procedure TATESKernel.InitComponents;
begin
  Solver.KBControl := KBControl;
  KBControl.KB.FileName := KBFileName;
  KBControl.ClearKB(KBControl.KB.KBOwner);
  Trace.LoadFromXML('<trace />');
end;

function TATESKernel.IsNumber(AttrName: string): boolean;
var i: integer;
begin
  Result := True;
  i:=1;
  while i<length(AttrName)do begin
    case AttrName[i] of
    '0'..'9': Inc(i);
    else
      begin
        Result := False;
        break;
      end;
    end
  end;
end;

function TATESKernel.GetNumber(S: string): integer;
var i: integer;
begin
  Result := -1;
  i:=length(S);
  while i>=0 do begin
    case S[i] of
    '0'..'9': Dec(i);
    else break;
    end
  end;
  if i<length(S) then begin
    Result := StrToInt(copy(S,i+1,Length(S)-i));
  end;
end;

{--------------------------------------------------------}

procedure TATESKernel.RequestValue(AttrPath: string);
begin
  if Assigned(OnRequestValue) then OnRequestValue(Self,AttrPath);
end;

procedure TATESKernel.ProcessError(Description: string);
begin
  if Assigned(OnError) then OnError(Self, Description);
end;

{--------------------------------------------------------}

procedure TATESKernel.InitInference;
begin
  Trace.LoadFromXML('<trace />');
end;

procedure TATESKernel.SetStyle(Style: string);
begin
  if UpString(Style) = 'BACKWARD' then KBControl.KB.Style := 1;
  if UpString(Style) = 'FORWARD' then KBControl.KB.Style := 2;
  if UpString(Style) = 'MIXED' then KBControl.KB.Style := 3; // ������ ����� � �� ����
end;

procedure TATESKernel.StartInference;
begin
  StopFlag := False;
  StartSolve(KBControl,StopFlag);
end;

procedure TATESKernel.StopInference;
begin
  StopFlag := True;
end;

{--------------------------------------------------------}

procedure TATESKernel.SetAttrValue(ObjName: string; AttrName, Value: string; CF, CF2, Acc: integer);
var
  i,n_attr,n_obj: integer;
  Obj: record_obj;
  Attr: record_attr;
  Assertion: TAssertion;
  found: boolean;
begin
  // �������� ����� �������� � �������� ��� � n_attr
  n_obj := GetNumber(ObjName);
  n_attr := GetNumber(AttrName);
  with KBControl.KB do begin
    i := 0;
    found := False; // ������ ������
    while i < ListObjs.Count do begin // ���� �� ��������
      Obj := record_obj(ListObjs.Items[i]);
      if (Obj.n_obj = n_obj) then begin
        found := true;
        break;
      end;
      Inc(i);
    end;
    if found then begin
      i := 0;
      found := false;
      while i < Obj.ListAttrs.Count do begin // ���� �� ���������
        Attr := record_attr(Obj.ListAttrs.Items[i]);
        if (UpString(AttrName) = UpString(Attr.comment))
        or (n_attr = Attr.n_attr)then begin
          Assertion := TAssertion(Attr.AssertionCollection.Items[0]);
          if Value = '' then Attr.Status := 1
          else begin
            Assertion.znach := Value;
            Assertion.factor1 := CF;
            Assertion.factor2 := CF2;
            Assertion.accuracy := Acc;
            Attr.status := 4;
          end;
          found := True;
          break;
        end;
        Inc(i);
      end;
    end;
  end;
  //if not Found then ProcessError('SetAttrValue: Attribute "'+AttrName+'" not found');
end;

function TATESKernel.GetAttrValueCF(ObjName: string; AttrName: string; var CF, CF2, Acc: integer): string;
var
  i,n_attr,n_obj: integer;
  Obj: record_obj;
  Attr: record_attr;
  Assertion: TAssertion;
  found: boolean;
begin
  Result := '';
  n_obj := GetNumber(ObjName);
  n_attr := GetNumber(AttrName);
  with KBControl.KB do begin
    i := 0;
    found := false;
    while i < ListObjs.Count do begin // ���� �� ��������
      Obj := record_obj(ListObjs.Items[i]);
      if (Obj.n_obj = n_obj) then begin
        found := true;
        break;
      end;
      Inc(i);
    end;
    if found then begin
      i := 0;
      found := false;
      while i<Obj.ListAttrs.Count do begin // ���� �� ���������
        Attr := record_attr(Obj.ListAttrs.Items[i]);
        if ((UpString(AttrName) = UpString(Attr.comment))
        or (n_attr = Attr.n_attr))
        and(Attr.AssertionCollection.Count>0)
        and(Attr.Status>2)
        then begin
          Assertion := TAssertion(Attr.AssertionCollection.Items[0]);
          CF := Assertion.factor1;
          CF2 := Assertion.factor2;
          Acc := Assertion.accuracy;
          Result := Assertion.znach;
          Found := True;
          break;
        end;
        Inc(i);
      end;
    end;
  end;
  //ShowMessage('n_attr: '+IntToStr(n_attr)+', '+AttrName+' = '+Result);
  //if not Found then ProcessError('GetAttrValue: Attribute "'+AttrName+'" not found');
end;

function TATESKernel.GetAttrValue(ObjName: string; AttrName: string): string;
var CF, CF2, Acc: integer;
begin
  Result := GetAttrValueCF(ObjName,AttrName,CF,CF2,Acc);
end;

procedure TATESKernel.RetrieveFact(ObjName: string; AttrName: string);
begin
  // ����� ���� � ������ � ���� ��������
  // ���� ����� ������ �� ����
end;

procedure TATESKernel.AddGoal(ObjName: string; Goal: string);
var
  i,n_attr,n_obj: integer;
  Obj: record_obj;
  Attr: record_attr;
  found: boolean;
begin
  n_obj := GetNumber(ObjName);
  n_attr := GetNumber(Goal);
  with KBControl.KB do begin
    i := 0;
    found := false;
    while i < ListObjs.Count do begin // ���� �� ��������
      Obj := record_obj(ListObjs.Items[i]);
      if (Obj.n_obj = n_obj) then begin
        found := true;
        break;
      end;
      Inc(i);
    end;
    if found then begin
      i := 0;
      found := false;
      while i<Obj.ListAttrs.Count do begin // ���� �� ���������
        Attr := record_attr(Obj.ListAttrs.Items[i]);
        if (UpString(Goal) = UpString(Attr.comment))
        or (n_attr = Attr.n_attr) then begin
          Attr.status := 2;
          Found := True;
          break;
        end;
        Inc(i);
      end;
    end;
  end;
  //if not Found then ProcessError('AddGoal: Attribute with the name "'+Goal+'" not found');
end;

procedure TATESKernel.LoadKB(FileName: string);
begin
  KBControl.ClearKB(KBControl.KB.KBOwner);
  KBControl.KB.FileName := FileName;
  KBControl.KB.Load;
end;

procedure TATESKernel.AddToTrace(n_obj, n_attr, n_rule, oznach: integer);
var
  Step: IXMLNode;
  i,j: integer;
  Obj: record_obj;
  Attr: record_attr;
begin
  Step := Trace.DocumentElement.AddChild('step');
  Step.Attributes['Obj'] := n_obj;
  Step.Attributes['Attr'] := n_attr;
  Step.Attributes['Rule'] := n_rule;
  Step.Attributes['Question'] := '� ��� ����?';
  Step.Attributes['Oznach'] := oznach;
  if n_rule = -1 then begin
    i:=0;
    while i<KBControl.KB.ListObjs.Count do begin
      j:=0;
      Obj := record_obj(KBControl.KB.ListObjs.Items[i]);
      if Obj.n_obj = n_obj then begin
        while j<Obj.ListAttrs.Count do begin
          Attr := record_attr(Obj.ListAttrs.Items[j]);
          if Attr.n_attr = n_attr then begin
            Step.Attributes['Question'] := Attr.comment;
            break;
          end;
          Inc(j);
        end;
        break;
      end;
      Inc(i);
    end;
  end;
end;

function TATESKernel.GetTrace: widestring;
begin
  Result := Trace.DocumentElement.XML;
end;

procedure TATESKernel.RuleToXML(RuleInd, oznach: integer; XMLRule: IXMLNode);
var
  N: IXMLNode;
  Rule: record_rule;
  i: integer;
  Sentential: TDDSentential;
  prefixed: string;
  found: boolean;
  Ass: TAssertion;
begin
  i:=0;
  found := false;
  while i<KBControl.KB.ListRules.Count do begin
    Rule := record_rule(KBControl.KB.ListRules.Items[i]);
    if Rule.n_rule = RuleInd then begin
      found := true;
      break;
    end;
    Inc(i);
  end;
  if not found then exit;
  XMLRule.Attributes['name'] := RuleInd;
  XMLRule.Attributes['confidence'] := 100;
  XMLRule.Attributes['comment'] := Rule.comment;
  XMLRule.Attributes['oznach'] := oznach;
  // ������� ���������� �������� ������� (���� ���)
  XMLRule.AddChild('attrs');
  for i:=0 to Rule.list_attr_if.Count-1 do begin
    Ass := TAssertion(Rule.list_attr_if.Items[i]);
    N := XMLRule.ChildNodes['attrs'].AddChild('attr');
    N.Attributes['object'] := Ass.n_obj;
    N.Attributes['attribute'] := Ass.n_attr;
  end;
  // �������� if ��������
  Sentential := TDDSentential.Create(NIL);
  Sentential.ArgPrefix := 'P';
  Sentential.InfixToPrefix(Rule.incident, prefixed); // D K N P1 D P2 P3 N P4
  Sentential.Free;
  N := XMLRule.AddChild('if');
  StructureToXML(Rule.list_attr_if, prefixed, N);
  // �������� then ����������
  N := XMLRule.AddChild('then');
  StructureToXML(Rule.list_attr_then, Rule.consequent, N);
  // �������� else ����������
  if Rule.list_attr_else.Count > 0 then begin
    N := XMLRule.AddChild('else');
    StructureToXML(Rule.list_attr_else, Rule.consequent_else, N);
  end;
end;

procedure TATESKernel.StructureToXML(list: TKBOwnedCollection; prefixed: string; var Node: IXMLNode);
var
  i,j: integer;
  Tokenizer: TDDTokenizer;
  N,P: IXMLNode;
  Tok: string;
  Ass: TAssertion;
  Obj: record_obj;
  Attr: record_attr;
  Tp: record_type;
begin
  N := Node;
  if prefixed = '' then // ���� ����������� ��������� ���, �� ������� ��� ���� 1 ����������
    prefixed := 'P1';
  Tokenizer := TDDTokenizer.Create(NIL);
  Tokenizer.Text := prefixed;
  Tokenizer.Tokenize;
  for i:=0 to Tokenizer.Tokens.Count-1 do begin
    Tok := Tokenizer.Tokens[i];
    case Tok[1] of
    'K': N := N.AddChild('and');
    'D': N := N.AddChild('or');
    'N': N := N.AddChild('not');
    'X': N := N.AddChild('xor');
    'P':
      begin
        Ass := TAssertion(list.Items[StrToInt(Copy(Tok,2,Length(Tok)-1))-1]);
        // ������ ������ � �������
        j:=0;
        while j<KBControl.KB.ListObjs.Count do begin
          Obj := record_obj(KBControl.KB.ListObjs.Items[j]);
          if Obj.n_obj = Ass.n_obj then break;
          Inc(j);
        end;
        j:=0;
        while j<Obj.ListAttrs.Count do begin
          Attr := record_attr(Obj.ListAttrs.Items[j]);
          if Attr.n_attr = Ass.n_attr then break;
          Inc(j);
        end;
        Tp := Attr.PtrType;
        case Tp.type_znach of
        0,2,3: // ���������� (0, 2) ��� �������� (3) �������
          begin
             P := N.AddChild('symbolic-assertion');
             P.Attributes['object'] := Obj.n_obj;
             P.Attributes['attribute'] := Attr.n_attr;
             P.Attributes['value'] := Ass.znach;
             P.Attributes['belief'] := Ass.factor1;
             P.Attributes['probability'] := Ass.factor2;
          end;
        1: // �������� �������
          begin
             P := N.AddChild('numeric-assertion');
             P.Attributes['object'] := Obj.n_obj;
             P.Attributes['attribute'] := Attr.n_attr;
             case Ass.predicat of
             0: P.Attributes['operator'] := '=';
             1: P.Attributes['operator'] := '>';
             2: P.Attributes['operator'] := '<';
             3: P.Attributes['operator'] := '>=';
             4: P.Attributes['operator'] := '<=';
             5: P.Attributes['operator'] := '<>';
             end;
             P.Text := Ass.znach;
             P.Attributes['belief'] := Ass.factor1;
             P.Attributes['probability'] := Ass.factor2;
             P.Attributes['accuracy'] := Ass.accuracy;
          end;
        end;
      end;
    end;
  end;
  Tokenizer.Free;
end;

{=========================================================}

end.
