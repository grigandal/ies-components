unit DD_Sentential;

{******************************************************************************
 * ���� DD_Sentential.pas �������� ����������� ���������������
 * ���������� ���������. ������ � ����� IE_pack.bpl � ���������� TDDTokenizer.
 * �����: << ������� �. �. >>    ���� ��������: << 16 �������, 2001 >>
 * ��������� ����������: << 16 �������, 2001 >>
 * Copyright (�) The DimOS Arts corp., 2001
 ******************************************************************************}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DD_Tokenizer;

type
  TDDSentential = class(TComponent)
  private
    FArgPrefix: char; //������� ����� ������� ��������� ('$','#','P',...)
  private
    // P - ������ � ���������� ������, I - � ���������
    function IsOp(c: char): boolean;
    function IsArg(s: string): boolean;
    // ��� �������� ��������� ������ � ����������
    function ToPrefix(Tokens: TStrings; b,e: integer): string;
    function CmpPriority(Op1, Op2: char): boolean; // True if op1 > op2
    function ParsOK(Tokens: TStrings; b,e: integer; var bounds: boolean): boolean; //�������� ��������� ������
    function OpWithLeastPriority(Tokens: TStrings; b,e: integer): integer;
    // ��� �������� ���������� � ���������
    function ToInfix(Tokens: TStrings; var Pos: integer): string;
    // ��� ���������� ������� �� ������� � ���������
    function GetArgEnding(P: TStrings; b: integer): integer;
    function GatherTokens(P: TStrings; b,e: integer): string;
    // ------------ ������ �������� ������ � ����������� ------------
    // ��� ���������� ������ ������������ (��� ������ ���������)
    function Contained(Conj: TStrings; arg: string; power: boolean): boolean;
    // ��� ���������� ������ ���������� �� ����������(Ex: '~$2&~$3&$4 ~$1&$1 $4&$2')
    function Subset(Conj1, Conj2: string): boolean; //True if Conj1 is a subset of Conj2
    // ��� ���������� ������ ���������� ��������
    function CLength(Conj: string): integer;
    function Equal(Conj1, Conj2: string): boolean;
    procedure Split(Conj: string; i: integer; var letter: string; var SubConj: string);
    // ------------ ���������� � ���� ---------------
    procedure AddUnique(Conjuncts: TStrings; Conj: string);
    // �������� ������ ���� ����������
    procedure CollectArgList(C: string; Args: TStrings);
    // ������ ��������� ������������� � ��������� Conj
    procedure SubtractArgs(Args: TStrings; Conj: string; Missing: TStrings);
    // ����������� ���������� �� ����
    procedure Grow(Conj: string; Missing: TStrings; ind: integer; Res: TStrings); // �����������
  public
    Error: string;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    // ����� ������ ��������������� �������
    // ������ ����������� ������� �� ������� �� ���� ���������� ��������
    procedure InfixToPrefix(I: string; var P: string);
    procedure PrefixToInfix(P: string; var I: string);
    function ApplyDeMorgan(P: string): string;
    function ApplyInvolution(P: string): string;
    function ApplySndDistr(P: string): string;
    // ���������� ������ ��������������� � ������������ ��� ������ ���������
    function ApplyContradiction(Conj: string): string;
    function ApplyIdempotency(Conj: string): string;
    // ��������� ��� ���� ���������� ��������������� � ������������
    function FormConjuncts(I: TStrings): string;
    function ApplyIdemNCont(C: string): string;
    function ApplyAbsorption(C: string): string;
    function ApplyExclusion(C: string): string;
    // � ������ ��� � ����� (������� ����������)
    function BuildTPatterns(F: string; prefix: boolean): string;
    function BuildSANF(C: string): string;
    // ��������� ����������� �������� � ���������������� ���������� ������
    // !!! ������ ������� �� ������� �� ���� ���������� �������� !!!
    function ToDotNotation(S: string): string;
    function ToSententialNotation(D: string): string;
  published
    property ArgPrefix: char read FArgPrefix write FArgPrefix;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('IE Tools', [TDDSentential]);
end;

{============= TDDSentential ===============}
constructor TDDSentential.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ArgPrefix := '$';
end;

destructor TDDSentential.Destroy;
begin
  inherited Destroy;
end;

// ------------ private procs -----------------

function TDDSentential.IsOp(c: char): boolean;
begin
  case c of
  '+','|', '&', '~', '�', 'D', 'N': Result := True;
  else Result := False;
  end;
end;

function TDDSentential.IsArg(s: string): boolean;
begin
  if (s[1]=ArgPrefix)or(s[1]='I') then Result := True
  else Result := False;
end;

// ---------------------- ��������� � ���������� ----------------------------

procedure TDDSentential.InfixToPrefix(I: string; var P: string);
var
  bounds: boolean;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  Tok.Text := I;
  Tok.Delimiters := '()&+|~';
  Tok.DelimAsToken := True;
  Tok.Tokenize;

  if ParsOK(Tok.Tokens, 0, Tok.Tokens.Count-1, bounds) then
    P := ToPrefix(Tok.Tokens, 0,Tok.Tokens.Count-1)
  else Error := 'Wrong parenthesis';
  Tok.Free;
end;

function TDDSentential.ToPrefix(Tokens: TStrings; b,e: integer): string;
var
  bounds: boolean;
  i: integer;
begin
  if b = e then Result := Tokens[b]+' '  //��� ��������
  else if ParsOK(Tokens, b,e, bounds) and bounds then
    Result := ToPrefix(Tokens, b+1, e-1) //������� ������
  else begin
    i := OpWithLeastPriority(Tokens, b,e);
    if Tokens[i]='&' then
      Result := 'K '+ToPrefix(Tokens,b,i-1)+ToPrefix(Tokens,i+1,e)
    else if (Tokens[i]='+')or(Tokens[i]='|') then
      Result := 'D '+ToPrefix(Tokens,b,i-1)+ToPrefix(Tokens,i+1,e)
    else if Tokens[i]='~' then
      Result := 'N '+ ToPrefix(Tokens,i+1,e)
  end;
end;

function TDDSentential.CmpPriority(Op1, Op2: char): boolean; // True if op1 > op2
begin
  Result := True;
  case op1 of
  '+','|': Result := False; // ������ ����, � � ��� ������� ������
  '&': if (Op2 = '&') or (Op2 = '~') then Result := False;
  '~': if (Op2 = '~') then Result := False;
  end;
end;

function TDDSentential.ParsOK(Tokens: TStrings; b,e: integer; var bounds: boolean): boolean;
var i,c: integer;
begin
  c:=0; //������ ������
  i:=b;
  Result := True;
  bounds := True; //���� ���� ������� ����������� ������ (����� ����� ����� ��������� ������ ���������)
  if (Tokens[b] <> '(') or (Tokens[e] <> ')') then bounds := False;
  while i<=e do begin
    if Tokens[i]='(' then Inc(c)
    else if Tokens[i]=')'then Dec(c);
    if c<0 then begin
      Result := False; //������ ����������� ������ ��� �����������
      break;
    end
    else if (c=0) and (i<e)then begin
      //���� ������ ����������� ��� �� ���������� �������, ������ ������� ������ ���
      bounds := False;
    end;
    Inc(i);
  end;
  if c>0 then Result := False; //������ ����������� ������
end;

function TDDSentential.OpWithLeastPriority(Tokens: TStrings; b,e: integer): integer;
var
  i, Nesting,
  Least, LNesting: integer;
begin
  Least := -1; //����� �� ���� warning, ���� �������� ������ ������
  LNesting := 0;
  //������ ������ ��������
  i := b;
  Nesting := 0;
  while i<=e do begin
    if Tokens[i] = '(' then Inc(Nesting)
    else if Tokens[i] = ')' then Dec(Nesting)
    else if IsOp(Tokens[i][1]) then begin
      Least := i;
      LNesting := Nesting;
      break;
    end;
    Inc(i);
  end;
  //������ �����������, ������� � ������� �������
  while i<=e do begin
    if Tokens[i] = '(' then Inc(Nesting)
    else if Tokens[i] = ')' then Dec(Nesting)
    else if IsOp(Tokens[i][1]) then begin
      if (LNesting > Nesting)
      or ((LNesting = Nesting) and (CmpPriority(Tokens[Least][1],Tokens[i][1])))
      then begin
        Least := i;
        LNesting := Nesting;
      end
    end;
    Inc(i);
  end;
  Result := Least;
end;

// -------------------------- ���������� � ��������� -------------------------

procedure TDDSentential.PrefixToInfix(P: string; var I: string);
var
  pos: integer;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  Tok.Text := P;
  Tok.Tokenize;
  Pos := 0;
  I := ToInfix(Tok.Tokens, pos);
  Tok.Free;
end;

function TDDSentential.ToInfix(Tokens: TStrings; var Pos: integer): string;
begin
  if Pos > Tokens.Count-1 then
    Result := ''
  else if IsArg(Tokens[Pos]) then begin
    Result := Tokens[Pos];
    Inc(Pos);
  end
  else if Tokens[Pos] = 'N' then begin
    Inc(Pos);
    // ���� ������ ���� �������� �� ��� ������
    if IsArg(Tokens[Pos]) then Result := '~ '+ToInfix(Tokens,Pos)
    else Result := '~('+ToInfix(Tokens,Pos)+')';
  end
  else if Tokens[Pos] = 'D' then begin
    Inc(Pos);
    // ���� ��� ������ ����� �� ������� ������ �� �����
    if Pos=1 then Result := ToInfix(Tokens,Pos)+' + '+ToInfix(Tokens,Pos)
    else Result := '('+ToInfix(Tokens,Pos)+' + '+ToInfix(Tokens,Pos)+')';
  end
  else if Tokens[Pos] = 'K' then begin
    Inc(Pos);
    // ���������� ��� ������ ������������
    Result := ToInfix(Tokens,Pos)+' & '+ToInfix(Tokens,Pos);
  end
end;

// ------------ ���������� ������� �� ������� � ��������� ��������� ---------
function TDDSentential.GetArgEnding(P: TStrings; b: integer): integer;
begin
  if P[b]='N' then    // ������� �������� ����� ���� ��������
    Result := GetArgEnding(P,b+1)
  else if (P[b]='K') or (P[b]='D') then // �������� - ���
    Result := GetArgEnding(P,GetArgEnding(P,b+1)+1)
  else Result := b;     //��� �������� $ddd �������� ���� �����
end;

function TDDSentential.GatherTokens(P: TStrings; b,e: integer): string;
var i: integer;
begin
  Result := '';
  for i:=b to e do
    Result := Result+P[i]+' '; // �������� ������ � ������
end;

function TDDSentential.ApplyDeMorgan(P: string): string;  // (~(a+b) = ~a&~b)
var
  wasUsed: boolean;
  s: string;
  i,e: integer;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := P;
    Tokenize;
    if Tokens.Count<=3 then Result := P //�������� ������
    else begin //���� ������� �� ������� � �����
      i:=0;
      wasUsed := False;
      s := '';
      while i <= (Tokens.Count-1) do begin
        if Tokens[i]<>'N' then begin //������ ��������
          s := s+Tokens[i]+' ';
          Inc(i);
        end
        else begin // ��������� ���������, �������� ������������ �� �������
          if Tokens[i+1]='K' then begin
            wasUsed := True;
            e := GetArgEnding(Tokens,i+2);
            s := s+'D N '+GatherTokens(Tokens,i+2,e);
            i := e+1;
            e := GetArgEnding(Tokens,i);
            s := s+'N '+GatherTokens(Tokens,i,e);
            i := e+1;
          end
          else if Tokens[i+1]='D'then begin
            wasUsed := True;
            e := GetArgEnding(Tokens,i+2);
            s := s+'K N '+GatherTokens(Tokens,i+2,e);
            i := e+1;
            e := GetArgEnding(Tokens,i);
            s := s+'N '+GatherTokens(Tokens,i,e);
            i := e+1;
          end
          else begin // ������ �������� �� ���������
            s := s+Tokens[i]+' ';
            Inc(i);
          end;
        end;
      end;
      if wasUsed then Result := ApplyDeMorgan(s)
      else Result := s;
    end;
  end;
  Tok.Free;
end;

function TDDSentential.ApplyInvolution(P: string): string; // (~~a = a)
var
  i: integer;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := P;
    Tokenize;
    if Tokens.Count<=2 then Result := P //�������� ������
    else begin //���� ������� �� ���� � �����
      i:=0;
      Result := '';
      while i <= (Tokens.Count-1) do begin
        if (Tokens[i]='N') and (Tokens[i+1]='N')then Inc(i,2)
        else begin
          Result := Result+Tokens[i]+' ';
          Inc(i);
        end;
      end;
    end;
  end;
  Tok.Free;
end;

// ------------ ���������� ������� ��������������� ������ ------------------
function TDDSentential.ApplySndDistr(P: string): string;   // (a&(b + c) = a&b +a&c)
var
  e,i: integer;
  wasUsed: boolean;
  fst,snd,thd,s: string;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := P;
    Tokenize;
    if Tokens.Count<=4 then Result := P //�������� ������
    else begin //���� ������� �� ������� � �����
      i:=0;
      wasUsed := False;
      s := '';
      while i <= (Tokens.Count-1) do begin
        if(Tokens[i]='K')
        and(Tokens[i+1]='D')then begin //������ �������� - ����������
          wasUsed := True;
          e := GetArgEnding(Tokens,i+2);
          fst := GatherTokens(Tokens,i+2,e);  // a
          i := e+1;
          e := GetArgEnding(Tokens,i);
          snd := GatherTokens(Tokens,i,e);    // b
          i := e+1;
          e := GetArgEnding(Tokens,i);
          thd := GatherTokens(Tokens,i,e);    // c
          s := s+'D K '+fst+thd+'K '+snd+thd; // ac + bc
          i := e+1;
        end
        else if(Tokens[i]='K')
        and(Tokens[GetArgEnding(Tokens,i+1)+1]='D')then begin //������ �������� - ����������
          wasUsed := True;
          e := GetArgEnding(Tokens,i+1);
          fst := GatherTokens(Tokens,i+1,e);  // c
          i := e+2; //��������� � ���������� D
          e := GetArgEnding(Tokens,i);
          snd := GatherTokens(Tokens,i,e);    // a
          i := e+1;
          e := GetArgEnding(Tokens,i);
          thd := GatherTokens(Tokens,i,e);    // b
          s := s+'D K '+fst+snd+'K '+fst+thd; // ca + cb
          i := e+1;
        end
        else begin
          s := s+Tokens[i]+' ';
          Inc(i);
        end;
      end;
      if wasUsed then Result := ApplySndDistr(s)
      else Result := s;
    end;
  end;
  Tok.Free;
end;

{======== ������ �������� ������ � ����������� =========}

function TDDSentential.FormConjuncts(I: TStrings): string;
var
  j: integer;
begin
  Result:='';
  j:=0;
  while j<I.Count do begin
    case I[j][1] of //������ ����������
    '+','|': Result := Result+' '; //�������� ��������� ���������
    '~','&': Result := Result+I[j];
    else
      if I[j][1]=ArgPrefix then Result := Result+I[j];
    end;
    Inc(j);
  end;
end;

// ------------------- ���������� ������ ��������������� -------------------
function TDDSentential.ApplyIdempotency(Conj: string): string;  // (a & a = a)
var
  i,j: integer;
  Tok: TDDTokenizer;
begin
  //������ ������������ ����� � ����������
  j:=0;
  Tok := TDDTokenizer.Create(Self);
  Result := '';
  with Tok do begin
    Text := Conj;
    Delimiters := '&';
    Tokenize;
    while (j <= Tokens.Count-2) do begin
      i:=j+1;
      // ��������, ����� �� � ��� �� ��������
      while i<Tokens.Count do begin
        if Tokens[j]=Tokens[i] then break;
        Inc(i);
      end;
      if i=Tokens.Count then Result := Result + Tokens[j]+'&';
      Inc(j);
    end;
    Result := Result+Tokens[Tokens.Count-1]; //��������� �����
  end;
  Tok.Free;
end;

// ---------- ���������� ������ ������������ (��� ������ ���������) -----------
function TDDSentential.Contained(Conj: TStrings; arg: string; power: boolean): boolean;
var i: integer;
begin
  //����� ����� - ������
  i:=0;
  Result := False;
  // �������� ����� ������� ������������ ��� ������������ (power)
  while i<Conj.Count do begin
    if power then begin //������ ������� ������������
      if Conj[i]=Arg then begin
        Result := True;
        break;
      end
    end
    else begin //������ ������� ������������
      if (Conj[i][1]='~')and(copy(Conj[i],2,Length(Conj[i])-1)=Arg) then begin
        Result := True;
        break;
      end
    end;
    Inc(i);
  end
end;

function TDDSentential.ApplyContradiction(Conj: string): string; // (a & ~a = False)
var
  i: integer;
  Tok: TDDTokenizer;
begin
  Result := Conj;
  Tok := TDDTokenizer.Create(Self);
  i:=0;
  with Tok do begin
    Text := Conj;
    Delimiters := '&'; //�������� �������� �� ������
    Tokenize;
    // �������� �������� �� ��������� ��������������� ����� (a&~a)
    while i<Tokens.Count do begin
      if ((Tokens[i][1]='~')and Contained(Tokens,copy(Tokens[i],2,Length(Tokens[i])-1),True))
      or ((Tokens[i][1]=ArgPrefix)and Contained(Tokens,Tokens[i],False))
      then begin
        Result := ''; // �������� ���������� ������
        break;
      end;
      Inc(i);
    end;
  end;
  Tok.Free;
end;

// ------ ��������� ��� ���� ���������� ��������������� � ������������ -------
function TDDSentential.ApplyIdemNCont(C: string): string;
var
  Tok: TDDTokenizer;
  j: integer;
  s: string;
begin
  Result := '';
  j:=0;
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := C;
    Tokenize;
    while j<Tokens.Count do begin
      s := ApplyContradiction(ApplyIdempotency(Tokens[j]));
      if s<>'' then begin //�������� �� �������� ������������
        Result := Result+s+' ';
      end;
      Inc(j);
    end;
  end;
  Tok.Free;
end;

// ------------------- ���������� ������ ���������� ------------------------
function TDDSentential.Subset(Conj1, Conj2: string): boolean;
var
  Tok: TDDTokenizer;
  j: integer;
  position: integer;
begin
  Result := True;
  j:=0;
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := Conj1;
    Delimiters := '&'; //�������� ������ �������� �� ������
    Tokenize;
    // ������ ������ ������� ��������� ������ ������� �� ������
    // ��������������, ��� ���������� ������ �� ����� ����
    while j<Tokens.Count do begin
      position := Pos(Tokens[j], Conj2);
      if ((position >1) and (Conj2[position-1]='~')) // ������� �� ���������
      or (position = 0) then begin // ��� ������ �� �����
        Result := False;
        break;
      end
      else
      Inc(j);
    end;
  end;
  Tok.Free;
end;

function TDDSentential.ApplyAbsorption(C: string): string;  // (a + a&b = a)
var
  Tok: TDDTokenizer;
  i,j: integer;
  ok: boolean;
begin
  Tok := TDDTokenizer.Create(Self);
  Result := '';
  with Tok do begin
    Text := C;
    Tokenize; //�������� �� ���������
    j:=0;
    while (j<Tokens.Count) do begin
      //���� ��������� ����������� ���������, ��������� ��� �� �����, ����� i=j
      i:=0;
      ok := True; // �������� �� ��� �� �����������
      while (i<Tokens.Count) do begin
        if(i<>j)and Subset(Tokens[i],Tokens[j])then begin //���� ���� ���� i ��������� j
          if Subset(Tokens[j],Tokens[i])then Tokens.Delete(i) //��������� �����
          else ok := False;
          //ShowMessage('�������� '+Tokens[j]+' �������� ���������� '+Tokens[i]);
          break;
        end;
        Inc(i);
      end;
      if ok then Result := Result+Tokens[j]+' ';
      Inc(j);
    end;
  end;
  Tok.Free;
end;

// ----------------- ���������� ������ ���������� �������� --------------
function TDDSentential.CLength(Conj: string): integer;
var Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  Tok.Text := Conj;
  Tok.DelimAsToken := False;
  Tok.Delimiters := '&';
  Tok.Tokenize;
  Result := Tok.Tokens.Count;
  Tok.Free;
end;

procedure TDDSentential.Split(Conj: string; i: integer; var letter: string; var SubConj: string);
var
  Tok: TDDTokenizer;
  j: integer;
begin
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Tok.DelimAsToken := False;
    Delimiters := '&';
    Text := Conj;
    Tokenize;
    letter := Tokens[i];
    Tokens.Delete(i);
    SubConj := '';
    if Tokens.Count>0 then begin
      SubConj := Tokens[0];
      if Tokens.Count>1 then
      for j:=1 to Tokens.Count-1 do
        SubConj := SubConj+'&'+Tokens[i];
    end;
  end;
end;

function TDDSentential.ApplyExclusion(C: string): string; // (a + ~a = True)
var
  Tok: TDDTokenizer;
  i,j,k: integer;
  SubConj,letter,arg: string;
  excl: boolean;
begin
  Tok := TDDTokenizer.Create(Self);
  j:=0;
  Result := '';
  with Tok do begin
    Text := C;
    Tokenize; //�������� �� ���������
    while (j<Tokens.Count) do begin
      // ���� ������� �������� ��������� ���������� �����, ����� i=j
      // � ������� ��������� ���� x1&�&xn&b � x1&�&xn&~b � ����: x1&�&xn.
      i:=0;
      excl := False;
      while (i<Tokens.Count) do begin
        // ���� ����� ���������, �� ������ �� ����������
        if (i<>j)and(CLength(Tokens[i])=CLength(Tokens[j])) then begin
          k := 0;
          while k<CLength(Tokens[i]) do begin
            Split(Tokens[i],k,letter,SubConj);
            if letter[1]='~' then arg := copy(letter,2,length(letter)-1)
            else arg := '~'+letter;
            if Equal(SubConj+'&'+arg,Tokens[j])then begin
              excl := True;
              Tokens.Delete(i);
              break;
            end;
            Inc(k);
          end; // while k
          if excl then begin
            Tokens.Delete(j);
            break;
          end;
        end;
        Inc(i);
      end;
      if excl then begin                   // ����� ������������� ���������...
        if SubConj='' then begin           // ���� ��������� �� �������, �� ��� ������
          Result := 'True';                // � ������ ����� �� ����������
          break;
        end
        else Result := Result+SubConj+' '  // ���� �� �����, �� �������� ���������
      end
      else Result := Result+Tokens[j]+' '; // ����� ������ ��������
      Inc(j);
    end;
  end;
  Tok.Free;
end;

// ------------- � ������ ��� � ����� (������� ����������) --------------
function TDDSentential.BuildTPatterns(F: string; prefix: boolean): string;
var
  P,I,s: string;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  if prefix then P := F else InfixToPrefix(F, P);
  // �� ������ + ��������� + II ��������������
  s := ApplySndDistr(ApplyInvolution(ApplyDeMorgan(P)));
  PrefixToInfix(s,I); // ��������� � ��������� ������
  Tok.Text := I;
  Tok.Delimiters := '()';
  Tok.DelimAsToken := True;
  Tok.Tokenize;
  // ��������������� + ������������� + ���������� + ���������� ��������
  Result := {ApplyExclusion(}ApplyAbsorption(ApplyIdemNCont(FormConjuncts(Tok.Tokens)));
  Tok.Free;
end;

// ----------------------- ���������� � ���� -------------------------------
function TDDSentential.Equal(Conj1, Conj2: string): boolean;
begin
  if Subset(Conj1,Conj2) and Subset(Conj2,Conj1) then Result := True
  else Result := False;
end;

procedure TDDSentential.AddUnique(Conjuncts: TStrings; Conj: string);
var i: integer;
begin
  i:=0;
  while i<Conjuncts.Count do begin
    if Equal(Conjuncts[i],Conj) then break;
    Inc(i);
  end;
  if i=Conjuncts.Count then Conjuncts.Add(Conj);
end;

procedure TDDSentential.CollectArgList(C: string; Args: TStrings);
  procedure UniqueInsert(Args: TStrings; Arg: string);
  var i: integer;
  begin
    i:=0;
    while i<Args.Count do begin
      if Args[i]=Arg then break;
      Inc(i);
    end;
    if i=Args.Count then Args.Add(Arg);
  end;
var
  Tok: TDDTokenizer;
  i: integer;
begin
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := C;
    Delimiters := '&~'; //������� ������ ���������
    Tokenize;
    for i:=0 to Tokens.Count-1 do UniqueInsert(Args,Tokens[i]);
  end;
  Tok.Free;
end;

procedure TDDSentential.SubtractArgs(Args: TStrings; Conj: string; Missing: TStrings);
var
  Tok: TDDTokenizer;
  i,j: integer;
begin
  Tok := TDDTokenizer.Create(Self);
  Missing.Text := Args.Text;
  with Tok do begin
    Text := Conj;
    Delimiters := '~&';
    Tokenize;
    // ������� �������� ��������, �� �� �������, � �� ��������� ���������
    for i:=0 to Tokens.Count-1 do
      for j:=0 to Args.Count-1 do
        if Tokens[i]=Args[j] then Missing[j]:='';
    // �������� ������ ���������
    j:= Missing.Count-1;
    while j>=0 do begin
      if Missing[j]=''then Missing.Delete(j);
      Dec(j);
    end;
  end;
  Tok.Free;
end;

procedure TDDSentential.Grow(Conj: string; Missing: TStrings; ind: integer; Res: TStrings);
var arg: string;
begin
  if ind>=0 then begin
    Arg := Missing[ind];
    Grow(Conj+'&'+Arg, Missing, ind-1, Res);
    Grow(Conj+'&~'+Arg, Missing, ind-1, Res); //����� Missing ��� ��������
  end
  else
    Res.Add(Conj);
end;

function TDDSentential.BuildSANF(C: string): string;
var
  SANF,Args,Missing,Res: TStrings;
  Tok: TDDTokenizer;
  i,j: integer;
begin
  SANF := TStringList.Create;
  Args := TStringList.Create;
  Missing := TStringList.Create;
  Res:= TStringList.Create;
  Tok := TDDTokenizer.Create(Self);
  with Tok do begin
    Text := C;
    Tokenize;
    CollectArgList(C, Args);
    for i:=0 to Tokens.Count-1 do begin
      SubtractArgs(Args,Tokens[i],Missing); //� Missing - ������������� � Tokens[i] ���������
      Res.Clear;
      Grow(Tokens[i],Missing,Missing.Count-1,Res);
      for j:=0 to Res.Count-1 do AddUnique(SANF,Res[j]);
    end;
  end;
  Result := GatherTokens(SANF,0,SANF.Count-1);
  Tok.Free;
  SANF.Free;
  Res.Free;
  Args.Free;
  Missing.Free;
end;

// ��������� ����������� �������� � ���������������� ���������� ������
function TDDSentential.ToDotNotation(S: string): string;
 function ToDot(T: TStrings; b: integer; var e: integer): string;
 begin
   if T[b]='N' then   // ������� �������� ����� ���� ��������
     Result := 'N'+ToDot(T,b+1,e)+'.'
   else if (T[b]='K') or (T[b]='D') then // �������� - ���
     Result := T[b]+ToDot(T,b+1,e)+ToDot(T,e+1,e)+'.'
   else begin
     Result := T[b]; //��� �������� $ddd �������� ���� �����
     e := b;
   end;
 end;
var
  i: integer;
  Tok: TDDTokenizer;
begin
  Tok := TDDTokenizer.Create(Self);
  Tok.Text := S;
  Tok.Tokenize;
  // ���� ������� ����������, ����� �������� (����� ����� ���������� ��������)
  Result := ToDot(Tok.Tokens,0,i);
  Tok.Free;
end;

function TDDSentential.ToSententialNotation(D: string): string;
var i, argN: integer;
begin
  Result := '';
  argN := 0;
  // ���� �������� ����������, ����� ������� ���������������� ����������
  // � ����������� ���� $ddd ������ I
  for i:=1 to length(D) do begin
    if D[i]<>'.' then
      if D[i]<>'I' then
        Result := Result+D[i]+' '
      else begin
        Result := Result+'$'+IntToStr(argN)+' ';
        Inc(argN);
      end;
  end;
end;

end.
