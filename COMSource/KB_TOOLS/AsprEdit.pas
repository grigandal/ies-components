unit AsprEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, VCL.Graphics, VCL.Controls, VCL.Forms,
  VCL.Dialogs,
  VCL.StdCtrls, KBTypes;

type
  TAsprSess = class(TForm)
    ModeEdit: TEdit;
    ModeLabel: TLabel;
    ProblemLabel: TLabel;
    ProblemEdit: TEdit;
    DBLabel: TLabel;
    DbEdit: TEdit;
    DictLabel: TLabel;
    DictEdit: TEdit;
    StageLabel: TLabel;
    TypeLabel: TLabel;
    CountLabel: TLabel;
    ProtokolLabel: TLabel;
    FieldLabel: TLabel;
    UnitedLabel: TLabel;
    TestedLabel: TLabel;
    StageEdit: TEdit;
    TypeEdit: TEdit;
    SolverLabel: TLabel;
    CountEdit: TEdit;
    ProtokolEdit: TEdit;
    FieldEdit: TEdit;
    UnitedEdit: TEdit;
    TestedEdit: TEdit;
    SolverEdit: TEdit;
    KbLabel: TLabel;
    ExplanationLabel: TLabel;
    ConsultLabel: TLabel;
    EditedLabel: TLabel;
    KbEdit: TEdit;
    ExplanationEdit: TEdit;
    ConsultEdit: TEdit;
    EditedEdit: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  { Detachment: TDetachment; }
  AsprSess: TAsprSess;

procedure SetData(KBControl: TKBControl);

implementation

{$R *.DFM}

procedure SetData(KBControl: TKBControl);
begin
  with AsprSess do
  begin
    with KBControl do
    begin
      { ����� ������ }
      if Mode then
        ModeEdit.Text := 'True'
      else
        ModeEdit.Text := 'False';

      { ������ �� �� ������� ��� }
      ProblemEdit.Text := FProblemName;
      { DbEdit.Text:=DatabaseName.Name;??? }

      { ������, ������������ ������������� }
      StageEdit.Text := FCurrentToolClass;

      { ������, ������������ ����������� �������������� }
      { ProjectData }

      { ������ ����� ���������������� }
      DictEdit.Text := DictName;
      TypeEdit.Text := FTypeOfTask;
      CountEdit.Text := IntToStr(CountOfSessions);
      if IsProtokol then
        ProtokolEdit.Text := 'True'
      else
        ProtokolEdit.Text := 'False';

      { ������ ���������������� }
      if IsKnowledgeField then
        FieldEdit.Text := 'True'
      else
        FieldEdit.Text := 'False';
      if IsUnited then
        UnitedEdit.Text := 'True'
      else
        UnitedEdit.Text := 'False';
      if IsTested then
        TestedEdit.Text := 'True'
      else
        TestedEdit.Text := 'False';

      { ������ ������������ �� }

      SolverEdit.Text := FNameOfSolver;
      if IsKb then
        KbEdit.Text := 'True'
      else
        KbEdit.Text := 'False';
      if IsExplanation then
        ExplanationEdit.Text := 'True'
      else
        ExplanationEdit.Text := 'False';
      { Style: Integer;
        CalcMethod: TCalcMethod; }

      { ������ ������������ �� }
      CountEdit.Text := IntToStr(FCountOfConsult);
      if IsEdited then
        EditedEdit.Text := 'True'
      else
        EditedEdit.Text := 'False';
    end;
  end;
end;

end.
