unit Wizards;

interface
  uses Classes, SysUtils, DsgnIntf, TypInfo, Paths,
       KBCompon, KBTypes, Dialogs, Forms, KBInt,
       RA_Classes, Types,
       FT_Dictionary, Agents, ActnList, ExtCtrls,
       LPDict, SemKat_, ProjLex, DefTask, LPTests;

  {$R Wizards.res}

  type
    TToolType=( _Nil, _TAdd, _TAddDict, _TAdition, _TConsult, _TDetachment,
                _THtvForm, _TKBControl, _TKBEdit, _TKBEditor, _TKBInterface,
                _TKBFormInterface, _TKBDBInterface,
                _TKBToolsWizard, _TKBVerifyWizard,
                _TKnowledgeBase, _TRepert, _TSearch, _TSelCons,
                _TShowForm, _TSolve, _TVirtControls,
                _KBDialog,
                _TKBDBGlossary, _TWordPred, _TWordAttr, _TWordNew, _TLexFunc, _TSCClasses,
                _TProjLex, _TDefTask, _TLPTests,
                _TMFDictionary);

    TVerifyType=( VNil, VTAdd, VTAddDict, VTAdition, VTConsult, VTDetachment,
                VTHtvForm, VTKBControl, VTKBEdit, VTKBEditor,VTKBInterface,
                VTKBFormInterface, VTKBDBInterface,
                VTKBToolsWizard, VTKBVerifyWizard,
                VTKnowledgeBase, VTRepert, VTSearch, VTSelCons,
                VTShowForm, VTSolve, VTVirtControls,
                VKBDialog,
                VTKBDBGlossary, VTWordPred, VTWordAttr, VTWordNew, VTLexFunc, VTSCClasses,
                VTProjLex, VTDefTask, VTLPTests,
                VTMFDictionary);

    TWizardComponent=class(TKBComponent)
    protected
    private
    public
      procedure Run; virtual;
      procedure Terminate; virtual;
    published
    end;

    TStepsCollectionItem=class(TKBCollectionItem)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
      procedure Run(Sender: TObject); virtual;
      constructor Create(ACollection: TCollection); override;
    published
    end;

    TStepsCollection=class(TKBOwnedCollection)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
      constructor Create(AOwner: TPersistent;ItemClass: TCollectionItemClass); virtual;{override;}
    published
    end;

    TKBToolCreateStepsItem=class(TStepsCollectionItem)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
      {x,y,}count: Integer;
      FToolType: TToolType;
      procedure Run(Sender: TObject); override;
      constructor Create(ACollection: TCollection); override;
    published
      property ToolType: TToolType read FToolType write FToolType;
    end;

    TKBVerifyStepsItem=class(TStepsCollectionItem)
    private
    { Private declarations }
    protected
    { Protected declarations }
    public
      {x,y,}count: Integer;
      FVerifyType: TVerifyType;
      FTaskName: String;
      procedure Run(Sender: TObject); override;
      constructor Create(ACollection: TCollection); override;
    published
      property VerifyType: TVerifyType read FVerifyType write FVerifyType;
      property TaskName: String read FTaskName write FTaskName;
    end;

    TKBToolsWizard=class(TWizardComponent)
    protected
    private
      FSteps: TStepsCollection;
    public
      {x,y,}count: Integer;
      LastKBInterface: TComponent;
      LastKBDBInterface: TComponent;
      LastKBFormInterface: TComponent;
      LastKB: TComponent;
      LastKBControl: TComponent;
      LastKBTool: TComponent;
      LastVirtControls: TComponent;
      LastShowForm: TComponent;
      LastProjLex: TComponent;
      LastDefTask: TComponent;
      LastGlossary: TAtDictionary;
      LastWordPred: TAtDictionary;
      LastWordAttr: TAtDictionary;
      LastWordNew: TAtDictionary;
      LastLexFunc: TAtDictionary;
      LastSCClasses: TAtDictionary;
      LastMFDictionary: TAtDictionary;
      LastKBVerifyWizard: TWizardComponent;
      FActiveDesign: Boolean;
      procedure SetActiveDesign(AValue: Boolean);
      procedure Run; override;
      procedure Design;
      {procedure Terminate; override;}
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
    published
      property ActiveDesign: Boolean read FActiveDesign write SetActiveDesign;
      property Steps: TStepsCollection read FSteps write FSteps;
    end;

    TKBVerifyWizard=class(TWizardComponent)
    private
      FSteps: TStepsCollection;
      TerminateFlag: Boolean;
    public
      procedure Run; override;
      procedure Terminate; override;
      procedure Verify(Protocol: TStrings); override;
      procedure RunVisual;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
    published
      property Steps: TStepsCollection read FSteps write FSteps;
    end;

    TWizardComponentEditor = class(TComponentEditor)
    protected
    public
      procedure Edit; override;
  end;


  TatKBCustomAction = class( TatAgentAction )
  end;

  TatKBWizardAction = class( TatKBCustomAction )
    private
      Finder: TTimer;
      FOnRun: TNotifyEvent;
      FAutomatic: Boolean;
      FForeignObjClassName : TComponentName;
      FCurrentToolClass: String;
      FExecuted: Boolean;
      procedure Find(Sender: TObject);
      procedure SetAutomatic(AValue: Boolean);
      procedure SetExecuted(AValue: Boolean);
    public
      procedure Run;override;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
    published
      property OnRun: TNotifyEvent read FOnRun write FOnRun;
      property Automatic: Boolean read FAutomatic write SetAutomatic;
      property ForeignObjClassName : TComponentName
        read FForeignObjClassName
        write FForeignObjClassName;
      property CurrentToolClass: String
         read FCurrentToolClass write FCurrentToolClass;
      property Executed: Boolean read FExecuted write SetExecuted;
   end;

  TatKBAction = class( TatKBCustomAction )
    private
      FKBTool: TKBTool;
    public
      procedure Run;override;
    published
      property KBTool: TKBTool read FKBTool write FKBTool;
  end;

procedure Register;

implementation
  uses Adition1, Consmain,
  HtvForm, Reper, SelCons, ShowForm,
  Solve, States1, Aspr95,
  EditKB;

procedure Register;
begin
  RegisterComponents('KB Wizard', [TKBToolsWizard, TKBVerifyWizard]);
  RegisterComponentEditor(TWizardComponent, TWizardComponentEditor);
  RegisterActions( '���������� ��', [TatKBWizardAction,TatKBAction], TatAgent);
end;

  procedure TWizardComponent.Run;
  begin
  end;

  procedure TWizardComponent.Terminate;
  begin
  end;

  procedure TWizardComponentEditor.Edit;
  begin
    TWizardComponent(Component).Run;
  end;

  constructor TStepsCollectionItem.Create(ACollection: TCollection);
  begin
    inherited Create(ACollection);
  end;

  constructor TKBToolCreateStepsItem.Create(ACollection: TCollection);
  begin
    inherited Create(ACollection);
  end;

  constructor TKBVerifyStepsItem.Create(ACollection: TCollection);
  begin
    inherited Create(ACollection);
  end;

  procedure TStepsCollectionItem.Run(Sender: TObject);
  begin
    ShowMessage('This is StepsCollectionItem');
  end;

  procedure TKBToolCreateStepsItem.Run(Sender: TObject);
  var
    Component: TComponent;
    PropInfo: PPropInfo;
    i: Integer;
  begin
    Component:=NIL;
    case FToolType of
      _TAdd: Component:=TAdd.Create(TKBToolsWizard(Sender).Owner);
      _TConsult: Component:=TConsult.Create(TKBToolsWizard(Sender).Owner);
      _TDetachment: Component:=TDetachment.Create(TKBToolsWizard(Sender).Owner);
      _THtvForm: Component:=THtvForm.Create(TKBToolsWizard(Sender).Owner);
      _TKBControl: Component:=TKBControl.Create(TKBToolsWizard(Sender).Owner);
      _TKBEdit: Component:=TKBEdit.Create(TKBToolsWizard(Sender).Owner);
      _TKBEditor: Component:=TKBEditor.Create(TKBToolsWizard(Sender).Owner);
      _TKBInterface: Component:=TKBInterface.Create(TKBToolsWizard(Sender).Owner);
      _TKBFormInterface: Component:=TKBFormInterface.Create(TKBToolsWizard(Sender).Owner);
      _TKBDBInterface: Component:=TKBDBInterface.Create(TKBToolsWizard(Sender).Owner);
      _TKBToolsWizard: Component:=TKBToolsWizard.Create(TKBToolsWizard(Sender).Owner);
      _TKBVerifyWizard: Component:=TKBVerifyWizard.Create(TKBToolsWizard(Sender).Owner);
      _TKnowledgeBase: Component:=TKnowledgeBase.Create(TKBToolsWizard(Sender).Owner);
      _TAdition: Component:=TAdition.Create(TKBToolsWizard(Sender).Owner);
      _TRepert: Component:=TRepert.Create(TKBToolsWizard(Sender).Owner);
      _TSelCons: Component:=TSelCons.Create(TKBToolsWizard(Sender).Owner);
      _TShowForm: Component:=TShowForm.Create(TKBToolsWizard(Sender).Owner);
      _TSolve: Component:=TSolve.Create(TKBToolsWizard(Sender).Owner);
      _TVirtControls: Component:=TVirtControls.Create(TKBToolsWizard(Sender).Owner);
      _KBDialog: Component:=TKBDialog.Create(TKBToolsWizard(Sender).Owner);
      _TKBDBGlossary: Component:=TKBDBGlossary.Create(TKBToolsWizard(Sender).Owner);
      _TWordPred: Component:=TWordPred.Create(TKBToolsWizard(Sender).Owner);
      _TWordAttr: Component:=TWordAttr.Create(TKBToolsWizard(Sender).Owner);
      _TWordNew: Component:=TWordNew.Create(TKBToolsWizard(Sender).Owner);
      _TLexFunc: Component:=TLexFunc.Create(TKBToolsWizard(Sender).Owner);
      _TSCClasses: Component:=TSCClasses.Create(TKBToolsWizard(Sender).Owner);
      _TProjLex: Component:=TProjLex.Create(TKBToolsWizard(Sender).Owner);
      _TDefTask: Component:=TDefTask.Create(TKBToolsWizard(Sender).Owner);
      _TLPTests: Component:=TLPTests.Create(TKBToolsWizard(Sender).Owner);
      _TMFDictionary: Component:=TMFDictionaryComponent.Create(TKBToolsWizard(Sender).Owner);
    end;
    if Component<>NIL then begin
      with Component do begin
        {Top:=x;
        Left:=y;}
        PropInfo:=GetPropInfo(Component.ClassInfo,'Name');
        SetStrProp(Component, PropInfo,
          Copy(Component.ClassName,2,Length(Component.ClassName))
          +IntToStr(count));
      end;
      if Component is TKBInterface then begin
        if not (Component is TKBDBInterface) then
          TKBToolsWizard(Sender).LastKBInterface:=Component;
      end;
      if Component is TKnowledgeBase then begin
        TKBToolsWizard(Sender).LastKB:=Component;
      end;
      if Component is TKBControl then begin
        TKBToolsWizard(Sender).LastKBControl:=Component;
      end;
      if Component is TKBTool then begin
        TKBToolsWizard(Sender).LastKBTool:=Component;
      end;
      if Component is TVirtControls then begin
        TKBToolsWizard(Sender).LastVirtControls:=Component;
      end;
      if Component is TShowForm then begin
        TKBToolsWizard(Sender).LastShowForm:=Component;
      end;
      if Component is TKBFormInterface then begin
        TKBToolsWizard(Sender).LastKBFormInterface:=Component;
        TKBFormInterface(Component).DesignMode:=dmDialog;
      end;
      if Component is TKBDBInterface then begin
        TKBToolsWizard(Sender).LastKBDBInterface:=Component;
      end;
      if Component is TProjLex then begin
        TKBToolsWizard(Sender).LastProjLex:=Component;
      end;
      if Component is TDefTask then begin
        TKBToolsWizard(Sender).LastDefTask:=Component;
      end;
      if Component is TKBDBGlossary then begin
        TKBToolsWizard(Sender).LastGlossary:=
          TKBDBGlossary(Component);
      end;
      if Component is TWordPred then begin
        TKBToolsWizard(Sender).LastWordPred:=
          TWordPred(Component);
      end;
      if Component is TWordAttr then begin
        TKBToolsWizard(Sender).LastWordAttr:=
          TWordAttr(Component);
      end;
      if Component is TWordNew then begin
        TKBToolsWizard(Sender).LastWordNew:=
          TWordNew(Component);
      end;
      if Component is TLexFunc then begin
        TKBToolsWizard(Sender).LastLexFunc:=
          TLexFunc(Component);
      end;
      if Component is TSCClasses then begin
        TKBToolsWizard(Sender).LastSCClasses:=
          TSCClasses(Component);
      end;
      if Component is TMFDictionaryComponent then begin
        TKBToolsWizard(Sender).LastMFDictionary:=
          TMFDictionaryComponent(Component);
      end;
      if Component is TKBVerifyWizard then begin
        TKBToolsWizard(Sender).LastKBVerifyWizard:=
        TWizardComponent(Component);
        for i:=1 to Ord(High(TVerifyType)) do begin
          TKBVerifyStepsItem(TKBVerifyWizard(Component).
            Steps.Add).VerifyType:=TVerifyType(i);
          with TKBVerifyStepsItem(TKBVerifyWizard(Component).Steps.Items[i-1]) do
          case TVerifyType(i) of
//            VTSolve: TKBVerifyStepsItem(TKBVerifyWizard(Component).Steps.Items[i-1]).TaskName:='����������� �������� ������';

            VTAdd: TaskName:='����������� ���������� ����������';
            VTAddDict: TaskName:='';
            VTConsult: TaskName:='';
            VTDetachment: TaskName:='';
            VTHtvForm: TaskName:='����������� �������������� ��������';
            VTKBControl: TaskName:='����������� "�������� �����"';
            VTKBEdit: TaskName:='';
            VTKBEditor: TaskName:='���������� �������� ���� ������';
            VTKBInterface: TaskName:='';
            VTKBFormInterface: TaskName:='����������� ��������� � ��������� �������';
            VTKBDBInterface: TaskName:='����������� ��������� � ������ ������';
            VTKBToolsWizard: TaskName:='';
            VTKBVerifyWizard: TaskName:='';
            VTKnowledgeBase: TaskName:='����������� ������� ������';
            VTAdition: TaskName:='����������� �������� ����������� � ���������';
            VTRepert: TaskName:='����������� ��������� ������������ �������';
            VTSearch: TaskName:='';
            VTSelCons: TaskName:='����������� ������������ �������� ������';
            VTShowForm: TaskName:='����������� �������� ������������';
            VTSolve: TaskName:='����������� �������� ������';
            VTVirtControls: TaskName:='';
            VKBDialog: TaskName:='';
            VTKBDBGlossary: TaskName:='';
            VTWordPred: TaskName:='';
            VTWordAttr: TaskName:='';
            VTWordNew: TaskName:='';
            VTLexFunc: TaskName:='';
            VTSCClasses: TaskName:='';
            VTProjLex: TaskName:='����������� ���������� ������� ���������� ���������';
            VTDefTask: TaskName:='����������� ����������� ���� �������� ������';
            VTLPTests: TaskName:='';
            VTMFDictionary: TaskName:='';
          end;
        end;
      end;
   end;
  end;

  procedure TKBToolsWizard.Design;
  begin
//    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBInterface;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBDBGlossary;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TWordPred;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TWordAttr;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TWordNew;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TLexFunc;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TSCClasses;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TMFDictionary;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TProjLex;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TDefTask;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TLPTests;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBDBInterface;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBFormInterface;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKnowledgeBase;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBControl;
//    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TDetachment;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TVirtControls;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TShowForm;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_THtvForm;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TAdition;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TAdd;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TSelCons;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TConsult;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TSolve;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBEditor;
    TKBToolCreateStepsItem(Steps.Add).FToolType:=_TKBVerifyWizard;
  end;


  constructor TStepsCollection.Create(AOwner: TPersistent;ItemClass: TCollectionItemClass);
  begin
    inherited Create(AOwner,ItemClass);
  end;

  constructor TKBToolsWizard.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    FSteps:=TStepsCollection.Create(Self,TKBToolCreateStepsItem{TStepsCollectionItem});
    FActiveDesign:=False;
  end;

  destructor TKBToolsWizard.Destroy;
  begin
    FSteps.Destroy;
    inherited Destroy;
  end;

  procedure TKBToolsWizard.Run;
  var
    i,j: Integer;
    Cmpn: TComponent;
  begin
   {x:=x+32;}
    with TForm(Self.Owner) do begin
      for i:=1 to ComponentCount do begin
        Cmpn:=Components[i-1];
        if Cmpn is TKBInterface then begin
          if Cmpn is TKBDBInterface then
             LastKBDBInterface:=Cmpn else
          if Cmpn is TKBFormInterface then
            LastKBFormInterface:=Cmpn
          else
            LastKBInterface:=Cmpn;
        end;
        if Cmpn is TKnowledgeBase then begin
          LastKB:=Cmpn;
        end;
        if Cmpn is TKBControl then begin
          LastKBControl:=Cmpn;
        end;
        if Cmpn is TKBTool then begin
          LastKBTool:=Cmpn;
        end;
        if Cmpn is TShowForm then begin
          LastShowForm:=Cmpn;
        end;
        if Cmpn is TProjLex then begin
          LastProjLex:=Cmpn;
        end;
        if Cmpn is TKBVerifyWizard then begin
          LastKBVerifyWizard:=TWizardComponent(Cmpn);
        end;
        if Cmpn is TWordPred then begin
          LastWordPred:=TAtDictionary(Cmpn);
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+PRED_FILE;
        end;
        if Cmpn is TWordAttr then begin
          LastWordAttr:=TAtDictionary(Cmpn);
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+ATTR_FILE;
        end;
        if Cmpn is TWordNew then begin
          LastWordNew:=TAtDictionary(Cmpn);
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+NEW_FILE;
        end;
        if Cmpn is TLexFunc then begin
          LastLexFunc:=TAtDictionary(Cmpn);
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+LEX_FILE;
        end;
        if Cmpn is TSCClasses then begin
          LastLexFunc:=TAtDictionary(Cmpn);
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+KAT_FIlE;
        end;
      end;
    end;

    for i:=1 to Steps.Count do begin
      {y:=y+50;}
      count:=count+1;
      {TKBToolCreateStepsItem(Steps.Items[i-1]).x:=x;
      TKBToolCreateStepsItem(Steps.Items[i-1]).y:=y;}
      TKBToolCreateStepsItem(Steps.Items[i-1]).count:=count;
      TKBToolCreateStepsItem(Steps.Items[i-1]).Run(Self);
    end;

    with TForm(Self.Owner) do begin
      for i:=1 to ComponentCount do begin
        Cmpn:=Components[i-1];
        if Cmpn is TKnowledgeBase then begin
          if not Assigned(TKnowledgeBase(Cmpn).KBInterface) then
            TKnowledgeBase(Cmpn).KBInterface:=TKBInterface(LastKBInterface);
          TKnowledgeBase(Cmpn).MFDictionary:=TMFDictionaryComponent(LastMFDictionary);
        end;
        if Cmpn is TKBControl then begin
          if not Assigned(TKBControl(Cmpn).KB) then
            TKBControl(Cmpn).KB:=TKnowledgeBase(LastKB);
        end;
        if Cmpn is TKBTool then begin
          if not Assigned(TKBTool(Cmpn).KBControl) then
            TKBTool(Cmpn).KBControl:=TKBControl(LastKBControl);
        end;
        if Cmpn is TShowForm then begin
          if not Assigned(TShowForm(Cmpn).VirtControls) then
            TShowForm(Cmpn).VirtControls:=TVirtControls(LastVirtControls);
            TShowForm(Cmpn).KBLPTool:=TKBLPTool(LastProjLex);
        end;
        if Cmpn is TAdition then begin
          if not Assigned(TAdition(Cmpn).VerifyWizard) then
            TAdition(Cmpn).VerifyWizard:=LastKBVerifyWizard;
        end;
        if Cmpn is TSelCons then begin
          if not Assigned(TSelCons(Cmpn).ProjLex) then
            TSelCons(Cmpn).ProjLex:=TProjLex(LastProjLex);
        end;
        if Cmpn is TVirtControls then begin
          if not Assigned(TVirtControls(Cmpn).KBControl) then
            TVirtControls(Cmpn).KBControl:=TKBControl(LastKBControl);
        end;
        if Cmpn is TKBEdit then begin
          if not Assigned(TKBEdit(Cmpn).ShowForm) then
            TKBEdit(Cmpn).ShowForm:=TShowForm(LastShowForm);
        end;
        if Cmpn is TKBLPTool then begin
          TKBLPTool(Cmpn).KBControl:=TKBControl(LastKBControl);
          TKBLPTool(Cmpn).WordPred:=LastWordPred;
          TKBLPTool(Cmpn).WordAttr:=LastWordAttr;
          TKBLPTool(Cmpn).LexFunc:=LastLexFunc;
          TKBLPTool(Cmpn).SCClasses:=LastSCClasses;
          TKBLPTool(Cmpn).MFDictionary:=LastMFDictionary;
          if Cmpn is TProjLex then begin
            TProjLex(Cmpn).WordNew:=LastWordNew;
          end;
          if Cmpn is TDefTask then begin
            TDefTask(Cmpn).ProjLex:=TProjLex(LastProjLex);
          end;
          if Cmpn is TLPTests then begin
            TLPTests(Cmpn).DefTask:=TDefTask(LastDefTask);
          end;
        end;
        if Cmpn is TWordPred then begin
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+PRED_FILE;
        end;
        if Cmpn is TWordAttr then begin
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+ATTR_FILE;
        end;
        if Cmpn is TWordNew then begin
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+NEW_FILE;
        end;
        if Cmpn is TLexFunc then begin
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+LEX_FILE;
        end;
        if Cmpn is TSCClasses then begin
          TAtDictionary(Cmpn).FileName:=GetFilePath(FilePath)+KAT_FIlE;
        end;
        if Cmpn is TDetachment then begin
          for j:=1 to ComponentCount do begin
          LastKBTool:=Components[j-1];
          if Components[j-1] is TKBTool then
            with TDetachment(Cmpn) do begin
              if not Assigned(Add) then if LastKBTool is TAdd then
                Add:=TAdd(LastKBTool);
              if not Assigned(Adition) then if LastKBTool is TAdition then
                Adition:=TAdition(LastKBTool);
              if not Assigned(Consult) then if LastKBTool is TConsult then
                Consult:=TConsult(LastKBTool);
              if not Assigned(HtvForm) then if LastKBTool is THtvForm then
                HtvForm:=THtvForm(LastKBTool);
              if not Assigned(Repert) then if LastKBTool is TRepert then
                Repert:=TRepert(LastKBTool);
              if not Assigned(SelCons) then if LastKBTool is TSelCons then
                SelCons:=TSelCons(LastKBTool);
              if not Assigned(ShowForm) then if LastKBTool is TShowForm then
                ShowForm:=TShowForm(LastKBTool);
              if not Assigned(Solve) then if LastKBTool is TSolve then
                Solve:=TSolve(LastKBTool);
            end;
          end;
        end;

        {if Cmpn.ClassName='TAdd' then;
        if Cmpn.ClassName='TAddDict' then;
        if Cmpn.ClassName='TAdition' then;
        if Cmpn.ClassName='TConsult' then;
        if Cmpn.ClassName='TDetachment' then;
        if Cmpn.ClassName='THtvForm' then;
        if Cmpn.ClassName='TKBControl' then;
        if Cmpn.ClassName='TKBEdit' then;
        if Cmpn.ClassName='TKBInterface' then;
        if Cmpn.ClassName='TKBToolsWizard' then;
        if Cmpn.ClassName='TKnowledgeBase' then;
        if Cmpn.ClassName='TRepert' then;
        if Cmpn.ClassName='TSearch' then;
        if Cmpn.ClassName='TSelCons' then;
        if Cmpn.ClassName='TShowForm' then;
        if Cmpn.ClassName='TSolve' then;
        if Cmpn.ClassName='TVirtControls' then;}
      end;

      if Assigned(LastKBFormInterface) then begin
        TKBFormInterface(LastKBFormInterface).Form.Designer:=TAtForm(Self.Owner);
        TKBFormInterface(LastKBFormInterface).Design(LastKB);
      end;

      if Assigned(LastKBDBInterface) then begin
        TKBDBInterface(LastKBDBInterface).Glossary:=TKBDBGlossary(LastGlossary);
      end;

    end;

  end;

  procedure TKBToolsWizard.SetActiveDesign(AValue: Boolean);
  begin
    if AValue then begin
      Steps.Clear;
      Design;
      FActiveDesign:=False;
    end;
  end;

  procedure TKBVerifyWizard.Run;
  var
    i: Integer;
  begin
    Self.Verify(NIL);
    TerminateFlag:=True;
    for i:=1 to Steps.Count do begin
      TKBVerifyStepsItem(Steps.Items[i-1]).Run(NIL);
      if TerminateFlag then break;
    end;
  end;

  procedure TKBVerifyWizard.Terminate;
  begin
    TerminateFlag:=False;
  end;

  procedure TKBVerifyWizard.Verify;
  begin
    inherited Verify(NIL);
  end;

  procedure TKBVerifyWizard.RunVisual;
  begin
    {}
  end;

  constructor TKBVerifyWizard.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    FSteps:=TStepsCollection.Create(Self,TKBVerifyStepsItem);
  end;

  destructor TKBVerifyWizard.Destroy;
  begin
    FSteps.Destroy;
    inherited Destroy;
  end;

  procedure TKBVerifyStepsItem.Run(Sender: TObject);
  var
    ClassRefr: TPersistentClass;
    i,j: Integer;
  begin
    ClassRefr:=NIL;
    case VerifyType of
      VTAdd: ClassRefr:=FindClass('TAdd');
      VTAddDict: ClassRefr:=FindClass('TAddDict');
      VTAdition: ClassRefr:=FindClass('TAdition');
      VTConsult: ClassRefr:=FindClass('TConsult');
      VTDetachment: ClassRefr:=FindClass('TDetachment');
      VTHtvForm: ClassRefr:=FindClass('THtvForm');
      VTKBControl: ClassRefr:=FindClass('TKBControl');
      VTKBEdit: ClassRefr:=FindClass('TKBEdit');
      VTKBEditor: ClassRefr:=FindClass('TKBEditor');
      VTKBInterface: ClassRefr:=FindClass('TKBInterface');
      VTKBFormInterface: ClassRefr:=FindClass('TKBFormInterface');
      VTKBDBInterface: ClassRefr:=FindClass('TKBDBInterface');
      VTKBToolsWizard: ClassRefr:=FindClass('TKBToolsWizard');
      VTKBVerifyWizard: ClassRefr:=FindClass('TKBToolsWizard');
      VTKnowledgeBase: ClassRefr:=FindClass('TKnowledgeBase');
      VTRepert: ClassRefr:=FindClass('TRepert');
      VTSearch: ClassRefr:=FindClass('TSearch');
      VTSelCons: ClassRefr:=FindClass('TSelCons');
      VTShowForm: ClassRefr:=FindClass('TShowForm');
      VTSolve: ClassRefr:=FindClass('TSolve');
      VTVirtControls: ClassRefr:=FindClass('TVirtControls');
      VKBDialog: ClassRefr:=FindClass('TKBDialog');
      VTKBDBGlossary: ClassRefr:=FindClass('TKBDBGlossary');
      VTWordPred: ClassRefr:=FindClass('TWordPred');
      VTWordAttr: ClassRefr:=FindClass('TWordAttr');
      VTWordNew: ClassRefr:=FindClass('TWordNew');
      VTLexFunc: ClassRefr:=FindClass('TLexFunc');
      VTSCClasses: ClassRefr:=FindClass('TSCClasses');
      VTMFDictionary: ClassRefr:=FindClass('TMFDictionaryComponent');
    end;
    if ClassRefr<>NIL then begin
      with Screen do begin
        for i:=1 to FormCount do begin
          with Forms[i-1] do begin
            for j:=1 to ComponentCount do begin
              if Components[j-1].ClassType=ClassRefr then begin
                if Sender is TStrings then
                  TKBComponent(Components[j-1]).Verify(TStrings(Sender)) else
                  TKBComponent(Components[j-1]).Verify(NIL);
              end;
            end;
          end;
        end;
      end;
    end;
  end;

  procedure TATKBWizardAction.Run;
  begin
    if not Automatic then
     if Assigned(FOnRun) then
       FOnRun(nil);
  end;

  procedure TATKBWizardAction.SetAutomatic(AValue: Boolean);
  begin
    if AValue then begin
    end else begin
    end;
    FAutomatic:=AValue;
  end;

  constructor TATKBWizardAction.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    Finder:=TTimer.Create(Self);
    Finder.Interval:=500;
    Finder.OnTimer:=Find;
    Executed:=False;
  end;

  destructor TATKBWizardAction.Destroy;
  begin
    Finder.Destroy;
    inherited Destroy;
  end;

  procedure TATKBWizardAction.Find(Sender: TObject);
  var
    i,j: Integer;
    ClassRef: TClass;
  begin
    if FAutomatic and
    not Executed then begin
      if (FForeignObjClassName<>'') then begin
        with Screen do begin
          for i:=1 to FormCount do begin
            with Forms[i-1] do begin
              ClassRef:=Forms[i-1].ClassType;
              if UpperCase(ClassRef.ClassName)=UpperCase(FForeignObjClassName) then begin
                Executed:=True;
                if Assigned(FOnRun) then
                  FOnRun(nil);
                break;
              end;
              for j:=1 to ComponentCount do begin
                ClassRef:=Components[j-1].ClassType;
                if UpperCase(ClassRef.ClassName)=UpperCase(FForeignObjClassName) then begin
                  Executed:=True;
                  if Assigned(FOnRun) then
                    FOnRun(nil);
                  break;
                end;
              end;
              if Executed then break;
            end;
          end;
        end;
      end else
      if (FCurrentToolClass<>'') then begin
        with Screen do begin
          for i:=1 to FormCount do begin
            with Forms[i-1] do begin
              for j:=1 to ComponentCount do begin
                if Components[j-1] is TKBControl then begin
                  if UpperCase(TKBControl(Components[j-1]).CurrentToolClass)=
                    UpperCase(CurrentToolClass) then begin
                    Executed:=True;
                    if Assigned(FOnRun) then
                      FOnRun(nil);
                    break;
                  end;
                end;
              end;
              if Executed then break;
            end;
          end;
        end;
      end else begin
        Executed:=True;
        if Assigned(FOnRun) then
          FOnRun(nil);
      end;
    end;
  end;

  procedure TATKBWizardAction.SetExecuted(AValue: Boolean);
  begin
    Finder.Enabled:=not AValue;
    FExecuted:=AValue;
  end;

  procedure TATKBAction.Run;
  begin
     If Assigned(FKBTool) then
       FKBTool.Run(nil);
  end;

  initialization

  finalization

  UnRegisterActions( [TatKBWizardAction,TatKBAction]);

end.
