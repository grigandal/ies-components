unit KBTypes;
interface

uses
  Windows,
  Classes,
  SysUtils,
  VCL.StdCtrls,
  VCL.ComCtrls,
  VCL.ExtCtrls,
  VCL.Controls,
  VCL.Dialogs,
  VCL.Forms,
  VCL.Menus,
  VCL.Buttons,
  VCL.Mask,
  VCL.Grids,
  dbTables,
  Aspr95,TypInfo,Paths,KBCompon;

{$R KBTypes.res}

const
  Inferenzmaschine:String='��-��������';

type

  TStage {From PlaningM:} = (etUnknown, etAnalize, etDesign, etRealization, etTesting);

  TSessionItem=class(TKBCollectionItem)
  public
//    FKF: TPersistent;
    FSessionName: String;
    FSessionID: Integer;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(KBCollectionItem: TKBCollectionItem); override;
  published
//    property KF: TPersistent read FKF write FKF;
    property SessionName: String read FSessionName write FSessionName;
    property SessionID: Integer read FSessionID write FSessionID;
  end;

  TKBControl = class(TKBComponent)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
      {����� ������}
      Mode: Boolean; {���������� (True) ��� � ������� ��������� (False)}

      {������ �� �� ������� ���}
      FProblemName: String; {��� ����������� ��}
      FDatabase: TDatabase; {�� ��� ���������� �� ��� ������}
      ProjectDict: TStringList; {������� ���������� ���������}

      {������, ��� ������������}
      FCurrentToolClass: String; { ������� ��������� }
      FStage: TStage; {������� ������ ���������� ����� ������� ���������� ��}
      FLastMethod: String;

      {������, ������������ ����������� ��������������}
      ProjectData: TList; {������ ������������� ��������� � �� �������}

      {������ ����� ����������������}
      DictName: String; {��� �������, ����������� ������� ���������� ��������� �
                        ������� ��������}
      FTypeOfTask: String; {��� �������� ������}
      FCountOfSessions: Integer; {���������� ���������� ������� ����������������}
      FIsProtokol: Boolean; {����������� �� �������� ����������������}
      Fcur_n_obj:  Integer;
      Fcur_n_attr:  Integer;
      Fcur_n_type:  Integer;
      Fcur_n_znach_if:  Integer;
      Fcur_n_znach_then:  Integer;
      Fcur_n_znach_else:  Integer;
      Fcur_form:  String;
      FCurrentSessionID: Integer;
      FSessions: TKBOwnedCollection;
      FCurrentStepNumber: Integer;

      {������ ����������������}
      FIsKnowledgeField: Boolean; {����������� �� ���� �� �������� ���� ������}
      FIsUnited: Boolean; {�������� �� ������ �������� ����������� �����������}
      FIsTested: Boolean; {��������� �� ������������ ���� ������}

      {������ ������������ ��}
      FNameOfSolver: String; {��� ������������� ��������}
      FIsKb: Boolean; {������������ �� �� ��� �� ��������}
      FIsExplanation: Boolean; {������������ �� ����������}
      FStyle: Integer; {����� ��������� ������}
      FCalcMethod: TCalcMethod; {����� ������ �������}

      {������ ������������ ��}
      FCountOfConsult: Integer; {���������� ����������� ������������}
      FIsEdited: Boolean; {���������� �� �������� ��� ��������� ��
                          ��� �� ���������}
      FKB: TKnowledgeBase;       { ����������� ���� ������ � ���� ������ }

      {��������� ��������}
      TypeOfEdit: Integer;     { ��� ��������������  }

      procedure Edit;
      procedure SetSessionID(Value: Integer);

      published

      property ProblemName: String
         read FProblemName write FProblemName;
      property Database: TDatabase
         read FDatabase write FDatabase;
      property CurrentToolClass: String
         read FCurrentToolClass write FCurrentToolClass;
      property Stage : TStage read FStage write FStage;
      property LastMethod: String read FLastMethod write FLastMethod;
      property TypeOfTask: String
         read FTypeOfTask write FTypeOfTask;
      property CountOfSessions: Integer
         read FCountOfSessions write FCountOfSessions;
      property IsProtokol: Boolean
         read  FIsProtokol write FIsProtokol;
      property cur_n_obj:  Integer
         read Fcur_n_obj write Fcur_n_obj;
      property cur_n_attr:  Integer
         read Fcur_n_attr write Fcur_n_attr;
      property cur_n_type:  Integer
         read  Fcur_n_type write Fcur_n_type;
      property cur_n_znach_if:  Integer
         read  Fcur_n_znach_if write Fcur_n_znach_if;
      property cur_n_znach_then:  Integer
         read  Fcur_n_znach_then write Fcur_n_znach_then;
      property cur_n_znach_else:  Integer
         read Fcur_n_znach_else write Fcur_n_znach_else;
      property cur_form:  String
         read Fcur_form write Fcur_form;
      property CurrentSessionID: Integer
         read FCurrentSessionID write SetSessionID;
      property Sessions: TKBOwnedCollection
         read FSessions write FSessions;
      property CurrentStepNumber: Integer
         read FCurrentStepNumber write FCurrentStepNumber;
      property IsKnowledgeField: Boolean
         read FIsKnowledgeField write FIsKnowledgeField;
      property IsUnited: Boolean
         read FIsUnited write FIsUnited;
      property IsTested: Boolean
         read FIsTested write FIsTested;
      property NameOfSolver: String
         read FNameOfSolver write FNameOfSolver;
      property IsKb: Boolean
         read FIsKb write FIsKb;
      property IsExplanation: Boolean
         read FIsExplanation write FIsExplanation;
      property Style: Integer
         read FStyle write FStyle;
      property CalcMethod: TCalcMethod
         read FCalcMethod write FCalcMethod;
      property CountOfConsult: Integer
         read FCountOfConsult write FCountOfConsult;
      property IsEdited: Boolean
         read FIsEdited write FIsEdited;

      property KB: TKnowledgeBase
         read FKB write FKB;

      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure Load(Name: String);
      procedure Save(Name: String);

      {procedure InitKB(AOwner: TComponent);}
      procedure ClearKB(AOwner: TComponent);
      {procedure DestroyKB;}
      procedure OpenKB;
      function GetMaxSessionID: Integer;
  end;

   TMethodsSet = class(TKBComponent)
   end;

   TKBToolItem=class;
   TKBTool=class;
   TKBLPTool=class;

   TVirtControls = class(TMethodsSet)
      public
      VirtLabel:     TLabel;
      VirtEdit:      TEdit;
      VirtMemo:      TMemo;
      VirtComboBox:  TComboBox;
      VirtButton:    TButton;
      VirtScrollBar: TScrollBar;
      VirtTrackBar:  TTrackBar;
      VirtPanel:     TPanel;
      VirtOther:     TObject;
      Labels:        TList;
      Edits:         TList;
      Memos:         TList;
      ComboBoxes:    TList;
      Buttons:       TList;
      ScrollBars:    TList;
      TrackBars:     TList;
      Panels:        TList;
      Others:        TList;
      PanelFlag:     Boolean;

      FKBControl: TKBControl;

      LastQuestion: String;
      LastAnswer: String;

      published

      property KBControl: TKBControl read FKBControl write FKBControl;

      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure Clear;

      procedure CREATE_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_COPY_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ADD_IF_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ADD_THEN_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure NEXT_IF_NOT_FIRST(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_NEXT_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_ATTR_FOR_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure COPY_D_TO_O(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure GET_T_COMMENT_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SAY_T_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_T_COMMENT_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure GET_T_LIST_ZNACH(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_IF(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_THEN(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure LOAD_O_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure LOAD_A_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure LOAD_T_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure LOAD_D_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure DEF_TYPE_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure FIND_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SELECT_IN_LIST_ZNACH(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SAY_TYPES(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_PRIV_N_IF(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure GOSUB(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure RETURN(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_INPUT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CREATE_OUTPUT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ALT_IF_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ALT_THEN_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_ELSE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ADD_OBJ_TO_CONN(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_LIST(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_TYPE_COMBO(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_TYPE_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_OBJECT_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_ATTR_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_RULE_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_FACTOR(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SET_SKIP(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure SKIP(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure FIND_ATTR_OF_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure CONSTRUCTS(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_ATTR_CONTROL(value1,value2,value3: String; KBLPTool: TKBLPTool);
      procedure ASSIGN_MF_LIST(value1,value2,value3: String; KBLPTool: TKBLPTool);
    end;

  _TVariants = array of Variant;

  TKBToolItem=class(TKBComponent)
   private
     {}
   protected
     {}
   public
     procedure Edit;    virtual;
     procedure Verify(Protocol: TStrings);  override;
     procedure Compile; virtual;
     function  Run( Sender: TObject{Parms: array of Variant}): TObject{Variant}; virtual;
   published
     {}
   end;

  TKBTool=class(TKBToolItem)
   private
     FSaveToolClass: String;
     FSaveStage: TStage;
   protected
     {}
   public
     FActive: Boolean;
     FKBControl: TKBControl;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function  Run( Sender: TObject{Parms: array of Variant}): TObject{Variant}; override;
    procedure SetActive(Value: Boolean);
    procedure SetStage(Value: TStage);
    procedure RestStage;
   published
     property Active: Boolean read FActive write SetActive;
     property KBControl: TKBControl read FKBControl write FKBControl;
     property SaveToolClass: String  read FSaveToolClass write FSaveToolClass;
     property SaveStage: TStage read FSaveStage write FSaveStage;
   end;

  TKBLPTool=class(TKBTool)
    public
      FWordPred: TAtDictionary;
      FWordAttr: TAtDictionary;
      FLexFunc: TAtDictionary;
      FSCClasses: TAtDictionary;
      FMFDictionary: TAtDictionary;
      constructor Create(AOwner: TComponent); override;
    published
      property WordPred: TAtDictionary read FWordPred write FWordPred;
      property WordAttr: TAtDictionary read FWordAttr write FWordAttr;
      property LexFunc: TAtDictionary read FLexFunc write FLexFunc;
      property SCClasses: TAtDictionary read FSCClasses write FSCClasses;
      property MFDictionary: TAtDictionary read FMFDictionary write FMFDictionary;
  end;

  TRunFunc=function ( Sender: TObject{Parms:array of Variant} ): TObject;

  TExternalType=(EXE,DLL);

  TInterfaceItem=interface
  end;

  TExternal=class(TPersistent)
  protected
    Handle: THandle;
  public
    FExternalName: String;
    FActive: Boolean;
    FFileName: String;
    FFuncName: String;
    FExternalType: TExternalType;
    FForwardInference: Boolean;
    FBackwardInference: Boolean;
    FMixedInference: Boolean;
    FFuzzyInference: Boolean;
    FLeftFactor: Boolean;
    FRightFactor: Boolean;
    FAccuracy: Boolean;
    FFormatName: String;
    Run: TRunFunc;
    constructor Create{(AOwner: TComponent); override};
    destructor Destroy; override;
    procedure SetActive(Value: Boolean);
    function RunExe( Sender: TObject{Parms:array of Variant} ): TObject{Variant};
    function RunDllFunc( Sender: TObject{Parms:array of Variant} ): TObject{Variant};
  published
    property ExternalName: String read FExternalName write FExternalName;
    property Active: Boolean read FActive write SetActive;
    property FileName: String read FFileName write FFileName;
    property FuncName: String read FFuncName write FFuncName;
    property ExternalType: TExternalType read FExternalType write FExternalType;
    property ForwardInference: Boolean read FForwardInference write FForwardInference;
    property BackwardInference: Boolean read FBackwardInference write FBackwardInference;
    property MixedInference: Boolean read FMixedInference write FMixedInference;
    property FuzzyInference: Boolean read FFuzzyInference write FFuzzyInference;
    property LeftFactor: Boolean read FLeftFactor write FLeftFactor;
    property RightFactor: Boolean read FRightFactor  write FRightFactor;
    property Accuracy: Boolean read FAccuracy write FAccuracy;
    property FormatName: String read FFormatName write FFormatName;
  end;

  TInterpreter=class(TKBTool)
  end;

  TCustomKBEditor=class(TKBTool)
  end;

{  TKBControlEditor=class(TComponentEditor)
    public
      FKBControl: TKBControl;
      procedure Edit; override;
      constructor Create(AComponent: TComponent;
                         ADesigner: IFormDesigner); override;
    published
      property KBControl: TKBControl read FKBControl write FKBControl;
  end;

  TKBToolEditor = class(TComponentEditor)
    protected

    public
      procedure Edit; override;
  end;}

  procedure _RegistryMethod( aClass:TClass; aMethod:Pointer; aMethodName:String; ArgsType:array of PTypeInfo; ARetType:PTypeInfo );
  procedure _RegistryFunction( aFunction:Pointer; aFuncName:String; ArgsType:array of PTypeInfo; ARetType:PTypeInfo );

procedure Register;

implementation
{$G+}
uses
  Script, AsprEdit;

procedure Register;
begin
  RegisterComponents('Engine', [TKBControl{, TVirtControls}]);
  RegisterClasses([TKBControl, TVirtControls]);
//  RegisterComponentEditor(TKBTool, TKBToolEditor);
end;

  constructor TKBControl.Create(AOwner: TComponent);
  begin
       Inherited Create(AOwner);
       RegistryTestableProperty('KB');
       {FVirtControls:=TVirtControls.Create;}

      {����� ������}
      Mode:=False;

      {������ �� �� ������� ���}
      ProblemName:='BigTest';
      {DBName:=StrPas('');}
//      ProjectDict:=NIL;
      ProjectDict:=TStringList.Create;

      {������ ��� ������������}
      CurrentToolClass:='TSetTask';

      {������, ������������ ����������� ��������������}
      ProjectData:=TList.Create;

      {������ ����� ����������������}
      DictName:=StrPas('');
      TypeOfTask:= '�����������';
      CountOfSessions:=0;
      CurrentStepNumber:=0;
      IsProtokol:=False;
      cur_n_znach_if:=0;
      cur_n_znach_then:=0;
      cur_n_znach_else:=0;
      Sessions:=TKBOwnedCollection.Create(Self,TSessionItem);

      {������ ����������������}
      IsKnowledgeField:=False;
      IsUnited:=False;
      IsTested:=False;

      {������ ������������ ��}
      NameOfSolver:=Inferenzmaschine;
      IsKb:=False;
      IsExplanation:=False;
      Style:=2;
      CalcMethod:=Demster;

      {������ ������������ ��}
      {InitKB(AOwner);}
      CountOfConsult:=0;
      IsEdited:=False;
      if KB<>NIL then
        KB.ProblemName:=ProblemName;

      TypeOfEdit:=0;
  end;

  destructor TKBControl.Destroy;
  begin
     {DestroyKB;}
     {FVirtControls.Destroy;}
     if Assigned(ProjectDict) then begin
       ProjectDict.Clear;
       ProjectDict.Destroy;
     end;
     ProjectData.Clear;
     ProjectData.Destroy;
     Sessions.Destroy;
     Inherited Destroy;
  end;


  procedure TKBControl.Load(Name: String);
  var
    lrFile: TextFile;
    V: Integer;
    S: string;
  begin
      System.Assign(lrFile,Name);
      Reset(lrFile);

      {����� ������}
      Readln(lrFile,V);
      if V=0 then Mode:=False
             else Mode:=True;

      {������ �� �� ������� ���}
      Readln(lrFile,S); ProblemName:=S;
      KB.ProblemName:=ProblemName;
      {Readln(lrFile,DBName);}

      {������, ������������ �������������}
      Readln(lrFile,FCurrentToolClass);

      {������, ������������ ����������� ��������������}
      {ProjectData;}

      {������ ����� ����������������}
      Readln(lrFile,DictName);
      Readln(lrFile,FTypeOfTask);
      Readln(lrFile,FCountOfSessions);
      Readln(lrFile,V);
      if V=0 then IsProtokol:=False
             else IsProtokol:=True;
      Readln(lrFile,Fcur_n_obj);
      Readln(lrFile,Fcur_n_attr);
      Readln(lrFile,Fcur_n_type);
      Readln(lrFile,Fcur_n_znach_if);
      Readln(lrFile,Fcur_n_znach_then);
      Readln(lrFile,Fcur_n_znach_else);
      Readln(lrFile,Fcur_form);

      {������ ����������������}
      Readln(lrFile,V);
       if V=0 then FIsKnowledgeField:=False
             else FIsKnowledgeField:=True;
      Readln(lrFile,V);
      if V=0 then FIsUnited:=False
             else FIsUnited:=True;
      Readln(lrFile,V);
      if V=0 then FIsTested:=False
             else FIsTested:=True;

      {������ ������������ ��}
      Readln(lrFile,FNameOfSolver);
      Readln(lrFile,V);
      if V=0 then FIsKb:=False
             else FIsKb:=True;
      Readln(lrFile,V);
      if V=0 then FIsExplanation:=False
             else FIsExplanation:=True;

      {������ ������������ ��}
      Readln(lrFile,FCountOfConsult);
      Readln(lrFile,V);
      if V=0 then IsEdited:=False
             else IsEdited:=True;

      Close(lrFile);

  end;

  procedure TKBControl.Save(Name: String);
  var
    F: TextFile;
   begin
      System.Assign(F,Name);
      Rewrite(F);

      {����� ������}
       if not Mode then Writeln(F,0)
                   else Writeln(F,1);

      {������ �� �� ������� ���}
      Writeln(F,ProblemName);
      {Writeln(F,DBName);}

      {������, ������������ �������������}
      Writeln(F,CurrentToolClass);

      {������, ������������ ����������� ��������������}
      {ProjectData;}

      {������ ����� ����������������}
      Writeln(F,DictName);
      Writeln(F,TypeOfTask);
      Writeln(F,CountOfSessions);
      if not IsProtokol then Writeln(F,0)
                        else Writeln(F,1);
      Writeln(F,cur_n_obj);
      Writeln(F,cur_n_attr);
      Writeln(F,cur_n_type);
      Writeln(F,cur_n_znach_if);
      Writeln(F,cur_n_znach_then);
      Writeln(F,cur_n_znach_else);
      Writeln(F,cur_form);

      {������ ����������������}
      if not IsKnowledgeField then Writeln(F,0)
                             else Writeln(F,1);
      if not IsUnited then Writeln(F,0)
                      else Writeln(F,1);
      if not IsTested then Writeln(F,0)
                      else Writeln(F,1);

      {������ ������������ ��}
      Writeln(F,NameOfSolver);
      if not IsKb then Writeln(F,0)
                  else Writeln(F,1);
      if not IsExplanation then Writeln(F,0)
                           else Writeln(F,1);

      {������ ������������ ��}
      Writeln(F,CountOfConsult);
      if not IsEdited then Writeln(F,0)
                      else Writeln(F,1);

      Close(F);
  end;

  procedure TKBControl.ClearKB(AOwner: TComponent);
  begin
    if FKB<>NIL then
      FKB.SetDefault;
  end;

  procedure TKBControl.OpenKB;
  var
    I: Integer;
    SaveDir: String;
    OpenDialog1: TOpenDialog;
  begin
    OpenDialog1:=TOpenDialog.Create(Self);
    OpenDialog1.Title:='�������';
    OpenDialog1.Filter := '���� ������ (*.KBS)|*.KBS|'+
                          '����� (*.SAS)|*.SAS';
    OpenDialog1.FileName:='*.KBS';
    OpenDialog1.InitialDir:=ResPath;
    if OpenDialog1.Execute then begin
      GetDir(0,SaveDir);
      if (System.Pos('.KBS',OpenDialog1.FileName)
         =(Length(OpenDialog1.FileName)-3)) or
         (System.Pos('.kbs',OpenDialog1.FileName)
         =(Length(OpenDialog1.FileName)-3)) then
        begin
          Self.ClearKB(Self.KB.KBOwner);
          Self.KB.FileName:=OpenDialog1.FileName;
          Self.KB.Load;
        end else begin
          Self.ClearKB(Self.KB.KBOwner);
          Self.KB.Import(OpenDialog1.FileName);
        end;
      ChDir(SaveDir);
    end;
    OpenDialog1.Destroy;
  end;

  constructor TVirtControls.Create(AOwner: TComponent);
  begin
    inherited Create(AOwner);
    Labels:=TList.Create;
    Edits:=TList.Create;
    Memos:=TList.Create;
    ComboBoxes:=TList.Create;
    Buttons:=TList.Create;
    ScrollBars:=TList.Create;
    TrackBars:=TList.Create;
    Panels:=TList.Create;
    Others:=TList.Create;
    PanelFlag:=False;
  end;

  destructor TVirtControls.Destroy;
  begin
    Clear;
    Labels.Destroy;
    Edits.Destroy;
    Memos.Destroy;
    ComboBoxes.Destroy;
    Buttons.Destroy;
    ScrollBars.destroy;
    TrackBars.Destroy;
    Panels.Destroy;
    Others.Destroy;
    inherited Destroy;
  end;

  procedure TVirtControls.Clear;
  begin
    while Labels.Count>0 do begin
      VirtLabel:=TLabel(Labels.Items[0]);
      Labels.Delete(0);
      VirtLabel.Destroy;
    end;
    while Edits.Count>0 do begin
      VirtEdit:=TEdit(Edits.Items[0]);
      Edits.Delete(0);
      VirtEdit.Destroy;
    end;
    while Memos.Count>0 do begin
      Virtmemo:=TMemo(Memos.Items[0]);
      Memos.Delete(0);
      VirtMemo.Destroy;
    end;
    while ComboBoxes.Count>0 do begin
      VirtComboBox:=TComboBox(ComboBoxes.Items[0]);
      ComboBoxes.Delete(0);
      VirtComboBox.Destroy;
    end;
    while Buttons.Count>0 do begin
      VirtButton:=TButton(Buttons.Items[0]);
      Buttons.Delete(0);
      VirtButton.Destroy;
    end;
    while ScrollBars.Count>0 do begin
      VirtScrollBar:=TScrollBar(ScrollBars.Items[0]);
      ScrollBars.Delete(0);
      VirtScrollBar.Destroy;
    end;
    while TrackBars.Count>0 do begin
      VirtTrackBar:=TTrackBar(TrackBars.Items[0]);
      TrackBars.Delete(0);
      VirtTrackBar.Destroy;
    end;
    while Others.Count>0 do begin
      VirtOther:=Others.Items[0];
      Others.Delete(0);
      if VirtOther is TMainMenu then TMainMenu(VirtOther).Destroy;
      if VirtOther is TPopUpMenu then TPopUpMenu(VirtOther).Destroy;
      if VirtOther is TCheckBox then TCheckBox(VirtOther).Destroy;
      if VirtOther is TRadioButton then TRadioButton(VirtOther).Destroy;
      if VirtOther is TListBox then TListBox(VirtOther).Destroy;
      if VirtOther is TGroupBox then TGroupBox(VirtOther).Destroy;
      if VirtOther is TRadioGroup then TRadioGroup(VirtOther).Destroy;
      if VirtOther is TSpeedButton then TSpeedButton(VirtOther).Destroy;
      if VirtOther is TMaskEdit then TMaskEdit(VirtOther).Destroy;
      if VirtOther is TStringGrid then TStringGrid(VirtOther).Destroy;
      if VirtOther is TDrawGrid then TDrawGrid(VirtOther).Destroy;
      if VirtOther is TScrollBox then TScrollBox(VirtOther).Destroy;
      if VirtOther is TShape then TShape(VirtOther).Destroy;
      if VirtOther is TBevel then TBevel(VirtOther).Destroy;
    end;
    while Panels.Count>0 do begin
      VirtPanel:=TPanel(Panels.Items[0]);
      Panels.Delete(0);
      VirtPanel.Destroy;
    end;
  end;

  procedure TVirtControls.CREATE_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_COPY_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_COPY_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ADD_IF_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ADD_IF_IN_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ADD_THEN_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ADD_THEN_IN_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.NEXT_IF_NOT_FIRST(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_NEXT_IF_NOT_FIRST,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_NEXT_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_NEXT_OBJECT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_ATTR_FOR_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_ATTR_FOR_OBJECT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.COPY_D_TO_O(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_COPY_D_TO_O,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.GET_T_COMMENT_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_GET_T_COMMENT_FOR_ATTR,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SAY_T_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SAY_T_COMMENT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_T_COMMENT_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_T_COMMENT_FOR_ATTR,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.GET_T_LIST_ZNACH(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_GET_T_LIST_ZNACH,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_IF(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_IF,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_THEN(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_THEN,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.LOAD_O_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_LOAD_O_COMM,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.LOAD_A_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_LOAD_A_COMM,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.LOAD_T_COMM(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_LOAD_T_COMM,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.LOAD_D_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_LOAD_D_NAME,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_TYPE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_NAME,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_OBJECT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.DEF_TYPE_FOR_ATTR(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_DEF_TYPE_FOR_ATTR,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.FIND_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_FIND_TYPE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SELECT_IN_LIST_ZNACH(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SELECT_IN_LIST_ZNACH,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SAY_TYPES(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SAY_TYPES,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_PRIV_N_IF(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_PRIV_N_IF,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.GOSUB(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_GOSUB,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.RETURN(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_RETURN,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_INPUT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_INPUT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CREATE_OUTPUT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CREATE_OUTPUT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ALT_IF_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ALT_IF_IN_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ALT_THEN_IN_RULE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ALT_THEN_IN_RULE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_ELSE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_ELSE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_TYPE(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_TYPE,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_OBJECT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_NAME(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_NAME,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ADD_OBJ_TO_CONN(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ADD_OBJ_TO_CONN,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_LIST(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_LIST,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_TYPE_COMBO(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_TYPE_COMBO,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_TYPE_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_TYPE_COMMENT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_OBJECT_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_OBJECT_COMMENT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_ATTR_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_ATTR_COMMENT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_RULE_COMMENT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_RULE_COMMENT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_FACTOR(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_FACTOR,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SET_SKIP(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SET_SKIP,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.SKIP(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_SKIP,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.FIND_ATTR_OF_OBJECT(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_FIND_ATTR_OF_OBJECT,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.CONSTRUCTS(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_CONSTRUCTS,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_ATTR_CONTROL(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_ATTR_CONTROL,value1,value2,value3,Self,KBLPTool);
  end;

  procedure TVirtControls.ASSIGN_MF_LIST(value1,value2,value3: String; KBLPTool: TKBLPTool);
  begin
    RunFunction(_ASSIGN_MF_LIST,value1,value2,value3,Self,KBLPTool);
  end;

procedure _RegistryMethod( aClass:TClass;
                          aMethod:Pointer;
                          aMethodName:String;
                          ArgsType:array of PTypeInfo;
                          ARetType:PTypeInfo );
begin
  {Types.RegistryMethod( aClass,
                        aMethod,
                        aMethodName,
                        ArgsType,
                        ARetType);}
  {ShowMessage('That is Registry of '+aMethodName);}
end;

procedure _RegistryFunction( aFunction:Pointer;
                            aFuncName:String;
                            ArgsType:array of PTypeInfo;
                            ARetType:PTypeInfo );
begin
  {Types.RegistryFunction( aFunction,
                          aFuncName,
                          ArgsType,
                          ARetType );}
  {ShowMessage('That is Registry of '+aFuncName);}
end;

procedure TKBToolItem.Edit;
begin
  ShowMessage('�������� �������');
end;

procedure TKBToolItem.Verify(Protocol: TStrings);
begin
  inherited Verify(Protocol);
  {ShowMessage('�������� ��������� �������');}
end;

procedure TKBToolItem.Compile;
begin
  ShowMessage('���������� ������ �������');
end;

function TKBToolItem.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
  ShowMessage('������������ ������ �������');
end;

{constructor TKBControlEditor.Create(AComponent: TComponent;
                         ADesigner: IFormDesigner);
begin
  Inherited Create(AComponent,ADesigner);
  KBControl:=TKBControl(AComponent);
end;}

{procedure TKBControlEditor.Edit;
begin
{$G+}
//  Application.CreateForm(TAsprSess, AsprSess);
{$G-}
{  SetData(KBControl);
  AsprSess.ShowModal;
  AsprSess.Destroy;
end;}

procedure TKBControl.Edit;
{var
  KBControlEditor: TKBControlEditor;
  ADesiner: IFormDesigner;}
begin
{  ADesiner:=NIL;
  KBControlEditor:=TKBControlEditor.Create(Self, ADesiner);
  KBControlEditor.Edit;
  KBControlEditor.Destroy;}
end;

constructor TKBTool.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  RegistryTestableProperty('KBControl');
end;

destructor TKBTool.Destroy;
begin
  inherited Destroy;
end;

constructor TKBLPTool.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  RegistryTestableProperty('WordPred');
  RegistryTestableProperty('WordAttr');
  RegistryTestableProperty('LexFunc');
  RegistryTestableProperty('SCClasses');
  RegistryTestableProperty('MFDictionary');
end;

procedure TKBTool.SetActive(Value: Boolean);
begin
  if Value then begin
    FActive:=Value;
    Run(Nil);
    FActive:=False;
  end;
end;

procedure TKBTool.SetStage(Value: TStage);
var
  ClassRef: TClass;
begin
  if Assigned(KBControl) then begin
    SaveToolClass:=KBControl.CurrentToolClass;
    SaveStage:=KBControl.Stage;
    ClassRef:=Self.ClassType;
    KBControl.CurrentToolClass:=ClassRef.ClassName;
    KBControl.Stage:=Value;
  end;
end;

procedure TKBTool.RestStage;
begin
  if Assigned(KBControl) then begin
    KBControl.CurrentToolClass:=SaveToolClass;
    KBControl.Stage:=SaveStage;
  end;
end;

function  TKBTool.Run( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
begin
  Verify(NIL);
  if IsVerified then begin
    SetStage(etUnknown);
    KBControl.CurrentToolClass:=Self.ClassName;
    RestStage;
  end;
  {inherited Run(Parms);}
end;

constructor TExternal.Create{(AOwner: TComponent)};
begin
  inherited Create{(AOwner)};
end;

destructor TExternal.Destroy;
begin
  inherited Destroy;
end;

function TExternal.RunExe( Sender: TObject{Parms:array of Variant} ): {Variant} TObject;
var
  Name: PAnsiChar;
begin
  StrPCopy(Name,FileName);
  WinExec(Name,SW_SHOWNORMAL);
end;

function TExternal.RunDllFunc( Sender: TObject ): TObject;
var
  Name: PWideChar;
  FnName: PWideChar;
begin
  StrPCopy(Name,FileName);
  StrPCopy(FnName,FuncName);
  FActive:=True;
  Handle:=LoadLibrary(Name);
  if Handle <> 0 then begin
    @Run:=GetProcAddress(Handle,FnName);
    if @Run <> nil then
    begin
      RunDllFunc:=Run(Sender);
    end;
    FreeLibrary(Handle);
  end;
  FActive:=False;
end;

procedure TExternal.SetActive(Value: Boolean);
var
  Name: PWideChar;
  FnName: PWideChar;
begin
  StrPCopy(Name,FileName);
  StrPCopy(FnName,FuncName);
  if Value <> FActive then
  try
    Value:=FActive;
    case ExternalType of
      EXE: begin
        if Value then begin
          WinExec(PAnsiChar(Name),SW_SHOWNORMAL);
          FActive:=False;
        end;
      end;
      DLL: begin
        if Value then begin
          Handle:=LoadLibrary(Name);
          if Handle <> 0 then
            @Run:=GetProcAddress(Handle,PAnsiChar(FnName));
        end else
          if Handle<>0 then
            FreeLibrary(Handle);
      end;
    end;
  except
    FActive:=False;
  end;
end;

{procedure TKBToolEditor.Edit;
begin
  TKBTool(Component).Run(Nil);
end;}

constructor TSessionItem.Create(ACollection: TCollection);
var
  i,MaxID: Integer;
begin
  inherited Create(ACollection);
  MaxID:=0;
  for i:=1 to TKBOwnedCollection(ACollection).Count do begin
    if TSessionItem(TKBOwnedCollection(ACollection).Items[i-1]).SessionID>MaxID
      then MaxID:=TSessionItem(TKBOwnedCollection(ACollection).Items[i-1]).SessionID;
  end;
  SessionID:=MaxID+1;
  SessionName:='Session'+IntToStr(SessionID);
end;

function TKBControl.GetMaxSessionID: Integer;
var
  i,MaxID: Integer;
begin
  MaxID:=0;
  for i:=1 to Sessions.Count do begin
    if TSessionItem(Sessions.Items[i-1]).SessionID>MaxID
      then MaxID:=TSessionItem(Sessions.Items[i-1]).SessionID;
  end;
  GetMaxSessionID:=MaxID;
end;

destructor TSessionItem.Destroy;
begin
//  FKF.Destroy;
  inherited Destroy;
end;

procedure TSessionItem.Assign(KBCollectionItem: TKBCollectionItem);
begin
//  FKF.Assign(TSessionItem(KBItem).FKF);
  FSessionName:=TSessionItem(KBCollectionItem).FSessionName;
  FSessionID:=TSessionItem(KBCollectionItem).FSessionID;
end;

procedure TKBControl.SetSessionID(Value: Integer);
var
  SessionItem: TSessionItem;
begin
  FCurrentSessionID:=Value;
  FCountOfSessions:=Sessions.Count;
  if GetMaxSessionID<Value then begin
    SessionItem:=TSessionItem(Sessions.Add);
    SessionItem.SessionID:=Value;
    SessionItem.SessionName:='Session'+IntToStr(Value);
  end;
  if Assigned(KB) then
    KB.SessionID:=Value;
end;

end.

