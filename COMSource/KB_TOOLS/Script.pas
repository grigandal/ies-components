unit Script;

interface

uses
  Classes,
  SysUtils,
  VCL.Dialogs,
  Aspr95,
  VCL.StdCtrls,
  VCL.ComCtrls,
  VCL.ExtCtrls,
  VCL.Controls,
  VCL.Menus,
  VCL.Buttons,
  VCL.Mask,
  VCL.Grids,
  VCL.Forms,
  Paths,
  KBTypes;

var
  attr_count: Integer=0;

type
  TFunctionName=(
  _SHOWFORM,                    {0 �������� �����}
  _Y_N,                         {1 �������� "��"/"���" � ���.GOTO,
                                    ���� "��";����� ������� �� ����.}
  _GOTO,                        {2 ������� �� ���. �������� ������}
  _CREATE_RULE,                 {3 ������� �������}
  _CREATE_COPY_RULE,            {4 ������� ����� �������, �������
                                    �������� ������� ������� ������
                                    �� ������� ���� �������-
                                    �������� � �� �������� ������.
                                    ����� � ��������}
  _ADD_IF_IN_RULE,              {5 �������� � ������� ��������
                                    ������� ���� �������� �������-
                                    ��������  (��� - �������)}
  _ADD_THEN_IN_RULE,            {6 �������� � �������� ��������
                                    ������� ���� �������� �������-
                                    �������� (��� - �������)}
  _NEXT_IF_NOT_FIRST,           {7 ������� � ���������� ����
                                    �������-��������, ���� ������
                                    ���� �� �������� ������, �
                                    ����� ������� �� ��������
                                    ������; ����� ������� �� ����.}
  _CREATE_NEXT_OBJECT,          {8 ������� ����� ������ � ������������
                                    ������� �����������, c���������� �
                                    ������� ��������}
  _CREATE_SUBOBJECT,            {9 ������� ������ ����� ������� �����
                                    � ������������ ������� �����������,
                                    c���������� � ������� ��������}
  _CREATE_ATTR_FOR_OBJECT,      {10 ������� ����� ������� � ������������
                                    ������� �����������}
  _COPY_D_TO_O,                 {11���������� �������� ����
                                    �������� �������� � �����������
                                    �������� �������}
  _GET_T_COMMENT_FOR_ATTR,      {12������� ����� ��� � ������
                                    ����������� ��� ������ ����
                                    ��� ������� ��� �� ������������
                                    (��� �������� ��������)}
  _SAY_T_COMMENT,               {13������ �� ����� ����������� ��
                                    �������� ���� (��� ��������
                                    ��������)}
  _CREATE_T_COMMENT_FOR_ATTR,   {14������� ����� ��� � ���������
                                    ����������� ��� ������ ����
                                    ������ "not(", ���� ������ ����
                                    ��� ��� (��� �������� ��������)}
  _GET_T_LIST_ZNACH,            {15������� �������� �������� ����
                                    (��� �������� ��������)
                                    ��� ������� ����� ��������,
                                    ������� ������� �������}
  _SAY_T_LIST_ZNACH,            {16������ �� ����� ��� �������
                                    �������� ���� (�������� ��������)}
  _SAY_T_DICT_1,                {17������ �� ����� �������� ��������
                                    ���� (��� �������� ��������)}
  _SET_IF,                      {18���������� ������� ������
                                    ��� �������-�������� �������
                                    �������� �������}
  _SET_THEN,                    {19���������� ������� ������
                                    ��� �������-�������� �������
                                    �������� �������}
  _COPY_PRIV_D_TO_O,            {20���������� �������� ����
                                    ��������������� �������� � �������
                                    � ����������� �������� �������}
  _SHOWSTRUCT,                  {21�������� ���������}
  _LOAD_O_COMM,                 {22��������� ����� ����������
                                    �������� ������� ������� ��
                                    ��������}
  _LOAD_A_COMM,                 {23��������� ����� ����������
                                    �������� �������� ������� ��
                                    ��������}
  _LOAD_T_COMM,                 {24��������� ����� ����������
                                    ��������     ���� ������� ��
                                    ��������}
  _LOAD_D_NAME,                 {25��������� ����� ����������
                                    �������� �������� ���� �������
                                    �� ��������}
  _CREATE_TYPE,                 {26������� ����� ���}
  _CREATE_NAME,                 {27������� ����� �������� ��������
                                    ����}
  _CREATE_OBJECT,               {28������� ����� ������ � ������������
                                    ������� �����������, �� �����������
                                   � ������� ��������}
  _DEF_TYPE_FOR_ATTR,           {29������� ������� ������� � �������
                                    �����}
  _FIND_TYPE,                   {30����� ��� �� �����������}
  _SELECT_IN_LIST_ZNACH,        {31������� ������� ��������
                                    �������� ����}
  _SAY_TYPES,                   {32������ �� ����� �����������
                                    �� ���� �����}
  _SET_PRIV_N_IF,               {33���������� ����� �� N ���
                                    �������-��������}
  _GOSUB,                       {34������� �� ������������}
  _RETURN,                      {35������� �� ������������}
  _CREATE_INPUT,                {36������� ������ � ������������
                                    ������� �����������,
                                    c���������� � ������� ��������
                                    � ����������� �� ����� ��������
                                    �������}
  _CREATE_OUTPUT,               {37������� ������ � ������������
                                    ������� �����������,
                                    c���������� � ������� ��������
                                    � ����������� �� ������ ��������
                                    �������}
  _ALT_IF_IN_RULE,              {38�������� � ������� ��������
                                    ������� �������������� ����
                                    �������� �������-
                                    ��������  (��� - �������)}
  _ALT_THEN_IN_RULE,            {39�������� � �������� ��������
                                    ������� ��������������
                                    ���� �������� �������-
                                    �������� (��� - �������)}
  _SAY_TEXT,                    {40������ �� ����� �����}
  _SELECT_TYPE,                 {41}
  _REP_RESH,                    {42}
  _LOAD_VALUES_FROM_FILE,       {43 �������� � ������ �������� �������� ����
                                  ����� �� �����, ���������� � ��������}
  _CREATE_TYPE_FROM_VALUE,      {44 ��������� ����� ���. ��� ���� � ��� t_comment
                                       ��������� �������� (znach), ������� �� �����
                                       ���� �������. ���� �������� �������� �� ����,
                                       �� � t_comment ��������� '!'}
  _ADD_ELSE_IN_RULE,            {45 �������� � �������� �������}
  _SET_ELSE,                    {46���������� ������� ������
                                    ��� �������-�������� '�����'
                                    �������� �������}
  _ASSIGN_TYPE,
  _ASSIGN_OBJECT,
  _SET_NAME,
  _CREATE_CONN,
  _ADD_OBJ_TO_CONN,
  _ASSIGN_LIST,
  _SET_TYPE_COMBO,
  _ASSIGN_TYPE_COMMENT,
  _ASSIGN_OBJECT_COMMENT,
  _ASSIGN_ATTR_COMMENT,
  _ASSIGN_RULE_COMMENT,
  _ASSIGN_FACTOR,
  _SET_SKIP,
  _SKIP,
  _FIND_ATTR_OF_OBJECT,
  _CONSTRUCTS,
  _ASSIGN_ATTR_CONTROL,
  _ASSIGN_MF_LIST,
  _CONSTRUCTS_LINGV,
  _ERROR                        {55 Error}

  );

  function SetFunction(str: PChar): TFunctionName;
  procedure RunFunction(name: TFunctionName;
                        value1: String;
                        value2: String;
                        value3: String;
                        VirtControls: TVirtControls;
                        KBLPTool: TKBLPTool);
  procedure GenIncident(ptr: record_rule);
  procedure gen_incident(list_attr_if: TCollection;
                         incident: PChar);


  var
    KB_: TKnowledgeBase;
    cur_obj, last_obj:  record_obj;
    cur_attr: record_attr;
    cur_type: record_type;
    cur_rule, last_rule: record_rule;
    cur_list: list;
    cur_value: value;
//    cur_mf_value: TMFOwner;
    cur_conn: record_conn;
    I,J,K,T: Integer;

implementation

  {procedure StartRepertImport(Detachment: TKBControl); far; external Repert;}

  function strcmp(s1,s2: PChar):Boolean;
  begin
    if StrComp(s1,s2)=0 then strcmp:=True else strcmp:=False;
  end;

  function SetFunction(str: PChar): TFunctionName;
  var
    FunctionName: TFunctionName;

  begin
  FunctionName:=_ERROR;
  if (strcmp(str,'SHOWFORM')              ) then FunctionName:=_SHOWFORM;
  if (strcmp(str,'Y_N')                   ) then FunctionName:=_Y_N;
  if (strcmp(str,'GOTO')                  ) then FunctionName:=_GOTO;
  if (strcmp(str,'CREATE_RULE')           ) then FunctionName:=_CREATE_RULE;
  if (strcmp(str,'CREATE_COPY_RULE')      ) then FunctionName:=_CREATE_COPY_RULE;
  if (strcmp(str,'ADD_IF_IN_RULE')        ) then FunctionName:=_ADD_IF_IN_RULE;
  if (strcmp(str,'ADD_THEN_IN_RULE')      ) then FunctionName:=_ADD_THEN_IN_RULE;
  if (strcmp(str,'NEXT_IF_NOT_FIRST')     ) then FunctionName:=_NEXT_IF_NOT_FIRST;
  if (strcmp(str,'CREATE_NEXT_OBJECT')    ) then FunctionName:=_CREATE_NEXT_OBJECT;
  if (strcmp(str,'CREATE_SUBOBJECT')      ) then FunctionName:=_CREATE_SUBOBJECT;
  if (strcmp(str,'CREATE_ATTR_FOR_OBJECT')) then FunctionName:=_CREATE_ATTR_FOR_OBJECT;
  if (strcmp(str,'COPY_D_TO_O')           ) then FunctionName:=_COPY_D_TO_O;
  if (strcmp(str,'GET_T_COMMENT_FOR_ATTR')) then FunctionName:=_GET_T_COMMENT_FOR_ATTR;
  if (strcmp(str,'SAY_T_COMMENT')         ) then FunctionName:=_SAY_T_COMMENT;
  if (strcmp(str,'CREATE_T_COMMENT_FOR_ATTR')) then FunctionName:=_CREATE_T_COMMENT_FOR_ATTR;
  if (strcmp(str,'GET_T_LIST_ZNACH')      ) then FunctionName:=_GET_T_LIST_ZNACH;
  if (strcmp(str,'SAY_T_LIST_ZNACH')      ) then FunctionName:=_SAY_T_LIST_ZNACH;
  if (strcmp(str,'SAY_T_DICT_1')          ) then FunctionName:=_SAY_T_DICT_1;
  if (strcmp(str,'SET_IF')                ) then FunctionName:=_SET_IF;
  if (strcmp(str,'SET_THEN')              ) then FunctionName:=_SET_THEN;
  if (strcmp(str,'COPY_PRIV_D_TO_O')      ) then FunctionName:=_COPY_PRIV_D_TO_O;
  if (strcmp(str,'SHOWSTRUCT')            ) then FunctionName:=_SHOWSTRUCT;
  if (strcmp(str,'LOAD_O_COMM')           ) then FunctionName:=_LOAD_O_COMM;
  if (strcmp(str,'LOAD_A_COMM')           ) then FunctionName:=_LOAD_A_COMM;
  if (strcmp(str,'LOAD_T_COMM')           ) then FunctionName:=_LOAD_T_COMM;
  if (strcmp(str,'LOAD_D_NAME')           ) then FunctionName:=_LOAD_D_NAME;
  if (strcmp(str,'CREATE_TYPE')           ) then FunctionName:=_CREATE_TYPE;
  if (strcmp(str,'CREATE_NAME')           ) then FunctionName:=_CREATE_NAME;
  if (strcmp(str,'CREATE_OBJECT')         ) then FunctionName:=_CREATE_OBJECT;
  if (strcmp(str,'DEF_TYPE_FOR_ATTR')     ) then FunctionName:=_DEF_TYPE_FOR_ATTR;
  if (strcmp(str,'FIND_TYPE')             ) then FunctionName:=_FIND_TYPE;
  if (strcmp(str,'SELECT_IN_LIST_ZNACH')  ) then FunctionName:=_SELECT_IN_LIST_ZNACH;
  if (strcmp(str,'SAY_TYPES')             ) then FunctionName:=_SAY_TYPES;
  if (strcmp(str,'SET_PRIV_N_IF')         ) then FunctionName:=_SET_PRIV_N_IF;
  if (strcmp(str,'GOSUB')                 ) then FunctionName:=_GOSUB;
  if (strcmp(str,'RETURN')                ) then FunctionName:=_RETURN;
  if (strcmp(str,'CREATE_INPUT')          ) then FunctionName:=_CREATE_INPUT;
  if (strcmp(str,'CREATE_OUTPUT')         ) then FunctionName:=_CREATE_OUTPUT;
  if (strcmp(str,'ALT_IF_IN_RULE')        ) then FunctionName:=_ALT_IF_IN_RULE;
  if (strcmp(str,'ALT_THEN_IN_RULE')      ) then FunctionName:=_ALT_THEN_IN_RULE;
  if (strcmp(str,'SAY_TEXT')              ) then FunctionName:=_SAY_TEXT;
  if (strcmp(str,'SELECT_TYPE')           ) then FunctionName:=_SELECT_TYPE;
  if (strcmp(str,'REP_RESH')              ) then FunctionName:=_REP_RESH;
  if (strcmp(str,'LOAD_VALUES_FROM_FILE') ) then FunctionName:=_LOAD_VALUES_FROM_FILE;
  if (strcmp(str,'CREATE_TYPE_FROM_VALUE')) then FunctionName:=_CREATE_TYPE_FROM_VALUE;
  if (strcmp(str,'ADD_ELSE_IN_RULE')      ) then FunctionName:=_ADD_THEN_IN_RULE;
  if (strcmp(str,'SET_ELSE')              ) then FunctionName:=_SET_ELSE;
  if (strcmp(str,'ASSIGN_TYPE')           ) then FunctionName:=_ASSIGN_TYPE;
  if (strcmp(str,'ASSIGN_OBJECT')         ) then FunctionName:=_ASSIGN_OBJECT;
  if (strcmp(str,'CREATE_CONN')           ) then FunctionName:=_CREATE_CONN;
  if (strcmp(str,'ADD_OBJ_TO_CONN')       ) then FunctionName:=_ADD_OBJ_TO_CONN;
  if (strcmp(str,'ASSIGN_LIST')           ) then FunctionName:=_ASSIGN_LIST;
  if (strcmp(str,'SET_TYPE_COMBO')        ) then FunctionName:=_SET_TYPE_COMBO;
  if (strcmp(str,'ASSIGN_TYPE_COMMENT')   ) then FunctionName:=_ASSIGN_TYPE_COMMENT;
  if (strcmp(str,'ASSIGN_OBJECT_COMMENT') ) then FunctionName:=_ASSIGN_OBJECT_COMMENT;
  if (strcmp(str,'ASSIGN_ATTR_COMMENT')   ) then FunctionName:=_ASSIGN_ATTR_COMMENT;
  if (strcmp(str,'ASSIGN_RULE_COMMENT')   ) then FunctionName:=_ASSIGN_RULE_COMMENT;
  if (strcmp(str,'ASSIGN_FACTOR')         ) then FunctionName:=_ASSIGN_FACTOR;
  if (strcmp(str,'SET_SKIP')              ) then FunctionName:=_SET_SKIP;
  if (strcmp(str,'SKIP')                  ) then FunctionName:=_SKIP;
  if (strcmp(str,'FIND_ATTR_OF_OBJECT')   ) then FunctionName:=_FIND_ATTR_OF_OBJECT;
  if (strcmp(str,'CONSTRUCTS')            ) then FunctionName:=_CONSTRUCTS;
  if (strcmp(str,'ASSIGN_ATTR_CONTROL')   ) then FunctionName:=_ASSIGN_ATTR_CONTROL;
  if (strcmp(str,'ASSIGN_MF_LIST')        ) then FunctionName:=_ASSIGN_MF_LIST;
  if (strcmp(str,'CONSTRUCTS_LINGV')      ) then FunctionName:=_CONSTRUCTS_LINGV;
  if (FunctionName=_ERROR) then begin
        ShowMessage(' �������� ��� �������: '+StrPas(str));
        Exit;
      end;

  SetFunction:=FunctionName;
  end;

  procedure RunFunction(name: TFunctionName;
                        value1: String;
                        value2: String;
                        value3: String;
                        VirtControls: TVirtControls;
                        KBLPTool: TKBLPTool);

  var
    Det_: TKBControl;
  begin
    Det_:=VirtControls.KBControl;
    case name of
      _SHOWFORM: begin
      end;
      _Y_N:;
      _GOTO:;
      _CREATE_RULE: begin
        cur_rule:=record_rule.Create(KB_.ListRules);
        {KB_.ListRules.Add(cur_rule);}
        with cur_rule do begin
          n_rule:=KB_.ListRules.Count;
          k_znach_if     := 0;
          k_znach_then   := 0;
          k_znach_else   := 0;
          StrCopy(incident,       ''  );
          StrCopy(consequent,     ''  );
          StrCopy(consequent_else,'');
          comment        := 'NewRule';
        end;
        Det_.cur_n_znach_if:=0;
        Det_.cur_n_znach_then:=0;
        Det_.cur_n_znach_else:=0;
      end;
      _CREATE_COPY_RULE: begin
        last_rule:=cur_rule;
        cur_rule:=record_rule.Create(KB_.ListRules);
        {KB_.ListRules.Add(cur_rule);}
        with cur_rule do begin
          n_rule:=KB_.ListRules.Count;
          k_znach_if     := 0;
          k_znach_then   := 0;
          k_znach_else   := 0;
          StrCopy(incident,       ''  );
          StrCopy(consequent,     ''  );
          StrCopy(consequent_else,'');
          comment        := last_rule.comment;
        end;
        if last_rule<>NIL then begin
          for i:=1 to Det_.cur_n_znach_if do begin
            cur_list:=list.Create(cur_rule.list_attr_if);
            with list(last_rule.list_attr_if.Items[i-1]) do begin
              cur_list.n_obj     :=  n_obj;
              cur_list.n_attr    :=  n_attr;
              cur_list.znach     :=  znach;
              cur_list.n_layer   :=  n_layer;
              cur_list.factor1   :=  factor1;
              cur_list.factor2   :=  factor2;
              cur_list.accuracy  :=  accuracy;
              cur_list.predicat  :=  predicat;
              cur_list.PtrAttr   :=  PtrAttr;
            end;
            {cur_rule.list_attr_if.Add(cur_list);}
          end;
          GenIncident(cur_rule);
          {for i:=1 to last_rule.list_attr_then.Count do begin
            cur_list:=list.Create;
            with list(last_rule.list_attr_then.Items[i-1]) do begin
              cur_list.n_obj     :=  n_obj;
              cur_list.n_attr    :=  n_attr;
              StrCopy(cur_list.znach,znach);
              cur_list.n_layer   :=  n_layer;
              cur_list.factor1   :=  factor1;
              cur_list.factor2   :=  factor2;
              cur_list.accuracy  :=  accuracy;
              cur_list.predicat  :=  predicat;
            end;
            cur_rule.list_attr_then.Add(cur_list);
          end;
          for i:=1 to last_rule.list_attr_else.Count do begin
            cur_list:=list.Create;
            with list(last_rule.list_attr_else.Items[i-1]) do begin
              cur_list.n_obj     :=  n_obj;
              cur_list.n_attr    :=  n_attr;
              StrCopy(cur_list.znach,znach);
              cur_list.n_layer   :=  n_layer;
              cur_list.factor1   :=  factor1;
              cur_list.factor2   :=  factor2;
              cur_list.accuracy  :=  accuracy;
              cur_list.predicat  :=  predicat;
            end;
            cur_rule.list_attr_else.Add(cur_list);
          end;}
        end;
      end;
      _ADD_IF_IN_RULE: begin
        cur_list:=list.Create(cur_rule.list_attr_if);
        Det_.CurrentStepNumber:=Det_.CurrentStepNumber+1;
        cur_list.PtrAttr:=cur_attr;
        {cur_rule.list_attr_if.Add(cur_list);}
        with cur_list do begin
          n_obj     :=  cur_obj.n_obj;
          n_attr    :=  cur_attr.n_attr;
          znach     :=  '';
          n_layer   :=  1;
          factor1   :=  50;
          factor2   :=  100;
          accuracy  :=  0;
          predicat  :=  0;
          Control   := '';
          Skip      := False;
          Step      := Det_.CurrentStepNumber;
        end;
        cur_rule.k_znach_if:=cur_rule.k_znach_if+1;
        Det_.cur_n_znach_if:=Det_.cur_n_znach_if+1;
        GenIncident(cur_rule);
     end;
      _ADD_THEN_IN_RULE:begin
        cur_list:=list.Create(cur_rule.list_attr_then);
        Det_.CurrentStepNumber:=Det_.CurrentStepNumber+1;
        cur_list.PtrAttr:=cur_attr;
        {cur_rule.list_attr_then.Add(cur_list);}
        with cur_list do begin
          n_obj     :=  cur_obj.n_obj;
          n_attr    :=  cur_attr.n_attr;
          znach     :=  '';
          n_layer   :=  1;
          factor1   :=  50;
          factor2   :=  100;
          accuracy  :=  0;
          predicat  :=  0;
          Control   := '';
          Skip      := False;
          Step      := Det_.CurrentStepNumber;
        end;
        cur_rule.k_znach_then:=cur_rule.k_znach_then+1;
        Det_.cur_n_znach_then:=Det_.cur_n_znach_then+1;
      end;
      _NEXT_IF_NOT_FIRST:;
      _CREATE_NEXT_OBJECT: begin
        last_obj:=cur_obj;
        cur_obj:=record_obj.Create(KB_.ListObjs);
        {KB_.ListObjs.Add(cur_obj);}
        with cur_obj do begin
          n_obj:=KB_.ListObjs.Count;
          k_attr     := 0;
          comment    := 'NewObj';
          Control   := '';
          if last_obj<>NIL then last:=last_obj.n_obj;
        end;
      end;
      _CREATE_SUBOBJECT:;
      _CREATE_ATTR_FOR_OBJECT:begin
        cur_attr:=record_attr.Create(cur_obj.ListAttrs);
        {cur_obj.ListAttrs.Add(cur_attr);}
        with cur_attr do begin
          Inc(attr_count);
          n_obj:=cur_obj.n_obj;
          n_attr:=attr_count;
          TAssertion(AssertionCollection.Items[0]).n_obj:=n_obj;
          TAssertion(AssertionCollection.Items[0]).n_attr:=n_attr;
          n_type:=cur_type.n_type;
          comment:=StrPas('�')+IntToStr(n_attr);
          {F}PtrType:=cur_type;
          {F}PtrObj:=cur_obj;
        end;
        cur_obj.k_attr:=cur_obj.k_attr+1;
      end;
      _COPY_D_TO_O:;
      _GET_T_COMMENT_FOR_ATTR:;
      _SAY_T_COMMENT: begin
        for i:=1 to VirtControls.Labels.Count do begin
          if CompareStr(TLabel(VirtControls.Labels.Items[i-1]).name,value2)=0
            then begin
              TLabel(VirtControls.Labels.Items[i-1]).Caption:=cur_type.comment;
              break;
            end;
        end;
        for i:=1 to VirtControls.Edits.Count do begin
          if CompareStr(TEdit(VirtControls.Edits.Items[i-1]).name,value2)=0
            then begin
              TEdit(VirtControls.Edits.Items[i-1]).Text:=cur_type.Fcomment;
              break;
            end;
        end;
      end;
      _CREATE_T_COMMENT_FOR_ATTR:;
      _GET_T_LIST_ZNACH:;
      _SAY_T_LIST_ZNACH: begin
        for i:=1 to VirtControls.Labels.Count do begin
          if CompareStr(TLabel(VirtControls.Labels.Items[i-1]).name,value2)=0
            then begin
              TLabel(VirtControls.Labels.Items[i-1]).Caption:=cur_value.{F}znach;
              break;
            end;
        end;
        for i:=1 to VirtControls.Edits.Count do begin
          if CompareStr(TEdit(VirtControls.Edits.Items[i-1]).name,value2)=0
            then begin
              TEdit(VirtControls.Edits.Items[i-1]).Text:=cur_value.{F}znach;
              break;
            end;
        end;
      end;
      _SAY_T_DICT_1:;
      _SET_IF: begin
        cur_list:=
          list(cur_rule.list_attr_if.Items[Det_.cur_n_znach_if-1]);
         for i:=1 to KB_.ListObjs.Count do begin
            cur_obj:=record_obj(KB_.ListObjs.Items[i-1]);
            if cur_obj.n_obj=cur_list.n_obj then begin
              for j:=1 to cur_obj.ListAttrs.Count do begin
                cur_attr:=record_attr(cur_obj.ListAttrs.Items[j-1]);
                if cur_attr.n_attr=cur_list.n_attr then break;
              end;
              break;
            end;
          end;
          for i:=1 to KB_.ListTypes.Count do begin
            cur_type:=record_type(KB_.ListTypes.Items[i-1]);
            if cur_type.n_type=cur_attr.n_type then break;
          end;
      end;
      _SET_THEN: begin
        cur_list:=
          list(cur_rule.list_attr_then.Items[Det_.cur_n_znach_then-1]);
          for i:=1 to KB_.ListObjs.Count do begin
            cur_obj:=record_obj(KB_.ListObjs.Items[i-1]);
            if cur_obj.n_obj=cur_list.n_obj then begin
              for j:=1 to cur_obj.ListAttrs.Count do begin
                cur_attr:=record_attr(cur_obj.ListAttrs.Items[j-1]);
                if cur_attr.n_attr=cur_list.n_attr then break;
              end;
              break;
            end;
          end;
          for i:=1 to KB_.ListTypes.Count do begin
            cur_type:=record_type(KB_.ListTypes.Items[i-1]);
            if cur_type.n_type=cur_attr.n_type then break;
          end;
      end;
      _COPY_PRIV_D_TO_O:;
      _SHOWSTRUCT:;
      _LOAD_O_COMM: begin
        cur_obj.comment:=value1;
      end;
      _LOAD_A_COMM: begin;
        cur_attr.comment:=value1;
      end;
      _LOAD_T_COMM: begin;
        cur_type.comment:=value1;
      end;
      _LOAD_D_NAME: begin;
        cur_value.{F}znach:=value1;
        //cur_mf_value.MF_Name:=value1;
      end;
      _CREATE_TYPE:begin
        {cur_type:=record_type.Create(KB_.ListTypes);}
        {KB_.ListTypes.Add(cur_type);}
        cur_type:=record_type(KB_.ListTypes.Add);
        with cur_type do begin
          n_type     :=KB_.ListTypes.Count;
          k_znach    := 0;
          type_znach := 2;
          min_znach:=StrPas('00000000');
          max_znach:=StrPas('00000000');
          comment    := StrPas('NewType');
          Control   := '';
        end;
      end;
      _CREATE_NAME: begin
         cur_type.list_znach.Add;
         cur_value:=value(cur_type.list_znach.Items[cur_type.list_znach.Count-1]);
         cur_value.{F}znach:='';

//         cur_type.MFList.Add;
//         cur_mf_value:=TMFOwner(cur_type.MFList.Items[cur_type.MFList.Count-1]);
//         cur_mf_value.MF_Name:='';

         cur_type.k_znach:=cur_type.k_znach+1;
      end;
      _CREATE_OBJECT: begin
        {cur_obj:=record_obj.Create(KB_.ListObjs);}
        {KB_.ListObjs.Add(cur_obj);}
        cur_obj:=record_obj(KB_.ListObjs.Add);
        with cur_obj do begin
          n_obj:=KB_.ListObjs.Count;
          k_attr     := 0;
          comment    := 'NewObj';
          last       := 0;
        end;
      end;
      _DEF_TYPE_FOR_ATTR: begin
        with cur_attr do begin
          n_type:=cur_type.n_type;
          {F}PtrType:=cur_type;
        end;
      end;
      _FIND_TYPE: begin
        for i:=1 to KB_.ListTypes.Count do begin
          cur_type:=record_type(KB_.ListTypes.Items[i-1]);
          if CompareStr(cur_type.comment,value1)=0 then break;
        end;
      end;
      _SELECT_IN_LIST_ZNACH:;
      _SAY_TYPES:;
      _SET_PRIV_N_IF: begin
        Det_.cur_n_znach_if:=Det_.cur_n_znach_if-1;
        if Det_.cur_n_znach_if>0 then begin
          cur_list:=
            list(cur_rule.list_attr_if.Items[Det_.cur_n_znach_if-1]);
          for i:=1 to KB_.ListObjs.Count do begin
            cur_obj:=record_obj(KB_.ListObjs.Items[i-1]);
            if cur_obj.n_obj=cur_list.n_obj then begin
              for j:=1 to cur_obj.ListAttrs.Count do begin
                cur_attr:=record_attr(cur_obj.ListAttrs.Items[j-1]);
                if cur_attr.n_attr=cur_list.n_attr then break;
              end;
              break;
            end;
          end;
          for i:=1 to KB_.ListTypes.Count do begin
            cur_type:=record_type(KB_.ListTypes.Items[i-1]);
            if cur_type.n_type=cur_attr.n_type then break;
          end;
        end;
      end;
      _GOSUB:;
      _RETURN:;
      _CREATE_INPUT:;
      _CREATE_OUTPUT:;
      _ALT_IF_IN_RULE: begin
        {cur_list:=list.Create(cur_rule.list_attr_if);}
        cur_list:=list(cur_rule.list_attr_if.Add);
        Det_.CurrentStepNumber:=Det_.CurrentStepNumber+1;
        cur_list.PtrAttr:=cur_attr;
        {cur_rule.list_attr_if.Add(cur_list);}
        with cur_list do begin
          n_obj     :=  cur_obj.n_obj;
          n_attr    :=  cur_attr.n_attr;
          znach     :=  '';
          n_layer   :=  2;
          factor1   :=  50;
          factor2   :=  1;
          accuracy  :=  0;
          predicat  :=  0;
          Control   := '';
          Skip      := False;
          Step      := Det_.CurrentStepNumber;
        end;
        cur_rule.k_znach_if:=cur_rule.k_znach_if+1;
        GenIncident(cur_rule);
      end;
      _ALT_THEN_IN_RULE:;
      _SAY_TEXT: begin
        for i:=1 to VirtControls.Labels.Count do begin
          if CompareStr(TLabel(VirtControls.Labels.Items[i-1]).name,value2)=0
            then begin
              TLabel(VirtControls.Labels.Items[i-1]).Caption:=value1;
              break;
            end;
        end;
      end;
      _SELECT_TYPE:;
      _REP_RESH:;
      _LOAD_VALUES_FROM_FILE:;
      _CREATE_TYPE_FROM_VALUE:;
      _ADD_ELSE_IN_RULE: begin
        {cur_list:=list.Create(cur_rule.list_attr_else);}
        cur_list:=list(cur_rule.list_attr_then.Add);
        Det_.CurrentStepNumber:=Det_.CurrentStepNumber+1;
        cur_list.PtrAttr:=cur_attr;
        {cur_rule.list_attr_else.Add(cur_list);}
        with cur_list do begin
          n_obj     :=  cur_obj.n_obj;
          n_attr    :=  cur_attr.n_attr;
          znach     :=  '';
          n_layer   :=  1;
          factor1   :=  50;
          factor2   :=  1;
          accuracy  :=  0;
          predicat  :=  0;
          Control   := '';
          Skip      := False;
          Step      := Det_.CurrentStepNumber;
       end;
        cur_rule.k_znach_else:=cur_rule.k_znach_else+1;
        Det_.cur_n_znach_else:=Det_.cur_n_znach_else+1;
      end;
      _ASSIGN_TYPE: begin
        cur_type.Control:=value2;
      end;
      _ASSIGN_OBJECT: begin
        cur_obj.Control:=value2;
      end;
      _SET_NAME: begin
        cur_value:=
          value(cur_type.list_znach.Items[cur_type.list_znach.Count-1]);
//        cur_mf_value:=
//          TMFOwner(cur_type.MFList.Items[cur_type.MFList.Count-1]);
      end;
      _CREATE_CONN: begin
        {cur_conn:=record_conn.Create(KB_.ListConns);}
        {KB_.ListConns.Add(cur_conn);}
        cur_conn:=record_conn(KB_.ListConns.Add);
        with cur_conn do begin
          n_conn:=KB_.ListConns.Count;
          k_znach     := 0;
          comment     := 'NewConn';
        end;
        {Det_.cur_n_znach_conn:=0;}
      end;
      _ADD_OBJ_TO_CONN: begin
        {cur_list:=list.Create(cur_conn.list_conn);}
        {cur_conn.list_conn.Add(cur_list);}
        cur_list:=list(cur_conn.list_conn.Add);
        with cur_list do begin
          n_obj     :=  cur_obj.n_obj;
          n_attr    :=  cur_attr.n_attr;
          znach     :=  '';
          n_layer   :=  1;
          factor1   :=  50;
          factor2   :=  100;
          accuracy  :=  0;
          predicat  :=  0;
        end;
        Inc(cur_conn.k_znach);
        {Inc(Det_.cur_n_znach_conn);}
      end;
      _ASSIGN_LIST: begin
        cur_list.Control:=value2;
      end;
      _SET_TYPE_COMBO: begin
        cur_type.Combo:=value2;
      end;
      _ASSIGN_TYPE_COMMENT: begin
        cur_type.Comm:=value2;
      end;
      _ASSIGN_OBJECT_COMMENT: begin
        cur_obj.Comm:=value2;
      end;
      _ASSIGN_ATTR_COMMENT: begin
        cur_attr.Comm:=value2;
      end;
      _ASSIGN_RULE_COMMENT: begin
        cur_rule.Comm:=value2;
      end;
      _ASSIGN_FACTOR: begin
        if value3='factor1'  then cur_list.Scale1:=value2;
        if value3='factor2'  then cur_list.Scale2:=value2;
        if value3='accuracy' then cur_list.Scale3:=value2;
      end;
      _SET_SKIP: begin
        cur_list.Skip:=True;
      end;
      _SKIP: begin
        cur_list:=
          list(cur_rule.list_attr_if.Items[Det_.cur_n_znach_if-1]);
        while (Det_.cur_n_znach_if>1) and cur_list.Skip do begin
        Det_.cur_n_znach_if:=Det_.cur_n_znach_if-1;
          cur_list:=
            list(cur_rule.list_attr_if.Items[Det_.cur_n_znach_if-1]);
        end;
      end;
      _FIND_ATTR_OF_OBJECT: begin
        if cur_obj<>NIL then begin
          for i:=1 to cur_obj.ListAttrs.Count do begin
            cur_attr:=record_attr(cur_obj.ListAttrs.Items[i-1]);
            if CompareStr(cur_attr.comment,value1)=0 then break;
          end;
        end;
        if (cur_attr=NIL) or
           (CompareStr(cur_attr.comment,value1)<>0) then begin
          {cur_attr:=record_attr.Create(cur_obj.ListAttrs);}
          {cur_obj.ListAttrs.Add(cur_attr);}
          cur_attr:=record_attr(cur_obj.ListAttrs.Add);
          with cur_attr do begin
            Inc(attr_count);
            n_obj:=cur_obj.n_obj;
            n_attr:=attr_count;
            n_type:=cur_type.n_type;
            TAssertion(AssertionCollection.Items[0]).n_obj:=n_obj;
            TAssertion(AssertionCollection.Items[0]).n_attr:=n_attr;
            comment:=value1;
            {F}PtrType:=cur_type;
            {F}PtrObj:=cur_obj;
          end;
          cur_obj.k_attr:=cur_obj.k_attr+1;
        end;
      end;
{      _CONSTRUCTS: Reper.StartRepertImport(Det_);
      _CONSTRUCTS_LINGV: begin
        if Assigned(KBLPTool) then begin
          if TProjLex(KBLPTool).FindSindroms(
            VirtControls.LastQuestion,
            VirtControls.LastAnswer)
            then begin
            if MessageDlg('���� ���� ���������. ��������� �������������� ����������������� ���������?',
              mtConfirmation, [mbYes, mbNo], 0) = mrYes then
            Reper.StartRepertImportLingv(Det_);
          end;
        end;
      end;}
      _ASSIGN_ATTR_CONTROL: begin
         for i:=1 to KB_.ListObjs.Count do begin
           if record_obj(KB_.ListObjs.Items[i-1]).n_obj=StrToInt(value2) then begin
             for j:=1 to record_obj(KB_.ListObjs.Items[i-1]).ListAttrs.Count do begin
               if record_attr(record_obj(KB_.ListObjs.Items[i-1]).ListAttrs.Items[j-1]).n_attr =
                 StrToInt(value3) then begin
                 cur_attr.Control:=value1;
                 break;
               end;
             end;
             break;
           end;
         end;
       end;
      _ASSIGN_MF_LIST: begin
        cur_type.MFControl:=value2;
      end;
      _ERROR:;
    end;
  end;

  procedure GenIncident(ptr: record_rule);
  begin
    gen_incident(ptr.list_attr_if,ptr.incident);
  end;

  procedure gen_incident(list_attr_if: TCollection;
                         incident: PChar);
  var
    PtrValue: list;
    p: PChar;
    Flag: Byte;
    i,n,l: Integer;
  begin
    if (list_attr_if<>NIL) and (list_attr_if.Count>0) then begin
      l:=StrLen(incident)-1;
      for i:=0 to l do incident[i]:=#0;
      l:=list_attr_if.Count-1;
      for i:=0 to 0 do begin
        PtrValue:=list(list_attr_if.Items[i]);
        n:=1;
        p:=incident;
        if ( PtrValue.n_layer=1 ) then begin
          StrPCopy(p,'P'+IntToStr(n));
          p:=p+StrLen(p);
          flag:=1;
        end else begin
          StrPCopy(p,'(P'+IntToStr(n)+')');
          p:=p+StrLen(p);
          flag:=2;
        end;
      end;
      for i:=1 to l do begin
        PtrValue:=list(list_attr_if.Items[i]);
        Inc(n);
        if ( PtrValue.n_layer=1 ) then begin
          if ( flag=2 ) then begin
            StrPCopy(p,')');
            p:=p+StrLen(p);
          end;
          StrPCopy(p,' & P'+IntToStr(n));
          p:=p+StrLen(p);
          flag:=1;
        end else begin
          if ( flag=1 ) then begin
            StrPCopy(p,' & (P'+IntToStr(n));
            p:=p+StrLen(p);
            flag:=2;
          end else begin
            StrPCopy(p,' | P'+IntToStr(n));
            p:=p+StrLen(p);
            flag:=2;
          end;
        end;
        if ( flag=2 ) then begin
            StrPCopy(p,')');
            p:=p+StrLen(p);
        end;
      end;
    end;
  end;

end.
