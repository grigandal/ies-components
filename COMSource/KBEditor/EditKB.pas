unit EditKB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RA_KBEditorForm, KBTypes, Aspr95;

type
  TKBEditor = class(TCustomKBEditor)
   public
    ShowWhat: string;
    function Run( Sender: TObject): TObject; override;
  end;

{  TKBEditorEditor = class(TKBToolEditor)
    public
      procedure Edit; override;
  end;
}
procedure Register;

var
  KBEditorForm: TKBEditorForm;

implementation

{$R *.res}

procedure Register;
begin
  RegisterComponents('KB Access', [TKBEditor]);
//  RegisterComponentEditor(TKBEditor, TKBEditorEditor);
end;

function TKBEditor.Run( Sender: TObject): TObject;
begin
  if Assigned(KBControl) then
  if Assigned(KBControl.KB) then
  begin
    KBEditorForm := TKBEditorForm.Create(Application);
    KBEditorForm.ShowWhat := ShowWhat;
    KBEditorForm.SetKB(KBControl.KB);
    if KBControl.KB.ProblemName = '' then KBEditorForm.Caption := '�������� ���� ������'
    else KBEditorForm.Caption := '�������� ���� ������' + ' - ' + KBControl.KB.ProblemName;
    KBEditorForm.ShowModal;
    KBEditorForm.Free;
  end
  else ShowMessage('���� ������ �� ����������');
end;
{
procedure TKBEditorEditor.Edit;
begin
  TKBEditor(Component).Run(Nil);
end;
}

initialization
  RegisterClasses([TKBEditor]);

end.

{
unit EditKB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls,
  DsgnIntf,
  KBTypes,
  Aspr95,
  RA_KBEditorForm;

type
  TKBEditorForm = class(TForm)
    KBEditorMainMenu: TMainMenu;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter1: TSplitter;
    KBMenuItem: TMenuItem;
    RuleMenuItem: TMenuItem;
    ObjectMenuItem: TMenuItem;
    TypeMenuItem: TMenuItem;
    HelpMenuItem: TMenuItem;
    LoadKBMenuItem: TMenuItem;
    SaveKBMenuItem: TMenuItem;
    ExitMenuItem: TMenuItem;
    AddRuleMenuItem: TMenuItem;
    EditRuleMenuItem: TMenuItem;
    DeleteRuleMenuItem: TMenuItem;
    AddObjectMenuItem: TMenuItem;
    EditObjectMenuItem: TMenuItem;
    DeleteObjectMenuItem: TMenuItem;
    AddTypeMenuItem: TMenuItem;
    AditTypeMenuItem: TMenuItem;
    DeleteTypeMenuItem: TMenuItem;
    N1: TMenuItem;
    procedure ExitMenuItemClick(Sender: TObject);
  private
  public
  end;

  TKBEditor = class(TCustomKBEditor)
    public
    function Run( Sender: TObject): TObject; override;
  end;

  TKBEditorEditor = class(TKBToolEditor)
    public
      procedure Edit; override;
  end;

var
  KBEditorForm: TKBEditorForm;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('KB Access', [TKBEditor]);
  RegisterComponentEditor(TKBEditor, TKBEditorEditor);
end;

function TKBEditor.Run( Sender: TObject): TObject;
begin
  For TKnowledgeBase editing use TKBEditor.KBControl.KB property
  Application.CreateForm(TKBEditorForm,KBEditorForm);
  KBEditorForm.ShowModal;
  KBEditorForm.Destroy;
end;

procedure TKBEditorForm.ExitMenuItemClick(Sender: TObject);
begin
  Close;
end;

procedure TKBEditorEditor.Edit;
begin
  TKBEditor(Component).Run(Nil);
end;
}

end.
