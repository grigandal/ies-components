unit RA_EditRuleForm;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, RA_KBEditorForm, EditKB, Aspr95, KBCompon,
  Dialogs;

type
  TEditRuleForm = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Label1: TLabel;
    EditName: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure EditNameChange(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditRuleForm: TEditRuleForm;
  CurRule: record_rule;

implementation

{$R *.DFM}

procedure TEditRuleForm.FormShow(Sender: TObject);
begin
 KBEditorForm.ActionRulesAdd.Enabled := false;
 KBEditorForm.ActionRulesDelete.Enabled := false;
 KBEditorForm.ComboBoxRules.Enabled := false;
 KBEditorForm.RadioGroupTypes.Enabled := false;
 //CurRule := record_rule(KBEditorForm.GetKB.ListRules.Items[KBEditorForm.ComboBoxRules.ItemIndex]);

end;

procedure TEditRuleForm.FormHide(Sender: TObject);
begin
 KBEditorForm.ActionRulesAdd.Enabled := true;
 KBEditorForm.ActionRulesDelete.Enabled := true;
 KBEditorForm.ComboBoxRules.Enabled := true;
 KBEditorForm.RadioGroupTypes.Enabled := true;

end;

procedure TEditRuleForm.EditNameChange(Sender: TObject);
begin
 //CurRule.comment := EditName.Text;
end;

procedure TEditRuleForm.OKBtnClick(Sender: TObject);
begin

 Close;
end;

procedure TEditRuleForm.CancelBtnClick(Sender: TObject);
begin
 Close;
end;

end.
