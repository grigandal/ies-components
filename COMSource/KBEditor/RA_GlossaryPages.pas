{***************************************************************}
{                                                               }
{       ������, ����������� �������� �������                    }
{                                                               }
{       1998 ����������� �. �.                                  }
{                                                               }
{***************************************************************}
unit RA_GlossaryPages;

interface

uses
  Classes, SysUtils, Forms, StdCtrls, Windows, Messages, Controls,
  KBCompon;

type

  // �����, ����������� �������� �������:
  TKBDBGlossaryPage = class(TAtDictionaryEntryItem)
  public
    FObjComment: string;
    FRecords: TOwnedCollection;
    constructor Create(Collection: TCollection); override;
  published
    property ObjComment: string read FObjComment write FObjComment;
    property Records: TOwnedCollection read FRecords write FRecords;
  end;

  // �����, ����������� ������ � �������:
  TKBDBGlossaryRecord = class(TAtDictionaryEntryItem)
  public
    FAttrComment: string;
    FFieldName: string;
  published
    property AttrComment: string read FAttrComment write FAttrComment;
    property FieldName: string read FFieldName write FFieldName;
  end;

  // �����, ����������� ��������� ������� � �������:
  TKBDBGlossaryPages = class(TKBCollection{TAtDictionary})
  public
    procedure Transfer(Source: TPersistent); // �������� �������� ������
  end;

implementation

procedure TKBDBGlossaryPages.Transfer(Source: TPersistent);
var
i, j: integer;
ASource: TKBDBGlossaryPages;
DestPage, SourcePage: TKBDBGlossaryPage;
ADestPage, ADestRec: TCollectionItem;
DestRec, SourceRec: TKBDBGlossaryRecord;
begin
  if (Source is TKBDBGlossaryPages) then
  begin
    ASource:=TKBDBGlossaryPages(Source);
    Clear;
    if ASource.Count>0 then
    for i:=0 to ASource.Count-1 do
    begin
      SourcePage:=TKBDBGlossaryPage(ASource.Items[i]);
      if SourcePage.Records.Count>0 then
      begin
        ADestPage:=Add;
        DestPage:=TKBDBGlossaryPage(ADestPage);
        DestPage.ObjComment:=SourcePage.ObjComment;
        for j:=0 to SourcePage.Records.Count-1 do
        begin
          SourceRec:=TKBDBGlossaryRecord(SourcePage.Records.Items[j]);
          if SourceRec.FieldName<>'' then
          begin
            ADestRec:=DestPage.Records.Add;
            DestRec:=TKBDBGlossaryRecord(ADestRec);
            DestRec.AttrComment:=SourceRec.AttrComment;
            DestRec.FieldName:=SourceRec.FieldName;
          end;
        end;
      end;
    end;
  end;
end;

constructor TKBDBGlossaryPage.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FRecords:=TOwnedCollection.Create(self, TKBDBGlossaryRecord);
end;

end.
