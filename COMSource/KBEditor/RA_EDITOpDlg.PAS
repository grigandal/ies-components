unit RA_EditOpDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls;

type
  TEditOpDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Panel1: TPanel;
    ImageOr: TImage;
    ImageAnd: TImage;
    RadioButtonOr: TRadioButton;
    RadioButtonAnd: TRadioButton;
    RadioButtonNot: TRadioButton;
    ImageNot: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditOpDlg: TEditOpDlg;

implementation

{$R *.DFM}

end.
