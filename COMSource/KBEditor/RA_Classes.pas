{***************************************************************}
{                                                               }
{       ������, ����������� �������� �������������� ��������    }
{       ��������� ��-���������� � �� ������������� ���          }
{                                                               }
{       1998 ����������� �. �.                                  }
{                                                               }
{***************************************************************}

unit RA_Classes;

interface

uses
{$IFDEF _DESIGNTIME}
  DesignEditors,
{$ENDIF}
  Classes, SysUtils, Aspr95, Forms, DB, StdCtrls, KBCompon,
  DesignIntf, Dialogs, Math, RA_GlossaryEditor, RA_KBDBIntEditor,
  Windows, Messages, Controls, RA_GlossaryPages, RA_DataSetEditor,
  Variants;

type

  // �����, ����������� ��������� ��� ��������������
  // ������� �� � ������ ����������
  TEditDataSet = class(TKBComponent)
  public
    FDataSet: TDataSet;
  published
    property DataSet: TDataSet read FDataSet write FDataSet;
  end;

{$IFDEF _DESIGNTIME}
  TEditDataSetEditor = class(TComponentEditor)
    procedure Edit; override;
  end;
{$ENDIF}

  // �����, ����������� �������:
  TKBDBGlossary = class(TAtDictionary)
  public
    FPages: TKBDBGlossaryPages;
    constructor Create(AOwner: TComponent); override;
  published
    property Pages: TKBDBGlossaryPages read FPages write FPages;
  end;
  
{$IFDEF _DESIGNTIME}
  // �����, ����������� �������� ���������� �������������� � ��:
  TKBDBInterfaceEditor = class(TComponentEditor)
    function GetVerbCount: integer; override;
    function GetVerb(Index: integer): string; override;
    procedure ExecuteVerb(Index: integer); override;
    procedure Edit; override;
  end;

  // �����, ����������� �������� ���������� �������:
  TKBDBGlossaryEditor = class(TComponentEditor)
    function GetVerbCount: integer; override;
    function GetVerb(Index: integer): string; override;
    procedure ExecuteVerb(Index: integer); override;
  end;
{$ENDIF}
  // �����, ����������� ��������� �������������� � ��:
  TKBDBInterface = class(TKBInterface)
  public
    FGlossary: TKBDBGlossary;
    FDataSet: TDataSet;
    procedure SetAttrValue( Source:TObject; var AValue:Variant;
                            var Success:Boolean ); override;
    procedure GetAttrValue( Source:TObject; var AValue:Variant;
                            var Success:Boolean ); override;
    procedure Design(Sender: TObject); override;
    procedure SetActive( Value: Boolean);
  published
    property Glossary: TKBDBGlossary read FGlossary write FGlossary;
    property DataSet: TDataSet read FDataSet write FDataSet;
    property Active: Boolean read FActive write SetActive;
  end;

  procedure Register;

implementation

{$R *.res}

procedure Register;
begin
  RegisterComponents('KB Interface', [TKBDBInterface, TKBDBGlossary, TEditDataSet]);
{$IFDEF _DESIGNTIME}
  RegisterComponentEditor(TKBDBInterface, TKBDBInterfaceEditor);
  RegisterComponentEditor(TKBDBGlossary, TKBDBGlossaryEditor);
  RegisterComponentEditor(TEditDataSet, TEditDataSetEditor);
{$ENDIF}
end;

constructor TKBDBGlossary.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPages:=TKBDBGlossaryPages.Create({self,} TKBDBGlossaryPage);
end;

{$IFDEF _DESIGNTIME}
function TKBDBInterfaceEditor.GetVerbCount: Integer;
begin
  Result:=1;
end;

function TKBDBInterfaceEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0: Result:='Edit KBDBInterface...';
  end;
end;

procedure TEditDataSetEditor.Edit;
var
Dialog: TDataSetEditForm;
begin
  Dialog:=TDataSetEditForm.Create(Application);
  Dialog.DataSource.DataSet:=TEditDataSet(Component).DataSet;
  if Dialog.ShowModal=mrOK then
  begin
    Dialog.Free;
  end
  else Dialog.Free;
end;

procedure TKBDBInterfaceEditor.ExecuteVerb(Index: Integer);
var
i: integer;
Dialog: TKBDBIntEditorForm;
ChosenGlossary, ChosenDataSet: TComponent;
begin
  Dialog:=TKBDBIntEditorForm.Create(Application);
  with Component.Owner do
  begin
    for i:=0 to ComponentCount-1 do
    begin
      if (Components[i] is TKBDBGlossary) then
        Dialog.CB_Glossary.Items.Add(Components[i].Name);
      if (Components[i] is TDataSet) then
        Dialog.CB_DataSet.Items.Add(Components[i].Name);
    end;
  end;
  if Dialog.ShowModal=mrOK then
  begin
    ChosenGlossary:=Component.Owner.FindComponent(Dialog.CB_Glossary.Text);
    TKBDBInterface(Component).Glossary:=TKBDBGlossary(ChosenGlossary);
    ChosenDataSet:=Component.Owner.FindComponent(Dialog.CB_DataSet.Text);
    TKBDBInterface(Component).DataSet:=TDataSet(ChosenDataSet);
    Designer.Modified;
    Dialog.Free;
  end
  else Dialog.Free;
end;

function TKBDBGlossaryEditor.GetVerbCount: Integer;
begin
  Result:=1;
end;

function TKBDBGlossaryEditor.GetVerb(Index: Integer): string;
begin
  case Index of
    0: Result:='Edit KBDBGlossary...';
  end;
end;

procedure TKBDBGlossaryEditor.ExecuteVerb(Index: Integer);
var
i: integer;
Dialog: TFormDlg;
begin
  Dialog:=TFormDlg.Create(Application);
  Dialog.Container:=TForm(Component.Owner);
  Dialog.Pages.Transfer(TKBDBGlossary(Component).Pages);
  Dialog.ObjListRefresh;
  with Component.Owner do
  begin
    for i:=0 to ComponentCount-1 do
    begin
      if (Components[i] is TDataSet) then
        Dialog.CB_DataSet.Items.Add(Components[i].Name);
      if (Components[i] is TKnowledgeBase) then
        Dialog.CB_KB.Items.Add(Components[i].Name);
    end;
  end;
  if Dialog.ShowModal=mrOK then
  begin
    TKBDBGlossary(Component).Pages.Transfer(Dialog.Pages);
    Designer.Modified;
    Dialog.Free;
  end
  else Dialog.Free;
end;
{$ENDIF}

procedure TKBDBInterface.SetAttrValue( Source:TObject; var AValue:Variant;
                                       var Success:Boolean );
var
i,j: integer;
CurAttr: record_attr;
CurRecord: TKBDBGlossaryRecord;
Page: TKBDBGlossaryPage;
PtrObj: record_obj;
begin
  Page:=nil;
  if (Source is record_attr) then PtrObj:=record_attr(Source).PtrObj;
  if (Source is record_obj) then PtrObj:=record_obj(Source);
  begin
    // ����� ������ �������� � �������
    if Glossary.Pages.Count>0 then
      for i:=0 to Glossary.Pages.Count-1 do
        if TKBDBGlossaryPage(Glossary.Pages.Items[i]).ObjComment=PtrObj.comment
        then Page:=TKBDBGlossaryPage(Glossary.Pages.Items[i]);
    if Page<>nil then
    // �������� ����������
    begin
      if Page.Records.Count>0 then
        for i:=0 to Page.Records.Count-1 do
          if PtrObj.FListAttrs.Count>0 then
          for j:=0 to PtrObj.FListAttrs.Count-1 do
          begin
            CurAttr:=record_attr(PtrObj.FListAttrs.Items[j]);
            CurRecord:=TKBDBGlossaryRecord(Page.Records.Items[i]);
            if CurRecord.AttrComment=CurAttr.comment then
            begin
              TAssertion(CurAttr.AssertionCollection.Items[0]).znach:=VarToStr(DataSet.FieldValues[CurRecord.FieldName]);
              CurAttr.status:=6;
            end;
          end;
    end;
  end;
end;

procedure TKBDBInterface.GetAttrValue( Source:TObject; var AValue:Variant; var Success:Boolean );
var
i,j: integer;
CurAttr: record_attr;
CurRecord: TKBDBGlossaryRecord;
Page: TKBDBGlossaryPage;
PtrObj: record_obj;
begin
  Page:=nil;
  if (Source is record_attr) then PtrObj:=record_attr(Source).PtrObj;
  if (Source is record_obj) then PtrObj:=record_obj(Source);
  begin
    // ����� ������ �������� � �������
    if Glossary.Pages.Count>0 then
      for i:=0 to Glossary.Pages.Count-1 do
        if TKBDBGlossaryPage(Glossary.Pages.Items[i]).ObjComment=PtrObj.comment
        then Page:=TKBDBGlossaryPage(Glossary.Pages.Items[i]);
    if Page<>nil then
    // �������� ����������
    begin
      if Page.Records.Count>0 then
        for i:=0 to Page.Records.Count-1 do
          if PtrObj.FListAttrs.Count>0 then
          for j:=0 to PtrObj.FListAttrs.Count-1 do
          begin
            CurAttr:=record_attr(PtrObj.FListAttrs.Items[j]);
            CurRecord:=TKBDBGlossaryRecord(Page.Records.Items[i]);
            if CurRecord.AttrComment=CurAttr.comment then
            begin
              DataSet.Edit;
              DataSet.FieldValues[CurRecord.FieldName]:=TAssertion(CurAttr.AssertionCollection.Items[0]).znach;
              DataSet.Post;
            end;
          end;
    end;
  end;
end;

{$IFDEF _DESIGNTIME}
procedure TKBDBInterfaceEditor.Edit;
begin
  if Component is TKBDBInterface then
    TKBDBInterface(Component).Design(Self);
end;
{$ENDIF}

procedure TKBDBInterface.Design(Sender: TObject);
var
  i: integer;
  Dialog: TKBDBIntEditorForm;
  ChosenGlossary, ChosenDataSet: TComponent;
begin
  Dialog:=TKBDBIntEditorForm.Create(Application);
  with Self do
  begin
    for i:=0 to ComponentCount-1 do
    begin
      if (Components[i] is TKBDBGlossary) then
        Dialog.CB_Glossary.Items.Add(Components[i].Name);
      if (Components[i] is TDataSet) then
        Dialog.CB_DataSet.Items.Add(Components[i].Name);
    end;
  end;
  if Dialog.ShowModal=mrOK then
  begin
    ChosenGlossary:=Self.Owner.FindComponent(Dialog.CB_Glossary.Text);
    TKBDBInterface(Self).Glossary:=TKBDBGlossary(ChosenGlossary);
    ChosenDataSet:=Self.Owner.FindComponent(Dialog.CB_DataSet.Text);
    TKBDBInterface(Self).DataSet:=TDataSet(ChosenDataSet);
//    Designer.Modified;
    Dialog.Free;
  end
  else Dialog.Free;
end;

procedure TKBDBInterface.SetActive( Value: Boolean);
begin
  if Value then begin
    FActive:=True;
    Design(Self);
    FActive:=False;
  end;
end;

end.
