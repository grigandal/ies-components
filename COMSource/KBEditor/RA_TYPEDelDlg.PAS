unit RA_TypeDelDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Aspr95, KBCompon, Dialogs;

type
  TTypeDelDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    LabelWarning: TLabel;
    ListBoxAttribs: TListBox;
    RadioGroupAsk: TRadioGroup;
    ComboBoxTypes: TComboBox;
    Image1: TImage;
    procedure RadioGroupAskClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    ListAttrs: TList;
    Types: TList;
    { Public declarations }
  end;

var
  TypeDelDlg: TTypeDelDlg;

implementation

{$R *.DFM}

procedure TTypeDelDlg.RadioGroupAskClick(Sender: TObject);
begin
 if (RadioGroupAsk.ItemIndex = 1) and (ComboBoxTypes.Items.Count > 0) then ComboBoxTypes.Visible := true
 else ComboBoxTypes.Visible := false;
end;

procedure TTypeDelDlg.OKBtnClick(Sender: TObject);
begin
 if ListBoxAttribs.Items.Count > 0 then begin
  if ListBoxAttribs.ItemIndex >= 0 then
   if RadioGroupAsk.ItemIndex = 0 then begin
    record_attr(ListAttrs.Items[ListBoxAttribs.ItemIndex]).Destroy;
    ListAttrs.Delete(ListBoxAttribs.ItemIndex);
    ListBoxAttribs.Items.Delete(ListBoxAttribs.ItemIndex);
    if ListBoxAttribs.Items.Count = 0 then ModalResult := mrOk;
   end else if ComboBoxTypes.ItemIndex >=0 then begin
    record_attr(ListAttrs.Items[ListBoxAttribs.ItemIndex]).n_type := record_type(Types.Items[ComboBoxTypes.ItemIndex]).n_type;
    ListAttrs.Delete(ListBoxAttribs.ItemIndex);
    ListBoxAttribs.Items.Delete(ListBoxAttribs.ItemIndex);
    if ListBoxAttribs.Items.Count = 0 then ModalResult := mrOk;
   end;
   ListBoxAttribs.ItemIndex := 0;
 end else begin
  ModalResult := mrOk;
 end;
end;

end.
