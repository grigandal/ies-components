{***************************************************************}
{                                                               }
{       �����, ����������� �����, ���������� ����������         }
{       ���������� �������                                      }
{                                                               }
{       1998 ����������� �. �.                                  }
{                                                               }
{***************************************************************}

unit RA_GlossaryEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DesignIntf, Buttons, DB, Grids, DBGrids, Aspr95, CheckLst,
  ComCtrls, Menus, ExtCtrls, ToolWin, KBCompon, RA_GlossaryPages, ImgList;

type
  TFormDlg = class(TForm)
    OKButton: TBitBtn;
    CancelButton: TBitBtn;
    ToolBar1: TToolBar;
    TB_New: TToolButton;
    Bevel1: TBevel;
    L_Dataset: TLabel;
    L_KB: TLabel;
    CB_DataSet: TComboBox;
    CB_KB: TComboBox;
    GB_KB: TGroupBox;
    GB_ObjList: TGroupBox;
    LB_ObjList: TListBox;
    GB_AttrList: TGroupBox;
    LV_AttrList: TListView;
    GB_Fields: TGroupBox;
    LB_Fields: TListBox;
    ImageList1: TImageList;
    TB_Open: TToolButton;
    TB_Save: TToolButton;
    ToolButton3: TToolButton;
    TB_Help: TToolButton;
    OpenDialog: TOpenDialog;
    procedure CB_DataSetChange(Sender: TObject);
    procedure ObjListRefresh;
    procedure AttrListRefresh;
    procedure LB_ObjListClick(Sender: TObject);
    procedure CB_KBChange(Sender: TObject);
    procedure LV_AttrListDblClick(Sender: TObject);
    procedure EnableBoxes;
    procedure DisableBoxes;
    procedure TB_NewClick(Sender: TObject);
    procedure FillPages;
    procedure FormCreate(Sender: TObject);
    procedure TB_OpenClick(Sender: TObject);
  private
    { Private declarations }
  public
    Container: TForm;  // ������ �� �����, ���������� �������
    KB: TKnowledgeBase; // ������ �� KB, ��������� � ���������� ������
    Mode: integer;  //  ����� ������ �������:
                    //  0 - ����� ����������� �������,
                    //  1 - ����� ����������� KB
    Pages: TKBDBGlossaryPages;  // ����� ������� �������
  end;

implementation

{$R *.DFM}

procedure TFormDlg.FillPages;  // ��������� �������� ���������� KB
var
i,j: integer;
NewPage: TKBDBGlossaryPage;
NewRecord: TKBDBGlossaryRecord;
CurObject: record_obj;
CurAttr: record_attr;
begin
  Pages.Clear;
  if KB.ListObjs.Count>0 then
    for i:=0 to KB.ListObjs.Count-1 do
    begin
      TCollectionItem(NewPage):=Pages.Add;
      CurObject:=record_obj(KB.ListObjs.Items[i]);
      NewPage.ObjComment:=CurObject.Comment;
      if CurObject.FListAttrs.Count>0 then
      for j:=0 to CurObject.FListAttrs.Count-1 do
      begin
        CurAttr:=record_attr(CurObject.FListAttrs.Items[j]);
        TCollectionItem(NewRecord):=NewPage.Records.Add;
        Newrecord.AttrComment:=CurAttr.comment;
      end;
    end;
end;

procedure TFormDlg.EnableBoxes;
begin
  L_DataSet.Enabled:=True;
  L_KB.Enabled:=True;
  CB_DataSet.Enabled:=True;
  CB_KB.Enabled:=True;
end;

procedure TFormDlg.DisableBoxes;
begin
  L_DataSet.Enabled:=False;
  L_KB.Enabled:=False;
  CB_DataSet.Enabled:=False;
  CB_KB.Enabled:=False;
end;

procedure TFormDlg.ObjListRefresh;
var
i: integer;
begin
  LB_ObjList.Clear;
  case Mode of
  0:
    if Pages.Count>0 then
    for i:=0 to Pages.Count-1 do
    begin
      LB_ObjList.Items.Add(TKBDBGlossaryPage(Pages.Items[i]).ObjComment);
    end;
  1:
    begin
      FillPages;
      if KB.ListObjs.Count>0 then
      for i:=0 to KB.ListObjs.Count-1 do
        begin
          LB_ObjList.Items.Add(record_obj(KB.ListObjs.Items[i]).Comment);
        end;
      end;
    end;
end;

procedure TFormDlg.AttrListRefresh;
var
i: integer;
TempListItem: TListItem;
begin
  case Mode of
  0:
    if Pages.Count>0 then
    begin
      LV_AttrList.Items.Clear;
      with TKBDBGlossaryPage(Pages.Items[LB_ObjList.ItemIndex]).Records do
        if Count>0 then
          for i:=0 to Count-1 do
          begin
            TempListItem:=LV_AttrList.Items.Add;
            TempListItem.Caption:=TKBDBGlossaryRecord(Items[i]).AttrComment;
            TempListItem.SubItems.Clear;
            TempListItem.SubItems.Add(TKBDBGlossaryRecord(Items[i]).FieldName);
          end;
    end;
  1:
    if KB.ListObjs.Count>0 then
    begin
      LV_AttrList.Items.Clear;
      with record_obj(KB.ListObjs.Items[LB_ObjList.ItemIndex]).FListAttrs do
        if Count>0 then
          for i:=0 to Count-1 do
          begin
            TempListItem:=LV_AttrList.Items.Add;
            TempListItem.Caption:=record_attr(Items[i]).Comment;
          end;
    end;
  end;
end;

procedure TFormDlg.LB_ObjListClick(Sender: TObject);
begin
  if Mode=0 then
    AttrListRefresh
  else
    if CB_KB.Text<>'' then
    begin
      AttrListRefresh;
    end;
end;

procedure TFormDlg.CB_DataSetChange(Sender: TObject);
var
i: integer;
ChosenDataSet: TDataSet;
begin
  ChosenDataSet:=TDataSet(Container.FindComponent(CB_DataSet.Text));
  LB_Fields.Clear;
  for i:=0 to ChosenDataSet.FieldCount-1 do
  begin
    if ChosenDataSet.DefaultFields then
      LB_Fields.Items.Add(ChosenDataSet.Fields[i].FieldName);
  end;
end;

procedure TFormDlg.CB_KBChange(Sender: TObject);
begin
  KB:=TKnowledgeBase(Container.FindComponent(CB_KB.Text));
  ObjListRefresh;
end;

procedure TFormDlg.LV_AttrListDblClick(Sender: TObject);
begin
  if (assigned(LV_AttrList.Selected)) and (LB_Fields.Items.Count>0) then
  begin
    LV_AttrList.Selected.SubItems.Clear;
    LV_AttrList.Selected.SubItems.
      Add(LB_Fields.Items.Strings[LB_Fields.ItemIndex]);
    TKBDBGlossaryRecord(TKBDBGlossaryPage(Pages.Items[LB_ObjList.ItemIndex]).
      Records.Items[LV_AttrList.Selected.Index]).FieldName:=
        LB_Fields.Items.Strings[LB_Fields.ItemIndex];
  end;
end;

procedure TFormDlg.TB_NewClick(Sender: TObject);
begin
  Pages.Clear;
  LB_ObjList.Clear;
  LB_Fields.Clear;
  LV_AttrList.Items.Clear;
  CB_KB.Text:='';
  CB_DataSet.Text:='';
  Mode:=1;
  EnableBoxes;
end;

procedure TFormDlg.FormCreate(Sender: TObject);
begin
  Pages:=TKBDBGlossaryPages.Create({self,} TKBDBGlossaryPage);
end;

procedure TFormDlg.TB_OpenClick(Sender: TObject);
begin
  if OpenDialog.Execute then ShowMessage('���� ������');
end;

end.
