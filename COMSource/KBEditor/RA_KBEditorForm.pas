unit RA_KBEditorForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Aspr95, StdCtrls, ImgList, KBCompon, Menus,
  Buttons, ExtCtrls, slstbox, ToolWin, ActnList, RA_AttribDlg, RA_TypeDlg,
  RA_YesNoDlg, RA_TypeDelDlg, RA_InputDlg, RA_CurList, RA_EditOpDlg;

type
  TKBEditorForm = class(TForm)
    ImageListStuff: TImageList;
    ImageListRules: TImageList;
    ListBoxObjects: TSectionListBox;
    ListBoxAttribs: TSectionListBox;
    ListBoxValues: TSectionListBox;
    PanelData: TPanel;
    PanelRules: TPanel;
    PanelAttribs: TPanel;
    Splitter3: TSplitter;
    Splitter2: TSplitter;
    Splitter1: TSplitter;
    Splitter4: TSplitter;
    ControlBarMenu: TControlBar;
    ToolBarMenu: TToolBar;
    ToolButtonBase: TToolButton;
    PopupMenuBase: TPopupMenu;
    MenuBaseExit: TMenuItem;
    ActionList: TActionList;
    ActionExit: TAction;
    ImageListMenus: TImageList;
    PopupMenuObjects: TPopupMenu;
    ActionObjectAdd: TAction;
    ActionObjectEdit: TAction;
    ActionObjectDelete: TAction;
    ToolButtonObjects: TToolButton;
    MenuObjectsAdd: TMenuItem;
    MenuObjectsEdit: TMenuItem;
    MenuObjectsDelete: TMenuItem;
    ActionAttribAdd: TAction;
    ActionAttribEdit: TAction;
    ActionAttribDelete: TAction;
    ActionTypeAdd: TAction;
    ActionTypeEdit: TAction;
    ActionTypeDelete: TAction;
    ActionValueAdd: TAction;
    ActionValueEdit: TAction;
    ActionValueDelete: TAction;
    PopupMenuAttribs: TPopupMenu;
    MenuAttribsAdd: TMenuItem;
    MenuAttribsEdit: TMenuItem;
    MenuAttribsDelete: TMenuItem;
    PopupMenuTypes: TPopupMenu;
    MenuTypesAdd: TMenuItem;
    MenuTypesEdit: TMenuItem;
    MenuTypesDelete: TMenuItem;
    PopupMenuValues: TPopupMenu;
    MenuValuesAdd: TMenuItem;
    MenuValuesEdit: TMenuItem;
    MenuValuesDelete: TMenuItem;
    ToolButtonAttribs: TToolButton;
    ToolButtonTypes: TToolButton;
    ToolButtonValues: TToolButton;
    PopupMenuRules: TPopupMenu;
    ToolButtonRules: TToolButton;
    MenuBaseSeparator: TMenuItem;
    MenuBaseSave: TMenuItem;
    RadioGroupTypes: TRadioGroup;
    ListBoxTypes: TSectionListBox;
    MenuObjectsSearch: TMenuItem;
    ActionObjectSearch: TAction;
    ActionAttribSearch: TAction;
    MenuAttribsSearch: TMenuItem;
    ActionTypeSearch: TAction;
    MenuTypesSearch: TMenuItem;
    TreeRule: TTreeView;
    ActionRulesAdd: TAction;
    MenuRulesAdd: TMenuItem;
    MenuRulesDelete: TMenuItem;
    ActionRulesDelete: TAction;
    ActionRulesEdit: TAction;
    MenuRulesEdit: TMenuItem;
    PopupMenuRuleNode: TPopupMenu;
    ActionRuleNodeEdit: TAction;
    ActionRuleNodeDelete: TAction;
    ActionRuleNodeAddOp: TAction;
    ActionRuleNodeAddExp: TAction;
    MenuRuleNodeAddOp: TMenuItem;
    MenuRuleNodeAddExp: TMenuItem;
    MenuRuleNodeEdit: TMenuItem;
    MenuRuleNodeDelete: TMenuItem;
    MenuRuleNodeSeparator: TMenuItem;
    MenuObjectsSeparator: TMenuItem;
    MenuAttribsSeparator: TMenuItem;
    MenuTypesSeparator: TMenuItem;
    StatusBarMain: TStatusBar;
    ActionRulesCopy: TAction;
    PanelRulesCombo: TPanel;
    LabelRules: TLabel;
    Image1: TImage;
    ButtonRulesOk: TSpeedButton;
    ButtonRulesCancel: TSpeedButton;
    ButtonRulesEdit: TSpeedButton;
    LabelRuleName: TLabel;
    ComboBoxRules: TComboBox;
    EditRuleName: TEdit;
    MenuRulesCopy: TMenuItem;
    N1: TMenuItem;
    ToolBarSearch: TToolBar;
    ToolButtonSearchPrev: TToolButton;
    ToolButtonSearchNext: TToolButton;
    ActionSearchNext: TAction;
    ActionSearchPrev: TAction;
    ToolButtonSearch: TToolButton;
    SaveKBDialog: TSaveDialog;

    procedure RefreshKBViewer;
    procedure RefreshObjects;
    procedure RefreshAttribs;
    procedure RefreshTypes;
    procedure RefreshValues;
    procedure RefreshRules;
    procedure RefreshRule(n: integer);
    procedure MakeNumbers;
    procedure FixTypeNumbers(f: integer);
    procedure Status(const f: string; const a: array of const);
    function Search(what: integer): boolean;
    procedure MakeTree(tree: TTreeNodes; rule: record_rule);
    procedure IncidentToTree(rule: record_rule; s: string; parent: TTreeNode);
    function GetArgsNum(s: string; var op: char): integer;
    function GetArg(s: string; num: integer): string;
    procedure RemoveBrackets(var s: string);
    function ArgToStr(rule: record_rule; s: string): string;
    procedure MakeRulesNumbers;
    function GetNumFromStr(s: string): integer;
    procedure RulesEditEnd(Sender: TObject);
    procedure RefreshNode(node: TTreeNode);
    procedure MakeRuleNumbers(rule: record_rule);
    procedure DeleteNode(node: TTreeNode);
    procedure ReindexNode(node: TTreeNode; n: integer);
    function CheckRuleTree: integer;
    function GetRuleString(node: TTreeNode): string;
    function GetSearchName: string;
    procedure RuleAssign(D,S: record_rule);
    procedure ListAssign(D,S: list);
    procedure RefreshStatus;

    function  InputAttribInfo(const ACaption: string; var comment: string; var ntype: integer): boolean;
    function  InputTypeInfo(const ACaption: string; var comment: string; var typeval: integer; var smin: string; var smax: string): boolean;
    function  AssertionToStr(ass: TAssertion): string;
    function  ConsequentToStr(rule: record_rule; i: integer): string;
    function  ConsequentElseToStr(rule: record_rule; i: integer): string;
    function  MessageYesNo(Text,Caption: string): integer;
    function  InputString(const Caption, Prompt: string; var Value: string): boolean;
    procedure FormShow(Sender: TObject);
    procedure ListBoxObjectsClick(Sender: TObject);
    procedure ListBoxAttribsClick(Sender: TObject);
    procedure ComboBoxRulesClick(Sender: TObject);
    procedure RadioGroupTypesClick(Sender: TObject);
    procedure ListBoxTypesClick(Sender: TObject);
    procedure ActionExitExecute(Sender: TObject);
    procedure ActionObjectAddExecute(Sender: TObject);
    procedure ActionObjectEditExecute(Sender: TObject);
    procedure ActionObjectDeleteExecute(Sender: TObject);
    procedure ActionAttribAddExecute(Sender: TObject);
    procedure ActionAttribDeleteExecute(Sender: TObject);
    procedure ActionAttribEditExecue(Sender: TObject);
    procedure ActionTypeAddExecute(Sender: TObject);
    procedure ActionTypeDeleteExecute(Sender: TObject);
    procedure ActionTypeEditExecute(Sender: TObject);
    procedure ActionValueAddExecute(Sender: TObject);
    procedure ActionValueDeleteExecute(Sender: TObject);
    procedure ActionValueEditExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuBaseSaveClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ListBoxValuesClick(Sender: TObject);
    procedure ActionObjectSearchExecute(Sender: TObject);
    procedure ActionAttribSearchExecute(Sender: TObject);
    procedure ActionTypeSearchExecute(Sender: TObject);
    procedure ActionRulesDeleteExecute(Sender: TObject);
    procedure ActionRulesAddExecute(Sender: TObject);
    procedure ActionRulesEditExecute(Sender: TObject);
    procedure TreeRuleMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ButtonRulesOkClick(Sender: TObject);
    procedure ButtonRulesCancelClick(Sender: TObject);
    procedure ComboBoxRulesChange(Sender: TObject);
    procedure PopupMenuRuleNodePopup(Sender: TObject);
    procedure ActionRuleNodeAddOpExecute(Sender: TObject);
    procedure ActionRuleNodeAddExpExecute(Sender: TObject);
    procedure ActionRuleNodeEditExecute(Sender: TObject);
    procedure ActionRuleNodeDeleteExecute(Sender: TObject);
    procedure TreeRuleChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ListBoxObjectsDblClick(Sender: TObject);
    procedure ListBoxObjectsSubItemRClick(Sender: TObject; sectionidx,
      subitemidx: Integer);
    procedure ListBoxAttribsSubItemRClick(Sender: TObject; sectionidx,
      subitemidx: Integer);
    procedure ListBoxTypesSubItemRClick(Sender: TObject; sectionidx,
      subitemidx: Integer);
    procedure ListBoxValuesSubItemRClick(Sender: TObject; sectionidx,
      subitemidx: Integer);
    procedure TreeRuleDblClick(Sender: TObject);
    procedure ListBoxObjectsEnter(Sender: TObject);
    procedure TreeRuleEnter(Sender: TObject);
    procedure ActionRulesCopyExecute(Sender: TObject);
    procedure ActionSearchNextExecute(Sender: TObject);
    procedure ActionSearchPrevExecute(Sender: TObject);
    procedure ToolButtonSearchClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    KB: TKnowledgeBase;
    CurList: TCurList;
    SearchList: TList;
    SearchMode: Integer;
    SearchIndex: integer;
    SearchString: string;
  public
    ShowWhat: string;
    procedure MarkListBoxItems(box: TSectionListBox; s: string);
    procedure SetKB(AKB: TKnowledgeBase);
    function GetKB: TKnowledgeBase;
    procedure SearchGoTo;
    procedure ShowItem(what: string);
  end;

var
 CurFocus: integer;
 TmpRule: record_rule;
 CurAssert: list;
 RuleTreeError: array[0..4] of string = ('��� � �������',
  '������������ ��������� � ������� ����',
  '������������ ��������� � ������� �����',
  '����������� ������ ���� ��������� ��� �������� � ������ ������� ����',
  '������������ ��������� (����� 2-�)');

implementation

uses RA_Search, RA_EditRuleExp;

{$R *.DFM}

procedure TKBEditorForm.RefreshObjects;
var n: integer;
begin
 n := CurList.ObjN;
 CurList.Mode := CurList.Mode;
 if n < CurList.ListObjs.Count then CurList.ObjN := n;
 RefreshKBViewer;
end;

procedure TKBEditorForm.RefreshAttribs;
begin
 CurList.RefreshAttribs(CurList.AttribN);
 RefreshKBViewer;
end;

procedure TKBEditorForm.RefreshTypes;
begin
 CurList.RefreshTypes(CurList.TypeN);
 RefreshKBViewer;
end;

procedure TKBEditorForm.RefreshValues;
var n: integer;
begin
 n := CurList.ValueN;
 CurList.TypeN := CurList.TypeN;
 if n < CurList.ListValues.Count then CurList.ValueN := n;
 RefreshKBViewer;
end;

procedure TKBEditorForm.SetKB(AKB: TKnowledgeBase);
begin
  KB:=AKB;
end;

function TKBEditorForm.GetKB: TKnowledgeBase;
begin
  Result:=KB;
end;

procedure TKBEditorForm.RefreshKBViewer;
var
 i,n: integer;
 CurType: record_type;
 CurValue: value;
 s: string;
 tmp: TSectionListBox;
begin
 ListBoxObjects.BeginUpdate;
 ListBoxAttribs.BeginUpdate;
 ListBoxTypes.BeginUpdate;
 ListBoxValues.BeginUpdate;

 ListBoxObjects.Sections.Items[0].SubItems.Clear;
 ListBoxAttribs.Sections.Items[0].SubItems.Clear;
 ListBoxTypes.Sections.Items[0].SubItems.Clear;
 ListBoxValues.Sections.Items[0].SubItems.Clear;

 for i := 0 to CurList.ListObjs.Count - 1 do
  ListBoxObjects.Sections.Items[0].SubItems.Add(CurList.nObj[i].comment);
 ListBoxObjects.ItemIndex := CurList.ObjN + 1;

 for i := 0 to CurList.ListAttribs.Count - 1 do
  ListBoxAttribs.Sections.Items[0].SubItems.Add(CurList.nAttrib[i].comment);
 ListBoxAttribs.ItemIndex := CurList.AttribN + 1;

 for i := 0 to CurList.ListTypes.Count - 1 do
  ListBoxTypes.Sections.Items[0].SubItems.Add(CurList.nType[i].comment);
 ListBoxTypes.ItemIndex := CurList.TypeN + 1;

 ListBoxValues.Sections.Items[0].Caption := '��������';
 CurType := CurList.GetCurType;
 if CurType <> nil then begin
  case CurType.type_znach of
   0,2: s := ' (���������� ���)';
   1: s := ' (�������� ���)';
   3: s := ' (�������� ���)';
  else
   s := '';
  end;
  ListBoxValues.Sections.Items[0].Caption := ListBoxValues.Sections.Items[0].Caption + s;
  if CurType.type_znach = 1 then begin
   if CurType.min_znach = CurType.max_znach then
    s := '-N..N'
   else
    s := CurType.min_znach + '..' + CurType.max_znach;
   ListBoxValues.Sections.Items[0].SubItems.Add(s);
  end else begin
   for i := 0 to CurType.list_znach.Count - 1 do begin
    CurValue := value(CurType.list_znach.Items[i]);
    s := CurValue.znach;
    if s = '' then s := '������������';
    ListBoxValues.Sections.Items[0].SubItems.Add(s);
   end;
   ListBoxValues.ItemIndex := CurList.ValueN + 1;
  end;
 end;

 n := ListBoxObjects.Sections.Items[0].SubItems.Count;
 if n = 0 then
  ListBoxObjects.Sections.Items[0].Caption := '�������'
 else if ListBoxObjects.ItemIndex > 0 then
  ListBoxObjects.Sections.Items[0].Caption := '������� (' + IntToStr(ListBoxObjects.ItemIndex) + '/' + IntToStr(n) + ')'
 else
  ListBoxObjects.Sections.Items[0].Caption := '������� (' + IntToStr(n) + ')';

 n := ListBoxAttribs.Sections.Items[0].SubItems.Count;
 if n = 0 then
  ListBoxAttribs.Sections.Items[0].Caption := '��������'
 else if ListBoxAttribs.ItemIndex > 0 then
  ListBoxAttribs.Sections.Items[0].Caption := '�������� (' + IntToStr(ListBoxAttribs.ItemIndex) + '/' + IntToStr(n) + ')'
 else
  ListBoxAttribs.Sections.Items[0].Caption := '�������� (' + IntToStr(n) + ')';

 n := ListBoxTypes.Sections.Items[0].SubItems.Count;
 if n = 0 then
  ListBoxTypes.Sections.Items[0].Caption := '����'
 else if ListBoxTypes.ItemIndex > 0 then
  ListBoxTypes.Sections.Items[0].Caption := '���� (' + IntToStr(ListBoxTypes.ItemIndex) + '/' + IntToStr(n) + ')'
 else
  ListBoxTypes.Sections.Items[0].Caption := '���� (' + IntToStr(n) + ')';

 if (CurFocus = SearchMode) and (SearchList.Count > 0) then begin
  case SearchMode of
   1: tmp := ListBoxObjects;
   2: tmp := ListBoxAttribs;
   3: tmp := ListBoxTypes;
  else
   tmp := nil;
  end;
  MarkListBoxItems(tmp, SearchString);
 end;
 
 ListBoxObjects.EndUpdate;
 ListBoxAttribs.EndUpdate;
 ListBoxTypes.EndUpdate;
 ListBoxValues.EndUpdate;
end;

procedure TKBEditorForm.FormShow(Sender: TObject);
begin
 if Assigned(KB) then begin
  CurList.KB := KB;
  CurList.Mode := RadioGroupTypes.ItemIndex;
  MakeNumbers;
  RadioGroupTypesClick(Self);
  RefreshRules;
  RefreshStatus;
 end else begin
  ShowMessage('���� ������ �� ����������');
  Close;
 end;
end;

procedure TKBEditorForm.ListBoxObjectsClick(Sender: TObject);
begin
 if CurFocus <> 1 then begin
  ListBoxObjects.Sections.Items[0].Lines := slNone;
  ListBoxAttribs.Sections.Items[0].Lines := slHorizontal;
  ListBoxTypes.Sections.Items[0].Lines := slHorizontal;
  CurList.RefreshObjects(ListBoxObjects.ItemIndex - 1);
 end else CurList.ObjN := ListBoxObjects.ItemIndex - 1;
 CurFocus := 1;
 RefreshKBViewer;
end;

procedure TKBEditorForm.ListBoxAttribsClick(Sender: TObject);
begin
 if (CurList.Mode = 0) and (CurFocus <> 2) then begin
  ListBoxObjects.Sections.Items[0].Lines := slNone;
  ListBoxAttribs.Sections.Items[0].Lines := slHorizontal;
  ListBoxTypes.Sections.Items[0].Lines := slHorizontal;
  CurList.RefreshAttribs(ListBoxAttribs.ItemIndex - 1);
 end else CurList.AttribN := ListBoxAttribs.ItemIndex - 1;
 CurFocus := 2;
 RefreshKBViewer;
end;

procedure TKBEditorForm.RefreshRules;
var
 CurRule: record_rule;
 i,t: integer;
 //tmpnode,tmpnode1: TTreeNode;
begin
 t := ComboBoxRules.ItemIndex;
 ComboBoxRules.Items.Clear;
 for i := 0 to KB.ListRules.Count - 1 do begin
  CurRule := record_rule(KB.ListRules.Items[i]);
  ComboBoxRules.Items.Add(CurRule.comment);
 end;
 if (t < ComboBoxRules.Items.Count) and (t >= 0) then ComboBoxRules.ItemIndex := t
 else if ComboBoxRules.Items.Count > 0 then ComboBoxRules.ItemIndex := 0;

 if ComboBoxRules.Items.Count = 0 then begin
  ActionRulesEdit.Enabled := false;
  ActionRulesDelete.Enabled := false;
  TreeRule.Items.Clear;
  LabelRules.Caption := '������ ���';
 end else begin
  ActionRulesEdit.Enabled := true;
  ActionRulesDelete.Enabled := true;

  RefreshRule(ComboBoxRules.ItemIndex);
 end;
end;

procedure TKBEditorForm.RefreshRule(n: integer);
var
 CurRule: record_rule;
// t: TListSection;
begin
 CurRule := record_rule(KB.ListRules.Items[n]);
 LabelRules.Caption := '������� (' + IntToStr(n + 1) + '/' + IntToStr(KB.ListRules.Count) + '):';

 MakeTree(TreeRule.Items,CurRule);
 TreeRule.Items.GetFirstNode.Expand(True);
 TreeRule.Items.GetFirstNode.GetNextSibling.Expand(True);
 if TreeRule.Items.GetFirstNode.GetNextSibling.GetNextSibling <> nil then
  TreeRule.Items.GetFirstNode.GetNextSibling.GetNextSibling.Expand(True);
end;

procedure TKBEditorForm.ComboBoxRulesClick(Sender: TObject);
begin
 RefreshRule(ComboBoxRules.ItemIndex);
end;

function TKBEditorForm.AssertionToStr(ass: TAssertion): string;
var
 p,s: string;
 obj: record_obj;
 attr: record_attr;
begin
 with ass do begin
  case predicat of
   0: p := ' = ';
   1: p := ' > ';
   2: p := ' < ';
   3: p := ' >= ';
   4: p := ' <= ';
   5: p := ' <> ';
  else
   p := ' = ';
  end;
  obj := record_obj(KB.ListObjs.Items[n_obj - 1]);
  attr := record_attr(obj.FListAttrs.Items[n_attr - 1]);

  s := obj.comment + '.' + attr.comment + p + '"' + Fznach + '" ' + w_factor + ' [' + IntToStr(factor1) + ';' + IntToStr(factor2) + '] ' + w_accuracy + ' ' + IntToStr(accuracy);
 end;
 Result := s;
end;

function TKBEditorForm.ConsequentToStr(rule: record_rule; i: integer): string;
var
 s: string;
begin
 s := AssertionToStr(list(rule.list_attr_then.Items[i]));
 Result := s;
end;

function TKBEditorForm.ConsequentElseToStr(rule: record_rule; i: integer): string;
var
 s: string;
begin
 s := AssertionToStr(list(rule.list_attr_else.Items[i]));
 Result := s;
end;

procedure TKBEditorForm.RadioGroupTypesClick(Sender: TObject);
begin
 if RadioGroupTypes.ItemIndex = 0 then begin
  ListBoxObjects.Sections.Items[0].Lines := slNone;
  ListBoxAttribs.Sections.Items[0].Lines := slHorizontal;
  ListBoxTypes.Sections.Items[0].Lines := slHorizontal;
  PopupMenuObjects.AutoPopup := true;
  PopupMenuAttribs.AutoPopup := true;
  PopupMenuTypes.AutoPopup := true;
  PopupMenuValues.AutoPopup := true;
  PopupMenuRules.AutoPopup := false;
  ToolButtonObjects.Enabled := true;
  ToolButtonAttribs.Enabled := true;
  ToolButtonTypes.Enabled := true;
  ToolButtonValues.Enabled := true;
  ToolButtonRules.Enabled := false;
  ActionRulesAdd.Enabled := false;
  ActionRulesEdit.Enabled := false;
  ActionRulesDelete.Enabled := false;
 end else begin
  ListBoxObjects.Sections.Items[0].Lines := slNone;
  ListBoxAttribs.Sections.Items[0].Lines := slHorizontal;
  ListBoxTypes.Sections.Items[0].Lines := slHorizontal;
  PopupMenuObjects.AutoPopup := false;
  PopupMenuAttribs.AutoPopup := false;
  PopupMenuTypes.AutoPopup := false;
  PopupMenuValues.AutoPopup := false;
  PopupMenuRules.AutoPopup := true;
  ToolButtonObjects.Enabled := false;
  ToolButtonAttribs.Enabled := false;
  ToolButtonTypes.Enabled := false;
  ToolButtonValues.Enabled := false;
  ToolButtonRules.Enabled := true;
  ActionRulesAdd.Enabled := true;
  ActionRulesEdit.Enabled := true;
  ActionRulesDelete.Enabled := true;
 end;

 CurList.Mode := RadioGroupTypes.ItemIndex;
 RefreshKBViewer;
end;

procedure TKBEditorForm.ListBoxTypesClick(Sender: TObject);
begin
 if (CurList.Mode = 0) and (CurFocus <> 3) then begin
  ListBoxObjects.Sections.Items[0].Lines := slHorizontal;
  ListBoxAttribs.Sections.Items[0].Lines := slHorizontal;
  ListBoxTypes.Sections.Items[0].Lines := slNone;
  CurList.RefreshTypes(ListBoxTypes.ItemIndex - 1);
 end else CurList.TypeN := ListBoxTypes.ItemIndex - 1;
 CurFocus := 3;
 RefreshKBViewer;
end;

procedure TKBEditorForm.ActionExitExecute(Sender: TObject);
begin
 Close;
end;

procedure TKBEditorForm.ActionObjectAddExecute(Sender: TObject);
var
 s: string;
 obj: record_obj;
begin
 if Sender is TKBItemInfo then s := TKBItemInfo(Sender).name
 else s := '������' + IntToStr(KB.ListObjs.Count + 1);
 if InputString('�������� ������','������� �������� �������',s) then begin
  obj := record_obj.Create(KB.ListObjs);
  obj.comment := s;
  obj.n_group := 1;
  obj.n_obj := KB.ListObjs.Count;
  MakeNumbers;
  RefreshObjects;
 end;
 RefreshStatus;
end;

procedure TKBEditorForm.ActionObjectEditExecute(Sender: TObject);
var
 s: string;
 obj: record_obj;
 nobj: integer;
begin
 nobj := CurList.ObjN;
 if nobj >= 0 then begin
  obj := CurList.GetCurObj;
  s := obj.comment;
  if InputString('�������� ������','������� ����� �������� �������',s) then begin
   obj.comment := s;
   RefreshObjects;
  end;
 end;
end;

procedure TKBEditorForm.ActionObjectDeleteExecute(Sender: TObject);
var
 s: string;
 obj: record_obj;
 nobj: integer;
begin
 nobj := CurList.ObjN;
 if nobj >= 0 then begin
  obj := CurList.GetCurObj;
  s := obj.comment;
  if MessageYesNo('������� ������ ''' + s + '''?','������� ������') = IDYES then begin
   obj.Destroy;
   MakeNumbers;
   RefreshObjects;
  end;
 end;
 RefreshStatus;
end;

function TKBEditorForm.InputAttribInfo(const ACaption: string; var comment: string; var ntype: integer): boolean;
var
 CurType: record_type;
 i: integer;
 ntypeval: integer;
 s,smin,smax: string;
begin
 AttribDlg.Caption := ACaption;
 AttribDlg.EditName.Text := comment;
 AttribDlg.ComboBoxTypes.Items.Clear;
 for i := 0 to KB.ListTypes.Count - 1 do begin
  CurType := record_type(KB.ListTypes.Items[i]);
  AttribDlg.ComboBoxTypes.Items.Add(CurType.comment);
 end;
 AttribDlg.ComboBoxTypes.ItemIndex := ntype;
 if AttribDlg.ShowModal = mrOk then begin
  comment := AttribDlg.EditName.Text;
  ntype := AttribDlg.ComboBoxTypes.ItemIndex;
  if ntype = -1 then begin
   if MessageYesNo('��� ''' + AttribDlg.ComboBoxTypes.Text + ''' �� ����������. �������?','�������� ��������') = IDYES then begin
    s := AttribDlg.ComboBoxTypes.Text;
    ntypeval := 1;
    smin := '0';
    smax := '0';
    if InputTypeInfo('�������� ���',s,ntypeval,smin,smax) then begin
     CurType := record_type.Create(KB.ListTypes);
     CurType.comment := s;
     CurType.type_znach := ntypeval;
     CurType.min_znach := smin;
     CurType.max_znach := smax;
     CurType.n_type := KB.ListTypes.Count;
     //RefreshTypes;
     ntype := CurType.n_type - 1;
     Result := true;
    end else Result := false;
   end else Result := false
  end else Result := true;
 end else Result := false;
end;

procedure TKBEditorForm.ActionAttribAddExecute(Sender: TObject);
var
 nobj,ntype: integer;
 obj: record_obj;
 attr: record_attr;
 s: string;
begin
 if Sender is TKBItemInfo then nobj := TKBItemInfo(Sender).nobj
 else nobj := CurList.ObjN;
 if nobj >= 0 then begin
  if KB.ListTypes.Count = 0 then begin
   ShowMessage('�������� ������� ���������� - �� ���������� �� ������ ����');
   exit;
  end;
  if Sender is TKBItemInfo then begin
   obj := record_obj(KB.ListObjs.Items[TKBItemInfo(Sender).nobj]);
   s := TKBItemInfo(Sender).name;
  end else begin
   obj := CurList.GetCurObj;
   s := '�������' + IntToStr(obj.FListAttrs.Count + 1);
  end;
  ntype := 0;
  if InputAttribInfo('�������� �������',s,ntype) then begin
   attr := record_attr.Create(obj.FListAttrs);
   attr.comment := s;
   attr.n_obj := obj.n_obj;
   attr.n_type := ntype + 1;
   attr.n_attr := obj.FListAttrs.Count;
   RefreshAttribs;
   //RefreshTypes;
  end;
 end else begin
  ShowMessage('���������� ������� � ������ ������� ����� ���������� ����� �������');
 end;
 RefreshStatus;
end;

procedure TKBEditorForm.ActionAttribDeleteExecute(Sender: TObject);
var
 s: string;
 attr: record_attr;
 nattr: integer;
begin
 nattr := CurList.AttribN;
 if (nattr >= 0) then begin
  attr := CurList.GetCurAttrib;
  s := attr.comment;
  if MessageYesNo('������� ������� ''' + s + '''?','������� �������') = IDYES then begin
   attr.Destroy;
   MakeNumbers;
   RefreshAttribs;
   //RefreshTypes;
  end;
 end;
 RefreshStatus; 
end;

procedure TKBEditorForm.ActionAttribEditExecue(Sender: TObject);
var
 nattr,ntype: integer;
 attr: record_attr;
 s: string;
begin
 nattr := CurList.AttribN;
 if (nattr >= 0) then begin
  attr := CurList.GetCurAttrib;
  s := attr.comment;
  ntype := attr.n_type - 1;
  if InputAttribInfo('�������� �������',s,ntype) then begin
   attr.comment := s;
   attr.n_type := ntype + 1;
   RefreshAttribs;
   //RefreshTypes;
  end;
 end;
end;

function TKBEditorForm.InputTypeInfo(const ACaption: string; var comment: string; var typeval: integer; var smin: string; var smax: string): boolean;
begin
 TypeDlg.Caption := ACaption;
 TypeDlg.EditName.Text := comment;
 TypeDlg.ComboBoxTypeVal.ItemIndex := typeval - 1;
 TypeDlg.EditMin.Text := smin;
 TypeDlg.EditMax.Text := smax;

 if TypeDlg.ComboBoxTypeVal.ItemIndex = 0 then TypeDlg.PanelMinMax.Visible := true
 else TypeDlg.PanelMinMax.Visible := false;

 if TypeDlg.ShowModal = mrOk then begin
  comment := TypeDlg.EditName.Text;
  typeval := TypeDlg.ComboBoxTypeVal.ItemIndex + 1;
  smin := TypeDlg.EditMin.Text;
  smax := TypeDlg.EditMax.Text;
  Result := true;
 end else Result := false;
end;

procedure TKBEditorForm.ActionTypeAddExecute(Sender: TObject);
var
 ntypeval: integer;
 CurType: record_type;
 s,smin,smax: string;
begin
 s := '���' + IntToStr(KB.ListTypes.Count + 1);
 ntypeval := 1;
 smin := '0';
 smax := '0';
 if InputTypeInfo('�������� ���',s,ntypeval,smin,smax) then begin
  CurType := record_type.Create(KB.ListTypes);
  CurType.comment := s;
  CurType.type_znach := ntypeval;
  CurType.min_znach := smin;
  CurType.max_znach := smax;
  CurType.n_type := KB.ListTypes.Count;
  RefreshTypes;
 end;
 RefreshStatus; 
end;

procedure TKBEditorForm.FixTypeNumbers(f: integer);
var
 i,j: integer;
 curobj: record_obj;
 curattr: record_attr;
begin
 for i := 0 to KB.ListObjs.Count - 1 do begin
  curobj := record_obj(KB.ListObjs.Items[i]);
  for j := 0 to curobj.k_attr - 1 do begin
   curattr := record_attr(curobj.FListAttrs.Items[j]);
   if curattr.n_type > f + 1 then curattr.n_type := curattr.n_type - 1;
  end;
 end;
end;

procedure TKBEditorForm.ActionTypeDeleteExecute(Sender: TObject);
var
 s: string;
 CurType: record_type;
 CurObj: record_obj;
 CurAttr: record_attr;
 ntype,i,j: integer;
 ListAttrs,ListTypes: TList;
begin
 ntype := CurList.TypeN;
 if ntype >= 0 then begin
  CurType := CurList.GetCurType;
  s := CurType.comment;
  if MessageYesNo('������� ��� ''' + s + '''?','������� ���') = IDYES then begin
   ListAttrs := TList.Create;
   ListTypes := TList.Create;
   for j := 0 to KB.ListObjs.Count - 1 do begin
    CurObj := record_obj(KB.ListObjs.Items[j]);
    for i := 0 to CurObj.FListAttrs.Count - 1 do begin
     CurAttr := record_attr(CurObj.FListAttrs.Items[i]);
     if CurAttr.n_type = CurType.n_type then ListAttrs.Add(CurAttr);
    end;
   end;
   //ShowMessage('1');
   if ListAttrs.Count > 0 then begin
   //ShowMessage('2');
    TypeDelDlg.ListAttrs := ListAttrs;
    TypeDelDlg.ListBoxAttribs.Items.Clear;
    for i := 0 to ListAttrs.Count - 1 do
     TypeDelDlg.ListBoxAttribs.Items.Add(record_attr(ListAttrs.Items[i]).comment);
    TypeDelDlg.ComboBoxTypes.Items.Clear;
    for i := 0 to KB.ListTypes.Count - 1 do
     if record_type(KB.ListTypes.Items[i]) <> CurType then begin
      ListTypes.Add(record_type(KB.ListTypes.Items[i]));
      TypeDelDlg.ComboBoxTypes.Items.Add(record_type(KB.ListTypes.Items[i]).comment);
     end;
    TypeDelDlg.ComboBoxTypes.ItemIndex := 0;
    TypeDelDlg.ListBoxAttribs.ItemIndex := 0;
    TypeDelDlg.RadioGroupAsk.Enabled := true;
    TypeDelDlg.RadioGroupAsk.ItemIndex := 0;
    TypeDelDlg.ComboBoxTypes.Visible := false;
    TypeDelDlg.Types := ListTypes;

    if ListTypes.Count = 0 then TypeDelDlg.RadioGroupAsk.Items.Text := '�������'
    else TypeDelDlg.RadioGroupAsk.Items.Text := '�������' + #13 + '�������� ���';

    if TypeDelDlg.ShowModal = mrOk then begin
     CurType.Destroy;
    end;
   end else CurType.Destroy;
   MakeNumbers;
   FixTypeNumbers(ntype);
   //RefreshAttribs;
   RefreshTypes;
   ListAttrs.Free;
   ListTypes.Free;
  end;
 end;
end;

procedure TKBEditorForm.ActionTypeEditExecute(Sender: TObject);
var
 ntype,ntypeval: integer;
 CurType: record_type;
 s,smin,smax: string;
begin
 ntype := CurList.TypeN;
 if ntype >= 0 then begin
  CurType := CurList.GetCurType;
  s := CurType.comment;
  ntypeval := CurType.type_znach;
  smin := CurType.min_znach;
  smax := CurType.max_znach;
  if InputTypeInfo('�������� ���',s,ntypeval,smin,smax) then begin
   CurType.comment := s;
   CurType.type_znach := ntypeval;
   CurType.min_znach := smin;
   CurType.max_znach := smax;
   RefreshTypes;
  end;
 end;
end;

procedure TKBEditorForm.ActionValueAddExecute(Sender: TObject);
var
 ntype: integer;
 CurType: record_type;
 CurValue: value;
 s: string;
begin
 if Sender is TKBItemInfo then ntype := TKBItemInfo(Sender).ntype
 else ntype := CurList.TypeN;
 if ntype >= 0 then begin
  if Sender is TKBItemInfo then begin
   CurType := record_type(KB.ListTypes.Items[ntype]);
   s := TKBItemInfo(Sender).name;
  end else begin
   CurType := CurList.GetCurType;
   s := '��������' + IntToStr(CurType.list_znach.Count + 1);
  end;
  if (CurType.type_znach <> 1) and InputString('�������� ��������','������� ��������',s) then begin
   CurValue := value.Create(CurType.list_znach);
   CurValue.znach := s;
   MakeNumbers;
   RefreshValues;
  end;
 end;
 RefreshStatus;
end;

procedure TKBEditorForm.ActionValueDeleteExecute(Sender: TObject);
var
 nvalue: integer;
 CurType: record_type;
 CurValue: value;
begin
 nvalue := CurList.ValueN;
 if nvalue >= 0 then begin
  CurType := CurList.GetCurType;
  if (CurType.type_znach <> 1) then begin
   CurValue := value(CurType.list_znach.Items[nvalue]);
   if MessageYesNo('������� �������� ''' + CurValue.znach + '''?','������� ��������') = IDYES then begin
    CurValue.Destroy;
    MakeNumbers;
    RefreshValues;
   end;
  end;
 end;
 RefreshStatus; 
end;

procedure TKBEditorForm.ActionValueEditExecute(Sender: TObject);
var
 nvalue: integer;
 CurType: record_type;
 CurValue: value;
 s: string;
 left,top: integer;
begin
 nvalue := CurList.ValueN;
 if nvalue >= 0 then begin
  CurType := CurList.GetCurType;
  if (CurType.type_znach <> 1) then begin
   CurValue := value(CurType.list_znach.Items[nvalue]);
   s := CurValue.znach;
   if InputString('�������� ��������','������� ��������',s) then begin
    CurValue.znach := s;
    RefreshValues;
   end;
  end else begin
   TypeDlg.PanelMinMax.Visible := true;
   TypeDlg.EditMin.Text := CurType.min_znach;
   TypeDlg.EditMax.Text := CurType.max_znach;

   TypeDlg.LabelName.Visible := false;
   TypeDlg.EditName.Visible := false;
   TypeDlg.LabelTypeVal.Visible := false;
   TypeDlg.ComboBoxTypeVal.Visible := false;
   left := TypeDlg.PanelMinMax.Left;
   top := TypeDlg.PanelMinMax.Top;
   TypeDlg.PanelMinMax.Left := 108;
   TypeDlg.PanelMinMax.Top := 44;

   if TypeDlg.ShowModal = mrOk then begin
    CurType.min_znach := TypeDlg.EditMin.Text;
    CurType.max_znach := TypeDlg.EditMax.Text;
    RefreshValues;
   end;
   TypeDlg.PanelMinMax.Left := left;
   TypeDlg.PanelMinMax.Top := top;
   TypeDlg.LabelName.Visible := true;
   TypeDlg.EditName.Visible := true;
   TypeDlg.LabelTypeVal.Visible := true;
   TypeDlg.ComboBoxTypeVal.Visible := true;
   TypeDlg.PanelMinMax.Visible := false;
  end;
 end;
end;

procedure TKBEditorForm.FormCreate(Sender: TObject);
begin
 CurList := TCurList.Create;
 SearchList := TList.Create;
 SearchMode := -1;
 SearchIndex := 0;
 SearchString := '';

 AttribDlg := TAttribDlg.Create(Self);
 TypeDlg := TTypeDlg.Create(Self);
 YesNoDlg := TYesNoDlg.Create(Self);
 TypeDelDlg := TTypeDelDlg.Create(Self);
 InputDlg := TInputDlg.Create(Self);
 SearchDlg := TSearchDlg.Create(Self);
 EditRuleExp := TEditRuleExp.Create(Self);
 EditOpDlg := TEditOpDlg.Create(Self);
end;

procedure TKBEditorForm.MenuBaseSaveClick(Sender: TObject);
begin
 if ButtonRulesOk.Visible then ButtonRulesOkClick(Self);
 if not ButtonRulesOk.Visible then begin
   if KB.FileName='' then begin
     if SaveKBDialog.Execute then begin
       KB.FileName:=SaveKBDialog.FileName;
     end;
   end;
   KB.Save;
 end;
end;

function TKBEditorForm.MessageYesNo(Text,Caption: string): integer;
begin
 YesNoDlg.Caption := Caption;
 YesNoDlg.LabelText.Caption := Text;

 if YesNoDlg.ShowModal = mrYes then Result := IDYES
 else Result := IDNO;
end;

function TKBEditorForm.InputString(const Caption, Prompt: string; var Value: string): boolean;
begin
 InputDlg.Caption := Caption;
 InputDlg.LabelPrompt.Caption := Prompt;
 InputDlg.EditInput.Text := Value;

 if InputDlg.ShowModal = mrOk then begin
  Value := InputDlg.EditInput.Text;
  Result := true;
 end else Result := false;
end;


procedure TKBEditorForm.FormDestroy(Sender: TObject);
begin
 CurList.Free;
 SearchList.Free;
 
 AttribDlg.Free;
 TypeDlg.Free;
 YesNoDlg.Free;
 TypeDelDlg.Free;
 InputDlg.Free;
 SearchDlg.Free;
 EditRuleExp.Free;
 EditOpDlg.Free;
end;

procedure TKBEditorForm.ListBoxValuesClick(Sender: TObject);
begin
 CurList.ValueN := ListBoxValues.ItemIndex - 1;
 CurFocus := 4;
end;

procedure TKBEditorForm.MakeNumbers;
var
 curobj: record_obj;
 curattr: record_attr;
 curtype: record_type;
 i, j: integer;
begin
 for i := 0 to KB.ListObjs.Count - 1 do begin
  curobj := record_obj(KB.ListObjs.Items[i]);
  curobj.n_obj := i + 1;
  curobj.n_group := 1;
  curobj.k_attr := curobj.FListAttrs.Count;
  for j := 0 to curobj.k_attr - 1 do begin
   curattr := record_attr(curobj.FListAttrs.Items[j]);
   curattr.n_attr := j + 1;
   curattr.n_obj := i + 1;
  end;
 end;
 for i := 0 to KB.ListTypes.Count - 1 do begin
  curtype := record_type(KB.ListTypes.Items[i]);
  curtype.n_type := i + 1;
  curtype.k_znach := curtype.list_znach.Count;
 end;
end;

procedure TKBEditorForm.Status(const f: string; const a: array of const);
var
 s: string;
begin
 s := Format(f,a);
 StatusBarMain.SimpleText := s;
end;

function TKBEditorForm.Search(what: integer): boolean;
var
 i,j: integer;
 s: string;
 curobj: record_obj;
 curattr: record_attr;
 curtype: record_type;
 tmplist: TList;
begin

 case what of
  1: SearchDlg.PageControl.ActivePage := SearchDlg.TabObject;
  2: SearchDlg.PageControl.ActivePage := SearchDlg.TabAttrib;
  3: SearchDlg.PageControl.ActivePage := SearchDlg.TabType;
 end;

 SearchDlg.ComboBoxObjects.Items.Clear;
 SearchDlg.ComboBoxObjects.Text := '';
 for i := 0 to KB.ListObjs.Count - 1 do
  SearchDlg.ComboBoxObjects.Items.Add(record_obj(KB.ListObjs.Items[i]).comment);

 SearchDlg.ComboBoxAttribObject.Items.Clear;
 SearchDlg.ComboBoxAttribType.Items.Clear;
 SearchDlg.ComboBoxAttrib.Items.Clear;
 SearchDlg.ComboBoxAttrib.Text := '';
 SearchDlg.ComboBoxAttribObject.Text := '';
 SearchDlg.ComboBoxAttribType.Text := '';
 for i := 0 to KB.ListObjs.Count - 1 do begin
  curobj := record_obj(KB.ListObjs.Items[i]);
  SearchDlg.ComboBoxAttribObject.Items.Add(curobj.comment);
  for j := 0 to curobj.FListAttrs.Count - 1 do begin
   SearchDlg.ComboBoxAttrib.Items.Add(record_attr(curobj.FListAttrs.Items[j]).comment);
  end;
 end;
 SearchDlg.ComboBoxType.Items.Clear;
 SearchDlg.ComboBoxType.Text := '';
 for i := 0 to KB.ListTypes.Count - 1 do begin
  SearchDlg.ComboBoxAttribType.Items.Add(record_type(KB.ListTypes.Items[i]).comment);
  SearchDlg.ComboBoxType.Items.Add(record_type(KB.ListTypes.Items[i]).comment);
 end;

 if SearchDlg.ShowModal = mrOk then begin
  SearchList.Clear;
  SearchMode := what;
  SearchIndex := 0;

  Result := false;
  Cursor := crHourGlass;

  if SearchDlg.PageControl.ActivePage = SearchDlg.TabObject then begin
   s := AnsiLowerCase(SearchDlg.ComboBoxObjects.Text);
   SearchString := s;
   for i := 0 to KB.ListObjs.Count - 1 do
    if System.Pos(s,AnsiLowerCase(record_obj(KB.ListObjs.Items[i]).comment)) <> 0 then begin
     //ListBoxObjectsClick(Self);
     //CurList.ObjN := i;
     //RefreshKBViewer;
     Result := true;
     //break;
     SearchList.Add(pointer(i));
    end;
  end else

  if SearchDlg.PageControl.ActivePage = SearchDlg.TabAttrib then begin
   tmplist := TList.Create;

   s := AnsiLowerCase(SearchDlg.ComboBoxAttrib.Text);
   SearchString := s;
   for i := 0 to KB.ListObjs.Count - 1 do begin
    curobj := record_obj(KB.ListObjs.Items[i]);
    for j := 0 to curobj.FListAttrs.Count - 1 do begin
     curattr := record_attr(curobj.FListAttrs.Items[j]);
     if System.Pos(s,AnsiLowerCase(curattr.comment)) <> 0 then
      tmplist.Add(curattr);
    end;
   end;

   if tmplist.Count > 0 then
    if not SearchDlg.CheckBoxAttribType.Checked then begin
     s := AnsiLowerCase(SearchDlg.ComboBoxAttribType.Text);
     i := 0;
     while i < tmplist.Count do begin
      if System.Pos(s,AnsiLowerCase(record_type(KB.ListTypes.Items[(record_attr(tmplist.Items[i]).n_type - 1)]).comment)) = 0 then
       tmplist.Delete(i)
      else inc(i);
     end;
    end;

   if tmplist.Count > 0 then
    if not SearchDlg.CheckBoxAttribObject.Checked then begin
     s := AnsiLowerCase(SearchDlg.ComboBoxAttribObject.Text);
     i := 0;
     while i < tmplist.Count do begin
      if System.Pos(s,AnsiLowerCase(record_obj(KB.ListObjs.Items[(record_attr(tmplist.Items[i]).n_obj - 1)]).comment)) = 0 then
       tmplist.Delete(i)
      else inc(i);
     end;
    end;

   if tmplist.Count > 0 then begin
    ListBoxAttribsClick(Self);
    for j := 0 to tmplist.Count - 1 do
     for i := 0 to CurList.ListAttribs.Count - 1 do
      if CurList.nAttrib[i] = tmplist.Items[j] then begin
       SearchList.Add(pointer(i));
       //break;
      end;
    //RefreshKBViewer;
    Result := true;
   end;

   tmplist.Free;
  end else

  if SearchDlg.PageControl.ActivePage = SearchDlg.TabType then begin
   s := AnsiLowerCase(SearchDlg.ComboBoxType.Text);
   SearchString := s;
   j := SearchDlg.RadioGroupType.ItemIndex + 1;
   for i := 0 to KB.ListTypes.Count - 1 do begin
    curtype := record_type(KB.ListTypes.Items[i]);
    if (((j = 3) or (j = curtype.type_znach)) and (System.Pos(s,AnsiLowerCase(curtype.comment)) <> 0)) then begin
     //RefreshKBViewer;
     SearchList.Add(pointer(i));
     Result := true;
     //break;
    end;
   end;
  end;

  Cursor := crDefault;
  if (SearchList.Count > 0) then SearchGoTo;
 end else Result := false;
end;

function TKBEditorForm.GetSearchName: string;
begin
 if SearchDlg.PageControl.ActivePage = SearchDlg.TabObject then Result := '������'
 else if SearchDlg.PageControl.ActivePage = SearchDlg.TabAttrib then Result := '�������'
 else if SearchDlg.PageControl.ActivePage = SearchDlg.TabType then Result := '���';
end;

procedure TKBEditorForm.ActionObjectSearchExecute(Sender: TObject);
begin
 if Search(1) then Status('%s ������',[GetSearchName])
 else Status('%s �� ������',[GetSearchName]);
end;

procedure TKBEditorForm.ActionAttribSearchExecute(Sender: TObject);
begin
 if Search(2) then Status('������� ������',[])
 else Status('������� �� ������',[]);
end;

procedure TKBEditorForm.ActionTypeSearchExecute(Sender: TObject);
begin
 if Search(3) then Status('��� ������',[])
 else Status('��� �� ������',[]);
end;

function TKBEditorForm.GetArgsNum(s: string; var op: char): integer;
var
 i,n,b: integer;
begin
 n := 0;
 b := 0;
 for i := 1 to Length(s) do begin
  if s[i] = '(' then inc(b)
  else if s[i] = ')' then dec(b)
  else if (((s[i] = '&') or (s[i] = '|') or (s[i] = '~')) and (b = 0)) then begin
   if (s[i] <> '~') then inc(n);
   op := s[i];
  end;
 end;
 Result := n + 1;
end;

function TKBEditorForm.GetArg(s: string; num: integer): string;
var
 i,n,b,p1,p2: integer;
 arg: string;
begin
 n := 0;
 b := 0;
 arg := '';
 p1 := 1;
 p2 := 0;
 for i := 1 to Length(s) do begin
  if s[i] = '(' then inc(b)
  else if s[i] = ')' then dec(b)
  else if (((s[i] = '&') or (s[i] = '|')) and (b = 0)) then begin
   inc(n);
   if n = num - 1 then p1 := i + 1
   else if n = num then p2 := i - 1;
  end else if ((s[i] = '~') and (b = 0)) then begin
   Result := GetArg(Copy(s, i + 1, Length(s) - i), 1);
   exit;
  end;
 end;

 if p2 < p1 then p2 := Length(s);
 arg := Copy(s,p1,p2 - p1 + 1);
 RemoveBrackets(arg);

 Result := arg;
end;

procedure TKBEditorForm.RemoveBrackets(var s: string);
var
 i: integer;
 n: integer;
begin
 s := Trim(s);
 if Length(s) < 1 then exit;
 if (s[1] = '(') and (s[Length(s)] = ')') then begin
  n := 0;
  for i := 1 to Length(s) do begin
   if s[i] = '(' then inc(n) else if s[i] = ')' then dec(n);
   if (n = 0) and (i <> Length(s)) then exit;
  end;
  if n = 0 then begin
   Delete(s,1,1);
   Delete(s,Length(s),1);
   Trim(s);
  end;
 end;
end;

function TKBEditorForm.GetNumFromStr(s: string): integer;
var
 i: integer;
 t: string;
begin
 t := '';
 for i := 1 to Length(s) do
  if (((s[i] >= '0') and (s[i] <= '9')) or (s[i] = '-')) then t := t + s[i];
 i := StrToInt(t);
 Result := i;
end;

function TKBEditorForm.ArgToStr(rule: record_rule; s: string): string;
var
 i: integer;
 t: string;
begin
 i := GetNumFromStr(s);
 t := AssertionToStr(list(rule.list_attr_if.Items[i - 1]));
 Result := t;
end;

procedure TKBEditorForm.IncidentToTree(rule: record_rule; s: string; parent: TTreeNode);
var
 tmpnode: TTreeNode;
 op: char;
 argsn,i: integer;
 ts: string;
begin
 argsn := GetArgsNum(s,op);
 if (argsn = 1) and (op <> '~') then begin
  ts := GetArg(s,1);
  tmpnode := parent.Owner.AddChild(parent,ArgToStr(rule,ts));
  tmpnode.ImageIndex := 5;
  tmpnode.SelectedIndex := tmpnode.ImageIndex;
  tmpnode.Data := pointer(GetNumFromStr(ts));
 end else {if argsn > 1 then} begin
  tmpnode := parent.Owner.AddChild(parent,'');
  if op = '|' then begin
   tmpnode.ImageIndex := 3;
   tmpnode.Text := '���';
   tmpnode.Data := pointer(-1)
  end else
  if op = '&' then begin
   tmpnode.ImageIndex := 4;
   tmpnode.Text := '�';
   tmpnode.Data := pointer(-2)
  end else
  if op = '~' then begin
   tmpnode.ImageIndex := 6;
   tmpnode.Text := '��';
   tmpnode.Data := pointer(-3)
  end;
  tmpnode.SelectedIndex := tmpnode.ImageIndex;
  for i := 1 to argsn do IncidentToTree(rule,GetArg(s,i),tmpnode);
 end;
end;

procedure TKBEditorForm.MakeTree(tree: TTreeNodes; rule: record_rule);
var
 tmpnode,tmpnode1: TTreeNode;
 s: string;
 i: integer;
begin
 tree.BeginUpdate;
 tree.Clear;

 tmpnode := tree.Add(tree.GetFirstNode,'����');
 tmpnode.ImageIndex := 0;
 tmpnode.SelectedIndex := tmpnode.ImageIndex;
 s := StrPas(rule.incident);
 RemoveBrackets(s);
 if s <> '' then IncidentToTree(rule,s,tmpnode);

 tmpnode := tree.Add(tree.GetFirstNode,'��');
 tmpnode.ImageIndex := 1;
 tmpnode.SelectedIndex := tmpnode.ImageIndex;
 for i := 0 to rule.list_attr_then.Count - 1 do begin
  tmpnode1 := tree.AddChild(tmpnode,ConsequentToStr(rule,i));
  tmpnode1.ImageIndex := 5;
  tmpnode1.SelectedIndex := tmpnode1.ImageIndex;
  tmpnode1.Data := pointer(101 + i);
 end;

 if rule.list_attr_else.Count > 0 then begin
  tmpnode := tree.Add(tree.GetFirstNode,'�����');
  tmpnode.ImageIndex := 2;
  tmpnode.SelectedIndex := tmpnode.ImageIndex;
  for i := 0 to rule.list_attr_else.Count - 1 do begin
   tmpnode1 := tree.AddChild(tmpnode,ConsequentElseToStr(rule,i));
   tmpnode1.ImageIndex := 5;
   tmpnode1.SelectedIndex := tmpnode1.ImageIndex;
   tmpnode1.Data := pointer(201 + i);
  end;
 end;

 tree.EndUpdate;
end;

procedure TKBEditorForm.ActionRulesDeleteExecute(Sender: TObject);
var
 currule: record_rule;
begin
 if (ComboBoxRules.ItemIndex < 0) or (KB.ListRules.Count = 0) then exit;
 currule := record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]);
 if MessageYesNo('������� ������� ''' + currule.comment + '''?','������� �������') = IDYES then begin
  currule.Destroy;
  MakeRulesNumbers;
  RulesEditEnd(Self);
  RefreshRules;
 end;
 RefreshStatus;
end;

procedure TKBEditorForm.ActionRulesAddExecute(Sender: TObject);
var
 newrule: record_rule;
begin
 newrule := record_rule.Create(KB.ListRules);
 RefreshRules;
 ComboBoxRules.ItemIndex := ComboBoxRules.Items.Count - 1;
 RefreshRules;
 newrule.consequent_else := 'new';
 ActionRulesEditExecute(Self);
 RefreshStatus;
end;

procedure TKBEditorForm.ActionRulesEditExecute(Sender: TObject);
var
 tmpnode: TTreeNode;
begin
 if (ComboBoxRules.ItemIndex < 0) or (KB.ListRules.Count = 0) then exit;

 ComboBoxRules.Enabled := false;
 RadioGroupTypes.Enabled := false;
 ActionRulesAdd.Enabled := false;
 ActionRulesEdit.Enabled := false;
 PopupMenuRuleNode.AutoPopup := true;

 EditRuleName.Visible := true;
 LabelRuleName.Visible := true;
 EditRuleName.Text := ComboBoxRules.Text;

 ButtonRulesEdit.Visible := false;
 ButtonRulesOk.Visible := true;
 ButtonRulesCancel.Visible := true;

 TmpRule := record_rule.Create(KB.ListRules);
 //TmpRule.Assign(record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]));
 RuleAssign(TmpRule,record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]));

 if TmpRule.list_attr_else.Count = 0 then begin
  tmpnode := TreeRule.Items.Add(TreeRule.Items.GetFirstNode,'�����');
  tmpnode.ImageIndex := 2;
  tmpnode.SelectedIndex := tmpnode.ImageIndex;
 end;
end;

procedure TKBEditorForm.RulesEditEnd(Sender: TObject);
begin
 ComboBoxRules.Enabled := true;
 RadioGroupTypes.Enabled := true;
 ActionRulesAdd.Enabled := true;
 ActionRulesEdit.Enabled := true;
 PopupMenuRuleNode.AutoPopup := false;

 EditRuleName.Visible := false;
 LabelRuleName.Visible := false;

 ButtonRulesEdit.Visible := true;
 ButtonRulesOk.Visible := false;
 ButtonRulesCancel.Visible := false;

 if TmpRule <> nil then begin
  TmpRule.Destroy;
  TmpRule := nil;
 end;
end;

procedure TKBEditorForm.MakeRulesNumbers;
var
 i: integer;
 currule: record_rule;
begin
 for i := 0 to KB.ListRules.Count - 1 do begin
  currule := record_rule(KB.ListRules.Items[i]);
  currule.n_rule := i + 1;
 end;
end;

procedure TKBEditorForm.TreeRuleMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 if Button = mbRight then TreeRule.Selected := TreeRule.GetNodeAt(X,Y);
end;

procedure TKBEditorForm.ButtonRulesOkClick(Sender: TObject);
var
 currule: record_rule;
 r: integer;
 s: string;
begin
 currule := record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]);

 r := CheckRuleTree;
 if r = 0 then begin
  TmpRule.comment := EditRuleName.Text;
  s := GetRuleString(TreeRule.Items.GetFirstNode.GetFirstChild);
  RemoveBrackets(s);
  TmpRule.Pincident := s;
  TmpRule.Pconsequent := '';
  TmpRule.Pconsequent_else := '';
  //currule.Assign(TmpRule);
  RuleAssign(currule,TmpRule);

  RulesEditEnd(Self);
  MakeRulesNumbers;
  RefreshRules;
 end else
  ShowMessage('������ � ��������� �������:' + #13 + RuleTreeError[r]);
end;

procedure TKBEditorForm.ButtonRulesCancelClick(Sender: TObject);
var
 currule: record_rule;
begin
 currule := record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]);
 if currule.consequent_else = 'new' then currule.Destroy;
 RulesEditEnd(Self);
 RefreshRules;
end;

procedure TKBEditorForm.ComboBoxRulesChange(Sender: TObject);
begin
 if (ComboBoxRules.ItemIndex < 0) or (KB.ListRules.Count = 0) then begin
  ActionRulesDelete.Enabled := false;
  ActionRulesEdit.Enabled := false;
 end else if RadioGroupTypes.ItemIndex = 1 then begin
  ActionRulesDelete.Enabled := true;
  ActionRulesEdit.Enabled := true;
 end;
end;

procedure TKBEditorForm.PopupMenuRuleNodePopup(Sender: TObject);
var
 tmpnode: TTreeNode;
begin
 tmpnode := TreeRule.Selected;
 ActionRuleNodeAddOp.Enabled := false;
 ActionRuleNodeAddExp.Enabled := false;
 ActionRuleNodeEdit.Enabled := false;
 ActionRuleNodeDelete.Enabled := false;

 if tmpnode <> nil then begin
  if integer(tmpnode.Data) <> 0 then begin
   ActionRuleNodeEdit.Enabled := true;
   ActionRuleNodeDelete.Enabled := true;
  end;
  if integer(tmpnode.Data) <= 0 then begin
   ActionRuleNodeAddOp.Enabled := true;
   ActionRuleNodeAddExp.Enabled := true;
   if (tmpnode.ImageIndex = 1) or (tmpnode.ImageIndex = 2) then
    ActionRuleNodeAddOp.Enabled := false
   else if (tmpnode.ImageIndex = 0) and (tmpnode.Count >= 1) then begin
    //ActionRuleNodeAddOp.Enabled := false;
    ActionRuleNodeAddExp.Enabled := false;
   end;
  end;
 end;
end;

procedure TKBEditorForm.ActionRuleNodeAddOpExecute(Sender: TObject);
var
 tmpnode,tmpnode1,tmpnode2: TTreeNode;
begin
 tmpnode := TreeRule.Selected;
 tmpnode2 := nil;
 if (integer(tmpnode.Data) <= 0) and (tmpnode.ImageIndex = 0) and (tmpnode.Count = 1) then begin
  tmpnode2 := tmpnode.Item[0];
 end;

 tmpnode1 := TreeRule.Items.AddChildObject(TreeRule.Selected,'���',pointer(-10));
 tmpnode1.ImageIndex := 3;
 tmpnode1.SelectedIndex := tmpnode1.ImageIndex;
 TreeRule.Selected := tmpnode1;

 if tmpnode2 <> nil then begin
  tmpnode2.MoveTo(tmpnode1,naAddChild);
 end;

 ActionRuleNodeEditExecute(Self);
end;

procedure TKBEditorForm.ActionRuleNodeAddExpExecute(Sender: TObject);
var
 tmpnode: TTreeNode;
 n: integer;
 tmpass: list;
begin
 if TreeRule.Selected.ImageIndex = 1 then begin
  tmpass := list.Create(TmpRule.list_attr_then);
  n := 100 + TmpRule.list_attr_then.Count;
 end else if TreeRule.Selected.ImageIndex = 2 then begin
  tmpass := list.Create(TmpRule.list_attr_else);
  n := 200 + TmpRule.list_attr_else.Count;
 end else begin
  tmpass := list.Create(TmpRule.list_attr_if);
  n := TmpRule.list_attr_if.Count;
 end;

 tmpnode := TreeRule.Items.AddChildObject(TreeRule.Selected,'',pointer(n));
 tmpnode.ImageIndex := 5;
 tmpnode.SelectedIndex := tmpnode.ImageIndex;
 TreeRule.Selected := tmpnode;
 CurAssert := tmpass;

 if (KB.ListObjs.Count > 0) then CurAssert.n_obj := 1;

 ActionRuleNodeEditExecute(Self);
end;

procedure TKBEditorForm.ActionRuleNodeEditExecute(Sender: TObject);
var
 curnode: TTreeNode;
 n: integer;
 a: boolean;
begin
 curnode := TreeRule.Selected;
 n := integer(curnode.Data);
 if n > 0 then begin
  if n < 100 then CurAssert := list(TmpRule.list_attr_if.Items[n - 1])
  else if n < 200 then CurAssert := list(TmpRule.list_attr_then.Items[n - 101])
  else CurAssert := list(TmpRule.list_attr_else.Items[n - 201]);

  EditRuleExp.FormStyle := fsStayOnTop;
  EditRuleExp.Show;
 end else if n < 0 then begin
  if integer(curnode.Data) = -10 then begin
   a := true;
   curnode.Data := pointer(-1);
  end else a := false;
  if integer(curnode.Data) = -1 then EditOpDlg.RadioButtonOr.Checked := true
  else if integer(curnode.Data) = -2 then EditOpDlg.RadioButtonAnd.Checked := true
  else if integer(curnode.Data) = -3 then EditOpDlg.RadioButtonNot.Checked := true;

  if EditOpDlg.ShowModal = mrOk then begin
   if EditOpDlg.RadioButtonOr.Checked then begin
    curnode.Text := '���';
    curnode.Data := pointer(-1);
    curnode.ImageIndex := 3;
   end else if EditOpDlg.RadioButtonAnd.Checked then begin
    curnode.Text := '�';
    curnode.Data := pointer(-2);
    curnode.ImageIndex := 4;
   end else if EditOpDlg.RadioButtonNot.Checked then begin
    curnode.Text := '��';
    curnode.Data := pointer(-3);
    curnode.ImageIndex := 6;
   end;
   curnode.SelectedIndex := curnode.ImageIndex;
  end else if a then curnode.Delete;
 end;
end;

procedure TKBEditorForm.ReindexNode(node: TTreeNode; n: integer);
var
 i: integer;
begin
 if node = nil then exit;
 for i := 0 to node.Count - 1 do ReindexNode(node.Item[i],n);

 if integer(node.Data) > n then node.Data := pointer(integer(node.Data)-1);
end;

procedure TKBEditorForm.DeleteNode(node: TTreeNode);
var
 i: integer;
begin
 if node = nil then exit;
 while node.Count > 0 do DeleteNode(node.Item[0]);

 if integer(node.Data) <= 0 then
  node.Delete
 else begin
  i := integer(node.Data);

  if i < 100 then begin
   ReindexNode(node.Owner.GetFirstNode,i);
   list(TmpRule.list_attr_if.Items[i - 1]).Destroy
  end else if i < 200 then begin
   ReindexNode(node.Owner.GetFirstNode.GetNextSibling,i);
   list(TmpRule.list_attr_then.Items[i - 101]).Destroy
  end else begin
   ReindexNode(node.Owner.GetFirstNode.GetNextSibling.GetNextSibling,i);
   list(TmpRule.list_attr_else.Items[i - 201]).Destroy
  end;

  node.Delete;
 end;
end;

procedure TKBEditorForm.ActionRuleNodeDeleteExecute(Sender: TObject);
var
 node: TTreeNode;
begin
 node := TreeRule.Selected;
 if (integer(node.Data) < 0) and (node.Count > 0) then begin
  if MessageYesNo('������� ��������� � ���������� ��������?','������� ��������') = IDNO then begin
   while node.Count > 0 do node.Item[0].MoveTo(node.Parent,naAddChild);
   node.Delete;
  end else DeleteNode(node);
 end else DeleteNode(node);
 MakeRuleNumbers(TmpRule);
end;

procedure TKBEditorForm.TreeRuleChange(Sender: TObject; Node: TTreeNode);
begin
 EditRuleExp.CancelBtnClick(Self);
 //if Node <> nil then Status('Data = %d, ImageIndex = %d, AbsoluteIndex = %d', [integer(Node.Data),Node.ImageIndex,Node.AbsoluteIndex]);
end;

procedure TKBEditorForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 EditRuleExp.CancelBtnClick(Self);
end;

procedure TKBEditorForm.ListBoxObjectsDblClick(Sender: TObject);
begin
 if RadioGroupTypes.ItemIndex = 1 then begin
  if EditRuleExp.Visible then EditRuleExp.ProcessDoubleClick(Self);
 end;
end;

procedure TKBEditorForm.RefreshNode(node: TTreeNode);
var
 n: integer;
 t: string;
 //i: integer;
 curass: list;
begin
 n := integer(node.Data);
 if n > 0 then begin
  if n < 100 then curass := list(TmpRule.list_attr_if.Items[n - 1])
  else if n < 200 then curass := list(TmpRule.list_attr_then.Items[n - 101])
  else curass := list(TmpRule.list_attr_else.Items[n - 201]);

  if (curass.n_obj < 0) or (curass.n_attr < 0) or (curass.znach = '') then begin
   node.Selected := true;
   ActionRuleNodeDeleteExecute(Self);
   exit;
  end;

  t := AssertionToStr(curass);
  node.Text := t;
 end else begin

 end;
end;

procedure TKBEditorForm.MakeRuleNumbers(rule: record_rule);
begin
 if rule <> nil then
  with rule do begin
   k_znach_if := list_attr_if.Count;
   k_znach_then := list_attr_then.Count;
   k_znach_else := list_attr_else.Count;
  end;
end;

function TKBEditorForm.CheckRuleTree: integer;
var
 i: integer;
 node: TTreeNode;
begin
 Result := 0;

 for i := 0 to TreeRule.Items.Count - 1 do begin
  node := TreeRule.Items[i];
  node.Selected := true;
  if integer(node.Data) = 0 then begin
   if (node.ImageIndex = 0) and (node.Count = 0) then Result := 1
   else if (node.ImageIndex = 0) and (node.Count > 1) then Result := 3
   else if (node.ImageIndex = 1) and (node.Count = 0) then Result := 2
  end else if integer(node.Data) < 0 then begin
   if (node.Count < 2) and (integer(node.Data) <> -3) then Result := 4;
  end;
  if Result <> 0 then break;
 end;
 //if TreeRule.Items.GetFirstNode.Count = 0 then Result := 1
 //else if TreeRule.Items.GetFirstNode.Count > 1 then Result := 3
 //else if TreeRule.Items.GetFirstNode.GetNextSibling.Count = 0 then Result := 2
end;

function TKBEditorForm.GetRuleString(node: TTreeNode): string;
var
 s: string;
 i: integer;
begin
 if node = nil then exit;
 s := '';

 if integer(node.Data) > 0 then s := 'P' + IntToStr(integer(node.Data))
 else if integer(node.Data) = -1 then begin
  s := '(';
  for i := 0 to node.Count - 1 do begin
   s := s + GetRuleString(node.Item[i]);
   if i < node.Count - 1 then s := s + ' | ';
  end;
  s := s + ')';
 end else if integer(node.Data) = -2 then begin
  s := '(';
  for i := 0 to node.Count - 1 do begin
   s := s + GetRuleString(node.Item[i]);
   if i < node.Count - 1 then s := s + ' & ';
  end;
  s := s + ')';
 end else if integer(node.Data) = -3 then begin
  s := '(~(';
  for i := 0 to node.Count - 1 do begin
   s := s + GetRuleString(node.Item[i]);
  end;
  s := s + '))';
 end;

 Result := s;
end;

procedure TKBEditorForm.ListBoxObjectsSubItemRClick(Sender: TObject;
  sectionidx, subitemidx: Integer);
begin
 ListBoxObjectsEnter(Self);
 ListBoxObjects.SetSelection(sectionidx,subitemidx);
 ListBoxObjectsClick(Self);
end;

procedure TKBEditorForm.ListBoxAttribsSubItemRClick(Sender: TObject;
  sectionidx, subitemidx: Integer);
begin
 ListBoxObjectsEnter(Self);
 ListBoxAttribs.SetSelection(sectionidx,subitemidx);
 ListBoxAttribsClick(Self);
end;

procedure TKBEditorForm.ListBoxTypesSubItemRClick(Sender: TObject;
  sectionidx, subitemidx: Integer);
begin
 ListBoxObjectsEnter(Self);
 ListBoxTypes.SetSelection(sectionidx,subitemidx);
 ListBoxTypesClick(Self);
end;

procedure TKBEditorForm.ListBoxValuesSubItemRClick(Sender: TObject;
  sectionidx, subitemidx: Integer);
begin
 ListBoxObjectsEnter(Self);
 ListBoxValues.SetSelection(sectionidx,subitemidx);
 ListBoxValuesClick(Self);
end;

procedure TKBEditorForm.TreeRuleDblClick(Sender: TObject);
begin
 if ButtonRulesCancel.Visible then ActionRuleNodeEditExecute(Self);
end;

procedure TKBEditorForm.RuleAssign(D,S: record_rule);
var
 i: integer;
 tmplist: list;
 lai,lat,lae: TKBOwnedCollection;
begin
 if (D = nil) or (S = nil) then exit;

 lai := D.list_attr_if;
 lat := D.list_attr_then;
 lae := D.list_attr_else;
 D.Assign(S);
 D.list_attr_if := lai;
 D.list_attr_then := lat;
 D.list_attr_else := lae;

 i := 1;
  with D do begin
    if list_attr_if.Count <> 0 then
      while list_attr_if.Count>0 do
       list(list_attr_if.Items[i-1]).Destroy;
    if list_attr_then.Count <> 0 then
      while list_attr_then.Count>0 do
        list(list_attr_then.Items[i-1]).Destroy;
    if list_attr_else.Count <> 0 then
      while list_attr_else.Count>0 do
        list(list_attr_else.Items[i-1]).Destroy;

    for i := 0 to S.list_attr_if.Count - 1 do begin
     tmplist := list.Create(D.list_attr_if);
     ListAssign(tmplist,list(S.list_attr_if.Items[i]));
    end;
    for i := 0 to S.list_attr_then.Count - 1 do begin
     tmplist := list.Create(D.list_attr_then);
     ListAssign(tmplist,list(S.list_attr_then.Items[i]));
    end;
    for i := 0 to S.list_attr_else.Count - 1 do begin
     tmplist := list.Create(D.list_attr_else);
     ListAssign(tmplist,list(S.list_attr_else.Items[i]));
    end;

  end;

end;

procedure TKBEditorForm.ListAssign(D,S: list);
begin
 with D do begin
  Assign(S);
  n_obj := S.n_obj;
  n_attr := S.n_attr;

  znach := S.znach;

  n_layer := S.n_layer;
  factor1 := S.factor1;
  factor2 := S.factor2;
  accuracy := S.accuracy;
  predicat := S.predicat;

  last_znach := S.last_znach;

  flag_del := S.flag_del;
  flag_end := S.flag_end;

  Scale1 := S.Scale1;
  Scale2 := S.Scale2;
  Scale3 := S.Scale3;

  Skip := S.Skip;
  MaximalPoint := S.MaximalPoint;
  PtrAttr := S.PtrAttr;
  last := S.last;
 end;
end;

procedure TKBEditorForm.RefreshStatus;
begin
 Status('���� ''%s'': �������� - %d, ����� - %d, ������ - %d',[KB.FileName,KB.ListObjs.Count,KB.ListTypes.Count,KB.ListRules.Count]);
end;

procedure TKBEditorForm.ListBoxObjectsEnter(Sender: TObject);
begin
 if not ButtonRulesEdit.Visible then exit;
 if RadioGroupTypes.ItemIndex = 0 then exit;
 RadioGroupTypes.ItemIndex := 0;
 RadioGroupTypesClick(Self);
end;

procedure TKBEditorForm.TreeRuleEnter(Sender: TObject);
begin
 if RadioGroupTypes.ItemIndex = 1 then exit;
 RadioGroupTypes.ItemIndex := 1;
 RadioGroupTypesClick(Self);
end;

procedure TKBEditorForm.ActionRulesCopyExecute(Sender: TObject);
var
 currule: record_rule;
 newrule: record_rule;
begin
 if (ComboBoxRules.ItemIndex < 0) or (KB.ListRules.Count = 0) then exit;

 currule := record_rule(KB.ListRules.Items[ComboBoxRules.ItemIndex]);
 newrule := record_rule.Create(KB.ListRules);
 RuleAssign(newrule, currule);
 RefreshRules;
 ComboBoxRules.ItemIndex := ComboBoxRules.Items.Count - 1;
 RefreshRules;
 RefreshStatus;

end;

procedure TKBEditorForm.SearchGoTo;
//var
// tmp: TSectionListBox;
begin
 ActionSearchPrev.Enabled := true;
 ActionSearchNext.Enabled := true;

 if SearchIndex = 0 then ActionSearchPrev.Enabled := false;
 if SearchIndex = SearchList.Count - 1 then ActionSearchNext.Enabled := false;

 if SearchList.Count = 0 then exit;

 case SearchMode of
  1: begin
      ListBoxObjectsClick(Self);
      //tmp := ListBoxObjects;
      CurList.ObjN := integer(SearchList.Items[SearchIndex]);
     end;
  2: begin
      ListBoxAttribsClick(Self);
      //tmp := ListBoxAttribs;
      CurList.AttribN := integer(SearchList.Items[SearchIndex]);
     end;
  3: begin
      ListBoxTypesClick(Self);
      //tmp := ListBoxTypes;
      CurList.TypeN := integer(SearchList.Items[SearchIndex]);
     end;
 else
  //tmp := nil;
 end;
 RefreshKBViewer;
 //MarkListBoxItems(tmp, SearchString);
 //RefreshKBViewer;
end;

procedure TKBEditorForm.ActionSearchNextExecute(Sender: TObject);
begin
 if SearchIndex < SearchList.Count - 1 then inc(SearchIndex);
 SearchGoTo;
end;

procedure TKBEditorForm.ActionSearchPrevExecute(Sender: TObject);
begin
 if SearchIndex > 0 then dec(SearchIndex);
 SearchGoTo;
end;

procedure TKBEditorForm.MarkListBoxItems(box: TSectionListBox; s: string);
var
 tmp, new: string;
 i, k: integer;
begin
 //box.BeginUpdate;
 if box = nil then exit;
 for i := 0 to SearchList.Count - 1 do begin
  new := box.Sections.Items[0].SubItems.Strings[integer(SearchList.Items[i])];
  tmp := AnsiLowerCase(new);
  k := System.Pos(s, tmp);
  Insert('</B>', new, k + Length(s));
  Insert('<B>', new, k);
  box.Sections.Items[0].SubItems.Strings[integer(SearchList.Items[i])] := new;
 end;
 //box.EndUpdate;
end;

procedure TKBEditorForm.ToolButtonSearchClick(Sender: TObject);
begin
 if Search(1) then Status('%s ������',[GetSearchName])
 else Status('%s �� ������',[GetSearchName]);
end;

procedure TKBEditorForm.FormActivate(Sender: TObject);
begin
 ShowItem(ShowWhat);
end;

procedure TKBEditorForm.ShowItem(what: string);
var
 objn, attrn, rulen, typen, valn, n: integer;
 tmp: string;
begin
 if (Length(what) = 0) then exit;

 ListBoxObjectsClick(Self);
 objn := -1;
 attrn := -1;
 rulen := -1;
 typen := -1;
 valn := -1;

 if (what[1] = 'T') then begin
  n := System.Pos(':', what);
  if n <> 0 then begin
   tmp := System.Copy(what, 2, n - 2);
   typen := StrToInt(tmp) - 1;
   if (what[n + 1] = 'V') then begin
    tmp := System.Copy(what, n + 2, Length(what) - (n + 1));
    valn := StrToInt(tmp) - 1;
   end;
  end else begin
   tmp := System.Copy(what, 2, Length(what) - 2);
   typen := StrToInt(tmp) - 1;
  end;
 end;

 if (what[1] = 'O') then begin
  n := System.Pos(':', what);
  if n <> 0 then begin
   tmp := System.Copy(what, 2, n - 2);
   objn := StrToInt(tmp) - 1;
   if (what[n + 1] = 'A') then begin
    tmp := System.Copy(what, n + 2, Length(what) - (n + 1));
    attrn := StrToInt(tmp) - 1;
   end;
  end else begin
   tmp := System.Copy(what, 2, Length(what) - 2);
   objn := StrToInt(tmp) - 1;
  end;
 end;

 if (what[1] = 'R') then begin
  tmp := System.Copy(what, 2, Length(what) - 1);
  rulen := StrToInt(tmp) - 1;
 end;

 if (rulen >= 0) then
  if (rulen < KB.ListRules.Count) then begin
   ComboBoxRules.ItemIndex := rulen;
   ComboBoxRulesClick(Self);
   TreeRule.SetFocus;
  end;

 if (objn >= 0) then begin
  ListBoxObjectsClick(Self);
  if (objn < KB.ListObjs.Count) then begin
   CurList.ObjN := objn;
   if (attrn >= 0) then if (attrn < record_obj(KB.ListObjs.Items[objn]).FListAttrs.Count) then begin
    CurList.AttribN := attrn;
    //ListBoxAttribsClick(Self);
   end;
  end;
  RefreshKBViewer;
 end;

 if (typen >= 0) then begin
  ListBoxTypesClick(Self);
  if (typen < KB.ListTypes.Count) then begin
   CurList.TypeN := typen;
   if (valn >= 0) then if (valn < record_type(KB.ListTypes.Items[typen]).list_znach.Count) then begin
    CurList.ValueN := valn;
   end;
  end;
  RefreshKBViewer;
 end;

end;

end.
