unit KB_Fuzzy;

 {******************************************************************************
 * ������ KB_Fuzzy.pas �������� ����������� ����������� ������ TValue:
 *
 * TFuzzyPoint - ����� ������� ��������������
 * TMembershipFunction - ������� ��������������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 4 ������, 2003 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, SysUtils, Dialogs,
  XML_Consts, Aspr95, {KBCompon,}
  KB_Containers{, KB_Types, KB_Values};

const
  eps = 0.000001; // ��� ������������ ������� ����� - �������� �� ���������
  zero = 0.0000001; // ����������� ����, �.�. ������ eps, ����� ��� ��������� double � InsertXY

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TFuzzyPoint �������� ����������� ����� ������� ��������������
  ////////////////////////////////////////////////////////////////////////////

  TFuzzyPoint = class(TKBItem)
  private
    fX: double;                 // �������� ����� ��
    fY: double;                 // �������� ����� �� (����������� � �������������� X ��������� ���������)
    procedure SetY(Y: double);
  public
    procedure SetXY(newX, newY: double);
    function GetXML: {Wide}String; override;
  published
    property X: double read fX write fX;
    property Y: double read fY write SetY;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TMembershipFunction �������� ����������� �������-�������� ��
  ////////////////////////////////////////////////////////////////////////////

  TMembershipFunction = class(TValue)
  private
    function GetXMin: double;
    function GetXMax: double;
  published
    property XMin: double read GetXMin;         // ����������� �������� ��
    property XMax: double read GetXMax;         // ������������ �������� ��
  public
    Points: TList;
    MFName: WideString;                         // ��� ��

//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
    procedure Evaluate; override;
    constructor Load(var F: Text; ACollection: TCollection);

    function CreateCopy: TMembershipFunction;   // ����������� ����� ��

    // ������������ � ��������������

    constructor Fuzzify(Value: TValue);         // ������������ ��������
    function Defuzzify: TValue;                 // �������������� ������� ������ �������

    // ��������������� �������

    procedure AddXY(X,Y: double);               // ���������� ����� � ����� ��
    procedure InsertXY(X,Y: double);            // ������� ����� � ������ ����� �� (����� �������), ������� ������������ ����� ���������
    procedure InsertMaxXY(X,Y: double);         // ������� ����� � ������ ����� �� (����� �������), ������� ������������ ���� ����� �������� ������
    function GetX(Index: integer): double;      // ���������� �������� �� ������� �����
    function GetY(Index: integer): double;      // ���������� �������� �� ������� �����

    function MFValueAt(X: double): double;      // �������� �� � X �������-�����������������
    function MaxLevel: double;                  // ������������ �������� ��

    // ������� �������� ��� �� (��������������� ������ ��)

    procedure XShiftBy(ShiftValue: double);     // ����� �� ��� X
    procedure XScaleBy(ScaleValue: double);     // ��������������� �� ��� X
    procedure YFuzzyBy(FuzzyValue: double);     // �������� �� ������ 0.5
    procedure Negate;                           // ��������� (��������� ������������ ������ 0.5)

    procedure YTopTruncateBy(alpha: double);    // �������� �� ������ alpha ������
    procedure YBottomTruncateBy(alpha: double); // �������� �� ������ alpha �����

    // ���������� ��������
    function Conjunction(MF: TMembershipFunction): TMembershipFunction;     // ����������
    function Disjunction(MF: TMembershipFunction): TMembershipFunction;     // ����������

    // �������������� ��������
    function FAdd(MF: TMembershipFunction): TMembershipFunction;        // ��������
    function FSub(MF: TMembershipFunction): TMembershipFunction;        // ���������
    function FMul(MF: TMembershipFunction): TMembershipFunction;        // ���������
    function FDiv(MF: TMembershipFunction): TMembershipFunction;        // �������

    // �������� ���������
    function FLt(MF: TMembershipFunction): double;      // ������
    function FGt(MF: TMembershipFunction): double;      // ������
    function FEq(MF: TMembershipFunction): double;      // �����
    function FNe(MF: TMembershipFunction): double;      // �������
    function FLe(MF: TMembershipFunction): double;      // ������ ��� �����
    function FGe(MF: TMembershipFunction): double;      // ������ ��� �����

    // ���������� ������ �������� ��� ����� � ������ ��������� � ����������� �� Op
    function EvalOp(Op: TOpMeta; Arg: TValue; var Res: TValue): boolean; override;

    // ������� "���������" ����� �� Points
    procedure Filter;

  private
    // ����������� ����� �� � ��������� ������
    procedure CopyPointsTo(var PointList: TList);
    // PC - ����� ����������� [PA1;PB1] � [PA2;PB2]
    function Intersect(PA1,PB1, PA2,PB2: TFuzzyPoint; var PC: TFuzzyPoint): boolean;
    // ������� ������� ��� �� ����� � ������ �� ����� X
    procedure Square(X: double; var L, R: double);
  public
    procedure Assign(MF: TMembershipFunction);
    function Defuzzificate : real;                         // ���������� �������� �� ����� ��������������.
  published
    property Defuzzified : real read Defuzzificate;               // �������� �� ����� ��������������.
  end;


implementation


/////////////////////////////////////////
// ������ ������ TFuzzyPoint
/////////////////////////////////////////

procedure TFuzzyPoint.SetXY(newX, newY: double);
begin
  X := newX;
  Y := newY;
end;

procedure TFuzzyPoint.SetY(Y: double);
begin
  if Y < 0 then fY := 0
  else if Y > 1 then fY := 1
  else fY := Y;
end;

function TFuzzyPoint.GetXML: {Wide}String;
begin
  Result := '<'+sPoint+' '+sX+'="' + FloatToStr(X) + '" '+sY+'="' + FloatToStr(Y) + '" />';
end;



/////////////////////////////////////////
// ������ ������ TMembershipFunction
/////////////////////////////////////////


constructor TMembershipFunction.Create(ACollection: TCollection);
begin
  inherited;
  Points := TList.Create;
end;

destructor TMembershipFunction.Destroy;
begin
  Points.Free;
  inherited;
end;

function TMembershipFunction.GetXML: {Wide}String;
var
  Doc: IXMLDocument;
  pt: IXMLNode;
  i: integer;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<'+sMF+' />');
  Doc.DocumentElement.Attributes[sName] := MFName;
  
  for i:=0 to Points.Count-1 do begin
    pt := Doc.DocumentElement.AddChild(sPoint);
    pt.Attributes[sX] := TFuzzyPoint(Points[i]).X;
    pt.Attributes[sY] := TFuzzyPoint(Points[i]).Y;
  end;

  Result := Doc.DocumentElement.XML;
end;

function TMembershipFunction.GetXMin: double;
begin
  if Points.Count = 0 then Result := 0
  else Result := TFuzzyPoint(Points[0]).X
end;

function TMembershipFunction.GetXMax: double;
begin
  if Points.Count = 0 then Result := 0
  else Result := TFuzzyPoint(Points[Points.Count-1]).X
end;

procedure TMembershipFunction.Evaluate;
begin
  // ����������������� � ResVal
  Defuzzify;
end;

function TMembershipFunction.CreateCopy: TMembershipFunction;
begin
  Result := TMembershipFunction.Create(nil);
  CopyPointsTo(Result.Points);
end;

procedure TMembershipFunction.CopyPointsTo(var PointList: TList);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  PointList.Clear;
  for i:=0 to Points.Count-1 do begin
    pt := TFuzzyPoint.Create(nil);
    pt.X := TFuzzyPoint(Points[i]).X;
    pt.Y := TFuzzyPoint(Points[i]).Y;
    PointList.Add(pt);
  end;
end;


// ������������ ��������
constructor TMembershipFunction.Fuzzify(Value: TValue);
var
  X, Acc, Belief: double;
begin
  inherited;
  Points := TList.Create;
  if Value.Evaluated = true then begin
    try
      X := Value.ResVal;
      Belief := Value.Attrs[sBelief] / 100;   // Y' = Y * belief + 0.5(1-belief)
      Acc := Value.Attrs[sAccuracy] / 100;    // 100% = 1 - ������ �����
      AddXY(X - X*(1 - Acc) - eps, 0.5*(1-Belief));    // ��� 0, �� �������� Belief
      AddXY(X, Acc * Belief + 0.5*(1-Belief));
      AddXY(X + X*(1 - Acc) + eps, 0.5*(1-Belief));
    except
    end;
  end;
end;


// �������������� ������� ������ �������
function TMembershipFunction.Defuzzify: TValue;
var
  i: integer;
  center, square, sum: double;
  pt1: TFuzzyPoint;
begin
  Result := TValue.Create(nil);
  Result.TypeMeta := tmNumeric;
  Result.Meta := vmLocal;
  Result.Evaluated := False;

  if (Points.Count > 0) then begin
    square := 0;
    sum := 0;
    center := (XMax + XMin) / 2; // �� ��������� - �������� ���������
    for i := 0 to Points.Count-1 do begin
      pt1 := TFuzzyPoint(Points[i]);
      Sum := Sum + pt1.Y;
      Square := Square + (pt1.Y*pt1.X);
    end;
    if sum <> 0 then center := square / sum;
    Result.Evaluated := true;
    Result.ResVal := center;
    // �������� ����� ����� � �� Self.Attrs
    Result.Attrs[sBelief] := MFValueAt(center);
  end;
end;

function TMembershipFunction.Defuzzificate : real;                         // ���������� �������� �� ����� ��������������.
var
  Val: TValue;
begin
  Val:=Defuzzify;
  Defuzzificate:=Val.ResVal;
  Val.Destroy;
end;

// ���������� ����� � ����� ��
procedure TMembershipFunction.AddXY(X,Y: double);
var pt: TFuzzyPoint;
begin
  pt := TFuzzyPoint.Create(nil);
  pt.X := X;
  pt.Y := Y;
  Points.Add(pt);
end;

// ������� ����� � ������ ����� ��
procedure TMembershipFunction.InsertXY(X,Y: double);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  i:=0;
  while i < Points.Count do begin
    pt := TFuzzyPoint(Points[i]);
    if abs(pt.X - X) < zero then begin
      pt.Y := Y;
      exit
    end
    else if pt.X < X then Inc(i)
    else break;
  end;
  pt := TFuzzyPoint.Create(nil);
  pt.X := X;
  pt.Y := Y;
  Points.Insert(i, pt);
end;

// ������� ����� � ������ ����� ��, ������� ������������ �������� ������������
procedure TMembershipFunction.InsertMaxXY(X,Y: double);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  i:=0;
  while i < Points.Count do begin
    pt := TFuzzyPoint(Points[i]);
    if abs(pt.X - X) < zero then begin
      if pt.Y < Y then pt.Y := Y;       // �������� ������ ���� ����� �������� ������ ������
      exit
    end
    else if pt.X < X then Inc(i)
    else break;
  end;
  pt := TFuzzyPoint.Create(nil);
  pt.X := X;
  pt.Y := Y;
  Points.Insert(i, pt);
end;

// ���������� �������� �� ������� �����
function TMembershipFunction.GetX(Index: integer): double;
begin
  Result := TFuzzyPoint(Points[Index]).X;
end;

// ���������� �������� �� ������� �����
function TMembershipFunction.GetY(Index: integer): double;
begin
  Result := TFuzzyPoint(Points[Index]).Y;
end;

// ����� �� ��� X �� �������� ShiftValue
procedure TMembershipFunction.XShiftBy(ShiftValue: double);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  if ShiftValue <> 0 then begin
    for i:=0 to Points.Count-1 do begin
      pt := Points[i];
      pt.X := pt.X + ShiftValue;
    end;
    //XMin := XMin + ShiftValue;
    //XMax := XMax + ShiftValue;
  end;
end;


// ��������������� �� ��� X �� �������� ScaleValue
procedure TMembershipFunction.XScaleBy(ScaleValue: double);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  if ScaleValue <> 0 then begin
    for i:=0 to Points.Count-1 do begin
      pt := Points[i];
      pt.X := pt.X * ScaleValue;
    end;
    //XMin := XMin * ScaleValue;
    //XMax := XMax * ScaleValue;
  end;
end;


// �������� �� ������ 0.5 � ������������ � ��������� FuzzyValue
procedure TMembershipFunction.YFuzzyBy(FuzzyValue: double);
var
  i: integer;
  pt: TFuzzyPoint;
begin
  for i:=0 to Points.Count-1 do begin
    pt := Points[i];
    pt.Y := (pt.Y - 0.5) * FuzzyValue + 0.5;
    if pt.Y > 1 then pt.Y := 1;
    if pt.Y < 0 then pt.Y := 0;
  end;
end;


// ��������� (��������� ������������ ������ 0.5)
procedure TMembershipFunction.Negate;
var i: integer;
begin
  for i:=0 to Points.Count-1 do
    TFuzzyPoint(Points[i]).Y := 1 - TFuzzyPoint(Points[i]).Y;
end;


// �������� �� � X �������-�����������������
function TMembershipFunction.MFValueAt(X: double): double;
var
  i: integer;
  pt1,pt2: TFuzzyPoint;
begin
  Result := 0;
  if Points.Count > 0 then begin
    // �������� ��������
    if X <= XMin then Result := TFuzzyPoint(Points[0]).Y
    else if X >= XMax then Result := TFuzzyPoint(Points[Points.Count-1]).Y
    else begin
      // ����� ��� �������� ������ X � ��������������� �������� ��� X �� �� ���������
      i:=0;
      pt2 := Points[0];
      while (i < Points.Count) do begin
        pt2 := Points[i];
        if pt2.X < X then Inc(i)
        else break; // ����� ����� ������ �� X
      end;
      if (i>=Points.Count) or (i<=0) then Result := pt2.Y
      else begin
        pt1 := Points[i-1]; // � ��� ����� ����� �� X
        try
          Result := pt1.Y + (X - pt1.X) * (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
        except
          Result := pt2.Y; // ������ ������� �� 0
        end;
      end;
    end;
  end;
end;


// ������������ �������� ��
function TMembershipFunction.MaxLevel: double;
var
  i: integer;
  pt: TFuzzyPoint;
begin
  Result := 0; // ������� �����������
  for i:=0 to Points.Count-1 do begin
    pt := Points[i];
    if pt.Y > Result then Result := pt.Y;
  end;
end;


// ���� ������� [PA1; PB1] � [PA2; PB2] ������������, �� ���������� ����� ����������� PC
function TMembershipFunction.Intersect(PA1,PB1, PA2,PB2: TFuzzyPoint; var PC: TFuzzyPoint): boolean;
var
  A1,B1,C1, A2,B2,C2: double; // ��� ��������� ������
  dPA1, dPB1, dPA2, dPB2: double;
  q1q2: double;
begin
  // �������� ����������� ��������:
  // PA2 � PB2 ����� �� ������ ������� �� [PA1; PB1]
  // PA1 � PB1 ����� �� ������ ������� �� [PA2; PB2]
  // A = - (y2 - y1), B = x2 - x1, C = y2 * x1 - y1 * x2

  Result := False;

  // ������, �� ������� ����� ������� [PA1; PB1]
  A1 := -(PB1.Y - PA1.Y);
  B1 := (PB1.X - PA1.X);
  C1 := (PB1.Y * PA1.X - PB1.X * PA1.Y);

  // ������, �� ������� ����� ������� [PA2; PB2]
  A2 := -(PB2.Y - PA2.Y);
  B2 := (PB2.X - PA2.X);
  C2 := (PB2.Y * PA2.X - PB2.X * PA2.Y);

  q1q2 := A1*B2 - A2*B1; // ��������� ������������ ������������ �������� ������

  // �������� ����� ������� �� ������ �� ����� ������ (������� ������)
  if abs(q1q2) < zero then begin
    // �����, ���� ������� �����-������ ���� �����
    PC := TFuzzyPoint.Create(nil);
    PC.X := PB1.X;
    PC.Y := PB1.Y;
    Result := True;
  end
  else begin
    // ����������
    dPA2 := A1 * PA2.X + B1 * PA2.Y + C1;  // ���������� PA2 �� [PA1; PB1]
    dPB2 := A1 * PB2.X + B1 * PB2.Y + C1;  // ���������� PB2 �� [PA1; PB1]
    dPA1 := A2 * PA1.X + B2 * PA1.Y + C2;  // ���������� PA1 �� [PA2; PB2]
    dPB1 := A2 * PB1.X + B2 * PB1.Y + C2;  // ���������� PA1 �� [PA2; PB2]

    // ���� ����� ���������� ������ � ��������� �� eps, �� ������������ ��� ��������
    if (dPA2 * dPB2 <= eps) and (dPA1 * dPB1 <= eps) then begin
      PC := TFuzzyPoint.Create(nil);
      PC.X := (B1*C2 - B2*C1) / q1q2;
      PC.Y := (A2*C1 - A1*C2) / q1q2;
      Result := True;
    end;
  end;
end;


// ����������
function TMembershipFunction.Conjunction(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  MF1,MF2: TMembershipFunction;
  x1,y1,x2,y2: double;
  PA1, PB1, PA2, PB2, PC: TFuzzyPoint;
begin
  // ���� ���� �� MF ������, �� ������ ���������� ������ MF
  if Points.Count = 0 then begin
    Result := MF.CreateCopy;
    exit;
  end;
  if MF.Points.Count = 0 then begin
    Result := Self.CreateCopy;
    exit;
  end;

  // ��� �������� ���������
  MF1 := Self.CreateCopy;
  MF1.MFName := 'MF1';
  MF2 := MF.CreateCopy;
  MF2.MFName := 'MF2';
  Result := TMembershipFunction.Create(nil);
  Result.MFName := 'Res';
  PC := TFuzzyPoint.Create(nil);

  // ��������� ��������
  x1 := MF1.GetX(0);
  y1 := MF1.GetY(0);
  x2 := MF2.GetX(0);
  y2 := MF2.GetY(0);
  if (x1 < x2) then MF2.InsertXY(x1, y2)
  else if (x1 > x2) then MF1.InsertXY(x2, y1);

  x1 := MF1.GetX(MF1.Points.Count-1);
  y1 := MF1.GetY(MF1.Points.Count-1);
  x2 := MF2.GetX(MF2.Points.Count-1);
  y2 := MF2.GetY(MF2.Points.Count-1);
  if (x1 < x2) then MF1.InsertXY(x2, y1)
  else if (x1 > x2) then MF2.InsertXY(x1, y2);
    
  // ������ ������ ������
  i := MF1.Points.Count-1;
  j := MF2.Points.Count-1;
  while (i>0) and (j>0) do begin
    PA1 := MF1.Points[i-1];     // ������ ������� [PA1; PB1]
    PB1 := MF1.Points[i];
    PA2 := MF2.Points[j-1];     // ������ ������� [PA2; PB2]
    PB2 := MF2.Points[j];
    if Intersect(PA1, PB1, PA2, PB2,  PC)
    then begin
      // ��������� ������ ����� ��������, ���� ��� ���� ������ ������
      if PB1.Y < MF2.MFValueAt(PB1.X) then Result.InsertXY(PB1.X, PB1.Y);
      if PB2.Y < MF1.MFValueAt(PB2.X) then Result.InsertXY(PB2.X, PB2.Y);
      // �������� ����� ����������� �� ��� ��� ��, ���� �� ��� ���
      MF1.InsertXY(PC.X, PC.Y);
      MF2.InsertXY(PC.X, PC.Y);
      Result.InsertXY(PC.X, PC.Y);
      // �������������� �������, ��� ����� �������� ������, � ���� ���������, �� ���
      if PA1.X >= PA2.X then Dec(i);
      if PA2.X >= PA1.X then Dec(j);
    end
    else begin
      // �������������� �������, ��� ����� �������� ������, � ���� ���������, �� ���
      // ��������� ������ ����� ����������� �������, ���� ��� ���� ������ ������
      if PA1.X >= PA2.X then begin
        if PB1.Y < MF2.MFValueAt(PB1.X) then Result.InsertXY(PB1.X, PB1.Y);
        Dec(i);
      end;
      if PA2.X >= PA1.X then begin
        if PB2.Y < MF1.MFValueAt(PB2.X) then Result.InsertXY(PB2.X, PB2.Y);
        Dec(j);
      end;
    end
  end;

  // ��� �� �������, �� ��� ����� � i � j = 0, ������ ��� �������� ��������� (��� ��� ����� ���� �� � or ������ and ��������� � while)
  // ��������� ����� ���������
  PA1 := MF1.Points[0];
  PA2 := MF2.Points[0];
  if (PA1.Y < PA2.Y) then Result.InsertXY(PA1.X, PA1.Y)
  else Result.InsertXY(PA2.X, PA2.Y);
end;


// ���������� (���������� ����������)
function TMembershipFunction.Disjunction(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  MF1,MF2: TMembershipFunction;
  x1,y1,x2,y2: double;
  PA1, PB1, PA2, PB2, PC: TFuzzyPoint;
begin
  // ���� ���� �� MF ������, �� ������ ���������� ������ MF
  if Points.Count = 0 then begin
    Result := MF.CreateCopy;
    exit;
  end;
  if MF.Points.Count = 0 then begin
    Result := Self.CreateCopy;
    exit;
  end;

  // ��� �������� ���������
  MF1 := Self.CreateCopy;
  MF1.MFName := 'MF1';
  MF2 := MF.CreateCopy;
  MF2.MFName := 'MF2';
  Result := TMembershipFunction.Create(nil);
  Result.MFName := 'Res';
  PC := TFuzzyPoint.Create(nil);

  // ��������� ��������
  x1 := MF1.GetX(0);
  y1 := MF1.GetY(0);
  x2 := MF2.GetX(0);
  y2 := MF2.GetY(0);
  if (x1 < x2) then MF2.InsertXY(x1, y2)
  else if (x1 > x2) then MF1.InsertXY(x2, y1);

  x1 := MF1.GetX(MF1.Points.Count-1);
  y1 := MF1.GetY(MF1.Points.Count-1);
  x2 := MF2.GetX(MF2.Points.Count-1);
  y2 := MF2.GetY(MF2.Points.Count-1);
  if (x1 < x2) then MF1.InsertXY(x2, y1)
  else if (x1 > x2) then MF2.InsertXY(x1, y2);

  // ������ ������ ������
  i := MF1.Points.Count-1;
  j := MF2.Points.Count-1;
  while (i>0) and (j>0) do begin
    PA1 := MF1.Points[i-1];     // ������ ������� [PA1; PB1]
    PB1 := MF1.Points[i];
    PA2 := MF2.Points[j-1];     // ������ ������� [PA2; PB2]
    PB2 := MF2.Points[j];
    if Intersect(PA1, PB1, PA2, PB2, PC)
    then begin
      // ��������� ������ ����� ��������, ���� ��� ���� ������ ������
      if PB1.Y > MF2.MFValueAt(PB1.X) then Result.InsertXY(PB1.X, PB1.Y);
      if PB2.Y > MF1.MFValueAt(PB2.X) then Result.InsertXY(PB2.X, PB2.Y);
      // �������� ����� ����������� �� ��� ��� ��, ���� �� ��� ���
      MF1.InsertXY(PC.X, PC.Y);
      MF2.InsertXY(PC.X, PC.Y);
      Result.InsertXY(PC.X, PC.Y);
      // �������������� �������, ��� ����� �������� ������, � ���� ���������, �� ���
      if PA1.X >= PA2.X then Dec(i);
      if PA2.X >= PA1.X then Dec(j);
    end
    else begin
      // �������������� �������, ��� ����� �������� ������, � ���� ���������, �� ���
      // ��������� ������ ����� ����������� �������, ���� ��� ���� ������ ������
      if PA1.X >= PA2.X then begin
        if PB1.Y > MF2.MFValueAt(PB1.X) then Result.InsertXY(PB1.X, PB1.Y);
        Dec(i);
      end;
      if PA2.X >= PA1.X then begin
        if PB2.Y > MF1.MFValueAt(PB2.X) then Result.InsertXY(PB2.X, PB2.Y);
        Dec(j);
      end;
    end
  end;

  // ��� �� �������, �� ��� ����� � i � j = 0, ������ ��� �������� ��������� (��� ��� ����� ���� �� � or ������ and ��������� � while)
  // ��������� ����� ���������
  PA1 := MF1.Points[0];
  PA2 := MF2.Points[0];
  if (PA1.Y > PA2.Y) then Result.InsertXY(PA1.X, PA1.Y)
  else Result.InsertXY(PA2.X, PA2.Y);
end;


// �������� �� ������ alpha ������
procedure TMembershipFunction.YTopTruncateBy(alpha: double);
var
  MF, Res: TMembershipFunction;
begin
  if (alpha > 1) then alpha := 1
  else if alpha < 0 then alpha := 0;

  MF := TMembershipFunction.Create(nil);
  MF.AddXY(Self.GetXMin, alpha);
  MF.AddXY(Self.GetXMax, alpha);

  Res := Self.Disjunction(MF);
  Res.CopyPointsTo(Self.Points);
end;


// �������� �� ������ alpha �����
procedure TMembershipFunction.YBottomTruncateBy(alpha: double);
var
  MF, Res: TMembershipFunction;
begin
  if (alpha > 1) then alpha := 1
  else if alpha < 0 then alpha := 0;

  MF := TMembershipFunction.Create(nil);
  MF.AddXY(Self.GetXMin, 1-alpha);
  MF.AddXY(Self.GetXMax, 1-alpha);

  Self.Negate;
  Res := Self.Disjunction(MF);
  Res.Negate;

  //Self.Negate;
  Res.CopyPointsTo(Self.Points);
end;


// ������� "���������" �����
procedure TMembershipFunction.Filter;
var
  i: integer;
  P1, P2, P3: TFuzzyPoint;
begin
  i := 1;
  while i<Points.Count-1 do begin
    P1 := Points[i-1];
    P2 := Points[i];
    P3 := Points[i+1];
    if (P1.Y >= P2.Y) and (P2.Y <= P3.Y)
    then Points.Delete(i)
    else Inc(i);
  end;
end;


// ��������������

// ��������
function TMembershipFunction.FAdd(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  P1, P2: TFuzzyPoint;
  X, Y: double;
begin
  Result := TMembershipFunction.Create(nil);
  for i:=0 to Self.Points.Count-1 do begin
    P1 := Self.Points[i];
    for j:=0 to MF.Points.Count-1 do begin
      P2 := MF.Points[j];
      X := P1.X + P2.X;                                 // ��� �������� - "+"
      if P1.Y < P2.Y then Y := P1.Y else Y := P2.Y;     // �������
      Result.InsertMaxXY(X, Y);
    end;
  end;
  Result.Filter;        // ������� ���������� ��
end;

// ���������
function TMembershipFunction.FSub(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  P1, P2: TFuzzyPoint;
  X, Y: double;
begin
  Result := TMembershipFunction.Create(nil);
  for i:=0 to Self.Points.Count-1 do begin
    P1 := Self.Points[i];
    for j:=0 to MF.Points.Count-1 do begin
      P2 := MF.Points[j];
      X := P1.X - P2.X;                                 // ��� ��������� - "-"
      if P1.Y < P2.Y then Y := P1.Y else Y := P2.Y;     // �������
      Result.InsertMaxXY(X, Y);
    end;
  end;
  Result.Filter;        // ������� ���������� ��
end;

// ���������
function TMembershipFunction.FMul(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  P1, P2: TFuzzyPoint;
  X, Y: double;
begin
  Result := TMembershipFunction.Create(nil);
  for i:=0 to Self.Points.Count-1 do begin
    P1 := Self.Points[i];
    for j:=0 to MF.Points.Count-1 do begin
      P2 := MF.Points[j];
      X := P1.X * P2.X;                                 // ��� ��������� - "*"
      if P1.Y < P2.Y then Y := P1.Y else Y := P2.Y;     // �������
      Result.InsertMaxXY(X, Y);
    end;
  end;
  Result.Filter;        // ������� ���������� ��
  //Result.Filter;        // � ��� ���, ��� ��� �� ������ ��� ��������  
end;

// �������
function TMembershipFunction.FDiv(MF: TMembershipFunction): TMembershipFunction;
var
  i,j: integer;
  P1, P2: TFuzzyPoint;
  X, Y: double;
begin
  Result := TMembershipFunction.Create(nil);
  for i:=0 to Self.Points.Count-1 do begin
    P1 := Self.Points[i];
    for j:=0 to MF.Points.Count-1 do begin
      P2 := MF.Points[j];
      if P2.X <> 0 then begin                             // �� ��������� ����� 0 � �����������
        X := P1.X / P2.X;                                 // ��� ������� - "/"
        if P1.Y < P2.Y then Y := P1.Y else Y := P2.Y;     // �������
        Result.InsertMaxXY(X, Y);
      end;
    end;
  end;
  Result.Filter;        // ������� ���������� ��
  Result.Filter;        // � ��� ���, ��� ��� �� ������ ��� ��������
end;


// �������� ��������� (���������� ����������� �� 0 �� 1)

// ������� ������� ��� �� ����� � ������ �� ������-���� ����� (��������������)
procedure TMembershipFunction.Square(X: double; var L, R: double);
var
  i: integer;
  delta: double;
begin
  L := 0;
  R := 0;
  InsertXY(X, MFValueAt(0));
  for i:=0 to Points.Count-2 do begin
    delta := (GetX(i+1) - GetX(i))*(GetY(i) + GetY(i+1))/2;   // ������� ��������� ��������
    if GetX(i+1) <= X then L := L + delta else R := R + delta;
  end;
end;

// ������
function TMembershipFunction.FGt(MF: TMembershipFunction): double;
var
  Diff: TMembershipFunction;
  L,R: double;
  Val: TValue;
begin
  // a > b <=> a-b > 0
  Result := 1;
  // ������, ������ ���� ������ ������� Self ������ ������� � MF �� OX
  if Self.GetXMin < MF.GetXMax then begin
    Diff := Self.FSub(MF); // ������� �� � �����
    Diff.Square(0, L, R);
    if (R >= 0) and ((L+R) <> 0) then Result := R /(L+R)
    else Result := 0;

    //Val := Diff.Defuzzify;
    //if Val.ResVal > 0 then Result := Diff.MFValueAt(Val.ResVal);
  end;
end;

// �����
function TMembershipFunction.FEq(MF: TMembershipFunction): double;
var
  temp: TMembershipFunction;
  //L,R: double;
  //Val: TValue;
begin
  // a = b <=> MaxLevel(a&b)
  temp := Conjunction(MF);
  Result := temp.MaxLevel;
{  Result := 0;
  // ������, ������ ���� Self ������������ � MF �� OX
  if (Self.GetXMin <= MF.GetXMax) or (MF.GetXMin <= Self.GetXMax) then begin
    temp := Self.FSub(MF);
    temp.Square(0, L, R);
    if (L+R) <> 0 then Result := 1 - abs((R-L) /(L+R))
    else Result := 0;

    Val := temp.Defuzzify;
    if Val.ResVal = 0 then Result := temp.MFValueAt(Val.ResVal);
  end;}
  temp.Free;
end;

// ������ ��� �����
function TMembershipFunction.FGe(MF: TMembershipFunction): double;
var
  Diff: TMembershipFunction;
  L,R: double;
  Val: TValue;
begin
  // a >= b <=> a-b >= 0
  {Result := 1;
  // ������, ������ ���� ������ ������� Self ������ ������� � MF �� OX
  if Self.GetXMin <= MF.GetXMax then begin
    Diff := Self.FSub(MF); // ������� �� � �����
    Diff.Square(0, L, R);
    if (R >= 0) and ((L+R) <> 0) then Result := R /(L+R)
    else Result := 0;

    // �� ������ ���� ���� � ����� ����������
    Val := Diff.Defuzzify;
    if Val.ResVal = 0 then Result := Diff.MFValueAt(Val.ResVal);
  end;}

  // ������ ������
  // a >= b <=> (a > b) V (a = b)
  L := FEq(MF);
  R := FGt(MF);
  Result := L + R - L*R;        //  a V b = a+b-a*b ��� �������
end;

// �������
function TMembershipFunction.FNe(MF: TMembershipFunction): double;
begin
  // a <> b <=> 1 - (a = b)
  Result := 1 - FEq(MF);
end;

// ������
function TMembershipFunction.FLt(MF: TMembershipFunction): double;
begin
  // a < b <=> b > a
  Result := MF.FGt(Self);
end;

// ������ ��� �����
function TMembershipFunction.FLe(MF: TMembershipFunction): double;
begin
  // a <= b <=> b >= a
  Result := MF.FGe(Self);
end;


// ��������� ��� �������� ��� ��

function TMembershipFunction.EvalOp(Op: TOpMeta; Arg: TValue;  var Res: TValue): boolean;
var MF: TMembershipFunction;
begin
  Result := false;
  if not (Arg is TMembershipFunction) then
    exit;

  MF := Arg as TMembershipFunction;
  case Op of
  ///////// ���������� //////////
  omAnd:
    begin
      Res := Conjunction(MF);
      Result := true;
    end;
  omOr:
    begin
      Res := Disjunction(MF);
      Result := true;
    end;
  omNot:
    begin
      Res := Self.CreateCopy;
      (Res as TMembershipFunction).Negate;
      Result := true;
    end;
  omXor:
    begin
      // ����� ��� �����������������, � ����� � ��� a & ~b + ~a & b
    end;
  ///////// ��������� //////////
  omGt:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FGt(MF);
      Result := true;
    end;
  omLt:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FLt(MF);
      Result := true;
    end;
  omEq:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FEq(MF);
      Result := true;
    end;
  omNe:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FNe(MF);
      Result := true;
    end;
  omGe:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FGe(MF);
      Result := true;
    end;
  omLe:
    begin
      Res := TValue.Create(nil);
      Res.TypeMeta := tmNumeric;
      Res.Evaluated := true;
      Res.ResVal := FLe(MF);
      Result := true;
    end;
  ///////// �������������� //////////
  omMul:
    begin
      Res := FMul(MF);
      Result := true;
    end;
  omSub:
    begin
      Res := FSub(MF);
      Result := true;
    end;
  omDiv:
    begin
      Res := FDiv(MF);
      Result := true;
    end;
  omAdd:
    begin
      Res := FAdd(MF);
      Result := true;
    end;
  omMod,omNeg:
    begin
      // ������ ���� �����������������
    end;
  end;
end;

procedure TMembershipFunction.Assign(MF: TMembershipFunction);
begin
  MF.CopyPointsTo(Self.Points);
  MFName:=MF.MFName;
end;

constructor TMembershipFunction.Load(var F: Text; ACollection: TCollection);
begin
end;

initialization
  RegisterClasses([TMembershipFunction,TFuzzyPoint]);

end.
