unit XML_Consts;

interface

const
  // ����� ����� � XML-�������� ���� ������
  sCreationDate = 'creation-date';
  sProblemInfo = 'problem-info';
  sKnowledgeBase = 'knowledge-base';

  sTypes = 'types';
  sClasses = 'classes';
  sInitialSituation = 'initial-situation';

  sType = 'type';
  sValue = 'value';
  sFrom = 'from';
  sTo = 'to';
  sParameter = 'parameter';
  sMF = 'mf';
  sPoint = 'point';
  sUnit = 'measure-item';
  sAlias = 'alias';

  sClass = 'class';
  sProgID = 'progid';
  sProperties = 'properties';
  sRules = 'rules';
  sMethods = 'methods';
  sProperty = 'property';
  sRule = 'rule';
  sMethod = 'method';

  sAttributes = 'attributes';
  sQuestion = 'question';
  sQuery = 'query';

  sCondition = 'condition';
  sAction = 'action';
  sElseAction = 'else-action';

  sValRef = 'val-ref';
  sEQ = 'eq';
  sNE = 'ne';
  sGT = 'gt';
  sLT = 'lt';
  sGE = 'ge';
  sLE = 'le';
  sNOT = 'not';
  sOR = 'or';
  sXOR = 'xor';
  sAND = 'and';
  sADD = 'add';
  sSUB = 'sub';
  sDIV = 'div';
  sMUL = 'mul';
  sMOD = 'mod';
  sNEG = 'neg';

  sVarDecl = 'var-decl';
  sStatements = 'statements';
  sWhile = 'while';
  sIf = 'if';
  sAssign = 'assignment';
  sCall = 'call';
  sExtCall = 'ext-call';
  sReturn = 'return';
  sParams = 'params';
  sParamsDecl = 'params-decl';
  sParamDecl = 'param-decl';
  sWith = 'with';

  sObjects = 'objects';
  sObject = 'object';
  sPropertyValue = 'property-value';
  sArgs = 'args';

  sGoals = 'goals';
  sGoalAssertion = 'goal-assertion';
  sGoal = 'goal';

  // �������� ����� � XML-��������

  sMeta = 'meta';
  sID = 'id';
  sDesc = 'desc';
  sDefault = 'default';
  sConfidence = 'confidence';
  sResultType = 'result-type';
  sModifier = 'modifier';
  sCheckBefore = 'check-before';
  sAncestor = 'ancestor';
  sName = 'name';
  sSource = 'source';
  sCParams = 'cparams';
  sLValue = 'lvalue';
  sBelief = 'belief';
  sProbability = 'probability';
  sAccuracy = 'accuracy';
  sExt = 'ext';
  sX = 'x';
  sY = 'y';
  sMinValue = 'min-value';
  sMaxValue = 'max-value';

  // �������� ���������
  
  sNumber = 'number';
  sString = 'string';
  sFuzzy = 'fuzzy';
  sTrue = 'true';
  sFalse = 'false';
  sBoolean = 'boolean';
  sArray = 'array';
  sReference = 'reference';
  sInferred = 'inferred';
  sSupplied = 'supplied';
  sIn = 'in';
  sOut = 'out';
  sBoth = 'both';


implementation

end.
 