unit KB_Containers;

 {******************************************************************************
 * ������ KB_Containers.pas �������� ����������� ��������� �������:
 *
 * TKBItem - ������� �����, ������� ����������� � XML
 * TKBType - ��������� ��� ���� ������, ������������� � ���� ������
 * TKBContainer - ��������� ���� ������ �������� ������
 * TKBClass - ��������� ��� ������ ���� ������
 * TAttrList - �����-��������, �������� �������� ���� <����������, ��������>
 * TValue - ��������� ��� �������� �������� ����-����
 * TInitial - ��������� ��� �������� ��������� ��������
 * TClassProperty - ��������� ��� �������� ������ ���� ������
 * TClassMethod - ��������� ��� ������ ������ ���� ������
 * TClassRule - ��������� ��� ������� ������ ���� ������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface
                                     
uses
  Classes, XMLDoc, XMLIntf, Variants,
  XML_Consts, Aspr95;

type

  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TSource ��������� ������ ��������� �������� �������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TPropertySource =
  (
    psQuestion,   // �������� �������� ���������� �������� ������� ������������
    psQuery,      // �������� �������� ���������� ����������� ������� � ��
    psInferred,   // �������� �������� ��������� ���������
    psSupplied,   // �������� �������� ������� �� ��������� ��������
    psNone        // ����������� �������� �������� �������� (��� ������� ���������� ��������)
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TTypeMeta ���������, ��� ������ ����� TKBType
  ////////////////////////////////////////////////////////////////////////////

  TTypeMeta = (tmNumeric, tmBoolean, tmSymbolic, tmFuzzy, tmArray, tmMethod, tmRef);


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TValueMeta ���������, ��� ������ ����� TValue
  ////////////////////////////////////////////////////////////////////////////

  TValueMeta = (vmProperty, vmFunction, vmLocal, vmReference, vmOperation);


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TOpMeta ��������� ��� ��������, ������������� � ������
  // ���������� ������ TOperation
  ////////////////////////////////////////////////////////////////////////////

  TOpMeta = (
    omMul, omSub, omDiv, omAdd, omNeg, omMod,   // ��������������
    omAnd, omXor, omOr, omNot,                  // ����������
    omGt, omLt, omEq, omNe, omGe, omLe          // ���������
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TInstrMeta ��������� ��� ����������
  ////////////////////////////////////////////////////////////////////////////

  TInstrMeta =
    (
    imAssign,   // ���������� ������������
    imReturn,   // ���������� �������� �������� ������� � ������ �� ���
    imWhile,    // ���������� �����
    imIf,       // ���������� ��������� ���������
    imCall,     // ���������� ������ ������ ������
    imExtCall,  // ���������� ��������� � COM-�������
    imVarDecl   // ���������� ���������� ��������� ����������
    );

  ////////////////////////////////////////////////////////////////////////////
  // ������������, ���������� ��-�������: �����������, �����������, ��������
  ////////////////////////////////////////////////////////////////////////////

  TConfidence = double;         // ����������� [0,1]
  TProbability = double;        // ����������� [0,1]
  TAccuracy = double;           // �������� [0,1]


  ////////////////////////////////////////////////////////////////////////////
  // ����� TKBItem �������� ������� ���� �����������, ����������� � XML
  ////////////////////////////////////////////////////////////////////////////

  {TKBItem = class(TComponent)
  public
    function XML: String; virtual;
  end;}

  ////////////////////////////////////////////////////////////////////////////
  // ����� TAttrList ������ ������ ��������� ���� <����������, ��������>
  ////////////////////////////////////////////////////////////////////////////

  TAttrList = class(TObject{TComponent})
  private
    Belief: Variant;
    Probability: Variant;
    Accuracy: Variant;
    function GetAttr(Name: WideString): Variant;
    procedure SetAttr(Name: WideString; Value: Variant);
  public
    constructor Create{(AOwner: TComponent)}; virtual;//override;
    property Items[Name: WideString]: Variant read GetAttr write SetAttr; default;
    function XML: WideString;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ������� ����� TKBType �������� ����������� ����, ������������ � ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TValue = class;               // please wait...

  TKBType = class(TKBItem)
  public
    ID: WideString;             // ������������� ����
    Meta: TTypeMeta;            // ������� ����
    Desc: WideString;           // �������� ����
    function Valid(Value: TValue): boolean; virtual; // ��������� �������� �� �������������� ����
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ������� ����� TValue �������� ����������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TValue = class(TKBItem)
  public
    TypeMeta: TTypeMeta;        // ������� �������� (��� runtime)
    TypeRef: TKBType;           // ������ �� ��� �������� (��� desisn-time � run-time)
    ResVal: Variant;            // ����������� ��� �������� ��������
    Meta: TValueMeta;           // ��� �������� (�������, ������, ��������, ��������)
    Attrs: TAttrList;           // �������� �������� (�����������, �������� � �.�.)
    Evaluated: boolean;         // ������� ����, ��� �������� ��������� ��� �������� ��� ����������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    // ��������� ����������� ��������
    procedure Evaluate; virtual;
    // ��������� ��������� �������� Op ��� ����� � Arg, �������� ��� � Res � ���������� True ���� ��������
    function EvalOp(Op: TOpMeta; Arg: TValue; var Res: TValue): boolean; virtual;
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TInitial �������� ����������� ��������� ��������
  ////////////////////////////////////////////////////////////////////////////

{  TInitial = class(TKBItem)
  public
    Objects: TList;
    Goals: TList;
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: WideString; override;
  end;
}

  ////////////////////////////////////////////////////////////////////////////
  // ����� TKBContainer �������� ����������� ���� ������ �������� ������
  ////////////////////////////////////////////////////////////////////////////

{  TKBContainer = class(TKBItem)
  public
    CreationDate: string;       // ���� �������� ���� ������
    ProblemName: string;        // �������� �������� ������
    Types: TList;               // ������ ����� - ������� TKBType
    Classes: TList;             // ������ ������� - TKBClass
    Initial: TInitial;          // ��������� ��������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: WideString; override;
    procedure Clear;            // ������� ����������
  end;
}

  ////////////////////////////////////////////////////////////////////////////
  // ����� TKBClass �������� ����������� ��� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TKBClass = class(TKBType)
  public
    AncestorID: WideString;     // ������ ������
    Properties: TList;          // �������� ������
    Rules: TList;               // ������� ������
    Methods: TList;             // ������ ������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
    function CreateInstance: TComponent;  virtual; // ������� ��������� ������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassProperty �������� ����������� ��� �������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassProperty = class(TKBItem)
  public
    ID: WideString;             // ��� ��������
    Desc: WideString;           // �������� ��������
    TypeRef: TKBType;           // ��� �������� (�� ������)
    Source: TPropertySource;    // ������ ��������� �������� ��������
    Question: WideString;       // ������ ��� ��������� �������� ��������
    Query: WideString;          // ������ � �� ��� ��������� �������� ��������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassRule �������� ����������� ��� ������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassRule = class(TKBItem)
  private
  public
    ID: WideString;             // ������������� �������
    Desc: WideString;           // �������� �������
    Confidence: TConfidence;    // ����������� � ���������� �������
    Condition: TValue;          // ������� ������� (���������� ���������)
    Action: TList;              // ������ ���������� - �������� TInstruction
    ElseAction: TList;          // ������ ���������� - �������� TInstruction
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassMethod �������� ����������� ��� ������ ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassMethod = class(TKBItem)
  public
    ID: WideString;             // ������������� ������
    Desc: WideString;           // �������� ������
    ResultTypeRef: TKBType;     // ��� ������������� �������� (�� ������)
    Params: TList;              // ������ ���������� ������ TParamDecl
    Locals: TList;              // ������ ��������� ���������� ������ TVarDecl
    Instructions: TList;        // ������ ���������� ������ - �������� TInstruction
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
  end;


implementation

uses KB_Values, OM_Containers;


/////////////////////////////////////////
// ������ ������ TKBItem
/////////////////////////////////////////

{function TKBItem.GetXML: WideString;
begin
  Result := '<kb-item />';      // ��������, ����������� ��������������
end;
}

/////////////////////////////////////////
// ������ ������ TAttrList
/////////////////////////////////////////

constructor TAttrList.Create{(AOwner: TComponent)};
begin
  inherited;
  Belief := 100;
  Probability := 100;
  Accuracy := 100;
end;

function TAttrList.GetAttr(Name: WideString): Variant;
begin
  if Name = sBelief then Result := Belief
  else if Name = sProbability then Result := Probability
  else if Name = sAccuracy then Result := Accuracy
end;

procedure TAttrList.SetAttr(Name: WideString; Value: Variant);
begin
  if VarIsNull(Value) then Value := '';

  if Name = sBelief then Belief := Value
  else if Name = sProbability then Probability := Value
  else if Name = sAccuracy then Accuracy := Value;
end;

function TAttrList.XML: WideString;
var s, a: WideString;
begin
  s := '';

  try
    a := Items[sBelief];
  except
    a := '';
  end;
  if a <> '' then s := s + sBelief + '="' + a + '" ';

  try
    a := Items[sProbability];
  except
    a := '';
  end;
  if a <> '' then s := s + sProbability + '="' + a + '" ';

  try
    a := Items[sAccuracy];
  except
    a := '';
  end;
  if a <> '' then s := s + sAccuracy + '="' + a + '" ';

  Result := '<' + sWith + ' ' + s + '/>';
end;


/////////////////////////////////////////
// ������ ������ TKBType
/////////////////////////////////////////

function TKBType.GetXML: {Wide}String;
begin
  Result := '<kb-type />';      // ��������, ����������� ��������������
end;

function TKBType.Valid(Value: TValue): boolean;
begin
  Result := True;               // ��������, ����������� �������������� � ��������
end;


/////////////////////////////////////////
// ������ ������ TValue
/////////////////////////////////////////

constructor TValue.Create(ACollection: TCollection);
begin
  inherited;
  Attrs := TAttrList.Create{(Self)};
  TypeRef := nil;
end;

destructor TValue.Destroy;
begin
  Attrs.Free;
  inherited;
end;

function TValue.GetXML: {Wide}String;
begin
  if Meta = vmLocal then
    Result := '<value>' + ResVal + '</value>'
  else if Meta = vmReference then begin
    Result := '<val-ref>' + '</val-ref>';
  end;
end;

procedure TValue.Evaluate;
begin
  if Meta = vmLocal then
    Evaluated := True;            // ��������, ����������� �������������� � ��������
end;

function TValue.EvalOp(Op: TOpMeta; Arg: TValue; var Res: TValue): boolean;
  function Min(a,b: double): double;
  begin
    if a > b then Result := b
    else Result := a;
  end;
  function Max(a,b: double): double;
  begin
    if a > b then Result := a
    else Result := b;
  end;

begin
  Result := False;
  if TypeMeta = tmNumeric then begin
    Res := TValue.Create(nil);
    Res.TypeMeta := tmNumeric;
    case Op of
    omAnd:
      begin
        Res.ResVal := min(Self.ResVal, Arg.ResVal);
        Res.Evaluated := True;
        Result := true;
      end;
    omOr:
      begin
        Res.ResVal := max(Self.ResVal, Arg.ResVal);
        Res.Evaluated := True;
        Result := true;
      end;
    omNot:
      begin
        Res.ResVal := 1-Self.ResVal;
        if Res.ResVal < 0 then Res.ResVal := 0;
        Res.Evaluated := True;
        Result := true;
      end;
    omXor:
      begin

      end;
{� & B = min(A, B), ��� A, B � [0; 1]
(2)		A | B = max(A, B) , ��� A, B � [0; 1]
(3)		~ A = 1 - A, ��� A � [0; 1]
(4)		A & ND = ND, ��� A � (0; 1]
(5)		A | ND = ND, ��� A � [0; 1)
(6)		ND & ND = ND
(7)		ND | ND = ND
(8)	~ ND = ND
(9)	0 & ND = 0
(10)	1 | ND = 1
}
    omGt:
      begin
        Res.ResVal := Self.ResVal > Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omLt:
      begin
        Res.ResVal := Self.ResVal < Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omEq:
      begin
        Res.ResVal := Self.ResVal = Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omNe:
      begin
        Res.ResVal := Self.ResVal <> Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omGe:
      begin
        Res.ResVal := Self.ResVal >= Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omLe:
      begin
        Res.ResVal := Self.ResVal <= Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;

    omMul:
      begin
        Res.ResVal := Self.ResVal * Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omSub:
      begin
        Res.ResVal := Self.ResVal - Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omDiv:
      begin
        if Arg.ResVal <> 0 then begin
          Res.ResVal := Self.ResVal / Arg.ResVal;
          Res.Evaluated := true;
          Result := true;
        end;
      end;
    omAdd:
      begin
        Res.ResVal := Self.ResVal + Arg.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    omMod:
      begin
        if Arg.ResVal <> 0 then begin
          Res.ResVal := Round(Self.ResVal) mod Round(Arg.ResVal);
          Res.Evaluated := true;
          Result := true;
        end;
      end;
    omNeg:
      begin
        Res.ResVal := -Self.ResVal;
        Res.Evaluated := true;
        Result := true;
      end;
    end;
  end;
end;

/////////////////////////////////////////
// ������ ������ TInitial
/////////////////////////////////////////

{constructor TInitial.Create(ACollection: TCollection);
begin
  inherited;
  Objects := TList.Create;
  Goals := TList.Create;
end;

destructor TInitial.Destroy;
begin
  Goals.Free;
  Objects.Free;
  inherited;
end;

function TInitial.GetXML: WideString;
var
  Doc, Child: IXMLDocument;
  i,j: integer;
  obj, prop: IXMLNode;
  Inst: TInstance;
  PV: TPropertyValue;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sInitialSituation+'/>');
  Doc.DocumentElement.AddChild(sObjects);

  for i := 0 to Objects.Count-1 do begin
    Inst := Objects[i];
    obj := Doc.DocumentElement.ChildNodes[sObjects].AddChild(sObject);
    obj.Attributes[sName] := Inst.Caption;
    obj.Attributes[sType] := Inst.ClassRef.ID;
    // ������� ��� �������� �������
    for j := 0 to Inst.PropertyList.Count-1 do begin
      PV := Inst.PropertyList[j];
      prop := obj.AddChild(sPropertyValue);
      prop.Attributes[sName] := PV.PropName;
      if PV.isGoal then prop.Attributes[sGoal] := sTrue;
      if PV.Value <> nil then begin
        Child.LoadFromXML(PV.Value.XML);
        prop.ChildNodes.Add(Child.DocumentElement.CloneNode(true));
      end;
    end;
  end;

  Result := Doc.DocumentElement.XML;
end;
}

/////////////////////////////////////////
// ������ ������ TKBContainer
/////////////////////////////////////////

{constructor TKBContainer.Create(ACollection: TCollection);
begin
  inherited;
  Types := TList.Create;
  Classes := TList.Create;
  Initial := TInitial.Create(Self);
end;

destructor TKBContainer.Destroy;
begin
  Types.Free;
  Classes.Free;
  Initial.Free;
  inherited;
end;

function TKBContainer.GetXML: WideString;
var
  Doc, Child: IXMLDocument;
  i: integer;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sKnowledgeBase+'/>');
  Doc.DocumentElement.Attributes[sCreationDate] := CreationDate;
  Doc.DocumentElement.AddChild(sProblemInfo);
  Doc.DocumentElement.ChildNodes[sProblemInfo].Text := ProblemName;

  Doc.DocumentElement.AddChild(sTypes);
  for i := 0 to Types.Count-1 do begin
    Child.LoadFromXML(TKBType(Types[i]).XML);
    Doc.DocumentElement.ChildNodes[sTypes].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Doc.DocumentElement.AddChild(sClasses);
  for i := 0 to Classes.Count-1 do begin
    Child.LoadFromXML(TKBClass(Classes[i]).XML);
    Doc.DocumentElement.ChildNodes[sClasses].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Child.LoadFromXML(Initial.XML);
  Doc.DocumentElement.ChildNodes.Add(Child.DocumentElement.CloneNode(true));

  Result := Doc.DocumentElement.XML;
end;

procedure TKBContainer.Clear;
begin
  Types.Clear;
  Classes.Clear;
end;
}
/////////////////////////////////////////
// ������ ������ TKBClass
/////////////////////////////////////////

constructor TKBClass.Create(ACollection: TCollection);
begin
  inherited;
  AncestorID := '';
  Desc := '';
  Properties := TList.Create;
  Rules := TList.Create;
  Methods := TList.Create;
end;

destructor TKBClass.Destroy;
begin
  Properties.Free;
  Rules.Free;
  Methods.Free;
  inherited;
end;

function TKBClass.GetXML: {Wide}String;
var
  Doc, Child: IXMLDocument;
  i: integer;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sClass+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sExt] := sFalse;

  if AncestorID <> '' then
    Doc.DocumentElement.Attributes[sAncestor] := AncestorID;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  Doc.DocumentElement.AddChild(sProperties);
  for i := 0 to Properties.Count-1 do begin
    Child.LoadFromXML(TClassProperty(Properties[i]).GetXML);
    Doc.DocumentElement.ChildNodes[sProperties].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Doc.DocumentElement.AddChild(sRules);
  for i := 0 to Rules.Count-1 do begin
    Child.LoadFromXML(TClassRule(Rules[i]).GetXML);
    Doc.DocumentElement.ChildNodes[sRules].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Doc.DocumentElement.AddChild(sMethods);
  for i := 0 to Methods.Count-1 do begin
    Child.LoadFromXML(TClassMethod(Methods[i]).GetXML);
    Doc.DocumentElement.ChildNodes[sMethods].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Result := Doc.DocumentElement.XML;
end;

// �������� ���������� ������
// (���������� ���������� TComponent, ����� �������� ������ ������� �����
function TKBClass.CreateInstance: TComponent;
var
  Instance: TInstance;
begin
  Instance := TInstance.Create(nil);
  Instance.ClassRef := Self;
  // �������� ����������, �������� � ���������
  // ���������� ������ ������� ��-�� ������� ����������:
  // � ������ ����������� ������� ���� � �� �� ������� �.�. ��-������� ����������


  Result := Instance;
end;

/////////////////////////////////////////
// ������ ������ TClassProperty
/////////////////////////////////////////

constructor TClassProperty.Create(ACollection: TCollection);
begin
  inherited;

end;

destructor TClassProperty.Destroy;
begin

  inherited;
end;

function TClassProperty.GetXML: {Wide}String;
var Doc: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sProperty+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;

  if TypeRef <> nil then
    Doc.DocumentElement.Attributes[sType] := (TypeRef as TKBType).ID;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  case Source of
    psQuestion: Doc.DocumentElement.Attributes[sSource] := 'question';
    psQuery:    Doc.DocumentElement.Attributes[sSource] := 'query';
    psInferred: Doc.DocumentElement.Attributes[sSource] := 'inferred';
    psSupplied: Doc.DocumentElement.Attributes[sSource] := 'supplied';
    psNone:     Doc.DocumentElement.Attributes[sSource] := 'none';
  end;

  if Query <> '' then begin
    Doc.DocumentElement.AddChild(sQuery);
    Doc.DocumentElement.ChildNodes[sQuery].Text := Query;
  end;

  if Question <> '' then begin
    Doc.DocumentElement.AddChild(sQuestion);
    Doc.DocumentElement.ChildNodes[sQuestion].Text := Question;
  end;

  Result := Doc.DocumentElement.XML;
end;

/////////////////////////////////////////
// ������ ������ TClassRule
/////////////////////////////////////////

constructor TClassRule.Create(ACollection: TCollection);
begin
  inherited;
  Action := TList.Create;
  ElseAction := TList.Create;
end;

destructor TClassRule.Destroy;
begin
  Action.Free;
  ElseAction.Free;
  inherited;
end;

function TClassRule.GetXML: {Wide}String;
var
  Doc, Child: IXMLDocument;
  i: integer;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sRule+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  Doc.DocumentElement.Attributes[sConfidence] := Confidence;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  Doc.DocumentElement.AddChild(sCondition);
  if Condition <> nil then begin
    Child.LoadFromXML(Condition.GetXML);
    Doc.DocumentElement.ChildNodes[sCondition].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Doc.DocumentElement.AddChild(sAction);
  for i:=0 to Action.Count-1 do begin
    Child.LoadFromXML(TKBItem(Action[i]).GetXML);
    Doc.DocumentElement.ChildNodes[sAction].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  if ElseAction.Count > 0 then begin
    Doc.DocumentElement.AddChild(sElseAction);
    for i:=0 to ElseAction.Count-1 do begin
      Child.LoadFromXML(TKBItem(ElseAction[i]).GetXML);
      Doc.DocumentElement.ChildNodes[sElseAction].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
    end;
  end;

  Result := Doc.DocumentElement.XML;
end;

/////////////////////////////////////////
// ������ ������ TClassMethod
/////////////////////////////////////////

constructor TClassMethod.Create(ACollection: TCollection);
begin
  inherited;
  Params := TList.Create;
  Locals := TList.Create;
  Instructions := TList.Create;
end;

destructor TClassMethod.Destroy;
begin
  Params.Free;
  Locals.Free;
  Instructions.Free;
  inherited;
end;

function TClassMethod.GetXML: {Wide}String;
var
  Doc, Child: IXMLDocument;
  i: integer;
begin
  Doc := TXMLDocument.Create(nil);
  Child := TXMLDocument.Create(nil);

  Doc.LoadFromXML('<'+sMethod+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  if ResultTypeRef <> nil then
    Doc.DocumentElement.Attributes[sResultType] := (ResultTypeRef as TKBType).ID;
  Doc.DocumentElement.Attributes[sCParams] := Params.Count;
  Doc.DocumentElement.Attributes[sDesc] := Desc;

  if Params.Count > 0 then begin
    Doc.DocumentElement.AddChild(sParams);
    for i:=0 to Params.Count-1 do begin
      Child.LoadFromXML(TKBItem(Params[i]).GetXML);
      Doc.DocumentElement.ChildNodes[sParams].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
    end;
  end;

  Doc.DocumentElement.AddChild(sStatements);
  for i:=0 to Instructions.Count-1 do begin
    Child.LoadFromXML(TKBItem(Instructions[i]).GetXML);
    Doc.DocumentElement.ChildNodes[sStatements].ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Result := Doc.DocumentElement.XML;
end;

end.
