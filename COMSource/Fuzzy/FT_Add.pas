//**************************************************************************//
//*                                                                        *//
//*  ������, ���������� �����-�� ����������� ��������� ��� ������ � ����-  *//
//*  ���� �������.                                                         *//
//*                                                                        *//
//*  �����: ������� �.�.                                                   *//
//*  ����������� � ���������: ������ �.�, ������� �.�.                                  *//
//*  ������: 2.0                                                           *//
//*  ���� ������ ����������: 16.01.2K                                      *//
//*                                                                        *//
//**************************************************************************//
unit FT_Add;

//----------------------------------------------------------------------------
interface

uses Classes, Aspr95, StdCtrls, Dialogs,
      LVstr, // ��������������� ����������
      KB_Fuzzy, // ������� ��������������
//     DR_Search,             // ������� ������ ��������������� �� ������� ������.
//     FT_Point,              // class TMFPoint.
//     FT_MembershipFunction, // class TMFOwner, class TMembershipFunction.
//     FT_LinguisticVariable, // class TMFList.
     FT_Dictionary;         // class TMFDictionary.
//     FT_FuzzyProcessor;     // class TFuzzyProcessor.

//*** ��������� ComboBox ������� ������� �������������� ***//
procedure AddMFList (curr_type : record_type; ComboBox : TComboBox);

//*** ��������� ����� ������� ��������������, ���� ��� ���������������� ***//
procedure AddMF (curr_type : record_type; ComboBox : TComboBox; Dictionary : TMFDictionaryComponent);

//*** ��������� ������� ������� ��������������, ��������� �������� ��� ***//
procedure AddMFFromType (cur_mf_value: TMembershipFunction; TypeName: String; Dictionary : TMFDictionaryComponent);

//*** ������������ ��� ��������, ���� ��� ���������� ***//
procedure CorrMFType (curr_type : record_type);

//*** ������ � Assertion �������������������� �������� ***//
procedure GetDefMF (Assertion : TAssertion);

//*** ������ � Assertion ������������������ �������� ***//
procedure GetFuzMF (Assertion : TAssertion);

//*** �������� ������������ ���� ��������� ***//
function TestMFAssertion (Ass1, Ass2 : TAssertion; KB : TKnowledgeBase) : Real;

//----------------------------------------------------------------------------
implementation
{$G+}
//*** ��������� ComboBox ������� ������� �������������� ***//
procedure AddMFList (curr_type : record_type; ComboBox : TComboBox);
var
  i : integer; // ������� ��� ����� For.
begin
  with ComboBox do begin                                                         // �������� � ComboBox...
    Items.Clear;                                                                 //   ������� ComboBox.
    if Assigned(curr_type.FLV) then
    for i:=0 to TLinguisticVariable(curr_type.FLV).mfList.Count-1 do begin                                //   ��� ���� ��������� ������...
      Items.Add(TMembershipFunction(TLinguisticVariable(curr_type.FLV).mfList.Items[i]).MFName);         //     ��������� � ComboBox ����� ���.
    end;
    //*** ����� ��������������� �� ������� ������ ***//
//    ComboBox.Text:=FindDeterminant (Value (curr_type.list_znach.Items[curr_type.list_znach.Count-1]).znach);
  end;
end;

//*** ��������� ����� ������� ��������������, ���� ��� ���������������� ***//
procedure AddMF (curr_type : record_type; ComboBox : TComboBox; Dictionary : TMFDictionaryComponent);
var
  Temp_LV  : TLinguisticVariable;  // ��������� ���������� ��� ������� � ����.
  Temp_MFOwner : TMembershipFunction; // ��������� ���������� ��� �������� ����� ��.
begin
//  if not Assigned(Dictionary) then Exit;
  if ComboBox.Text='' then Exit;
  Temp_LV:=TLinguisticVariable(curr_type.FLV);                                                // ������������ �� �������� ��.
  if (TLinguisticVariable(curr_type.FLV).FindMFbyName(ComboBox.Text)=nil) then begin                   // ���� �� �� ����� ����� � ������ �� � �������� �������, ��...
    Temp_MFOwner:=Temp_LV.Add;                                              //   ��������� ����� ������ ������� � ������.
    Temp_MFOwner.mfName:=ComboBox.Text;                                        //   ���������� ����� ���.
    if (Dictionary.FindMFbyName (Temp_LV.NameLV, Temp_MFOwner.mfName)<>Nil) then begin         //   ���� � ���������� ������� ���� �� � ����� �� ������, ��...
      Temp_MFOwner.Assign(TLinguisticVariable(curr_type.FLV).FindMFbyName (Temp_MFOwner.mfName)); //     ������ �������� ��.
    end;
  end;
end;

//*** ��������� ������� ������� ��������������, ��������� �������� ��� ***//
procedure AddMFFromType (cur_mf_value: TMembershipFunction; TypeName: String; Dictionary : TMFDictionaryComponent);
var
  S: String;
  PtrMF: TMembershipFunction;
begin
  S:=cur_mf_value.MFName;
  PtrMF:=Dictionary.FMFTypes.FindMFbyName(TypeName);
  if PtrMF<>NIL then begin
    cur_mf_value.Assign(PtrMF);
    if S<>'' then
      cur_mf_value.MFName:=S;
  end;
end;

procedure CorrMFType (curr_type : record_type);
var
  i           : integer;
  Temp_MFList : TList;
  Temp_MF     : TMembershipFunction;
begin
  //*** ��� �������� ������������� ������ �� � �� ���������� ***//
{  Temp_MFList:=curr_type.MFList;
  for i:=0 to Temp_MFList.Count-1 do begin
    Temp_MF:=TMFOwner (Temp_MFList.Items[i]).MembershipFunction;
    if (Temp_MF.Count=0) then begin
      //*** ������� ������ �� - ���������� ������� ***//
    end;
  end;}
end;

procedure GetDefMF (Assertion : TAssertion);
var
  i            : integer;
  n_type       : integer;
  MF_Name      : string;
  Temp_LV       : TLinguisticVariable;
  Temp_MFOwner : TMembershipFunction;
  curr_attr    : record_attr;
  curr_type    : record_type;
begin
  MF_Name:=Assertion.Fznach;
  curr_attr:=record_attr (Assertion.Ptr_attr);
  if not Assigned(curr_attr) then begin
    Exit;
  end;
  if not Assigned(curr_attr.PtrType) then begin
    Exit;
  end;
  n_type:=curr_attr.n_type;
  curr_type:=curr_attr.PtrType;
  if (curr_type.n_type<>n_type) then begin
    ShowMessage ('ERROR');
    Exit;
  end;
  if Assigned(curr_type.FLV) then begin
    Temp_LV:=TLinguisticVariable(curr_type.FLV);
    if (Temp_LV.FindMFbyName(MF_Name)<>Nil) then begin
      Temp_MFOwner:=Temp_LV.FindMFbyName (MF_Name);
      Assertion.Fznach:=Temp_LV.GetDefuzzifiedString(Temp_MFOwner.Defuzzified);
    end else begin
    // ��������� �������������� ��������.
    end;
  end;  
end;

procedure GetFuzMF (Assertion : TAssertion);
var
  MF_Name      : string;
begin
  MF_Name:=Assertion.fznach;
end;

function TestMFAssertion (Ass1, Ass2 : TAssertion; KB : TKnowledgeBase) : real;
var
  Name_1    : string;
  Name_2    : string;
  LV        : TLinguisticVariable;
  MFOwner   : TMembershipFunction;
  Operand_1 : TMembershipFunction;
  Operand_2 : TMembershipFunction;
//  Fuzzy     : TFuzzyProcessor;
  Res: TMembershipFunction;
  output    : real;
begin
  Name_1:=Ass1.Fznach;
  Name_2:=Ass2.Fznach;
  if ((Ass1.Ptr_attr=Nil) or (Ass2.Ptr_attr=Nil)) then begin
    TestMFAssertion:=0;
    Exit;
  end;
  if (record_attr (Ass1.Ptr_attr).n_type<>record_attr (Ass2.Ptr_attr).n_type) then begin
    TestMFAssertion:=0;
    Exit;
  end;
  LV:=TLinguisticVariable(record_attr (Ass1.Ptr_attr).PtrType.FLV);
  if (LV.FindMFbyName (Name_1)<>Nil) then begin
    MFOwner:=LV.FindMFbyName (Name_1);
    Operand_1:=MFOwner;
    LV:=TLinguisticVariable(record_attr (Ass2.Ptr_attr).PtrType.FLV);
    if (LV.FindMFbyName (Name_2)<>Nil) then begin
      MFOwner:=LV.FindMFbyName (Name_2);
      Operand_2:=MFOwner;

      {Fuzzy:=TFuzzyProcessor.Create (Nil);
      Fuzzy.Operand_1:=Operand_1;
      Fuzzy.Operand_2:=Operand_2;
      Fuzzy.Activate (foConjunction);
      output:=Fuzzy.Result.FindMaximum (0, Fuzzy.Result.Count-1);
      Fuzzy.Free;}

      Res:=Operand_1.Conjunction(Operand_2);
      output:=Res.MaxLevel;
      Res.Destroy;
    end
    else begin
      // ��������� �������������� ��������.
    end;
  end
  else begin
    // ��������� �������������� ��������.
  end;
  TestMFAssertion:=output;
end;

//----------------------------------------------------------------------------
end.
