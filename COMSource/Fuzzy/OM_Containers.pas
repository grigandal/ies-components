unit OM_Containers;

 {******************************************************************************
 * ������ OM_Containers.pas �������� ����������� ��������� �������:
 *
 * TOMContainer - ��������� ������� ������
 * TInstance - ��������� ���������� ������ ���� ������
 * TGoal - ��������� ����
 * TStatement - ��������� �������� �����������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 2 ������, 2003 >>
 ******************************************************************************}

interface

uses
  Classes,
  KB_Containers, KB_Values;

type                  

  ////////////////////////////////////////////////////////////////////////////
  // ����� TOMContainer �������� ����������� ������� ������
  ////////////////////////////////////////////////////////////////////////////

  TOMContainer = class(TComponent)
  public
    Objects: TList;
    Goals: TList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;            // ������� ����������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TInstance �������� ����������� ��� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TInstance = class(TComponent)
  public
    Caption: WideString;
    ClassRef: TKBType;
    PropertyList: TList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  private
    procedure SetProperty(Name: WideString; Value: TValue);
    function GetProperty(Name: WideString): TValue;
  public
    property Properties[Name: WideString]: TValue read GetProperty write SetProperty;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TGoal �������� ����������� ��� �������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TGoal = class(TComponent)
  public
    GoalRef: TReference;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TStatement �������� ����������� ��� �������� �����������
  ////////////////////////////////////////////////////////////////////////////

  TStatement = class(TGoal)
  public
    ValueRef: TValue;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TPropertyValue �������� ����������� ��� �������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TPropertyValue = class(TComponent)
  public
    PropName: WideString;
    Value: TValue;
    isGoal: boolean;
  end;


implementation


/////////////////////////////////////////
// ������ ������ TOMContainer
/////////////////////////////////////////

constructor TOMContainer.Create(AOwner: TComponent);
begin
  inherited;
  Objects := TList.Create;
  Goals := TList.Create;
end;

destructor TOMContainer.Destroy;
begin
  Goals.Free;
  Objects.Free;
  inherited;
end;

procedure TOMContainer.Clear;
begin
  Goals.Clear;
  Objects.Clear;
end;


////////////////////////////////////////
// ������ ������ TInstance
/////////////////////////////////////////

constructor TInstance.Create(AOwner: TComponent);
begin
  inherited;
  PropertyList := TList.Create;
end;

destructor TInstance.Destroy;
begin
  PropertyList.Free;
  inherited;
end;

procedure TInstance.SetProperty(Name: WideString; Value: TValue);
var
  i: integer;
  found: boolean;
  pv: TPropertyValue;
begin
  // ��������� ����� �������� � �������� ��� ��������
  found := false;
  i := 0;
  while i < PropertyList.Count do begin
    if TPropertyValue(PropertyList[i]).PropName = Name then begin
      TPropertyValue(PropertyList[i]).Value.Free;
      TPropertyValue(PropertyList[i]).Value := Value;
      found := true;
      break;
    end;
    Inc(i);
  end;

  // ���� �� ����� �� ��������
  if not found then begin
    pv := TPropertyValue.Create(self);
    pv.PropName := Name;
    pv.Value := Value;
    PropertyList.Add(pv);
  end;
end;

function TInstance.GetProperty(Name: WideString): TValue;
var i: integer;
begin
  Result := nil; // ���� �� ������
  i := 0;
  while i < PropertyList.Count do begin
    if TPropertyValue(PropertyList[i]).PropName = Name then begin
      Result := TPropertyValue(PropertyList[i]).Value;
      break;
    end;
    Inc(i);
  end;
end;


////////////////////////////////////////
// ������ ������ TGoal
/////////////////////////////////////////


////////////////////////////////////////
// ������ ������ TStatement
/////////////////////////////////////////


////////////////////////////////////////
// ������ ������ TPropertyValue
/////////////////////////////////////////

end.
