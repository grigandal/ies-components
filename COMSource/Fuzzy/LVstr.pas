unit LVstr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, KB_Fuzzy, Aspr95,
  MSXML_TLB, KBCompon;

type
  TLinguisticVariable = class(TKBItem)
  private

  public

    NameLV   : String; // ��� ��
    dMin   : Integer;  // ����������� ��������
    dMax   : Integer;  // ������������ ��������
    Dimension  : String; // ������� ���������
    mfList : TList;    // ������ ������� ��������������
    DimControl: String; //�������� ����� ��� ���������� ������
    MinControl: String; //�������� ����� ��� ���������� ������
    MaxControl: String; //�������� ����� ��� ���������� ������
    procedure Load (XmlFileName : String); overload;
    procedure Load (X: MSXML_TLB.IXMLDOMDocument); overload;
    procedure Save (XmlFileName : String);  overload;
    procedure Save (var F: Text);  overload;
    function  GetXML: String; override;
    function  Add: TMembershipFunction;
    procedure Clear;
    procedure LoadXML(XML: String); overload;
    constructor Create(ACollection: TCollection); override;
    constructor Load(var F: Text; ACollection: TCollection); overload;
    constructor LoadXML(XML: String; ACollection: TCollection ); overload; override;
    destructor Destroy; override;
    function FindMFbyName (iName : string) : TMembershipFunction; // ����� � ������� �� �� �����.
    function GetDefuzzifiedString (iValue : real) : string; // ���������� ��� ��, �������� ����� ��������������� ��������.
    procedure Assign(KBItem: TKBItem); override;
    procedure UpdateMinMax;
  {published
    property NameLV: String read FNameLV write FNameLV; // ��� ��
    property dMin   : Integer read FdMin write FdMin;  // ����������� ��������
    property dMax   : Integer read FdMax write FdMax;  // ������������ ��������
    property Dimension  : String read FDimension write FDimension; // ������� ���������
    property mfList : TList read FmfList write FmfList;    // ������ ������� ��������������
    property DimControl: String read FDimControl write FDimControl; //�������� ����� ��� ���������� ������
    property MinControl: String read FMinControl write FMinControl; //�������� ����� ��� ���������� ������
    property MaxControl: String read FMaxControl write FMaxControl; //�������� ����� ��� ���������� ������
    }
  end;

  TMFTypes = class(TLinguisticVariable);

implementation

constructor TLinguisticVariable.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  mfList := TList.Create;
end;

procedure TLinguisticVariable.Clear;
var
  Obj: TObject;
begin
  while mfList.Count>0 do begin
    Obj:=mfList.Items[mfList.Count-1];
    mfList.Delete(mfList.Count-1);
    Obj.Destroy;
  end;
end;

function TLinguisticVariable.Add: TMembershipFunction;
var
  Obj: TMembershipFunction;
begin
  Obj:=TMembershipFunction.Create(nil);
  mfList.Add(Obj);
  Result:=Obj;
end;

destructor TLinguisticVariable.Destroy;
begin
  if mfList.Count>0 then Clear;
  mfList.Destroy;
  inherited Destroy;
end;

procedure TLinguisticVariable.Load(X: MSXML_TLB.IXMLDOMDocument);
var
  NodeLV, NodeMF, NodePoint : MSXML_TLB.IXMLDOMNode;
  ElementAttributes: MSXML_TLB.IXMLDOMNamedNodeMap;
  j,k : Integer;
  NumOfMFs, NumOfPoints : Integer;
  MF : TMembershipFunction;
  FP : TFuzzyPoint;
begin
  Clear;
  NodeLV:=X.documentElement; // root ��������� �� ������ xml �����
  ElementAttributes := NodeLV.attributes;
  NameLV := ElementAttributes.getNamedItem('Name').text;
  dMin := StrToInt(ElementAttributes.getNamedItem('dMin').text);
  dMax := StrToInt(ElementAttributes.getNamedItem('dMax').text);
  Dimension := ElementAttributes.getNamedItem('Dimension').text;
  if ElementAttributes.getNamedItem('DimControl')<>NIL then
    DimControl := ElementAttributes.getNamedItem('DimControl').text;
  if ElementAttributes.getNamedItem('MinControl')<>NIL then
    MinControl := ElementAttributes.getNamedItem('MinControl').text;
  if ElementAttributes.getNamedItem('MaxControl')<>NIL then
    DimControl := ElementAttributes.getNamedItem('MaxControl').text;

  NumOfMFs := NodeLV.childNodes.length; //���������� ����� �������
  for j:=0 to NumofMFs -1 do
  begin
    MF:=TMembershipFunction.Create(nil);
    NodeMF := NodeLV.childNodes[j];
    ElementAttributes := NodeMF.attributes;
    MF.MFName := ElementAttributes.getNamedItem('Name').text;
    NumOfPoints:=NodeMF.childNodes.length;
    for k:=0 to NumOfPoints - 1 do
    begin
      FP:=TFuzzyPoint.Create(nil);
      NodePoint:=NodeMF.ChildNodes[k];
      ElementAttributes := NodePoint.attributes;
      FP.X:=StrToFloat(ElementAttributes.getNamedItem('X').text);
      FP.Y:=StrToFloat(ElementAttributes.getNamedItem('Y').Text);
      MF.Points.Add(FP);
    end;
    mfList.Add(MF);
  end;
end;


procedure TLinguisticVariable.Load(XmlFileName:string);
var
  X: MSXML_TLB.IXMLDOMDocument;
begin
  X:=CoDOMDocument.Create;
  X.Load(XmlFileName);//�������� xml �����
  Load(X);
end;

procedure TLinguisticVariable.Save(XmlFileName:string);
var
  X: MSXML_TLB.IXMLDOMDocument;
begin
  X:=CoDOMDocument.Create;
  X.LoadXML(GetXML);
  X.Save(XmlFileName);
end;

function TLinguisticVariable.GetXML: String;
var
  X: MSXML_TLB.IXMLDOMDocument;
  NodeLV, NodeMF, NodePoint : MSXML_TLB.IXMLDOMNode;
  AttributeNode: IXMLDOMNode;
  j,k : Integer;
  NumOfMFs, NumOfPoints : Integer;
begin
  X:=CoDOMDocument.Create;
  NodeLV := X.createElement(Self.ClassName);

  AttributeNode:=X.createAttribute('Name');
  AttributeNode.nodeValue:=NameLV;
  NodeLV.attributes.setNamedItem(AttributeNode); // SetAttribute('Name',NameLV); // ������� ��������

  AttributeNode:=X.createAttribute('dMin');
  AttributeNode.nodeValue:=dMin;
  NodeLV.attributes.setNamedItem(AttributeNode); // NodeLV.SetAttribute('dMin',dMin);

  AttributeNode:=X.createAttribute('dMax');
  AttributeNode.nodeValue:=dMax;
  NodeLV.attributes.setNamedItem(AttributeNode); //NodeLV.SetAttribute('dMax',dMax);

  AttributeNode:=X.createAttribute('Dimension');
  AttributeNode.nodeValue:=Dimension;
  NodeLV.attributes.setNamedItem(AttributeNode);

  AttributeNode:=X.createAttribute('DimControl');
  AttributeNode.nodeValue:=DimControl;
  NodeLV.attributes.setNamedItem(AttributeNode);

  AttributeNode:=X.createAttribute('MinControl');
  AttributeNode.nodeValue:=MinControl;
  NodeLV.attributes.setNamedItem(AttributeNode);

  AttributeNode:=X.createAttribute('MaxControl');
  AttributeNode.nodeValue:=MaxControl;
  NodeLV.attributes.setNamedItem(AttributeNode);

  NumOfMFs:=mfList.Count;
  for j:=0 to NumOfMFs-1 do //���� �� ���� �������� �������.
  begin
    NodeMF:=X.createElement('TMembershipFunction');
    NodeLV.appendChild(NodeMF);

    AttributeNode:=X.createAttribute('Name');
    AttributeNode.nodeValue:=TMembershipFunction(mfList.Items[j]).MFName;
    NodeMF.attributes.setNamedItem(AttributeNode);
    //NodeMF.SetAttribute('Name',TMembershipFunction(mfList.Items[j]).MFName);

    NumOfPoints:=TMembershipFunction(mfList.Items[j]).Points.Count;
    for k:=0 to NumOfPoints-1 do begin
      NodePoint:=X.createElement('FPoint');
      NodeMF.appendChild(NodePoint);

      AttributeNode:=X.createAttribute('X');
      AttributeNode.nodeValue:=
        FloatToStr(TFuzzyPoint(TMembershipFunction(mfList.Items[j]).Points.Items[k]).X);
      NodePoint.attributes.setNamedItem(AttributeNode);
      //NodePoint.SetAttribute('X',TFuzzyPoint(TMembershipFunction(mfList.Items[j]).Points.Items[k]).X);

      AttributeNode:=X.createAttribute('Y');
      AttributeNode.nodeValue:=
        FloatToStr(TFuzzyPoint(TMembershipFunction(mfList.Items[j]).Points.Items[k]).Y);
      NodePoint.attributes.setNamedItem(AttributeNode);
      //NodePoint.SetAttribute('Y,TFuzzyPoint(TMembershipFunction(mfList.Items[j]).Points.Items[k]).Y);
    end;
  end;
  X.appendChild(NodeLV);
  GetXML:=X.XML;
end;

constructor TLinguisticVariable.LoadXML(XML: String; ACollection: TCollection );
begin
  Self:=TLinguisticVariable(ACollection.Add);
  Self.LoadXML(XML);
end;

procedure TLinguisticVariable.LoadXML(XML: String);
var
  X: MSXML_TLB.IXMLDOMDocument;
begin
  X:=CoDOMDocument.Create;
  X.loadXML(XML);
  Load(X);
end;

function TLinguisticVariable.FindMFbyName (iName : string) : TMembershipFunction;
var
  i           : integer;      // ������� ��� ����� For.
  j           : integer;      // ������� ��� ����� For.
  Temp_MF     : TMembershipFunction;     // ��������� ���������� ��� ������� � ���-����������.
begin
 with mfList do begin
  if (Count<>0) then begin                                         // ���� � ��� ���� ��������������� ����������, ��...
    for j:=0 to Count-1 do begin                                   //       ��� ���� ����-��������...
      Temp_MF:=TMembershipFunction(Items[j]);                      //         ������� ��������� ��.
      if (Temp_MF.MFName=iName) then begin                         //         ���� �� ��� ����� ���������, ��...
        FindMFbyName:=Temp_MF;                                     //           ���������� ��������� �� ��� ��.
        Exit;                                                      //           ����������� ������.
      end;
    end;
  end;
 end;
 FindMFbyName:=nil;                                                // ���� ������ �� �����, �� ���������� ������ ���������.
end;

function TLinguisticVariable.GetDefuzzifiedString (iValue : real) : string;
var
  i            : integer;  // ������� ��� ����� For.
  local_max    : real;     // ��������� �������� ����� �� �� �������� iValue.
  max_index    : integer;  // ������ ������������ ��.
  Temp_MFOwner : TMembershipFunction; // ��������� ���������� ��� ������� � ��������� ������.
begin
  local_max:=0;                                                                   // �������������� ��������� �������� �����.
  max_index:=0;                                                                   // �������������� ������ ������������ �� ����� - ��������, �� ������ �� ��������.
  for i:=0 to mfList.Count-1 do begin                                                    // ��� ���� ��...
    Temp_MFOwner:=mfList[i];                                                     //   ������� ��������� ��.
    if (Temp_MFOwner.MFValueAt (iValue)>local_max) then begin //   ���� �� �������� �� iValue ������ �������� ���������, ��...
      local_max:=Temp_MFOwner.MFValueAt (iValue);             //     ���������� ����� ��������.
      max_index:=i;                                                               //     ���������� ������ ��.
    end;
  end;
  Temp_MFOwner:=mfList[max_index];                                     // ������� "������������" ��.
  GetDefuzzifiedString:=Temp_MFOwner.mfName;                                     // ���������� �� ���.
end;

constructor TLinguisticVariable.Load(var F: Text; ACollection: TCollection);
var
  PtrMF: TMembershipFunction;
begin
  Self:=TLinguisticVariable(ACollection.Add);
end;

procedure TLinguisticVariable.Save (var F: Text);
begin
end;

procedure TLinguisticVariable.Assign(KBItem: TKBItem);
var
  i: Integer;
  MembershipFunction: TMembershipFunction;
begin
  inherited Assign(KBItem);
  if KBItem is TLinguisticVariable then begin
    NameLV       :=  TLinguisticVariable(KBItem).NameLV;
    dMin         :=  TLinguisticVariable(KBItem).dMin;
    dMax         :=  TLinguisticVariable(KBItem).dMax;
    Dimension    :=  TLinguisticVariable(KBItem).Dimension;
    mfList.Clear;
    for i:=1 to TLinguisticVariable(KBItem).mfList.Count do begin
      MembershipFunction:=Add;
      MembershipFunction.Assign(
        TMembershipFunction(TLinguisticVariable(KBItem).mfList.Items[i-1]));
    end;
  end;
end;

procedure TLinguisticVariable.UpdateMinMax;
var
  i: Integer;
begin
  for i:=1 to mfList.Count do begin
    if dMin>Trunc(TMembershipFunction(mfList.Items[i-1]).XMin) then
      dMin:=Trunc(TMembershipFunction(mfList.Items[i-1]).XMin);
    if Trunc(TMembershipFunction(mfList.Items[i-1]).XMax)<
      TMembershipFunction(mfList.Items[i-1]).XMax then begin
        if dMax<Trunc(TMembershipFunction(mfList.Items[i-1]).XMax)+1 then
          dMax:=Trunc(TMembershipFunction(mfList.Items[i-1]).XMax)+1;
    end else begin
      if dMax<Trunc(TMembershipFunction(mfList.Items[i-1]).XMax) then
         dMax:=Trunc(TMembershipFunction(mfList.Items[i-1]).XMax);
    end;
  end;
end;

initialization
  RegisterClasses([TLinguisticVariable,TMFTypes]);

end.
