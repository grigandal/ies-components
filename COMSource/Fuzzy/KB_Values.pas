unit KB_Values;

 {******************************************************************************
 * ������ KB_Values.pas �������� ����������� ����������� ������ TValue:
 *
 * TReference - ������ �� ��������
 * TOperation - �������� ��� ���������� (������� �����)
 * TMulOp - �������� ��������������� ���������
 * TDivOp - �������� ��������������� �������
 * TModOp - �������� ������ ������� �� �������
 * TSubOp - �������� ��������������� ���������
 * TAddOp - �������� ��������������� ��������
 * TNegOp - �������� ��������������� ���������
 * TAndOp - �������� ����������� �
 * TOrOp - �������� ����������� ���
 * TNotOp - �������� ����������� ��
 * TXorOp - �������� ����������� ��-�����
 * TGtOp - �������� ��������� "������"
 * TLtOp - �������� ��������� "������"
 * TEqOp - �������� ��������� "���������"
 * TNeOp - �������� ��������� "�����������"
 * TGeOp - �������� ��������� "������ ��� �����"
 * TLeOp - �������� ��������� "������ ��� �����"
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf,
  XML_Consts, KB_Containers, KB_Types;

type

  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TRefMeta ��������� ������� ������, ���������� � TReference
  ////////////////////////////////////////////////////////////////////////////

  TRefMeta = (
    rmProperty,
    rmMethod
  );


  ////////////////////////////////////////////////////////////////////////////
  // ����� TReference �������� ������� �� �����-���� ��������
  ////////////////////////////////////////////////////////////////////////////

  TReference = class(TValue)
  public
    RefMeta: TRefMeta;  // ������� ������
    ID: WideString;     // ��� ��������, �� ������� ���������
    Params: TList;      // ���������, ���� ��������� �� ����� ��� ��������-������
    Ref: TReference;    // ������ ���������� ������, �������� ������
    LValue: boolean;    // �������� �� �������� �� ������ ����������, ������� ����� ��������� ��������
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
    procedure Evaluate; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TOperation �������� ���������, ������������ ��� ����������
  ////////////////////////////////////////////////////////////////////////////

  TOperation = class(TValue)
  public
    OpMeta: TOpMeta;
    Operand1: TValue;
    Operand2: TValue;
    OpResult: TValue;   // ���� ������ ���������
    isBinary: boolean;
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
    function GetXML: {Wide}String; override;
    procedure Evaluate; override;
    function ValidateOperandsTypes: boolean;
  published
    property EvaluationPossible: boolean read ValidateOperandsTypes;
  end;

  ////////////////////////////////////////////////////////////////////////////
  // �������������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TMulOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TDivOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TModOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TSubOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TAddOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TNegOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  ////////////////////////////////////////////////////////////////////////////
  // ���������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TAndOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TOrOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TXorOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TNotOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  ////////////////////////////////////////////////////////////////////////////
  // �������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TGtOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TLtOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TEqOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TNeOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TGeOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;

  TLeOp = class(TOperation)
  public
//    constructor Create(AOwner: TComponent); override;
    constructor Create(ACollection: TCollection); override;
    procedure Evaluate; override;
  end;


implementation


/////////////////////////////////////////
// ������ ������ TValRef
/////////////////////////////////////////

constructor TReference.Create(ACollection: TCollection);
begin
  inherited;
  ID := '';
  LValue := true;
  Ref := nil;
  Params := nil;
end;

destructor TReference.Destroy;
begin
  Params.Free;
  Ref.Free;
  inherited;
end;

function TReference.GetXML: {Wide}String;
var Doc, Child: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<'+sValRef+'/>');
  Doc.DocumentElement.Attributes[sID] := ID;
  if LValue then
    Doc.DocumentElement.Attributes[sLValue] := sTrue
  else
    Doc.DocumentElement.Attributes[sLValue] := sFalse;
  Doc.DocumentElement.Attributes[sMeta] := sProperty;

  if Ref <> nil then begin
    Child := TXMLDocument.Create(nil);
    Child.LoadFromXML(Ref.GetXML);
    Doc.DocumentElement.ChildNodes.Add(Child.DocumentElement.CloneNode(true));
  end;

  Result := Doc.DocumentElement.XML;
end;

procedure TReference.Evaluate;
begin
  // �������� ������ �� �������� � ��������� �� Result
end;



/////////////////////////////////////////
// ������ ������ TOperation
/////////////////////////////////////////

constructor TOperation.Create(ACollection: TCollection);
begin
  inherited;
  Operand1 := NIL;
  Operand2 := NIL;
  isBinary := True;
end;

destructor TOperation.Destroy;
begin
  Operand1.Free;
  Operand2.Free;
  inherited;
end;

function TOperation.GetXML: {Wide}String;
var ops, s: WideString;
begin
  // �������� ��������
  ops := Operand1.GetXML;
  if isBinary then ops := ops + Operand2.GetXML;

  // �������� ��������
  s := Attrs.XML;
  if s = '<' + sWith + ' />' then s := '';

  // ������ ���������
  case OpMeta of
  omAdd: Result := '<add>'+ops+ s+'</add>';
  omDiv: Result := '<div>'+ops+ s+'</div>';
  omSub: Result := '<sub>'+ops+ s+'</sub>';
  omMul: Result := '<mul>'+ops+ s+'</mul>';
  omMod: Result := '<mod>'+ops+ s+'</mod>';
  omAnd: Result := '<and>'+ops+ s+'</and>';
  omNot: Result := '<not>'+ops+ s+'</not>';
  omXor: Result := '<xor>'+ops+ s+'</xor>';
  omOr:  Result := '<or>'+ops+ s+'</or>';
  omLt:  Result := '<lt>'+ops+ s+'</lt>';
  omGt:  Result := '<gt>'+ops+ s+'</gt>';
  omEq:  Result := '<eq>'+ops+ s+'</eq>';
  omNe:  Result := '<ne>'+ops+ s+'</ne>';
  omGe:  Result := '<ge>'+ops+ s+'</ge>';
  omLe:  Result := '<le>'+ops+ s+'</le>';
  end;

end;

function TOperation.ValidateOperandsTypes: boolean;
begin
  Result := False;

end;

procedure TOperation.Evaluate;
var
  FactOp1, FactOp2: TValue;
begin
  // ���� ��������, �������� �� ���������� �������� ��������
  // � �������� ����, ���� ����
  // ������ ����� ���������� EvalOp � ���������

  if not Evaluated and EvaluationPossible then begin
    if Operand1.Meta = vmOperation then
      FactOp1 := (Operand1 as TOperation).OpResult
    else
      FactOp1 := Operand1;
      
    if Operand2.Meta = vmOperation then
      FactOp2 := (Operand2 as TOperation).OpResult
    else
      FactOp2 := Operand2;

    Evaluated := FactOp1.EvalOp(OpMeta, FactOp2, OpResult)
  end;
end;



/////////////////////////////////////////
// ������ �������������� ��������
/////////////////////////////////////////


// ������ ������ TMulOp

constructor TMulOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omMul;
end;

procedure TMulOp.Evaluate;
begin
  ResVal := Operand1.ResVal * Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TDivOp

constructor TDivOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omDiv;
end;

procedure TDivOp.Evaluate;
begin
  ResVal := Operand1.ResVal / Operand2.ResVal;
  if (TypeMeta = tmNumeric) then
  Evaluated := True;
end;


// ������ ������ TModOp

constructor TModOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omMod;
end;

procedure TModOp.Evaluate;
begin
  ResVal := Round(Operand1.ResVal) mod Round(Operand2.ResVal);
  Evaluated := True;
end;


// ������ ������ TAddOp

constructor TAddOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omAdd;
end;

procedure TAddOp.Evaluate;
begin
  ResVal := Operand1.ResVal + Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TSubOp

constructor TSubOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omSub;
end;

procedure TSubOp.Evaluate;
var Res: TValue;
begin
  if Operand1.TypeMeta = tmFuzzy then Operand1.EvalOp(omSub, Operand2, Res);

  Evaluated := True;
end;


// ������ ������ TNegOp

constructor TNegOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omNeg;
  isBinary := False;
end;

procedure TNegOp.Evaluate;
begin
  ResVal := - Operand1.ResVal;
  Evaluated := True;
end;


/////////////////////////////////////////
// ������ ���������� ��������
/////////////////////////////////////////


// ������ ������ TAndOp

constructor TAndOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omAnd;
end;

procedure TAndOp.Evaluate;
begin
  ResVal := Operand1.ResVal and Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TOrOp

constructor TOrOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omOr;
end;

procedure TOrOp.Evaluate;
begin
  ResVal := Operand1.ResVal or Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TNotOp

constructor TNotOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omNot;
  isBinary := False;
end;

procedure TNotOp.Evaluate;
begin
  ResVal := not Operand1.ResVal;
  Evaluated := True;
end;


// ������ ������ TXorOp

constructor TXorOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omXor;
end;

procedure TXorOp.Evaluate;
begin
  ResVal := Operand1.ResVal xor Operand2.ResVal;
  Evaluated := True;
end;


/////////////////////////////////////////
// ������ �������� ���������
/////////////////////////////////////////


// ������ ������ TGtOp

constructor TGtOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omGt;
end;

procedure TGtOp.Evaluate;
begin
  ResVal := Operand1.ResVal > Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TLtOp

constructor TLtOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omLt;
end;

procedure TLtOp.Evaluate;
begin
  ResVal := Operand1.ResVal < Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TEqOp

constructor TEqOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omEq;
end;

procedure TEqOp.Evaluate;
begin
  ResVal := Operand1.ResVal = Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TNeOp

constructor TNeOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omNe;
  isBinary := False;
end;

procedure TNeOp.Evaluate;
begin
  ResVal := Operand1.ResVal <> Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TGeOp

constructor TGeOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omGe;
end;

procedure TGeOp.Evaluate;
begin
  ResVal := Operand1.ResVal >= Operand2.ResVal;
  Evaluated := True;
end;


// ������ ������ TLeOp

constructor TLeOp.Create(ACollection: TCollection);
begin
  inherited;
  OpMeta := omLe;
end;

procedure TLeOp.Evaluate;
begin
  ResVal := Operand1.ResVal <= Operand2.ResVal;
  Evaluated := True;
end;

end.
