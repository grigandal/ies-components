unit UMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, AppEvnts;

type
  TMainForm = class(TForm)
    ProgressBar: TProgressBar;
    Timer: TTimer;
    ApplicationEvents1: TApplicationEvents;
    Memo1: TMemo;
    procedure TimerTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Activate(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure ApplicationEvents1Minimize(Sender: TObject);
    procedure ApplicationEvents1Restore(Sender: TObject);
  private
    { Private declarations }
    ShallClose: boolean;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.TimerTimer(Sender: TObject);
begin
  ProgressBar.StepIt;

  // ����-�������
  {if (ProgressBar.Position >= ProgressBar.Max)
  or (ProgressBar.Position <= ProgressBar.Min)
  then
    ProgressBar.Step := - ProgressBar.Step;}

  // �� �������
  if (ProgressBar.Position >= ProgressBar.Max)
  then
    ProgressBar.Position := ProgressBar.Min;

  Application.ProcessMessages;
  if ShallClose then Close;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  //CanClose := ShallClose;
end;

procedure TMainForm.ApplicationEvents1Activate(Sender: TObject);
begin
  Memo1.Lines.Add(IntToHex(Application.Handle, 8) + ' activate');
  PostMessage(Application.Handle, WM_SHOWWINDOW, $FFFF, SW_OTHERUNZOOM);
end;

procedure TMainForm.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
  if Application.Handle <> Msg.hwnd then exit;

  //$B000, $B001 - show hide

  if  (Msg.Message <> $000F)  // WM_PAINT
  //and (Msg.Message <> $00A0)  // WM_NCMOUSEMOVE
  //and (Msg.Message <> $0112)  // WM_SYSCOMMAND
  and (Msg.Message <> $0113)  // WM_TIMER
  //and (Msg.Message <> $0118)  // WM_SHOWWINDOW

  {and (Msg.Message <> $0100)  // WM_KEYDOWN
  and (Msg.Message <> $0101)  // WM_KEYUP
  and (Msg.Message <> $0102)  // WM_CHAR
  and (Msg.Message <> $0104)  // WM_SYSKEYDOWN
  }
  and (Msg.Message <> $0200)  // WM_MOUSEMOVE
  {and (Msg.Message <> $0201)  // WM_LBUTTONDOWN
  and (Msg.Message <> $0202)  // WM_LBUTTONUP
  and (Msg.Message <> $0204)  // WM_RBUTTONDOWN
  and (Msg.Message <> $0205)  // WM_RBUTTONUP

  //and (Msg.Message <> $B01A)

  and (Msg.Message <> $C099)  // WM_PAINT
  and (Msg.Message <> $C09E)
  and (Msg.Message <> $C09F)
  and (Msg.Message <> $C0A1)}
  then
    Memo1.Lines.Add(IntToHex(Msg.hwnd, 8) + ' ' + IntToHex(Msg.message, 4) + ' ' + IntToHex(Msg.wParam, 8))

end;

procedure TMainForm.ApplicationEvents1Minimize(Sender: TObject);
begin
  //Memo1.Lines.Add('minimized');
end;

procedure TMainForm.ApplicationEvents1Restore(Sender: TObject);
begin
  //Memo1.Lines.Add('restored');
end;

end.
