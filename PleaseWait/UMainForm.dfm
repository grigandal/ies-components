object MainForm: TMainForm
  Left = 324
  Top = 240
  BorderStyle = bsDialog
  Caption = #1055#1086#1078#1072#1083#1091#1081#1089#1090#1072', '#1087#1086#1076#1086#1078#1076#1080#1090#1077'...'
  ClientHeight = 354
  ClientWidth = 265
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object ProgressBar: TProgressBar
    Left = 8
    Top = 8
    Width = 249
    Height = 25
    Min = 0
    Max = 200
    Smooth = True
    Step = 1
    TabOrder = 0
  end
  object Memo1: TMemo
    Left = 8
    Top = 80
    Width = 249
    Height = 265
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object Timer: TTimer
    Interval = 100
    OnTimer = TimerTimer
    Left = 224
    Top = 8
  end
  object ApplicationEvents1: TApplicationEvents
    OnActivate = ApplicationEvents1Activate
    OnMessage = ApplicationEvents1Message
    OnMinimize = ApplicationEvents1Minimize
    OnRestore = ApplicationEvents1Restore
    Left = 8
    Top = 40
  end
end
