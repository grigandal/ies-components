unit board_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 08.01.2006 23:00:48 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\DD\delphi\Blackboard\BOARD.tlb (1)
// LIBID: {75A5ADB3-3E7B-11D6-AD45-C901648D9705}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\System32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  boardMajorVersion = 1;
  boardMinorVersion = 0;

  LIBID_board: TGUID = '{75A5ADB3-3E7B-11D6-AD45-C901648D9705}';

  IID_IBlackboard: TGUID = '{75A5ADB4-3E7B-11D6-AD45-C901648D9705}';
  CLASS_Blackboard: TGUID = '{75A5ADB6-3E7B-11D6-AD45-C901648D9705}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IBlackboard = interface;
  IBlackboardDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Blackboard = IBlackboard;


// *********************************************************************//
// Interface: IBlackboard
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {75A5ADB4-3E7B-11D6-AD45-C901648D9705}
// *********************************************************************//
  IBlackboard = interface(IDispatch)
    ['{75A5ADB4-3E7B-11D6-AD45-C901648D9705}']
    procedure AddObject(const ObjectPath: WideString; out Exists: OleVariant); safecall;
    procedure RemoveObject(const ObjectPath: WideString; out Exists: OleVariant); safecall;
    procedure SetParamValue(const ObjectPath: WideString; const ParamName: WideString; 
                            Value: OleVariant; out Exists: OleVariant); safecall;
    procedure GetParamValue(const ObjectPath: WideString; const ParamName: WideString; 
                            out Value: OleVariant; out Exists: OleVariant); safecall;
    procedure RemoveParam(const ObjectPath: WideString; const ParamName: WideString; 
                          out Exists: OleVariant); safecall;
    procedure GetChildCount(const ObjectPath: WideString; const ChildName: WideString; 
                            out Count: OleVariant; out Exists: OleVariant); safecall;
    procedure SetXMLText(const ObjectPath: WideString; const XML: WideString); safecall;
    procedure GetXMLText(const ObjectPath: WideString; out XML: OleVariant); safecall;
    procedure SaveToFile(const FileName: WideString); safecall;
    procedure ShowObjectTree; safecall;
    procedure LoadFromFile(const FileName: WideString); safecall;
    procedure FindObject(const ObjectPath: WideString; const ParamName: WideString; 
                         Value: OleVariant; out Index: OleVariant); safecall;
    function  HTML: WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IBlackboardDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {75A5ADB4-3E7B-11D6-AD45-C901648D9705}
// *********************************************************************//
  IBlackboardDisp = dispinterface
    ['{75A5ADB4-3E7B-11D6-AD45-C901648D9705}']
    procedure AddObject(const ObjectPath: WideString; out Exists: OleVariant); dispid 1;
    procedure RemoveObject(const ObjectPath: WideString; out Exists: OleVariant); dispid 2;
    procedure SetParamValue(const ObjectPath: WideString; const ParamName: WideString; 
                            Value: OleVariant; out Exists: OleVariant); dispid 3;
    procedure GetParamValue(const ObjectPath: WideString; const ParamName: WideString; 
                            out Value: OleVariant; out Exists: OleVariant); dispid 4;
    procedure RemoveParam(const ObjectPath: WideString; const ParamName: WideString; 
                          out Exists: OleVariant); dispid 5;
    procedure GetChildCount(const ObjectPath: WideString; const ChildName: WideString; 
                            out Count: OleVariant; out Exists: OleVariant); dispid 6;
    procedure SetXMLText(const ObjectPath: WideString; const XML: WideString); dispid 7;
    procedure GetXMLText(const ObjectPath: WideString; out XML: OleVariant); dispid 8;
    procedure SaveToFile(const FileName: WideString); dispid 9;
    procedure ShowObjectTree; dispid 10;
    procedure LoadFromFile(const FileName: WideString); dispid 11;
    procedure FindObject(const ObjectPath: WideString; const ParamName: WideString; 
                         Value: OleVariant; out Index: OleVariant); dispid 12;
    function  HTML: WideString; dispid 15;
  end;

// *********************************************************************//
// The Class CoBlackboard provides a Create and CreateRemote method to          
// create instances of the default interface IBlackboard exposed by              
// the CoClass Blackboard. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBlackboard = class
    class function Create: IBlackboard;
    class function CreateRemote(const MachineName: string): IBlackboard;
  end;

implementation

uses ComObj;

class function CoBlackboard.Create: IBlackboard;
begin
  Result := CreateComObject(CLASS_Blackboard) as IBlackboard;
end;

class function CoBlackboard.CreateRemote(const MachineName: string): IBlackboard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Blackboard) as IBlackboard;
end;

end.
