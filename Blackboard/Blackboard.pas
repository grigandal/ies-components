unit Blackboard;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, board_TLB, StdVcl, XMLDoc, XMLIntf, Variants,
  SysUtils, Dialogs, Forms, Controls, ComCtrls, Classes,
  XMLTreeView, HTMLConst;

type
  TBlackboard = class(TAutoObject, IBlackboard)
  protected
    procedure AddObject(const ObjectPath: WideString; out Exists: OleVariant);
      safecall;
    procedure GetChildCount(const ObjectPath, ChildName: WideString; out Count,
      Exists: OleVariant); safecall;
    procedure GetParamValue(const ObjectPath, ParamName: WideString; out Value,
      Exists: OleVariant); safecall;
    procedure GetXMLText(const ObjectPath: WideString; out XML: OleVariant);
      safecall;
    procedure RemoveObject(const ObjectPath: WideString;
      out Exists: OleVariant); safecall;
    procedure RemoveParam(const ObjectPath, ParamName: WideString;
      out Exists: OleVariant); safecall;
    procedure SetParamValue(const ObjectPath, ParamName: WideString;
      Value: OleVariant; out Exists: OleVariant); safecall;
    procedure SetXMLText(const ObjectPath, XML: WideString); safecall;
    procedure LoadFromFile(const FileName: WideString); safecall;
    procedure SaveToFile(const FileName: WideString); safecall;
    procedure ShowObjectTree; safecall;
    procedure FindObject(const ObjectPath, ParamName: WideString;
      Value: OleVariant; out Index: OleVariant); safecall;
    function HTML: WideString; safecall;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  private
    TreeFrm: TForm;
    SB: TStatusBar;
    XMLTV: TXMLTreeView;
    XMLDoc: IXMLDocument;
    procedure SplitPath(const ObjectPath: WideString; var Parent, Path: WideString);
    procedure SplitName(const ObjectName: WideString; var Name: WideString; var Index: integer);
    function GetNodeFrom(Kids: IXMLNodeList; ObjectPath: WideString): IXMLNode;
    function GetNode(ObjectPath: WideString): IXMLNode;
    procedure UpdateFrm;
  end;

implementation

uses ComServ;

// ----------------------------- constructor -----------------------------------
procedure TBlackboard.Initialize;
begin
  inherited Initialize;
  XMLDoc := TXMLDocument.Create(NIL);
  XMLDoc.LoadFromXML('<bb />');
  XMLDoc.Encoding := 'utf-8'; // ��� ��������� �������� �����
  TreeFrm := TForm.Create(NIL);
  TreeFrm.Caption := '������ �������� �������� �����';
  TreeFrm.Height := 500;
  TreeFrm.Width := 500;
  TreeFrm.Left := 0;
  TreeFrm.Top := (Screen.Height - TreeFrm.Height) div 2;
  SB := TStatusBar.Create(TreeFrm);
  SB.Parent := TreeFrm;
  SB.Align := alBottom;
  SB.AutoHint := True;
  XMLTV := TXMLTreeView.Create(TreeFrm);
  XMLTV.Parent := TreeFrm;
  XMLTV.Align := alClient;
  XMLTV.Font.Name := 'Courier New';
end;

destructor TBlackboard.Destroy;
begin
  TreeFrm.RemoveControl(SB);
  TreeFrm.RemoveControl(XMLTV);
  SB.Free;
  XMLTV.Free;
  TreeFrm.Free;
  inherited Destroy;
end;

// ----------------------------- protected -------------------------------------
procedure TBlackboard.AddObject(const ObjectPath: WideString;
  out Exists: OleVariant);
var
  OldPath,First,Rest,Name: WideString;
  Index: integer;
  Node,Temp: IXMLNode;
begin
  // ��������� ������ � ������ ObjectPath ����������� ������
  // ��������� �����������, ���� �� ������ �� ����������, ����� �� �����������
  Exists := True;
  if ObjectPath<>'' then begin
    OldPath := ObjectPath;
    SplitPath(OldPath,First,Rest); // ����� X[i].Y.Z �� X[i] � Y.Z
    SplitName(First,Name,Index);   // ����� X[i] �� X � i
    Node := XMLDoc.DocumentElement;
    // ���� ������ ������ � ���� ������, �� ��� ok
    if (Node.NodeName = Name) and (Index = 0) then begin
      OldPath := Rest;
      while Rest<>'' do begin
        SplitPath(OldPath, First,Rest);
        SplitName(First, Name,Index);
        Temp := GetNodeFrom(Node.ChildNodes, First); // �������� ��������� ����
        if Temp = NIL then begin       // ����� ��������� ����������� ���� ���� �� ������ �� ����� ���� Rest
          Temp := Node.AddChild(Name); // � �� ����� ����� ������
          Exists := False;
        end;
        OldPath := Rest;
        Node := temp;
      end;
      // ���� ������ ��� �����������, �� ������� ����������� (���� ��� �� ������)
      if Exists and (Node <> XMLDoc.DocumentElement) then Node.ParentNode.AddChild(Name);
      // ������� �����, ������������ ������ ��������
      if TreeFrm.Visible then UpdateFrm;
    end;
  end;
end;

procedure TBlackboard.GetChildCount(const ObjectPath,
  ChildName: WideString; out Count, Exists: OleVariant);
var
  i: integer;
  Node: IXMLNode;
begin
  Exists := False;
  Count := 0;
  Node := GetNode(ObjectPath);
  if Node <> NIL then begin
    Exists := True;
    if ChildName='' then Count := Node.ChildNodes.Count
    else begin
      i:=0;
      while i<Node.ChildNodes.Count do begin
        if Node.ChildNodes.Nodes[i].NodeName = ChildName then Inc(Count);
        Inc(i);
      end;
    end;
  end;
end;

procedure TBlackboard.GetParamValue(const ObjectPath,
  ParamName: WideString; out Value, Exists: OleVariant);
var Node: IXMLNode;
begin
  Exists := False;
  Value := null();
  Node := GetNode(ObjectPath);
  if Node <> NIL then begin
    Value := Node.Attributes[ParamName];
    if Value <> null() then Exists := True;
  end;
end;

procedure TBlackboard.GetXMLText(const ObjectPath: WideString;
  out XML: OleVariant);
var
  Node: IXMLNode;
begin
  XML := '';
  Node := GetNode(ObjectPath);
  if Node <> NIL then XML := Node.XML;
end;

procedure TBlackboard.RemoveObject(const ObjectPath: WideString;
  out Exists: OleVariant);
var Node, Parent: IXMLNode;
begin
  Exists := False;
  Node := GetNode(ObjectPath);
  if (Node <> NIL) and (Node <> XMLDoc.DocumentElement) then begin
    Exists := True;
    Parent := Node.ParentNode;
    Parent.ChildNodes.Delete(Parent.ChildNodes.IndexOf(Node));
    // ������� �����, ������������ ������ ��������
    if TreeFrm.Visible then UpdateFrm;
  end;
end;

procedure TBlackboard.RemoveParam(const ObjectPath, ParamName: WideString;
  out Exists: OleVariant);
var Node: IXMLNode;
begin
  Exists := False;
  Node := GetNode(ObjectPath);
  if Node <> NIL then begin
    Exists := True;
    Node.Attributes[ParamName] := Null();
    // ������� �����, ������������ ������ ��������
    if TreeFrm.Visible then UpdateFrm;
  end;
end;

procedure TBlackboard.SetParamValue(const ObjectPath,
  ParamName: WideString; Value: OleVariant; out Exists: OleVariant);
var Node: IXMLNode;
begin
  Exists := True;  // ���� ������ �� ����������, �� AddObject ������ False
  Node := GetNode(ObjectPath);
  if Node = NIL then begin
    AddObject(ObjectPath, Exists);
    Node := GetNode(ObjectPath);
  end;
  try
    Node.Attributes[ParamName] := Value;
  except
    on e: Exception do ShowMessage('BB: ������ '+e.Message+' ��� ������������ �������� �������� ' + ParamName + ' ������� '+ ObjectPath);
  end;
  // ������� �����, ������������ ������ ��������
  if TreeFrm.Visible then UpdateFrm;
end;

procedure TBlackboard.SetXMLText(const ObjectPath, XML: WideString);
var
  XMLDocument: IXMLDocument;
  Node: IXMLNode;
  Exists: OleVariant;
begin
  if XML<>'' then begin
    Node := GetNode(ObjectPath);

    // ���� ���� ���, �� �������� ���
    if Node = NIL then begin
      AddObject(ObjectPath, Exists);
      Node := GetNode(ObjectPath);
      //ShowMessage(ObjectPath + ' # ' + XML);  // bb.wm � bb.wm.rules �����������
    end;

    // ��������� ����� ������� �������� XML-���������
    XMLDocument := TXMLDocument.Create(NIL);
    XMLDocument.LoadFromXML(XML);
    Node.ChildNodes.Add(XMLDocument.DocumentElement.CloneNode(True));
    // ������� �����, ������������ ������ ��������
    if TreeFrm.Visible then UpdateFrm;
  end;
end;

procedure TBlackboard.LoadFromFile(const FileName: WideString);
begin
  XMLDoc.LoadFromFile(FileName);
  // ������� �����, ������������ ������ ��������
  if TreeFrm.Visible then UpdateFrm;
end;

procedure TBlackboard.SaveToFile(const FileName: WideString);
begin
  XMLDoc.SaveToFile(FileName);
end;

procedure TBlackboard.ShowObjectTree;
begin
  UpdateFrm;
  TreeFrm.Show;
end;

procedure TBlackboard.FindObject(const ObjectPath, ParamName: WideString;
  Value: OleVariant; out Index: OleVariant);
safecall;
var
  Node: IXMLNode;
  Name: OleVariant;
begin
  // ������� � ObjectPath � ����� ���� ������,
  // �������� ParamName �������� ����� �������� Value
  // ������������ ������ ����������� �������, ����� -1
  // Ex: FindObject('bb.wm.facts.fact[3]', 'AttrPath', '������1.�������1', Index)
  Index := -1;
  Node := GetNode(ObjectPath);
  if (Value <> null) and (Node <> NIL) and (Node <> XMLDoc.DocumentElement) then
  begin
    while Node <> NIL do begin
      Name := Node.Attributes[ParamName];
      if (Name <> null) and (AnsiCompareStr(Name, Value) = 0) then break;
      try
        Node := Node.NextSibling;
      except
        Node := NIL;
      end;
    end;
    if Node <> NIL then Index := Node.ParentNode.ChildNodes.IndexOf(Node);
  end;
end;


function TBlackboard.HTML: WideString;
 // ���������� ������ � ������ ���� � ���������� ��� ���������
 function FormCaption(XMLNode: IXMLNode): WideString;
 var
   s: WideString;
   i: integer;
 begin
    s := XMLNode.NodeName;
    for i:=0 to XMLNode.AttributeNodes.Count-1 do
      s := s+' '+XMLNode.AttributeNodes[i].NodeName+'='+XMLNode.AttributeNodes[i].NodeValue+'';
    Result := s;
 end;
 // ���������� ��� �� javascript �� ���������� ����� � TreeView
 function RecurseInto(XMLNode: IXMLNode; var LastIndex: integer): WideString;
 var
   i, ParentIndex: integer;
 begin
   ParentIndex := LastIndex;
   Result := '';
   for i:=0 to XMLNode.ChildNodes.Count-1 do begin
     LastIndex := LastIndex+1; // ������� ����
     Result := Result +
       '  t = TV.Nodes.Add(TV.Nodes('+IntToStr(ParentIndex)+'), 0);' + #10 +
       '  t.Text = "' + FormCaption(XMLNode.ChildNodes[i]) + '";' + #10 +
       '  t.Parent = TV.Nodes('+IntToStr(ParentIndex)+');' + #10#10 +
       RecurseInto(XMLNode.ChildNodes[i], LastIndex);
   end;
 end;
var
  TVScript: WideString;
  LastIndex: integer;
begin
  // ������� HTML ������ �� XML ������, ����� �� xml ����� ��������
  LastIndex := 1;
  TVScript :=
    '  t = TV.Nodes.Add(TV.nodeValue, 0);' + #10 +
    '  t.Text = "'+FormCaption(XMLDoc.DocumentElement) + '";' + #10#10 +
    RecurseInto(XMLDoc.DocumentElement, LastIndex) +
    '  TV.Nodes(1).Selected = 1';
  Result :=
    '<p>html from blackboard</p>' + #10 +
    '<p>' + TVDef + '</p>' + #10 +
    '<script>' + #10 + TVScript + #10 + '</script>';
end;

// ------------------------------ private --------------------------------------
procedure TBlackboard.SplitPath(const ObjectPath: WideString;
  var Parent, Path: WideString); // X.Y.Z -> (X, Y.Z)
var i: integer;
begin
  Parent := '';
  Path := '';
  if Length(ObjectPath)>0 then begin
    i := Pos('.',ObjectPath);
    if i=0 then Parent := ObjectPath
    else begin
      Parent := Copy(ObjectPath,1,i-1);
      Path := Copy(ObjectPath,i+1,Length(ObjectPath)-i);
    end;
  end;
end;

procedure TBlackboard.SplitName(const ObjectName: WideString; var Name: WideString; var Index: integer);
var
  i,j: integer;
  s: string;
begin
  Name := ObjectName;
  Index := 0; // ������ ������� �� ���������
  if Length(ObjectName)>0 then begin
    i := Pos('[',ObjectName);
    j := Pos(']',ObjectName);
    if (i>0)and(j>i) then begin
      Name := Copy(ObjectName,1,i-1);
      s := Copy(ObjectName,i+1,j-i-1);
      try
        Index := StrToInt(s);
      except
        Index := 0;
      end;
    end;
  end;
end;

// ���������� ���� XMLNode �� ��������� Kids �� ��� ����, ���� �� ����������, ����� NIL
function TBlackboard.GetNodeFrom(Kids: IXMLNodeList; ObjectPath: WideString): IXMLNode;
var
  Parent, Path, Name: WideString;
  Index,i: integer;
begin
  Result := NIL;
  SplitPath(ObjectPath, Parent, Path);
  SplitName(Parent, Name, Index);
  i:=0;
  while i<Kids.Count do begin
    if Kids.Nodes[i].NodeName=Name then begin
      if index=0 then begin
        if Path=''
        then Result := Kids.Nodes[i]
        else Result := GetNodeFrom(Kids.Nodes[i].ChildNodes,Path);
        break;
      end;
      Dec(index);
    end;
    Inc(i);
  end;
end;

// ���������� ���� XMLNode �� ��� ����, ���� �� ����������, ����� NIL
function TBlackboard.GetNode(ObjectPath: WideString): IXMLNode;
var
  Parent, Path, Name: WideString;
  Index: integer;
begin
  if ObjectPath='' then Result := NIL
  else begin
    SplitPath(ObjectPath, Parent, Path);
    SplitName(Parent, Name, Index);
    if (XMLDoc.DocumentElement.NodeName=Name) and (Index=0) then begin
      if Path='' then Result := XMLDoc.DocumentElement
      else Result := GetNodeFrom(XMLDoc.DocumentElement.ChildNodes, Path);
    end;
  end;
end;

// ��������� ������ �������� �� �����
procedure TBlackboard.UpdateFrm;
begin
  XMLTV.Hide;
  XMLTV.XMLDocument.LoadFromXML(XMLDoc.XML.Text);
  XMLTV.SyncronizeTree;
  XMLTV.Items[0].Selected := True;
  XMLTV.Show;
end;

// -----------------------------------------------------------------------------

initialization
  TAutoObjectFactory.Create(ComServer, TBlackboard, Class_Blackboard,
    ciMultiInstance, tmApartment);
end.
