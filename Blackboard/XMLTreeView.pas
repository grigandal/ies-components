unit XMLTreeView;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, ComCtrls,
  XMLDoc, XMLIntf, Dialogs;

type
  TNodeOption = (
    noShowAttributes,   // ���������� �� � ������ �������� �����
    noShowTextNodes,    // ���������� �� ��������� ����
    noTextNodesAsText,  // ������ #text ������������ ��� ����� ����
    noReadOnly          // �������������� ���������
  );
  TNodeOptions = set of TNodeOption;

  TXMLTreeView = class(TTreeView)
  private
    FNodeOptions: TNodeOptions;
    procedure GetAttributes(XMLNode: IXMLNode; TreeNode: TTreeNode);
    procedure RecurseInto(XMLNode: IXMLNode; TreeNode: TTreeNode);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SyncronizeTree;
    function GetXMLNode(Node: TTreeNode): IXMLNode;
    function GetTreeNode(Node: IXMLNode): TTreeNode;
    procedure NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  published
    XMLDocument: IXMLDocument;
    property NodeOptions: TNodeOptions read FNodeOptions write FNodeOptions;
  end;

implementation

{=============================== TXMLTreeView ============================}
constructor TXMLTreeView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  XMLDocument := TXMLDocument.Create(NIL);
  Self.ReadOnly := True;
  Self.RightClickSelect := True;
  NodeOptions := [noShowAttributes, noShowTextNodes, noTextNodesAsText, noReadOnly];
  Self.OnMouseUp := NodeMouseUp;
end;

destructor TXMLTreeView.Destroy;
begin
  inherited Destroy;
end;

procedure TXMLTreeView.NodeMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var Node: TTreeNode;
begin
  // ��� ����� ���������� ������ ������� ���� ���� ���� �������� OnChange
  if Button = mbRight then begin
    Node := Self.Selected; // �������� ��� �������� ������ �������
    Node.Selected := True;    // �����������, �.�. ����� ��������� ��������� ��, ��� ����
    Node.Focused := True;
    if Assigned(Self.OnChange) then Self.OnChange(Sender, Self.Selected);
  end;
end;

procedure TXMLTreeView.SyncronizeTree;
var
  XMLRoot: IXMLNode;
begin
  Self.Items.Clear;
  if (not XMLDocument.Active)or(XMLDocument.DocumentElement = nil) then exit;
  XMLRoot := XMLDocument.DocumentElement; // ������ ��������� XML
  if xmlRoot.NodeType = ntText then
    Self.Items.AddChild(NIL,XMLRoot.Text)
  else begin
    Self.Items.AddChild(NIL,XMLRoot.NodeName);
    GetAttributes(XMLRoot,Items[0]);
    RecurseInto(XMLRoot,Items[0]);
  end;
  Self.FullExpand;
end;

function TXMLTreeView.GetXMLNode(Node: TTreeNode): IXMLNode;
var XMLParent: IXMLNode;
begin
  if Node = NIL then
    Result := NIL
  else if Node = Items[0] then // ����� �� ��������
    Result := XMLDocument.DocumentElement
  else begin
    XMLParent := GetXMLNode(Node.Parent);
    Result := XMLParent.ChildNodes.Nodes[Node.Index];
  end;
end;

function TXMLTreeView.GetTreeNode(Node: IXMLNode): TTreeNode;
var Parent: TTreeNode;
begin
  if Node = NIL then
    Result := NIL
  else if Node = XMLDocument.DocumentElement then
    Result := Items[0]
  else begin
    Parent := GetTreeNode(Node.ParentNode);
    Result := Parent.Item[Node.ParentNode.ChildNodes.IndexOf(Node)];
  end;
end;

//-------------------------------- private procs -------------------------------
procedure TXMLTreeView.GetAttributes(XMLNode: IXMLNode; TreeNode: TTreeNode);
var
  i: integer;
  XMLAttr: IXMLNode;
begin
  if noShowAttributes in NodeOptions then
  for i:=0 to XMLNode.AttributeNodes.Count-1 do begin
    XMLAttr := XMLNode.AttributeNodes.Nodes[i];             
    Self.Items.AddChild(TreeNode,XMLAttr.NodeName+' = "'+XMLAttr.Text+'"');
  end;
end;

procedure TXMLTreeView.RecurseInto(XMLNode: IXMLNode; TreeNode: TTreeNode);
var
  i: integer;
  XMLChild: IXMLNode;
  TreeChild: TTreeNode;
begin
  for i:=0 to xmlNode.ChildNodes.Count-1 do begin
    XMLChild := xmlNode.ChildNodes.Nodes[i];
    if xmlChild.NodeType = ntText then begin
      if (noShowTextNodes in NodeOptions) then // ���� ���� ���������� ��������� ����
        if (noTextNodesAsText in NodeOptions) then // ���� ���������� ��� �����
          Self.Items.AddChild(TreeNode,xmlChild.Text)
        else
          Self.Items.AddChild(TreeNode,xmlChild.NodeName)
    end
    else begin // ����� �������� ��� ��� � ����� ������
      TreeChild := Self.Items.AddChild(TreeNode,xmlChild.NodeName);
      GetAttributes(XMLChild,TreeChild);
      RecurseInto(XMLChild,TreeChild);
    end;
  end;
end;


end.
