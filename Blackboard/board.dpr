library board;

uses
  ComServ,
  board_TLB in 'board_TLB.pas',
  Blackboard in 'Blackboard.pas' {Blackboard: CoClass},
  XMLTreeView in 'XMLTreeView.pas',
  HTMLConst in 'HTMLConst.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
