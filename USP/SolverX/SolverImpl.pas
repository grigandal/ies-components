unit SolverImpl;

{$WARN SYMBOL_PLATFORM OFF}
{ ******************************************************************************
  * ������ SolverImpl.pas �������� ������� TSolverX, ����������� ����������:
  *
  * SolverXLib_TLB::IESComponent - ���������, ����������� ����� ������������ ��
  * SolverXLib_TLB::ISolver - ��������� ��� ������ ��������
  *
  * �����: << ������� �. �. >>
  * ���� ��������: << 29 ������, 2003 >>
  ****************************************************************************** }

interface

uses
  ComObj, ActiveX, StdVcl, XMLDoc, XMLIntf, Variants, SysUtils,
  SolverXLib_TLB, KB_Common, KB_Containers, IE_Solver, IE_Debugger;

type
  TSolverX = class(TAutoObject, ISolver, IESComponent)
  private
    ConfigDoc: IXMLDocument;
    InitialSituation: IXMLDocument;
    ExplData: IXMLDocument;
    // IESComponent
    Name: WideString;
    Broker: OleVariant;
    // ISolver
  protected
    // IESComponent
    function Get_Broker: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName, MessageText: WideString;
      var Output: OleVariant); safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Stop; safecall;
    // ISolver

  public
    Debugger: TDebugger; // � ������ TSolver
    procedure RequestValue(Sender: TObject; Ref: WideString);
    procedure DemonRun(Receiver, MsgIn: WideString; var Output: OleVariant);
    procedure Initialize; override;
    destructor Destroy; override;
  private
    Reader: Variant;
    // ��������� ������
    procedure ShellSync(const aCommando: string);
    procedure LoadInitialSituation();
    procedure PutFactsOnBlackBoard();
    procedure PutTraceOnBlackBoard();
    procedure ClearFactsFromBB();
    function GetExplData: WideString;
    procedure ConfigureSolver;
    procedure SetupFromXML(n: IXMLNode);
  end;

implementation

uses
  ComServ, VCL.Controls, Windows, VCL.Dialogs, VCL.Forms,
  SolverConsts, OM_Containers, KB_Values;

procedure TSolverX.Initialize;
begin
  inherited;
  ConfigDoc := TXMLDocument.Create(nil);
  ConfigDoc.LoadFromXML('<config />');

  InitialSituation := TXMLDocument.Create(nil);
  InitialSituation.LoadFromXML('<facts />');

  ExplData := TXMLDocument.Create(NIL);
  ExplData.LoadFromXML('<explanations />');

  // �������������� ���������� ������
  if ErrorHandler = nil then
    ErrorHandler := TErrorHandler.Create(nil);
  ErrorHandler.Silent := true;

  Debugger := TDebugger.Create(nil);
  Debugger.Name := 'Debugger';

  // ������� ����������� ������� �� ��������� �� Debugger �� ���� �����������
  Debugger.Solver.OnRequestValue := RequestValue;
  Debugger.Solver.OnDemon := DemonRun; // ������� ����� �� �� ����� ����
  Debugger.Solver.Loader.PtrOnDemon := DemonRun;
  // ����� Loader ���� �� ���� �����������
end;

destructor TSolverX.Destroy;
begin
  ExplData := nil;
  InitialSituation := nil;
  ConfigDoc := nil;
  Debugger.Free;
  inherited;
end;

// ���������� ������� OnRequestValue
procedure TSolverX.RequestValue(Sender: TObject; Ref: WideString);
var
  Output, Value, Exists, Index, Attr: OleVariant;
  Doc: IXMLDocument;
  Attrs: TAttrList;
  Asked: boolean;
begin
  // ���� ��� �������� �����, �� ���� �� ������ (����)
  if VarIsEmpty(Broker.Blackboard) then
    exit;

  // ����� �������������, ��� ���������� ������� �� �������� �����
  PutFactsOnBlackBoard;

  // ��������������, ��� Dialoger ������� �� �������� ����� �������� ����� ��������
  Doc := TXMLDocument.Create(NIL);
  Doc.LoadFromXML('<message />');
  with Doc.DocumentElement do
  begin
    Attributes['ProcName'] := 'GetFact';
    AddChild('param');
    ChildNodes[0].Attributes['Name'] := 'AttrName';
    ChildNodes[0].NodeValue := Ref;
  end;
  Broker.SendMessage(Name, 'Dialoger', Doc.DocumentElement.XML, Output);
  // ����� ����� ������ Stop
  Doc := nil;

  if Debugger.Solver.StopFlag then
    exit;

  // � ������ ��� �������� c BB
  Broker.Blackboard.FindObject(BBFacts, BBAttrName, Ref, Index);
  if Index >= 0 then
  begin
    Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(Index) + ']',
      BBAttrValue, Value, Exists);
    if Exists then
    begin
      Attrs := TAttrList.Create(nil);
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(Index) + ']',
        BBBelief, Attr, Exists);
      if Exists then
        Attrs.Belief := Attr / 100;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(Index) + ']',
        BBMaxBelief, Attr, Exists);
      if Exists then
        Attrs.Probability := Attr / 100;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(Index) + ']',
        BBAccuracy, Attr, Exists);
      if Exists then
        Attrs.Accuracy := Attr / 100;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(Index) + ']',
        BBAsked, Attr, Exists);
      Asked := Exists and (Attr = 'T');

      Debugger.Solver.SetFact(Ref, Value, Attrs, Asked);
      Attrs.Free;
    end;
  end;
end;

// ���������� ������� �� �������� ����������
procedure TSolverX.DemonRun(Receiver, MsgIn: WideString;
  var Output: OleVariant);
begin
  Broker.SendMessage(Name, Receiver, MsgIn, Output);
end;


// ---------------------------------------------------------------
// --------------------- ������ IESComponent ---------------------
// ---------------------------------------------------------------

function TSolverX.Get_Broker: OleVariant;
begin
  Result := Broker;
end;

function TSolverX.Get_Name: WideString;
begin
  Result := Name;
end;

procedure TSolverX.Set_Broker(Value: OleVariant);
begin
  Broker := Value;
end;

procedure TSolverX.Set_Name(const Value: WideString);
begin
  Name := Value;
end;

// ��������� �.�. 2020
// ���������� ���������� ������� � cmd
procedure TSolverX.ShellSync(const aCommando: string);
var
  tmpStartupInfo: TStartupInfo;
  tmpProcessInformation: TProcessInformation;
  tmpProgram: String;
begin
  tmpProgram := trim(aCommando);
  FillChar(tmpStartupInfo, SizeOf(tmpStartupInfo), 0);
  with tmpStartupInfo do
  begin
    cb := SizeOf(TStartupInfo);
    wShowWindow := SW_HIDE;
  end;

  if CreateProcess(nil, pchar(tmpProgram), nil, nil, true, CREATE_NO_WINDOW,
    nil, nil, tmpStartupInfo, tmpProcessInformation) then
  begin
    // loop every 10 ms
    while WaitForSingleObject(tmpProcessInformation.hProcess, 10) > 0 do
    begin
      Application.ProcessMessages;
    end;
    CloseHandle(tmpProcessInformation.hProcess);
    CloseHandle(tmpProcessInformation.hThread);
  end
  else
  begin
    RaiseLastOSError;
  end;
end;

procedure TSolverX.Configurate(const Config: WideString);
{ <config>
  <FileName>c:\dd\delphi\dialoger\es\medes.kbs</FileName>
  <setup Style="forward" MultipleValues="false" Authentic="false" Subdialogs="true"/>
  </config> }
var
  n: IXMLNode;
  fn, path, cmd: WideString;
begin
  ConfigDoc.LoadFromXML(Config);
  n := ConfigDoc.DocumentElement.ChildNodes['FileName'];
  if not VarIsNull(n) then
  begin
    fn := n.Text;
    // ���� ���� �������������, �� ������� ����
    if AnsiPos(':\', fn) = 0 then
    begin
      try
        path := Broker.path;
      except
        // ���� ��� �������� Path ��� ��� �������
        path := ExtractFilePath(Application.ExeName)
      end;
      fn := path + fn;
    end;
    Debugger.Solver.KBFileName := fn;

    // ���������, ���� �� �������������� ���� ����� ���������
    // ���� ���������� kbs, �� � ��������� �����
    if AnsiCompareStr(ExtractFileExt(Debugger.Solver.KBFileName), '.kbs') = 0
    then
    begin
      // Reader := CreateOleObject('ReadKB.KBReader');
      // Reader.FileName := Debugger.Solver.KBFileName;
      // Reader.Load;
      { ��������� �. �. 2020
        ReadKB.KBReader �� ������ �������� ����, ��������� ���� ���������
      }

      ShowMessage(GetCurrentDir);
      cmd := '"' + GetCurrentDir + '\KBInterpriter\venv\Scripts\pythonw" "' +
        GetCurrentDir + '"\KBInterpriter\main.py -o "' + fn + '" -s "' +
        ExtractFilePath(fn) + '~temp.xml"';
      ShowMessage(cmd);
      ShellSync(cmd);

      fn := ExtractFilePath(fn) + '~temp.xml';
      // Reader.SaveToFile(fn);
      Debugger.Solver.KBFileName := fn;
      Reader := Unassigned;
    end;
    // ���� xml, �� 2 ��������
    // ��� ������, ��������� �� �����

    // Debugger.Solver.Loader.LoadFile(Debugger.Solver.KB, Debugger.Solver.KBFileName);
    Debugger.DebugForm.LoadKB(Debugger.Solver.KBFileName);
    Debugger.Solver.BuildGoalMap;
  end;

  n := ConfigDoc.DocumentElement.ChildNodes.FindNode('setup');
  if n <> nil then
    SetupFromXML(n);
end;

procedure TSolverX.SetupFromXML(n: IXMLNode);
begin
  if not VarIsNull(n.Attributes['Style']) then
  begin
    if AnsiCompareText(n.Attributes['Style'], 'forward') = 0 then
      Debugger.Solver.Configuration.Style := isForward
    else if AnsiCompareText(n.Attributes['Style'], 'backward') = 0 then
      Debugger.Solver.Configuration.Style := isBackward
    else if AnsiCompareText(n.Attributes['Style'], 'mixed') = 0 then
      Debugger.Solver.Configuration.Style := isMixed;
  end;

  if not VarIsNull(n.Attributes['Subdialogs']) then
  begin
    if AnsiCompareText(n.Attributes['Subdialogs'], 'false') = 0 then
      Debugger.Solver.Configuration.Subdialogs := false
    else
      Debugger.Solver.Configuration.Subdialogs := true;
  end;

  if not VarIsNull(n.Attributes['Authentic']) then
  begin
    if AnsiCompareText(n.Attributes['Authentic'], 'true') = 0 then
      Debugger.Solver.Configuration.Authentic := true
    else
      Debugger.Solver.Configuration.Authentic := false;
  end;
end;

procedure TSolverX.ProcessMessage(const SenderName, MessageText: WideString;
  var Output: OleVariant);
var
  MessageDoc: IXMLDocument;
  Param: IXMLNode;
  ProcName, s: string;
  Exists: Variant;
begin

  // ������ ������: Init, SetGoal, Run, ShowBlackboard, ConfigureSolver, EditKB, Debug, GetExplData

  MessageDoc := TXMLDocument.Create(nil);
  MessageDoc.LoadFromXML(MessageText);
  ProcName := MessageDoc.DocumentElement.Attributes['ProcName'];
  // ������������� �������� � ������� ������
  // <message ProcName="Init"><setup Style="forward" MultipleValues="false" Authentic="false" Subdialogs="true"/></message>
  if (AnsiCompareText(ProcName, 'TWorkMemoryConfigurator') = 0) or
    (AnsiCompareText(ProcName, 'Init') = 0) then
  begin
    // ���� ���� ��� setup, �� �������� ��������
    Param := MessageDoc.DocumentElement.ChildNodes.FindNode('setup');
    if Param <> nil then
      SetupFromXML(Param);
    ExplData.LoadFromXML('<explanations />');
    Debugger.Solver.Init;
    exit;
  end;

  // ������� ������
  if (AnsiCompareText(ProcName, 'TKnowledgeBase.ClearWorkMemory') = 0) then
  begin
    // ������� ����������
    InitialSituation.LoadFromXML('<facts />');
    ExplData.LoadFromXML('<explanations />');
    // ������� ��� �� �������� ����� (������� wm � explanations)
    ClearFactsFromBB;
    Debugger.Solver.Init;
    exit;
  end;

  // ��������� ����. ��� ���� ������ ����� Solver.Init � Solver.Run
  // <message ProcName="AddGoal"><goal AttrPath="������1.�������1" /></message>
  if AnsiCompareText(ProcName, 'AddGoal') = 0 then
  begin
    Output := 1; // �� ��������� ��������� ����� �������������
    Param := MessageDoc.DocumentElement.ChildNodes[0];
    if (Param = nil) or VarIsNull(Param.Attributes['AttrPath']) then
      exit;
    s := Param.Attributes['AttrPath'];
    Debugger.Solver.SetGoal(s);
    Output := 0; // ��������� �������������
    exit;
  end;

  // <message ProcName="SetGoal">������2.�������3</message>
  if AnsiCompareText(ProcName, 'SetGoal') = 0 then
  begin
    Output := 1; // �� ��������� ��������� ����� �������������
    s := MessageDoc.DocumentElement.Text;
    Debugger.Solver.SetGoal(s);
    Output := 0; // ��������� �������������
    exit;
  end;

  // ��������� ��������
  // <message ProcName="Run"/>
  if (AnsiCompareText(ProcName, 'Run') = 0) or
    (AnsiCompareText(ProcName, 'TSolve') = 0) then
  begin
    // Debugger.DebugForm.LoadKB(Debugger.Solver.KBFileName); // ������-�� �� �����

    if not VarIsEmpty(Broker.Blackboard) then
      LoadInitialSituation();
    // Debugger.Show;

    try
      Debugger.Solver.Run;
    except
      on E: Exception do
        ShowMessage('Debugger.Solver.Run: ' + E.Message);
    end;

    try
      if not VarIsEmpty(Broker.Blackboard) then
      begin
        PutFactsOnBlackBoard();
        PutTraceOnBlackBoard();
        Broker.Blackboard.SetXMLText('bb', GetExplData);
        Broker.Blackboard.SaveToFile('bb.xml');
      end;

    except
      on E: Exception do
        ShowMessage('dump blackboard: ' + E.Message);
    end;

    exit;
  end;

  // �������� �������� �����
  // <message ProcName='ShowBlackboard'></message>
  if AnsiCompareText(ProcName, 'ShowBlackboard') = 0 then
  begin
    Broker.Blackboard.ShowObjectTree;
    exit;
  end;

  // ���������������� ���������� ��������
  if (AnsiCompareText(ProcName, 'ConfigureSolver') = 0) or
    (AnsiCompareText(ProcName, 'Configure') = 0) then
  begin
    ConfigureSolver;
    Output := ConfigDoc.DocumentElement.XML;
    exit;
  end;

  // ������ XML ��������� ��
  // <message ProcName="EditKB"></message>
  if AnsiCompareText(ProcName, 'EditKB') = 0 then
  begin
    s := ExtractFilePath(Application.ExeName) + 'xmleditor\xmleditor.exe ' +
      ConfigDoc.DocumentElement.ChildNodes['kb'].Attributes['FileName'];
    WinExec(PAnsiChar(s + #0), SW_SHOW);
    exit;
  end;

  // ������ ���������
  if AnsiCompareText(ProcName, 'Debug') = 0 then
  begin
    Debugger.DebugForm.LoadKB(Debugger.Solver.KBFileName);
    Debugger.Show;
    exit;
  end;

  // ����� ������ ��� ������������ ����������
  // <message ProcName='GetExplData'></message>
  if AnsiCompareText(ProcName, 'GetExplData') = 0 then
  begin
    Output := GetExplData;
    exit;
  end;

  // ��� ��������
  // <message ProcName='Mig81'><text>sadf</text></message>
  if AnsiCompareText(ProcName, 'Mig81') = 0 then
  begin
    if MessageDoc.DocumentElement.ChildNodes.Count > 0 then
    begin
      Param := MessageDoc.DocumentElement.ChildNodes[0];
      s := Param.Text;
    end
    else
      s := '';
    if InputQuery('ProcName="Mig81"',
      '������, ����! �� ������ ��� ������� ���� ��� ������:', s) then
      Output := WideString(s)
    else
      Output := Unassigned;
    exit;
  end;
end;

procedure TSolverX.Stop;
begin
  Debugger.Solver.StopFlag := true;
end;

// ---------------------------------------------------------------
// --------------------- ������ ISolver --------------------------
// ---------------------------------------------------------------

// --------------------------- ������� ---------------------------------------
procedure TSolverX.LoadInitialSituation();
var
  i: integer;
  path, Value, Count, Exists, Attr: OleVariant;
  fact: IXMLNode;
  Attrs: TAttrList;
  Asked, Status: boolean;
begin
  try
    Broker.Blackboard.SaveToFile('bb.xml');
    InitialSituation.LoadFromXML('<facts />');
    // ��������� � �������� ����� ����� � �� � ��������� �� � InitialSituation ��� �����������
    Broker.Blackboard.GetChildCount(BBFactsPath, BBFact, Count, Exists);
    if (Exists = false) or (Count = 0) then
      exit;
    for i := 0 to Count - 1 do
    begin
      Attrs := TAttrList.Create(nil);
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBAttrName, path, Exists);
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBAttrValue, Value, Exists);
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBBelief, Attr, Exists);
      if Exists = true then
        try
          Attrs.Belief := StrToInt(Attr) / 100;
        except
          Attrs.Belief := 1;
        end;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBMaxBelief, Attr, Exists);
      if Exists = true then
        try
          Attrs.Probability := StrToInt(Attr) / 100;
        except
          Attrs.Probability := 1;
        end;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBAccuracy, Attr, Exists);
      if Exists = true then
        try
          Attrs.Accuracy := StrToInt(Attr) / 100;
        except
          Attrs.Accuracy := 0;
        end;
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBStatus, Attr, Exists);
      Status := (Exists = true) and (Attr = 'T');
      Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
        BBAsked, Attr, Exists);
      Asked := (Exists = true) and (Attr = 'T');

      Debugger.Solver.SetFact(path, Value, Attrs, Asked);
      if Status then
        continue; // ��� ������ ����

      fact := InitialSituation.DocumentElement.AddChild(BBFact);
      fact.Attributes[BBAttrName] := path;
      if Asked then
      begin
        fact.Attributes[sAsked] := sTrue;
      end;

      if not VarIsNull(Value) then
      begin
        fact.Attributes[BBAttrValue] := Value;
        if Attrs.IsBelief then
          fact.Attributes[BBBelief] := FloatToStr(round(Attrs.Belief * 100));
        if Attrs.IsProbability then
          fact.Attributes[BBMaxBelief] :=
            FloatToStr(round(Attrs.Probability * 100));
        if Attrs.IsAccuracy then
          fact.Attributes[BBAccuracy] :=
            FloatToStr(round(Attrs.Accuracy * 100));
      end;
      Attrs.Free;
    end;

    // ��������� � �������� ����� ���� � �� � ��������� �� � InitialSituation
    // �� ���� ��������� �������� - ������ ������� ������ ����� ������������ initial ������

    Broker.Blackboard.GetChildCount(BBGoalsPath, BBGoal, Count, Exists);

    if (Exists = false) or (Count = 0) then
      exit;
    i := 0;

    while i < Count do
    begin
      Broker.Blackboard.GetParamValue(BBGoals + '[' + IntToStr(i) + ']',
        BBAttrName, path, Exists);
      fact := InitialSituation.DocumentElement.AddChild(BBGoal);
      fact.Attributes[BBAttrName] := path;
      Debugger.Solver.SetGoal(path); // ������� � OM.Goals
      Inc(i);
    end;
  except
    on E: Exception do
      ShowMessage('LoadInitialSituation: ' + E.Message);
  end;
end;

procedure TSolverX.ClearFactsFromBB();
var
  i: integer;
  path, Count, Exists: OleVariant;
begin
  Broker.Blackboard.RemoveObject('bb.explanations', Exists);

  // ������ ������ �� �����, ������� �������� ������i.�������i ��� ������i (������)
  // Broker.Blackboard.RemoveObject('bb.wm', Exists); //
  Broker.Blackboard.RemoveObject('bb.wm.rules', Exists);

  Broker.Blackboard.GetChildCount(BBFactsPath, BBFact, Count, Exists);
  if (Exists = 0) or (Count = 0) then
    exit;
  for i := Count - 1 downto 0 do
  begin // ����� �����, ��� ��� RemoveObject ����
    Broker.Blackboard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']',
      BBAttrName, path, Exists);
    if (AnsiPos('������', path) > 0) or (AnsiPos('�������', path) > 0) then
    begin
      Broker.Blackboard.RemoveObject(BBFacts + '[' + IntToStr(i) + ']', Exists);
    end;
  end;
end;

procedure TSolverX.PutFactsOnBlackBoard();
  procedure PutFact(IDValue: TIDValue; IDName: WideString);
  var
    idv: TIDValue;
    v: TValue;
    Env: TInstance;
    i: integer;
    Count, Exists: OleVariant;
  begin
    v := IDValue.Value as TValue;
    if v <> nil then
    begin
      // ���� ��� ���� ����� �� �������� �����, �� �������, ����� �������
      Broker.Blackboard.FindObject(BBFacts, 'AttrPath', IDName, Count);
      if Count < 0 then
      begin
        // ������� ����
        Broker.Blackboard.GetChildCount(BBFactsPath, BBFact, Count, Exists);
        Broker.Blackboard.AddObject(BBFacts + '[' + IntToStr(Count) +
          ']', Exists);
      end;

      Broker.Blackboard.SetParamValue(BBFacts + '[' + IntToStr(Count) + ']',
        'AttrPath', IDName, Exists);
      Broker.Blackboard.SetParamValue(BBFacts + '[' + IntToStr(Count) + ']',
        'Value', v.ResVal, Exists);
      if IDValue.Value.Attrs.IsBelief then
        Broker.Blackboard.SetParamValue(BBFacts + '[' + IntToStr(Count) + ']',
          'Belief', FloatToStr(round(v.Attrs.Belief * 100)), Exists);
      if IDValue.Value.Attrs.IsProbability then
        Broker.Blackboard.SetParamValue(BBFacts + '[' + IntToStr(Count) + ']',
          'MaxBelief', FloatToStr(round(v.Attrs.Probability * 100)), Exists);
      if IDValue.Value.Attrs.IsAccuracy then
        Broker.Blackboard.SetParamValue(BBFacts + '[' + IntToStr(Count) + ']',
          'Accuracy', FloatToStr(round(v.Attrs.Accuracy * 100)), Exists);

      // ���� ��� ������ �� ������, �� ������� ��� ��������
      if v.ByRef then
      begin
        try
          Env := TInstance(integer(v.ResVal));
          for i := 0 to Env.PropertyList.Count - 1 do
          begin
            idv := Env.PropertyList.Items[i];
            PutFact(idv, IDName + '.' + idv.ID);
          end
        except
        end;
      end;
    end;
  end;

var
  World: TInstance;
  idv: TIDValue;
  i: integer;
begin
  // ���� ����� ��� ����������, �������� � �.�., ������ ������ ������
  // ShowMessage('Puttin'' facts');
  // ������� ��� ��� ���� � ������� ������ ������ World
  World := Debugger.Solver.OM.Objects[0];
  for i := 0 to World.PropertyList.Count - 1 do
  begin
    idv := World.PropertyList.Items[i];
    PutFact(idv, idv.ID);
  end;
end;

procedure TSolverX.PutTraceOnBlackBoard();
var
  trace, rule, cond, n, then_: IXMLNode;
  i: integer;
  Count, Exists: OleVariant;
begin
  trace := Debugger.Solver.trace.DocumentElement;
  Broker.Blackboard.RemoveObject('bb.wm.rules', Exists);
  for i := 0 to trace.ChildNodes.Count - 1 do
  begin
    rule := trace.ChildNodes[i];
    if rule.NodeName <> 'rule' then
      continue;
    Broker.Blackboard.GetChildCount('bb.wm.rules', 'rule', Count, Exists);
    Broker.Blackboard.AddObject('bb.wm.rules.rule[' + IntToStr(Count) +
      ']', Exists);
    Broker.Blackboard.SetParamValue('bb.wm.rules.rule[' + IntToStr(Count) + ']',
      'name', rule.Attributes['id'], Exists);
    if rule.Attributes['then'] = 'true' then
      Broker.Blackboard.SetParamValue('bb.wm.rules.rule[' + IntToStr(Count) +
        ']', 'oznach', '0', Exists)
    else
      Broker.Blackboard.SetParamValue('bb.wm.rules.rule[' + IntToStr(Count) +
        ']', 'oznach', '1', Exists)
  end;
end;

function TSolverX.GetExplData: WideString;
var
  // Trace: IXMLDocument;
  Step, n, F: IXMLNode;
begin
  // ��������� ��������� ��������
  n := ExplData.DocumentElement.AddChild('initial-situation');
  // ��������� ��������� �������� � ������ Configuration
  F := ConfigDoc.DocumentElement.ChildNodes['setup'];
  case Debugger.Solver.Configuration.Style of
    isForward:
      F.Attributes['Style'] := 'forward';
    isBackward:
      F.Attributes['Style'] := 'backward';
    isMixed:
      F.Attributes['Style'] := 'mixed';
  end;
  if Debugger.Solver.Configuration.Subdialogs then
    F.Attributes['Subdialogs'] := 'true'
  else
    F.Attributes['Subdialogs'] := 'false';
  if Debugger.Solver.Configuration.Authentic then
    F.Attributes['Authentic'] := 'true'
  else
    F.Attributes['Authentic'] := 'false';
  n.ChildNodes.Add(F.CloneNode(true));

  // ��������� ����� � ����
  n.ChildNodes.Add(InitialSituation.DocumentElement.CloneNode(true));
  // ��������� ������
  ExplData.DocumentElement.ChildNodes.Add
    (Debugger.Solver.trace.DocumentElement.CloneNode(true));
  {
    Trace := TXMLDocument.Create(NIL); // �������� �� ��� ����� � ESKernel
    Trace.LoadFromXML(ESKernel.GetTrace); // <step Obj='n_obj' Attr='n_attr' Rule='n_rule' />
    N := ExplData.DocumentElement.AddChild('trace');
    for i:=0 to Trace.DocumentElement.ChildNodes.Count-1 do begin
    Step := Trace.DocumentElement.ChildNodes[i];
    Obj := Step.Attributes['Obj'];
    Attr := Step.Attributes['Attr'];
    Value := ESKernel.GetAttrValueCF(Obj, Attr, CF, CF2, Acc);

    // �������� �������, ���� ��� ����������� ( > -1)
    if Step.Attributes['Rule'] > -1 then begin // ���� ������� ����������� � �� �������
    ESKernel.RuleToXML(Step.Attributes['Rule'], Step.Attributes['Oznach'], N.AddChild('rule'));
    end;

    // �������� ����� ����
    if Step.Attributes['Rule'] = -1 then begin
    F := N.AddChild('question');
    F.Attributes['text'] := Step.Attributes['Question'];
    end
    else begin
    F := N.AddChild('fact');
    end;
    F.Attributes['object'] := Obj;
    F.Attributes['attribute'] := Attr;
    F.Attributes['value'] := value;
    F.Attributes['belief'] := CF;
    F.Attributes['maxbelief'] := CF2;
    F.Attributes['accuracy'] := Acc;
    end;
  }
  Result := ExplData.DocumentElement.XML;
end;

procedure TSolverX.ConfigureSolver;
begin
  Debugger.DebugForm.AConfigurateExecute(nil);
end;

initialization

TAutoObjectFactory.Create(ComServer, TSolverX, Class_SolverX, ciMultiInstance,
  tmApartment);

end.
