unit SolverX_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 14.06.2023 20:21:27 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Users\AILAB\Downloads\ies-components-master\USP\SolverX\SolverX (1)
// LIBID: {177DE904-FD06-4984-843F-55E079F983C4}
// LCID: 0
// Helpfile:
// HelpString: SolverX Library
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// Errors:
//   Hint: TypeInfo 'SolverX' changed to 'SolverX_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SolverXMajorVersion = 1;
  SolverXMinorVersion = 0;

  LIBID_SolverX: TGUID = '{177DE904-FD06-4984-843F-55E079F983C4}';

  IID_ISolver: TGUID = '{8E967341-7F28-48D7-958A-3A12C18780AF}';
  IID_IESComponent: TGUID = '{24AA3E90-4651-4CAE-8859-5159DCC0A47A}';
  CLASS_SolverX_: TGUID = '{548DBDCF-7829-444F-91A3-77EDD2226A7A}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ISolver = interface;
  ISolverDisp = dispinterface;
  IESComponent = interface;
  IESComponentDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  SolverX_ = IESComponent;


// *********************************************************************//
// Interface: ISolver
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E967341-7F28-48D7-958A-3A12C18780AF}
// *********************************************************************//
  ISolver = interface(IDispatch)
    ['{8E967341-7F28-48D7-958A-3A12C18780AF}']
  end;

// *********************************************************************//
// DispIntf:  ISolverDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E967341-7F28-48D7-958A-3A12C18780AF}
// *********************************************************************//
  ISolverDisp = dispinterface
    ['{8E967341-7F28-48D7-958A-3A12C18780AF}']
  end;

// *********************************************************************//
// Interface: IESComponent
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24AA3E90-4651-4CAE-8859-5159DCC0A47A}
// *********************************************************************//
  IESComponent = interface(IDispatch)
    ['{24AA3E90-4651-4CAE-8859-5159DCC0A47A}']
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString;
                             var Output: OleVariant); safecall;
    procedure Stop; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IESComponentDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {24AA3E90-4651-4CAE-8859-5159DCC0A47A}
// *********************************************************************//
  IESComponentDisp = dispinterface
    ['{24AA3E90-4651-4CAE-8859-5159DCC0A47A}']
    procedure Configurate(const Config: WideString); dispid 1;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString;
                             var Output: OleVariant); dispid 2;
    procedure Stop; dispid 3;
    property Name: WideString dispid 4;
    property Broker: OleVariant dispid 5;
  end;

// *********************************************************************//
// The Class CoSolverX_ provides a Create and CreateRemote method to
// create instances of the default interface IESComponent exposed by
// the CoClass SolverX_. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoSolverX_ = class
    class function Create: IESComponent;
    class function CreateRemote(const MachineName: string): IESComponent;
  end;

implementation

uses System.Win.ComObj;

class function CoSolverX_.Create: IESComponent;
begin
  Result := CreateComObject(CLASS_SolverX_) as IESComponent;
end;

class function CoSolverX_.CreateRemote(const MachineName: string): IESComponent;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SolverX_) as IESComponent;
end;

end.

