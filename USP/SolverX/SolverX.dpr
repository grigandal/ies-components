library SolverX;

{******************************************************************************
 * ActiveX ���������� SolverX �������� COM-�������� ��� �������������� ��������.
 * �������� ������������ ����� ������ ������������� � ��������� ����������,
 * ������������� �� IDispatch.
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 29 ������, 2003 >>
 ******************************************************************************}

uses
  ComServ,
  SolverXLib_TLB in 'SolverXLib_TLB.pas',
  SolverImpl in 'SolverImpl.pas' {SolverX: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
