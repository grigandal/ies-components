from at40_kb_interpriter import KBLexer, KBParser
from tkinter import messagebox as mb
from wait_dialog import run_func_with_loading_popup
import sys, datetime, os


def parse_kb(open_path, save_path, enc='utf-8'):
    try:
        text = open(open_path, encoding=enc).read()
        lexer = KBLexer().build()
        p = KBParser()
        parser = p.build()
        tokens = lexer.lex(text)
        parsed = parser.parse(tokens)
        p.check_inferred()
        tXML = p.types.XML
        cXML = f'<classes>{str().join([p.classes.get_class_by_name(n).XML for n in p.classes.all_names()] + [p.world.XML])}</classes>'
        kb_xml = f'<?xml version="1.0" encoding="UTF-8"?>\n<knowledge-base creation-date="{str(datetime.datetime.now())[:-7]}">{tXML + cXML}\n</knowledge-base>'
        mb.showinfo('saving', save_path)
        with open(save_path,'w', encoding='utf-8') as save_file:
            save_file.write(kb_xml)
            save_file.close()
        os._exit(0)
    except Exception as e:
        mb.showerror('KB Interprite Error', str(e))
        os._exit(0)


if __name__ == '__main__':
    print("ok")
    open_path = 'E:\Загрузки\Оболочка\УЗД Прототипы\DiagnosisMG\CancerConverted.kbs'
    if '-o' in sys.argv:
        open_path = sys.argv[sys.argv.index('-o') + 1]

    save_path = 'E:\Загрузки\Оболочка\УЗД Прототипы\DiagnosisMG\~temp.xml'
    if '-s' in sys.argv:
        save_path = sys.argv[sys.argv.index('-s') + 1]
        print("save_path")
        print (save_path)

    enc = 'utf-8'
    if '-e' in sys.argv:
        enc = sys.argv[sys.argv.index('-e') + 1]

    run_func_with_loading_popup(parse_kb, 'Interpriting knowlage base', 'Wait...', 9, 200, open_path, save_path, enc=enc)
