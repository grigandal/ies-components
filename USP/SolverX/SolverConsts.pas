unit SolverConsts;

interface

const
  // ��������� ��� ������� � ������ �� Blackboard
  BBFactsPath = 'bb.wm.facts';
  BBFact = 'fact';
  BBFacts = BBFactsPath+'.'+BBFact;
  BBGoalsPath = 'bb.wm.goals';
  BBGoal = 'goal';
  BBGoals = BBGoalsPath +'.'+BBGoal;
  BBAttrName = 'AttrPath';
  BBAttrValue = 'Value';
  BBBelief = 'Belief';
  BBMaxBelief = 'MaxBelief';
  BBAccuracy = 'Accuracy';
  BBAsked = 'Asked';
  BBStatus = 'Status';

  // ��������� ��� ��������� ���������������� ����������
  cSubdialogsOption     = 'Subdialogs';
  cAuthenticOption      = 'Authentic';
  cStyleOption          = 'Style';
  cForwardStyle         = 'Forward';
  cBackwardStyle        = 'Backward';
  cMixedStyle           = 'Mixed';
  cTrue                 = 'true';
  cFalse                = 'false';
  cOn                   = 'on';
  cOff                  = 'off';
  cBayes                = 'Bayes';
  cDempster             = 'Dempster';
  cNone                 = 'None';


implementation

end.
