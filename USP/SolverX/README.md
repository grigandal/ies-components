# АТ-РЕШАТЕЛЬ

## Настройка среды

1. Запускаем RAD Studio и открываем в ней SolverX.dpr или SolverX.dproj

2. В панели меню открываем `Tools->Options`, в открывшемся диалоге в левом меню переходим в раздел `IDE->Environment Variables` и добавляем новую пользовательскую переменную, нажав `New...`

<details>
  <summary>Изображения к 2</summary>

![EVD](./README/EnvVarDialog.png)
</details>
<br>

3. В открывшемся поддиалоге укажите название переменной `ProjDir`, а как значение укажите корневую папку всех компонентов. В моем случае, если данный проект находится в папке `E:\Delphi\ES AT\USP\SolverX`, то мне необходимо как значение указать `E:\Delphi\ES AT`

<details>
  <summary>Изображения к 3</summary>

![DVD](./README/DirVariableDialog.png)

</details>
<br>

4. В том же `Tools->Options` в левом меню переходим в раздел `Language->Delphi->Library`, и нажимаем `...` для редактирования `Library Path`. В открывшемся диалоге по очереди прописываем следующие пути и добавляем их с помощью `Add`: 
    
    - $(ProjDir)\COMSource\CONS95
    - $(ProjDir)\USP\Containers
    - $(ProjDir)\COMSource\KB_TOOLS
    - $(ProjDir)\USP\Debug

<details>
  <summary>Изображения к 4</summary>

![LPE](./README/LibPathEdit.png)

![LPD](./README/LibPathDialog.png)

![LPN](./README/LibPathDone.png)

</details>
<br>

5. Проделываем то же самое для `Browsering Path`

<details>
  <summary>Изображения к 5</summary>

![BPE](./README/BrPathEdit.png)

![BPD](./README/BrPathDialog.png)

</details>
<br>

6. Теперь необходимо установить BDE. Для этого переходим в [эту папку](/BDE_Installer), выбираем установщик BDE для своей версии и устанавливаем. Потом переходим в RAD Studio, в панели меню открываем `Component->Install Packages` и в открывшемся диалоге нажимаем Add. Выбираем файл `dclbde***.bpl` где вместо `***` стоят цифры (у меня 260, могут быть другие). Обычно этот файл находится в папке `C:\Program Files (x86)\Embarcadero\Studio\XX.X\bin`, где `XX.X` - номер версии. После того, как выбрали файл, убедимся, что галочка на `Embarcadero BDE DB Components` активна.

<details>
  <summary>Изображения к 6</summary>

![PDG](./README/PackageDialog.png)

![PDGB](./README/PackageDialogBDE.png)

</details>
<br>

## Отладка

1. В RAD Studio в панели меню собираем проект `Project->Build SolverX` и затем регистрируем ActiveX-сервер `Run->ActiveX Server->Register`. Либо открываем командную строку или PowerShell **ОТ ИМЕНИ АДМИНИСТРАТОРА**, переходим в текушую папку `cd ....` и выполняем команду `regsvr32 SolverX.dll`.

2. В панели меню открываем `Run->Parameters...` и в поле ***Host Application*** выбираем `.exe` файл любого прототипа ИЭС (например `es.exe`, `diagnosis.exe` в любой директории). Теперь при запуске `Run` будет запускаться этот прототип, и решатель можно отлаживать.