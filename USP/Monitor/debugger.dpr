program debugger;

{%ToDo 'debugger.todo'}

uses
  Forms,
  KB_Common,
  IE_Debugger;

{$R *.res}

var
  Dbg: TDebugger;

begin
  Application.Initialize;
  ErrorHandler := TErrorHandler.Create(Application);   // �������� ���������� ������
  ErrorHandler.Silent := true;
  ErrorHandler.InitLog;

  Dbg := TDebugger.Create(Application);
  Dbg.Name := 'Debugger';
  Dbg.Show;

  Application.Run;

  Dbg.Free;
  ErrorHandler.Free;
end.
