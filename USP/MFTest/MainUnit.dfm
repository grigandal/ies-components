object MainForm: TMainForm
  Left = 195
  Top = 107
  Width = 782
  Height = 525
  ActiveControl = LoadB
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1085#1072#1076' '#1092#1091#1085#1082#1094#1080#1103#1084#1080' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 346
    Width = 56
    Height = 13
    Caption = #1040#1088#1075#1091#1084#1077#1085#1090#1099
  end
  object Label2: TLabel
    Left = 8
    Top = 378
    Width = 52
    Height = 13
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
  end
  object Label3: TLabel
    Left = 264
    Top = 442
    Width = 70
    Height = 13
    Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
  end
  object Label4: TLabel
    Left = 8
    Top = 410
    Width = 25
    Height = 13
    Caption = #1060#1055'1'
  end
  object Label5: TLabel
    Left = 8
    Top = 442
    Width = 25
    Height = 13
    Caption = #1060#1055'2'
  end
  object Label6: TLabel
    Left = 264
    Top = 376
    Width = 67
    Height = 13
    Caption = #1059#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
  end
  object Label7: TLabel
    Left = 264
    Top = 408
    Width = 47
    Height = 13
    Caption = #1058#1086#1095#1085#1086#1089#1090#1100
  end
  object Label8: TLabel
    Left = 264
    Top = 344
    Width = 32
    Height = 13
    Caption = #1063#1080#1089#1083#1086
  end
  object SourceChart: TChart
    Left = 8
    Top = 8
    Width = 385
    Height = 321
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      #1048#1089#1093#1086#1076#1085#1099#1077' '#1092#1091#1085#1082#1094#1080#1080' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080)
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.ExactDateTime = False
    LeftAxis.Increment = 0.1
    LeftAxis.Maximum = 1
    View3D = False
    Color = clWhite
    TabOrder = 0
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Title = #1060#1055'1'
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      Title = #1060#1055'2'
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object DestChart: TChart
    Left = 408
    Top = 8
    Width = 361
    Height = 321
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      #1056#1077#1079#1091#1083#1100#1090#1080#1088#1091#1102#1097#1072#1103' '#1092#1091#1085#1082#1094#1080#1103' '#1087#1088#1080#1085#1072#1076#1083#1077#1078#1085#1086#1089#1090#1080)
    BottomAxis.ExactDateTime = False
    BottomAxis.Increment = 1
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.ExactDateTime = False
    LeftAxis.Increment = 0.1
    LeftAxis.Maximum = 1
    View3D = False
    Color = clWhite
    TabOrder = 1
    object Series3: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clTeal
      Title = #1060#1055
      LinePen.Width = 2
      Pointer.InflateMargins = True
      Pointer.Style = psCircle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object LoadB: TButton
    Left = 184
    Top = 344
    Width = 65
    Height = 22
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
    Default = True
    TabOrder = 2
    OnClick = LoadBClick
  end
  object MFEdit: TEdit
    Left = 72
    Top = 344
    Width = 105
    Height = 21
    TabOrder = 3
    Text = 'args.xml'
  end
  object OpRG: TRadioGroup
    Left = 408
    Top = 336
    Width = 361
    Height = 129
    Caption = #1054#1087#1077#1088#1072#1094#1080#1103
    Columns = 4
    Items.Strings = (
      'AND'
      'OR'
      'Logic EQ'
      'UpTrunc'
      'LowTrunc'
      #1057#1083#1086#1078#1077#1085#1080#1077
      #1042#1099#1095#1080#1090#1072#1085#1080#1077
      #1059#1084#1085#1086#1078#1077#1085#1080#1077
      #1044#1077#1083#1077#1085#1080#1077
      '='
      '<>'
      '<'
      '<='
      '>'
      '>='
      #1056#1072#1079#1084#1099#1090#1080#1077
      #1052#1072#1089#1096#1090#1072#1073
      #1057#1076#1074#1080#1075
      #1060#1072#1079#1079#1080#1092'.')
    TabOrder = 4
    OnClick = OpRGClick
  end
  object ResEdit: TEdit
    Left = 72
    Top = 376
    Width = 105
    Height = 21
    TabOrder = 5
    Text = 'res.xml'
  end
  object NegMF1: TButton
    Left = 184
    Top = 408
    Width = 65
    Height = 21
    Caption = #1054#1073#1088#1072#1090#1080#1090#1100
    TabOrder = 6
    OnClick = NegMF1Click
  end
  object NegMF2: TButton
    Left = 184
    Top = 440
    Width = 65
    Height = 21
    Caption = #1054#1073#1088#1072#1090#1080#1090#1100
    TabOrder = 7
    OnClick = NegMF2Click
  end
  object CoefEdit: TEdit
    Left = 344
    Top = 440
    Width = 49
    Height = 21
    TabOrder = 8
    Text = '0.7'
  end
  object DefuzzB: TButton
    Left = 696
    Top = 472
    Width = 73
    Height = 21
    Caption = 'Defuzzificate'
    TabOrder = 9
    OnClick = DefuzzBClick
  end
  object Arg1CB: TComboBox
    Left = 48
    Top = 408
    Width = 129
    Height = 21
    ItemHeight = 13
    TabOrder = 10
    OnChange = Arg1CBChange
  end
  object Arg2CB: TComboBox
    Left = 48
    Top = 440
    Width = 129
    Height = 21
    ItemHeight = 13
    TabOrder = 11
    OnChange = Arg2CBChange
  end
  object CFEdit: TEdit
    Left = 344
    Top = 376
    Width = 49
    Height = 21
    TabOrder = 12
    Text = '95'
  end
  object AccEdit: TEdit
    Left = 344
    Top = 408
    Width = 49
    Height = 21
    TabOrder = 13
    Text = '99'
  end
  object ValueEdit: TEdit
    Left = 344
    Top = 344
    Width = 49
    Height = 21
    TabOrder = 14
    Text = '5'
  end
end
