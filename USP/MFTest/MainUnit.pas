unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart,
  XMLIntf, XMLDoc,
  KB_Fuzzy, KB_Containers, KB_Values, KB_Types, KB_Common;

type
  TMainForm = class(TForm)
    SourceChart: TChart;
    DestChart: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    LoadB: TButton;
    MFEdit: TEdit;
    OpRG: TRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    ResEdit: TEdit;
    NegMF1: TButton;
    NegMF2: TButton;
    CoefEdit: TEdit;
    Label3: TLabel;
    DefuzzB: TButton;
    Arg1CB: TComboBox;
    Arg2CB: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CFEdit: TEdit;
    AccEdit: TEdit;
    Label8: TLabel;
    ValueEdit: TEdit;
    procedure LoadBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OpRGClick(Sender: TObject);
    procedure NegMF1Click(Sender: TObject);
    procedure NegMF2Click(Sender: TObject);
    procedure DefuzzBClick(Sender: TObject);
    procedure Arg1CBChange(Sender: TObject);
    procedure Arg2CBChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    mf1,mf2,res: TMembershipFunction;
    lp: TFuzzyType;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  res := TMembershipFunction.Create(self);
  lp := TFuzzyType.Create(self);
end;

procedure TMainForm.LoadBClick(Sender: TObject);
var
  Doc: IXMLDocument;
  i,j: integer;
  n: IXMLNode;
  pt: TFuzzyPoint;
  mf: TMembershipFunction;
begin
  DecimalSeparator := '.';
  // �� XML ��������
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromFile(MFEdit.Text);

  Arg1CB.Items.Clear;
  Arg2CB.Items.Clear;
  lp.MFList.Clear;

  for j:=0 to Doc.DocumentElement.ChildNodes.Count-1 do begin
    n := Doc.DocumentElement.ChildNodes[j];
    mf := TMembershipFunction.Create(self);
    for i:=0 to n.ChildNodes.Count-1 do begin
      pt := TFuzzyPoint.Create(nil);
      pt.X := StrToFloat(n.ChildNodes[i].Attributes['x']);
      pt.Y := StrToFloat(n.ChildNodes[i].Attributes['y']);
      mf.Points.Add(pt);
    end;
    lp.MFList.Add(mf);
    Arg1CB.Items.Add('MF'+IntToStr(j));
    Arg2CB.Items.Add('MF'+IntToStr(j));
  end;

  if lp.MFList.Count > 0 then begin
    Arg1CB.ItemIndex := 0;
    Arg1CBChange(self);
  end;

  if lp.MFList.Count > 1 then begin
    Arg2CB.ItemIndex := 1;
    Arg2CBChange(self);
  end;
end;

procedure TMainForm.OpRGClick(Sender: TObject);
var
  i: integer;
  Doc: IXMLDocument;
  value: TValue;
  r1, r2: TMembershipFunction;
begin
  if (OpRg.ItemIndex < 9) or (OpRg.ItemIndex > 14) then
    DestChart.Series[0].Clear;

  case OpRG.ItemIndex of
  0: // AND
    begin
      res := mf1.Conjunction(mf2);
    end;
  1: // OR
    begin
      res := mf1.Disjunction(mf2);
    end;
  2: // Logic EQ = a & b + ~a & ~b
    begin
      mf2.Negate;
      mf1.Negate;
      r1 := mf1.Conjunction(mf2);
      mf2.Negate;
      mf1.Negate;
      r2 := mf1.Conjunction(mf2);
      res := r1.Disjunction(r2);
    end;
  3: // ������� ��������
    begin
      res := mf1.CreateCopy;
      res.YTopTruncateBy(StrToFloat(CoefEdit.Text));
    end;
  4: // ������ ��������
    begin
      res := mf1.CreateCopy;
      res.YBottomTruncateBy(StrToFloat(CoefEdit.Text));
    end;

  5: // ��������
    begin
      res := mf1.FAdd(mf2);
    end;
  6: // ���������
    begin
      res := mf1.FSub(mf2);
    end;
  7: // ���������
    begin
      res := mf1.FMul(mf2);
    end;
  8: // �������
    begin
      res := mf1.FDiv(mf2);
    end;


  9: // �����
    begin
      mf1.EvalOp(omEq, mf2, value);
    end;
  10: // �� �����
    begin
      mf1.EvalOp(omNe, mf2, value);
    end;
  11: // ������
    begin
      mf1.EvalOp(omLt, mf2, value);
    end;
  12: // ������ ��� �����
    begin
      mf1.EvalOp(omLe, mf2, value);
    end;
  13: // ������
    begin
      mf1.EvalOp(omGt, mf2, value);
    end;
  14: // ������ ��� �����
    begin
      mf1.EvalOp(omGe, mf2, value);
    end;

  15: // ��������
    begin
      res := mf1.CreateCopy;
      res.YFuzzyBy(StrToFloat(CoefEdit.Text));
    end;
  16: // ���������������
    begin
      res := mf1.CreateCopy;
      res.XScaleBy(StrToFloat(CoefEdit.Text));
    end;
  17: // �����
    begin
      res := mf1.CreateCopy;
      res.XShiftBy(StrToFloat(CoefEdit.Text));
    end;

  18: // ������������
    begin
      Value := TValue.Create(nil);
      Value.ResVal := StrToFloat(ValueEdit.Text);
      Value.Attrs['belief'] := StrToFloat(CFEdit.Text);
      Value.Attrs['accuracy'] := StrToFloat(AccEdit.Text);
      Value.Evaluated := true;
      res := TMembershipFunction.Fuzzify(Value);
    end;
  end;

  if (OpRg.ItemIndex >= 9) and (OpRg.ItemIndex <= 14) then begin
    ShowMessage(value.ResVal);
  end
  else begin
    Doc := TXMLDocument.Create(nil);
    Doc.LoadFromXML('<shell/>');
    res.XML(Doc.DocumentElement);
    Doc.LoadFromXML(Doc.DocumentElement.ChildNodes[0].XML);
    Doc.SaveToFile(ResEdit.Text);
    for i:=0 to res.Points.Count-1 do
      DestChart.Series[0].AddXY(res.GetX(i), res.GetY(i));
  end;

  OpRG.ItemIndex := -1;
end;

procedure TMainForm.NegMF1Click(Sender: TObject);
var i: integer;
begin
  mf1.Negate;
  SourceChart.Series[0].Clear;
  for i:=0 to mf1.Points.Count-1 do
    SourceChart.Series[0].AddXY(mf1.GetX(i),mf1.GetY(i));
end;

procedure TMainForm.NegMF2Click(Sender: TObject);
var i: integer;
begin
  mf2.Negate;
  SourceChart.Series[1].Clear;
  for i:=0 to mf2.Points.Count-1 do
    SourceChart.Series[1].AddXY(mf2.GetX(i),mf2.GetY(i));
end;

procedure TMainForm.Arg1CBChange(Sender: TObject);
var i: integer;
begin
  mf1.Free;
  mf1 := TMembershipFunction(lp.MFList[Arg1CB.ItemIndex]).CreateCopy;
  SourceChart.Series[0].Clear;
  for i:=0 to mf1.Points.Count-1 do
    SourceChart.Series[0].AddXY(mf1.GetX(i), mf1.GetY(i));
end;

procedure TMainForm.Arg2CBChange(Sender: TObject);
var i: integer;
begin
  mf2.Free;
  mf2 := TMembershipFunction(lp.MFList[Arg2CB.ItemIndex]).CreateCopy;
  SourceChart.Series[1].Clear;
  for i:=0 to mf2.Points.Count-1 do
    SourceChart.Series[1].AddXY(mf2.GetX(i), mf2.GetY(i));
end;

procedure TMainForm.DefuzzBClick(Sender: TObject);
var
  List: TList;
  value1, value2: TValue;
  s: string;
  i: integer;
begin
  value1 := res.Defuzzify;
  s := '  Method1: '+ #13#10+FloatToStr(value1.ResVal)+' with '+FloatToStr(value1.Attrs['belief']);
  s := s + #13#10;

  list := res.MultiDefuzzify;
  if (list <> nil) and (list.Count > 0) then begin
    s := s + '  Method2: ';
    for i:=0 to list.count-1 do begin
      value2 := list[i];
      s := s + #13#10 + FloatToStr(value2.ResVal)+' with '+FloatToStr(value2.Attrs['belief']);
    end;
  end;
  list.Free;

  ShowMessage(s);
end;

end.
