unit KBReaderImpl;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, ReadKB_TLB, StdVcl, XMLIntf, XMLDoc, SysUtils, Dialogs, Classes,
  Aspr95, KBTypes, KBCompon, DD_Sentential, DD_Tokenizer;

type
  TKBReader = class(TAutoObject, IKBRead)
  private
    fFileName: WideString;
    fKRLName: WideString;
    fXML: WideString;
  protected
    function Get_FileName: WideString; safecall;
    function Get_KRLName: WideString; safecall;
    function Get_XML: WideString; safecall;
    procedure Load; safecall;
    procedure LoadFromFile(const FileName: WideString); safecall;
    procedure Set_FileName(const Value: WideString); safecall;
    procedure Set_KRLName(const Value: WideString); safecall;
    procedure FlushStat(const FileName: WideString); safecall;
    procedure SaveToFile(const FileName: WideString); safecall;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  private
    KBControl: TKBControl;
    Doc: IXMLDocument;
    procedure ATtoXML(Root: IXMLNode);
    procedure TypesToXML(Node: IXMLNode);
    procedure ObjectsToXML(Node: IXMLNode);
    procedure ConditionToXML(list: TKBOwnedCollection; Tokens: TStrings; Node: IXMLNode; var Last: integer);
    procedure ActionToXML(list: TKBOwnedCollection; var Node: IXMLNode);
    function Inferrable(ObjInd, AttrInd: integer): boolean;
  end;

implementation

uses ComServ;


procedure TKBReader.Initialize;
begin
  inherited;
  KBControl := TKBControl.Create(nil);
  KBControl.KB := TKnowledgeBase.Create(KBControl);
  KBControl.KB.KBInterface := TKBInterface.Create(KBControl);

  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<knowledge-base />');
  Doc.Encoding := 'utf-8'; // ��� ��������� �������� �����
end;

destructor TKBReader.Destroy;
begin
  KBControl.KB.KBInterface.Free;
  KBControl.KB.Free;
  KBControl.Free;
  inherited;
end;

function TKBReader.Get_FileName: WideString;
begin
  Result := fFileName;
end;

function TKBReader.Get_KRLName: WideString;
begin
  Result := fKRLName;
end;

function TKBReader.Get_XML: WideString;
begin
  Result := fXML;
end;

procedure TKBReader.Set_FileName(const Value: WideString);
begin
  fFileName := Value;
end;

procedure TKBReader.Set_KRLName(const Value: WideString);
begin
  fKRLName := Value;
end;

procedure TKBReader.LoadFromFile(const FileName: WideString);
begin
  fFileName := FileName;
  Load;
end;

procedure TKBReader.SaveToFile(const FileName: WideString);
begin
  Doc.SaveToFile(FileName);
end;

// ��������� � XML � ������� ���
procedure TKBReader.Load;
begin
  KBControl.ClearKB(KBControl.KB.KBOwner);
  KBControl.KB.FileName := fFileName;
  KBControl.KB.Load;

  ATtoXML(Doc.DocumentElement);

  fXML := Doc.DocumentElement.XML;
end;

procedure TKBReader.ATtoXML(Root: IXMLNode);
var t: IXMLNode;
begin
  Root.Attributes['creation-date'] := DateTimeToStr(Now);
  t := Root.AddChild('problem-info');
  t.Text := KBControl.KB.ProblemName;
  // ����
  t := Root.AddChild('types');
  TypesToXML(t);
  // �������, ��������, �������
  t := Root.AddChild('classes');
  ObjectsToXML(t);
end;

procedure TKBReader.TypesToXML(Node: IXMLNode);
var
  i,j: integer;
  t: record_type;
  n,m: IXMLNode;
begin
  for i:=0 to KBControl.KB.ListTypes.Count-1 do begin
    t := record_type(KBControl.KB.ListTypes.Items[i]);
    if (t.type_znach >= 0) and (t.type_znach < 4) then begin
      n := Node.AddChild('type');
      n.Attributes['id'] := '���'+IntToStr(t.n_type);
      case t.type_znach of
      0,2:      // ����������
        begin
          n.Attributes['meta'] := 'string';
          for j := 0 to t.list_znach.Count-1 do begin
            m := n.AddChild('value');
            m.Text := value(t.list_znach.Items[j]).znach;
          end;
        end;
      1:        // ��������
        begin
          n.Attributes['meta'] := 'number';
          m := n.AddChild('from');
          m.Text := t.min_znach;
          m := n.AddChild('to');
          m.Text := t.max_znach;
        end;
      3:        // ��������������� ����������
        begin
          n.Attributes['meta'] := 'fuzzy';
          for j := 0 to t.list_znach.Count-1 do begin
            m := n.AddChild('value');
            m.Text := value(t.list_znach.Items[j]).znach;
          end;
        end;
      4:        // �������
        begin
          // �� ������� ��� ���
        end;
      end;
      n.Attributes['desc'] := t.comment;
    end;
  end;
end;

function TKBReader.Inferrable(ObjInd, AttrInd: integer): boolean;
var
  obj: record_obj;
  attr: record_attr;
  rule: record_rule;
  ass: TAssertion;
  i,j: integer;
begin
  // ���������� true ���� ������� ������� (���� � ��������� ���� �� ������ �������)
  Result := false;
  obj := record_obj(KBControl.KB.ListObjs.Items[ObjInd]);
  attr := record_attr(obj.ListAttrs.Items[AttrInd]);
  i := 0;
  while i < KBControl.KB.ListRules.Count do begin
    rule := record_rule(KBControl.KB.ListRules.Items[i]);
    // ������ � then
    j:=0;
    while j < rule.list_attr_then.Count do begin
      ass := TAssertion(rule.list_attr_then.Items[j]);
      if (ass.n_obj = obj.n_obj) and (ass.n_attr = attr.n_attr) then begin
        Result := true;
        break;
      end;
      Inc(j);
    end;
    // ������ � else, ���� �� ����� � then
    if not Result then begin
      j:=0;
      while j < rule.list_attr_else.Count do begin
        ass := TAssertion(rule.list_attr_else.Items[j]);
        if (ass.n_obj = obj.n_obj) and (ass.n_attr = attr.n_attr) then begin
          Result := true;
          break;
        end;
        Inc(j);
      end;
    end;
    if Result then break;
    Inc(i);
  end;
end;

procedure TKBReader.ObjectsToXML(Node: IXMLNode);
var
  i,j, last: integer;
  obj: record_obj;
  attr: record_attr;
  rule: record_rule;
  n,m,l,q: IXMLNode;
  Sentential: TDDSentential;
  Tokenizer: TDDTokenizer;
  prefixed: string;
begin
  Tokenizer := TDDTokenizer.Create(NIL);

  for i:=0 to KBControl.KB.ListObjs.Count-1 do begin
    obj := record_obj(KBControl.KB.ListObjs.Items[i]);
    n := Node.AddChild('class');
    n.Attributes['id'] := '�����'+IntToStr(obj.n_obj);
    n.Attributes['desc'] := obj.comment;

    // ��������
    m := n.AddChild('properties');
    for j:=0 to obj.ListAttrs.Count-1 do begin
      attr := record_attr(obj.ListAttrs.Items[j]);
      l := m.AddChild('property');
      l.Attributes['id'] := '�������'+IntToStr(attr.n_attr);
      l.Attributes['type'] := '���'+IntToStr(attr.n_type);
      l.Attributes['desc'] := attr.comment;
      if Inferrable(i,j) then // ���� ������� �������
        l.Attributes['source'] := 'inferred'
      else begin
        l.Attributes['source'] := 'question'; // �� ���������
        q := l.AddChild('question');
        q.Text := attr.comment;
      end;
    end;
  end;

  n := Node.AddChild('class');
  n.Attributes['id'] := 'world';
  n.Attributes['desc'] := '����� �������� ������, ���������� � ���� ���������� ������ ������� � ����� �������';

  // ��������
  m := n.AddChild('properties');
  for i:=0 to KBControl.KB.ListObjs.Count-1 do begin
    obj := record_obj(KBControl.KB.ListObjs.Items[i]);
    l := m.AddChild('property');
    l.Attributes['id'] := '������'+IntToStr(obj.n_obj);
    l.Attributes['type'] := '�����'+IntToStr(obj.n_obj);
    l.Attributes['source'] := 'none';
    l.Attributes['create'] := 'true';
    l.Attributes['desc'] := '��������� ������ �����'+IntToStr(obj.n_obj);
  end;

  // �������
  Sentential := TDDSentential.Create(NIL);
  Sentential.ArgPrefix := 'P';
  m := n.AddChild('rules');
  
  // ������� ������� �� kbs
  for i:=0 to KBControl.KB.ListRules.Count-1 do begin
    rule := record_rule(KBControl.KB.ListRules.Items[i]);
    l := m.AddChild('rule');
    l.Attributes['id'] := rule.name + IntToStr(rule.n_rule);
    l.Attributes['meta'] := 'simple';
    l.Attributes['desc'] := rule.comment;
    // �������� if ��������
    q := l.AddChild('condition');
    Sentential.InfixToPrefix(Rule.incident, prefixed); // D K N P1 D P2 P3 N P4
    Tokenizer.Text := prefixed;
    Tokenizer.Tokenize;
    last := 0; // ��������� ������������ �����
    ConditionToXML(rule.list_attr_if, Tokenizer.Tokens, q, last);

    // �������� then ����������
    q := l.AddChild('action');
    ActionToXML(rule.list_attr_then, q);
    // �������� else ����������
    if rule.list_attr_else.Count > 0 then begin
      q := l.AddChild('else-action');
      ActionToXML(Rule.list_attr_else, q);
    end;
  end;

  // ������  // � AT ������ ���� ���
  m := n.AddChild('methods');

  Sentential.Free;
  Tokenizer.Free;
end;

procedure TKBReader.ConditionToXML(list: TKBOwnedCollection; Tokens: TStrings; Node: IXMLNode; var Last: integer);
var
  n, q, r: IXMLNode;
  Tok: string;
  Ass: TAssertion;
begin
  if Tokens[Last][1] = 'K' then begin
    n := Node.AddChild('and');
    Inc(Last);
    ConditionToXML(list, Tokens, n, Last);
    ConditionToXML(list, Tokens, n, Last);
  end
  else if Tokens[Last][1] = 'D' then begin
    n := Node.AddChild('or');
    Inc(Last);
    ConditionToXML(list, Tokens, n, Last);
    ConditionToXML(list, Tokens, n, Last);
  end
  else if Tokens[Last][1] = 'X' then begin
    n := Node.AddChild('xor');
    Inc(Last);
    ConditionToXML(list, Tokens, n, Last);
    ConditionToXML(list, Tokens, n, Last);
  end
  else if Tokens[Last][1] = 'N' then begin
    n := Node.AddChild('not');
    Inc(Last);
    ConditionToXML(list, Tokens, n, Last);
  end
  else if Tokens[Last][1] = 'P' then begin
    // ����������� �����������
    Tok := Tokens[Last];
    Ass := TAssertion(list.Items[StrToInt(Copy(Tok,2,Length(Tok)-1))-1]);
    case Ass.predicat of
    0: n := Node.AddChild('eq');
    1: n := Node.AddChild('gt');
    2: n := Node.AddChild('lt');
    3: n := Node.AddChild('ge');
    4: n := Node.AddChild('le');
    5: n := Node.AddChild('ne');
    end;
    // ���� � ��������
    q := n.AddChild('ref');
    q.Attributes['id'] := '������'+IntToStr(ass.n_obj);
    r := q.AddChild('ref');
    r.Attributes['id'] := '�������'+IntToStr(ass.n_attr);
    // ��������
    q := n.AddChild('value');
    q.Text := Ass.znach;
    // ��������
    q := n.AddChild('with');
    q.Attributes['belief'] := Ass.factor1;
    q.Attributes['probability'] := Ass.factor2;
    q.Attributes['accuracy'] := Ass.accuracy;
    Inc(Last);
  end;
end;

procedure TKBReader.ActionToXML(list: TKBOwnedCollection; var Node: IXMLNode);
var
  i: integer;
  ass: TAssertion;
  n,p,q: IXMLNode;
begin
  for i:=0 to list.Count-1 do begin
    ass := TAssertion(list.Items[i]);
    n := Node.AddChild('assign');
    // ���� � ��������
    p := n.AddChild('ref');
    p.Attributes['id'] := '������'+IntToStr(ass.n_obj);
    q := p.AddChild('ref');
    q.Attributes['id'] := '�������'+IntToStr(ass.n_attr);
    // ��� �����������
    p := n.AddChild('value');
    p.Text := ass.znach;
    // �������� ������������
    p := n.AddChild('with');
    p.Attributes['belief'] := ass.factor1;
    p.Attributes['probability'] := ass.factor2;
    p.Attributes['accuracy'] := ass.accuracy;
  end;
end;

procedure TKBReader.FlushStat(const FileName: WideString);
var
  f: Text;
  r: record_rule;
  ass: TAssertion;
  obj: record_obj;
  attr: record_attr;
  tp: record_type;
  i,j,k,m: integer;
begin
  KBControl.ClearKB(KBControl.KB.KBOwner);
  KBControl.KB.FileName := fFileName;
  KBControl.KB.Load;

  AssignFile(f, FileName);
  Rewrite(f);

  Writeln(f, 'Obj; Attr; Value; Type; TypeCount');

  // �������� �� ��������� � ��������� ��� ������� ��� ����������� � ��������
  for i:=0 to KBControl.KB.ListRules.Count-1 do begin
    r := record_rule(KBControl.KB.ListRules.Items[i]);
    for j:=0 to r.list_attr_if.Count-1 do begin
      ass := TAssertion(r.list_attr_if.Items[j]);
      // ������ ������ � �������
      for k:=0 to KBControl.KB.ListObjs.Count-1 do begin
        obj := record_obj(KBControl.KB.ListObjs.Items[k]);
        if obj.n_obj = ass.n_obj then begin
          for m:=0 to obj.ListAttrs.Count-1 do begin
            attr := record_attr(obj.ListAttrs.Items[m]);
            if attr.n_attr = ass.n_attr then begin
              tp := attr.PtrType;          
              Writeln(f, IntToStr(ass.n_obj), '; ', IntToStr(ass.n_attr), '; ', ass.znach, '; ', IntToStr(attr.n_type),'; ',IntToStr(tp.list_znach.Count));
              break;
            end;
          end;
        end;
      end;
    end;
  end;

  CloseFile(f);
end;


initialization
  TAutoObjectFactory.Create(ComServer, TKBReader, Class_KBReader,
    ciMultiInstance, tmApartment);
end.
