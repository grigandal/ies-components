unit ReadKB_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 04.04.2007 13:12:54 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Dd\Delphi\USP\KBReader\ReadKB.tlb (1)
// LIBID: {C829FFB3-CF06-485A-9418-B83B7D97F570}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ReadKBMajorVersion = 1;
  ReadKBMinorVersion = 0;

  LIBID_ReadKB: TGUID = '{C829FFB3-CF06-485A-9418-B83B7D97F570}';

  IID_IKBRead: TGUID = '{2B9953E6-3295-492F-A2EB-B4AB987C8782}';
  CLASS_KBReader: TGUID = '{ADE1B0BD-0B9A-4B67-8442-778C8681E372}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IKBRead = interface;
  IKBReadDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  KBReader = IKBRead;


// *********************************************************************//
// Interface: IKBRead
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2B9953E6-3295-492F-A2EB-B4AB987C8782}
// *********************************************************************//
  IKBRead = interface(IDispatch)
    ['{2B9953E6-3295-492F-A2EB-B4AB987C8782}']
    function  Get_KRLName: WideString; safecall;
    procedure Set_KRLName(const Value: WideString); safecall;
    function  Get_FileName: WideString; safecall;
    procedure Set_FileName(const Value: WideString); safecall;
    function  Get_XML: WideString; safecall;
    procedure Load; safecall;
    procedure LoadFromFile(const FileName: WideString); safecall;
    procedure FlushStat(const FileName: WideString); safecall;
    procedure SaveToFile(const FileName: WideString); safecall;
    property KRLName: WideString read Get_KRLName write Set_KRLName;
    property FileName: WideString read Get_FileName write Set_FileName;
    property XML: WideString read Get_XML;
  end;

// *********************************************************************//
// DispIntf:  IKBReadDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2B9953E6-3295-492F-A2EB-B4AB987C8782}
// *********************************************************************//
  IKBReadDisp = dispinterface
    ['{2B9953E6-3295-492F-A2EB-B4AB987C8782}']
    property KRLName: WideString dispid 1;
    property FileName: WideString dispid 2;
    property XML: WideString readonly dispid 3;
    procedure Load; dispid 4;
    procedure LoadFromFile(const FileName: WideString); dispid 5;
    procedure FlushStat(const FileName: WideString); dispid 6;
    procedure SaveToFile(const FileName: WideString); dispid 7;
  end;

// *********************************************************************//
// The Class CoKBReader provides a Create and CreateRemote method to          
// create instances of the default interface IKBRead exposed by              
// the CoClass KBReader. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoKBReader = class
    class function Create: IKBRead;
    class function CreateRemote(const MachineName: string): IKBRead;
  end;

implementation

uses ComObj;

class function CoKBReader.Create: IKBRead;
begin
  Result := CreateComObject(CLASS_KBReader) as IKBRead;
end;

class function CoKBReader.CreateRemote(const MachineName: string): IKBRead;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_KBReader) as IKBRead;
end;

end.
