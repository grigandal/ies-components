library ReadKB;

uses
  ComServ,
  ReadKB_TLB in 'ReadKB_TLB.pas',
  KBReaderImpl in 'KBReaderImpl.pas' {KBReader: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
