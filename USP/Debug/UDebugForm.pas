unit UDebugForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, VCL.Graphics, VCL.Controls, VCL.Forms,
  VCL.Dialogs, VCL.Menus, VCL.ToolWin, VCL.ComCtrls, VCL.ImgList, VCL.ActnList, VCL.Buttons, VCL.Grids,
  VCL.ExtCtrls, VCL.StdCtrls, XMLDoc, XMLIntf,
  OM_Containers, KB_Common, KB_Containers, KB_Values, KB_Types,
  UEditFactForm, VCL.CheckLst, System.ImageList, System.Actions;

const
  strKBLoaded = '���� ������ ���������';
  strKBNotLoaded = '���� ������ �� ���������';
  strInProcess = '����������� �����';
  strStopped = '����� ����������';
  strInited = '����� ���������������';

type
  TDebugForm = class(TForm)
    SB: TStatusBar;
    ToolBar1: TToolBar;
    MainMenu: TMainMenu;
    FileMenuItem: TMenuItem;
    LoadKBItem: TMenuItem;
    QuitItem: TMenuItem;
    RunMenuItem: TMenuItem;
    StepItItem: TMenuItem;
    N6: TMenuItem;
    ClearWMItem: TMenuItem;
    RollBackItem: TMenuItem;
    ActionList: TActionList;
    MenuIL: TImageList;
    ALoadKB: TAction;
    AQuit: TAction;
    AClearWM: TAction;
    AStepIt: TAction;
    ARollBack: TAction;
    N9: TMenuItem;
    OpenDlg: TOpenDialog;
    ASaveWM: TAction;
    ALoadWM: TAction;
    SaveWMItem: TMenuItem;
    LoadWMItem: TMenuItem;
    QuitSB: TSpeedButton;
    ToolButton2: TToolButton;
    LoadKBSB: TSpeedButton;
    ClearWMSB: TSpeedButton;
    StepItSB: TSpeedButton;
    RollBackSB: TSpeedButton;
    ToolButton3: TToolButton;
    SaveWMSB: TSpeedButton;
    LoadWMSB: TSpeedButton;
    AStop: TAction;
    StopItem: TMenuItem;
    ARun: TAction;
    RunItem: TMenuItem;
    RunSB: TSpeedButton;
    StopSB: TSpeedButton;
    HelpMenuItem: TMenuItem;
    AHelp: TAction;
    HelpItem: TMenuItem;
    HelpSB: TSpeedButton;
    ToolButton1: TToolButton;
    Timer: TTimer;
    AEditWM: TAction;
    AConfigurate: TAction;
    ConfigSolverItem: TMenuItem;
    PageControl: TPageControl;
    OMSheet: TTabSheet;
    TraceSheet: TTabSheet;
    LogGB: TGroupBox;
    TraceMemo: TMemo;
    PanelSmall: TPanel;
    VS: TSplitter;
    PropertiesGB: TGroupBox;
    WMSG: TStringGrid;
    WMGB: TGroupBox;
    WMTV: TTreeView;
    CRSSheet: TTabSheet;
    CGSSheet: TTabSheet;
    SetCLB: TCheckListBox;
    CSRLabel: TLabel;
    procedure ALoadKBExecute(Sender: TObject);
    procedure AQuitExecute(Sender: TObject);
    procedure AClearWMExecute(Sender: TObject);
    procedure AStepItExecute(Sender: TObject);
    procedure ARollBackExecute(Sender: TObject);
    procedure ASaveWMExecute(Sender: TObject);
    procedure ALoadWMExecute(Sender: TObject);
    procedure AStopExecute(Sender: TObject);
    procedure ARunExecute(Sender: TObject);
    procedure AHelpExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure WMTVChange(Sender: TObject; Node: TTreeNode);
    procedure AEditWMExecute(Sender: TObject);
    procedure AConfigurateExecute(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure WMTVDblClick(Sender: TObject);
    procedure WMSGKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    // ������
    KBLoaded: boolean;  // ��������� �� ��
    InProcess: boolean; // ���� �� ������ �����
    // ��������� ��������� ������� ������
    LastMemState: WideString;
  public
    procedure LoadKB(KBFileName: string);
    procedure FillControlsForScreenShot;
    procedure UpdateOMSheet;      // ���������� ���������� ������� ������
    procedure UpdateCRSSheet;     // ���������� ����������� ����� ������
    procedure UpdateTraceSheet;   // ���������� ������ ������
  end;

//var
//  DebugForm: TDebugForm;

implementation

uses IE_Debugger, UConfigSolverForm, IE_Solver, KB_Instructions;

{$R *.dfm}


procedure TDebugForm.AQuitExecute(Sender: TObject);
var s: string;
begin
  if InProcess then begin
    s := '�� �������, ��� ������ ���������� ����� � ��������� ������?';
    if MessageDlg(s, mtConfirmation, [mbYes, mbNo], 0) = mrNo
    then Exit;
  end;

  Close;
end;


procedure TDebugForm.FormShow(Sender: TObject);
begin
  SB.Panels[0].Text := strKBNotLoaded;
  SB.Panels[1].Text := '';
  SB.Panels[2].Text := DateToStr(Date);

  WMSG.ColWidths[0] := 20; // ��� ��������� ��������
  WMSG.Cells[1,0] := '��������';
  WMSG.ColWidths[1] := 90;
  WMSG.Cells[2,0] := '��������';
  WMSG.ColWidths[2] := 150;
  WMSG.Cells[3,0] := 'Bel';
  WMSG.ColWidths[3] := 40;
  WMSG.Cells[4,0] := 'Pl';
  WMSG.ColWidths[4] := 40;
  WMSG.Cells[5,0] := 'Acc';
  WMSG.ColWidths[5] := 40;

  //FillControlsForScreenShot;
end;


procedure TDebugForm.TimerTimer(Sender: TObject);
begin
  SB.Panels[2].Text := TimeToStr(Time);
end;


procedure TDebugForm.LoadKB(KBFileName: string);
begin
  with Owner as TDebugger do
  try
    Solver.KBFileName := KBFileName;
    Solver.Loader.LoadFile(Solver.KB, Solver.KBFileName);
  except
    ShowMessage('Debugger: ������ ��� �������� ��');
  end;

  try
    (Owner as TDebugger).Solver.Init;
    // �������� ������
    AStepIt.Enabled := true;
    ARun.Enabled := true;
    ARollBack.Enabled := false;
    AStop.Enabled := false;
    ALoadWM.Enabled := true;
    SB.Panels[0].Text := strKBLoaded;
    SB.Panels[1].Text := strInited; // ���� ����������������
    KBLoaded := true;
    
    // �������� ��� ��������
    UpdateOMSheet;
  except
    ShowMessage('Debugger: ������ ��� ������������� ��������');
  end;
end;

procedure TDebugForm.ALoadKBExecute(Sender: TObject);
var s: string;
begin
  if InProcess then begin
    s := '�� �������, ��� ������ ���������� ����� � ��������� ���� ������?';
    if MessageDlg(s, mtConfirmation, [mbYes, mbNo], 0) = mrNo
    then Exit;
  end;

  // �������� ���� ������

  if OpenDlg.Execute then LoadKB(OpenDlg.FileName);
end;


procedure TDebugForm.AClearWMExecute(Sender: TObject);
begin
  // ������� ������� ������ � �� �������������
  with Owner as TDebugger do Solver.Init;
  SB.Panels[1].Text := strInited;

  // ����� ���� �������� �������� ������� ������
  PageControlChange(PageControl);
end;


procedure TDebugForm.ASaveWMExecute(Sender: TObject);
var
  Doc: IXMLDocument;
begin
  // �������� ������� ������ ���� � ������ ���� � ����

  with Owner as TDebugger do DumpMemToXML(LastMemState);
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML(LastMemState);
  Doc.Encoding := 'utf-8';
  Doc.SaveToFile('memdump.xml');
end;


procedure TDebugForm.ALoadWMExecute(Sender: TObject);
begin
  // ����������� ������� ������
  with Owner as TDebugger do LoadMemFromXML(LastMemState);
  SB.Panels[1].Text := strInited;
end;


procedure TDebugForm.ARunExecute(Sender: TObject);
begin
  if not KBLoaded then begin
    ShowMessage('Debugger: ���� ������ �� ���������');
    exit;
  end;

  // ��������� �������� � ������� ������
  // ��� ���� ���������� QueryValue ������������ � TDebugger

  InProcess := True;
  ARun.Enabled := false;
  AStepIt.Enabled := false;
  AStop.Enabled := true;
  SB.Panels[1].Text := strInProcess;

  with Owner as TDebugger do Solver.Run;
end;


procedure TDebugForm.AStepItExecute(Sender: TObject);
begin
  // ������� ��� ������

  InProcess := True;
  ARun.Enabled := false;
  AStop.Enabled := true;
  SB.Panels[1].Text := strInProcess;

  AStepIt.Enabled := false;
  with Owner as TDebugger do StepIt;
  AStepIt.Enabled := true;

  // ����� ���� �������� �������� ������� ������
  PageControlChange(PageControl);
end;


procedure TDebugForm.ARollBackExecute(Sender: TObject);
begin
  // ��������� �����

  ARollBack.Enabled := false;
  with Owner as TDebugger do RollBack;
  ARollBack.Enabled := true;
end;


procedure TDebugForm.AStopExecute(Sender: TObject);
begin
  // ���������� �����

  InProcess := False;
  ARun.Enabled := true;
  AStepIt.Enabled := true;
  AStop.Enabled := false;

  with Owner as TDebugger do Solver.StopFlag := true;

  SB.Panels[1].Text := strStopped;

  // ����� ���� �������� �������� ������� ������
  PageControlChange(PageControl);
end;


procedure TDebugForm.AHelpExecute(Sender: TObject);
begin
  // ����� �������
  ShowMessage('����� �� ���� ������...');
end;


procedure TDebugForm.FillControlsForScreenShot;
var
  i: integer;
  r,s: TTreeNode;
begin
  PropertiesGB.Caption := '������: ��������������� ������';
  // ������ �������
  WMSG.Cells[0,1] := '����� ��������� ���������';
  WMSG.Cells[0,2] := '����������� ����';
  WMSG.Cells[0,3] := '������';
  WMSG.Cells[0,4] := '�������� ����';
  WMSG.Cells[0,5] := '������������';
  WMSG.Cells[0,6] := '�������� �������';
  // ������ �������
  WMSG.Cells[1,1] := '��������(������� ����������, ����������)';
  WMSG.Cells[1,2] := '36.4';
  WMSG.Cells[1,3] := '����';
  WMSG.Cells[1,4] := '����';
  WMSG.Cells[1,5] := '�������';
  WMSG.Cells[1,6] := '���������';
  // ������ �������
  WMSG.Cells[2,1] := '100';
  WMSG.Cells[2,2] := '';
  WMSG.Cells[2,3] := '100';
  WMSG.Cells[2,4] := '80';
  WMSG.Cells[2,5] := '95';
  WMSG.Cells[2,6] := '100';
  // ���������
  for i := 1 to 6 do WMSG.Cells[3,i] := '���. ����';

  with WMTV.Items do begin
    r := Add(NIL, '�������');
    s := AddChild(r, '��������������� ������');
    AddChild(s, 'a');
    s := AddChild(r, '���������� ������������');
    AddChild(s, 'a');
    s := AddChild(r, '��������');
    AddChild(s, 'a');
    s := AddChild(r, '������������ �����������');
    AddChild(s, 'a');
    s := AddChild(r, '����������');
    AddChild(s, '�������������� �������');
    AddChild(s, '����� ������������');
    AddChild(s, '����� ������������ ��������');
    AddChild(s, '����� �����������');

    r := Add(NIL, '����');
    AddChild(r, '�������������� �������');
  end;
  WMTV.Items[0].Expanded := true;
end;

procedure TDebugForm.UpdateOMSheet; // ���������� ���������� ������� ������
  function RefAsString(Ref: TReference): WideString;
  begin
    if Ref.Ref = nil then
      Result := Ref.ID
    else
      Result := Ref.ID + '.' + RefAsString(Ref.Ref);
  end;
var
  i, j: integer;
  o, g, t: TTreeNode;
  OM: TOMContainer;
  Obj: TInstance;
  Goal: TGoal;
  idv: TIDValue;
  s: string;
begin
  OM := (Owner as TDebugger).Solver.OM;
  with WMTV.Items do begin
    Clear;
    o := Add(NIL, '�������');
    for i := 0 to OM.Objects.Count-1 do begin
      Obj := OM.Objects[i];
      t := AddChild(o, '$'+IntToHex(integer(Obj), 8)+': ' + Obj.ClassID);
      for j := 0 to Obj.PropertyList.Count-1 do begin
        idv := Obj.PropertyList[j];
        AddChild(t, idv.ID);
      end;
    end;
    g := Add(NIL, '����');
    for i:=0 to OM.Goals.Count-1 do begin
      Goal := OM.Goals[i];
      if Goal.Achieved then s := ' +'
      else s := ' -';
      if Goal.Primary then s := s + ' <Primary> '
      else s := s + ' <Subgoal> ';
      s := s + RefAsString(Goal.GoalRef);
      AddChild(g, s);
    end;
  end;
  WMTV.FullExpand;
end;

procedure TDebugForm.UpdateCRSSheet;
var
  sum,i,j: integer;
  CR: TConflictRec;
  v: WideString;
  obj: TInstance;
  OM: TOMContainer;
begin
  // ������� ����������� �����
  sum := 0;
  SetCLB.Clear;
  OM := (Owner as TDebugger).Solver.OM;
  for i:=0 to OM.Objects.Count-1 do begin
    obj := OM.Objects[i];
    sum := sum + obj.ConflictRules.Count;
    for j:=0 to obj.ConflictRules.Count-1 do begin
      CR := obj.ConflictRules[i];
      v := CR.CondValue.ResVal;
      SetCLB.Items.Add(CR.Env.ClassID+'($'+IntToHex(integer(CR.Env),8) + '). ������� ' + CR.Rule.ID + ', ���������� ������� = ' + v + ' // ' + (CR.Rule as TClassRule).Desc);
    end;
  end;
  if sum = 0 then
    CSRLabel.Caption := '����� � ����������� ������ 0 ������'
  else
    CSRLabel.Caption := '����� � ����������� ������ ' +IntToStr(sum) + ' ������(�)';
end;

procedure TDebugForm.UpdateTraceSheet;   // ���������� ������ ������
begin
  with (Owner as TDebugger).Solver do begin
    TraceMemo.Lines.Assign(Trace.XML);
  end;
end;


procedure TDebugForm.WMTVChange(Sender: TObject; Node: TTreeNode);
var
  Mem: TOMContainer;
  Obj: TInstance;
  idv: TIDValue;
  i: integer;
  c: TKBClass;
  cp: TClassProperty;
begin
  // ������ ����� ������ (������� 1)
  WMSG.RowCount := 1;
  if (Node.Level = 1) and (Node.Parent.Text = '�������') then begin
    Mem := (Owner as TDebugger).Solver.OM;
    Obj := Mem.Objects[Node.Index];
    // ������� ��� ��������
    if Obj <> nil then
    for i := 0 to Obj.PropertyList.Count-1 do begin
      idv := Obj.PropertyList[i];
      if (idv.Value <> nil) then begin
        WMSG.RowCount := WMSG.RowCount + 1;
        WMSG.Cells[1, WMSG.RowCount-1] := idv.ID;
        WMSG.Objects[1, WMSG.RowCount-1] := idv; // �������� � ������ ������ �� ��������������� TIDValue

        // �������� ��������, �������� �.�. ������� �� TInstance
        if not VarIsNull(TValue(idv.Value).ResVal) then begin
          WMSG.Cells[2, WMSG.RowCount-1] := TValue(idv.Value).ResVal;
          // ������ ������ �����������
          if idv.Value.Attrs.IsBelief then
            WMSG.Cells[3, WMSG.RowCount-1] := FloatToStr(idv.Value.Attrs.Belief)
          else
            WMSG.Cells[3, WMSG.RowCount-1] := '';
          // ������ ������ �����������
          if idv.Value.Attrs.IsProbability then
            WMSG.Cells[4, WMSG.RowCount-1] := FloatToStr(idv.Value.Attrs.Probability)
          else
            WMSG.Cells[4, WMSG.RowCount-1] := '';
          // ��������
          if idv.Value.Attrs.IsAccuracy then
            WMSG.Cells[5, WMSG.RowCount-1] := FloatToStr(idv.Value.Attrs.Accuracy)
          else
            WMSG.Cells[5, WMSG.RowCount-1] := '';
        end
        else begin
          WMSG.Cells[2, WMSG.RowCount-1] := '';
          WMSG.Cells[3, WMSG.RowCount-1] := '';
          WMSG.Cells[4, WMSG.RowCount-1] := '';
          WMSG.Cells[5, WMSG.RowCount-1] := '';
        end;

        // ���� ��� ������, �� ������ �������� ������� �����
        if TValue(idv.Value).ByRef then begin
          WMSG.Cells[2, WMSG.RowCount-1] := '$'+IntToHex(TValue(idv.Value).ResVal, 8);
        end;

        // �������� �������� ��������, ��������� � ���� property � KB
        c := Obj.ClassRef as TKBClass;
        cp := GlbFindItem(c.Properties, idv.ID) as TClassProperty; // �������� �� ������� � ������� ...
        if cp <> nil then
        case cp.Source of
          psQuestion: WMSG.Cells[0, WMSG.RowCount-1] := 'Q';    // question
          psQuery:    WMSG.Cells[0, WMSG.RowCount-1] := 'db';   // query
          psInferred: WMSG.Cells[0, WMSG.RowCount-1] := 'I';    // inferred
          psSupplied: WMSG.Cells[0, WMSG.RowCount-1] := 'S';    // supplied
          psNone:     WMSG.Cells[0, WMSG.RowCount-1] := 'N';    // none
        end
        else
          WMSG.Cells[0, WMSG.RowCount-1] := 'I';
      end;
    end;
  end;
  if WMSG.RowCount < 2 then begin
    WMSG.RowCount := 2;
    WMSG.Rows[1].Clear;
  end;
  WMSG.FixedRows := 1;
end;

procedure TDebugForm.AEditWMExecute(Sender: TObject);
begin
  // ������������� ������� ������
end;

procedure TDebugForm.AConfigurateExecute(Sender: TObject);
begin
  // ��������� ��������
  with Owner as TDebugger do begin

    // �������� ������� ���������
    ConfigSolverForm.edKBFileName.Text := Solver.KBFileName;

    case Solver.Configuration.Style of
      isForward:  ConfigSolverForm.cbStrategyType.ItemIndex := 0;
      isBackward: ConfigSolverForm.cbStrategyType.ItemIndex := 1;
      isMixed:    ConfigSolverForm.cbStrategyType.ItemIndex := 2;
    end;

    if Solver.Configuration.Authentic then
      ConfigSolverForm.cbAuthentic.ItemIndex := 0
    else
      ConfigSolverForm.cbAuthentic.ItemIndex := 1;

    if Solver.Configuration.Subdialogs then
      ConfigSolverForm.cbSubdialogs.ItemIndex := 0
    else
      ConfigSolverForm.cbSubdialogs.ItemIndex := 1;

    // �������� ����� � ����������� � ���� ��, �� ��������
    if ConfigSolverForm.ShowModal = mrOK then begin

      Solver.KBFileName := ConfigSolverForm.edKBFileName.Text;

      case ConfigSolverForm.cbStrategyType.ItemIndex of
        0: Solver.Configuration.Style := isForward;
        1: Solver.Configuration.Style := isBackward;
        2: Solver.Configuration.Style := isMixed;
      end;

      case ConfigSolverForm.cbAuthentic.ItemIndex of
        0: Solver.Configuration.Authentic := false;
        1: Solver.Configuration.Authentic := true;
      end;

      case ConfigSolverForm.cbSubdialogs.ItemIndex of
        0: Solver.Configuration.Subdialogs := true;
        1: Solver.Configuration.Subdialogs := false;
      end;
    end;
  end;
end;

procedure TDebugForm.PageControlChange(Sender: TObject);
begin
  if PageControl.ActivePageIndex = OMSheet.PageIndex then begin
    // ���� ������� �� �������� � ���������� ������� ������
    UpdateOMSheet
  end
  else if PageControl.ActivePageIndex = CRSSheet.PageIndex then begin
    // ���� ������� �� �������� � ����������� �������
    UpdateCRSSheet
  end
  else if PageControl.ActivePageIndex = TraceSheet.PageIndex then begin
    // ���� ������� �� �������� � ������� ������
    UpdateTraceSheet
  end;
end;

procedure TDebugForm.WMTVDblClick(Sender: TObject);
begin
  // ���� ��������� ���� ������������� ��������-���������,
  // �� ������� �� ����, ��������������� ����������� ��������

  // !!! ��������
   
end;

procedure TDebugForm.WMSGKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  idv: TIDValue;
  v: variant;
  d: double;
begin
  // ���� ������������� ������, �� ������ ��������� � ������� ������
  if (Key <> VK_RETURN) or (WMSG.Row = 0) or (WMSG.Col < 2) then exit;
  idv := WMSG.Objects[1, WMSG.Row] as TIDValue;

  if WMSG.Col = 2 then begin
    // ������������� ��������

    if VarIsNumeric(TValue(idv.Value).ResVal) then
    try
      v := StrToFloat(WMSG.Cells[2, WMSG.Row]);
    except
      ShowMessage('������������ �������� � ��������� �������');
    end
    else
      v := WMSG.Cells[2, WMSG.Row];

    try
      TValue(idv.Value).ResVal := v;
    except
      ShowMessage('�������� �� ���������');
    end;
  end
  else if WMSG.Col = 3 then begin
    // ������������� �����������
    if WMSG.Cells[3, WMSG.Row] = '' then
      idv.Value.Attrs.IsBelief := false
    else
    try
      d := StrToFloat(WMSG.Cells[3, WMSG.Row]);
    except
      ShowMessage('������������ �������� � ��������� �������');
    end;
    try
      idv.Value.Attrs.Belief := d;
    except
      ShowMessage('�������� �� ���������');
    end;
  end
  else if WMSG.Col = 4 then begin
    // ������������� �����������
    if WMSG.Cells[4, WMSG.Row] = '' then
      idv.Value.Attrs.IsProbability := false
    else
    try
      d := StrToFloat(WMSG.Cells[4, WMSG.Row]);
    except
      ShowMessage('������������ �������� � ��������� �������');
    end;
    try
    idv.Value.Attrs.Probability := d;
    except
      ShowMessage('�������� �� ���������');
    end;
  end
  else if WMSG.Col = 5 then begin
    // ������������� ��������
    if WMSG.Cells[5, WMSG.Row] = '' then
      idv.Value.Attrs.IsAccuracy := false
    else
    try
      d := StrToFloat(WMSG.Cells[5, WMSG.Row]);
    except
      ShowMessage('������������ �������� � ��������� �������');
    end;
    try
    idv.Value.Attrs.Accuracy := d;
    except
      ShowMessage('�������� �� ���������');
    end;
  end;
end;

end.
