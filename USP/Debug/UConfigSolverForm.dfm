object ConfigSolverForm: TConfigSolverForm
  Left = 155
  Top = 193
  Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1080#1088#1086#1074#1072#1085#1080#1077' '#1084#1072#1096#1080#1085#1099' '#1074#1099#1074#1086#1076#1072
  ClientHeight = 207
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 417
    Height = 207
    Align = alClient
    Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1086#1085#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1076#1083#1103' '#1084#1072#1096#1080#1085#1099' '#1074#1099#1074#1086#1076#1072
    TabOrder = 0
    ExplicitWidth = 425
    ExplicitHeight = 219
    object OpenKBButton: TSpeedButton
      Left = 392
      Top = 40
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333330000000
        00003333377777777777333330FFFFFFFFF03FF3F7FFFF33FFF7003000000FF0
        00F077F7777773F77737E00FBFBFB0FFFFF07773333FF7FF33F7E0FBFB00000F
        F0F077F333777773F737E0BFBFBFBFB0FFF077F3333FFFF733F7E0FBFB00000F
        F0F077F333777773F737E0BFBFBFBFB0FFF077F33FFFFFF733F7E0FB0000000F
        F0F077FF777777733737000FB0FFFFFFFFF07773F7F333333337333000FFFFFF
        FFF0333777F3FFF33FF7333330F000FF0000333337F777337777333330FFFFFF
        0FF0333337FFFFFF7F37333330CCCCCC0F033333377777777F73333330FFFFFF
        0033333337FFFFFF773333333000000003333333377777777333}
      NumGlyphs = 2
      OnClick = OpenKBButtonClick
    end
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 46
      Height = 13
      Caption = #1060#1072#1081#1083' '#1041#1047
    end
    object Label2: TLabel
      Left = 8
      Top = 72
      Width = 93
      Height = 13
      Caption = #1057#1090#1088#1072#1090#1077#1075#1080#1103' '#1074#1099#1074#1086#1076#1072
    end
    object Label3: TLabel
      Left = 8
      Top = 120
      Width = 175
      Height = 13
      Caption = #1042#1077#1076#1077#1085#1080#1077' '#1091#1090#1086#1095#1085#1103#1102#1097#1080#1093' '#1087#1086#1076#1076#1080#1072#1083#1086#1075#1086#1074
    end
    object Label4: TLabel
      Left = 8
      Top = 168
      Width = 175
      Height = 13
      Caption = #1054#1073#1088#1072#1073#1086#1090#1082#1072' '#1085#1077#1076#1086#1089#1090#1086#1074#1077#1088#1085#1099#1093' '#1079#1085#1072#1085#1080#1081
    end
    object edKBFileName: TEdit
      Left = 8
      Top = 40
      Width = 385
      Height = 21
      TabOrder = 0
    end
    object cbStrategyType: TComboBox
      Left = 8
      Top = 88
      Width = 409
      Height = 21
      TabOrder = 1
      Items.Strings = (
        #1055#1088#1103#1084#1086#1081' '#1074#1099#1074#1086#1076
        #1054#1073#1088#1072#1090#1085#1099#1081' '#1074#1099#1074#1086#1076
        #1057#1084#1077#1096#1072#1085#1085#1099#1081' '#1074#1099#1074#1086#1076)
    end
    object cbSubdialogs: TComboBox
      Left = 8
      Top = 136
      Width = 409
      Height = 21
      TabOrder = 2
      Items.Strings = (
        #1044#1072
        #1053#1077#1090)
    end
    object cbAuthentic: TComboBox
      Left = 8
      Top = 184
      Width = 409
      Height = 21
      TabOrder = 3
      Items.Strings = (
        #1053#1077' '#1087#1088#1086#1074#1086#1076#1080#1090#1089#1103
        #1041#1072#1081#1077#1089' / '#1044#1077#1084#1087#1089#1090#1077#1088'-'#1064#1077#1092#1092#1077#1088)
    end
  end
  object Panel1: TPanel
    Left = 417
    Top = 0
    Width = 124
    Height = 207
    Align = alRight
    TabOrder = 1
    ExplicitLeft = 425
    ExplicitHeight = 219
    object BitBtn1: TBitBtn
      Left = 8
      Top = 16
      Width = 107
      Height = 25
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 8
      Top = 48
      Width = 105
      Height = 25
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
    object BitBtn3: TBitBtn
      Left = 8
      Top = 80
      Width = 105
      Height = 25
      Kind = bkHelp
      NumGlyphs = 2
      TabOrder = 2
    end
  end
  object odKBFileSelect: TOpenDialog
    Filter = #1060#1072#1081#1083#1099' '#1041#1047'|*.kbs'
    Left = 352
    Top = 8
  end
end
