object EditFactForm: TEditFactForm
  Left = 367
  Top = 204
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077'/'#1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1092#1072#1082#1090#1072
  ClientHeight = 153
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 8
    Top = 8
    Width = 48
    Height = 13
    Caption = #1057#1074#1086#1081#1089#1090#1074#1086
  end
  object Label4: TLabel
    Left = 8
    Top = 40
    Width = 48
    Height = 13
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077
  end
  object Label5: TLabel
    Left = 8
    Top = 72
    Width = 67
    Height = 13
    Caption = #1059#1074#1077#1088#1077#1085#1085#1086#1089#1090#1100
  end
  object Label6: TLabel
    Left = 8
    Top = 104
    Width = 70
    Height = 13
    Caption = #1042#1086#1079#1084#1086#1078#1085#1086#1089#1090#1100
  end
  object Label7: TLabel
    Left = 8
    Top = 136
    Width = 47
    Height = 13
    Caption = #1058#1086#1095#1085#1086#1089#1090#1100
  end
  object CFEdit: TEdit
    Left = 88
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object OKB: TButton
    Left = 232
    Top = 72
    Width = 113
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelB: TButton
    Left = 232
    Top = 104
    Width = 113
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
  end
  object StopB: TButton
    Left = 232
    Top = 136
    Width = 113
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1074#1099#1074#1086#1076
    ModalResult = 3
    TabOrder = 3
  end
  object CF2Edit: TEdit
    Left = 88
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object AccEdit: TEdit
    Left = 88
    Top = 136
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object ChoosePropB: TButton
    Left = 320
    Top = 8
    Width = 22
    Height = 22
    Caption = '...'
    TabOrder = 6
    OnClick = ChoosePropBClick
  end
  object RefEdit: TEdit
    Left = 88
    Top = 8
    Width = 225
    Height = 21
    Enabled = False
    TabOrder = 7
  end
  object ChooseValueB: TButton
    Left = 320
    Top = 40
    Width = 22
    Height = 22
    Caption = '...'
    TabOrder = 8
    OnClick = ChooseValueBClick
  end
  object ValueEdit: TEdit
    Left = 88
    Top = 40
    Width = 225
    Height = 21
    TabOrder = 9
  end
end
