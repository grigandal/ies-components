unit IE_Debugger;

 {******************************************************************************
 * ������ IE_Debugger.pas �������� ����������� ��������� �������:
 *
 * TDebugger - �������� ��� �������������� ��������
 * TSolverHistory - ��������� ������ ���������
 * THistRec - ��������� ������ �� ��������� ������� ������ �� 1 ����
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 8 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, Variants, VCL.Controls, SysUtils,
  KB_Common, KB_Containers, IE_Solver, UDebugForm, UEditFactForm, UConfigSolverForm;

type

  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TRecMeta ��������� ��� ���������, ���������� � THistRec
  ////////////////////////////////////////////////////////////////////////////

  TRecMeta = (rmFiredRule, rmInferredFacts, rmAskedFacts);

  ////////////////////////////////////////////////////////////////////////////
  // ����� THistRec ������ ��������� ��, ������������ �� 1 ��� ������ ��
  ////////////////////////////////////////////////////////////////////////////

  THistRec = class(TComponent)
  private
  public
    Meta: TRecMeta;
    FiredRule: integer;
    NewFacts: TList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  ////////////////////////////////////////////////////////////////////////////
  // ����� TSolverHistory ������������ ����� ������ ��������� ��
  ////////////////////////////////////////////////////////////////////////////

  TSolverHistory = class(TComponent)
  private
  public
    History: TList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    // ��������������� ��������� �� �� ���� � �������� Ind � History
    procedure RestoreState(Ind: Integer);
  end;

  ////////////////////////////////////////////////////////////////////////////
  // ����� TDebugger ������������ ����� �������� � ������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TDebugger = class(TComponent)
  private
  public
    DebugForm: TDebugForm;
    EditFactForm: TEditFactForm;
    ConfigSolverForm: TConfigSolverForm;
    Solver: TSolver;
    SH: TSolverHistory;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    // ����� �� ��������� ��� OnRequestValue
    procedure RequestValue(Sender: TObject; Ref: WideString);

    // ����� �� �������� ����������
    procedure DemonRun(Receiver, MsgIn: WideString; var Output: OleVariant);

    // ���������� ���� ���������
    procedure Show;
    // ������� ���� ���������
    procedure Hide;
    // ������� ��� ������
    procedure StepIt;
    // �������� ��� ������
    procedure RollBack;
    // ��������� ���������� ������� ������ � XML
    procedure DumpMemToXML(var Mem: WideString);
    // ������������ ������� ������ �� XML
    procedure LoadMemFromXML(Mem: WideString);
  end;


implementation

uses
  VCL.Dialogs;

/////////////////////////////////////////
// ������ ������ THistRec
/////////////////////////////////////////

constructor THistRec.Create(AOwner: TComponent);
begin
  inherited;
  NewFacts := TList.Create;
end;

destructor THistRec.Destroy;
begin
  NewFacts.Free;
  inherited;
end;


/////////////////////////////////////////
// ������ ������ TSolverHistory
/////////////////////////////////////////

constructor TSolverHistory.Create(AOwner: TComponent);
begin
  inherited;
  History := TList.Create;
end;

destructor TSolverHistory.Destroy;
begin
  History.Free;
  inherited;
end;

// ��������������� ��������� �� �� ���� � �������� Ind � History
procedure TSolverHistory.RestoreState(Ind: Integer);
begin
end;



/////////////////////////////////////////
// ������ ������ TDebugger
/////////////////////////////////////////

constructor TDebugger.Create(AOwner: TComponent);
begin
  inherited;
  Solver := TSolver.Create(AOwner);
  Solver.OnRequestValue := RequestValue;
  Solver.OnDemon := DemonRun; // ������� ����� �� �� ����� ����
  Solver.Loader.PtrOnDemon := DemonRun;  // ����� Loader ���� �� ���� �����������
  SH := TSolverHistory.Create(AOwner);
  DebugForm := TDebugForm.Create(Self);
  EditFactForm := TEditFactForm.Create(Self);
  ConfigSolverForm := TConfigSolverForm.Create(Self);
end;

destructor TDebugger.Destroy;
begin
  ConfigSolverForm.Free;
  DebugForm.Free;
  EditFactForm.Free;
  Solver.Free;
  SH.Free;
  inherited;
end;

// ���������� ���� ���������
procedure TDebugger.Show;
begin
  DebugForm.ShowModal;
end;

// ������� ���� ���������
procedure TDebugger.Hide;
begin
  DebugForm.Hide;
end;

// ������� ��� ������
procedure TDebugger.StepIt;
begin
  Solver.StepIt;
end;

// �������� ��� ������
procedure TDebugger.RollBack;
begin
end;

// ��������� ���������� ������� ������ � XML
procedure TDebugger.DumpMemToXML(var Mem: WideString);
var
  Doc: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<shell/>');
  Solver.OM.XML(Doc.DocumentElement);
  Mem := Doc.DocumentElement.ChildNodes[0].XML;
end;

// ������������ ������� ������ �� XML
procedure TDebugger.LoadMemFromXML(Mem: WideString);
var
  Doc: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML(Mem);
  Solver.OM.Clear;
  Solver.Loader.LoadOM(Solver.OM, Doc.DocumentElement, true);
  Doc := nil;
end;

// ���������� ������� OnRequestValue
procedure TDebugger.RequestValue(Sender: TObject; Ref: WideString);
var
  Attrs: TAttrList;
  Value: Variant;
begin
  EditFactForm.RefEdit.Text := Ref;

  case EditFactForm.ShowModal of
  mrOK:
    begin
      Value := EditFactForm.ValueEdit.Text;

      Attrs := TAttrList.Create(nil);
      if EditFactForm.CFEdit.Text <> '' then
        Attrs.Belief := StrToFloat(EditFactForm.CFEdit.Text);

      if EditFactForm.CF2Edit.Text <> '' then
        Attrs.Probability := StrToFloat(EditFactForm.CF2Edit.Text);

      if EditFactForm.AccEdit.Text <> '' then
        Attrs.Accuracy := StrToFloat(EditFactForm.AccEdit.Text);

      // ������������ ���� ����� ����
      Solver.SetFact(Ref, Value, Attrs, true);
      Attrs.Free;
    end;
  mrCancel:
    begin
      // ������������ ������� ���� �����
    end;
  mrAbort:
    begin
      // ������������ ���������� ���������� �����
      Solver.StopFlag := true;
    end;
  end;
end;


// ���������� ������� �� �������� ����������
procedure TDebugger.DemonRun(Receiver, MsgIn: WideString; var Output: OleVariant);
var
  Res: string;
begin
  if InputQuery('OnDemonEvent', MsgIn, Res) then
    Output := WideString(Res)
  else
    Output := Unassigned;
end;


end.
