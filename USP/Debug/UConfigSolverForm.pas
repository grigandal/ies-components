unit UConfigSolverForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, VCL.Graphics, VCL.Controls, VCL.Forms,
  VCL.Dialogs, VCL.ExtCtrls, VCL.StdCtrls, VCL.Buttons;

type
  TConfigSolverForm = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    edKBFileName: TEdit;
    OpenKBButton: TSpeedButton;
    Label1: TLabel;
    cbStrategyType: TComboBox;
    Label2: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    odKBFileSelect: TOpenDialog;
    Label3: TLabel;
    cbSubdialogs: TComboBox;
    Label4: TLabel;
    cbAuthentic: TComboBox;
    procedure OpenKBButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//var
//  frmSolverConfig: TfrmSolverConfig;

implementation

{$R *.dfm}

procedure TConfigSolverForm.OpenKBButtonClick(Sender: TObject);
begin
     if odKBFileSelect.Execute then
        edKBFileName.Text := odKBFileSelect.FileName;
end;

end.
