unit KBTester;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, COMObj, XMLDoc, XMLIntf,
  XML_Routines, KB_Containers, DD_Tokenizer;

type
  TMainForm = class(TForm)
    GroupBox2: TGroupBox;
    ChooseKB: TButton;
    KBEdit: TEdit;
    ConvertB: TButton;
    OpenDlg: TOpenDialog;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    WhereEdit: TEdit;
    MemTestB: TButton;
    XMLMemo: TMemo;
    StatB: TButton;
    UseDTDCB: TCheckBox;
    Tok: TDDTokenizer;
    TokenizeB: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ChooseKBClick(Sender: TObject);
    procedure ConvertBClick(Sender: TObject);
    procedure MemTestBClick(Sender: TObject);
    procedure StatBClick(Sender: TObject);
    procedure TokenizeBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Reader: OleVariant;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  Reader := CreateOleObject('ReadKB.KBReader');
end;

procedure TMainForm.ChooseKBClick(Sender: TObject);
begin
  if OpenDlg.Execute then begin
    KBEdit.Text := OpenDlg.FileName;
  end;
end;

procedure TMainForm.ConvertBClick(Sender: TObject);
var
  Doc: IXMLDocument;
  XML: WideString;
begin
  Reader.FileName := KBEdit.Text;
  Reader.Load;
  XML := Reader.XML;
  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML(XML);
  Doc.Encoding := 'utf-8'; // ��� ��������� �������� �����
  Doc.SaveToFile(WhereEdit.Text);
  ShowMessage('Done!');
end;

procedure TMainForm.MemTestBClick(Sender: TObject);
var
  Doc: IXMLDocument;
  XMLLoader: TXMLLoader;
  KB: TKBContainer;
begin
  // ����� ���� ����������� ��������� � ������� ������ � ��������� �������
  Doc := TXMLDocument.Create(nil);
  if UseDTDCB.Checked then Doc.ParseOptions := [poValidateOnParse];
  Doc.LoadFromFile(WhereEdit.Text);


  KB := TKBContainer.Create(nil);
  XMLLoader := TXMLLoader.Create(nil);
  XMLLoader.LoadXML(KB, Doc.DocumentElement.XML);

  ShowMessage('loaded');

  Doc.LoadFromXML('<dumped/>');
  KB.XML(Doc.DocumentElement); // ������� � Doc ��� ��
  XMLMemo.Lines.Text := Doc.DocumentElement.XML;

  ShowMessage('formed');

  Doc.SaveToFile('is-dump.xml');
  Doc := nil;
end;

procedure TMainForm.StatBClick(Sender: TObject);
begin
  Reader.FileName := KBEdit.Text;
  Reader.FlushStat('stat.csv');
  ShowMessage('������ ���� stat.csv');
end;

procedure TMainForm.TokenizeBClick(Sender: TObject);
begin
  Tok.LoadTextFromFile(KBEdit.Text);
  Tok.Tokenize;
  XMLMemo.Lines.Assign(Tok.Tokens);

end;

end.
