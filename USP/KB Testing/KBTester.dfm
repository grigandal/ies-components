object MainForm: TMainForm
  Left = 271
  Top = 106
  Width = 491
  Height = 500
  Caption = #1058#1077#1089#1090#1086#1074#1086#1077' '#1086#1082#1088#1091#1078#1077#1085#1080#1077' '#1076#1083#1103' USP'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 8
    Top = 8
    Width = 465
    Height = 97
    Caption = #1058#1077#1089#1090' IKBRead'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 290
      Height = 13
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1041#1047' '#1089' '#1071#1055#1047' '#1040#1058' '#1074' XML '#1080' '#1089#1086#1093#1088#1072#1085#1077#1085#1080#1077' XML '#1074' '#1092#1072#1081#1083#1077
    end
    object Label2: TLabel
      Left = 8
      Top = 40
      Width = 36
      Height = 13
      Caption = #1054#1090#1082#1091#1076#1072
    end
    object Label3: TLabel
      Left = 8
      Top = 64
      Width = 24
      Height = 13
      Caption = #1050#1091#1076#1072
    end
    object ChooseKB: TButton
      Left = 336
      Top = 32
      Width = 113
      Height = 25
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083' '#1089' '#1041#1047
      TabOrder = 0
      OnClick = ChooseKBClick
    end
    object KBEdit: TEdit
      Left = 64
      Top = 36
      Width = 233
      Height = 21
      TabOrder = 1
    end
    object ConvertB: TButton
      Left = 336
      Top = 64
      Width = 113
      Height = 25
      Caption = #1055#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1090#1100
      TabOrder = 2
      OnClick = ConvertBClick
    end
    object WhereEdit: TEdit
      Left = 64
      Top = 64
      Width = 233
      Height = 21
      TabOrder = 3
      Text = 'converted.xml'
    end
  end
  object MemTestB: TButton
    Left = 8
    Top = 116
    Width = 75
    Height = 25
    Caption = 'MemTest'
    TabOrder = 1
    OnClick = MemTestBClick
  end
  object XMLMemo: TMemo
    Left = 8
    Top = 152
    Width = 465
    Height = 313
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object StatB: TButton
    Left = 96
    Top = 116
    Width = 75
    Height = 25
    Caption = 'FlushStat'
    TabOrder = 3
    OnClick = StatBClick
  end
  object UseDTDCB: TCheckBox
    Left = 192
    Top = 120
    Width = 113
    Height = 17
    Caption = #1059#1095#1080#1090#1099#1074#1072#1090#1100' DTD'
    TabOrder = 4
  end
  object TokenizeB: TButton
    Left = 392
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Tokenize'
    TabOrder = 5
    OnClick = TokenizeBClick
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '*.kbs'
    Filter = 'Knowledge base files (*.kbs)|*.kbs|All files (*.*)|*.*'
    Title = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1073#1072#1079#1091' '#1079#1085#1072#1085#1080#1081
    Left = 312
    Top = 56
  end
  object Tok: TDDTokenizer
    Delimiters = ';=[]&|.~()<>'
    BlankAsSymbol = []
    DelimAsToken = True
    QuoteChar = '"'
    Left = 328
    Top = 112
  end
end
