unit OM_Containers;

{ ******************************************************************************
  * ������ OM_Containers.pas �������� ����������� ��������� �������:
  *
  * TIDValue - ��������� �������� ����������
  * TGoal - ��������� ����
  * TConflictRec - ������ � ������� � ����������� ������
  *
  * �����: << ������� �. �. >>
  * ���� ��������: << 2 ������, 2003 >>
  ****************************************************************************** }

interface

uses
  Classes, XMLDoc, XMLIntf, SysUtils,
  KB_Common, KB_Containers, KB_Values;

type

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TIDValue �������� ����������� ��� �������� ����������
  /// /////////////////////////////////////////////////////////////////////////

  TIDValue = class(TNamedItem) // ������������� ���� DTD 'property-value'
  public
    // � ������������� ���� ID ����� ��� ��������, ��������� ���������� ��� ���������
    InstRef: TInstance; // ������ �� ������������ ������
    Value: TAbstractValue;
    // OM, �������� ��������, ��������� ���������� ��� ���������
    Asked: boolean; // OM, ������������� �� �������� � ����������� ����������
    procedure XML(Parent: IXMLNode); override;
  end;

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TGoal �������� ����������� ��� �������� ���������
  /// /////////////////////////////////////////////////////////////////////////

  TGoal = class(TXMLItem) // ������������� ���� DTD 'goal'
  public
    RefString: WideString; // ��������� ������������� ������ �� ������� ��������
    GoalRef: TReference;
    // OM, ��������� ��� �������� �������� ������������ ������� world
    ValueRef: TAbstractValue;
    // OM, TValue ��� TReference, �������� �������� ��������, ������� ��������� �����������
    Achieved: boolean; // OM, ���������� �� ����
    Primary: boolean;
    // OM, ��������� ���� (���������� � ����� ����, � �� ���������)
    Inferrable: boolean; // OM, �������� �� ���� � ��������
    CycleID: integer;
    // OM, ����� ����� ������ ��������, �� ������� ���� ���� ����������
    SubGoals: TList; // OM, ������ �������� ���� TGoal
    RefCount: integer;
    ParentGoal: TGoal;
    // OM, ������� ������ �� ����� ������ ������ ������� �� ��� ����
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure FindSubgoals(Env: TInstance);
    procedure SelectGoalForSubdialog(World: TInstance; var CurBestGoal: TGoal);
    // �������� ���� ��� ����������� ����������
    procedure CheckAchieved(Env: TInstance);
    procedure IncRef;
  end;


  // ��������� �.�. 2020
  /// /////////////////////////////////////////////////////////////////////////
  // ����� TGoalDot �������� ����������� ��� ���� � ����� �����
  // (������������ ��� ����� �������� ������ �����)
  /// /////////////////////////////////////////////////////////////////////////

  TGoalDot = class
  public
    Goal: TGoal;
    ChildNeibours: TList;
    ParentNeibours: TList;
    Meta: integer;
    Used: TList;
    procedure AddParent(D: TGoalDot);
    procedure AddChild(D: TGoalDot);
    function HasChild(D: TGoalDot): boolean;
    function IsChildOf(D: TGoalDot): boolean;
    function GetAllUniqueNeibours: TList;
    function GetZMParents: TList;
    function GetNUParents(D: TGoalDot): TList;
    function GetZMChildren: TList;
    function GetNUChildren(D: TGoalDot): TList;
    procedure SpawnSubgoals(Stack: TList);
    constructor Create;
    destructor Destroy;
  private
    function DotInList(D: TGoalDot; L: TList): boolean;
  end;


  // ��������� �.�. 2020
  /// /////////////////////////////////////////////////////////////////////////
  // ����� TGoalNet �������� ����������� ��� ���� �����
  // (������������ ��� ����� �������� ������ �����)
  /// /////////////////////////////////////////////////////////////////////////

  TGoalNet = class
  public
    Dots: TList; // ������ ����� �����
    KB: TKBContainer; // ��, �� ������� ������
    OM: TOMContainer;
    // ��������� �����
    function ParseRef(RefString: WideString): TReference;
    // ---------------
    function HasDot(D: TGoalDot): boolean;
    procedure AddDot(D: TGoalDot);
    function GetGoalDotByRefString(RefString: string): TGoalDot;
    procedure CheckAchieved;
    procedure BuildFromKB(KB: TKBContainer; OM: TOMContainer);
    function GetMiniXML: string;
    constructor Create(AOwner: TComponent);
    destructor Destroy;
  private
    procedure SpawnSubgoals;
    procedure ResetMetas;
    procedure ResetUsed;
    function DotInList(D: TGoalDot; L: TList): boolean;
    function GetFirstLayer: TList;
    // ����� ��� ����� ������� ���� (�� ����������� � ���������� �� ������ �������)
    function GetLastLayer: TList;
    // ����� ��� ����� ���������� ���� (�� ����������� � ������� �� ������ �������)
    function GetLeafs: TList;
    // ����� ��� ����� ������� � ���������� ����
    function GetPremises(rule: TNamedItem): TList;
    // ����� ��� �����, ����������� � ������� �������
    function GetConseqs(rule: TNamedItem): TList;
    // ����� ��� �����, ����������� � ���������� �������
  end;

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TConflictRec �������� ������ � ������� � ����������� ������
  /// /////////////////////////////////////////////////////////////////////////

  TConflictRec = class(TXMLItem) // ������������� ���� DTD 'rec'
  public
    Env: TInstance; // OM, ������, ��� ������� �������� ������� ������
    rule: TNamedItem; // OM, �� ����� ���� TClassRule
    CondValue: TValue; // OM, �������� ������� �������
    Rate: integer; // ������ ���������� �������
    CycleID: integer;
    // ����� ����� ��������, �� ������� ������� ������ � ����������� �����
    Then_: boolean;
    // ������� ������������ then �����, ������������� ��������� � Fire
    procedure XML(Parent: IXMLNode); override;
  end;

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TRuleState �������� ������ � ��������� �������
  /// /////////////////////////////////////////////////////////////////////////

  TRuleState = class(TXMLItem) // ������������� ���� DTD 'state'
  public
    rule: TNamedItem; // OM, �� ����� ���� TClassRule
    Active: boolean;
    procedure XML(Parent: IXMLNode); override;
  end;

  /// /////////////////////////////////////////////////////////////////////////
  // ��������������� ��� ��� �������������� ���������� ����� �����
  /// /////////////////////////////////////////////////////////////////////////

  TGoalStackElement = record
    Goal: TGoal;
    CurrentIndex: integer;
  end;

  PGoalStackElement = ^TGoalStackElement;

implementation

uses
  KB_Instructions, KB_Types, Windows, VCL.Forms;

procedure wtd(s: string); // UOTPUT WRITE TO DEBUG CONSOLE
begin
  OutputDebugString(PWideChar(s));
end;

/// /////////////////////////////////////
// ������ ������ TGoal
/// //////////////////////////////////////

constructor TGoal.Create(AOwner: TComponent);
begin
  inherited;
  SubGoals := TList.Create();
  RefCount := 1;
  ParentGoal := Nil;
  // Asked := false;
end;

destructor TGoal.Destroy;
begin
  SubGoals.Free;
  inherited;
end;

procedure TGoal.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
  G: TGoal;
begin
  N := Parent.AddChild(sGoal);
  GoalRef.XML(N);

  if ValueRef <> nil then
    ValueRef.XML(N);

  if Achieved then
    N.Attributes[sAchieved] := sTrue
  else
    N.Attributes[sAchieved] := sFalse;

  if Primary then
    N.Attributes[sPrimary] := sTrue
  else
    N.Attributes[sPrimary] := sFalse;

  if Inferrable then
    N.Attributes[sInferrable] := sTrue
  else
    N.Attributes[sInferrable] := sFalse;

  // N.Attributes[sCycle] := CycleID;

  N.Attributes[sRefCount] := RefCount;

  if SubGoals.Count > 0 then
  begin
    M := N.AddChild(sGoals);
    for i := 0 to SubGoals.Count - 1 do
    begin
      G := SubGoals[i];
      G.XML(M);
    end;
  end;
end;

procedure TGoal.FindSubgoals(Env: TInstance);
var
  r: TClassRule;
  KBClass: TKBClass;
  i: integer;
begin
  KBClass := Env.ClassRef as TKBClass;
  for i := 0 to KBClass.Rules.Count - 1 do
  begin
    r := KBClass.Rules[i];
    // ���� � ������� ��� ���� ������������
    if r.rule.Achieves(Env, Self) then
    begin
      // �� ��� �������� � ������� ���� ������� ���������
      r.rule.Condition.SpawnSubgoals(Env, Self.SubGoals);
    end;
  end;
end;

// �������� ���� ��� ����������� ����������
procedure TGoal.SelectGoalForSubdialog(World: TInstance;
  var CurBestGoal: TGoal);
var
  G: TGoal;
  i: integer;
  Asked: boolean;
  IDValue: TIDValue;
begin
  // ���� ��� ���������� ����, �� nil
  if Achieved then
    exit;
  if Inferrable then
  begin
    for i := 0 to SubGoals.Count - 1 do
    begin
      G := SubGoals[i];
      // if CurBestGoal = nil then
      G.SelectGoalForSubdialog(World, CurBestGoal)
      // else if CurBestGoal.RefCount < G.RefCount then
      // G.SelectGoalForSubdialog(World, CurBestGoal)

    end;
  end
  else
    // ����������
    // ��������� ������������� �� �� ��������. ���� �� �� ���� ������
    IDValue := Self.GoalRef.GetIDValue(World) as TIDValue;
  Asked := (IDValue <> nil) and IDValue.Asked;
  if not Asked then
  begin
    // ��� ���, ����� ������� � ������� ������ ��������, ���� ����� ����
    if (CurBestGoal <> nil) then
    begin
      if (CurBestGoal.RefCount < Self.RefCount) then
        CurBestGoal := Self;
    end
    else
      CurBestGoal := Self;
  end;
end;

// ���������, ���������� �� ���� � ������������� ���� Achieved
procedure TGoal.CheckAchieved(Env: TInstance);
var
  G: TGoal;
  i: integer;
  IDValue: TIDValue;
begin
  if Achieved then
    exit;
  IDValue := Self.GoalRef.GetIDValue(Env) as TIDValue;
  Achieved := (IDValue <> nil) and (IDValue.Value <> nil);
  // ��� �������� ���� ��������
  for i := 0 to SubGoals.Count - 1 do
  begin
    G := SubGoals[i];
    G.CheckAchieved(Env);
  end;

end;

procedure TGoal.IncRef;
var
  i: integer;
  t: TGoal;
begin
  Self.RefCount := Self.RefCount + 1;
  if Self.SubGoals <> nil then
    for i := 0 to Self.SubGoals.Count - 1 do
    begin
      t := Self.SubGoals[i];
      t.IncRef;
    end;
end;

// ��������� �.�. 2020
/// /////////////////////////////////////
// ������ ������ TGoalDot
/// //////////////////////////////////////

constructor TGoalDot.Create;
begin
  Self.Used := TList.Create;
  Self.Goal := TGoal.Create(Nil);
  Self.ChildNeibours := TList.Create;
  Self.ParentNeibours := TList.Create;
end;

destructor TGoalDot.Destroy;
begin
  Self.ChildNeibours.Destroy;
  Self.ParentNeibours.Destroy;
end;

function TGoalDot.HasChild(D: TGoalDot): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to Self.ChildNeibours.Count - 1 do
  begin
    if TGoalDot(Self.ChildNeibours[i]).Goal.RefString = D.Goal.RefString then
    begin
      Result := True;
      exit;
    end;
  end;
end;

function TGoalDot.IsChildOf(D: TGoalDot): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to Self.ParentNeibours.Count - 1 do
  begin
    if TGoalDot(Self.ParentNeibours[i]).Goal.RefString = D.Goal.RefString then
    begin
      Result := True;
      exit;
    end;
  end;
end;

procedure TGoalDot.AddParent(D: TGoalDot);
var
  i: integer;
begin
  if not(Self.Goal.RefString = D.Goal.RefString) then
  begin
    if not(Self.IsChildOf(D)) then
      Self.ParentNeibours.Add(D);
    if not(D.HasChild(Self)) then
      D.AddChild(Self);
  end;
end;

procedure TGoalDot.AddChild(D: TGoalDot);
var
  i: integer;
begin
  if not(Self.Goal.RefString = D.Goal.RefString) then
  begin
    if not(Self.HasChild(D)) then
      Self.ChildNeibours.Add(D);
    if not(D.IsChildOf(Self)) then
      D.AddParent(Self);
  end;
end;

function TGoalDot.DotInList(D: TGoalDot; L: TList): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to L.Count - 1 do
  begin
    if TGoalDot(L[i]).Goal.RefString = D.Goal.RefString then
    begin
      Result := True;
      exit;
    end;
  end;

end;

function TGoalDot.GetAllUniqueNeibours: TList;
var
  i: integer;
begin

  for i := 0 to Self.ChildNeibours.Count - 1 do
  begin
    Result.Add(TGoalDot(Self.ChildNeibours[i]))
  end;

  for i := 0 to Self.ParentNeibours.Count - 1 do
  begin
    if not(Self.DotInList(Self.ParentNeibours[i], Result)) then
      Result.Add(TGoalDot(Self.ParentNeibours[i]))
  end;

end;

function TGoalDot.GetZMParents: TList;
var
  i: integer;

begin
  Result := TList.Create;
  for i := 0 to Self.ParentNeibours.Count do
    if TGoalDot(Self.ParentNeibours[i]).Meta = 0 then
      Result.Add(TGoalDot(Self.ParentNeibours[i]))

end;

function TGoalDot.GetNUParents(D: TGoalDot): TList;
var
  i: integer;

begin
  Result := TList.Create;
  for i := 0 to Self.ParentNeibours.Count - 1 do
    if not Self.DotInList(D, TGoalDot(Self.ParentNeibours[i]).Used) then
      Result.Add(TGoalDot(Self.ParentNeibours[i]))

end;

function TGoalDot.GetZMChildren: TList;
var
  i: integer;

begin
  Result := TList.Create;
  for i := 0 to Self.ChildNeibours.Count - 1 do
    if TGoalDot(Self.ChildNeibours[i]).Meta = 0 then
      Result.Add(TGoalDot(Self.ChildNeibours[i]))

end;

function TGoalDot.GetNUChildren(D: TGoalDot): TList;
var
  i: integer;

begin
  Result := TList.Create;
  for i := 0 to Self.ChildNeibours.Count - 1 do
    if not Self.DotInList(D, TGoalDot(Self.ChildNeibours[i]).Used) then
      Result.Add(TGoalDot(Self.ChildNeibours[i]))

end;

procedure TGoalDot.SpawnSubgoals(Stack: TList);
var
  i: integer;
  D: TGoalDot;
begin
  for i := 0 to Self.ChildNeibours.Count - 1 do
  begin
    D := Self.ChildNeibours[i];
    if not Self.DotInList(D, Stack) then
      Self.Goal.SubGoals.Add(D.Goal);
  end;

end;

// ��������� �.�. 2020
/// /////////////////////////////////////
// ������ ������ TGoalNet
/// //////////////////////////////////////

constructor TGoalNet.Create(AOwner: TComponent);
begin
  Self.Dots := TList.Create;
  Self.KB := TKBContainer.Create(Nil);
  Self.OM := TOMContainer.Create(Nil);
end;

destructor TGoalNet.Destroy;
begin
  Self.Dots.Free;
end;

function TGoalNet.ParseRef(RefString: WideString): TReference;
var
  dot: integer;
begin
  dot := Pos('.', RefString);
  Result := nil;
  if RefString <> '' then
  begin
    Result := TReference.Create(nil);
    if dot = 0 then
    begin
      Result.ID := RefString;
      // ������ � ����� ��
      Result.TypeRef := Self.KB.GetKBRef(Result.ID) as TKBType;
    end
    else
    begin
      Result.ID := Copy(RefString, 1, dot - 1);
      // ������ � ����� ��
      Result.TypeRef := Self.KB.GetKBRef(Result.ID) as TKBType;
      Result.Ref := ParseRef(Copy(RefString, dot + 1, length(RefString)));
    end;
  end;
end;

function TGoalNet.GetMiniXML: string;

  function GoalHeadXml(G: TGoal): string;
  begin
    Result := '<goal ref="' + G.RefString + '" refcount="' +
      IntToStr(G.RefCount) + '">';
  end;

  function Spawn(s: string; N: integer): string;
  var
    i: integer;
  begin
    Result := '';
    for i := 0 to N do
      Result := Result + s;
  end;

var
  LastLayer, DotStack, NaibourStack: TList;
  i, counter: integer;
  Last, Neibour: TGoalDot;
begin
  Self.ResetMetas;
  Result := '<goal-net>';
  DotStack := TList.Create;
  NaibourStack := TList.Create;
  LastLayer := Self.GetLastLayer; // ����� �������� ����

  for i := 0 to LastLayer.Count - 1 do
  begin
  Application.ProcessMessages;
    Self.ResetMetas;
    // ������� �� �������� ���� ���� � ������� ���� � ��� ������ ����� ������� �������
    // ����� �������� ������������ ����� ������, �������� ������������� �����
    DotStack.Clear;
    Last := LastLayer[i];
    DotStack.Add(Last);
    Result := Result + #13#10 + Spawn('    ', DotStack.Count - 1) +
      GoalHeadXml(Last.Goal) + #13#10 + Spawn('    ', DotStack.Count) +
      '<subgoals>';
    Last.Meta := 1;

    while DotStack.Count <> 0 do
    begin
    Application.ProcessMessages;
      Last := DotStack[DotStack.Count - 1];
      if Last.GetZMChildren.Count <> 0 then
      // ���� ���� �������� �����, � ������� ����� ����� 0 (�.�. �� �� �� ��������)
      begin
        Neibour := Last.GetZMChildren[0];
        DotStack.Add(Neibour);
        Result := Result + #13#10 + Spawn('    ', DotStack.Count + 1) +
          GoalHeadXml(Neibour.Goal) + #13#10 + Spawn('    ', DotStack.Count + 2) +
          '<subgoals>';
        // ��������, ��� �������� �����
        Neibour.Meta := 1;
      end
      else
      begin
        Result := Result + #13#10 + Spawn('    ', DotStack.Count + 2) +
          '</subgoals>' + #13#10 + Spawn('    ', DotStack.Count + 1) +
          '</goal>';
        DotStack.Delete(DotStack.Count - 1);
      end;
    end;
  end;
  Self.ResetMetas;
  Result := Result + #13#10 + '</goal-net>';
end;

procedure TGoalNet.ResetMetas;
var
  i: integer;
begin
  for i := 0 to Self.Dots.Count - 1 do
    TGoalDot(Self.Dots[i]).Meta := 0;
end;

procedure TGoalNet.ResetUsed;
var
  i: integer;
begin
  for i := 0 to Self.Dots.Count - 1 do
    TGoalDot(Self.Dots[i]).Used.Clear;
end;

function TGoalNet.DotInList(D: TGoalDot; L: TList): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to L.Count - 1 do
  begin
    if TGoalDot(L[i]).Goal.RefString = D.Goal.RefString then
    begin
      Result := True;
      exit;
    end;
  end;

end;

function TGoalNet.GetLastLayer: TList;
var
  i: integer;
begin
  Result := TList.Create;
  for i := 0 to Self.Dots.Count - 1 do
  begin
    if TGoalDot(Self.Dots[i]).ParentNeibours.Count = 0 then
      Result.Add(TGoalDot(Self.Dots[i]))
  end;
end;

function TGoalNet.GetFirstLayer: TList;
var
  i: integer;
begin
  Result := TList.Create;
  for i := 0 to Self.Dots.Count - 1 do
  begin
    if TGoalDot(Self.Dots[i]).ChildNeibours.Count = 0 then
      Result.Add(TGoalDot(Self.Dots[i]))
  end;
end;

function TGoalNet.GetLeafs: TList;
var
  i: integer;
  F, L: TList;
begin
  Result := TList.Create;
  F := Self.GetFirstLayer;
  L := Self.GetLastLayer;
  for i := 0 to F.Count - 1 do
  begin
    Result.Add(TGoalDot(F[i]));
  end;

  for i := 0 to L.Count - 1 do
  begin
    if not(Self.DotInList(L[i], Result)) then
      Result.Add(TGoalDot(L[i]));
  end;

end;

function TGoalNet.GetPremises(rule: TNamedItem): TList;
var
  i: integer;
  D: TGoalDot;
  Ref: TReference;

begin
  Result := TList.Create;
  for i := 0 to Self.Dots.Count - 1 do
  begin
    D := Self.Dots[i];
    Ref := Self.ParseRef(D.Goal.RefString);
    if (rule as TClassRule).rule.Condition.DependsOnRef(Ref) then
      Result.Add(D)
  end;

end;

function TGoalNet.GetConseqs(rule: TNamedItem): TList;
var
  i: integer;
  D: TGoalDot;
  Inst: TInstance;

begin
  Result := TList.Create;
  Inst := Self.OM.Objects[0];
  for i := 0 to Self.Dots.Count - 1 do
  begin
    D := Self.Dots[i];
    if (rule as TClassRule).rule.Achieves(Inst, D.Goal) then
      Result.Add(D)
  end;
end;

procedure TGoalNet.BuildFromKB(KB: TKBContainer; OM: TOMContainer);
// ������ ������� ������������� ��

var
  Cls, World: TKBClass;
  Inst: TInstance;
  O: TXMLItem;
  D: TGoalDot;
  i, j, k, M, N, tgc: integer;
  r: TClassRule;
  L, rs, atrs, prems, concs: TList;
  t: boolean;
  s, sxml: WideString;
  G, SG, HG, TMPG, TMPS: TGoal;
  doc: IXMLDocument;
begin
  Self.KB := KB;
  Self.OM := OM;
  Inst := Self.OM.Objects[0];

  // ������� ��� �������� � ������� �� ������ ����
  for i := 0 to KB.Classes.Count - 1 do
  begin
  Application.ProcessMessages;
    Cls := KB.Classes[i];
    if Cls.ID <> 'world' then
    Application.ProcessMessages;
      for j := 0 to Cls.Properties.Count - 1 do
      begin
      Application.ProcessMessages;
        for k := 0 to Inst.PropertyList.Count - 1 do
        begin
          O := Inst.PropertyList[k];
          if O is TInstance then
          begin
            if TInstance(O).ClassID = Cls.ID then
              O := TIDValue(Inst.PropertyList[k]);
          end;
          if O is TIDValue then
          begin
            if (TIDValue(O).Value as TValue).TypeRef.ID = Cls.ID then
              O := TIDValue(Inst.PropertyList[k]);
          end;
        end;
        s := TIDValue(O).ID + '.' + TClassProperty(Cls.Properties[j]).ID;
        G := TGoal.Create(Nil);
        G.RefString := s;
        G.GoalRef := ParseRef(s);
        D := TGoalDot.Create;
        D.Goal := G;
        Self.AddDot(D);
      end;
      Application.ProcessMessages;
  end;
  Application.ProcessMessages;

  // ����� ����������� �� �������� � ��������� ����

  World := GlbFindItem(KB.Classes, 'world') as TKBClass;
  // ����� world � ��
  Inst := OM.Objects[0];
  // ��������� ������ world � ������� ������    (�� ���� inst.classref ������ ���� ����� world)

  // ����������� �� ���� ��������
  for i := 0 to World.Rules.Count - 1 do
  begin
  Application.ProcessMessages;
    r := World.Rules[i];
    // ������� ����-�������
    prems := Self.GetPremises(r);
    // ������� ����-���������
    concs := Self.GetConseqs(r);
    for j := 0 to prems.Count - 1 do
    begin
    Application.ProcessMessages;
      for k := 0 to concs.Count - 1 do
      begin
      Application.ProcessMessages;
        // ���������� - ������������ �����, ������� - ��������
        TGoalDot(prems[j]).AddParent(concs[k]);
        // AddParent ��������� ������ ���� ����� ����� �� ���� ����� ������������
      end;
    end;

  end;

  // ��������� �������� ������ ���� ��� ���������� ������������ �� ��������
  for i := 0 to Self.Dots.Count - 1 do
    TGoalDot(Self.Dots[i]).Goal.RefCount := TGoalDot(Self.Dots[i])
      .ParentNeibours.Count;

  // ����� ����� ��� ��������� ����� ��������� � ������� ��� ������ ����-����

  Self.SpawnSubgoals;
end;

procedure TGoalNet.SpawnSubgoals;

var
  LastLayer, DotStack, NaibourStack: TList;
  i, counter: integer;
  Last, Neibour: TGoalDot;
begin
  Self.ResetMetas;
  DotStack := TList.Create;
  NaibourStack := TList.Create;
  LastLayer := Self.GetLastLayer; // ����� �������� ����

  for i := 0 to LastLayer.Count - 1 do
  begin
  Application.ProcessMessages;
    // ������� �� �������� ���� ���� � ������� ���� � ��� ������ ����� ������� �������
    // ����� �������� ������������ ����� ������, �������� ������������� �����
    DotStack.Clear;
    Last := LastLayer[i];
    DotStack.Add(Last);
    Last.SpawnSubgoals(DotStack);
    Last.Meta := 1;

    while DotStack.Count <> 0 do
    begin
    Application.ProcessMessages;
      Last := DotStack[DotStack.Count - 1];
      if Last.GetZMChildren.Count <> 0 then
      // ���� ���� �������� �����, � ������� ����� ����� 0 (�.�. �� �� �� ��������)
      begin
        Neibour := Last.GetZMChildren[0];
        if Neibour.Goal.RefString = '�������.���_�����������' then
        begin
          wtd('�������.���_����������� childs');
          for counter := 0 to Neibour.ChildNeibours.Count - 1 do
            wtd('    ' + TGoalDot(Neibour.ChildNeibours[Counter]).Goal.RefString);
        end;

        DotStack.Add(Neibour);
        Neibour.SpawnSubgoals(DotStack);
        // ��������, ��� �������� �����
        Neibour.Meta := 1;
      end
      else
      begin
        DotStack.Delete(DotStack.Count - 1);
      end;

    end;
  end;
  Self.ResetMetas;

end;

function TGoalNet.HasDot(D: TGoalDot): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to Self.Dots.Count - 1 do
  begin
    if TGoalDot(Self.Dots[i]).Goal.RefString = D.Goal.RefString then
    begin
      Result := True;
      exit;
    end;
  end;
end;

procedure TGoalNet.AddDot(D: TGoalDot);
begin
  if not(Self.HasDot(D)) then
    Self.Dots.Add(D)
end;

function TGoalNet.GetGoalDotByRefString(RefString: string): TGoalDot;
var
  i: integer;
begin
  Result := Nil;
  for i := 0 to Self.Dots.Count - 1 do
  begin
    if TGoalDot(Self.Dots[i]).Goal.RefString = RefString then
      Result := Self.Dots[i];
  end;
end;

procedure TGoalNet.CheckAchieved;
var
  i: integer;
begin
  for i := 0 to Self.Dots.Count - 1 do
  begin
    TGoalDot(Self.Dots[i]).Goal.Achieved :=
      Self.OM.HasKBValue(TGoalDot(Self.Dots[i]).Goal.RefString);
  end;
end;

/// /////////////////////////////////////
// ������ ������ TIDValue
/// //////////////////////////////////////

procedure TIDValue.XML(Parent: IXMLNode);
var
  N: IXMLNode;
begin
  N := Parent.AddChild(sPropertyValue);
  N.Attributes[sID] := ID;
  if Value <> nil then
    Value.XML(N); // TAbstractValue

  if Asked then
    N.Attributes[sAsked] := sTrue
  else
    N.Attributes[sAsked] := sFalse;
end;

/// /////////////////////////////////////
// ������ ������ TConflictRec
/// //////////////////////////////////////

procedure TConflictRec.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
begin
  N := Parent.AddChild(sRule);
  N.Attributes[sID] := rule.ID;
  N.Attributes[sRate] := Rate;
  if Then_ then
    N.Attributes[sThen] := 'true'
  else
    N.Attributes[sThen] := 'false';

  M := N.AddChild(sObject);
  M.Attributes[sAddr] := '$' + IntToHex(integer(Env), 8);
  M.Attributes[sType] := Env.ClassID;

  M := N.AddChild(sCondition);
  CondValue.XML(M);
end;

/// /////////////////////////////////////
// ������ ������ TRuleState
/// //////////////////////////////////////

procedure TRuleState.XML(Parent: IXMLNode);
var
  N: IXMLNode;
begin
  N := Parent.AddChild(sRuleState);
  N.Attributes[sID] := rule.ID;
  if Active then
    N.Attributes[sActive] := sTrue
  else
    N.Attributes[sActive] := sFalse
end;

end.
