unit KB_Values;

 {******************************************************************************
 * ������ KB_Values.pas �������� ����������� ����������� ������ TValue:
 *
 * TValue - ��������� ��� �������� ��������
 * TReference - ��������� ��� [����������] ������ �������, ��������� � ��������� � �.�.
 * TOperation - �������� ��� ����������
 * TLibFunction - ���������������� �������
 * TValArray - ������ ��������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, Math, Variants, SysUtils, Dialogs,
  KB_Common, KB_Containers, KB_Types;

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TValue �������� ����������� �������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TValue = class(TAbstractValue)// ������������� ���� DTD 'value'
  public
    ResVal: Variant;            // KB, ����������� ��� �������� ��������
    ByRef: boolean;             // OM, ������� ����, ��� ����������� ������, � �� ��������
    // ��������� ����������� ��������
    function Evaluate(Env: TInstance; Locals: TList): TAbstractValue; override;
    // ���������� TValue ��������� �������� Op ��� ����� � Arg, ���� �������, �� nil
    function EvalOp(Op: TOpMeta; Arg: TValue): TAbstractValue; virtual;
    procedure XML(Parent: IXMLNode); override;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); override; // ���������� ������� �� ������� �������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TReference - ������ ���������� [������������������] ���������
  // � ����-���� (�������, �������� � �.�.). �� ������ ��������, �� ����������.
  ////////////////////////////////////////////////////////////////////////////

  TReference = class(TAbstractValue) // ������������� ���� DTD 'ref'
  public
    ID: WideString;     // KB, ��� �������� � ������� ����������
    Params: TList;      // KB, ��������� TIDValue, ���� ���������� � ������ ��� ��������-�������
    Ref: TReference;    // KB, ������ ���������� ������, �������� ������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Evaluate(Env: TInstance; Locals: TList): TAbstractValue; override; // ������� ��������
    procedure AssignValue(Env: TInstance; Locals: TList; Value: TValue; Asked: boolean); // ����������� ��������
    function Equals(Env: TInstance; VarRef: TReference; VarRefEnv: TInstance): boolean;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); override; // ���������� ������� �� ������� �������
    function AsString: WideString;
    function GetIDValue(Env: TInstance): TNamedItem; // ���������� TIDValue, ���� ��� ������ �� ��������
  private
    // ������� ������� ID ������� ������ (��� ������� Equals)
    function GetLastID: WideString;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TOperation �������� ���������, ������������ ��� ����������
  ////////////////////////////////////////////////////////////////////////////

  TOperation = class(TAbstractValue)
  public
    OpMeta: TOpMeta;
    Operand1: TAbstractValue;
    Operand2: TAbstractValue;
    isBinary: boolean;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Evaluate(Env: TInstance; Locals: TList): TAbstractValue; override;
    function EvaluationPossible: boolean;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); override; // ���������� ������� �� ������� �������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TLibFunction ��������� ���������������� �������
  ////////////////////////////////////////////////////////////////////////////

  TLibFunction = class(TAbstractValue)  // ������������� ���� DTD 'func'
  private
    FOnDemon: TDemonEvent;      // KB, ���������� �������
  public
    FuncMeta: TLibFuncMeta;     // KB (������� ID)
    Operands: TList;            // ������� TAbstractValue
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Evaluate(Env: TInstance; Locals: TList): TAbstractValue; override;
    function EvaluationPossible: boolean;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); override; // ���������� ������� �� ������� �������
  published
    property OnDemon: TDemonEvent read FOnDemon write FOnDemon;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TValArray �������� ����������� ������� ��������
  ////////////////////////////////////////////////////////////////////////////

  TValArray = class(TAbstractValue)
  public
    VL: TList; // ������ ����������� TAbstractValue
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); override; // ���������� ������� �� ������� �������
  private
    procedure SetItem(Index: integer; V: TAbstractValue);
    function GetItem(Index: integer): TAbstractValue;
  public
    property Items[Index: Integer]: TAbstractValue read GetItem write SetItem; default;
  end;

  // ��������� ������������ ������� ����������� ��� Res �� ������ ����������
  procedure CalcNotFactors(Arg, Res: TAttrList);
  procedure CalcAndFactors(Arg1, Arg2, Res: TAttrList);
  procedure CalcOrFactors(Arg1, Arg2, Res: TAttrList);
  procedure CalcEHFactors(E, H, R: TAttrList);  // ����������� ��� ������������ �������
  procedure JoinEvidences(E1, E2, R: TAttrList);

  // ��� ��������� ����� ������� ��� belief � accuracy � �������� ��� probability
  procedure CalcComparisonFactors(Arg1, Arg2, Res: TAttrList);

implementation

uses
  OM_Containers;


// ��������� ������������ ������� ����������� ��� Res �� ������ ����������

procedure CalcNotFactors(Arg, Res: TAttrList);
begin
  if Arg.IsBelief and Arg.IsProbability then begin
    // Dempster-Shafer
    Res.Belief := Arg.Belief; //1 - Arg.Probability;
    Res.Probability := Arg.Probability; //1 - Arg.Belief;
  end
  else if Arg.IsBelief then begin
    // Bayes
    Res.Belief := Arg.Belief; //1 - Arg.Belief;
  end;
end;

procedure CalcAndFactors(Arg1, Arg2, Res: TAttrList);
begin
  // ����� Bayes ��������� � DS

  // ���� ���� ���� ���� ������, �� �������
  if not Arg1.IsBelief then Res.Belief := Arg2.Belief;
  if not Arg2.IsBelief then Res.Belief := Arg1.Belief;
  if Arg1.IsBelief and Arg2.IsBelief then
    Res.Belief := Arg1.Belief * Arg2.Belief;

  // ������� ��� Dempster-Shafer
  if not Arg1.IsProbability then Res.Probability := Arg2.Probability;
  if not Arg2.IsProbability then Res.Probability := Arg1.Probability;
  if Arg1.IsProbability and Arg2.IsProbability then
    Res.Probability := Arg1.Probability * Arg2.Probability;
end;

procedure CalcOrFactors(Arg1, Arg2, Res: TAttrList);
begin
  // ����� Bayes ��������� � DS
  if not Arg1.IsBelief then Res.Belief := Arg2.Belief;
  if not Arg2.IsBelief then Res.Belief := Arg1.Belief;
  if Arg1.IsBelief and Arg2.IsBelief then
    Res.Belief := Arg1.Belief + Arg2.Belief - Arg1.Belief*Arg2.Belief;

  // ������� ��� Dempster-Shafer
  if not Arg1.IsProbability then Res.Probability := Arg2.Probability;
  if not Arg2.IsProbability then Res.Probability := Arg1.Probability;
  if Arg1.IsProbability and Arg2.IsProbability then
    Res.Probability := Arg1.Probability + Arg2.Probability - Arg1.Probability * Arg2.Probability;
end;

procedure CalcEHFactors(E, H, R: TAttrList);
begin
  // ������������ ����� ������� ��� H � ����������� �� E
  // E->H ����� ��� ������ [Bel(E->H), Pl(E->H)] � [Bel(~E->H), Pl(~E->H)]
  // �������, ��� ������ ������ ����� ��� [0, 1]

  if E.IsBelief and H.IsBelief and H.IsProbability then begin
    // DS
    R.Belief := H.Belief * E.Belief;
    R.Probability := 1 - E.Belief + H.Probability * E.Belief;
  end;
end;

procedure JoinEvidences(E1, E2, R: TAttrList);
var
  denominator: double;
begin
  // ����������� ������������ ds
  if E1.IsBelief and E1.IsProbability and E2.IsBelief and E2.IsProbability then begin
    denominator := 1 - E1.Belief*(1-E2.Probability) - E2.Belief*(1-E1.Probability);
    R.Belief := (E1.Belief * E2.Probability + E1.Probability * E2.Belief - E1.Belief * E2.Belief) / denominator;
    R.Probability := E1.Probability * E2.Probability / denominator;
  end;
end;

// ��� ��������� ����� ������� ��� belief � accuracy � �������� ��� probability
procedure CalcComparisonFactors(Arg1, Arg2, Res: TAttrList);
begin
  // �����������
  if not Arg1.IsBelief then Res.Belief := Arg2.Belief
  else begin
    if not Arg2.IsBelief then Res.Belief := Arg1.Belief
    else Res.Belief := Math.Min(Arg1.Belief, Arg2.Belief);
  end;

  // �����������
  if not Arg1.IsProbability then Res.Probability := Arg2.Probability
  else begin
    if not Arg2.IsProbability then Res.Probability := Arg1.Probability
    else Res.Probability := Math.Max(Arg1.Probability, Arg2.Probability);
  end;

  // ��������
  if not Arg1.IsAccuracy then Res.Accuracy := Arg2.Accuracy
  else begin
    if not Arg2.IsAccuracy then Res.Accuracy := Arg1.Accuracy
    else Res.Accuracy := Math.Min(Arg1.Accuracy, Arg2.Accuracy);
  end;
end;


/////////////////////////////////////////
// ������ ������ TValue
/////////////////////////////////////////

procedure TValue.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sValue);
  if ByRef then begin
    N.Text := '$'+IntToHex(integer(ResVal),8);
    N.Attributes[sByRef] := sTrue;
  end
  else
    N.Text := ResVal;
  Attrs.XML(Parent);
end;


function TValue.Evaluate(Env: TInstance; Locals: TList): TAbstractValue;
var
  R: TValue;
begin
  // ������� TValue � TMembershipFunction ������ �������� ����
  R := TValue.Create(nil);
  R.ResVal := ResVal;
  R.TypeMeta := TypeMeta;
  R.TypeRef := TypeRef;
  R.ByRef := ByRef;

  R.Attrs.Assign(Attrs);
  {if Attrs.IsBelief then R.Attrs.Belief := Attrs.Belief;
  if Attrs.IsProbability then R.Attrs.Probability := Attrs.Probability;
  if Attrs.IsAccuracy then R.Attrs.Accuracy := Attrs.Accuracy;}

  Result := R;
end;


function TValue.EvalOp(Op: TOpMeta; Arg: TValue): TAbstractValue;
var
  r: TValue;
  s1, s2: string; // ���� ��������� ����
begin
  // ����� ������������ ������� OpIsArythm, OpIsLogic, OpIsTest
  r := nil;
  Result := nil;

  // !!! ResVal = -1, true, 1 - ��� ��� ������...   �����������

  // ����������� boolean � �����, ����� ����� �� �����������
  if VarIsType(Self.ResVal, varBoolean) then
    Self.ResVal := ord(boolean(Self.ResVal));

  if VarIsType(Arg.ResVal, varBoolean) then
    Arg.ResVal := ord(boolean(Arg.ResVal));

  // ���������� 1: ND <=> :TValue = nil ��� :TValue.ResVal = NULL

  // ���������� 2: � TValue, ��� �������� �������� EvalOp, ResVal ������ ���������.
  // ���� �� ND, �� � Arg.ResVal ���� ND. ��� ������������ ���������� �������.
  // ������ ��������� ND ��������� � �������� (��� �������� ������� �������).

  // ���������� 3: � ���������� ���� ����� ������� �������� ����������������

  if TypeMeta = tmSymbolic then begin
    // ������� ����������, ��������� ��������: ���������

    // ���� ���-���� �� ���������, �� ���������� ����������
    if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
      exit;

    try
      s1 := Self.ResVal;
      s2 := Arg.ResVal;
    except
      ErrorHandler.HandleError(eEvalFailed, '���� ��������� ������������');
    end;

    r := TValue.Create(nil);
    r.TypeMeta := tmNumeric; // �� ����� ���� ��������� ��������� �����

    case Op of
    // ���������
    omGt:
      begin
        // �����������������
        r.ResVal := s1 > s2;
      end;
    omLt:
      begin
        // �����������������
        r.ResVal := s1 < s2;
      end;
    omEq:
      begin
        // ����� ����� ������ ������� ����������� (����� ��������)
        r.ResVal := s1 = s2;
      end;
    omNe:
      begin
        // ����� ����� ������ ������� ����������� (����� ��������)
        r.ResVal := s1 <> s2;
      end;
    omGe:
      begin
        // �����������������
        r.ResVal := s1 >= s2;
      end;
    omLe:
      begin
        // �����������������
        r.ResVal := s1 <= s2;
      end;
    end;
  end
  else if TypeMeta = tmNumeric then begin
    // ������� ��������, �������� ���������, ����������
    // ��� ������������� � ��������� ��� � ������
    // ���������: ��������� ���������� �������� �� ��������������

    r := TValue.Create(nil);
    r.TypeMeta := tmNumeric;

    case Op of
    // ������
    omAnd:
      begin
        // 0 & ND = 0
        // � & B = min(A, B), ��� A, B in [0; 1]
        // A & ND = ND, ��� A in (0; 1]
        // ND & ND = ND
        if (Self.ResVal = 0) or (Arg.ResVal = 0) then
          r.ResVal := integer(0)
        else if not VarIsNull(Self.ResVal) and not VarIsNull(Arg.ResVal) then
          r.ResVal := min(Self.ResVal, Arg.ResVal)
        else
          r.ResVal := NULL;

        // ���������� ������� �����������, ���� ����
        if not VarIsNull(r.ResVal) then CalcAndFactors(Self.Attrs, Arg.Attrs, r.Attrs);
      end;
    omOr:
      begin
        // 1 | ND = 1
        // A | B = max(A, B), ��� A, B in [0; 1]
        // ND | ND = ND
        // A | ND = ND, ��� A in [0; 1)
        if (Self.ResVal = 1) or (Arg.ResVal = 1) then
          r.ResVal := integer(1)
        else if not VarIsNull(Self.ResVal) and not VarIsNull(Arg.ResVal) then
          r.ResVal := max(Self.ResVal, Arg.ResVal)
        else
          r.ResVal := NULL;

        // ���������� ������� �����������, ���� ����
        if not VarIsNull(r.ResVal) then CalcOrFactors(Self.Attrs, Arg.Attrs, r.Attrs);
      end;
    omXor:
      begin
        // ND ^ ND = ND
        // ND ^ A = ND, ��� A in [0; 1]
        // A ^ B = A xor B
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          r.ResVal := boolean(Self.ResVal) xor boolean(Arg.ResVal);
      end;
    omNot:
      begin
        // ~ A = 1 - A, ��� A in [0; 1]
        // ~ ND = ND
        if VarIsNull(Self.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := min(1, max(0, 1 - Self.ResVal)); // ���������� �������� in [0; 1]
        end;

        // ���������� ������� �����������, ���� ����
        if not VarIsNull(r.ResVal) then CalcNotFactors(Self.Attrs, r.Attrs);
      end;

    // ���������
    omGt:
      begin
        // ��������� �������� �� null, � ��� ������...
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal > Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;
    omLt:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal < Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;
    omEq:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal = Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;
    omNe:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal <> Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;
    omGe:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal >= Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;
    omLe:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else begin
          r.ResVal := Self.ResVal <= Arg.ResVal;
          CalcComparisonFactors(Self.Attrs, Arg.Attrs, r.Attrs);
        end;
      end;

    // ����������
    omMul:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          r.ResVal := Self.ResVal * Arg.ResVal;
      end;
    omSub:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          r.ResVal := Self.ResVal - Arg.ResVal;
      end;
    omDiv:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          if Arg.ResVal <> 0 then
            r.ResVal := Self.ResVal / Arg.ResVal;
      end;
    omAdd:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          r.ResVal := Self.ResVal + Arg.ResVal;
      end;
    omMod:
      begin
        if VarIsNull(Self.ResVal) or VarIsNull(Arg.ResVal) then
          r.ResVal := NULL
        else
          if Arg.ResVal <> 0 then
            r.ResVal := Round(Self.ResVal) mod Round(Arg.ResVal);
      end;
    omNeg:
      begin
        if VarIsNull(Self.ResVal) then
          r.ResVal := NULL
        else
          r.ResVal := - Self.ResVal;
      end;
    end;
  end;

  Result := r;
end;

procedure TValue.SpawnSubgoals(Env: TInstance; GL: TList);
begin
  // � TValue ������ �� ��������
end;

/////////////////////////////////////////
// ������ ������ TReference
/////////////////////////////////////////

constructor TReference.Create(AOwner: TComponent);
begin
  inherited;
  ID := '';
  Ref := nil;
  Params := TList.Create;
end;

destructor TReference.Destroy;
begin
  Params.Free;
  Ref.Free;
  inherited;
end;

procedure TReference.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sRef);
  N.Attributes[sID] := ID;

  if Ref <> nil then Ref.XML(N); // ���� ������ ���������

  Attrs.XML(Parent);

  // --- ������� � runtime �������� ---
  //N := Parent.AddChild(sRuntime);
  //N.Text := ResVal;
end;

function TReference.Evaluate(Env: TInstance; Locals: TList): TAbstractValue;
var
  KBClass: TKBClass;
  Method: TClassMethod;
  Val: TValue;
  IDValue: TIDValue;
  i: integer;
  AbsVal: TAbstractValue;
  found: boolean;
  RtmParams: TList;
  CurEnv: TInstance;
  CurRef: TReference;
begin
  Result := nil;
  if Env = nil then exit; // ���� �� ��� ������ ��� ��������� �������

  // ��������� ������ ��������� ����������, ����� ���� ����� � Env �������� ������ � CurEnv �������� ������
  // Env ��� ���������� ����������, CurEnv ��� ������ ������� � �������
  CurRef := Self;
  CurEnv := Env;
  while CurRef <> nil do begin
    // ������� � RtmParams ����������� Params ��� Env
    RtmParams := TList.Create;
    for i := 0 to CurRef.Params.Count-1 do begin
      IDValue := CurRef.Params[i];
      if IDValue.Value <> nil then begin
        Val := IDValue.Value.Evaluate(Env, Locals) as TValue;
        RtmParams.Add(Val);
      end
      else begin
        RtmParams.Add(nil);
        ErrorHandler.HandleError(eEvalFailed,
          'TReference: �������� ��������� �� �� ��������� "'+IDValue.ID+
          '" ������� "'+IntToHex(integer(IDValue.InstRef),8)+'": '+IDValue.InstRef.ClassID+' ������� nil');
      end;
    end;

    Stack.Add(RtmParams); // �������� ��������� ��� ����

    // ������ ������ ��������� �� ����� ��� �������� ������ TIntance
    AbsVal := nil;
    found := false;

    // ������ ����� ���� �� ID ����� ���������� � ��������� ����������
    IDValue := GlbFindItem(Locals, CurRef.ID) as TIDValue;
    if IDValue <> nil then begin // ������ ���� TIDValue
      // ����� ����������
      if IDValue.Value <> nil then begin
        AbsVal := IDValue.Value.Evaluate(Env, Locals);
      end;
      found := true;
    end;

    if not found then begin
      // ���� � ��������� (� ��� ����� COM)
      Val := CurEnv.GetProperty(CurRef.ID) as TValue;
      if Val <> nil then begin // ������ ���� ��� ����������� TValue
        // ����� ��������
        AbsVal := Val.Evaluate(CurEnv, Locals);
        found := true;
      end
    end;

    if not found then begin
      // ���� � �������, ���� � ������� ���� ������ �� ����� ��
      if CurEnv.ClassRef <> nil then begin
        KBClass := CurEnv.ClassRef as TKBClass;
        Method := KBClass.Method[CurRef.ID] as TClassMethod;
        if Method <> nil then begin
          // ����� ����� ������ ��
          AbsVal := Method.Interprete(CurEnv, CurRef.Params); // e��� ��� ���������, �� ����� nil
          found := true;
        end;
      end;
    end;

    if not found then begin
      // ��������� ������� COM-�����
      AbsVal := CurEnv.COMInvoke(CurRef.ID);
      if AbsVal <> nil then begin
        // ����� � ������ ������� COM-�����
        found := true;
        // �������� ��������� ���� �������� � Params; ����� Params � RtmParams �. ���������!!!
        for i := 0 to RtmParams.Count-1 do begin
          IDValue := CurRef.Params[i];
          Val := RtmParams[i];
          if IDValue.Value is TValue then
            TValue(IDValue.Value).ResVal := Val.ResVal
          else if IDValue.Value is TReference then
            TReference(IDValue.Value).AssignValue(CurEnv, Locals, Val, false);
        end;
      end;
    end;

    Stack.Remove(Stack.Last);

    // ��� ������, ���� �������� �������� ��� �� ��������� ��� ��� ������ ID ��� ����� �� ������ ��������
    if not found then begin
      //ErrorHandler.HandleError(eEvalFailed, 'TReference.Evaluate: �� ������ �� ����� �� �������� � ������ "'+ID+'" � ������� "'+IntToHex(integer(Env),8)+'": '+Env.ClassID);
      exit;
    end;

    CurRef := CurRef.Ref;

    // ���� ������ ���������, �� ��������� �������� TInstance � ��������� ������
    if CurRef <> nil then begin
      // !!! �������, ��� COM-������ TInstance ���������� �� �����, ��������� � ����������������
      if AbsVal <> nil then begin
        // ����� �� ����� ���������� � COM
        CurEnv := TInstance(integer(TValue(AbsVal).ResVal));
      end
      else begin
        ErrorHandler.HandleError(eEvalFailed, 'TReference: ��������� ������ �� �� ���������, �� ��� ��������� � ������ ��� �������� "'+ID+'" ������� "'+IntToHex(integer(Env),8)+'": '+Env.ClassID+' ������� nil');
        exit;
      end;
    end;
  end; // while CurRef <> nil

  Result := AbsVal;
end;

procedure TReference.AssignValue(Env: TInstance; Locals: TList; Value: TValue; Asked: boolean);
var
  KBClass: TKBClass;
  Method: TClassMethod;
  Val: TValue;
  NextEnv: TInstance;
  IDValue: TIDValue;
  i: integer;
  RtmParams: TList;
  AbsVal: TAbstractValue;
  found: boolean;
begin
  if Env = nil then exit; // ���� �� ��� ������ ��� ��������� �������

  // !!! ������� �� ������� Value ��� �����������?

  // ���� ���� �������� ������������� �����, �� ���� �������� ����������� ������������

  // ���� ��������� �� ������, �� ����������� �� ������
  // ���� ��������� �� ��������, �� ����������� ��������

  // ������� � RtmParams ����������� Params
  RtmParams := TList.Create;
  for i := 0 to Params.Count-1 do begin
    Val := TIDValue(Params[i]).Value.Evaluate(Env, Locals) as TValue;
    RtmParams.Add(Val);
  end;

  Stack.Add(RtmParams); // �������� ��������� ��� ����
  found := false;

  if Ref = nil then begin
    // ������ ����� ���� �� ID ����� ���������� � ��������� ����������
    IDValue := GlbFindItem(Locals, ID) as TIDValue;
    if IDValue <> nil then begin // ������ ���� TIDValue
      // ����� ����������
      IDValue.Value := Value;
      IDValue.Asked := Asked;
      found := true;
    end;

    if not found then begin
      // ��������� ��������� c������� (� ��� ����� COM)
      Env.SetProperty(ID, Value, Asked);
      found := true; // ���� ���� �������� �� ����, �� ��������� ���� <ID, Value>
    end;
  end
  else begin // ������ ���������
    // ������ ����� ���� �� ID ����� ���������� � ��������� ����������
    IDValue := GlbFindItem(Locals, ID) as TIDValue;
    if IDValue <> nil then begin // ������ ���� TIDValue
      // ����� ����������
      AbsVal := IDValue.Value.Evaluate(Env, Locals);
      found := true;
    end;

    if not found then begin
      // ���� � ��������� (� ��� ����� COM)
      Val := Env.GetProperty(ID) as TValue;
      if Val <> nil then begin // ������ ���� ��� ����������� TValue
        // ����� ��������
        AbsVal := Val.Evaluate(Env, Locals); // ���������
        found := true;
      end
    end;

    if not found then begin
      // ���� � �������, ���� � ������� ���� ������ �� ����� ��
      if Env.ClassRef <> nil then begin
        KBClass := Env.ClassRef as TKBClass;
        Method := KBClass.Method[ID] as TClassMethod;
        if Method <> nil then begin
          // ����� ����� ������ ��
          AbsVal := Method.Interprete(Env, Params); // e��� ��� ���������, �� ����� nil
          found := true;
        end;
      end;
    end;

    // !!! �������, ��� COM-������ TInstance ���������� �� �����, ��������� � ����������������

    if AbsVal <> nil then begin
      NextEnv := TInstance(integer(TValue(AbsVal).ResVal));
      Ref.AssignValue(NextEnv, Locals, Value, Asked);
    end
    else
      ErrorHandler.HandleError(eEvalFailed, 'TReference: ��������� ������ �� �� ���������, �� ��� ��������� � ������ ��� �������� "'+ID+'" ������� "'+IntToHex(integer(Env),8)+'": '+Env.ClassID+' ������� nil');

  end;

  Stack.Remove(Stack.Last);

  // ��� ������, ���� �������� �������� ��� �� ��������� ��� ��� ������ ID ��� ����� �� ������ ��������
  if not found then begin
    ErrorHandler.HandleError(eEvalFailed, 'TReference.Assign: �� ������ �� ����� �� �������� � ������ "'+ID+'" � ������� "'+IntToHex(integer(Env),8)+'": '+Env.ClassID);
    exit;
  end;
end;

// ������� ������� ID ������� ������
function TReference.GetLastID: WideString;
 function GetLastID(Ref: TReference): WideString;
 begin
   if Ref.Ref = nil then Result := Ref.ID
   else Result := GetLastID(Ref.Ref);
 end;
begin
  Result := GetLastID(Self);
end;

// ������� ���������� ������ ������ ������������ Env �� ������� VarRef ������������ VarRefEnv
// ���� ������ ��������� �� ���� � �� �� ��������, �� ������������ true
function TReference.Equals(Env: TInstance; VarRef: TReference; VarRefEnv: TInstance): boolean;
 function GetLastInstance(Ref: TReference; Env: TInstance): TInstance;
 var v: TValue;
 begin
   if Ref.Ref = nil then Result := Env
   else begin
     v := TValue(Env.GetProperty(Ref.ID));
     if (v <> nil) and v.ByRef and not VarIsNull(v.ResVal) then
       Result := GetLastInstance(Ref.Ref, TInstance(integer(v.ResVal)))
     else
       Result := nil;
   end;
 end;
begin
  // ���� ��������� �������� �������������� � ��������� ��������� �� �������� TInstance, �� ok
  Result :=
    (Self.GetLastID = VarRef.GetLastID) and
    (GetLastInstance(Self, Env) = GetLastInstance(VarRef, VarRefEnv));
end;

function TReference.AsString: WideString;
var
  s: WideString;
begin
  s := ID;
  if Ref <> nil then s := s +'.'+Ref.AsString;
  Result := s;
end;

procedure TReference.SpawnSubgoals(Env: TInstance; GL: TList);
var
  G: TGoal;
  i: integer;
begin
  // ���� �������� ������ �� �������� � �������,
  // ���� ��� ������ �� �����, �� ��������� �������� !!!
  // ��� �� ������� ����� ���� ���-�� � ������ !!!
  // ��������, ���� �� ���� �� � ������ GL �� ����

  for i:=0 to GL.Count-1 do begin
    G := GL[i];
    if Self.Equals(Env, G.GoalRef, Env) then begin
      G.RefCount := G.RefCount + 1;
      exit;
    end;
  end;

  G := TGoal.Create(nil);
  G.GoalRef := Self;            // ���� �� ������������
  G.RefString := Self.AsString;
  G.Achieved := false;
  G.Primary := false;           // �������
  G.CycleID := -1;
  G.FindSubGoals(Env);
  G.Inferrable := G.SubGoals.Count > 0;
  //G.PropertyValue := Self.GetIDValue(Env) as TIDValue;
  GL.Add(G);
end;


function TReference.GetIDValue(Env: TInstance): TNamedItem;
var
  Val: TValue;
begin
  Result := nil;
  if Env = nil then exit; // ���� �� ��� ������ ��� ��������� �������

  if Ref = nil then begin
    Result := Env.GetIDValue(ID) as TIDValue;
  end
  else begin // ������ ���������
    Val := Env.GetProperty(ID) as TValue;
    if Val <> nil then begin
      Result := Ref.GetIDValue(TInstance(integer(Val.ResVal)));
    end;
  end;
end;


/////////////////////////////////////////
// ������ ������ TOperation
/////////////////////////////////////////

constructor TOperation.Create(AOwner: TComponent);
begin
  inherited;
  Operand1 := NIL;
  Operand2 := NIL;
  isBinary := True;
end;

destructor TOperation.Destroy;
begin
  Operand1.Free;
  Operand2.Free;
  inherited;
end;

procedure TOperation.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  case OpMeta of
  omAdd: N := Parent.AddChild(sAdd);
  omDiv: N := Parent.AddChild(sDiv);
  omSub: N := Parent.AddChild(sSub);
  omMul: N := Parent.AddChild(sMul);
  omMod: N := Parent.AddChild(sMod);
  omAnd: N := Parent.AddChild(sAnd);
  omNot: N := Parent.AddChild(sNot);
  omXor: N := Parent.AddChild(sXor);
  omOr:  N := Parent.AddChild(sOr);
  omLt:  N := Parent.AddChild(sLt);
  omGt:  N := Parent.AddChild(sGt);
  omEq:  N := Parent.AddChild(sEq);
  omNe:  N := Parent.AddChild(sNe);
  omGe:  N := Parent.AddChild(sGe);
  omLe:  N := Parent.AddChild(sLe);
  end;

  Operand1.XML(N);
  if isBinary then Operand2.XML(N);

  Attrs.XML(N);
end;

function TOperation.EvaluationPossible: boolean;
begin
  Result := true;

end;

function TOperation.Evaluate(Env: TInstance; Locals: TList): TAbstractValue;
var
  FactOp1, FactOp2: TValue;
begin
  // ��������� ���� TOperation, TReference � TLibFunction
  // TValue � TMembershipFunction ��������� �� ����
  FactOp2 := nil;

  if (Operand1 is TReference) or (Operand1 is TOperation) or (Operand1 is TLibFunction) then
    FactOp1 := Operand1.Evaluate(Env, Locals) as TValue
  else
    FactOp1 := Operand1 as TValue;

  if isBinary then
  if (Operand2 is TReference) or (Operand2 is TOperation) or (Operand2 is TLibFunction) then
    FactOp2 := Operand2.Evaluate(Env, Locals) as TValue
  else
    FactOp2 := Operand2 as TValue;

  // ������ ������� ����� �������: �� :TValue = nil �������� � :TValue.ResVal = NULL
  // ����� ������ ����� ���� ������� EvalOp � ������� ��������
  if FactOp1 = nil then begin
    FactOp1 := TValue.Create(nil);
    FactOp1.ResVal := NULL;
  end;
  if FactOp2 = nil then begin
    FactOp2 := TValue.Create(nil);
    FactOp2.ResVal := NULL;
  end;

  // ����� ����� ���� ������ � TValue � TMembershipFunction
  // ���� ��������, �������� �� ���������� �������� ��������
  // � �������� ����, ���� ���� (� ����� ��������� ���������� ��� ������)
  // ������ ����� ���������� EvalOp � ���������

  // ������ ������� �������� ������, �� �������� �� �����������
  
  Result := nil;

  if EvaluationPossible() then begin
    Result := FactOp1.EvalOp(OpMeta, FactOp2);
    if (Result <> nil) and VarIsNull(TValue(Result).ResVal) then
      Result := nil;
  end;
end;


procedure TOperation.SpawnSubgoals(Env: TInstance; GL: TList);
begin
  // ���� �������� ��������, ������ ������� �� ������ ����!
  Operand1.SpawnSubgoals(Env, GL);
  if Operand2 <> nil then
    Operand2.SpawnSubgoals(Env, GL);
end;


/////////////////////////////////////////
// ������ ������ TLibFunction
/////////////////////////////////////////

constructor TLibFunction.Create(AOwner: TComponent);
begin
  inherited;
  Operands := TList.Create;
end;

destructor TLibFunction.Destroy;
begin
  Operands.Clear;
  Operands.Free;
  inherited;
end;

procedure TLibFunction.XML(Parent: IXMLNode);
var
  N: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sFunc);

  case FuncMeta of
  lfCtg:        N.Attributes[sID] := sCtg;
  lfTg:         N.Attributes[sID] := sTg;
  lfCos:        N.Attributes[sID] := sCos;
  lfSin:        N.Attributes[sID] := sSin;
  lfArcsin:     N.Attributes[sID] := sArcsin;
  lfArccos:     N.Attributes[sID] := sArccos;
  lfArctg:      N.Attributes[sID] := sArctg;
  lfArcctg:     N.Attributes[sID] := sArcctg;
  lfAbs:        N.Attributes[sID] := sAbs;
  lfPI:         N.Attributes[sID] := sPI;
  lfCeil:       N.Attributes[sID] := sCeil;
  lfFloor:      N.Attributes[sID] := sFloor;
  lfFrac:       N.Attributes[sID] := sFrac;
  lfInt:        N.Attributes[sID] := sInt;
  lfRound:      N.Attributes[sID] := sRound;
  lfTrunc:      N.Attributes[sID] := sTrunc;
  lfExp:        N.Attributes[sID] := sExp;
  lfLn:         N.Attributes[sID] := sLn;
  lfLog2:       N.Attributes[sID] := sLog2;
  lfLog10:      N.Attributes[sID] := sLog10;
  lfSqrt:       N.Attributes[sID] := sSqrt;
  lfSqr:        N.Attributes[sID] := sSqr;
  lfMin:        N.Attributes[sID] := sMin;
  lfMax:        N.Attributes[sID] := sMax;
  lfOut:        N.Attributes[sID] := sOut;
  lfConcat:     N.Attributes[sID] := sConcat;
  lfSend:       N.Attributes[sID] := sSend;
  else          N.Attributes[sID] := '';
  end;

  for i:=0 to Operands.Count-1 do
    TValue(Operands[i]).XML(N);
end;

function TLibFunction.EvaluationPossible: boolean;
begin
  Result := true;

end;

function TLibFunction.Evaluate(Env: TInstance; Locals: TList): TAbstractValue;
var
  s, ss: WideString;
  v: TValue;
  Op1, Op2: variant;
  i: integer;
  FuncRes: OleVariant;
begin
  Result := nil;
  FuncRes := Unassigned;

  if FuncMeta = lfSend then begin
    // �������� ���������, ������ �������� - ����������, ������ - ���������
    v := TAbstractValue(Operands[0]).Evaluate(Env, Locals) as TValue;
    if (v = nil) or VarIsEmpty(v.ResVal) then exit;
    s := v.ResVal;

    v := TAbstractValue(Operands[1]).Evaluate(Env, Locals) as TValue;
    ss := '';
    if v <> nil then ss := v.ResVal;

    if Assigned(OnDemon) then OnDemon(s, ss, FuncRes);
  end
  else if FuncMeta = lfConcat then begin
    // ��������� n ���������� � ��� � ���� ������ ����
    s := '';
    for i:=0 to Operands.Count-1 do begin
      v := TAbstractValue(Operands[i]).Evaluate(Env, Locals) as TValue;
      if (v = nil) or VarIsEmpty(v.ResVal) then continue;
      ss := v.ResVal; // ����� ��������� ���������������
      s := s + ss;
    end;
    FuncRes := s;
  end
  else if FuncMeta = lfOut then begin
    // ��������� n ���������� � ��� � ���� ������ ����
    s := '';
    for i:=0 to Operands.Count-1 do begin
      v := TAbstractValue(Operands[i]).Evaluate(Env, Locals) as TValue;
      if (v = nil) or VarIsEmpty(v.ResVal) then continue;
      ss := v.ResVal; // ����� ��������� ���������������
      s := s + ss;
    end;
    ShowMessage('out: ' + s);
  end
  else
  try
    // �������� �������� ���������
    if Operands.Count > 0 then begin
      v := TAbstractValue(Operands[0]).Evaluate(Env, Locals) as TValue;
      if (v = nil) or VarIsEmpty(v.ResVal) then exit;
      Op1 := v.ResVal;
    end;

    if Operands.Count > 1 then begin
      v := TAbstractValue(Operands[1]).Evaluate(Env, Locals) as TValue;
      if (v = nil) or VarIsEmpty(v.ResVal) then exit;
      Op2 := v.ResVal;
    end;

    // �������� �������� �������
    case FuncMeta of
    // ����������� �������
    lfPI:         FuncRes := System.Pi;
    // ������� �������
    lfCtg:        FuncRes := Math.Cotan(Op1);
    lfTg:         FuncRes := Math.Tan(Op1);
    lfCos:        FuncRes := System.Cos(Op1);
    lfSin:        FuncRes := System.Sin(Op1);
    lfArcsin:     FuncRes := Math.arcsin(Op1);
    lfArccos:     FuncRes := Math.arccos(Op1);
    lfArctg:      FuncRes := System.arctan(Op1);
    lfArcctg:     FuncRes := Math.arccot(Op1);
    lfAbs:        FuncRes := System.Abs(Op1);
    lfCeil:       FuncRes := Math.Ceil(Op1);
    lfFloor:      FuncRes := Math.Floor(Op1);
    lfFrac:       FuncRes := System.Frac(Op1);
    lfInt:        FuncRes := System.Int(Op1);
    lfRound:      FuncRes := System.Round(Op1);
    lfTrunc:      FuncRes := System.Trunc(Op1);
    lfExp:        FuncRes := System.Exp(Op1);
    lfLn:         FuncRes := System.Ln(Op1);
    lfLog2:       FuncRes := Math.Log2(Op1);
    lfLog10:      FuncRes := Math.Log10(Op1);
    lfSqrt:       FuncRes := System.Sqrt(Op1);
    lfSqr:        FuncRes := System.Sqr(Op1);
    // �������� �������
    lfMin:        FuncRes := Math.Min(Op1, Op2);
    lfMax:        FuncRes := Math.Max(Op1, Op2);
    end;
  except
  end;

  Result := TValue.Create(nil);
  TValue(Result).ResVal := FuncRes;
end;


procedure TLibFunction.SpawnSubgoals(Env: TInstance; GL: TList);
var
  i: integer;
begin
  // ���� �������� ��������
  for i:=0 to Operands.Count-1 do
    TAbstractValue(Operands[i]).SpawnSubgoals(Env, GL);
end;


/////////////////////////////////////////
// ������ ������ TValArray
/////////////////////////////////////////

constructor TValArray.Create(AOwner: TComponent);
begin
  inherited;
  VL := TList.Create;
end;

destructor TValArray.Destroy;
begin
  VL.Clear;
  VL.Free;
  inherited;
end;

procedure TValArray.XML(Parent: IXMLNode);
var
  N: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sValueArray);
  for i:=0 to VL.Count-1 do begin
    if VL[i] <> nil then Items[i].XML(N);
  end;
end;

procedure TValArray.SetItem(Index: integer; V: TAbstractValue);
begin
  VL[Index] := V;
end;

function TValArray.GetItem(Index: integer): TAbstractValue;
begin
  Result := VL[Index];
end;

procedure TValArray.SpawnSubgoals(Env: TInstance; GL: TList);
var
  i: integer;
begin
  // ���� �������� ��������
  for i:=0 to VL.Count-1 do
    TAbstractValue(VL[i]).SpawnSubgoals(Env, GL);
end;

end.
