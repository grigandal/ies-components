unit KB_Containers;

 {******************************************************************************
 * ������ KB_Containers.pas �������� ����������� ��������� �������:
 *
 * TXMLItem - ������� �����-��������� ��� ��������� ��, ������� ����������� � XML
 * TNamedItem - ��������� TXMLItem, ���������� ���������� ID, ������� ��� ��������� �������
 * TKBType - ��������� ��� ���� ������, ������������� � ���� ������
 * TKBContainer - ��������� ���� ������ �������� ������
 * TAttrList - �����-��������, �������� �������� ���� <����������, ��������>
 * TAbstractValue - ������ ����������� ��� �������� �������� ����-����
 * TOMContainer - ��������� ������� ������ � ��������� �������� � ��
 * TClassProperty - ��������� �������� ������ ���� ������
 * TClassMethod - ��������� ������ ������ ���� ������
 * TClassRule - ��������� ������� ������ ���� ������ (��������� � KB_Instructions.pas)
 * TInstance - ��������� ���������� ������ ���� ������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface
                                     
uses
  Classes, XMLDoc, XMLIntf, Variants, Math, SysUtils, ActiveX, Windows, 
  KB_Common, Dialogs;

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TXMLItem �������� ������� ���� �����������, ����������� � XML
  ////////////////////////////////////////////////////////////////////////////

  TXMLItem = class(TComponent)
  public
    procedure XML(Parent: IXMLNode); virtual; abstract;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TAttrList ������ ������ ��������� ���� <����������, ��������>
  ////////////////////////////////////////////////////////////////////////////

  TAttrList = class(TXMLItem)   // ������������� ���� DTD 'with'
  private
    FBelief: double;            // KB
    FProbability: double;       // KB
    FAccuracy: double;          // KB
    procedure SetBelief(V: double);
    procedure SetProbability(V: double);
    procedure SetAccuracy(V: double);
  published
    property Belief: double read FBelief write SetBelief;
    property Probability: double read FProbability write SetProbability;
    property Accuracy: double read FAccuracy write SetAccuracy;
  public
    IsBelief: boolean;
    IsProbability: boolean;
    IsAccuracy: boolean;
    procedure XML(Parent: IXMLNode); override;
    procedure Assign(Source: TPersistent); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TOMContainer �������� ����������� ������� ������
  ////////////////////////////////////////////////////////////////////////////

  TOMContainer = class(TXMLItem)
  public
    Objects: TList;             // ������ TInstance
    Goals: TList;               // ������ TGoal
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure Clear;            // ������� ����������
    function AnyGoalAchieved: boolean;  // true, ���� ���������� ���� ���� ��������� ����
    function AllGoalsAchieved: boolean; // true, ���� ���������� ��� ��������� ����
    function ConflictRulesPresent: boolean;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TKBContainer �������� ����������� ���� ������ �������� ������
  ////////////////////////////////////////////////////////////////////////////

  TKBContainer = class(TXMLItem)// ������������� ���� DTD 'knowledge-base'
  public
    CreationDate: string;       // KB, ���� �������� ���� ������
    ProblemName: string;        // KB, �������� �������� ������
    Types: TList;               // KB, ������ ����� - ������� TKBType
    Classes: TList;             // KB, ������ ������� - TKBClass
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure Clear;            // ������� ����������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TNamedItem �������� ������� ���� ������������� ����������� ��
  // ������������ ��� �������������� ������ �������� �� �� ID � �������
  // ��� ������� TNamedItem ����� ��������� � ������� TList
  ////////////////////////////////////////////////////////////////////////////

  TNamedItem = class(TXMLItem)
  public
    ID: WideString;             // ������������� ��������
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ������� ����� TKBType �������� ����������� ����, ������������ � ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TAbstractValue = class;       // please wait...

  TKBType = class(TNamedItem)   // ������������� ���� DTD 'type'
  public
    Desc: WideString;           // KB, �������� ����
    function Valid(Value: TAbstractValue): boolean; virtual; // ��������� �������� �� �������������� ����
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TInstance �������� ����������� ��� ���������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TInstance = class(TXMLItem)   // ������������� ���� DTD 'object'
  public
    PropertyList: TList;        // OM, ������ TIDValue �� ���������� ������� �������

    ClassID: WideString;        // OM, �������� ������
    ClassRef: TKBType;          // ������ �� �����, ���� ����������� �������� ������ ������

    RuleStates: TList;          // OM, ������ TRuleState ��������� ������
    ConflictRules: TList;       // OM, ������ TConflictRec

    OMRef: TOMContainer;        // ������ �� ������� ������
    FObj: IDispatch;            // ������ �� COM-������ (���� ����������� �� COM ��� ������� ��������� COM)
    TypeInfo: ITypeInfo;        // ������ �� ��������������� ���������� �����
    TypeAttr: PTypeAttr;        // COM-�������� �������

    // runtime-���������, �������� ������ ��� ��� ������ ������ ��� ��������
    RtmParams: TList;       // ����������� ������� TAbstractValue ��� ������ ������� ��� �������, ������������� �����
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    procedure XML(Parent: IXMLNode); override;
    procedure SetClassRef(CR: TKBType);
  private
    procedure SetObj(Value: IDispatch);
    function GetIntfName(Intf: IDispatch): WideString; // ������� ��� dispinterface
  public
    property Obj: IDispatch read FObj write SetObj;
    // ��������� � COM ��������
    procedure SetProperty(ID: WideString; Value: TAbstractValue; Asked: boolean);
    function GetProperty(ID: WideString): TAbstractValue;
    function GetIDValue(ID: WideString): TNamedItem; //TIDValue;
  // ��������� ��� COM-��������
  private
    function GetMemberDesc(MemID, InvKind: integer): PFuncDesc;
    function FormatParams(fd: PFuncDesc): DISPPARAMS;
    function Variant2TagVariant(v: variant): tagVariant;
    function TagVariant2Variant(tv: tagVariant): variant;
    function OleMethod(MemName: WideString; var Val: Variant): boolean; // ���������� false, ���� ������ ���
    function OlePropertyGet(MemName: WideString; var Val: Variant): boolean; // ���������� false, ���� �������� ���
    function OlePropertySet(MemName: WideString): boolean; // ���������� false, ���� �������� ���
  public
    function COMInvoke(ID: WideString): TAbstractValue; // ������ COM ��������� ������ TClassMethod.Interpete
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����������� ������� ����� TAbstractValue �������� ������� ���� ����������� ������
  ////////////////////////////////////////////////////////////////////////////

  PValue = ^TAbstractValue;
  TAbstractValue = class(TXMLItem)
  public
    TypeMeta: TTypeMeta;        // ������� �������� (��� runtime)
    TypeRef: TKBType;           // ������ �� ��� �������� (��� desisn-time � run-time)
    Attrs: TAttrList;           // �������� �������� (�����������, �������� � �.�.)
    // ��������� ����������� ��������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Evaluate(Env: TInstance; Locals: TList): TAbstractValue; virtual; abstract;
    procedure SpawnSubgoals(Env: TInstance; GL: TList); virtual; abstract; // ���������� ������� �� ������� ������ Env
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassProperty �������� ����������� ��� �������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassProperty = class(TNamedItem) // ������������� ���� DTD 'property'
  public
    Desc: WideString;           // KB, �������� ��������
    TypeRef: TKBType;           // KB, ��� �������� (�� ������)
    Source: TPropertySource;    // KB, ������ ��������� �������� ��������
    Question: WideString;       // KB, ������ ��� ��������� �������� ��������
    Query: WideString;          // KB, ������ � �� ��� ��������� �������� ��������
    Default: TAbstractValue;    // KB, �������� �� ���������
    ShallCreate: boolean;       // KB, ������� �� ��������� ��������-������ ��� �������� ���������� ������ ������
    WasInherited: boolean;      // ����������� � ��, �������� ���� �� ��� �������� ����������� �� �������
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassMethod �������� ����������� ��� ������ ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassMethod = class(TNamedItem) // ������������� ���� DTD 'method'
  public
    Desc: WideString;           // KB, �������� ������
    ResultTypeRef: TKBType;     // KB, ��� ������������� �������� (�� ������)
    ParamsDecl: TList;          // KB, ������ ���������� ������ - TParamDecl
    Instructions: TList;        // KB, ������ ���������� ������ - �������� TInstruction
    WasInherited: boolean;      // ��� �� ���� ����� ���������� �� �������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    // Locals - ������ TIDValue �� ���������� ��������� ����������
    function Interprete(Env: TInstance; Locals: TList): TAbstractValue; // �� ����� ���� TValue
  end;


  // ���������� ������ ��������� �������� � ������ ID � ������ List
  function GlbFindItem(List: TList; ID: WideString): TNamedItem;


implementation


uses KB_Values, OM_Containers, KB_Instructions, KB_Types;


///////////////////////////////////////////////////////////////////
// ���������� ������ ��������� �������� � ID � ������
///////////////////////////////////////////////////////////////////

function GlbFindItem(List: TList; ID: WideString): TNamedItem;
var
  i: integer;
  Item: TNamedItem;
begin
  Result := nil;
  i := 0;
  if List <> nil then
  while i<List.Count do begin
    Item := List[i];
    if (Item <> nil) and (AnsiCompareText(Item.ID, ID) = 0) then begin
      Result := Item;
      break;
    end;
    Inc(i);
  end;
end;


/////////////////////////////////////////
// ������ ������ TKBType
/////////////////////////////////////////

procedure TKBType.XML(Parent: IXMLNode);
begin
  Parent.AddChild('kb-type');   // ��������, ����������� ��������������
end;

function TKBType.Valid(Value: TAbstractValue): boolean;
begin
  Result := True;               // ��������, ����������� �������������� � ��������
end;


/////////////////////////////////////////
// ������ ������ TAttrList
/////////////////////////////////////////

procedure TAttrList.SetBelief(V: double);
begin
  FBelief := V;
  IsBelief := true;
end;

procedure TAttrList.SetProbability(V: double);
begin
  FProbability := V;
  IsProbability := true;
end;

procedure TAttrList.SetAccuracy(V: double);
begin
  FAccuracy := V;
  IsAccuracy := true;
end;

procedure TAttrList.XML(Parent: IXMLNode);
var
  N: IXMLNode;
begin
  if IsBelief or IsProbability or IsAccuracy then begin
    N := Parent.AddChild(sWith);
    if IsBelief then N.Attributes[sBelief] := Round(Belief * 100);
    if IsProbability then N.Attributes[sProbability] := Round(Probability * 100);
    if IsAccuracy then N.Attributes[sAccuracy] := Round(Accuracy * 100);
  end;
end;

procedure TAttrList.Assign(Source: TPersistent);
var S: TAttrList;
begin
  if Source is TAttrList then begin
    S := Source as TAttrList;
    
    Belief := S.Belief;
    Probability := S.Probability;
    Accuracy := S.Accuracy;

    IsBelief := S.IsBelief;
    IsProbability := S.IsProbability;
    IsAccuracy := S.IsAccuracy;
  end;
end;


/////////////////////////////////////////
// ������ ������ TOMContainer
/////////////////////////////////////////

constructor TOMContainer.Create(AOwner: TComponent);
begin
  inherited;
  Objects := TList.Create;
  Goals := TList.Create;
end;

destructor TOMContainer.Destroy;
begin
  Goals.Free;
  Objects.Free;
  inherited;
end;

procedure TOMContainer.XML(Parent: IXMLNode);
var
  i: integer;
  N, M: IXMLNode;
begin
  N := Parent.AddChild(sOperatingMemory);

  // ������ ��� �������
  M := N.AddChild(sObjects);
  for i := 0 to Objects.Count-1 do
    TXMLItem(Objects[i]).XML(M);        // TInstance

  // ������ ����
  M := N.AddChild(sGoals);
  for i := 0 to Goals.Count-1 do
    TXMLItem(Goals[i]).XML(M);          // TGoal
end;

procedure TOMContainer.Clear;
begin
  Goals.Clear;
  Objects.Clear;
end;

// ���������, ���������� ���� ���� ��������� ����
function TOMContainer.AnyGoalAchieved: boolean;
var
  i: integer;
  G: TGoal;
begin
  i := 0;
  Result := false;
  while i < Goals.Count do begin
    G := Goals[i];
    if G.Primary and G.Achieved then begin
      Result := true;
      break;
    end;
    Inc(i);
  end;
end;

function TOMContainer.AllGoalsAchieved: boolean;
var
  i: integer;
  G: TGoal;
begin
  i := 0;
  Result := true;
  while i < Goals.Count do begin
    G := Goals[i];
    if G.Primary and not G.Achieved then begin
      Result := false;
      break;
    end;
    Inc(i);
  end;
end;

// true, ���� ���� ���� �� ���� ������� � ����������� ������
function TOMContainer.ConflictRulesPresent: boolean;
var
  i: integer;
  obj: TInstance;
begin
  Result := false;
  i:=0;
  while i<Objects.Count do begin
    obj := Objects[i];
    if obj.ConflictRules.Count > 0 then begin
      Result := true;
      break;
    end;
    Inc(i);
  end;
end;

/////////////////////////////////////////
// ������ ������ TKBContainer
/////////////////////////////////////////

constructor TKBContainer.Create(AOwner: TComponent);
begin
  inherited;
  Types := TList.Create;
  Classes := TList.Create;
end;

destructor TKBContainer.Destroy;
begin
  Types.Free;
  Classes.Free;
  inherited;
end;

procedure TKBContainer.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sKnowledgeBase);
  N.Attributes[sCreationDate] := CreationDate;
  N.AddChild(sProblemInfo);
  N.ChildNodes[sProblemInfo].Text := ProblemName;

  M := N.AddChild(sTypes);
  for i := 0 to Types.Count-1 do
    TKBType(Types[i]).XML(M);

  M := N.AddChild(sClasses);
  for i := 0 to Classes.Count-1 do
    TKBClass(Classes[i]).XML(M);
end;

procedure TKBContainer.Clear;
begin
  Types.Clear;
  Classes.Clear;
end;


/////////////////////////////////////////
// ������ ������ TClassProperty
/////////////////////////////////////////


procedure TClassProperty.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sProperty);
  N.Attributes[sID] := ID;

  if TypeRef <> nil then
    N.Attributes[sType] := (TypeRef as TKBType).ID;

  N.Attributes[sDesc] := Desc;

  case Source of
    psQuestion: N.Attributes[sSource] := sQuestion;
    psQuery:    N.Attributes[sSource] := sQuery;
    psInferred: N.Attributes[sSource] := sInferred;
    psSupplied: N.Attributes[sSource] := sSupplied;
    psNone:     N.Attributes[sSource] := sNone;
  end;

  if Query <> '' then begin
    N.AddChild(sQuery);
    N.ChildNodes[sQuery].Text := Query;
  end;

  if Question <> '' then begin
    N.AddChild(sQuestion);
    N.ChildNodes[sQuestion].Text := Question;
  end;

  if ShallCreate then begin
    N.Attributes[sCreate] := 'true';
  end;

  if Default <> nil then begin
    N.Attributes[sDefault] := 'true';
    Default.XML(N);
  end;
end;


/////////////////////////////////////////
// ������ ������ TClassMethod
/////////////////////////////////////////

constructor TClassMethod.Create(AOwner: TComponent);
begin
  inherited;
  ParamsDecl := TList.Create;
  Instructions := TList.Create;
end;

destructor TClassMethod.Destroy;
begin
  ParamsDecl.Free;
  Instructions.Free;
  inherited;
end;

procedure TClassMethod.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sMethod);
  N.Attributes[sID] := ID;

  if ResultTypeRef <> nil then
    N.Attributes[sResultType] := (ResultTypeRef as TKBType).ID;

  N.Attributes[sDesc] := Desc;

  if ParamsDecl.Count > 0 then begin
    M := N.AddChild(sParams);
    for i:=0 to ParamsDecl.Count-1 do
      TXMLItem(ParamsDecl[i]).XML(M);
  end;

  M := N.AddChild(sStatements);
  for i:=0 to Instructions.Count-1 do
    TXMLItem(Instructions[i]).XML(M);
end;

function TClassMethod.Interprete(Env: TInstance; Locals: TList): TAbstractValue;
var
  i: integer;
  Inst: TInstruction;
  Attrs: TAttrList;
begin
  // ��������� ����������� ��������� � ������������ �� ������� ����������� ����������
  if (Locals <> nil) and (Locals.Count >= ParamsDecl.Count) then begin
    for i:=0 to ParamsDecl.Count-1 do begin
      TIDValue(Locals[i]).ID := TParamDecl(ParamsDecl[i]).ID;
    end;
  end;

  Result := nil;
  i := 0;
  while i<Instructions.Count do begin
    Inst := Instructions[i];
    Inc(i);
    if Inst.Disabled then continue; // ������� ��������� �� continue
    Attrs := nil;
    Result := Inst.Interprete(Env, Attrs, Locals);
    if Inst.ShallStop then break;
  end;

  // !!! �� ���������, ���� �� Locals ���������
  // ������� �� instance � progid �� ������� ref �� Locals?
  // �������, ���� ������� ������ instance = 1
  Locals.Clear; 
end;


////////////////////////////////////////
// ������ ������ TInstance
/////////////////////////////////////////

constructor TInstance.Create(AOwner: TComponent);
begin
  inherited;
  PropertyList := TList.Create;
  RuleStates := TList.Create;
  ConflictRules := TList.Create;
  FObj := nil; // ������ FObj, � �� Obj, ��� ��� ����� SetObj ���������, � ��� TLB ���������
end;

destructor TInstance.Destroy;
begin
  ConflictRules.Free;
  RuleStates.Free;
  PropertyList.Free;
  inherited;
end;

procedure TInstance.Clear;
begin
  RuleStates.Clear;
  ConflictRules.Clear;
end;

procedure TInstance.XML(Parent: IXMLNode);
var
  N,M : IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sObject);
  N.Attributes[sAddr] := '$'+IntToHex(integer(Self),8);
  N.Attributes[sType] := ClassID;

  // ������ �������� �������
  M := N.AddChild(sProperties);
  for i:=0 to PropertyList.Count-1 do
    TXMLItem(PropertyList[i]).XML(M);           // TIDValue

  // ������ ��������� ������
  M := N.AddChild(sRuleStates);
  for i := 0 to RuleStates.Count-1 do
    TXMLItem(RuleStates[i]).XML(M);             // TRuleState

  // ������ ����������� �����
  M := N.AddChild(sCRS);
  for i := 0 to ConflictRules.Count-1 do
    TXMLItem(ConflictRules[i]).XML(M);          // TConflictRec

end;

procedure TInstance.SetClassRef(CR: TKBType);
var
  i: integer;
  R: TClassRule;
  rs: TRuleState;
  C: TKBClass;
begin
  // ��������� ����� ������ �� ����� � ������� RuleStates
  // ����� ������� CreateInstance ������ �������� ��������� ������������� ������
  ClassRef := CR;
  C := ClassRef as TKBClass;
  for i:=0 to C.Rules.Count-1 do begin
    R := TClassRule(C.Rules[i]);
    rs := TRuleState.Create(nil);
    rs.Rule := R;
    rs.Active := true;
    RuleStates.Add(rs);
  end;
end;

procedure TInstance.SetObj(Value: IDispatch);
begin
  FObj := Value;
  try
    FObj.GetTypeInfo(0, LOCALE_USER_DEFAULT, TypeInfo);
    TypeInfo.GetTypeAttr(TypeAttr);
  except
    ErrorHandler.HandleError(eTypeInfoErr, '���������� ����� ��� ������� "'+IntToHex(integer(Self),8)+'": '+ClassID+' ����������');
  end;
end;

procedure TInstance.SetProperty(ID: WideString; Value: TAbstractValue; Asked: boolean);
var
  idv: TIDValue;
begin
  // ��������� ����� �������� � �������� ��� ��������

  idv := GlbFindItem(PropertyList, ID) as TIDValue;
  if idv <> nil then begin
    // ���� �������� ���������, �� ������� ����������� ������������
    if idv.Value <> nil then begin
      if (idv.Value as TValue).ResVal = (Value as TValue).ResVal then begin
        JoinEvidences(idv.Value.Attrs, Value.Attrs,  idv.Value.Attrs);
      end
      else begin
        idv.Value.Free;
        idv.Value := Value as TValue;
        idv.Asked := Asked;
      end
    end
    else begin
      idv.Value := Value as TValue;
      idv.Asked := Asked;
    end;
    exit;
  end;

  // ���� �� �����, �� ������ COM-��������
  RtmParams := Stack.Last;
  RtmParams.Add(Value); // ������� ����� �������� ��� ��������� ��������
  if (Obj <> nil) and OlePropertySet(ID) then exit;

  // ���� ����� �� ����� �� �������� ����� ���� <��������, ��������>
  // ����� ����������������� ����� �������� ID ������������ � ClassRef
  idv := TIDValue.Create(self);
  idv.ID := ID;
  idv.InstRef := Self;
  idv.Value := Value as TValue;
  idv.Asked := Asked;
  PropertyList.Add(idv);
end;

function TInstance.GetProperty(ID: WideString): TAbstractValue;
var
  idv: TIDValue;
  OleVal: variant;
begin
  // ���� �������� ����� ���������, �� ��� ������ ����� � RtmParams
  RtmParams := Stack.Last;
  Result := nil; // ���� �� ������

  // ��������� ����� �������� � ������� ��� ��������
  idv := GlbFindItem(PropertyList, ID) as TIDValue;
  if idv <> nil then begin
    Result := IDV.Value;
    exit;
  end;

  // ���� �� �����, �� ������ COM-��������
  if (Obj <> nil) and OlePropertyGet(ID, OleVal) then begin
    Result := TValue.Create(nil);
    TValue(Result).ResVal := OleVal;
    exit;
  end;

  // ���� ��� � �� �����, �� �������� �� ��������� ��� �� ���������� ������ ��������,
  // �� ��������� �� ID ���� ����� � ���������� ����������, �� ������ ���������
  // ErrorHandler.HandleError(eInvalidID, 'Instance of '+ClassID+': ������� '+ID+' �� �������� ��������� ��� ��� �� ��������� ��������');
end;

function TInstance.GetIDValue(ID: WideString): TNamedItem; //TIDValue;
var
  OleVal: variant;
  IDValue: TIDValue;
begin
  // ��������� ����� �������� � ������� ��� ��������
  Result := GlbFindItem(PropertyList, ID) as TIDValue;

  // ���� �� �����, �� ������ COM-��������
  if (Result = nil) and (Obj <> nil) and OlePropertyGet(ID, OleVal) then begin
    IDValue := TIDValue.Create(nil);
    IDValue.ID := ID;
    IDValue.Value := TValue.Create(nil);
    TValue(IDValue.Value).ResVal := OleVal;
    Result := IDValue;
  end;
end;


function TInstance.COMInvoke(ID: WideString): TAbstractValue;
var OleVal: variant;
begin
  Result := nil;
  RtmParams := Stack.Last;
  if (Obj <> nil) and OleMethod(ID, OleVal) then begin
    Result := TValue.Create(nil);
    TValue(Result).ResVal := OleVal;
  end;
end;

function TInstance.GetMemberDesc(MemId, InvKind: integer): PFuncDesc;
var
  i: integer;
  fd: PFuncDesc;
begin
  Result := nil;
  fd := nil;
  i := 0;
  while i < TypeAttr.cFuncs do begin
    // ���� � ������������ � invkind, ����� ����������� �������� get � set
    TypeInfo.GetFuncDesc(i, fd);
    if (fd.memid = MemID) and (fd.invkind and InvKind <> 0) then begin
      Result := fd;
      break;
    end;
    inc(i);
  end;
end;

// ��������������� ���������� ��� � �������� tagVariant
{
tagVARIANT = record
    vt: TVarType;
    wReserved1: Word;
    wReserved2: Word;
    wReserved3: Word;
    case Integer of
      VT_UI1:                  (bVal: Byte);
      VT_I2:                   (iVal: Smallint);
      VT_I4:                   (lVal: Longint);
      VT_R4:                   (fltVal: Single);
      VT_R8:                   (dblVal: Double);
      VT_BOOL:                 (vbool: TOleBool);
      VT_ERROR:                (scode: HResult);
      VT_CY:                   (cyVal: Currency);
      VT_DATE:                 (date: TOleDate);
      VT_BSTR:                 (bstrVal: PWideChar);
      VT_UNKNOWN:              (unkVal: Pointer);
      VT_DISPATCH:             (dispVal: Pointer);
      VT_ARRAY:                (parray: PSafeArray);
      VT_BYREF or VT_UI1:      (pbVal: ^Byte);
      VT_BYREF or VT_I2:       (piVal: ^Smallint);
      VT_BYREF or VT_I4:       (plVal: ^Longint);
      VT_BYREF or VT_R4:       (pfltVal: ^Single);
      VT_BYREF or VT_R8:       (pdblVal: ^Double);
      VT_BYREF or VT_BOOL:     (pbool: ^TOleBool);
      VT_BYREF or VT_ERROR:    (pscode: ^HResult);
      VT_BYREF or VT_CY:       (pcyVal: ^Currency);
      VT_BYREF or VT_DATE:     (pdate: ^TOleDate);
      VT_BYREF or VT_BSTR:     (pbstrVal: ^WideString);
      VT_BYREF or VT_UNKNOWN:  (punkVal: ^IUnknown);
      VT_BYREF or VT_DISPATCH: (pdispVal: ^IDispatch);
      VT_BYREF or VT_ARRAY:    (pparray: ^PSafeArray);
      VT_BYREF or VT_VARIANT:  (pvarVal: PVariant);
      VT_BYREF:                (byRef: Pointer);
      VT_I1:                   (cVal: Char);
      VT_UI2:                  (uiVal: Word);
      VT_UI4:                  (ulVal: LongWord);
      VT_INT:                  (intVal: Integer);
      VT_UINT:                 (uintVal: LongWord);
      VT_BYREF or VT_DECIMAL:  (pdecVal: PDecimal);
      VT_BYREF or VT_I1:       (pcVal: PChar);
      VT_BYREF or VT_UI2:      (puiVal: PWord);
      VT_BYREF or VT_UI4:      (pulVal: PInteger);
      VT_BYREF or VT_INT:      (pintVal: PInteger);
      VT_BYREF or VT_UINT:     (puintVal: PLongWord);
  end;
}

function TInstance.Variant2TagVariant(v: Variant): tagVariant;
begin
  if VarIsType(v, varVariant) then begin
    Result.vt := VT_VARIANT;
    Result.pvarVal := @v;
  end
  else if VarIsType(v, varBoolean) then begin
    Result.vt := VT_BOOL;
    Result.vbool := v;
  end
  else if VarIsStr(v) then begin
    Result.vt := VT_BSTR;
    Result.bstrVal := StringToOleStr(v);
  end
  else if VarIsType(v, varDispatch) then begin
    Result.vt := VT_DISPATCH;
    Result.pdispVal := pointer(integer(v));
  end
  else if VarIsType(v, varUnknown) then begin
    Result.vt := VT_UNKNOWN;
    Result.unkVal := pointer(integer(v));
  end
  else if VarIsType(v, varError) then begin
    Result.vt := VT_ERROR;
    Result.scode := v;
  end
  else if VarIsType(v, varDate) then begin
    Result.vt := VT_DATE;
    Result.date := v;
  end
  else if VarIsType(v, varCurrency) then begin
    Result.vt := VT_CY;
    Result.cyVal := v;
  end
  else if VarIsType(v, varArray) then begin
    Result.vt := VT_ARRAY;
    Result.parray := pointer(integer(v));
  end
  else if VarIsType(v, varByRef) then begin
    Result.vt := VT_BYREF;
    Result.byRef := pointer(integer(v));
  end
  else if VarIsOrdinal(v) then begin
    Result.vt := VT_I4;
    Result.lVal := v;
  end
  else if VarIsStr(v) then begin
    Result.vt := VT_R8;
    Result.dblVal := v;
  end
  else if VarIsNull(v) then begin
    Result.vt := VT_NULL;
  end
  else begin
    Result.vt := VT_PTR;
    Result.intVal := v;
  end
end;

// ��������������� �������� tagVariant � ����������� ����
function TInstance.TagVariant2Variant(tv: tagVariant): variant;
begin
  case tv.vt of
  VT_I1, VT_I2, VT_I4, VT_INT,
  VT_UI1, VT_UI2, VT_UI4, VT_UINT,
  VT_DECIMAL:           Result := tv.lVal;
  VT_R4:                Result := tv.fltVal;
  VT_R8:                Result := tv.dblVal;
  VT_CY:                Result := tv.cyVal;
  VT_DATE:              Result := tv.date;
  VT_BSTR:              Result := OleStrToString(tv.bstrVal);
  VT_DISPATCH:          Result := integer(tv.pdispVal);
  VT_UNKNOWN:           Result := integer(tv.punkVal);
  VT_ERROR:             Result := tv.scode;
  VT_HRESULT:           Result := tv.scode;
  VT_BOOL:              Result := tv.vbool;
  VT_VOID:              Result := NULL;
  VT_SAFEARRAY:         Result := integer(tv.parray);
  VT_CARRAY:            Result := integer(tv.parray);
  VT_VARIANT:           Result := integer(tv.byRef);
  VT_PTR:               Result := integer(tv.byRef);
  VT_USERDEFINED:       Result := integer(tv.byRef);
  VT_LPSTR:             Result := integer(tv.byRef);
  VT_LPWSTR:            Result := integer(tv.byRef);
  VT_EMPTY:             Result := unassigned; // ����������� ������
  else
    Result := tv.lVal;
  end;
end;

// ��������������� ������ ���������� TIDValue � ��������� DISPPARAMS
function TInstance.FormatParams(fd: PFuncDesc): DISPPARAMS;
var
  DParams: DISPPARAMS;
  ElemDesc: tagELEMDESC;
  i, k: integer;
  Val: TValue;
  tv: tagVariant;
  inst: TInstance;
begin
  // ���� ������� dispid, �� �������������� ����� ����������� ������� dispid ��������, ���� ���, �� ����� �������
  // ���������� ��������� ���� � ����� ������, ����� ��� �������������
  k := RtmParams.Count; // fd.cParams;

  DParams.cArgs := k;
  DParams.cNamedArgs := k;

  // COMParams ���� ������������� � DISPPARAMS
  if fd.cParams > 0 then begin

    // ������� ������ ��� ����������� ���������
    GetMem(DParams.rgvarg, sizeof(TVariantArg)*k);
    // ������� ������ ��� DispID ������������� ���������� (���� ������� ��, �� � ������� �� ������)
    GetMem(DParams.rgdispidNamedArgs, sizeof(TDispID)*k);

    // ������ dispid ������������� ���������� (������ rgdispidNamedArgs)
    // ����� ������� �� ����������, ����� ���� � ����� ��������, � ������ dispid ������� �����
    for i:=0 to k-1 do begin
      DParams.rgdispidNamedArgs[i] := i;
    end;

    // �������� �������� ���������� (������ rgvarg)
    // !!! ��������� ������ ���������� [in, out] � ��������� [optional]

    for i := 0 to k-1 do begin
      val := RtmParams[i];
      tv := Variant2TagVariant(val.ResVal);

      // ������������� ��� ��������� � ������������ � TypeLib
      // ���������: ����  � tlb �������, �� �� ������������ ��� - ���� ����������
      ElemDesc := fd.lprgelemdescParam[i];
      if ElemDesc.tdesc.vt <> VT_VARIANT then
        tv.vt := ElemDesc.tdesc.vt;

      // ���� ��������� IDispatch, �� ������� Obj �� ������ �� TInstance � ���������
      if ElemDesc.tdesc.vt = VT_DISPATCH then begin
        inst := TInstance(integer(val.ResVal));
        if inst <> nil then tv.pdispVal := @inst.Obj
        else tv.pdispVal := nil;
      end;

      // ����� ���� �������� ��� ���������, ���� �� out, �� ���� ��� byref ����������, � �������� ����� � �� ����
      if boolean(ElemDesc.paramdesc.wParamFlags and PARAMFLAG_FOUT) then begin
        DParams.rgvarg[i].vt := VT_BYREF;
        DParams.rgvarg[i].byRef := @tv;
      end
      else
        DParams.rgvarg[i] := tv;
    end;

    // NB! ������������ �������� � ����� ��������� �������� ������ ����� dispid=DISPID_PROPERTYPUT, � �� ���������� �����
    // ������ ����� �� ��������� � ������ ����������
    if (fd.invkind = INVOKE_PROPERTYPUT) or (fd.invkind = INVOKE_PROPERTYPUTREF) then
      DParams.rgdispidNamedArgs[fd.cParams-1] := DISPID_PROPERTYPUT;
  end;

  Result := DParams;
end;

function TInstance.OleMethod(MemName: WideString; var Val: Variant): boolean;
var
  MemID: integer;
  DParams: DISPPARAMS;
  fd: PFuncDesc;
  VarResult: tagVariant;
  i: integer;
  h: HRESULT;
begin
  Result := true;

  // ������ dispid �������� � ������ MemName
  Obj.GetIDsOfNames(GUID_NULL, @MemName, 1, LOCALE_USER_DEFAULT, @MemID);
  if MemID = -1 then begin
    Result := false;
    exit;
  end;
  
  // ��������� �����, ��� TDispID � �������������, ��� ��� �����
  fd := GetMemberDesc(MemID, INVOKE_FUNC);
  if fd = nil then begin
    // ������ ��� ����� �������, ������...
    Result := false;
    exit;
  end;


  // ���������� ���������, ��������� ������ COMParams
  DParams := FormatParams(fd);

  h := 0;
  try
    h := Obj.Invoke(MemID, GUID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, DParams, @VarResult, nil, nil);
  except
    on E: Exception do ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to invoke method "'+MemName+'", hresult: '+IntToHex(h,8) + ', exception: '+ E.Message);
  end;
  if h <> 0 then ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to invoke method "'+MemName+'", hresult: '+IntToHex(h,8));

  // �������� [out] ���������
  for i:=0 to RtmParams.Count-1 do begin // ���� �� ��� (DParams.cArgs-1)
    TValue(RtmParams[i]).ResVal := TagVariant2Variant(DParams.rgvarg[i]);
  end;

  // ������� ������������ �������� [out, retval]
  Val := TagVariant2Variant(VarResult);
end;

function TInstance.GetIntfName(Intf: IDispatch): WideString;
var
  TypeInfo: ITypeInfo;
  TypeAttr: PTypeAttr;
begin
  Intf.GetTypeInfo(0, LOCALE_USER_DEFAULT, TypeInfo);
  TypeInfo.GetTypeAttr(TypeAttr);
  TypeInfo.GetDocumentation(TypeAttr.memidConstructor, @Result, nil, nil, nil);
end;

function TInstance.OlePropertyGet(MemName: WideString; var Val: Variant): boolean;
var
  MemID: integer;
  Params: DISPPARAMS;
  fd: PFuncDesc;
  VarResult: tagVariant;
  Inst: TInstance;
  h: HRESULT;
begin
  Result := true;

  // ������ dispid �������� � ������ MemName
  Obj.GetIDsOfNames(GUID_NULL, @MemName, 1, LOCALE_USER_DEFAULT, @MemID);
  if MemID = -1 then begin
    Result := false;
    exit;
  end;

  // ��������� �����, ��� TDispID � �������������, ��� ��� ��������
  fd := GetMemberDesc(MemID, INVOKE_PROPERTYGET);
  if fd = nil then begin
    // ������ ��� ������ ��������, ��� ���������, ���� �� ����� ���� ��� �����
    Result := false;
    exit;
  end;

  // ���������� ���������, ��������� ������ COMParams
  Params := FormatParams(fd);

  // ��� ��������� ������ ���� ������� � DISPATCH_PROPERTYGET � DISPATCH_METHOD...
  h := 0;
  try
    h := Obj.Invoke(MemID, GUID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET or DISPATCH_METHOD, Params, @VarResult, nil, nil);
  except
    on E: Exception do ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to to get property "'+MemName+'", hresult: '+IntToHex(h,8) + ', exception: '+ E.Message);
  end;
  if h <> 0 then ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to get property "'+MemName+'", hresult: '+IntToHex(h,8));

  // �������� ��� ������������� ��������
  // ���� �������� �������� ����, �� ������ ��� � ResVal
  // � ���� ��� IDispatch, �� ���� � ResVal ������� TInstance �� ��������� Obj = ����� IDispatch

  // ���� ���� ������, �� ������ 0
  if h <> 0 then Val := 0
  else
  if VarResult.vt = VT_DISPATCH then begin
    Inst := TInstance.Create(nil);
    Inst.Obj := IDispatch(VarResult.dispVal);
    Inst.ClassID := GetIntfName(Inst.Obj); // �������� �� Obj ��� ��� ����������
    Val := integer(Inst);
  end
  else
    Val := TagVariant2Variant(VarResult);
end;

function TInstance.OlePropertySet(MemName: WideString): boolean;
var
  MemID: integer;
  Params: DISPPARAMS;
  fd: PFuncDesc;
  Flags: integer;
  h: HRESULT;
begin
  Result := true;

  // ������ dispid �������� � ������ MemName
  Obj.GetIDsOfNames(GUID_NULL, @MemName, 1, LOCALE_USER_DEFAULT, @MemID);
  if MemID = -1 then begin
    Result := false;
    exit;
  end;
  
  // ��������� �����, ��� TDispID � �������������, ��� ��� ��������
  fd := GetMemberDesc(MemID, INVOKE_PROPERTYPUT or INVOKE_PROPERTYPUTREF);
  if fd = nil then begin
    // ������ ��� ������ ��������, ��� ���������, ���� �� ����� ���� ��� �����
    Result := false;
    exit;
  end;


  // ���������� ���������, ��������� ������ COMParams
  Params := FormatParams(fd);

  Flags := DISPATCH_PROPERTYPUT;
  if fd.invkind = INVOKE_PROPERTYPUTREF then Flags := DISPATCH_PROPERTYPUTREF;

  h := 0;
  try
    h := Obj.Invoke(MemID, GUID_NULL, LOCALE_USER_DEFAULT, Flags, Params, nil, nil, nil);
  except
    on E: Exception do ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to to set property "'+MemName+'", hresult: '+IntToHex(h,8) + ', exception: '+ E.Message);
  end;
  if h <> 0 then ErrorHandler.HandleError(eInvokeFailed, 'Instance '+IntToHex(integer(Self),8)+' of class "'+ClassID+'" failed to set property "'+MemName+'", hresult: '+IntToHex(h,8));
end;


/////////////////////////////////////////
// ������ ������ TAbstractValue
/////////////////////////////////////////

constructor TAbstractValue.Create(AOwner: TComponent);
begin
  inherited;
  Attrs := TAttrList.Create(Self);
  TypeRef := nil;
end;

destructor TAbstractValue.Destroy;
begin
  Attrs.Free;
  inherited;
end;


end.
