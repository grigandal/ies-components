unit KB_Instructions;

 {******************************************************************************
 * ������ KB_Instructions.pas �������� ����������� ��������� �������:
 *
 * TParamDecl - �����, ������������ �������� ������ ������ ���� ������
 * TInstruction - ��������� ��� ���������� (������� �����)
 * TAssignInstruction - ���������� ������������
 * TReturnInstruction - ���������� �������� �������� � ������
 * TIfInstruction - ���������� ��������� ���������
 * TWhileInstruction - ���������� �����
 * TCallInstruction - ���������� ������ ������ ������
 * TVarDeclInstruction - ���������� ���������� ��������� ����������
 * TCreateInstruction - ���������� �������� ���������� ������
 * TSetGoalInstruction - ���������� ��������� ���� ������
 *
 * TClassRule - �����-��������� ��� ������� ������ ���� ������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, COMObj, SysUtils, Variants, // Dialogs,
  KB_Common, KB_Values, KB_Containers, OM_Containers, VCL.Graphics, Windows, StdVCL, VCL.Dialogs;

type

  ////////////////////////////////////////////////////////////////////////////
  // ����� TParamDecl �������� ����������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TParamDecl = class(TNamedItem) // ������������� ���� DTD 'param-decl'
  public
    // � ������������� ���� ID �������� ������������� ��������� ������ ������
    TypeRef: TKBType;           // KB, ������ �� ��� ���������
    Modifier: TModifierSet;     // KB, ������������ (�� �������)
    Default: TValue;            // KB, ���������������� ��������, ���� ����
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TInstruction �������� ����������� ����������� (������� �����)
  ////////////////////////////////////////////////////////////////////////////

  TInstruction = class(TXMLItem)
  public
    ShallStop: boolean;         // KB, ������� ����, ��� ������� �������� ��������� ���������� ���� ���������� ���������� (��� ����������� return)
    Disabled: boolean;          // KB, ������� ����, ��� ���������� ��������� �� ���� (��� ����������� ���������������)
    Comment: WideString;        // KB, �������������� ����������� � ����������
    // Interprete ���������� TValue, ����� �������� �������� return ������
    // CondAttrs - �������� �������� ��� ���� ���������� - ����� ������� ����������� ������� �������
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; virtual; abstract;
    function Achieves(Env: TInstance; Goal: TGoal): boolean; virtual;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TVarDeclInstruction ��������� ���������� ���������� ��������� ����������
  ////////////////////////////////////////////////////////////////////////////

  TVarDeclInstruction = class(TInstruction) // ������������� ���� DTD 'var-decl'
  public
    ID: WideString;             // KB, ��� ��������� ����������
    TypeRef: TKBType;           // KB, ������ �� ��� ����������� ����������
    Default: TValue;            // KB, ���������������� ��������, ���� ����
    constructor Create(AOwner: TComponent); override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TAssignInstruction ��������� ���������� ������������
  ////////////////////////////////////////////////////////////////////////////

  TAssignInstruction = class(TInstruction) // ������������� ���� DTD 'assign'
  public
    VarRef: TReference;         // KB, ������ �� ����������, ����������� ��� ����������� Env
    Expr: TAbstractValue;       // KB, ����������� ���������
    ByRef: boolean;             // KB, ������� ����, ��� ����������� ������, � �� ��������
    Attrs: TAttrList;           // KB, ������ ��������� ������������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
    function Achieves(Env: TInstance; Goal: TGoal): boolean; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TReturnInstruction ��������� ���������� �������� �������� � ������
  ////////////////////////////////////////////////////////////////////////////

  TReturnInstruction = class(TInstruction) // ������������� ���� DTD 'return'
  public
    Value: TAbstractValue;      // KB, ������������ ��������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TCreateInstruction ��������� ���������� �������� ���������� ������
  ////////////////////////////////////////////////////////////////////////////

  TCreateInstruction = class(TInstruction) // ������������� ���� DTD 'create'
  public
    VarRef: TReference;         // KB, ������ �� ����������, ������� ���� ��������� ��������� ������
    ClassMeta: TAncestorMeta;   // KB, ��� ������
    ClassGUID: WideString;      // KB, ������ � (ProgID | CLSID) COM-������� ��� � KBID ������ ��
    ClassesRef: TList;          // ��������� �� ������ ������� � KBContainer
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override; // ������� COM-������ � ���� ��� � TInstance
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TIfInstruction ��������� ���������� ��������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TIfInstruction = class(TInstruction) // ������������� ���� DTD 'if'
  public
    Condition: TAbstractValue;  // KB, ������� - ���������� ���������
    Instructions: TList;        // KB, ������ TInstruction, ����������� � ������ ���������� Condition
    ElseInstructions: TList;    // KB, ������ TInstruction, ����������� � ������ �������� Condition
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
    function Achieves(Env: TInstance; Goal: TGoal): boolean; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TClassRule �������� ����������� ��� ������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TClassRule = class(TNamedItem)// ������������� ���� DTD 'rule'
  public
    Meta: TRuleMeta;            // KB, ��� �������
    Desc: WideString;           // KB, �������� �������
    Rule: TIfInstruction;       // KB, ���������� �������
    WasInherited: boolean;      // ���� �� ��� ������� ����������� �� �������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure Interprete(Env: TInstance; CondAttrs: TAttrList; Then_: boolean);
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TWhileInstruction ��������� ���������� �����
  ////////////////////////////////////////////////////////////////////////////

  TWhileInstruction = class(TInstruction) // ������������� ���� DTD 'while'
  public
    Condition: TAbstractValue;  // KB, ������� ���������� �����
    Instructions: TList;        // KB, ����������, ����������� ������ �����
    CheckBefore: boolean;       // KB, ��������� �� ������� �� ����� ����� ��� �����
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; Attrs: TAttrList; Locals: TList): TValue; override;
    function Achieves(Env: TInstance; Goal: TGoal): boolean; override;
  private
    function Loop(Env: TInstance; CondAttrs: TAttrList; Locals: TList): TValue; // ������ ����� ����� � ���������� �������� �� return
    function ShallLoop(Env: TInstance; Locals: TList): boolean; // ��������� ������� ����� � ��������, ������ �� �����
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TCallInstruction ��������� ���������� ������ ������
  ////////////////////////////////////////////////////////////////////////////

  TCallInstruction = class(TInstruction) // ������������� ���� DTD 'call'
  public
    MethodRef: TReference;      // KB, ������ �� ����������� ����� ������
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
    function Achieves(Env: TInstance; Goal: TGoal): boolean; override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TSetGoalInstruction ��������� ���������� ��������� ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TSetGoalInstruction = class(TInstruction) // ������������� ���� DTD 'setgoal'
  public
    VarRef: TReference;         // KB, ������ �� �������� ������, ���������� �������
    Value: TAbstractValue;      // KB, ��������� �������� ����, ���� ����
    constructor Create(AOwner: TComponent); override;
    procedure XML(Parent: IXMLNode); override;
    function Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue; override;
  end;


implementation

uses KB_Types;


/////////////////////////////////////////
// ������ ������ TParamDecl
/////////////////////////////////////////

procedure TParamDecl.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sParamDecl);
  N.Attributes[sID] := ID;
  if TypeRef <> nil then
    N.Attributes[sType] := TypeRef.ID;

  if Modifier = [mdIn, mdOut] then
    N.Attributes[sModifier] := sBoth
  else if Modifier = [mdIn] then
    N.Attributes[sModifier] := sIn
  else if Modifier = [mdOut] then
    N.Attributes[sModifier] := sOut;

  if Default <> nil then Default.XML(N);
end;


/////////////////////////////////////////
// ������ ������ TInstruction
/////////////////////////////////////////

function TInstruction.Achieves(Env: TInstance; Goal: TGoal): boolean;
begin
  Result := false;
end;

/////////////////////////////////////////
// ������ ������ TAssignInstruction
/////////////////////////////////////////

constructor TAssignInstruction.Create(AOwner: TComponent);
begin
  inherited;
  Expr := nil;
  VarRef := nil;
  Attrs := TAttrList.Create(Self);
  ByRef := false;
end;

destructor TAssignInstruction.Destroy;
begin
  Expr.Free;
  Attrs.Free;
  inherited;
end;

procedure TAssignInstruction.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sAssign);
  if ByRef then N.Attributes[sByRef] := sTrue;
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;

  if VarRef <> nil then VarRef.XML(N);
  if Expr <> nil then Expr.XML(N);
  Attrs.XML(N);
end;


function TAssignInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
var
  v: TValue;
  s: widestring;
begin
  // ����� ���� ������ ������� ����������� ������� ������� (E [s;t], E->H [n;p]  |-  H [n';p'])

  // �������-���������� �� ������. ���� ������ ������� �������� � ������ �� ������� (������������� �� ������)
  // ������������� �� �������� ����������� ����������� Create
  Result := nil;

  ShallStop := false;
  v := Expr.Evaluate(Env, Locals, Nil) as TValue; // ��������� ���������; ����� �� ���, ������� ������ �������
  if v <> nil then begin
    v.ByRef := ByRef;
    if not ByRef then begin
      // ���������� ������� ����������� ������ �����
      // 1. ��������� ������� �� Attrs � v.Attrs, ���� ��� ����, ����� ��������� �������
      v.Attrs.Assign(Attrs);
      // 2. ����� ������� CondAttrs

      //s := 'evidence: ' + FloatToStr(CondAttrs.Belief) + ' ' + FloatToStr(CondAttrs.Probability) + #10#13;
      //s := s + 'hypothesis: ' + FloatToStr(v.Attrs.Belief) + ' ' + FloatToStr(v.Attrs.Probability) + #10#13;
      if CondAttrs <> nil then CalcEHFactors(CondAttrs, v.Attrs,  v.Attrs);
      //s := s + 'result: ' + FloatToStr(v.Attrs.Belief) + ' ' + FloatToStr(v.Attrs.Probability) + #10#13;
      //ShowMessage(s);
    end;

    {if (Expr is TOperation)  then begin
      // �������� �������� ������ ������������� ��� ��������, ���������� �� ByRef
      v := TOperation(Expr).OpResult;
    end
    else begin
      v := TValue.Create(nil);
      v.ResVal := Expr.ResVal;
      if ByRef then
        v.ByRef := true         // �� ������
      else
        v.ByRef := false;       // �� ��������
    end;}

    VarRef.AssignValue(Env, KB, Locals, v, false);
  end
  else
    ErrorHandler.HandleError(eEvalFailed, 'AssignInstruction: �� ���� ��������� ��������� ��� ������� "'+IntToHex(integer(Env),8)+'": '+Env.ClassID + '. Assignment comment: ' + self.Comment);
end;


function TAssignInstruction.Achieves(Env: TInstance; Goal: TGoal): boolean;
begin
  // ���������, ��������� �� Goal.GoalRef � Self.VarRef �� ���� � �� �� ��������
  Result := VarRef.Equals(Env, Goal.GoalRef, Env.OMRef.Objects[0]);
end;


/////////////////////////////////////////
// ������ ������ TCreateInstruction
/////////////////////////////////////////

constructor TCreateInstruction.Create(AOwner: TComponent);
begin
  inherited;
  VarRef := nil;
  ClassesRef := nil;
end;

destructor TCreateInstruction.Destroy;
begin
  ClassesRef := nil;
  inherited;
end;

procedure TCreateInstruction.XML(Parent: IXMLNode);
var N,M: IXMLNode;
begin
  N := Parent.AddChild(sCreate);
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;

  if VarRef <> nil then VarRef.XML(N);

  M := N.AddChild(sClassInfo);
  M.Text := ClassGUID;
  if ClassMeta = amKBID then M.Attributes[sMeta] := sKBID
  else if ClassMeta = amProgID then M.Attributes[sMeta] := sProgID
  else if ClassMeta = amCLSID then M.Attributes[sMeta] := sCLSID;
end;

function TCreateInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
var
  Inst: TInstance;
  KBClass: TKBClass;
  InstRef: TValue;
begin
  // Attrs �� ����������
  Result := nil;
  KBClass := nil;
  Inst := nil;
  ShallStop := false;
  if ClassMeta = amKBID then begin
    // ������ ����� �� ClassGUID
    KBClass := GlbFindItem(ClassesRef, ClassGUID) as TKBClass;
    if KBClass <> nil then
      Inst := KBClass.CreateInstance as TInstance
    else
      ErrorHandler.HandleError(eInvalidID, 'CreateInstruction: �� ������ ����� "' + ClassGUID + '"');
  end
  else begin
    Inst := TInstance.Create(nil);
    Inst.ClassID := ClassGUID;
    try
      if ClassMeta = amProgId then
        Inst.Obj := CreateOleObject(ClassGUID)
      else if ClassMeta = amCLSID then
        Inst.Obj := CreateCOMObject(StringToGUID(ClassGUID)) as IDispatch;
    except
      ErrorHandler.HandleError(eInvalidGUID, 'CreateInstruction: ������ ��� �������� COM-������� �� GUID "' + ClassGUID + '"');
    end;
  end;

  // ������� ����� ������ � ������ ��������
  Inst.OMRef := Env.OMRef;
  Env.OMRef.Objects.Add(Inst);
  KBClass.CreateObjectProperties(Inst, KB); // �������� ������ ����� ���������� ������ �� OM

  // ������ ���� �������� Inst ��������� ���������� VarRef (Inst ����� �������� nil)
  InstRef := TValue.Create(nil);
  InstRef.ByRef := true;
  InstRef.ResVal := integer(Inst); // ���� ��������� �� Inst � ResVal: variant
  VarRef.AssignValue(Env, KB, Locals, InstRef, false);
  Result := InstRef;

  // ����� ���������� ������� ���� initial
  if (ClassMeta = amKBID) and (KBClass <> nil) then
    KBClass.FireInitialRules(Inst);
end;


/////////////////////////////////////////
// ������ ������ TReturnInstruction
/////////////////////////////////////////

constructor TReturnInstruction.Create(AOwner: TComponent);
begin
  inherited;
  Value := nil;
end;

destructor TReturnInstruction.Destroy;
begin
  Value.Free;
  inherited;
end;

procedure TReturnInstruction.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sReturn);
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;
  if Value <> nil then Value.XML(N);
end;

function TReturnInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
begin
  Result := Value.Evaluate(Env, Locals, Nil) as TValue;
  ShallStop := true; // �� ������� ����� ������������ �� ������� ���������
end;


/////////////////////////////////////////
// ������ ������ TIfInstruction
/////////////////////////////////////////

constructor TIfInstruction.Create(AOwner: TComponent);
begin
  inherited;
  Condition := nil;
  Instructions := TList.Create;
  ElseInstructions := TList.Create;
end;

destructor TIfInstruction.Destroy;
begin
  Condition.Free;
  Instructions.Free;
  ElseInstructions.Free;
  inherited;
end;

procedure TIfInstruction.XML(Parent: IXMLNode);
var
  N: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sIf);
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;
  Condition.XML(N);

  N := Parent.AddChild(sAction);
  for i:=0 to Instructions.Count-1 do
    TXMLItem(Instructions[i]).XML(N);

  if ElseInstructions.Count > 0 then begin
    N := Parent.AddChild(sElseAction);
    for i:=0 to ElseInstructions.Count-1 do
      TXMLItem(ElseInstructions[i]).XML(N);
  end;
end;

function TIfInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
var
  InstList: TList;
  Inst: TInstruction;
  i: integer;
  v: TValue;
begin
  Result := nil;
  ShallStop := false;  // ������ ����� ���� return

  v := Condition.Evaluate(Env, Locals, Nil) as TValue;
  if v <> nil then begin
    if (v.ResVal = true) or (v.ResVal <> 0) then // ����� ��������
      InstList := Instructions
    else
      InstList := ElseInstructions;
    i:=0;
    while i<InstList.Count do begin
      Inst := InstList[i];
      Inc(i);
      if Inst.Disabled then continue; // ������� ��������� �� continue
      Result := Inst.Interprete(Env, Nil, CondAttrs, Locals);
      if Inst.ShallStop then begin
        ShallStop := true;
        break;
      end;
      Inc(i);
    end;
  end;
end;

function TIfInstruction.Achieves(Env: TInstance; Goal: TGoal): boolean;
var i: integer;
begin
  Result := false;
  i:=0;
  while i < Instructions.Count do begin
    Result := TInstruction(Instructions[i]).Achieves(Env, Goal);
    if Result then break;
    Inc(i);
  end;
  if not Result then begin
    i:=0;
    while i < ElseInstructions.Count do begin
      Result := TInstruction(ElseInstructions[i]).Achieves(Env, Goal);
      if Result then break;
      Inc(i);
    end;
  end;
end;


/////////////////////////////////////////
// ������ ������ TClassRule
/////////////////////////////////////////

constructor TClassRule.Create(AOwner: TComponent);
begin
  inherited;
  Rule := TIfInstruction.Create(AOwner);
end;

destructor TClassRule.Destroy;
begin
  Rule.Free;
  inherited;
end;

procedure TClassRule.XML(Parent: IXMLNode);
var
  N, M: IXMLNode;
  i: integer;
  s:string;
begin
  N := Parent.AddChild(sRule);
  N.Attributes[sID] := ID;
  N.Attributes[sDesc] := Desc;

  case Meta of
    rmSimple:   N.Attributes[sMeta] := sSimple;
    rmInitial:  N.Attributes[sMeta] := sInitial;
    rmFinal:    N.Attributes[sMeta] := sFinal;
  end;

  M := N.AddChild(sCondition);
  s := M.XML;
  Rule.Condition.XML(M);

  M := N.AddChild(sAction);
  for i:=0 to Rule.Instructions.Count-1 do
    TXMLItem(Rule.Instructions[i]).XML(M);

  if Rule.ElseInstructions.Count > 0 then begin
    M := N.AddChild(sElseAction);
    for i:=0 to Rule.ElseInstructions.Count-1 do
      TXMLItem(Rule.Instructions[i]).XML(M);
  end;
end;

procedure TClassRule.Interprete(Env: TInstance; CondAttrs: TAttrList; Then_: boolean);
var
  i: integer;
  Instructions, Variables: TList;
  Inst: TInstruction;
begin
  if Then_ then
    Instructions := Rule.Instructions
  else
    Instructions := Rule.ElseInstructions;

  Variables := TList.Create; // ������ TIDValue �� ���������� ��������� ����������
  i := 0;

  //if Then_ and (Instructions.Count > 0)  then
    //ShowMessage('����������� ������� ' + ID + ' ' + Desc + #10#13 + FloatToStr(CondAttrs.Belief) + ' ' + FloatToStr(CondAttrs.Probability));

  while i<Instructions.Count do begin
    Inst := Instructions[i];
    Inc(i);
    if Inst.Disabled then continue;
    // � Attrs ����� ������� ����������� ������� ������� [s; t]
    Inst.Interprete(Env, Nil, CondAttrs, Variables); // ��������� �� �����
    if Inst is TReturnInstruction then break;
  end;
  Variables.Clear;
  Variables.Free;
end;


/////////////////////////////////////////
// ������ ������ TWhileInstruction
/////////////////////////////////////////

constructor TWhileInstruction.Create(AOwner: TComponent);
begin
  inherited;
  CheckBefore := True;
  Condition := nil;
  Instructions := TList.Create;
end;

destructor TWhileInstruction.Destroy;
begin
  Condition.Free;
  Instructions.Free;
  inherited;
end;

procedure TWhileInstruction.XML(Parent: IXMLNode);
var
  N: IXMLNode;
  i: integer;
begin
  N := Parent.AddChild(sWhile);
  if CheckBefore then
    N.Attributes[sCheckBefore] := sTrue
  else
    N.Attributes[sCheckBefore] := sFalse;
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;

  Condition.XML(N);
  if Instructions.Count > 0 then begin
    N := N.AddChild(sStatements);
    for i:=0 to Instructions.Count-1 do
      TXMLItem(Instructions[i]).XML(N);
  end;
end;


function TWhileInstruction.Loop(Env: TInstance; CondAttrs: TAttrList; Locals: TList): TValue;
var
  Inst: TInstruction;
  i: integer;
begin
  // ������ ����� ����� � ���������� ShallStop
  Result := nil;
  ShallStop := false;
  i:=0;
  while i<Instructions.Count do begin
    Inst := Instructions[i];
    Inc(i);
    if Inst.Disabled then continue;
    Result := Inst.Interprete(Env, Nil, CondAttrs, Locals);
    if Inst.ShallStop then begin
      ShallStop := true;
      break;
    end;
  end;
end;

function TWhileInstruction.ShallLoop(Env: TInstance; Locals: TList): boolean;
var
  v: TValue;
begin
  // ��������� ������� ����� � ��������, ������ �� �����
  Result := false;
  v := Condition.Evaluate(Env, Locals, Nil) as TValue;
  if (v <> nil) and (v.ResVal = true) then // ����� ��������
    Result := true;
end;


function TWhileInstruction.Interprete(Env: TInstance; KB:TKBContainer; Attrs: TAttrList; Locals: TList): TValue;
begin
  Result := nil;
  ShallStop := false; // ������ while ����� ���� return

  // ���� ���� while do
  if CheckBefore then begin
    while ShallLoop(Env, Locals) do begin
      Result := Loop(Env, Attrs, Locals);
      if ShallStop then break;
    end
  end
  // ���� ���� do while
  else begin
    repeat
      Result := Loop(Env, Attrs, Locals);
      if ShallStop then break;
    until not ShallLoop(Env, Locals);
  end;
end;

function TWhileInstruction.Achieves(Env: TInstance; Goal: TGoal): boolean;
var i: integer;
begin
  Result := false;
  i:=0;
  while i < Instructions.Count do begin
    Result := TInstruction(Instructions[i]).Achieves(Env, Goal);
    if Result then break;
    Inc(i);
  end;
end;

/////////////////////////////////////////
// ������ ������ TCallInstruction
/////////////////////////////////////////

constructor TCallInstruction.Create(AOwner: TComponent);
begin
  inherited;
  MethodRef := nil;
end;

destructor TCallInstruction.Destroy;
begin
  inherited;
end;

procedure TCallInstruction.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sCall);
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;
  if MethodRef <> nil then MethodRef.XML(N);
end;

function TCallInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
begin
  Result := MethodRef.Evaluate(Env, Locals, Nil) as TValue;
end;

function TCallInstruction.Achieves(Env: TInstance; Goal: TGoal): boolean;
begin
  // ������� Achieves ��� ���������� ���������
  Result := false;
end;


/////////////////////////////////////////
// ������ ������ TVarDeclInstruction
/////////////////////////////////////////

constructor TVarDeclInstruction.Create(AOwner: TComponent);
begin
  inherited;
  Default := nil;
  TypeRef := nil;
end;

procedure TVarDeclInstruction.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sVarDecl);
  N.Attributes[sID] := ID;
  if TypeRef <> nil then
    N.Attributes[sType] := TypeRef.ID;
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;

  if Default <> nil then Default.XML(N);
end;

function TVarDeclInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
var VarDecl: TIDValue;
begin
  Result := nil;
  // ���� � Locals �������� ���������� (���� �� ��� ��� ���)
  VarDecl := GlbFindItem(Locals, ID) as TIDValue;
  if VarDecl <> nil then begin
    // ��� ����
    VarDecl.Value := Default;
  end
  else begin
    // ��� ���
    VarDecl := TIDValue.Create(nil);
    VarDecl.ID := ID;
    VarDecl.InstRef := nil; // ��� ������� � ��������� ����������
    // ��������� �������� �� Default, ���� ��� ����
    if Default <> nil then
      VarDecl.Value := Default.Evaluate(Env, Locals, Nil);
    // ���� �������� �� ���� ���������, �� �������� ������
    if VarDecl.Value = nil then
      VarDecl.Value := TValue.Create(nil);
    VarDecl.Value.TypeRef := TypeRef;
    Locals.Add(VarDecl);
  end;
end;


/////////////////////////////////////////
// ������ ������ TSetGoalInstruction
/////////////////////////////////////////

constructor TSetGoalInstruction.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TSetGoalInstruction.XML(Parent: IXMLNode);
var N: IXMLNode;
begin
  N := Parent.AddChild(sSetGoal);
  if Disabled then N.Attributes[sDisabled] := sTrue;
  if Comment <> '' then N.Attributes[sComment] := Comment;

  if VarRef <> nil then VarRef.XML(N);
end;

function TSetGoalInstruction.Interprete(Env: TInstance; KB:TKBContainer; CondAttrs: TAttrList; Locals: TList): TValue;
var
  G: TGoal;
begin
  Result := nil;
  G := TGoal.Create(nil);
  G.GoalRef := VarRef; // ����� ������������ ���� �� VarRef
  G.ValueRef := Value;
  G.Achieved := false;
  G.Primary := true; // ���� ���������, ��� ��� �������� � ����� ����
  G.Inferrable := false; // ��������� ������������� ������ ���������� ���� ����
  G.CycleID := -1;

  Env.OMRef.Goals.Add(G);
end;

end.
