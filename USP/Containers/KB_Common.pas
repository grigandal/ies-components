unit KB_Common;

 {******************************************************************************
 * ������ KB_Common.pas �������� ��������� ��� ���� ��������� � XML-�������� ��,
 * ����-������������, ���� ������, �������� ������, �����-���������� ������
 *
 * ���������� ��� ���� �������:
 *  �������� �������, ��������� � kb_schema.dtd, ���������� ��������� 'KB'
 *  �������� �������, ��������� � om_schema.dtd, ���������� ��������� 'OM'
 *
 * TErrorHandler - �����-���������� ������
 * ��������� ��� ����� � ���������� ����� Application.Init � Application.Run !!!
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 1 �������, 2002 >>
 ******************************************************************************}

interface

uses
  Classes, VCL.Dialogs, SysUtils, xmldoc, xmlintf;

const
  ////////////////////////////////////////////////////////////////////////////
  // ���� ������
  ////////////////////////////////////////////////////////////////////////////

  eSuccess      = 0;

  // ������, ����������� ��� �������� �� �� XML � ������� ������
  eMissAttr     = 100;  // ����������� ��������� �������
  eMissChild    = 101;  // ����������� ��������� �������� �������
  eWrongOrder   = 102;  // �������� ������� �������� ���������
  eWrongAttr    = 103;  // ��������� ������� �� ��������� � �����������
  eWrongTag     = 104;  // ��������� ��� �� ��������� � �����������
  eBadFormed    = 105;  // ����������� ����������� XML-��������
  eNotKB        = 106;  // �������� ��� �� �������� ����� ��

  // ������, ����������� � runtime
  eFuzzifyErr   = 190;  // ������ ������������
  eInvalidGUID  = 200;  // ������ ��� �������� COM-������� � ������ GUID
  eTypeInfoErr  = 201;  // ������ ��� ���������� ������ �� ���������� �����
  eInvokeFailed = 205;  // ����� COM-������ ������ ������
  eInvalidID    = 210;  // ��������� ������������� �� ������ � ������ ���������������
  eInvalidRef   = 211;  // ������ ��������� �� �������������� ��������
  eEvalFailed   = 220;  // �������� ��������� �� ���� ���������

  ////////////////////////////////////////////////////////////////////////////
  // ����� ����� � XML-�������� ���� ������
  ////////////////////////////////////////////////////////////////////////////

  sCreationDate = 'creation-date';
  sProblemInfo = 'problem-info';
  sKnowledgeBase = 'knowledge-base';

  sTypes = 'types';
  sClasses = 'classes';
  sInitialSituation = 'initial-situation';
  sOperatingMemory = 'operating-memory';

  sType = 'type';
  sValue = 'value';
  sFrom = 'from';
  sTo = 'to';
  sParameter = 'parameter';
  sMF = 'mf';
  sPoint = 'point';
  sUnit = 'measure-item';
  sAlias = 'alias';
  sValueArray = 'varray';

  sClass = 'class';
  sClassInfo = 'class-info';
  sAncestor = 'ancestor';
  sProperties = 'properties';
  sRules = 'rules';
  sMethods = 'methods';
  sProperty = 'property';
  sRule = 'rule';
  sMethod = 'method';

  sQuestion = 'question';
  sQuery = 'query';

  sCondition = 'condition';
  sAction = 'action';
  sElseAction = 'else-action';

  sRef = 'ref';
  sEQ = 'eq';
  sNE = 'ne';
  sGT = 'gt';
  sLT = 'lt';
  sGE = 'ge';
  sLE = 'le';
  sNOT = 'not';
  sOR = 'or';
  sXOR = 'xor';
  sAND = 'and';
  sADD = 'add';
  sSUB = 'sub';
  sDIV = 'div';
  sMUL = 'mul';
  sMOD = 'mod';
  sNEG = 'neg';
  sFunc = 'func';

  // ��������� �.�, 15.06.2023. ��������������, �� �� ������ ���������� ��� ������ ����� �������� �� ��
  sEvInt = 'EvIntRel';
  sAlAttr = 'AlAttr';
  sIntRel = 'IntRel';
  sEvRel = 'EvRel';
  // ����� ��������������

  sSignifierType = 'signifier_type';
  sSignifier = 'signifier';

  sVarDecl = 'var-decl';
  sStatements = 'statements';
  sWhile = 'while';
  sIf = 'if';
  sAssign = 'assign';
  sCall = 'call';
  sReturn = 'return';
  sCreate = 'create';
  sSetGoal = 'setgoal';
  sParams = 'params';
  sParamsDecl = 'params-decl';
  sParamDecl = 'param-decl';
  sWith = 'with';

  sObjects = 'objects';
  sObject = 'object';
  sPropertyValue = 'property-value';
  sArgs = 'args';

  sGoals = 'goals';
  sGoal = 'goal';
  sCRS = 'conflict-set';
  sRuleStates = 'rule-states';
  sRuleState = 'rule-state';

  // �������� ����� � XML-��������

  sMeta = 'meta';
  sID = 'id';
  sAddr = 'addr';
  sDesc = 'desc';
  sDefault = 'default';
  sResultType = 'result-type';
  sModifier = 'modifier';
  sCheckBefore = 'check-before';
  sSource = 'source';
  sBelief = 'belief';
  sProbability = 'probability';
  sAccuracy = 'accuracy';
  sX = 'x';
  sY = 'y';
  sMinValue = 'min-value';
  sMaxValue = 'max-value';
  sComment = 'comment';
  sDisabled = 'disabled';
  sByRef = 'byref';
  sActive = 'active';
  sAchieved = 'achieved';
  sPrimary = 'primary';
  sInferrable = 'inferrable';
  sRate = 'rate';
  sReceiver = 'receiver';
  sCycle = 'cycle';
  sRefCount = 'refcount';
  sRefString = 'refstring';
  sAsked = 'asked';
  sThen = 'then';

  // �������� ���������

  sNumber = 'number';
  sString = 'string';
  sFuzzy = 'fuzzy';
  sTrue = 'true';
  sFalse = 'false';
  sBoolean = 'boolean';
  sArray = 'array';
  sLocal = 'local';

  sProgID = 'progid';
  sClsID = 'clsid';
  sKBID = 'kbid';

  sInferred = 'inferred';
  sSupplied = 'supplied';
  sNone = 'none';

  sSimple = 'simple';
  sInitial = 'initial';
  sFinal = 'final';

  sIn = 'in';
  sOut = 'out';
  sBoth = 'both';

  sRuntime = 'runtime-value';

  // ���������������� �������
  sCtg = 'ctg';
  sTg = 'tg';
  sSin = 'sin';
  sCos = 'cos';
  sArcsin = 'arcsin';
  sArccos = 'arccos';
  sArctg = 'arctg';
  sArcctg = 'arcctg';
  sAbs = 'abs';
  sPI = 'pi';
  sCeil = 'ceil';
  sFloor = 'floor';
  sFrac = 'frac';
  sInt = 'int';
  sRound = 'round';
  sTrunc = 'trunc';
  sExp = 'exp';
  sLn = 'ln';
  sLog2 = 'log2';
  sLog10 = 'log10';
  sSqrt = 'sqrt';
  sSqr = 'sqr';
  sMin = 'min';
  sMax = 'max';
  sConcat = 'concat';
  sSend = 'send';

type

  ////////////////////////////////////////////////////////////////////////////
  // �������, ������������ ���������
  ////////////////////////////////////////////////////////////////////////////

  // ������� ��� ��������� �������� �������� ������� �����
  TRequestValueEvent = procedure(Sender: TObject; Ref: WideString) of object;

  // ������� ��� �������� ���������� �� ���
  TDemonEvent = procedure(Receiver, MsgIn: WideString; var Value: OleVariant) of object;

  // ����� ��������� [�� ������������]
  TErrorEvent = procedure(Sender: TObject; Description: WideString) of object;


  ////////////////////////////////////////////////////////////////////////////
  // ������������, ���������� ��-�������: �����������, �����������, ��������
  ////////////////////////////////////////////////////////////////////////////

  TConfidence = double;         // ����������� [0,1]
  TProbability = double;        // ����������� [0,1]
  TAccuracy = double;           // �������� [0,1]


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TTypeMeta ��������� ������� �������� � TValue
  ////////////////////////////////////////////////////////////////////////////

  TTypeMeta =
  (
    tmNumeric,          // �������� ���
    tmSymbolic,         // ����������
    tmFuzzy,            // ��������
    tmRef               // ������
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TSource ��������� ������ ��������� �������� �������� ������ ���� ������
  ////////////////////////////////////////////////////////////////////////////

  TPropertySource =
  (
    psQuestion,         // �������� �������� ���������� �������� ������� ������������
    psQuery,            // �������� �������� ���������� ����������� ������� � ��
    psInferred,         // �������� �������� ��������� ���������
    psSupplied,         // �������� �������� ������� �� ��������� ��������
    psNone              // ����������� �������� �������� �������� (��� ������� ���������� ��������)
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TRuleMeta ��������� ��� ������� � ������ TRule
  ////////////////////////////////////////////////////////////////////////////

  TRuleMeta =
  (
    rmSimple,   // ������� �������
    rmInitial,  // �������, ������������� ��� �������� ���������� ������
    rmFinal     // �������, ������������� ��� ����������� ���������� ������
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TAncestorMeta ��������� ��� ������ ������
  ////////////////////////////////////////////////////////////////////////////

  TAncestorMeta =
  (
    amNone,             // ����� �������� ������� (�� ����� ������)
    amKBID,             // ����� ����������� �� ������� ������ ��
    amProgID,           // ����� ����������� �� COM-�������, ��� �������� ����� ProgID
    amCLSID             // ����� ����������� �� COM-�������, ��� �������� ����� CLSID
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TOpMeta ��������� ��� ��������, ������������� � ������
  // ���������� ������ TOperation
  ////////////////////////////////////////////////////////////////////////////

  TOpMeta = (
    omMul, omSub, omDiv, omAdd, omNeg, omMod,   // ��������������
    omAnd, omXor, omOr, omNot,                  // ����������
    omGt, omLt, omEq, omNe, omGe, omLe,         // ���������
    omTp                                        // ��������� �.�. 2023, ��������������, ������������ ������������ ���������
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TModifier ���������� ������������ ��������� ������
  // ��������� TModifierSet ���������� ����� ������������� ��������� ������
  ////////////////////////////////////////////////////////////////////////////

  TModifier =
  (
    mdIn,               // ���������, ��� �������� �������� �������
    mdOut               // ���������, ��� �������� �������� ��������
  );

  TModifierSet = set of TModifier;


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TLibFuncMeta ��������� ��� ���������������� �������
  ////////////////////////////////////////////////////////////////////////////

  TLibFuncMeta = (
    lfCtg, lfTg, lfCos, lfSin, lfArcsin, lfArccos, lfArctg, lfArcctg,
    lfAbs, lfPI, lfCeil, lfFloor, lfFrac, lfInt, lfRound, lfTrunc,
    lfExp, lfLn, lfLog2, lfLog10, lfSqrt, lfSqr, lfMin, lfMax,
    lfOut, lfConcat, lfSend
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TStyle ��������� ��������� ������ ��������
  ////////////////////////////////////////////////////////////////////////////

  TStyle =
  (
    isForward,          // ������ ����� (����� [ + ����])
    isBackward,         // �������� ����� (���� + �����)
    isMixed,            // ��������� ����� (���� + �����)
    isGenCheck          // ��������� � �������� (���� - ���, ������ - 0) [subject to change]
  );


  ////////////////////////////////////////////////////////////////////////////
  // ���-������������ TStopEvent ��������� ������� ����������� ������
  // ���������� ������ �� ������� �� ��������� TStopEvent ����� � �������� ������
  ////////////////////////////////////////////////////////////////////////////

  TStopEvent =
  (
    seNoRules,          // �����, ���� ����������� ����� ������ ����
    seGoalsAchieved,    // �����, ���� ��� ���� ����������
    seAnyGoalAchieved,  // �����, ���� ���� �� ���� ���� ����������
    seStopFlag          // �����, ���� ���������� ���� ��������
  );

  TStopEventSet = set of TStopEvent;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TErrorHandler �������� ������� ���� �����������, ����������� � XML
  ////////////////////////////////////////////////////////////////////////////

  TErrorHandler = class(TComponent)
  private
    fLog: WideString;   // ��� ����� ���� ������ ��������� �� �������
    fSilent: boolean;   // ���������� �� ��������� �� ������� (false - ����������)

  public
    LogDoc: IXMLDocument;  // ���� ����� ������ ������
    LastError: integer; // ��� ��������� ������������ ������
    constructor Create(AOwner: TComponent); override;
    procedure HandleError(ErrorCode: integer; ErrorText: WideString);
    procedure InitLog;  // ������������� ����
    function GetErrorDesc(ErrorCode: integer): WideString; // ���������� �������� ������ � ����� ErrorCode
    procedure Log(mes: WideString);
    function TimeStamp: WideString;
  published
    property LogFile: WideString read fLog write fLog;
    property Silent: boolean read fSilent write fSilent;
  end;

  function OpIsArythm(OpMeta: TOpMeta): boolean;
  function OpIsLogic(OpMeta: TOpMeta): boolean;
  function OpIsTest(OpMeta: TOpMeta): boolean;

var
  ErrorHandler: TErrorHandler = nil;
  Stack: TList;

implementation


function OpIsArythm(OpMeta: TOpMeta): boolean;
begin
  case OpMeta of
  omMul, omSub, omDiv, omAdd, omNeg, omMod:
    Result := true
  else
    Result := false;
  end;
end;

function OpIsLogic(OpMeta: TOpMeta): boolean;
begin
  case OpMeta of
  omAnd, omXor, omOr, omNot:
    Result := true
  else
    Result := false;
  end;
end;

function OpIsTest(OpMeta: TOpMeta): boolean;
begin
  case OpMeta of
  omGt, omLt, omEq, omNe, omGe, omLe:
    Result := true
  else
    Result := false;
  end;
end;

/////////////////////////////////////////
// ������ ������ TErrorHandler
/////////////////////////////////////////

constructor TErrorHandler.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  LastError := eSuccess;
  Silent := false;
  LogFile := 'c:\us-log.xml';
  LogDoc := TXMLDocument.Create(NIL);
  InitLog;
end;

procedure TErrorHandler.InitLog;
begin
  // ������ ����� ���
  if FileExists(LogFile) then begin
    LogDoc.LoadFromFile(LogFile); // ���������� ������ ���
  end
  else
    LogDoc.LoadFromXML('<log/>');

  LogDoc.Encoding := 'utf-8';

  LastError := eSuccess;
  LogDoc.DocumentElement.AddChild('newsession').Attributes['created'] := DateTimeToStr(Now);
end;

procedure TErrorHandler.HandleError(ErrorCode: integer; ErrorText: WideString);
var n: IXMLNode;
begin
  LastError := ErrorCode;
  if Silent then begin
    n := LogDoc.DocumentElement.AddChild('error');
    n.Attributes['code'] := ErrorCode;
    n.Attributes['time'] := TimeToStr(Now);
    n.Attributes['desc'] := GetErrorDesc(ErrorCode);
    n.Text := ErrorText;
    //if ErrorCode <> eEvalFailed then // ������������� ������
    //  LogDoc.SaveToFile(LogFile);
  end
  else ShowMessage(ErrorText);
end;

procedure TErrorHandler.Log(mes: WideString);
var n: IXMLNode;
begin
  n := LogDoc.DocumentElement.AddChild('message');
  n.Attributes['time'] := TimeToStr(Now);
  n.Text := mes;
  //LogDoc.SaveToFile(LogFile);
end;

function TErrorHandler.GetErrorDesc(ErrorCode: integer): WideString;
var s: WideString;
begin
  case ErrorCode of
    eSuccess:           s := 'success';

    eMissAttr:          s := 'Missing attribute';
    eMissChild:         s := 'Missing child';
    eWrongOrder:        s := 'Wrong child order';
    eWrongAttr:         s := 'Wrong attribute value';
    eWrongTag:          s := 'Wrong tag name';
    eBadFormed:         s := 'Not well-formed XML document';
    eNotKB:             s := 'Root element does not identify a KB';

    eFuzzifyErr:        s := 'Fuzzification failed';
    eInvalidID:         s := 'Identifier not found in list';
    eInvalidRef:        s := 'Reference refers to unexisting entity';
    eEvalFailed:        s := 'Expression evaluation failed';

    eInvalidGUID:       s := 'Invalid guid';
    eTypeInfoErr:       s := 'TypeInfo can not be accessed';
    eInvokeFailed:      s := 'IDispatch.Invoke failed';
  else
    s := 'Unknown error';
  end;
  Result := s;
end;

function TErrorHandler.TimeStamp: WideString;
begin
  Result := DateTimeToStr(Now);
end;

initialization
  // �������� ���� � ����� ������ ������� ���������� ��� ������� � ������� ��� ����������
  Stack := TList.Create;
  Stack.Add(TList.Create);

finalization
  TList(Stack.Last).Free;
  Stack.Free;

end.












