unit IE_Solver;

{ ******************************************************************************
  * ������ IE_Solver.pas �������� ����������� ��������� �������:
  *
  * TSolver - ���������� ������ ������
  * TSolverConfig - �����-�������� TSolver, �������� ���������������� ����������
  *
  * �����: << ������� �. �. >>
  * ���� ��������: << 8 �������, 2002 >>
  ****************************************************************************** }

interface

uses
  Classes, XMLDoc, XMLIntf, Variants, SysUtils, VCL.Dialogs,
  KB_Common, XML_Routines, KB_Containers, OM_Containers,
  KB_Instructions, KB_Types, KB_Values, Winapi.windows;

type

  /// /////////////////////////////////////////////////////////////////////////
  // �����-�������� TSolverConfig �������� ���������������� ����������
  /// /////////////////////////////////////////////////////////////////////////

  TSolverConfig = class(TPersistent)
  private
    FStyle: TStyle; // ��������� ������
    FSubdialogs: boolean; // �������� �� ���. ������� ������������
    FStopEvent: TStopEventSet; // ������� ������
    FAuthentic: boolean; // ������ ������ ������
    FFiringThreshold: double; // ����� ������������ �������
  published
    property Style: TStyle read FStyle write FStyle;
    property Subdialogs: boolean read FSubdialogs write FSubdialogs;
    property StopEvent: TStopEventSet read FStopEvent write FStopEvent;
    property Authentic: boolean read FAuthentic write FAuthentic;
    property FiringThreshold: double read FFiringThreshold
      write FFiringThreshold;
  end;

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TSolver ������������ ����� ���������� ������ ������
  /// /////////////////////////////////////////////////////////////////////////

  TSolver = class(TComponent)
  private
    FConfiguration: TSolverConfig;
    FOnRequestValue: TRequestValueEvent;
    FOnError: TErrorEvent;
    FOnDemon: TDemonEvent;
    procedure Fire(Rec: TConflictRec);
    function ShallStop: boolean; // ��������� ������� ������
    function ParseRef(RefString: WideString): TReference;
    // ���������, ���������� ��� ������, �������� � ��������� ������
    procedure FRun;
    procedure BRun;
    procedure MRun;
    function GetKBRef(ID: string): TKBType;
    function FStepIt: boolean; // true, ���� �� ��������� ������� ������
    function BStepIt: boolean; // true, ���� �� ��������� ������� ������
    function MStepIt: boolean; // true, ���� �� ��������� ������� ������
    procedure FMatch;
    procedure BMatch;
  public
    KB: TKBContainer; // ���� ������ � �������������� ������� ������
    OM: TOMContainer; // ������� ������
    CycleID: integer; // ����� ����� ��������
    GoalNet: TGoalNet; // "�����" �����, ����
    KBFileName: string;
    Loader: TXMLLoader;
    StopFlag: boolean;
    // ���� ����������� ��������, �������� �����, ����������� ����� ��������� �����
    Trace: IXMLDocument;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    // ������ ��������
    procedure Run;
    // ���������� ������ ���� ������
    procedure StepIt;
    // �������� ��� �������
    // ����� ��� ������� ��������
    procedure RequestValue(Ref: WideString);
    // ����� ��� ������ ��������� ������� ������ ����� ���� ������
    procedure SaveOMChanges;
    // ������������� �������� ��������
    procedure SetFact(RefString: WideString; Value: variant; Attrs: TAttrList;
      Asked: boolean);
    // ���������� ����
    procedure SetGoal(RefString: WideString);
    // ���������������� ������� ������
    procedure Init;
    // ��������� ����������� ������� � ������
    procedure AddRuleToTrace(Rec: TConflictRec);
    procedure BuildGoalMap;
  published
    property Configuration: TSolverConfig read FConfiguration
      write FConfiguration;
    // ����� ��� ������� ��������
    property OnRequestValue: TRequestValueEvent read FOnRequestValue
      write FOnRequestValue;
    property OnError: TErrorEvent read FOnError write FOnError;
    // ����� ��� �������� ���������� �� ��� � �������� ��������
    property OnDemon: TDemonEvent read FOnDemon write FOnDemon;
  end;

implementation

uses KB_Fuzzy, VCL.Forms;

/// //////////////////////////////////////
// ������ ������ TSolver
/// //////////////////////////////////////

constructor TSolver.Create(AOwner: TComponent);
begin
  inherited;
  KB := TKBContainer.Create(AOwner);
  OM := TOMContainer.Create(AOwner);
  Loader := TXMLLoader.Create(AOwner);
  Configuration := TSolverConfig.Create;
  Configuration.Style := isForward;
  Configuration.Subdialogs := true;
  Configuration.StopEvent := [seStopFlag];
  // seStopFlag ������ ��, ����� ����� �� ����������
  Configuration.Authentic := false;
  Configuration.FiringThreshold := 0.5;
  Trace := TXMLDocument.Create(Self);
  Trace.LoadFromXML('<trace/>');
  Trace.Encoding := 'utf-8';
  Trace.Version := '1.0';
end;

destructor TSolver.Destroy;
begin
  Trace := nil;
  Configuration.Free;
  OM.Free;
  KB.Free;
  Loader.Free;
  inherited;
end;

// ��������� ������ �� ������� ���������� �� (����� ��� ���)(��������� �.�.)
function TSolver.GetKBRef(ID: string): TKBType;
begin
  Result := KB.GetKBRef(ID) as TKBType;
end;


// ������������� ������� ������

procedure TSolver.Init;
var
  Doc: IXMLDocument;
  KBClass: TKBClass;
  Inst: TInstance;
begin
  Trace.LoadFromXML('<trace/>'); // ������� ������ ������

  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<dump comment="TSolver.Init"/>');
  Doc.Encoding := 'utf-8'; // ��� ��������� �������� �����

  Configuration.StopEvent := [seStopFlag];
  if (Configuration.Style = isForward) or (Configuration.Style = isMixed) then
    Configuration.StopEvent := Configuration.StopEvent + [seNoRules];
  if Configuration.Style = isBackward then
    Configuration.StopEvent := Configuration.StopEvent + [seGoalsAchieved];

  CycleID := 1;

  // � ������� ������ ���� ������� ��������� ������ World � ��������� ��� ������� initially
  OM.Clear;
  KBClass := GlbFindItem(KB.Classes, 'World') as TKBClass;
  if KBClass <> nil then
  begin
    Inst := KBClass.CreateInstance as TInstance;
    Inst.OMRef := OM;
    OM.Objects.Add(Inst);
    KBClass.CreateObjectProperties(Inst, KB);
    // �������� ������ ����� ���������� ������ �� OM
    KBClass.FireInitialRules(Inst);
  end;

  KB.XML(Doc.DocumentElement); // ������� � Doc ��� ��
  Doc.SaveToFile('is-dump.xml');
  Doc := nil;
  ErrorHandler.LogDoc.SaveToFile(ErrorHandler.LogFile);
end;


// ������ ��������

procedure TSolver.Run;
var
  Doc: IXMLDocument;
begin
  Trace.LoadFromXML('<trace/>'); // ������� ������ ������

  Doc := TXMLDocument.Create(nil);
  Doc.LoadFromXML('<dump comment="TSolver.Init"/>');
  Doc.Encoding := 'utf-8'; // ��� ��������� �������� �����

  StopFlag := false;
  case Configuration.Style of
    isForward:
      FRun();
    isBackward:
      BRun();
    isMixed:
      MRun();
  end;
  ErrorHandler.LogDoc.SaveToFile(ErrorHandler.LogFile);

  OM.XML(Doc.DocumentElement);
  Doc.SaveToFile('OM-dump.xml');
end;

// ������ �����
procedure TSolver.FRun;
var
  tm1, tm2: TDateTime;
  s: WideString;
begin
  Configuration.StopEvent := Configuration.StopEvent + [seNoRules];
  // ���� �� ��� ��� ���� �� ����� ����������� ���� �� ���� ������� ������
  tm1 := Now;
  while FStepIt() do
  begin
    tm2 := Now;
    s := 'Step ' + IntToStr(CycleID) + '. Time ' +
      IntToStr(Round((tm2 - tm1) * 86400000)) + ' ms';
    ErrorHandler.Log(s);
    tm1 := tm2;
    Inc(CycleID);
  end;
end;

// �������� �����
procedure TSolver.BRun;
var
  tm1, tm2: TDateTime;
  s: WideString;
begin
  tm1 := Now;
  while BStepIt() do
  begin
    tm2 := Now;
    s := 'Step ' + IntToStr(CycleID) + '. Time ' +
      IntToStr(Round((tm2 - tm1) * 86400000)) + ' ms';
    ErrorHandler.Log(s);
    tm1 := tm2;
    Inc(CycleID);
  end;
end;

// ��������� �����
procedure TSolver.MRun;
begin
  // ��������� ������ �����, ���� ����������� ����� �� ����
  Configuration.StopEvent := Configuration.StopEvent + [seNoRules];
  FRun;
  // ����� ����������� ����� ����, �� ��� ������ ������
  // ��������� ��������, ���� �� ���������� ������ ������� ������
  Configuration.StopEvent := Configuration.StopEvent - [seNoRules] +
    [seGoalsAchieved];
  if not StopFlag then
    BRun;
end;

function TSolver.ShallStop: boolean;
begin
  Result := false;
  if not Result and (seNoRules in Configuration.StopEvent) then
  begin
    Result := not OM.ConflictRulesPresent
  end;

  if not Result and (seGoalsAchieved in Configuration.StopEvent) then
  begin
    Result := OM.AllGoalsAchieved;
  end;

  if not Result and (seAnyGoalAchieved in Configuration.StopEvent) then
  begin
    Result := OM.AnyGoalAchieved;
  end;

  if not Result and (seStopFlag in Configuration.StopEvent) then
  begin
    Result := StopFlag;
  end;
end;

// ��� ������� ������
function TSolver.FStepIt: boolean;
var
  i: integer;
  obj: TInstance;
begin
  // �������� ����������� ����� ������
  FMatch(); // ������� ����������� �����
  Result := not ShallStop(); // ��������� ����� ����� �������������
  if OM.ConflictRulesPresent then
  begin
    for i := 0 to OM.Objects.Count - 1 do
    begin
      obj := OM.Objects[i];
      if obj.ConflictRules.Count > 0 then
      begin
        // ��������� �������
        Fire(obj.ConflictRules[0]);
        // ������ �� ������������ ������
        obj.ConflictRules.Delete(0);
      end;
    end;
  end;
end;

// ��� ��������� ������
function TSolver.BStepIt: boolean;
var
  i: integer;
  obj: TInstance;
  G, BG: TGoal;
begin
  // �������� ����������� ����� ������

  BMatch; // ������� ����������� �����
  Result := not ShallStop(); // ��������� ����� ����� �������������
  if OM.ConflictRulesPresent then
  begin
    for i := 0 to OM.Objects.Count - 1 do
    begin
      obj := OM.Objects[i];
      if obj.ConflictRules.Count > 0 then
      begin
        // ��������� �������
        Fire(obj.ConflictRules[0]);
        // ������ �� ������������ ������
        obj.ConflictRules.Delete(0);
      end;
    end;
  end
  else
  begin
    // ����������� ����� ������, ������ ����������� ���� - ������� �� � ������������
    if Configuration.Subdialogs then
    begin
      BG := nil;
      for i := 0 to OM.Goals.Count - 1 do
      begin
        G := OM.Goals[i];
        G.SelectGoalForSubdialog(OM.Objects[0], BG);
      end;
      if BG <> nil then
      begin
        // ShowMessage(BG.RefString);
        RequestValue(BG.RefString);
        // �� �������� ����� ����� ������� Asked, ����� ������ �� ���������� ���� �����
        Result := true; // ����� ����� ����� �����
      end;
    end;
  end;

  // ��������� ����������� ����� � �������� ����� Achieved
  for i := 0 to OM.Goals.Count - 1 do
  begin
    G := OM.Goals[i];
    G.CheckAchieved(OM.Objects[0]);
  end;
end;

// ��� ��������� ������������ ������ ��� ��������� ������ � ���������
function TSolver.MStepIt: boolean;
begin
  // ����������� seNoRules � ��������� �������� - ������� ������� ������
  if seNoRules in Configuration.StopEvent then
  begin
    Result := FStepIt;
    if not Result then
    begin
      // ������ ����� ����������, ������ �� ��������
      Configuration.StopEvent := Configuration.StopEvent - [seNoRules];
    end;
  end
  else
  begin
    Result := BStepIt;
  end;
end;

// ���������� ������ ���� ������ (��� ���������� ������ � ���������)

procedure TSolver.StepIt;
begin
  case Configuration.Style of
    isForward:
      FStepIt;
    isBackward:
      BStepIt;
    isMixed:
      MStepIt;
  end;
end;

procedure wtc(s: string);
begin
  OutputDebugString(PWideChar(s));
end;

// ��������� ������������� ��� ������� ������
procedure TSolver.FMatch;
var
  p: TConflictRec;
  CR: TClassRule;
  Env: TInstance;
  i, j: integer;
  Locals: TList;
  CondValue: TValue;
  rs: TRuleState;
  node: IXMLDocument;
  child: IXMLNode;
begin
  Locals := TList.Create;
  // ��� ���� �������� (����������� TKBClass)
  for i := 0 to OM.Objects.Count - 1 do
  begin
  Application.ProcessMessages;
    Env := OM.Objects[i];
    for j := 0 to Env.RuleStates.Count - 1 do
    begin
      Application.ProcessMessages;
      rs := Env.RuleStates[j];
      CR := rs.Rule as TClassRule;
      wtc('Matching rule:');
      wtc('META IS SIMPLE: ' + BoolToSTR(CR.Meta = rmSimple));
      wtc(('META IS INITIAL: ' + BoolToSTR(CR.Meta = rmInitial)));
      wtc(('META IS FINAL: ' + BoolToSTR(CR.Meta = rmFinal)));
      wtc(('DESC: ' + CR.Desc));
      node := TXMLDocument.Create(nil);
      node.LoadFromXML('<rulexml><expression/></rulexml>');
      child := node.DocumentElement.ChildNodes.FindNode('expression');
      CR.Rule.Condition.XML(child);
      wtc(StringReplace('XML: ' + child.XML, '<', #13#10 + '<',
        [rfReplaceAll]));
      wtc(('COMMENT: ' + CR.Rule.Comment));
      if (rs.Active) and (CR.Meta <> rmInitial) and (CR.Meta <> rmFinal) then
      begin

        CondValue := CR.Rule.Condition.Evaluate(Env, Locals, KB) as TValue;
        // ���� ������� ����������, �� ��������� � ����������� �����
        if CondValue <> nil then
        begin
          if (CondValue.ResVal = 0) and (CR.Rule.ElseInstructions.Count = 0)
          then
            continue;
          if (CondValue.ResVal = 1) and (CR.Rule.Instructions.Count = 0)
          then
            continue;
          p := TConflictRec.Create(nil);
          p.Env := Env;
          p.Rule := CR;
          p.CondValue := CondValue;
          Env.ConflictRules.Add(p);
          // ����� ����������
          rs.Active := false;
          // if CR.ID = '12' then ShowMessage(FloatToStr(CondValue.Attrs.Belief));
        end;
      end;
    end;
    Application.ProcessMessages;
  end;
  Locals.Free;
end;

// ��������� ������������� ��� ��������� ������
procedure TSolver.BMatch;
var
  p: TConflictRec;
  CR: TClassRule;
  Env: TInstance;
  i, j, k: integer;
  Locals: TList;
  CondValue: TValue;
  rs: TRuleState;
  G: TGoal;
begin
  Locals := TList.Create;
  // ��� ���� �������� (����������� TKBClass)
  for i := 0 to OM.Objects.Count - 1 do
  begin
  Application.ProcessMessages;
    Env := OM.Objects[i];
    for j := 0 to Env.RuleStates.Count - 1 do
    begin
    Application.ProcessMessages;
      rs := Env.RuleStates[j];
      CR := rs.Rule as TClassRule;
      if (rs.Active) and (CR.Meta <> rmInitial) and (CR.Meta <> rmFinal) then
      begin
        CondValue := CR.Rule.Condition.Evaluate(Env, Locals, KB) as TValue;
        // ���� ������� ����������, �� ����� ��������� � ����������� �����
        if CondValue <> nil then
        begin
          if (CondValue.ResVal = 0) and (CR.Rule.ElseInstructions.Count = 0)
          then
            continue;
          if (CondValue.ResVal = 1) and (CR.Rule.Instructions.Count = 0)
          then
            continue;
          p := TConflictRec.Create(nil);
          p.Env := Env;
          p.Rule := CR;
          p.CondValue := CondValue;
          Env.ConflictRules.Add(p);
          // ����� ����������
          rs.Active := false;
        end
        else
        begin
          // ����� ���� ����������� � ��������� �������
          for k := 0 to OM.Goals.Count - 1 do
          begin
            G := OM.Goals[i];
            if G.CycleID = -1 then
              G.CycleID := CycleID - 1;
            if CR.Rule.Achieves(Env, G) then
            begin
              G.Inferrable := true;
              // ����� ���� ���������� ������, �� ��� ������������ ��������
              // �������� ������� �� ������� �������
              //
            end;
          end;
        end;
      end;
    end;
    Application.ProcessMessages;
  end;
  Locals.Free;
end;

procedure wtd(s: string); // UOTPUT WRITE TO DEBUG CONSOLE
begin
  OutputDebugString(PWideChar(s));
end;
// ��������� ���������� �������

procedure TSolver.Fire(Rec: TConflictRec);
var
  r: TClassRule;
begin
  r := Rec.Rule as TClassRule;
  wtd('Fire ' + r.Desc);
  Rec.Then_ := integer(boolean(Rec.CondValue.ResVal)) >
    Configuration.FiringThreshold;
  r.Interprete(Rec.Env, Rec.CondValue.Attrs, Rec.Then_);

  // � ������ ������� �����, ����� ���� ����������
  if Rec.Then_ or (r.Rule.ElseInstructions.Count > 0) then
    AddRuleToTrace(Rec);
end;

// ����� ��� ������� ��������
procedure TSolver.RequestValue(Ref: WideString);
begin
  if Assigned(OnRequestValue) then
    OnRequestValue(Self, Ref);
end;

// ����� ��� ���������� ��������� ������� ������ ����� ���� ������
procedure TSolver.SaveOMChanges;
begin
end;


// ������������� �������� ��������

procedure TSolver.SetFact(RefString: WideString; Value: variant;
  Attrs: TAttrList; Asked: boolean);
var
  Ref: TReference;
  Env: TInstance;
  V: TValue;
  tmp: TTypeMeta;
  dot: integer;
  s: string;
  TR: TKBType;
  i: integer;
  mf: TMembershipFunction;
begin
  // !!! ���� ����� ��� ����� ���� ������ �� ����

  // RefString - ������ �� �������� � �������� ������� ������������ ������� World

  // ���� �������� ���
  if VarIsNull(Value) then
    V := nil
  else
  begin
    // �������� �������� ��� Value

    // tmp := GetKBTypeMeta(RefString);

    // ������ � ����� ��
    s := RefString;
    while Pos('.', s) <> 0 do
    begin
      s := Copy(s, Pos('.', s) + 1, length(s))
    end;

    TR := GetKBRef(s);

    mf := nil;
    if (TR is TFuzzyType) then
    // ���� ��� - ��������, ��������� ������� ������� �������������� �� ������
    begin
      for i := 0 to TFuzzyType(TR).MFList.Count - 1 do
      begin
        if TMembershipFunction(TFuzzyType(TR).MFList.Items[i]).MFName = Value
        then
          mf := TMembershipFunction(TFuzzyType(TR).MFList.Items[i])
      end;
    end;

    V := TValue.Create(nil);

    if (mf <> nil) then
    // ���� ������� ������� �������������� - ��� � ���� ���� ��������
    begin
      V := mf;
    end;

    V.TypeRef := TR;

    if (true) then

      V.ResVal := Value;
    V.Attrs.Assign(Attrs); // ������ ��� ������ ���������

  end;

  // ��������� �������� �������� (������������ ������� world - �� ������ ������ � ������)
  Env := OM.Objects[0];
  Ref := ParseRef(RefString); // �������� RefString
  Ref.AssignValue(Env, KB, nil, V, Asked);
end;

// ������������� �������� ��������

procedure TSolver.SetGoal(RefString: WideString);

  procedure SetPrimaries(G: TGoal; V: boolean);
  var
    i: integer;
  begin
    G.Primary := V;
    for i := 0 to G.SubGoals.Count - 1 do
    begin
      SetPrimaries(TGoal(G.SubGoals[i]), false);
    end;
  end;

  procedure SetInferribles(G: TGoal);
  var
    i: integer;
  begin
    G.Inferrable := G.SubGoals.Count > 0;
    for i := 0 to G.SubGoals.Count - 1 do
    begin
      SetInferribles(TGoal(G.SubGoals[i]));
    end;
  end;

var
  Env: TInstance;
  G: TGoal;
begin
  // !!! ���� ����� ��� ����� ���� ������ �� �����, ������ ���� ��� ���� ����� ����

  // RefString - ������ �� �������� � �������� ������� ������������ ������� World

  // �������� �������� ��� Goal
  // G := TGoal.Create(nil);
  // GoalNet.CheckAchieved;

  G := GoalNet.GetGoalDotByRefString(RefString).Goal;

  // G.RefString := RefString;
  // G.GoalRef := ParseRef(RefString); // �������� RefString

  G.Achieved := false; // ���������!!!
  SetPrimaries(G, true); // ���� ���������, ��� ��� �������� � ����� ����
  G.CycleID := CycleID;

  // ��������� ���� (������������ ������� world - �� ������ ������ � ������)
  { Env := OM.Objects[0];
    G.FindSubGoals(Env); }
  SetInferribles(G);

  OM.Goals.Add(G);
end;


// ��������� ���������, �������� TReference �� ������ � �������� ������

function TSolver.ParseRef(RefString: WideString): TReference;
var
  dot: integer;
begin
  // ������� � RefString ����� ���� ��������� � ���������-��������
  dot := Pos('.', RefString);
  Result := nil;
  if RefString <> '' then
  begin
    Result := TReference.Create(nil);
    if dot = 0 then
    begin
      Result.ID := RefString;
      // ������ � ����� ��
      Result.TypeRef := GetKBRef(Result.ID);
    end
    else
    begin
      Result.ID := Copy(RefString, 1, dot - 1);
      // ������ � ����� ��
      Result.TypeRef := GetKBRef(Result.ID);
      Result.Ref := ParseRef(Copy(RefString, dot + 1, length(RefString)));
    end;
  end;
end;

procedure TSolver.AddRuleToTrace(Rec: TConflictRec);
var
  n: IXMLNode;
begin
  // ��������� ������� � ������ ������
  n := Trace.DocumentElement.AddChild('step');
  n.Text := IntToStr(CycleID);
  Rec.XML(Trace.DocumentElement);
end;

procedure TSolver.BuildGoalMap;
var
  m: TGoalNet;
  s: TStringList;
begin
  m := TGoalNet.Create(Nil);
  m.BuildFromKB(KB, OM);
  s := TStringList.Create;
  s.Add(m.GetMiniXML);
  s.SaveToFile('GOALS.xml', TEncoding.UTF8);
  GoalNet := m;
end;

end.
