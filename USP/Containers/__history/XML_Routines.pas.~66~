unit XML_Routines;

{ ******************************************************************************
  * ������ XML_Routines.pas �������� ����������� ��������� �������:
  *
  * TXMLLoader - �����-��������� �� XML, ������� ��� ���� �������
  *
  * �����: << ������� �. �. >>
  * ���� ��������: << 14 �������, 2002 >>
  ****************************************************************************** }

interface

uses
  Classes, SysUtils, XMLDoc, XMLIntf, Variants, VCL.Dialogs, VCL.Forms,
  KB_Common, KB_Containers, KB_Values, KB_Types, KB_Instructions;

type

  /// /////////////////////////////////////////////////////////////////////////
  // ����� TXMLLoader ����� ��� ���� � ���� ��������� �� XML
  /// /////////////////////////////////////////////////////////////////////////

  TXMLLoader = class(TComponent)
  private
    KBC: TKBContainer;
    Signifier: TKBClass;

    procedure Load(KBContainer: TKBContainer; Doc: IXMLDocument);

    procedure LoadTypes(N: IXMLNode);
    procedure LoadClasses(N: IXMLNode);
    procedure LoadSignifier();
    procedure AddPropToSignifier(propId: string);
    function GetTypeRefByID(TypeID: WideString): TKBType;

    // ��������� ��������� ������ ������������, ������� ����������� ���������
    procedure ResolveReferences; // [subject to change]
  public
    PtrOnDemon: TDemonEvent;
    // ���� ������������� ���������� ������� �������� ��������� (��� TSendMsgInstruction)

    constructor Create(AOwner: TComponent); override;

    // ��� ������ �������� Load
    procedure LoadXML(KBContainer: TKBContainer; XML: WideString);
    procedure LoadFile(KBContainer: TKBContainer; FileName: WideString);

    // ������� ��������� �������� � ��� ������� ����� ��������� ������� ���� Initially
    procedure LoadOM(OM: TOMContainer; N: IXMLNode; FireRules: boolean);

    procedure LoadProperties(L, TypeList: TList; N: IXMLNode);
    procedure LoadRules(L: TList; N: IXMLNode);
    procedure LoadMethods(L: TList; N: IXMLNode);

    procedure LoadValue(V: PValue; N: IXMLNode); // �����������
    procedure LoadInstrList(L: TList; N: IXMLNode);
    procedure LoadWith(Attrs: TAttrList; N: IXMLNode);
  end;

implementation

uses OM_Containers, KB_Fuzzy;

procedure wtd(s: string);
begin
  // OutputDebugString(PWideChar(s));
  showMessage(s);
end;

constructor TXMLLoader.Create(AOwner: TComponent);
begin
  inherited;
  if ErrorHandler = nil then
    ErrorHandler := TErrorHandler.Create(AOwner);
  KBC := nil;
  Signifier := nil;
end;

// �������� �� XML
procedure TXMLLoader.LoadXML(KBContainer: TKBContainer; XML: WideString);
var
  Doc: IXMLDocument;
begin
  Doc := TXMLDocument.Create(nil);
  try
    Doc.LoadFromXML(XML);
  except
    ErrorHandler.HandleError(eWrongTag, 'KBLoader: ������������ xml ��������');
    exit;
  end;

  Load(KBContainer, Doc);
end;

// �������� �� ����� XML
procedure TXMLLoader.LoadFile(KBContainer: TKBContainer; FileName: WideString);
var
  Doc: IXMLDocument;
begin
  ErrorHandler.InitLog;

  Doc := TXMLDocument.Create(nil);
  try
    Doc.LoadFromFile(FileName);
  except
    ErrorHandler.HandleError(eBadFormed, 'KBLoader: ������������ xml ��������');
    exit;
  end;

  Load(KBContainer, Doc);
end;

procedure TXMLLoader.Load(KBContainer: TKBContainer; Doc: IXMLDocument);
var
  N: IXMLNode;
begin
  if AnsiCompareText(Doc.DocumentElement.NodeName, sKnowledgeBase) <> 0 then
  begin
    ErrorHandler.HandleError(eNotKB, 'KBLoader: �������� �������� ������� ��');
    exit;
  end;

  KBC := KBContainer;
  KBC.Clear;

  // ���� ���� �� ����������, �� �� ��������� ��-�� [doAutoCreateNode]

  KBC.CreationDate := '';
  if not VarIsNull(Doc.DocumentElement.Attributes[sCreationDate]) then
    KBC.CreationDate := Doc.DocumentElement.Attributes[sCreationDate];

  KBC.ProblemName := Doc.DocumentElement.ChildNodes[sProblemInfo].Text;

  N := Doc.DocumentElement.ChildNodes[sTypes];
  LoadTypes(N);

  N := Doc.DocumentElement.ChildNodes[sClasses];
  LoadClasses(N);
  //
  ResolveReferences;
end;

procedure TXMLLoader.LoadTypes(N: IXMLNode);
var
  i, j, k: integer;
  t: TKBType;
  s: string;
  Meta, attrX, attrY: WideString;
  fLOG: IXMLDocument;
  M, PS, PM: IXMLNode;
  Val: Variant;
  MF: TMembershipFunction;
  L: TList;
  TMP: TFuzzyType;
begin
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    Application.ProcessMessages;
    M := N.ChildNodes[i];
    t := nil;

    if not VarIsNull(M.Attributes[sMeta]) then
      Meta := M.Attributes[sMeta]
    else
      ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' +
        sMeta + ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);

    // �������� �������
    if AnsiCompareText(Meta, sNumber) = 0 then
    begin
      t := TNumericType.Create(nil);
      // ���� ���� �����������
      if M.ChildNodes.Count = 2 then
        with t as TNumericType do
        begin
          Val := M.ChildNodes[sFrom].Text;
          try
            MinValue := Val;
          except
            MinValue := 0;
          end;
          Val := M.ChildNodes[sTo].Text;
          try
            MaxValue := Val;
          except
            MaxValue := 0;
          end;
        end;
    end

    // ��������� �������
    else if AnsiCompareText(Meta, sString) = 0 then
    begin
      t := TSymbolicType.Create(nil);
      // ���� ���� ��������
      if M.ChildNodes.Count > 0 then
        with t as TSymbolicType do
        begin
          for j := 0 to M.ChildNodes.Count - 1 do
          begin
            Values.Add(M.ChildNodes[j].Text);
          end;
        end;
    end

    // �������� �������, �������������� ��������� �.�. 2019
    // ������ XML:
    {
      <type id="���3" meta="fuzzy" desc="��������� ������ � ������ �������">
      <parametr min-value="0" max-value="10">
      <value>����-���-����</value>
      <mf>
      <point X="0.0001" Y="0.9999" />
      <point X="1" Y="0.5" />
      <point X="10" Y="0.0001" />
      </mf>
      </parametr>
      <parametr min-value="0" max-value="10">
      <value>����-���-����</value>
      <mf>
      <point X="0.0001" Y="0.0001" />
      <point X="1" Y="0.5" />
      <point X="10" Y="0.9999" />
      </mf>
      </parametr>
      </type>
      - ��� M.
      M.ChildNodes - �������� <parametr min-value="...>...</parametr>
    }
    else if AnsiCompareText(Meta, sFuzzy) = 0 then
    begin
      TMP := TFuzzyType.Create(nil);
      L := TList.Create; // ���� ����� ������ ������� �������������� ��
      with TMP as TFuzzyType do
      begin
        for j := 0 to M.ChildNodes.Count - 1 do
        // ����������� �� ��������� <parametr>, �� ������� ������� �� � ��������� ������ �� ��
        begin
          MF := TMembershipFunction.Create(Nil);
          // ������� ��, �� ���������� - �������� <mf><point X="../>..<point X="../></mf>
          PM := M.ChildNodes[j];

          MF.MFName := (PM.ChildNodes['value'].Text);
          PS := PM.ChildNodes['mf'];
          for k := 0 to PS.ChildNodes.Count - 1 do
          // ��������� ���������� ����� � �������� �� �� <point>
          begin
            attrX := stringReplace((PS.ChildNodes[k].Attributes['X']),
              '.', ',', []);
            attrY := stringReplace((PS.ChildNodes[k].Attributes['Y']),
              '.', ',', []);
            MF.AddXY(strtofloat(attrX), strtofloat(attrY));
          end;
          L.Add(MF);
        end;
        TMP.MFList := L;
        t := TMP;
        fLOG := TXMLDocument.Create(nil);
        fLOG.LoadFromXML('<t/>');
        t.XML(fLOG.DocumentElement);

        s := ('GOT FUZZY TYPE: ' + #13#10 + stringReplace('XML: ' +
          fLOG.DocumentElement.XML, '<', #13#10 + '<', [rfReplaceAll]));

      end;
    end
    // ��������� �������
    else if AnsiCompareText(Meta, sRef) = 0 then
    begin
      // ���������� method pointer type
      t := TReferenceType.Create(nil); // �� ���???
    end;

    if t <> nil then
    begin // ������ �� �����������
      if not VarIsNull(M.Attributes[sID]) then
        t.ID := M.Attributes[sID]
      else
        ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' +
          sID + ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);

      if not VarIsNull(M.Attributes[sDesc]) then
        t.Desc := M.Attributes[sDesc]
      else
        t.Desc := '';

      KBC.Types.Add(t);
    end
    else
    begin
      ErrorHandler.HandleError(eWrongAttr, 'KBLoader: ���������� ��� ' + Meta +
        ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);
    end;
  end;
end;

function TXMLLoader.GetTypeRefByID(TypeID: WideString): TKBType;
begin
  Result := GlbFindItem(KBC.Types, TypeID) as TKBType;
  if Result = nil then
    Result := GlbFindItem(KBC.Classes, TypeID) as TKBType;
end;

procedure TXMLLoader.LoadClasses(N: IXMLNode);
var
  i: integer;
  c: TKBClass;
  ct, pt: IXMLNode;
  p: tclassproperty;
begin
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    Application.ProcessMessages;
    ct := N.ChildNodes[i]; // class tag

    c := TKBClass.Create(nil);
    c.ClassesRef := KBC.Classes;

    if not VarIsNull(ct.Attributes[sID]) then
      c.ID := ct.Attributes[sID]
    else
      ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' + sID
        + ' � ���� ' + ct.ParentNode.NodeName + '.' + ct.NodeName);

    if not VarIsNull(ct.Attributes[sDesc]) then
      c.Desc := ct.Attributes[sDesc];

    pt := ct.ChildNodes.FindNode(sAncestor);
    if pt <> nil then
    begin
      pt := pt.ChildNodes[sClassInfo];
      c.AncestorID := pt.Text;
      if AnsiCompareText(pt.Attributes[sMeta], sKBID) = 0 then
        c.AncestorMeta := amKBID
      else if AnsiCompareText(pt.Attributes[sMeta], sProgID) = 0 then
        c.AncestorMeta := amProgID
      else if AnsiCompareText(pt.Attributes[sMeta], sCLSID) = 0 then
        c.AncestorMeta := amCLSID;
    end
    else
      c.AncestorMeta := amNone;

    LoadProperties(c.Properties, KBC.Types, ct.ChildNodes[sProperties]);

    pt := ct.ChildNodes[sRules];
    LoadRules(c.Rules, pt);

    pt := ct.ChildNodes[sMethods];
    LoadMethods(c.Methods, pt);

    KBC.Classes.Add(c);
  end;
  if Signifier <> Nil then
  begin
    KBC.Classes.Add(Signifier);
    for i := 0 to KBC.Classes.Count - 1 do
    begin
      c := KBC.Classes[i];
      if c.ID = 'world' then
      begin
        p := tclassproperty.Create(nil);
        p.ID := 'signifier';
        p.TypeRef := Signifier;
        p.Source := psNone;
      end;
    end;
  end;
end;

procedure TXMLLoader.LoadProperties(L, TypeList: TList; N: IXMLNode);
var
  cp: tclassproperty;
  i: integer;
  p: IXMLNode;
  Source, TypeID: WideString;
begin
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    p := N.ChildNodes[i];
    cp := tclassproperty.Create(nil);

    if not VarIsNull(p.Attributes[sID]) then
      cp.ID := p.Attributes[sID]
    else
      ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' + sID
        + ' � ���� ' + p.ParentNode.NodeName + '.' + p.NodeName);

    if not VarIsNull(p.Attributes[sDesc]) then
      cp.Desc := p.Attributes[sDesc];

    // ��������� ��������� �������� �������� ��������
    cp.Source := psNone;
    if not VarIsNull(p.Attributes[sSource]) then
    begin
      Source := p.Attributes[sSource];
      if AnsiCompareText(Source, sQuestion) = 0 then
      begin
        cp.Source := psQuestion;
        cp.Question := p.ChildNodes[sQuestion].Text;
      end
      else if AnsiCompareText(Source, sQuery) = 0 then
      begin
        cp.Source := psQuery;
        cp.Query := p.ChildNodes[sQuery].Text;
      end
      else if AnsiCompareText(Source, sInferred) = 0 then
        cp.Source := psInferred
      else if AnsiCompareText(Source, sSupplied) = 0 then
        cp.Source := psSupplied;
    end;

    // ������ ������ ��� ��������
    cp.TypeRef := nil;
    if not VarIsNull(p.Attributes[sType]) then
    begin
      TypeID := p.Attributes[sType];
      cp.TypeRef := GetTypeRefByID(TypeID); // ����� � nil ��������
    end;

    // �������� �������� �� ���������, ���� ��� ����
    cp.Default := nil;
    if p.ChildNodes.FindNode(sValue) <> nil then
    begin
      LoadValue(@cp.Default, p.ChildNodes[sValue]);
      // ���� ���� ������� �� ���������, �� � �� ���� ��������
      if p.ChildNodes.FindNode(sWith) <> nil then
        LoadWith(cp.Default.Attrs, p.ChildNodes[sWith]);
    end;

    // ��� �������-��������, �������� ������ create
    if p.Attributes[sCreate] = true then
      cp.ShallCreate := true
    else
      cp.ShallCreate := False;

    L.Add(cp);
  end;
end;

procedure TXMLLoader.LoadSignifier();
var
  has: boolean;
  i: integer;
  t: TKBType;
  s: string;
begin
  has := False;
  for i := 0 to KBC.Types.Count - 1 do
  begin
    t := KBC.Types[i];
    s := 'signifier_type';
    if t.ID = s then
    begin
      // ���� �� ��� ���������� � ��������� ����, ��� ��� ���������� ��� � ��������� �������� TRUE � FALSE
      has := true;
      break
    end;
  end;

  if not has then
  begin
    t := TSymbolicType.Create(nil);
    t.ID := 'signifier_type';
    with t as TSymbolicType do
    begin
      Values.Add('TRUE');
      Values.Add('FALSE');
    end;
    KBC.Types.Add(t)
  end;

  if Signifier = nil then
  begin
    Signifier := TKBClass.Create(nil);
    Signifier.ID := 'signifier';
  end;

end;

procedure TXMLLoader.AddPropToSignifier(propId: string);
var
  i: integer;
  t: TKBType;
  p: tclassproperty;
  s: string;
begin
  for i := 0 to KBC.Types.Count - 1 do
  begin
    t := KBC.Types[i];
    s := 'signifier_type';
    if t.ID = s then
    begin
      break
    end;
  end;

  p := tclassproperty.Create(nil);
  p.ID := propId;
  p.TypeRef := t;
  p.Source := psInferred;

  Signifier.Properties.Add(p);
end;

procedure TXMLLoader.LoadRules(L: TList; N: IXMLNode);
var
  cr: TClassRule;
  i: integer;
  p, r: IXMLNode;
begin
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    p := N.ChildNodes[i];
    cr := TClassRule.Create(nil);
    cr.ID := p.Attributes[sID];

    if AnsiCompareText(p.Attributes[sMeta], sSimple) = 0 then
      cr.Meta := rmSimple
    else if AnsiCompareText(p.Attributes[sMeta], sInitial) = 0 then
      cr.Meta := rmInitial
    else if AnsiCompareText(p.Attributes[sMeta], sFinal) = 0 then
      cr.Meta := rmFinal;

    if not VarIsNull(p.Attributes[sDesc]) then
      cr.Desc := p.Attributes[sDesc];

    if p.ChildNodes.Count < 2 then
      continue;

    // �������� �������
    r := p.ChildNodes[sCondition];
    LoadValue(@cr.Rule.Condition, r.ChildNodes[0]);

    // �������� ���������
    r := p.ChildNodes[sAction];
    LoadInstrList(cr.Rule.Instructions, r); // ��������

    // ���� ���� �����
    if p.ChildNodes.FindNode(sElseAction) <> Nil then
    begin
      r := p.ChildNodes[sElseAction];
      LoadInstrList(cr.Rule.ElseInstructions, r); // ��������
    end;

    L.Add(cr);
  end;
end;

procedure TXMLLoader.LoadMethods(L: TList; N: IXMLNode);
var
  cm: TClassMethod;
  i, j: integer;
  M: IXMLNode;
  params, statements, pn: IXMLNode;
  ParamDecl: TParamDecl;
begin
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    M := N.ChildNodes[i];
    cm := TClassMethod.Create(nil);

    if not VarIsNull(M.Attributes[sID]) then
      cm.ID := M.Attributes[sID]
    else
      ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' + sID
        + ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);

    if not VarIsNull(M.Attributes[sDesc]) then
      cm.Desc := M.Attributes[sDesc];

    // ������ ������ ��� ������������� �������� � ����� � �������
    cm.ResultTypeRef := nil;
    if not VarIsNull(M.Attributes[sResultType]) then
      cm.ResultTypeRef := GetTypeRefByID(M.Attributes[sResultType]) as TKBType;

    // �������� ���������
    params := M.ChildNodes[sParamsDecl];
    for j := 0 to params.ChildNodes.Count - 1 do
    begin
      pn := params.ChildNodes[j];
      ParamDecl := TParamDecl.Create(nil);

      if not VarIsNull(pn.Attributes[sID]) then
        ParamDecl.ID := pn.Attributes[sID]
      else
        ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' +
          sID + ' � ���� ' + params.NodeName + '.' + pn.NodeName);

      // ������� ����������� ���������
      ParamDecl.Modifier := [];
      if not VarIsNull(pn.Attributes[sModifier]) then
      begin
        if AnsiCompareText(pn.Attributes[sModifier], sIn) = 0 then
          ParamDecl.Modifier := [mdIn];
        if AnsiCompareText(pn.Attributes[sModifier], sOut) = 0 then
          ParamDecl.Modifier := [mdOut];
        if AnsiCompareText(pn.Attributes[sModifier], sBoth) = 0 then
          ParamDecl.Modifier := [mdIn, mdOut];
      end;

      // ������� ��� ������������� ��������
      ParamDecl.TypeRef := nil;
      if not VarIsNull(pn.Attributes[sType]) then
        ParamDecl.TypeRef := GetTypeRefByID(pn.Attributes[sType]);

      // ��������� ��������
      ParamDecl.Default := nil;
      if pn.ChildNodes.FindNode(sValue) <> nil then
        LoadValue(@ParamDecl.Default, pn.ChildNodes[sValue]);

      // �������
      cm.ParamsDecl.Add(ParamDecl);
    end;

    // �������� ����������
    statements := M.ChildNodes[sStatements];
    LoadInstrList(cm.Instructions, statements);

    L.Add(cm);
  end;
end;

function buildRef(ref: string; addSignifier: boolean): string;
var
  ID, rest: string;
  p: integer;
begin
  p := pos('.', ref);
  if p = 0 then
    Result := '<ref id="' + ref + '"></ref>'
  else
  begin
    ID := Copy(ref, 1, p - 1);
    rest := Copy(ref, p + 1, length(ref));
    Result := '<ref id="' + ID + '">' + buildRef(rest, False) + '</ref>';
  end;
  if addSignifier then
    Result := '<ref id="signifier">' + Result + '</ref>';
end;

function GetNodePath(Node: IXMLNode): string;
var
  N, c: IXMLNode;
  L: TList;
  i, p: integer;
  s: string;
begin
  N := Node;
  if N.Attributes['initialPath'] <> NULL then
    Result := N.Attributes['initialPath']
  else
  begin
    Result := N.NodeName;
    if N.ParentNode <> NIL then
    begin
      L := TList.Create;
      p := -1;
      for i := 0 to N.ParentNode.ChildNodes.Count - 1 do
      begin
        c := N.ParentNode.ChildNodes[i];
        if c.NodeName = N.NodeName then
        begin
          if c = N then
            p := L.Count;
          L.Add(c);
        end;
      end;
      if L.Count > 1 then
        Result := Result + '[' + inttostr(p) + ']';
      s := GetNodePath(N.ParentNode);
      if s <> '#document' then
        Result := s + '/' + Result;
    end;
  end;
end;

procedure TXMLLoader.LoadValue(V: PValue; N: IXMLNode);
// ���� �������� Fuzzy � ��������� TypeRef!!!
var
  Op: TOperation;
  Val: TValue;
  NamedParam: TIDValue;
  i, j: integer;
  M, s: IXMLNode;
  D: IXMLDocument;
  p, refXml: string;
begin
  // � V^ - ��������, � N - xml � ��������
  V^ := nil;

  // ������� ��������
  if AnsiCompareText(N.NodeName, sValue) = 0 then
  begin
    V^ := TValue.Create(nil);
    if VarIsNull(N.Attributes[sMeta]) then
      TValue(V^).ResVal := N.Text // �� ��������� sString
    else
    begin // ������ ��� ��������, ����������� ��� (TODO !!!)
      if AnsiCompareText(N.Attributes[sMeta], sNumber) = 0 then
        TValue(V^).ResVal := StrToInt(N.Text) // ��� �����
      else if AnsiCompareText(N.Attributes[sMeta], sFuzzy) = 0 then
        TValue(V^).ResVal := N.Text // ��� ��, ���� ����� �� �� �����?
      else if AnsiCompareText(N.Attributes[sMeta], sRef) = 0 then
        TValue(V^).ResVal := StrToInt(N.Text) // ��� ������
      else
        TValue(V^).ResVal := N.Text;
    end;

    exit; // ����� �������� �� �����
  end
  else

    // ������ �� ��������
    if AnsiCompareText(N.NodeName, sRef) = 0 then
    begin
      V^ := TReference.Create(nil);
      with V^ as TReference do
      begin
        ID := N.Attributes[sID];

        // ���� ���� ����������� ���������, �� �������� ��
        M := N.ChildNodes.FindNode(sParams);
        if M <> nil then
        begin
          for j := 0 to M.ChildNodes.Count - 1 do
          begin
            NamedParam := TIDValue.Create(nil);
            NamedParam.ID := '';
            // ����� ���������� ��������� �� ������� �� ���������� ��� ������� ������
            NamedParam.InstRef := nil;
            // ��� ������� � ��������� (�������� � runtime), ���� ����� ��������� � �������� ������
            LoadValue(@NamedParam.Value, M.ChildNodes[j]);
            params.Add(NamedParam);
          end;
        end;

        // ���� ���� ��������� ������, �� �������� ��
        M := N.ChildNodes.FindNode(sRef);
        if M <> nil then
          LoadValue(@ref, M);

        // ���� ���� ��������, �� �������� ��
        M := N.ChildNodes.FindNode(sWith);
        if M <> nil then
          LoadWith(@Attrs, M);
      end;
      exit;
    end
    else

      // ���������� �������
      if AnsiCompareText(N.NodeName, sFunc) = 0 then
      begin
        V^ := TLibFunction.Create(nil);
        with V^ as TLibFunction do
        begin
          for i := 0 to N.ChildNodes.Count - 1 do
          begin
            LoadValue(@Val, N.ChildNodes[i]);
            Operands.Add(Val);
          end;

          if AnsiCompareText(N.Attributes[sID], sCtg) = 0 then
            FuncMeta := lfCtg
          else if AnsiCompareText(N.Attributes[sID], sTg) = 0 then
            FuncMeta := lfTg
          else if AnsiCompareText(N.Attributes[sID], sSin) = 0 then
            FuncMeta := lfSin
          else if AnsiCompareText(N.Attributes[sID], sCos) = 0 then
            FuncMeta := lfCos
          else if AnsiCompareText(N.Attributes[sID], sArctg) = 0 then
            FuncMeta := lfArctg
          else if AnsiCompareText(N.Attributes[sID], sArcctg) = 0 then
            FuncMeta := lfArcctg
          else if AnsiCompareText(N.Attributes[sID], sArcSin) = 0 then
            FuncMeta := lfArcsin
          else if AnsiCompareText(N.Attributes[sID], sArccos) = 0 then
            FuncMeta := lfArccos
          else if AnsiCompareText(N.Attributes[sID], sAbs) = 0 then
            FuncMeta := lfAbs
          else if AnsiCompareText(N.Attributes[sID], sPi) = 0 then
            FuncMeta := lfPi
          else if AnsiCompareText(N.Attributes[sID], sCeil) = 0 then
            FuncMeta := lfCeil
          else if AnsiCompareText(N.Attributes[sID], sFloor) = 0 then
            FuncMeta := lfFloor
          else if AnsiCompareText(N.Attributes[sID], sFrac) = 0 then
            FuncMeta := lfFrac
          else if AnsiCompareText(N.Attributes[sID], sInt) = 0 then
            FuncMeta := lfInt
          else if AnsiCompareText(N.Attributes[sID], sRound) = 0 then
            FuncMeta := lfRound
          else if AnsiCompareText(N.Attributes[sID], sTrunc) = 0 then
            FuncMeta := lfTrunc
          else if AnsiCompareText(N.Attributes[sID], sExp) = 0 then
            FuncMeta := lfExp
          else if AnsiCompareText(N.Attributes[sID], sLn) = 0 then
            FuncMeta := lfLn
          else if AnsiCompareText(N.Attributes[sID], sLog2) = 0 then
            FuncMeta := lfLog2
          else if AnsiCompareText(N.Attributes[sID], sLog10) = 0 then
            FuncMeta := lfLog10
          else if AnsiCompareText(N.Attributes[sID], sSqrt) = 0 then
            FuncMeta := lfSqrt
          else if AnsiCompareText(N.Attributes[sID], sSqr) = 0 then
            FuncMeta := lfSqr
          else if AnsiCompareText(N.Attributes[sID], sMin) = 0 then
            FuncMeta := lfMin
          else if AnsiCompareText(N.Attributes[sID], sMax) = 0 then
            FuncMeta := lfMax
          else if AnsiCompareText(N.Attributes[sID], sOut) = 0 then
            FuncMeta := lfOut
          else if AnsiCompareText(N.Attributes[sID], sConcat) = 0 then
            FuncMeta := lfConcat
          else if AnsiCompareText(N.Attributes[sID], sSend) = 0 then
            FuncMeta := lfSend
          else
            ErrorHandler.HandleError(eWrongAttr, 'KBLoader: ���������� ������� '
              + N.Attributes[sID] + ' � ���� ' + N.NodeName);

          OnDemon := PtrOnDemon;
        end;
        exit;
      end
      else

      begin
        // ��� ��������� ������� ����������
        V^ := TOperation.Create(nil);
        Op := V^ as TOperation;

        if AnsiCompareText(N.NodeName, sEQ) = 0 then
          Op.OpMeta := omEQ
        else if AnsiCompareText(N.NodeName, sNE) = 0 then
          Op.OpMeta := omNE
        else if AnsiCompareText(N.NodeName, sGT) = 0 then
          Op.OpMeta := omGT
        else if AnsiCompareText(N.NodeName, sLT) = 0 then
          Op.OpMeta := omLT
        else if AnsiCompareText(N.NodeName, sGE) = 0 then
          Op.OpMeta := omGE
        else if AnsiCompareText(N.NodeName, sLE) = 0 then
          Op.OpMeta := omLE
        else if AnsiCompareText(N.NodeName, sNOT) = 0 then
          Op.OpMeta := omNOT
        else if AnsiCompareText(N.NodeName, sOR) = 0 then
          Op.OpMeta := omOR
        else if AnsiCompareText(N.NodeName, sXOR) = 0 then
          Op.OpMeta := omXOR
        else if AnsiCompareText(N.NodeName, sAND) = 0 then
          Op.OpMeta := omAND
        else if AnsiCompareText(N.NodeName, sADD) = 0 then
          Op.OpMeta := omADD
        else if AnsiCompareText(N.NodeName, sSUB) = 0 then
          Op.OpMeta := omSUB
        else if AnsiCompareText(N.NodeName, sDIV) = 0 then
          Op.OpMeta := omDIV
        else if AnsiCompareText(N.NodeName, sMOD) = 0 then
          Op.OpMeta := omMOD
        else if AnsiCompareText(N.NodeName, sMUL) = 0 then
          Op.OpMeta := omMUL
        else if AnsiCompareText(N.NodeName, sNEG) = 0 then
          Op.OpMeta := omNEG

          // ��������� �. �, 2023, ��������������, ����� ������������ ������������ ���������
        else if (AnsiCompareText(N.NodeName, sEvInt) = 0) or
          (AnsiCompareText(N.NodeName, sAlAttr) = 0) or
          (AnsiCompareText(N.NodeName, sIntRel) = 0) or
          (AnsiCompareText(N.NodeName, sEvRel) = 0) then
        begin;
          LoadSignifier();
          Op.OpMeta := omTP;
        end
        else
          ErrorHandler.HandleError(eWrongTag,
            'KBLoader: ����������� ��� ��� �������� ' + N.ParentNode.NodeName +
            '.' + N.NodeName);

        if (Op.OpMeta = omNEG) or (Op.OpMeta = omNOT) then
          Op.isBinary := False;

        // ��������� ���� �����

        if N.ChildNodes.Count > 0 then
        begin
          s := N.ChildNodes[0];
          if Op.OpMeta = omTP then // ���� ��������������, ���� ��������� ����
          begin
            D := TXMLDocument.Create(nil);
            p := GetNodePath(N);

            AddPropToSignifier(p);

            refXml := buildRef(p, true);
            D.LoadFromXML('<xml>' + refXml + '</xml>');
            s := D.DocumentElement.ChildNodes[0];
          end;
          LoadValue(@Op.Operand1, s)
        end
        else
          ErrorHandler.HandleError(eMissChild,
            'KBLoader: ����������� ������ ������� � ���� ' +
            N.ParentNode.NodeName + '.' + N.NodeName);

        if Op.isBinary then
        begin
          if Op.OpMeta = omTP then
          begin
            D.LoadFromXML('<xml><value>TRUE</value></xml>');
            s := D.DocumentElement.ChildNodes[0];
            LoadValue(@Op.Operand2, s);
          end
          else if N.ChildNodes.Count > 1 then
            LoadValue(@Op.Operand2, N.ChildNodes[1])
          else
            ErrorHandler.HandleError(eMissChild,
              'KBLoader: ����������� ������ ������� � ���� ' +
              N.ParentNode.NodeName + '.' + N.NodeName);
        end;

        if Op.OpMeta = omTP then
        begin
          Op.OpMeta := omEQ;
        end;

        // ���� ���� �������� � �������� ���������, �� �������� ��
        M := N.ChildNodes.FindNode(sWith);
        if M <> nil then
          LoadWith(Op.Attrs, M);
      end;
end;

procedure TXMLLoader.LoadInstrList(L: TList; N: IXMLNode);
var
  i: integer;
  inst: TInstruction;
  M, kid: IXMLNode;
begin
  // � L - ������ ����������, � N - ������ ������
  for i := 0 to N.ChildNodes.Count - 1 do
  begin
    M := N.ChildNodes[i];
    inst := nil;

    // ���������� ��������� ����������
    if AnsiCompareText(M.NodeName, sVarDecl) = 0 then
    begin
      inst := TVarDeclInstruction.Create(nil);
      with inst as TVarDeclInstruction do
      begin
        if not VarIsNull(M.Attributes[sID]) then
          ID := M.Attributes[sID]
        else
          ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' +
            sID + ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);

        // �������� ������ �� ���
        if not VarIsNull(M.Attributes[sType]) then
          TypeRef := GetTypeRefByID(M.Attributes[sType])
        else
          ErrorHandler.HandleError(eMissAttr, 'KBLoader: ����������� ������� ' +
            sType + ' � ���� ' + M.ParentNode.NodeName + '.' + M.NodeName);

        // �������� ��������� ��������, ���� �������������
        kid := M.ChildNodes.FindNode(sValue);
        if kid <> nil then
          LoadValue(@Default, kid);

        if M.Attributes[sDisabled] = sTrue then
          Disabled := true; // �� ��������� false

        if not VarIsNull(M.Attributes[sComment]) then
          Comment := M.Attributes[sComment];
      end
    end
    else

      // ����
      if AnsiCompareText(M.NodeName, sWhile) = 0 then
      begin
        inst := TWhileInstruction.Create(nil);
        with inst as TWhileInstruction do
        begin
          if not VarIsNull(M.Attributes[sCheckBefore]) and
            M.Attributes[sCheckBefore] = sFalse then
            CheckBefore := False; // �� ��������� true

          LoadValue(@Condition, M.ChildNodes[sCondition].ChildNodes[0]);
          LoadInstrList(Instructions, M.ChildNodes[sStatements]);

          if M.Attributes[sDisabled] = sTrue then
            Disabled := true; // �� ��������� false

          if not VarIsNull(M.Attributes[sComment]) then
            Comment := M.Attributes[sComment];
        end
      end
      else

        // �������� �����������
        if AnsiCompareText(M.NodeName, sIf) = 0 then
        begin
          inst := TIfInstruction.Create(nil);
          with inst as TIfInstruction do
          begin
            LoadValue(@Condition, M.ChildNodes[sCondition].ChildNodes[0]);

            LoadInstrList(Instructions, M.ChildNodes[sAction]);
            if M.ChildNodes.Count > 2 then
              LoadInstrList(Instructions, M.ChildNodes[sElseAction]);

            if M.Attributes[sDisabled] = sTrue then
              Disabled := true; // �� ��������� false

            if not VarIsNull(M.Attributes[sComment]) then
              Comment := M.Attributes[sComment];
          end
        end
        else

          // ������������
          if AnsiCompareText(M.NodeName, sAssign) = 0 then
          begin
            inst := TAssignInstruction.Create(nil);
            with inst as TAssignInstruction do
            begin
              LoadValue(@VarRef, M.ChildNodes[0]);
              // ��������� ������ �� ����������
              LoadValue(@Expr, M.ChildNodes[1]); // ��������� ��������

              // ��������� ��������, ���� ����
              kid := M.ChildNodes.FindNode(sWith);
              if kid <> nil then
                LoadWith(Attrs, kid);

              if M.Attributes[sByRef] = sTrue then
                ByRef := true; // �� ��������� false

              if M.Attributes[sDisabled] = sTrue then
                Disabled := true; // �� ��������� false

              if not VarIsNull(M.Attributes[sComment]) then
                Comment := M.Attributes[sComment];
            end
          end
          else

            // �������� ���������� ������
            if AnsiCompareText(M.NodeName, sCreate) = 0 then
            begin
              inst := TCreateInstruction.Create(nil);
              with inst as TCreateInstruction do
              begin
                LoadValue(@VarRef, M.ChildNodes[sRef]);
                // ��������� ������ �� ����������
                kid := M.ChildNodes[sClassInfo];
                ClassGUID := kid.Text; // ��������� �������� ������
                if AnsiCompareText(kid.Attributes[sMeta], sKBID) = 0 then
                  ClassMeta := amKBID
                else if AnsiCompareText(kid.Attributes[sMeta], sProgID) = 0 then
                  ClassMeta := amProgID
                else if AnsiCompareText(kid.Attributes[sMeta], sCLSID) = 0 then
                  ClassMeta := amCLSID;
                // ���� ��������� �� ������ �������, ����� ���������� ����� ��������� ���������� �������
                ClassesRef := KBC.Classes;

                if M.Attributes[sDisabled] = sTrue then
                  Disabled := true; // �� ��������� false

                if not VarIsNull(M.Attributes[sComment]) then
                  Comment := M.Attributes[sComment];
              end
            end
            else

              // ����� ������
              if AnsiCompareText(M.NodeName, sCall) = 0 then
              begin
                inst := TCallInstruction.Create(nil);
                with inst as TCallInstruction do
                begin
                  LoadValue(@MethodRef, M.ChildNodes[0]);

                  if M.Attributes[sDisabled] = sTrue then
                    Disabled := true; // �� ��������� false

                  if not VarIsNull(M.Attributes[sComment]) then
                    Comment := M.Attributes[sComment];
                end
              end
              else

                // ������� ��������
                if AnsiCompareText(M.NodeName, sReturn) = 0 then
                begin
                  inst := TReturnInstruction.Create(nil);
                  with inst as TReturnInstruction do
                  begin
                    LoadValue(@Value, M.ChildNodes[0]);

                    if M.Attributes[sDisabled] = sTrue then
                      Disabled := true; // �� ��������� false

                    if not VarIsNull(M.Attributes[sComment]) then
                      Comment := M.Attributes[sComment];
                  end
                end;

    // ������������ ����
    if AnsiCompareText(M.NodeName, sSetGoal) = 0 then
    begin
      inst := TSetGoalInstruction.Create(nil);
      with inst as TSetGoalInstruction do
      begin
        LoadValue(@VarRef, M.ChildNodes[sRef]);

        // �������� ���� (���� ����)
        Value := nil;
        if M.ChildNodes.FindNode(sValue) <> nil then
          LoadValue(@Value, M.ChildNodes[sValue]);

        if M.Attributes[sDisabled] = sTrue then
          Disabled := true; // �� ��������� false

        if not VarIsNull(M.Attributes[sComment]) then
          Comment := M.Attributes[sComment];
      end
    end;

    if inst <> nil then
    begin
      L.Add(inst);
    end;
  end;
end;

procedure TXMLLoader.LoadWith(Attrs: TAttrList; N: IXMLNode);
begin
  // �����������
  if not VarIsNull(N.Attributes[sBelief]) then
    Attrs.Belief := StrToInt(N.Attributes[sBelief]) / 100;

  // �����������
  if not VarIsNull(N.Attributes[sProbability]) then
    Attrs.Probability := StrToInt(N.Attributes[sProbability]) / 100;

  // �������� (������������� �����������)
  if not VarIsNull(N.Attributes[sAccuracy]) then
    Attrs.Accuracy := StrToInt(N.Attributes[sAccuracy]) / 100;
end;

// ������� ������ ������������ �������� ������� ������
// ������� ��������� ���������, ����������� �����, ��������� ������ � �.�.
// ����� ����� ���� ���������� ����� � ������������ ����
procedure TXMLLoader.LoadOM(OM: TOMContainer; N: IXMLNode; FireRules: boolean);
var
  i, j: integer;
  inst: TInstance;
  idv: TIDValue;
  M, obj, prop, rec: IXMLNode;
  KBClass: TKBClass;
  Goal: TGoal;
  cr: TConflictRec;
  RS: TRuleState;
begin
  // �������� �������
  for i := 0 to N.ChildNodes[sObjects].ChildNodes.Count - 1 do
  begin
    obj := N.ChildNodes[sObjects].ChildNodes[i];
    KBClass := GlbFindItem(KBC.Classes, obj.Attributes[sType]) as TKBClass;

    if KBClass = nil then
    begin
      ErrorHandler.HandleError(eInvalidID, 'KBLoader: �� ������ ����� ' +
        obj.Attributes[sType]);
      continue;
    end;

    inst := KBClass.CreateInstance as TInstance;
    inst.OMRef := OM;

    // �������� �������� �������
    M := obj.ChildNodes.FindNode(sProperties);
    if M <> NIL then
      for j := 0 to M.ChildNodes.Count - 1 do
      begin
        prop := M.ChildNodes[j]; // property-value
        idv := TIDValue.Create(inst);
        idv.ID := prop.Attributes[sID];

        // �������� ��������, ���� ��� ����
        if prop.ChildNodes.Count > 0 then
        begin
          LoadValue(@idv.Value, prop.ChildNodes[0]);
        end;

        inst.PropertyList.Add(idv);
      end;

    // �������� �������� �����
    M := obj.ChildNodes.FindNode(sRuleStates);
    if M <> NIL then
      for j := 0 to M.ChildNodes.Count - 1 do
      begin
        rec := M.ChildNodes[j]; // rule-state
        RS := TRuleState.Create(nil);

        // !!! �������� !!!
      end;

    // �������� ����������� �����
    M := obj.ChildNodes.FindNode(sCRS);
    if M <> NIL then
      for j := 0 to M.ChildNodes.Count - 1 do
      begin
        rec := M.ChildNodes[j]; // rule
        cr := TConflictRec.Create(nil);
        cr.Rate := rec.Attributes[sRate];

        // !!! �������� !!!
      end;

    if FireRules then
      KBClass.FireInitialRules(inst);

    OM.Objects.Add(inst);
  end;

  // �������� ������ �����
  for i := 0 to N.ChildNodes[sGoals].ChildNodes.Count - 1 do
  begin
    Goal := TGoal.Create(nil);
    M := N.ChildNodes[sGoals].ChildNodes[i];

    if M.Attributes[sAchieved] = sTrue then
      Goal.Achieved := true
    else
      Goal.Achieved := False;

    if M.Attributes[sPrimary] = sTrue then
      Goal.Primary := true
    else
      Goal.Primary := False;

    LoadValue(@Goal.GoalRef, M.ChildNodes[sRef]);

    if M.ChildNodes.FindNode(sValue) <> nil then
      LoadValue(@Goal.ValueRef, M.ChildNodes[sValue]);
    OM.Goals.Add(Goal);
  end;
end;

procedure TXMLLoader.ResolveReferences;
var
  i: integer;
  KBClass: TKBClass;
begin
  for i := 0 to KBC.Classes.Count - 1 do
  begin
    KBClass := KBC.Classes.Items[i];
    // ���� ����� ���������� �� ������� ������ ��, �� ���� � ���� ��� �������� ��������
    if KBClass.AncestorMeta = amKBID then
    begin

    end;
  end;
end;

end.
