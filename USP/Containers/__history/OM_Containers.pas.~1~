unit OM_Containers;

 {******************************************************************************
 * ������ OM_Containers.pas �������� ����������� ��������� �������:
 *
 * TIDValue - ��������� �������� ����������
 * TGoal - ��������� ����
 * TConflictRec - ������ � ������� � ����������� ������
 *
 * �����: << ������� �. �. >>
 * ���� ��������: << 2 ������, 2003 >>
 ******************************************************************************}

interface

uses
  Classes, XMLDoc, XMLIntf, SysUtils,
  KB_Common, KB_Containers, KB_Values;

type            

  ////////////////////////////////////////////////////////////////////////////
  // ����� TIDValue �������� ����������� ��� �������� ����������
  ////////////////////////////////////////////////////////////////////////////

  TIDValue = class(TNamedItem)  // ������������� ���� DTD 'property-value'
  public
    // � ������������� ���� ID ����� ��� ��������, ��������� ���������� ��� ���������
    InstRef: TInstance;         // ������ �� ������������ ������
    Value: TAbstractValue;      // OM, �������� ��������, ��������� ���������� ��� ���������
    Asked: boolean;             // OM, ������������� �� �������� � ����������� ����������
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TGoal �������� ����������� ��� �������� ���������
  ////////////////////////////////////////////////////////////////////////////

  TGoal = class(TXMLItem)       // ������������� ���� DTD 'goal'
  public
    RefString: WideString;      // ��������� ������������� ������ �� ������� ��������
    GoalRef: TReference;        // OM, ��������� ��� �������� �������� ������������ ������� world
    ValueRef: TAbstractValue;   // OM, TValue ��� TReference, �������� �������� ��������, ������� ��������� �����������
    Achieved: boolean;          // OM, ���������� �� ����
    Primary: boolean;           // OM, ��������� ���� (���������� � ����� ����, � �� ���������)
    Inferrable: boolean;        // OM, �������� �� ���� � ��������
    CycleID: integer;           // OM, ����� ����� ������ ��������, �� ������� ���� ���� ����������
    SubGoals: TList;            // OM, ������ �������� ���� TGoal
    RefCount: integer;          // OM, ������� ������ �� ����� ������ ������ ������� �� ��� ����
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure XML(Parent: IXMLNode); override;
    procedure FindSubgoals(Env: TInstance);
    procedure SelectGoalForSubdialog(World: TInstance; var CurBestGoal: TGoal); // �������� ���� ��� ����������� ����������
    procedure CheckAchieved(Env: TInstance);
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TConflictRec �������� ������ � ������� � ����������� ������
  ////////////////////////////////////////////////////////////////////////////

  TConflictRec = class(TXMLItem)// ������������� ���� DTD 'rec'
  public
    Env: TInstance;             // OM, ������, ��� ������� �������� ������� ������
    Rule: TNamedItem;           // OM, �� ����� ���� TClassRule
    CondValue: TValue;          // OM, �������� ������� �������
    Rate: integer;              // ������ ���������� �������
    CycleID: integer;           // ����� ����� ��������, �� ������� ������� ������ � ����������� �����
    Then_: boolean;             // ������� ������������ then �����, ������������� ��������� � Fire
    procedure XML(Parent: IXMLNode); override;
  end;


  ////////////////////////////////////////////////////////////////////////////
  // ����� TRuleState �������� ������ � ��������� �������
  ////////////////////////////////////////////////////////////////////////////

  TRuleState = class(TXMLItem)  // ������������� ���� DTD 'state'
  public
    Rule: TNamedItem;           // OM, �� ����� ���� TClassRule
    Active: boolean;
    procedure XML(Parent: IXMLNode); override;
  end;


implementation

uses
  KB_Instructions, KB_Types;

////////////////////////////////////////
// ������ ������ TGoal
/////////////////////////////////////////

constructor TGoal.Create(AOwner: TComponent);
begin
  inherited;
  SubGoals := TList.Create();
  RefCount := 1;
  //Asked := false;
end;

destructor TGoal.Destroy;
begin
  SubGoals.Free;
  inherited;
end;

procedure TGoal.XML(Parent: IXMLNode);
var
  N,M: IXMLNode;
  i: integer;
  G: TGoal;
begin
  N := Parent.AddChild(sGoal);
  GoalRef.XML(N);

  if ValueRef <> nil then ValueRef.XML(N);

  if Achieved then N.Attributes[sAchieved] := sTrue
  else N.Attributes[sAchieved] := sFalse;

  if Primary then N.Attributes[sPrimary] := sTrue
  else N.Attributes[sPrimary] := sFalse;

  if Inferrable then N.Attributes[sInferrable] := sTrue
  else N.Attributes[sInferrable] := sFalse;

  //N.Attributes[sCycle] := CycleID;

  N.Attributes[sRefCount] := RefCount;

  if Subgoals.Count > 0 then begin
    M := N.AddChild(sGoals);
    for i:=0 to Subgoals.Count-1 do begin
      G := Subgoals[i];
      G.XML(M);
    end;
  end;
end;


procedure TGoal.FindSubgoals(Env: TInstance);
var
  r: TClassRule;
  KBClass: TKBClass;
  i: integer;
begin
  KBClass := Env.ClassRef as TKBClass;
  for i:= 0 to KBClass.Rules.Count-1 do begin
    r := KBClass.Rules[i];
    // ���� � ������� ��� ���� ������������
    if r.Rule.Achieves(Env, Self) then begin
      // �� ��� �������� � ������� ���� ������� ���������
      r.Rule.Condition.SpawnSubgoals(Env, Self.SubGoals);
    end;
  end;
end;

// �������� ���� ��� ����������� ����������
procedure TGoal.SelectGoalForSubdialog(World: TInstance; var CurBestGoal: TGoal);
var
  G: TGoal;
  i: integer;
  asked: boolean;
  IDValue: TIDValue;
begin
  // ���� ��� ���������� ����, �� nil
  if Achieved then exit;
  if Inferrable then begin
    for i:=0 to Subgoals.Count-1 do begin
      G := Subgoals[i];
      G.SelectGoalForSubdialog(World, CurBestGoal);
    end;
  end
  else
    // ����������
    // ��������� ������������� �� �� ��������. ���� �� �� ���� ������
    IDValue := Self.GoalRef.GetIDValue(World) as TIDValue;
    asked := (IDValue <> nil) and IDValue.Asked;
    if not asked then begin
      // ��� ���, ����� ������� � ������� ������ ��������, ���� ����� ����
      if (CurBestGoal <> nil) then begin
        if (CurBestGoal.RefCount < Self.RefCount) then
          CurBestGoal := Self;
      end
      else
        CurBestGoal := Self;
    end;
end;

// ���������, ���������� �� ���� � ������������� ���� Achieved
procedure TGoal.CheckAchieved(Env: TInstance);
var
  G: TGoal;
  i: integer;
  IDValue: TIDValue;
begin
  if Achieved then exit;
  IDValue := Self.GoalRef.GetIDValue(Env) as TIDValue;
  Achieved := (IDValue <> nil) and (IDValue.Value <> nil);
  // ��� �������� ���� ��������
  for i:=0 to Subgoals.Count-1 do begin
    G := Subgoals[i];
    G.CheckAchieved(Env);
  end;

end;

////////////////////////////////////////
// ������ ������ TIDValue
/////////////////////////////////////////

procedure TIDValue.XML(Parent: IXMLNode);
var
  N: IXMLNode;
begin
  N := Parent.AddChild(sPropertyValue);
  N.Attributes[sID] := ID;
  if Value <> nil then Value.XML(N); // TAbstractValue

  if Asked then N.Attributes[sAsked] := sTrue
  else N.Attributes[sAsked] := sFalse;
end;


////////////////////////////////////////
// ������ ������ TConflictRec
/////////////////////////////////////////

procedure TConflictRec.XML(Parent: IXMLNode);
var
  N,M: IXMLNode;
begin
  N := Parent.AddChild(sRule);
  N.Attributes[sID] := Rule.ID;
  N.Attributes[sRate] := Rate;
  if Then_ then N.Attributes[sThen] := 'true' else N.Attributes[sThen] := 'false';

  M := N.AddChild(sObject);
  M.Attributes[sAddr] := '$'+IntToHex(integer(Env), 8);
  M.Attributes[sType] := Env.ClassID;

  M := N.AddChild(sCondition);
  CondValue.XML(M);
end;


////////////////////////////////////////
// ������ ������ TRuleState
/////////////////////////////////////////

procedure TRuleState.XML(Parent: IXMLNode);
var
  N: IXMLNode;
begin
  N := Parent.AddChild(sRuleState);
  N.Attributes[sID] := Rule.ID;
  if Active then N.Attributes[sActive] := sTrue
  else N.Attributes[sActive] := sFalse
end;

end.
