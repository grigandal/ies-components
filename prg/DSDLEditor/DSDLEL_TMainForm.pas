unit DSDLEL_TMainForm;

interface

  uses
    Windows,
    Messages,
    SysUtils,
    Classes,
    Graphics,
    Controls,
    Forms,
    Dialogs,
    ExtCtrls,
    DSDLEC_TDSDLEField,
    DSDLEC_TEditField,
    DSDLEC_TTreeField,
    DSDLEC_TMessagesField,
    Menus,
    ToolWin,
    ComCtrls,
    Buttons;

  type
    TMainForm = class(TForm)
      EditField: TEditField;
      MessagesField: TMessagesField;
      StatusBar: TStatusBar;
      ToolBar: TToolBar;
      MainMenu: TMainMenu;
      FileMenuItem: TMenuItem;
      NemMenuItem: TMenuItem;
      OpenMenuItem: TMenuItem;
      SaveMenuItem: TMenuItem;
      SaveAsMenuItem: TMenuItem;
      SeparatorMenuItem1: TMenuItem;
      ExitMenuItem: TMenuItem;
      GSplitter: TSplitter;
      VSplitter: TSplitter;
      NewSpeedButton: TSpeedButton;
      OpenSpeedButton: TSpeedButton;
      SaveSpeedButton: TSpeedButton;
      TreeField: TTreeField;
      EditMenuItem: TMenuItem;
      ToolsMenuItem: TMenuItem;
      DebugMenuItem: TMenuItem;
      RunMenuItem: TMenuItem;
      RunCurrentMenuItem: TMenuItem;
      CheckErrorsMenuItem: TMenuItem;
      CheckAccessibilityTimer: TTimer;
      CheckErrorsSpeedButton: TSpeedButton;
      RunSpeedButton: TSpeedButton;
      RunCurrentSpeedButton: TSpeedButton;
      SeparatorSpeedButton1: TSpeedButton;
      OpenDialog: TOpenDialog;
      SaveDialog: TSaveDialog;
      CutMenuItem: TMenuItem;
      CopyMenuItem: TMenuItem;
      PasteMenuItem: TMenuItem;
      DeleteMenuItem: TMenuItem;
      SelectAllMenuItem: TMenuItem;
      UndoMenuItem: TMenuItem;
      SeparatorMenuItem2: TMenuItem;
      PasteSpeedButton: TSpeedButton;
      CopySpeedButton: TSpeedButton;
      CutSpeedButton: TSpeedButton;
      UndoSpeedButton: TSpeedButton;
      SeparatorSpeedButton2: TSpeedButton;

      procedure FormShow(Sender: TObject);
      procedure ExitMenuItemClick(Sender: TObject);
      procedure CheckErrorsMenuItemClick(Sender: TObject);
      procedure RunMenuItemClick(Sender: TObject);
      procedure RunCurrentMenuItemClick(Sender: TObject);
      procedure CheckAccessibilityTimerTimer(Sender: TObject);
      procedure NemMenuItemClick(Sender: TObject);
      procedure SaveAsMenuItemClick(Sender: TObject);
      procedure SaveMenuItemClick(Sender: TObject);
      procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
      procedure OpenMenuItemClick(Sender: TObject);
      procedure EditFieldChangeText(Text: String; CurPos: Integer);
      procedure EditFieldChangePos(Pos: Integer);
      procedure TreeFieldSetPos(Pos: Integer);
      procedure MessagesFieldSetPos(Line, Col: Integer);
      procedure FormCreate(Sender: TObject);
      procedure UndoMenuItemClick(Sender: TObject);
      procedure CutMenuItemClick(Sender: TObject);
      procedure CopyMenuItemClick(Sender: TObject);
      procedure PasteMenuItemClick(Sender: TObject);
      procedure DeleteMenuItemClick(Sender: TObject);
      procedure SelectAllMenuItemClick(Sender: TObject);

    private
      FFileNameForced : Boolean;
      FStopped : Boolean;

      procedure SetFileNameForced(Value : Boolean);
      procedure SaveIfNeeded(var CanContinue : Boolean);

    public
      property Stopped : Boolean read FStopped write FStopped;
      property FileNameForced : Boolean read FFileNameForced write SetFileNameForced;
    end;


implementation

{$R *.DFM}

  uses
    ComObj,
    Variants,
    Clipbrd,
    XMLIntf,
    XMLDoc,
    DSDLEC_Types,
    DSDLEL_Constants;


  procedure TMainForm.SetFileNameForced(Value : Boolean);
  begin
    FFileNameForced := Value;
    SaveAsMenuItem.Visible := not Value;
    NemMenuItem.Visible := not Value;
    OpenMenuItem.Visible := not Value;
    NewSpeedButton.Visible := not Value;
    OpenSpeedButton.Visible := not Value;
  end;


  procedure TMainForm.FormCreate(Sender: TObject);
  begin
    FFileNameForced := False;
    Stopped := False;
  end;


  procedure TMainForm.FormShow(Sender: TObject);
  begin
    if EditField.Text = '' then NemMenuItemClick(Self);
  end;


  procedure TMainForm.EditFieldChangeText(Text: String; CurPos: Integer);
  begin
    TreeField.RefreshTree(Text, CurPos);
  end;


  procedure TMainForm.EditFieldChangePos(Pos: Integer);
  var
    Line, Col : Integer;
    ItemName : String;
    ItemKind : TItemKind;
  begin
    TreeField.SetActiveItem(Pos, ItemName, ItemKind);

    EditField.GetTextPos(Line, Col);
    StatusBar.Panels[0].Text := IntToStr(Line) + ' : ' + IntToStr(Col);

//    TreeField.GetActiveItem(ItemName, ItemKind);
    RunCurrentMenuItem.Enabled := ItemKind <> ikNoItem;
    RunCurrentSpeedButton.Enabled := ItemKind <> ikNoItem;
  end;


  procedure TMainForm.TreeFieldSetPos(Pos: Integer);
  begin
    EditField.SetTextPos(Pos);
  end;


  procedure TMainForm.MessagesFieldSetPos(Line, Col: Integer);
  begin
    EditField.SetTextPos(Line, Col);
  end;


  procedure TMainForm.ExitMenuItemClick(Sender: TObject);
  begin
    Close();
  end;


  procedure TMainForm.CheckErrorsMenuItemClick(Sender: TObject);
  var
    Dialoger : OleVariant;
    ErrText : OleVariant;
    OldCursor : TCursor;
  begin
    MessagesField.Messages := msgFindingInProgress;
    OldCursor := Cursor;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages();

    Dialoger := CreateOleObject('DialogerLib.Dialoger');
    Dialoger.Test(EditField.Text, ErrText);
    Dialoger := Unassigned;
    MessagesField.Messages := ErrText;

    Screen.Cursor := OldCursor;
    Application.ProcessMessages();
  end;


  procedure TMainForm.RunMenuItemClick(Sender: TObject);
  var
    Config : IXMLDocument;
    Starter : OleVariant;
  begin
    Config := TXMLDocument.Create(nil);
    Config.LoadFromXML(RunConfig);
    Config.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes[0].NodeValue := EditField.Text;

    Starter := CreateOleObject('StarterLib.Starter');
    Starter.Config := Config.DocumentElement.XML;
    Starter.Start;
  end;


  procedure TMainForm.RunCurrentMenuItemClick(Sender: TObject);
  var
    ItemName : String;
    ItemKind : TItemKind;
    Config : IXMLDocument;
    Starter : OleVariant;
  begin
    TreeField.GetActiveItem(ItemName, ItemKind);
    case ItemKind of
      ikNoItem : ;

      ikMessage :
      begin
        Config := TXMLDocument.Create(nil);
        Config.LoadFromXML(SendConfig);
        Config.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes[0].NodeValue := EditField.Text;
        Config.DocumentElement.ChildNodes[1].ChildNodes[0].ChildNodes[0].NodeValue := ItemName;

        Starter := CreateOleObject('StarterLib.Starter');
        Starter.Config := Config.DocumentElement.XML;
        Starter.Start;
      end;

      ikScenario :
      begin
        Config := TXMLDocument.Create(nil);
        Config.LoadFromXML(ExecuteConfig);
        Config.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes[0].NodeValue := EditField.Text;
        Config.DocumentElement.ChildNodes[1].ChildNodes[0].ChildNodes[0].NodeValue := ItemName;

        Starter := CreateOleObject('StarterLib.Starter');
        Starter.Config := Config.DocumentElement.XML;
        Starter.Start;
      end;
    end;
  end;


  procedure TMainForm.SaveIfNeeded(var CanContinue : Boolean);
  var
    DlgRes : Integer;
  begin
    CanContinue := True;
    if EditField.Modified then
    begin
      if Stopped
      then DlgRes := MessageDlg('���� ��� �������. ��������� ���������?',
                                mtConfirmation, [mbYes, mbNo], 0)
      else DlgRes := MessageDlg('���� ��� �������. ��������� ���������?',
                                mtConfirmation, [mbYes, mbNo, mbCancel], 0);

      case DlgRes of
        mrYes :
        begin
          if SaveDialog.FileName = '' then CanContinue := SaveDialog.Execute();
          if CanContinue then EditField.Save(SaveDialog.FileName);
        end;

        mrNo : ;

        mrCancel : CanContinue := False;
      end;
    end;
  end;


  procedure TMainForm.NemMenuItemClick(Sender: TObject);
  var
    CanContinue : Boolean;
  begin
    SaveIfNeeded(CanContinue);

    if CanContinue then
    begin
      EditField.Text := NewDSDLText;
      EditField.SetTextPos(1);
      if not FileNameForced then
      begin
        OpenDialog.FileName := '';
        SaveDialog.FileName := '';
      end;
      EditField.Modified := False;
    end;
  end;


  procedure TMainForm.OpenMenuItemClick(Sender: TObject);
  var
    CanContinue : Boolean;
  begin
    SaveIfNeeded(CanContinue);

    if CanContinue then
    begin
      if OpenDialog.Execute() then EditField.Load(OpenDialog.FileName);
      SaveDialog.FileName := OpenDialog.FileName;
    end;
  end;


  procedure TMainForm.SaveAsMenuItemClick(Sender: TObject);
  begin
    if SaveDialog.Execute() then
    begin
      EditField.Save(SaveDialog.FileName);
      OpenDialog.FileName := SaveDialog.FileName;
    end;
  end;


  procedure TMainForm.SaveMenuItemClick(Sender: TObject);
  begin
    if SaveDialog.FileName <> ''
    then EditField.Save(SaveDialog.FileName)
    else SaveAsMenuItemClick(Self);
  end;


  procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    SaveIfNeeded(CanClose);
  end;


  procedure TMainForm.CheckAccessibilityTimerTimer(Sender: TObject);
  begin
    UndoMenuItem.Enabled := EditField.Memo.CanUndo;
    UndoSpeedButton.Enabled := EditField.Memo.CanUndo;

    CutMenuItem.Enabled := EditField.Memo.SelLength > 0;
    CutSpeedButton.Enabled := EditField.Memo.SelLength > 0;

    CopyMenuItem.Enabled := EditField.Memo.SelLength > 0;
    CopySpeedButton.Enabled := EditField.Memo.SelLength > 0;

    PasteMenuItem.Enabled := ClipBoard.AsText <> '';
    PasteSpeedButton.Enabled := ClipBoard.AsText <> '';

    DeleteMenuItem.Enabled := EditField.Memo.SelLength > 0;
  end;


  procedure TMainForm.UndoMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.Undo;
  end;


  procedure TMainForm.CutMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.CutToClipboard;
  end;


  procedure TMainForm.CopyMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.CopyToClipboard;
  end;


  procedure TMainForm.PasteMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.PasteFromClipboard;
  end;


  procedure TMainForm.DeleteMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.ClearSelection;
  end;


  procedure TMainForm.SelectAllMenuItemClick(Sender: TObject);
  begin
    EditField.Memo.SelectAll;
  end;

end.
