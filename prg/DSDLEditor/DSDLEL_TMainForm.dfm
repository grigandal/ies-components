�
 TMAINFORM 0�3  TPF0	TMainFormMainFormLeft� Top� WidthHeight4Caption2   Редактор сценариев диалогаColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MenuMainMenuOldCreateOrderPositionpoDesktopCenterOnCloseQueryFormCloseQueryOnCreate
FormCreatePixelsPerInch`
TextHeight 	TSplitter	GSplitterLeft Top�WidthHeightCursorcrVSplitAlignalBottomMinSize  	TSplitter	VSplitterLeft� TopWidthHeight�CursorcrHSplitMinSize  
TEditField	EditFieldLeft� TopWidthXHeight�AlignalClient
BevelInner	bvLowered
BevelOuterbvNone
BevelWidthConstraints.MinHeightConstraints.MinWidthTabOrder OnChangeTextEditFieldChangeTextOnChangePosEditFieldChangePos  TMessagesFieldMessagesFieldLeft Top�WidthHeight9AlignalBottom
BevelInner	bvLowered
BevelOuterbvNone
BevelWidthConstraints.MinHeightConstraints.MinWidthTabOrderOnSetPosMessagesFieldSetPos  
TStatusBar	StatusBarLeft Top�WidthHeightPanels	AlignmenttaCenterText1 : 1WidthU Width2  SimplePanel  TToolBarToolBarLeft Top WidthHeightCaptionToolBarTabOrder TSpeedButtonNewSpeedButtonLeft TopWidthHeightHint   Создать (Ctrl+N)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337�333330����337�333330����3?��333��������ww�333w;������7w�3?�ww30��  337�3wws330���3337�37�330��3337�3w�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphsParentShowHintShowHint	OnClickNemMenuItemClick  TSpeedButtonOpenSpeedButtonLeftTopWidthHeightHint   Открыть (Ctrl+O)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337��?�330� 337�sws330����3?����?���� �ww�wssw;������7w��?�ww30�  337�swws330���3337��7�330��3337�sw�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphsParentShowHintShowHint	OnClickOpenMenuItemClick  TSpeedButtonSaveSpeedButtonLeft.TopWidthHeightHint   Сохранить (Ctrl+S)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333 pw 3333wwww3333 �� 3333w�3w3333 �� 3333w��w3333    ?���wwww        wwwwwwww������333337������?����� �̜�w7swwww����9�3?����w�  �𙙓ww77ww�������?���www �  9�3w7ww7w����9�3?��s7w���3y�3w7�?ww3�����3��swws3   33333www33333	NumGlyphsParentShowHintShowHint	OnClickSaveMenuItemClick  TSpeedButtonSeparatorSpeedButton1LeftETopWidth
HeightEnabledFlat	  TSpeedButtonUndoSpeedButtonLeftOTopWidthHeightHint   Откат (Ctrl+Z)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� P  UUUWwwuU�U    UPUUwwwwU�UU U PUUUw�wWUUU UPUUUUw�W_�UUPP UUUW�uw�UUU PPUUUw�Ww�UUU	�UUU�uwww�UPU	��UW_wwwu�PP��0UWu�wwW_U PU	UwWUwuuuUUUUP��0UU_UWWWWUUUU3UUuUUuuUUUUUUP��UU�UUW_UUPUUUU�UWUUUUu�UUUUUUP�UUUUUUW_UUUUUUUUUUUUUUu	NumGlyphsParentShowHintShowHint	OnClickUndoMenuItemClick  TSpeedButtonCutSpeedButtonLeftfTopWidthHeightHint   Вырезать (Ctrl+X)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333333333�33333303333337333333s33��33ws3< 330w337w�37w����3p  ww�wwww�3�0w���w�w�w����  wwwpwwwwwwww<��     7wwwwwww330���33�?��3< ��37ww773�<���3w�?373�<��3w�ss?�3� 3�  3ww�?ww3<�3�37w3s73333�3333�s3333   33333www3	NumGlyphsParentShowHintShowHint	OnClickCutMenuItemClick  TSpeedButtonCopySpeedButtonLeft}TopWidthHeightHint   Копировать (Ctrl+C)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333���333w33w333���333w33w333���333w33w333���?��w��w     w pwwwwwwww���� w�3337��w���� {�?����w � � �w7w7ww����9�3?����w�  �𙙓ww77ww�������?���www �  9�3w7ww7w����9�3?��s7w���3y�3w7�?ww3�����3��swws3   33333www33333	NumGlyphsParentShowHintShowHint	OnClickCopyMenuItemClick  TSpeedButtonPasteSpeedButtonLeft� TopWidthHeightHint   Вставить (Ctrl+V)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333����333?���333  ��333ww77333����3���3��p   �  7www�ww������wwwww��sww{���3wwwww��3����3w33w�3��� �3w33ww����9�3w33w�w�������w33www������w33wwww���9�3w��w�w� w p9�3wwwww�w�w w�9�3w���w�w3� {���3ww�wwws3p   3337wwws333	NumGlyphsParentShowHintShowHint	OnClickPasteMenuItemClick  TSpeedButtonSeparatorSpeedButton2Left� TopWidth
HeightEnabledFlat	  TSpeedButtonCheckErrorsSpeedButtonLeft� TopWidthHeightHint8   Проверить на наличие ошибок (F7)Flat	
Glyph.Data
z  v  BMv      v   (                                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UP    UUUUUUUUUUP����UUUUUUUUUUP���UUUUUUUUUUP���UUUUUUUUUUP����UUUUUUUUUUZ��UUUUUUUUUU����UUUUUUUUUZ� ���UUUUUUUUU� ����UUUUUUUUUUP�
�UUUUUUUUUUP���UUUUUUUUUUP���UUUUUUUUUUP���UUUUUUUUUUP   Z�UUUUUUUUUUUUUUUZ�UUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphsParentShowHintShowHint	OnClickCheckErrorsMenuItemClick  TSpeedButtonRunSpeedButtonLeft� TopWidthHeightHint7   Выполнить главный сценарий (F9)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333333333333333330333333333333333�33333333333333� 33333333333333��3333333333333�� 3333333333333���333333333333��� 333333333333����333333333333���3333333333333���3333333333333��33333333333333��33333333333333�333333333333333�33333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickRunMenuItemClick  TSpeedButtonRunCurrentSpeedButtonLeft� TopWidthHeightHint:   Выполнить текущий элемент (Ctrl+F9)Flat	
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33     333�����333����3����??�30  � �3���?��83�����3�3?�33�3�� � �3�3����83����� �3�3?���83��  
� 3�3����83������ 3�?�����3�   
���������3� ���� ���88���0 ����8���8���30�����33883���33�
��33��8���30����338��38�33	���33������3303  
33383���333	NumGlyphsParentShowHintShowHint	OnClickRunCurrentMenuItemClick   
TTreeField	TreeFieldLeft TopWidth� Height�AlignalLeft
BevelInner	bvLowered
BevelOuterbvNone
BevelWidthConstraints.MinHeightConstraints.MinWidthTabOrderOnSetPosTreeFieldSetPos  	TMainMenuMainMenuLeft TopB 	TMenuItemFileMenuItemCaption   $09; 	TMenuItemNemMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333�33;;�������3�    330����330����330����330����3�������;������30��  330���3330��3330�� ;�33�  3�3;�3;�3;��33;333;Caption   !>740BLShortCutN@OnClickNemMenuItemClick  	TMenuItemOpenMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333�33;;�������3�    330����330����330� 330����3��� �;������30�  330���3330�3330�� ;�33�  3�3;�3;�3;��33;333;Caption   Открыть...ShortCutO@OnClickOpenMenuItemClick  	TMenuItemSaveMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333 �� 3333 pw 3333 ww 3333            ������������ �̜�����9�3  �𙙓������� �  9�3���9�3��3��3�����3   33333Caption	   !>E@0=8BLShortCutS@OnClickSaveMenuItemClick  	TMenuItemSaveAsMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCaption   Сохранить как...ShortCutS�  OnClickSaveAsMenuItemClick  	TMenuItemSeparatorMenuItem1Caption-  	TMenuItemExitMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3     33����33���33���33��33��33��33��33��33��33���33��33��33���33���33     3Caption   KE>4OnClickExitMenuItemClick   	TMenuItemEditMenuItemCaption   @02:0 	TMenuItemUndoMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� P  UUU    UPUU U PUUU UPUUUUPP UUUU PPUUUU	�UUPU	��UPP��0UU PU	UUUUP��0UUUU3UUUUUP��UPUUUU�UUUUUUP�UUUUUUUCaption   B:0BShortCutZ@OnClickUndoMenuItemClick  	TMenuItemSeparatorMenuItem2Caption-  	TMenuItemCutMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33333333333330333333�3< 330�33��3�  �3�0�����  ����<��     330���3< ��3�<���3�<��3� 3�  3<�3�3333�3333   3Caption   K@570BLShortCutX@OnClickCutMenuItemClick  	TMenuItemCopyMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333����333����333����333����     � ����� ������ �� � � �����9�3  �𙙓������� �  9�3���9�3��3��3�����3   33333Caption
   >?8@>20BLShortCutC@OnClickCopyMenuItemClick  	TMenuItemPasteMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333����333  ��333�����   �  ������������3�����3���� �3����9�3������������������9�3 � �9�3� ��9�3� ����3�   333Caption   AB028BLShortCutV@OnClickPasteMenuItemClick  	TMenuItemDeleteMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCaption   G8AB8BLShortCut.@OnClickDeleteMenuItemClick  	TMenuItemSelectAllMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCaption   Выделить всеShortCutA@OnClickSelectAllMenuItemClick   	TMenuItemDebugMenuItemCaption   B;04:0 	TMenuItemCheckErrorsMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UP    UUP����UUP���UUP���UUP����UUZ��UU����UZ� ���U� ����UUP�
�UUP���UUP���UUP���UUP   Z�UUUUUUUZ�UUUUUUUUCaption3   Проверить на наличие ошибокShortCutvOnClickCheckErrorsMenuItemClick  	TMenuItemRunMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33333333333333333: 333333:�33333:� 33333:��3333:�� 3333:���333:���3333:���3333:��33333:��33333:�333333:�333333:3333333333333Caption2   Выполнить главный сценарийShortCutxOnClickRunMenuItemClick  	TMenuItemRunCurrentMenuItemBitmap.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 33     333����30  � �3�����3�� � �3����� �3��  
� 3������ 3�   
��� ���� 0 ����30�����33�
��30����33	���3303  
333Caption0   Выполнить текущий элементShortCutx@OnClickRunCurrentMenuItemClick   	TMenuItemToolsMenuItemCaption   =AB@C<5=BK   TTimerCheckAccessibilityTimerInterval2OnTimerCheckAccessibilityTimerTimerLeft@Top@  TOpenDialog
OpenDialog
DefaultExtdsfFilterH   Файлы сценариев диалога|*.dsf|Все файлы|*.*Title   Открытие файлаLeft Toph  TSaveDialog
SaveDialogFilterH   Файлы сценариев диалога|*.dsf|Все файлы|*.*Title   Сохранение файлаLeft@Toph   