unit DSDLEL_TDSDLEditor;

{$WARN SYMBOL_PLATFORM OFF}

interface

  uses
    ComObj,
    ActiveX,
    Variants,
    Forms,
    DSDLEditorLib_TLB,
    DSDLEL_TMainForm,
    StdVcl;

  type
    TDSDLEditor = class(TAutoObject, IDSDLEditor)
    private
      Name : WideString;
      Broker : OleVariant;

      MainForm : TMainForm;
      FileName : WideString;

    protected
      function Get_Name: WideString; safecall;
      procedure Set_Name(const Value: WideString); safecall;
      function Get_Broker: OleVariant; safecall;
      procedure Set_Broker(Value: OleVariant); safecall;

      procedure Configurate(const Config: WideString); safecall;
      procedure ProcessMessage(const SndrName, MsgText: WideString; out Output: OleVariant); safecall;
      procedure Stop; safecall;
      { Protected declarations }

    public
      procedure Initialize; override;
      destructor Destroy; override;
    end;


implementation

  uses
    ComServ,
    XMLIntf,
    XMLDoc,
    SysUtils,
    DSDLEL_Constants;


  procedure TDSDLEditor.Initialize;
  begin
    inherited Initialize;
  end;


  destructor TDSDLEditor.Destroy;
  begin
    inherited Destroy;
  end;


  function TDSDLEditor.Get_Name: WideString;
  begin
    Result := Name;
  end;


  procedure TDSDLEditor.Set_Name(const Value: WideString);
  begin
    Name := Value;
  end;


  function TDSDLEditor.Get_Broker: OleVariant;
  begin
    Result := Broker;
  end;

  procedure TDSDLEditor.Set_Broker(Value: OleVariant);
  begin
    Broker := Value;
  end;


  procedure TDSDLEditor.Configurate(const Config: WideString);
  var
    XMLDoc : IXMLDocument;
    i : Integer;
    path: WideString;
  begin
    XMLDoc := TXMLDocument.Create(nil);
    XMLDoc.LoadFromXML(Config);

    for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
    begin
      if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = FileNameNode
      then begin
        FileName := XMLDoc.DocumentElement.ChildNodes[i].Text;
        // ���� ���� �������������, �� ������� ����
        if AnsiPos(':\', FileName) = 0 then begin
          if VarIsNull(Broker) then path := ExtractFilePath(Application.ExeName)
          else path := Broker.Path;
          FileName := path + FileName;
        end;
      end;
    end;
  end;


  procedure TDSDLEditor.ProcessMessage(const SndrName, MsgText: WideString; out Output: OleVariant);
  var
    XMLDoc : IXMLDocument;
    ProcName : WideString;
    DSNode : IXMLNode;
    V : Variant;
  begin
    XMLDoc := TXMLDocument.Create(nil);
    XMLDoc.LoadFromXML(MsgText);

    if (UpperCase(XMLDoc.DocumentElement.NodeName) = MessageNode) then
    begin
      ProcName := UpperCase(XMLDoc.DocumentElement.Attributes[ProcNameAttr]);

      //��������� ��������� ��� DesignTime...
      if ProcName = UpperCase(CreateUserInterfaceProc) then
      begin
        DSNode := XMLDoc.DocumentElement.ChildNodes[0].ChildNodes[0].ChildNodes.FindNode(DialogScenarioNode);
        if DSNode=nil then
        begin
          DSNode := XMLDoc.DocumentElement.ChildNodes[0].ChildNodes[0].AddChild(DialogScenarioNode);
          DSNode.Attributes[FileNameAttr] := '';
        end;
        FileName := DSNode.Attributes[FileNameAttr];

        MainForm := TMainForm.Create(nil);

        try
          MainForm.EditField.Load(FileName);
          MainForm.SaveDialog.FileName := FileName;
          MainForm.OpenDialog.FileName := FileName;
        except
          MainForm.NemMenuItemClick(nil);
        end;

        MainForm.ShowModal();
        DSNode.Attributes[FileNameAttr] := WideString(MainForm.SaveDialog.FileName);

        MainForm.Free;
        MainForm := nil;

        Broker.SendMessage(DSDLEditorName, ATProjectName,
                           '<Message ProcName="SetProjectValues"><Param><ProjectValues>'+ XMLDoc.DocumentElement.ChildNodes[0].ChildNodes[0].XML+ '</ProjectValues></Param></Message>',
                           V);
      end;


      //��������� ��������� ��� RunTime...
      if (ProcName = UpperCase(RunProc)) and not Assigned(MainForm) then
      begin
        MainForm := TMainForm.Create(nil);
        try
          MainForm.EditField.Load(FileName);
          MainForm.SaveDialog.FileName := FileName;
          MainForm.OpenDialog.FileName := FileName;
        except
          MainForm.NemMenuItemClick(nil);
        end;
        MainForm.ShowModal();
        MainForm.Free();
        MainForm := nil;
      end;
    end;
  end;


  procedure TDSDLEditor.Stop;
  begin
    if Assigned(MainForm) then
    begin
      MainForm.Stopped := True;
      MainForm.Close();
    end;
  end;


initialization

  TAutoObjectFactory.Create(ComServer, TDSDLEditor, Class_DSDLEditor, ciMultiInstance, tmApartment);
  
end.
