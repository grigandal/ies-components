library DSDLEditorLib;

uses
  ComServ,
  DSDLEditorLib_TLB in 'DSDLEditorLib_TLB.pas',
  DSDLEL_TDSDLEditor in 'DSDLEL_TDSDLEditor.pas' {DSDLEditor: CoClass},
  DSDLEL_Constants in 'DSDLEL_Constants.pas',
  DSDLEL_TMainForm in 'DSDLEL_TMainForm.pas' {MainForm};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
