unit DSDLEL_Constants;

interface

  const
    RunConfig =
      '<config>' +
        '<object Name="Dialoger" Class="DialogerLib.Dialoger">' +
          '<config>' +
            '<DSDLText></DSDLText>' +
          '</config>' +
        '</object>' +

        '<message RcvrName="Dialoger">' +
          '<message ProcName="Run">' +
          '</message>' +
        '</message>' +
      '</config>';

    ExecuteConfig =
      '<config>' +
        '<object Name="Dialoger" Class="DialogerLib.Dialoger">' +
          '<config>' +
            '<DSDLText></DSDLText>' +
          '</config>' +
        '</object>' +

        '<message RcvrName="Dialoger">' +
          '<message ProcName="Execute">' +
            '<ScenarioName></ScenarioName>' +
          '</message>' +
        '</message>' +
      '</config>';

    SendConfig =
      '<config>' +
        '<object Name="Dialoger" Class="DialogerLib.Dialoger">' +
          '<config>' +
            '<DSDLText></DSDLText>' +
          '</config>' +
        '</object>' +

        '<message RcvrName="Dialoger">' +
          '<message ProcName="Send">' +
            '<MessageName></MessageName>' +
          '</message>' +
        '</message>' +
      '</config>';

    NewDSDLText = '/������� ��������/' + #13 + #10 +
                  'scenario ���������������;' + #13 + #10 +
                  '  ' + #13 + #10 +
                  'end;' + #13 + #10 +
                  '' + #13 + #10 +
                  '/�������������� ��������/' + #13 + #10 +
                  '' + #13 + #10 +
                  '/���������/';

    FileNameNode = 'FILENAME';
    DSDLTextNode = 'DSDLTEXT';
    MessageNode = 'MESSAGE';
    ProcNameAttr = 'ProcName';
    FileNameAttr = 'FileName';
    DialogScenarioNode = 'DialogScenario';
    RunProc = 'RUN';
    CreateUserInterfaceProc = 'CREATEUSERINTERFACE';

    msgFindingInProgress = '���� ����� ������ � ������...';

    DSDLEditorName = 'DSDLEditor';
    ATProjectName = 'ATProject';

implementation

end.
