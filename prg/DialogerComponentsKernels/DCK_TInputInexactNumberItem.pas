unit DCK_TInputInexactNumberItem;

interface
  uses
    Classes,
    StdCtrls,
    DCK_TInputItem;

  type
    TInputInexactNumberItem = class(TInputItem)
      private
        Edit : TEdit;
        PromptLabel : TLabel;
        AccEdit : TEdit;
        AccLabel : TLabel;

      protected
        function GetPrompt() : String; override;
        procedure SetPrompt(Value : String); override;

        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        function CanClickOK() : Boolean; override;
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;


implementation
  uses
    Controls,
    SysUtils,
    DCK_Constants,
    DCK_Types, DCK_TDialogItem;


  function TInputInexactNumberItem.GetPrompt() : String;
  begin
    Result := PromptLabel.Caption;
  end;


  procedure TInputInexactNumberItem.SetPrompt(Value : String);
  begin
    PromptLabel.Caption := Value;
    AlignContents();
  end;


  procedure TInputInexactNumberItem.CreateContents();
  begin
    Edit := TEdit.Create(Self);
    InsertControl(Edit);
    Edit.ParentFont := True;
    Edit.Ctl3D := constCtl3D;

    PromptLabel := TLabel.Create(Self);
    InsertControl(PromptLabel);
    PromptLabel.ParentFont := True;
    PromptLabel.FocusControl := Edit;

    AccEdit := TEdit.Create(Self);
    InsertControl(AccEdit);
    AccEdit.ParentFont := True;
    AccEdit.Ctl3D := constCtl3D;

    AccLabel := TLabel.Create(Self);
    InsertControl(AccLabel);
    AccLabel.Caption := '�����������(%):';
    AccLabel.ParentFont := True;
    AccLabel.FocusControl := AccEdit;
  end;


  procedure TInputInexactNumberItem.AlignContents();
  begin
    ClientHeight := Edit.Height;

    AccLabel.Left := 0;
    AccLabel.Top := 2;
    AccLabel.Left := ClientWidth - constAccuracyEditWidth - AccLabel.Width - constSpaceWidth;

    AccEdit.Top := 0;
    AccEdit.Left := ClientWidth - constAccuracyEditWidth;
    AccEdit.Width := constAccuracyEditWidth;

    //***
    PromptLabel.Left := 0;
    PromptLabel.Top := 2;

    Edit.Top := 0;
    if PromptLabel.Caption <> '' then
    begin
      Edit.Left := constSpaceWidth + PromptLabel.Width;
      Edit.Width := ClientWidth - (PromptLabel.Width + 4 * constSpaceWidth + constAccuracyEditWidth + AccLabel.Width);
    end else
    begin
      Edit.Left := 0;
      Edit.Width := 3 * constSpaceWidth + constAccuracyEditWidth + AccLabel.Width;
    end;
    Edit.Height := ClientHeight;
  end;


  function TInputInexactNumberItem.CanClickOK() : Boolean;
  begin
    {
    try
      StrToFloat(Edit.Text);
      StrToFloat(AccEdit.Text);
      Result := True;
    except
      Result := False;
    end;
    }
    //����� � ������ �� �������...
    Result := True;
    if (Edit.Text <> '') or (AccEdit.Text <> '') then
    try
      StrToFloat(Edit.Text);
      StrToFloat(AccEdit.Text);
    except
      Result := False;
    end;
  end;


  procedure TInputInexactNumberItem.PrepareForInput();
  var
    Value : String;
  begin
    OnGetAttrValue(AttrName, Value);
    try
      StrToFloat(Value);
      Edit.Text := Value;
    except
      Edit.Text := '';
    end;

    OnGetAttrProp(AttrName, constAccuracy, Value);
    try
      StrToFloat(Value);
      AccEdit.Text := Value;
    except
      AccEdit.Text := '0';
    end;
  end;


  procedure TInputInexactNumberItem.ConfirmInput();
  begin
    OnSetAttrValue(AttrName, Edit.Text);
    OnSetAttrProp(AttrName, constAccuracy, AccEdit.Text);
  end;

end.
