unit DCK_TDialogItem;

interface
  uses
    Controls,
    ExtCtrls,
    Classes;


  type
    TDialogItem = class(TPanel)
      private
        function GetClientHeight() : Integer;
        procedure SetClientHeight(Value : Integer);
        function GetClientWidth() : Integer;

        procedure AlignSelf();

      protected
        procedure AlignAll();
        procedure CreateContents(); virtual; abstract;
        procedure AlignContents(); virtual; abstract;

      public
        property ClientWidth : Integer read GetClientWidth;
        property ClientHeight : Integer read GetClientHeight write SetClientHeight;

        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;
    end;

implementation
  uses
    DCK_Constants;


  constructor TDialogItem.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Left := constSpaceWidth;
    Top := 0;
    Width := (Owner as TControl).ClientWidth - 2 * constSpaceWidth;
    Height := 0;
    BevelInner := bvNone;
    BevelOuter := bvNone;
    ParentFont := True;
    (Owner as TWinControl).InsertControl(Self);

    (Owner as TControl).Height := (Owner as TControl).Height + constSpaceHeight;

    CreateContents();
    AlignContents();

    AlignAll();
  end;


  destructor TDialogItem.Destroy();
  begin
    inherited Destroy();
  end;


  procedure TDialogItem.AlignSelf();
  var
    i : Integer;
    LowerEdge : Integer;
  begin
    i := ComponentIndex - 1;
    while (i >= 0) and not (Owner.Components[i] is TDialogItem) do Dec(i);
    if i >= 0
    then LowerEdge := (Owner.Components[i] as TDialogItem).Top + (Owner.Components[i] as TDialogItem).Height
    else LowerEdge := 0;
    Top := LowerEdge + constSpaceHeight;
  end;


  procedure TDialogItem.AlignAll();
  var
    i : Integer;
  begin
    for i := 0 to Owner.ComponentCount - 1 do
      if Owner.Components[i] is TDialogItem
      then (Owner.Components[i] as TDialogItem).AlignSelf();
  end;


  function TDialogItem.GetClientHeight() : Integer;
  begin
    Result := inherited ClientHeight;
  end;


  procedure TDialogItem.SetClientHeight(Value : Integer);
  begin
    (Owner as TControl).Height := (Owner as TControl).Height + (Value - inherited ClientHeight);
    inherited ClientHeight := Value;
    AlignAll();
  end;


  function TDialogItem.GetClientWidth() : Integer;
  begin
    Result := inherited ClientWidth;
  end;

end.
