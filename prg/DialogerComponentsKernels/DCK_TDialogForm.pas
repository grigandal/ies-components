unit DCK_TDialogForm;

interface

  uses
    Windows,
    Messages,
    SysUtils,
    Classes,
    Graphics, 
    Controls,
    Forms,
    Dialogs,
    ExtCtrls,
    Buttons,
    StdCtrls,
    CheckLst,
    ComCtrls,
    ToolWin,
    Menus,
    DCK_Types;

  type
    TDialogForm = class(TForm)
      HelpSpeedButton: TSpeedButton;
      OKSpeedButton: TSpeedButton;
      CancelSpeedButton: TSpeedButton;
      CanClickOKTimer: TTimer;
      PopupMenu: TPopupMenu;
      MenuSpeedButton: TSpeedButton;
      procedure OKSpeedButtonClick(Sender : TObject);
      procedure CancelSpeedButtonClick(Sender: TObject);
      procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
      procedure HelpSpeedButtonClick(Sender: TObject);
      procedure CanClickOKTimerTimer(Sender: TObject);
      procedure FormClose(Sender: TObject; var Action: TCloseAction);
      procedure FormActivate(Sender: TObject);
      procedure MenuSpeedButtonClick(Sender: TObject);

    private
      OKButtonPressed : Boolean;
      FHelpFile : String;

    public
      property HelpFile : String read FHelpFile write FHelpFile;
  end;


implementation

{$R *.DFM}

  uses
    DCK_Constants,
    DCK_TInputItem,
    DCK_TDialogFormKernel;


  procedure TDialogForm.FormActivate(Sender: TObject);
  var
    i : Integer;
  begin
    OKButtonPressed := False;
    MenuSpeedButton.Enabled := PopupMenu.Items.Count > 0;
    HelpSpeedButton.Enabled := HelpFile <> '';

    if not Assigned((Owner as TDialogFormKernel).OnCancelStep) then
    begin
      OKSpeedButton.Left := CancelSpeedButton.Left;
      CancelSpeedButton.Visible := False;
    end;


    for i := 0 to ComponentCount - 1 do
      if Components[i] is TInputItem then
      begin
        (Components[i] as TInputItem).OnSetAttrValue := (Owner as TDialogFormKernel).OnSetAttrValue;
        (Components[i] as TInputItem).OnSetAttrProp := (Owner as TDialogFormKernel).OnSetAttrProp;
        (Components[i] as TInputItem).OnGetAttrValue := (Owner as TDialogFormKernel).OnGetAttrValue;
        (Components[i] as TInputItem).OnGetAttrProp := (Owner as TDialogFormKernel).OnGetAttrProp;

        (Components[i] as TInputItem).PrepareForInput();
      end;
  end;


  procedure TDialogForm.OKSpeedButtonClick(Sender: TObject);
  var
    i : Integer;
  begin
    OKButtonPressed := True;

    for i := 0 to ComponentCount - 1 do
      if Components[i] is TInputItem then (Components[i] as TInputItem).ConfirmInput();

    Close();
  end;


  procedure TDialogForm.CancelSpeedButtonClick(Sender: TObject);
  begin
    if Assigned((Owner as TDialogFormKernel).OnCancelStep) then (Owner as TDialogFormKernel).OnCancelStep();
    Close();
  end;


  procedure TDialogForm.HelpSpeedButtonClick(Sender: TObject);
  begin
    if Assigned ((Owner as TDialogFormKernel).OnViewHelp) then (Owner as TDialogFormKernel).OnViewHelp(HelpFile);
  end;


  procedure TDialogForm.MenuSpeedButtonClick(Sender: TObject);
  var
    ScreenPoint : TPoint;
  begin
    ScreenPoint := ClientToScreen(Point(MenuSpeedButton.Left, MenuSpeedButton.Top + MenuSpeedButton.Height));

    PopupMenu.Popup(ScreenPoint.X, ScreenPoint.Y);

    MenuSpeedButton.Down := False;
  end;


  procedure TDialogForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  begin
    if (Key = 13) and OKSpeedButton.Enabled then OKSpeedButtonClick(Self);
    if Key = 27 then CancelSpeedButtonClick(Self);
    if (Key = 112) and HelpSpeedButton.Enabled then HelpSpeedButtonClick(Self);
  end;


  procedure TDialogForm.CanClickOKTimerTimer(Sender: TObject);
  var
    i : Integer;
    IsOkEnabled : Boolean;
  begin
    IsOkEnabled := True;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TInputItem
      then IsOkEnabled := IsOkEnabled and ((Components[i] as TInputItem).CanClickOK());
    OKSpeedButton.Enabled := IsOkEnabled;
  end;


  procedure TDialogForm.FormClose(Sender: TObject; var Action: TCloseAction);
  var
    i : Integer;
  begin
    for i := 0 to ComponentCount - 1 do
      if (Components[i] is TInputItem) and Assigned((Components[i] as TInputItem).OnSetAttrProp)
      then (Components[i] as TInputItem).OnSetAttrProp((Components[i] as TInputItem).AttrName,
                                                        constAskedProp,
                                                        constTruePropValue);

    if not OKButtonPressed and Assigned((Owner as TDialogFormKernel).OnCancelStep)
    then (Owner as TDialogFormKernel).OnCancelStep();
  end;

end.
