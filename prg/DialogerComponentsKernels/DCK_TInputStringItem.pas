unit DCK_TInputStringItem;

interface
  uses
    Classes,
    StdCtrls,
    DCK_TInputItem;

  type
    TInputStringItem = class(TInputItem)
      private
        Edit : TEdit;
        PromptLabel : TLabel;

      protected
        function GetPrompt() : String; override;
        procedure SetPrompt(Value : String); override;

        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        function CanClickOK() : Boolean; override;
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;


implementation
  uses
    Controls,
    SysUtils,
    DCK_Constants,
    DCK_Types;


  function TInputStringItem.GetPrompt() : String;
  begin
    Result := PromptLabel.Caption;
  end;


  procedure TInputStringItem.SetPrompt(Value : String);
  begin
    PromptLabel.Caption := Value;
    AlignContents();
  end;


  procedure TInputStringItem.CreateContents();
  begin
    Edit := TEdit.Create(Self);
    InsertControl(Edit);
    Edit.ParentFont := True;
    Edit.Ctl3D := constCtl3D;

    PromptLabel := TLabel.Create(Self);
    InsertControl(PromptLabel);
    PromptLabel.ParentFont := True;
    PromptLabel.FocusControl := Edit;
  end;


  procedure TInputStringItem.AlignContents();
  begin
    ClientHeight := Edit.Height;

    PromptLabel.Left := 0;
    PromptLabel.Top := 2;

    Edit.Top := 0;
    if PromptLabel.Caption <> '' then
    begin
      Edit.Left := constSpaceWidth + PromptLabel.Width;
      Edit.Width := ClientWidth - (PromptLabel.Width + constSpaceWidth);
    end else
    begin
      Edit.Left := 0;
      Edit.Width := ClientWidth;
    end;
    Edit.Height := ClientHeight;
  end;


  function TInputStringItem.CanClickOK() : Boolean;
  {
  var
    i : Integer;
  }
  begin
    Result := True;
    {
    Result := True;
    for i := 1 to Length(Edit.Text) do Result := Result and (Edit.Text[i] = ' ');
    Result := not Result;
    }
  end;


  procedure TInputStringItem.PrepareForInput();
  var
    AttrValue : String;
  begin
    OnGetAttrValue(AttrName, AttrValue);
    Edit.Text := AttrValue;
  end;


  procedure TInputStringItem.ConfirmInput();
  begin
    OnSetAttrValue(AttrName, Edit.Text);
    //�� ���������, CF=100%...
    OnSetAttrProp(AttrName, constBelief, IntToStr(constBeliefMax));
  end;

end.
