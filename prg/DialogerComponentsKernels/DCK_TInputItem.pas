unit DCK_TInputItem;

interface
  uses
    Classes,
    Controls,
    StdCtrls,
    DCK_Types,
    DCK_TDialogItem;


  type
    TInputItem = class(TDialogItem)
      private
        FAttrName : String;
        FValue : String;

        FOnSetAttrValue : TOnSetAttrValue;
        FOnSetAttrProp : TOnSetAttrProp;
        FOnGetAttrValue : TOnGetAttrValue;
        FOnGetAttrProp : TOnGetAttrProp;

      protected
        function GetPrompt() : String; virtual; abstract;
        procedure SetPrompt(Value : String); virtual; abstract;

      public
        constructor Create(AOwner : TComponent); override;

        function CanClickOK() : Boolean; virtual; abstract;
        procedure PrepareForInput(); virtual; abstract;
        procedure ConfirmInput(); virtual; abstract;

      published
        property Prompt : String read GetPrompt write SetPrompt;
        property AttrName : String read FAttrName write FAttrName;

        property OnSetAttrValue : TOnSetAttrValue read FOnSetAttrValue write FOnSetAttrValue;
        property OnSetAttrProp : TOnSetAttrProp read FOnSetAttrProp write FOnSetAttrProp;
        property OnGetAttrValue : TOnGetAttrValue read FOnGetAttrValue write FOnGetAttrValue;
        property OnGetAttrProp : TOnGetAttrProp read FOnGetAttrProp write FOnGetAttrProp;
    end;


implementation
  uses
    DCK_Constants;


  constructor TInputItem.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    FAttrName := '';
    FValue := '';
  end;

end.
