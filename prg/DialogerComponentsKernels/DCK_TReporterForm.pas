unit DCK_TReporterForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ToolWin, ComCtrls;

type
  TReporterForm = class(TForm)
    ReportMemo: TMemo;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CloseSpeedButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReporterForm: TReporterForm;

implementation

{$R *.DFM}

  procedure TReporterForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  begin
    if (Key = 27) then CloseSpeedButtonClick(Self);
  end;


  procedure TReporterForm.CloseSpeedButtonClick(Sender: TObject);
  begin
    Close();
  end;

end.
