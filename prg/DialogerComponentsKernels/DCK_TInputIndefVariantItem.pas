unit DCK_TInputIndefVariantItem;

interface
  uses
    Classes,
    StdCtrls,
    CheckLst,
    ComCtrls,
    DCK_TInputItem,
    DCK_TInputVariantItem;

  type
    TInputIndefVariantItem = class(TInputVariantItem)
      private
        BeliefTrackBar : TTrackBar;
        BeliefLabel : TLabel;
        LowTickLabel : TLabel;
        MedTickLabel : TLabel;
        HiTickLabel : TLabel;

      protected
        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;

    
implementation
  uses
    SysUtils,
    Controls,
    LR_Functions,
    DCK_Constants,
    DCK_Types,
    DCK_TDialogItem;


  procedure TInputIndefVariantItem.CreateContents();
  begin
    inherited CreateContents();

    BeliefTrackBar := TTrackBar.Create(Self);
    InsertControl(BeliefTrackBar);
    BeliefTrackBar.Min := constBeliefTrackBarMin;
    BeliefTrackBar.Max := constBeliefTrackBarMax;
    BeliefTrackBar.TickMarks := tmTopLeft;
    BeliefTrackBar.TickStyle := tsAuto;

    BeliefLabel := TLabel.Create(Self);
    InsertControl(BeliefLabel);
    BeliefLabel.Caption := '�����������';

    LowTickLabel := TLabel.Create(Self);
    InsertControl(LowTickLabel);
    LowTickLabel.Caption := '  0%';
    LowTickLabel.Transparent := True;
    LowTickLabel.Alignment := taLeftJustify;

    MedTickLabel := TLabel.Create(Self);
    InsertControl(MedTickLabel);
    MedTickLabel.Caption := ' 50%';
    MedTickLabel.Transparent := True;
    MedTickLabel.Alignment := taCenter;

    HiTickLabel := TLabel.Create(Self);
    InsertControl(HiTickLabel);
    HiTickLabel.Caption := '100%';
    HiTickLabel.Transparent := True;
    HiTickLabel.Alignment := taRightJustify;
  end;


  procedure TInputIndefVariantItem.AlignContents();
  begin
    inherited AlignContents();

    ClientHeight := ClientHeight + constBeliefTrackBarHeight + constSpaceHeight + constBeliefTickHeight;

    BeliefLabel.Left := 0;
    BeliefLabel.Top := RadioListBox.Top + RadioListBox.Height +
                       constSpaceHeight +
                       constBeliefTickHeight + constBeliefTrackBarHeight -
                       BeliefLabel.Height - 5;

    LowTickLabel.Top := RadioListBox.Top + RadioListBox.Height + constSpaceHeight;
    LowTickLabel.Left := BeliefLabel.Left + BeliefLabel.Width + constSpaceWidth;
    LowTickLabel.Width := ClientWidth - LowTickLabel.Left;
    LowTickLabel.Height := constBeliefTickHeight;

    MedTickLabel.Top := LowTickLabel.Top;
    MedTickLabel.Left := LowTickLabel.Left;
    MedTickLabel.Width := LowTickLabel.Width;
    MedTickLabel.Height := LowTickLabel.Height;

    HiTickLabel.Top := LowTickLabel.Top;
    HiTickLabel.Left := LowTickLabel.Left;
    HiTickLabel.Width := LowTickLabel.Width;
    HiTickLabel.Height := LowTickLabel.Height;

    BeliefTrackBar.Left := BeliefLabel.Left + BeliefLabel.Width + constSpaceWidth;
    BeliefTrackBar.Top := RadioListBox.Top + RadioListBox.Height + constSpaceHeight + constBeliefTickHeight;
    BeliefTrackBar.Width := ClientWidth - BeliefTrackBar.Left;
    BeliefTrackBar.Height := constBeliefTrackBarHeight;
  end;


  procedure TInputIndefVariantItem.PrepareForInput();
  var
    PropValue : String;
  begin
    inherited PrepareForInput();

    OnGetAttrProp(AttrName, constBelief, PropValue);
    try
      BeliefTrackBar.Position := round(constBeliefTrackBarMin +
                                       (StrToInt(PropValue) - constBeliefMin) *
                                       ((constBeliefTrackBarMax - constBeliefTrackBarMin) /
                                        (constBeliefMax - constBeliefMin)));
    except
      BeliefTrackBar.Position := constBeliefTrackBarMax;
    end;
  end;


  procedure TInputIndefVariantItem.ConfirmInput();
  begin
    inherited ConfirmInput();

    OnSetAttrProp(AttrName, constBelief,
                  IntToStr(round(constBeliefMin +
                                 (BeliefTrackBar.Position  - constBeliefTrackBarMin) *
                                 ((constBeliefMax - constBeliefMin) /
                                  (constBeliefTrackBarMax - constBeliefTrackBarMin)))));
  end;

end.
