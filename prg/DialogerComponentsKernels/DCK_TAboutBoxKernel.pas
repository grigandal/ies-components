unit DCK_TAboutBoxKernel;

interface
  uses
    Classes,
    Forms,
    DCK_TAboutBoxForm;

  type
    TAboutBoxKernel = class(TComponent)
      private
        AboutBoxForm : TAboutBoxForm;

        function GetCaption() : String;
        procedure SetCaption(Value : String);

        function GetPictureFile() : String;
        procedure SetPictureFile(Value : String);

        function GetProgramName() : String;
        procedure SetProgramName(Value : String);

        function GetAboutText() : String;
        procedure SetAboutText(Value : String);

      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure Activate();
        procedure Stop();

      published
        property Caption : String read GetCaption write SetCaption;
        property PictureFile : String read GetPictureFile write SetPictureFile;
        property ProgramName : String read GetProgramName write SetProgramName;
        property AboutText : String read GetAboutText write SetAboutText;
    end;


implementation
  uses
    DCK_Constants;


  constructor TAboutBoxKernel.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    AboutBoxForm := TAboutBoxForm.Create(Self);
  end;


  destructor TAboutBoxKernel.Destroy();
  begin
    AboutBoxForm.Free();
    inherited Destroy();
  end;


  function TAboutBoxKernel.GetCaption() : String;
  begin
    Result := AboutBoxForm.Caption;
  end;


  procedure TAboutBoxKernel.SetCaption(Value : String);
  begin
    AboutBoxForm.Caption := Value;
  end;


  function TAboutBoxKernel.GetPictureFile() : String;
  begin
    Result := '';
  end;


  procedure TAboutBoxKernel.SetPictureFile(Value : String);
  begin
    try
      AboutBoxForm.Image.Picture.LoadFromFile(Value);
    except
    end;
  end;


  function TAboutBoxKernel.GetProgramName() : String;
  begin
    Result := AboutBoxForm.ProgramNameStaticText.Caption;
  end;


  procedure TAboutBoxKernel.SetProgramName(Value : String);
  begin
    AboutBoxForm.ProgramNameStaticText.Caption := Value;
  end;


  function TAboutBoxKernel.GetAboutText() : String;
  begin
    Result := AboutBoxForm.AboutTextMemo.Lines.Text;
  end;


  procedure TAboutBoxKernel.SetAboutText(Value : String);
  begin
    AboutBoxForm.AboutTextMemo.Lines.Text := Value;
  end;


  procedure TAboutBoxKernel.Activate();
  begin
    AboutBoxForm.ShowModal();
  end;


  procedure TAboutBoxKernel.Stop();
  begin
    AboutBoxForm.Close();
  end;

end.
