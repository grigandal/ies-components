unit DCK_TAlternativerKernel;

interface
  uses
    Classes,
    Forms,
    Menus,
    DCK_Types,
    DCK_TAlternativerForm,
    DCK_TAlternative;

  type
    TAlternativerKernel = class(TComponent)
      private
        MainForm : TAlternativerForm;
        FCaption : String;
        FPictureFile : String;

        FOnExecuteSubscenario : TOnExecuteSubscenario;
        FOnStopAll : TOnStopAll;

        procedure SetPictureFile(Value : String);

      protected
        procedure CreateMainFormIfNotExists();
        procedure DestroyMainFormIfExists();
        function ItemByName(OwnerItem : TMenuItem; ItemName : String) : TAlternative;

      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure AddAlternative(AlternativePath, SubscenarioName : String);
        procedure Activate();
        procedure Stop();

      published
        property Caption : String read FCaption write FCaption;
        property PictureFile : String read FPictureFile write SetPictureFile;

        property OnExecuteSubscenario : TOnExecuteSubscenario read FOnExecuteSubscenario write FOnExecuteSubscenario;
        property OnStopAll : TOnStopAll read FOnStopAll write FOnStopAll;
    end;


implementation
  uses
    DCK_Constants,
    DCK_TOutputStringItem,
    DCK_TOutputAttentionItem,
    DCK_TOutputQuestionItem;


  constructor TAlternativerKernel.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    MainForm := nil;
    FCaption := '';
    FPictureFile := '';
  end;


  destructor TAlternativerKernel.Destroy();
  begin
    DestroyMainFormIfExists();
    inherited Destroy();
  end;


  procedure TAlternativerKernel.CreateMainFormIfNotExists();
  begin
    if MainForm = nil then MainForm := TAlternativerForm.Create(Self);
    if Caption <> '' then MainForm.Caption := Caption;
  end;


  procedure TAlternativerKernel.DestroyMainFormIfExists();
  begin
    if MainForm <> nil then MainForm.Destroy();
    MainForm := nil;
  end;


  function TAlternativerKernel.ItemByName(OwnerItem : TMenuItem; ItemName : String) : TAlternative;
  var
    i : Integer;
    Found : Boolean;
  begin
    i := 0;
    Found := False;

    while (i < OwnerItem.Count) and not Found do
    begin
      Found := OwnerItem.Items[i].Caption = ItemName;
      Inc(i);
    end;

    if Found
    then Result := OwnerItem.Items[i - 1] as TAlternative
    else Result := nil;
  end;


  procedure TAlternativerKernel.SetPictureFile(Value : String);
  begin
    CreateMainFormIfNotExists();
    FPictureFile := Value;
    try
      MainForm.Image.Picture.LoadFromFile(PictureFile);
      MainForm.ClientWidth := MainForm.Image.Picture.Width;
      MainForm.ClientHeight := MainForm.ClientHeight + MainForm.Image.Picture.Height;
    except
    end;
  end;


  procedure TAlternativerKernel.AddAlternative(AlternativePath, SubscenarioName : String);
  var
    i : Integer;
    AlternativeName : String;
    CurOwnerItem : TMenuItem;
    CurItem : TAlternative;
  begin
    CreateMainFormIfNotExists();

    CurOwnerItem := MainForm.MainMenu.Items;

    i := 0;
    while i <= Length(AlternativePath) do
    begin
      AlternativeName := '';
      Inc(i);
      while (i <= Length(AlternativePath)) and
            (AlternativePath[i] <> constSeparator) do
      begin
        AlternativeName := AlternativeName + AlternativePath[i];
        Inc(i);
      end;

      CurItem := ItemByName(CurOwnerItem, AlternativeName);
      // ���� ����� � ����� ������ ����, �� ������� ��� �������, ����� �������
      //�... ������� �������...
      if CurItem = nil then
      begin
        CurItem := TAlternative.Create(Self);
        CurItem.Caption := AlternativeName;
        CurOwnerItem.Add(CurItem);
      end;
      CurItem.SubscenarioName := SubscenarioName;
      CurItem.OnExecuteSubscenario := OnExecuteSubscenario;
      CurOwnerItem := CurItem;
    end;
  end;


  procedure TAlternativerKernel.Activate();
  begin
    CreateMainFormIfNotExists();
    MainForm.ShowModal();
    DestroyMainFormIfExists();
  end;


  procedure TAlternativerKernel.Stop();
  begin
    if MainForm <> nil then
    begin
      MainForm.CanCloseFlag := True;
      MainForm.Close();
    end;
  end;

end.
