unit DCK_Types;

interface
  type
    TOnExecuteSubscenario = procedure (SubScenarioName : String) of object;
    TOnStopAll = procedure () of object;

    TOnSetAttrValue = procedure(AttrName, AttrValue : String) of object;
    TOnSetAttrProp = procedure(AttrName, PropName, PropValue : String) of object;
    TOnGetAttrValue = procedure(AttrName : String; var AttrValue : String) of object;
    TOnGetAttrProp = procedure(AttrName, PropName : String; var PropValue : String) of object;

    TOnViewHelp = procedure(HelpFile : String) of object;
    TOnCancelStep = procedure() of object;

implementation

end.
