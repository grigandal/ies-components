unit DCK_TInputSubdefVariantItem;

interface
  uses
    Classes,
    StdCtrls,
    CheckLst,
    ComCtrls,
    DCK_TInputItem,
    DCK_TInputVariantItem;

  type
    TInputSubdefVariantItem = class(TInputVariantItem)
      private
        MinBeliefTrackBar : TTrackBar;
        MinBeliefLabel : TLabel;
        MinBeliefLowTickLabel : TLabel;
        MinBeliefMedTickLabel : TLabel;
        MinBeliefHiTickLabel : TLabel;

        MaxBeliefTrackBar : TTrackBar;
        MaxBeliefLabel : TLabel;

        procedure SynchronizeBeliefs(Sender : TObject);

      protected
        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;


implementation
  uses
    SysUtils,
    Controls,
    LR_Functions,
    DCK_Constants,
    DCK_Types,
    DCK_TDialogItem;


  procedure TInputSubdefVariantItem.SynchronizeBeliefs(Sender : TObject);
  begin
    if MinBeliefTrackBar.Position > MaxBeliefTrackBar.Position then
    begin
      if Sender = MinBeliefTrackBar then MaxBeliefTrackBar.Position := MinBeliefTrackBar.Position;

      if Sender = MaxBeliefTrackBar then MinBeliefTrackBar.Position := MaxBeliefTrackBar.Position;
    end;
  end;


  procedure TInputSubdefVariantItem.CreateContents();
  begin
    inherited CreateContents();

    MinBeliefTrackBar := TTrackBar.Create(Self);
    InsertControl(MinBeliefTrackBar);
    MinBeliefTrackBar.Min := constBeliefTrackBarMin;
    MinBeliefTrackBar.Max := constBeliefTrackBarMax;
    MinBeliefTrackBar.TickMarks := tmTopLeft;
    MinBeliefTrackBar.TickStyle := tsAuto;
    MinBeliefTrackBar.OnChange := SynchronizeBeliefs;

    MinBeliefLabel := TLabel.Create(Self);
    InsertControl(MinBeliefLabel);
    MinBeliefLabel.Caption := '����������� (min)';

    MinBeliefLowTickLabel := TLabel.Create(Self);
    InsertControl(MinBeliefLowTickLabel);
    MinBeliefLowTickLabel.Caption := '  0%';
    MinBeliefLowTickLabel.Transparent := True;
    MinBeliefLowTickLabel.Alignment := taLeftJustify;

    MinBeliefMedTickLabel := TLabel.Create(Self);
    InsertControl(MinBeliefMedTickLabel);
    MinBeliefMedTickLabel.Caption := ' 50%';
    MinBeliefMedTickLabel.Transparent := True;
    MinBeliefMedTickLabel.Alignment := taCenter;

    MinBeliefHiTickLabel := TLabel.Create(Self);
    InsertControl(MinBeliefHiTickLabel);
    MinBeliefHiTickLabel.Caption := '100%';
    MinBeliefHiTickLabel.Transparent := True;
    MinBeliefHiTickLabel.Alignment := taRightJustify;

    MaxBeliefTrackBar := TTrackBar.Create(Self);
    InsertControl(MaxBeliefTrackBar);
    MaxBeliefTrackBar.Min := constBeliefTrackBarMin;
    MaxBeliefTrackBar.Max := constBeliefTrackBarMax;
    MaxBeliefTrackBar.TickMarks := tmTopLeft;
    MaxBeliefTrackBar.TickStyle := tsAuto;
    MaxBeliefTrackBar.OnChange := SynchronizeBeliefs;

    MaxBeliefLabel := TLabel.Create(Self);
    InsertControl(MaxBeliefLabel);
    MaxBeliefLabel.Caption := '����������� (max)';
  end;


  procedure TInputSubdefVariantItem.AlignContents();
  begin
    inherited AlignContents();

    ClientHeight := ClientHeight + constBeliefTrackBarHeight * 2 + constSpaceHeight + constBeliefTickHeight;

    MinBeliefLabel.Left := 0;
    MinBeliefLabel.Top := RadioListBox.Top + RadioListBox.Height +
                       constSpaceHeight +
                       constBeliefTickHeight + constBeliefTrackBarHeight -
                       MinBeliefLabel.Height - 5;

    MaxBeliefLabel.Left := 0;
    MaxBeliefLabel.Top := MinBeliefLabel.Top + constBeliefTrackBarHeight;

    MinBeliefLowTickLabel.Top := RadioListBox.Top + RadioListBox.Height + constSpaceHeight;
    MinBeliefLowTickLabel.Left := MinBeliefLabel.Left + MinBeliefLabel.Width + constSpaceWidth;
    MinBeliefLowTickLabel.Width := ClientWidth - MinBeliefLowTickLabel.Left;
    MinBeliefLowTickLabel.Height := constBeliefTickHeight;

    MinBeliefMedTickLabel.Top := MinBeliefLowTickLabel.Top;
    MinBeliefMedTickLabel.Left := MinBeliefLowTickLabel.Left;
    MinBeliefMedTickLabel.Width := MinBeliefLowTickLabel.Width;
    MinBeliefMedTickLabel.Height := MinBeliefLowTickLabel.Height;

    MinBeliefHiTickLabel.Top := MinBeliefLowTickLabel.Top;
    MinBeliefHiTickLabel.Left := MinBeliefLowTickLabel.Left;
    MinBeliefHiTickLabel.Width := MinBeliefLowTickLabel.Width;
    MinBeliefHiTickLabel.Height := MinBeliefLowTickLabel.Height;

    MinBeliefTrackBar.Left := MinBeliefLabel.Left + MinBeliefLabel.Width + constSpaceWidth;
    MinBeliefTrackBar.Top := RadioListBox.Top + RadioListBox.Height + constSpaceHeight + constBeliefTickHeight;
    MinBeliefTrackBar.Width := ClientWidth - MinBeliefTrackBar.Left;
    MinBeliefTrackBar.Height := constBeliefTrackBarHeight;

    MaxBeliefTrackBar.Left := MinBeliefLabel.Left + MinBeliefLabel.Width + constSpaceWidth;
    MaxBeliefTrackBar.Top := MinBeliefTrackBar.Top + MinBeliefTrackBar.Height;
    MaxBeliefTrackBar.Width := ClientWidth - MaxBeliefTrackBar.Left;
    MaxBeliefTrackBar.Height := constBeliefTrackBarHeight;
  end;


  procedure TInputSubdefVariantItem.PrepareForInput();
  var
    PropValue : String;
  begin
    inherited PrepareForInput();

    OnGetAttrProp(AttrName, constMinBelief, PropValue);
    try
      MinBeliefTrackBar.Position := round(constBeliefTrackBarMin +
                                          (StrToInt(PropValue) - constBeliefMin) *
                                          ((constBeliefTrackBarMax - constBeliefTrackBarMin) /
                                           (constBeliefMax - constBeliefMin)));
    except
      MinBeliefTrackBar.Position := constBeliefTrackBarMax;
    end;

    OnGetAttrProp(AttrName, constMaxBelief, PropValue);
    try
      MaxBeliefTrackBar.Position := round(constBeliefTrackBarMin +
                                          (StrToInt(PropValue) - constBeliefMin) *
                                          ((constBeliefTrackBarMax - constBeliefTrackBarMin) /
                                           (constBeliefMax - constBeliefMin)));
    except
      MaxBeliefTrackBar.Position := constBeliefTrackBarMax;
    end;
  end;


  procedure TInputSubdefVariantItem.ConfirmInput();
  begin
    inherited ConfirmInput();

    OnSetAttrProp(AttrName, constMinBelief,
                  IntToStr(round(constBeliefMin +
                                 (MinBeliefTrackBar.Position  - constBeliefTrackBarMin) *
                                 ((constBeliefMax - constBeliefMin) /
                                  (constBeliefTrackBarMax - constBeliefTrackBarMin)))));

    OnSetAttrProp(AttrName, constMaxBelief,
                  IntToStr(round(constBeliefMin +
                                 (MaxBeliefTrackBar.Position  - constBeliefTrackBarMin) *
                                 ((constBeliefMax - constBeliefMin) /
                                  (constBeliefTrackBarMax - constBeliefTrackBarMin)))));
  end;

end.
