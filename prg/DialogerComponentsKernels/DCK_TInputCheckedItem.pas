unit DCK_TInputCheckedItem;

interface
  uses
    Classes,
    StdCtrls,
    DCK_TInputItem;

  type
    TInputCheckedItem = class(TInputItem)
      private
        CheckBox : TCheckBox;
        StaticText : TLabel;

        FCheckedValue : String;
        FUnCheckedValue : String;

      protected
        function GetPrompt() : String; override;
        procedure SetPrompt(Value : String); override;
        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        property CheckedValue : String read FCheckedValue write FCheckedValue;
        property UnCheckedValue : String read FUnCheckedValue write FUnCheckedValue;

        function CanClickOK() : Boolean; override;
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;


implementation

  uses
    Controls,
    SysUtils,
    DCK_Constants,
    DCK_Types,
    LR_Functions;


  function TInputCheckedItem.GetPrompt() : String;
  begin
    Result := StaticText.Caption;
  end;


  procedure TInputCheckedItem.SetPrompt(Value : String);
  begin
    StaticText.Caption := Value;
    AlignContents();
  end;


  procedure TInputCheckedItem.CreateContents();
  begin
    StaticText := TLabel.Create(Self);
    InsertControl(StaticText);
    StaticText.ParentFont := True;
    StaticText.WordWrap := True;

    CheckBox := TCheckBox.Create(Self);
    InsertControl(CheckBox);
    CheckBox.ParentFont := True;
    CheckBox.Ctl3D := constCtl3D;
  end;


  procedure TInputCheckedItem.AlignContents();
  begin
    CheckBox.Left := 0;
    CheckBox.Top := 0;
    CheckBox.Width := CheckBox.Height;

    StaticText.Left := CheckBox.Width + constSpaceWidth;
    StaticText.Width := ClientWidth - (CheckBox.Width + constSpaceWidth);
    StaticText.Top := 0;

    if StaticText.Height > CheckBox.Height
    then ClientHeight := StaticText.Height
    else ClientHeight := CheckBox.Height;
  end;


  function TInputCheckedItem.CanClickOK() : Boolean;
  begin
    Result := True;
  end;


  procedure TInputCheckedItem.PrepareForInput();
  var
    AttrValue : String;
  begin
    OnGetAttrValue(AttrName, AttrValue);
    CheckBox.Checked := AreStringsEqual(AttrValue, CheckedValue);
  end;


  procedure TInputCheckedItem.ConfirmInput();
  begin
    if CheckBox.Checked
    then OnSetAttrValue(AttrName, CheckedValue)
    else OnSetAttrValue(AttrName, UnCheckedValue);
    //�� ���������, CF=100%...
    OnSetAttrProp(AttrName, constBelief, IntToStr(constBeliefMax));
  end;

end.
