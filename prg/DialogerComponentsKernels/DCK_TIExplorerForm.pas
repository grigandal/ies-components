unit DCK_TIExplorerForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls, ComCtrls, ToolWin, Buttons;

type
  TIExplorerForm = class(TForm)
    Panel: TPanel;
    WebBrowser: TWebBrowser;
    ToolBar1: TToolBar;
    GoBackSpeedButton: TSpeedButton;
    GoForwardSpeedButton: TSpeedButton;
    procedure GoBackSpeedButtonClick(Sender: TObject);
    procedure GoForwardSpeedButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

  procedure TIExplorerForm.GoBackSpeedButtonClick(Sender: TObject);
  begin
    try WebBrowser.GoBack except end;
  end;

  
  procedure TIExplorerForm.GoForwardSpeedButtonClick(Sender: TObject);
  begin
    try WebBrowser.GoForward except end;
  end;

end.
