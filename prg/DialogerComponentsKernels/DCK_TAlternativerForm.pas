unit DCK_TAlternativerForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls;

type
  TAlternativerForm = class(TForm)
    MainMenu: TMainMenu;
    Image: TImage;
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FCanCloseFlag : Boolean;
//    Thread : TThread;
  public
    { Public declarations }
    property CanCloseFlag : Boolean read FCanCloseFlag write FCanCloseFlag;
  end;

  
implementation

{$R *.DFM}
  uses
    DCK_TAlternativerKernel;

  const
    constRefreshDelay = 1000;

  type
    TRefreshThread = class(TThread)
      protected
        Image : TImage;
        LastTime : LongInt;
        constructor Create(CreateSuspended: Boolean);
        procedure Execute(); override;
    end;


  constructor TRefreshThread.Create(CreateSuspended : Boolean);
  begin
    inherited Create(CreateSuspended);
    Image := nil;
    LastTime := 0;
  end;

   
  procedure TRefreshThread.Execute();
  var
    CurTime : LongInt;
  begin
    while not Terminated do
    begin
      CurTime := GetTickCount();
      if ((CurTime - LastTime) > constRefreshDelay) and (Image <> nil) then
      begin
        LastTime := CurTime;
        Image.Refresh();
      end;
    end;
  end;

   
  procedure TAlternativerForm.FormActivate(Sender: TObject);
  begin
    CanCloseFlag := False;
//    (Thread as TRefreshThread).Image := Image;
//    Thread.Resume();
  end;


  procedure TAlternativerForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    CanClose := CanCloseFlag;
    if not CanClose then (Owner as TAlternativerKernel).OnStopAll();
    //� ����� ������� �.�. ������� Stop
    CanClose := CanCloseFlag;
  end;


  procedure TAlternativerForm.FormCreate(Sender: TObject);
  begin
//    Thread := TRefreshThread.Create(True);
//    Thread.Priority := tpLower;
  end;

  
  procedure TAlternativerForm.FormDestroy(Sender: TObject);
  begin
//    Thread.Terminate();
  end;

end.
