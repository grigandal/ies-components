unit DCK_TAlternative;

interface
  uses
    Menus,
    DCK_Types;


  type
    TAlternative = class(TMenuItem)
      private
        FSubscenarioName : String;
        FOnExecuteSubscenario : TOnExecuteSubscenario;

      public
        property SubscenarioName : String read FSubscenarioName write FSubscenarioName;
        property OnExecuteSubscenario : TOnExecuteSubscenario read FOnExecuteSubscenario write FOnExecuteSubscenario;

        procedure Click();override;
    end;


implementation

  procedure TAlternative.Click();
  begin
    if Count = 0
    then OnExecuteSubscenario(SubscenarioName)
    else inherited Click();
  end;

end.
