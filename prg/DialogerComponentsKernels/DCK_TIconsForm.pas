unit DCK_TIconsForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type
  TIconsForm = class(TForm)
    AttentionImage: TImage;
    QuestionImage: TImage;
    InformationImage: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IconsForm: TIconsForm;

implementation

{$R *.DFM}

initialization
  IconsForm := TIconsForm.Create(nil);


finalization
  IconsForm.Destroy();

end.
