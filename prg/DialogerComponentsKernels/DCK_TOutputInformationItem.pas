unit DCK_TOutputInformationItem;

interface
  uses
    Classes,
    StdCtrls,
    ExtCtrls,
    DCK_TDialogItem;


  type
    TOutputInformationItem = class(TDialogItem)
      private
        StaticText : TLabel;
        Icon : TImage;
        procedure SetText(Value : String);
        function GetText() : String;

      protected
        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        property Text : String read GetText write SetText;
    end;


implementation
  uses
    Controls,
    DCK_Constants,
    DCK_Types,
    DCK_TIconsForm;


  procedure TOutputInformationItem.CreateContents();
  begin
    StaticText := TLabel.Create(Self);
    InsertControl(StaticText);
    StaticText.ParentFont := True;
    StaticText.WordWrap := True;

    Icon := TImage.Create(Self);
    InsertControl(Icon);
    Icon.Picture.Assign(IconsForm.InformationImage.Picture);
  end;


  procedure TOutputInformationItem.AlignContents();
  begin
    Icon.Left := 0;
    Icon.Top := 0;
    Icon.Width := constIconWidth;
    Icon.Height := constIconHeight;

    StaticText.Left := constIconWidth + constSpaceWidth;
    StaticText.Width := ClientWidth - constIconWidth - constSpaceWidth;
    if StaticText.Height > constIconHeight then
    begin
      ClientHeight := StaticText.Height;
      StaticText.Top := 0;
    end else
    begin
      ClientHeight := constIconHeight;
      StaticText.Top := (ClientHeight - StaticText.Height)  div 2;
    end;
  end;


  procedure TOutputInformationItem.SetText(Value : String);
  begin
    StaticText.Caption := Value;
    AlignContents();
  end;


  function TOutputInformationItem.GetText() : String;
  begin
    Result := StaticText.Caption;
  end;

end.
