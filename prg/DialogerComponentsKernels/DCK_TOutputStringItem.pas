unit DCK_TOutputStringItem;

interface
  uses
    Classes,
    StdCtrls,
    DCK_TDialogItem;

  type
    TOutputStringItem = class(TDialogItem)
      private
        StaticText : TLabel;
        procedure SetText(Value : String);
        function GetText() : String;
        procedure SetAlignment(Value : TAlignment);
        function GetAlignment() : TAlignment;

      protected
        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        property Text : String read GetText write SetText;
        property Alignment : TAlignment read GetAlignment write SetAlignment;
    end;


implementation
  uses
    Controls,
    DCK_Constants,
    DCK_Types;


  procedure TOutputStringItem.CreateContents();
  begin
    StaticText := TLabel.Create(Self);
    InsertControl(StaticText);
    StaticText.ParentFont := True;
    StaticText.WordWrap := True;
  end;


  procedure TOutputStringItem.AlignContents();
  begin
    StaticText.Width := ClientWidth;
    ClientHeight := StaticText.Height;
  end;

  
  procedure TOutputStringItem.SetText(Value : String);
  begin
    StaticText.Caption := Value;
    AlignContents();
  end;


  function TOutputStringItem.GetText() : String;
  begin
    Result := StaticText.Caption;
  end;


  procedure TOutputStringItem.SetAlignment(Value : TAlignment);
  begin
    StaticText.Alignment := Value;
    AlignContents();
  end;


  function TOutputStringItem.GetAlignment() : TAlignment;
  begin
    Result := StaticText.Alignment;
  end;

end.
