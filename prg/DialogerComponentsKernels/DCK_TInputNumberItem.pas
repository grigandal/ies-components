unit DCK_TInputNumberItem;

interface
  uses
    Classes,
    StdCtrls,
    DCK_TInputItem;

  type
    TInputNumberItem = class(TInputItem)
      private
        Edit : TEdit;
        PromptLabel : TLabel;

      protected
        function GetPrompt() : String; override;
        procedure SetPrompt(Value : String); override;

        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        function CanClickOK() : Boolean; override;
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;
    end;


implementation
  uses
    Controls,
    SysUtils,
    DCK_Constants,
    DCK_Types;


  function TInputNumberItem.GetPrompt() : String;
  begin
    Result := PromptLabel.Caption;
  end;


  procedure TInputNumberItem.SetPrompt(Value : String);
  begin
    PromptLabel.Caption := Value;
    AlignContents();
  end;


  procedure TInputNumberItem.CreateContents();
  begin
    Edit := TEdit.Create(Self);
    InsertControl(Edit);
    Edit.ParentFont := True;
    Edit.Ctl3D := constCtl3D;

    PromptLabel := TLabel.Create(Self);
    InsertControl(PromptLabel);
    PromptLabel.ParentFont := True;
    PromptLabel.FocusControl := Edit;
  end;


  procedure TInputNumberItem.AlignContents();
  begin
    ClientHeight := Edit.Height;

    PromptLabel.Left := 0;
    PromptLabel.Top := 2;

    Edit.Top := 0;
    if PromptLabel.Caption <> '' then
    begin
      Edit.Left := constSpaceWidth + PromptLabel.Width;
      Edit.Width := ClientWidth - (PromptLabel.Width + constSpaceWidth);
    end else
    begin
      Edit.Left := 0;
      Edit.Width := ClientWidth;
    end;
    Edit.Height := ClientHeight;
  end;


  function TInputNumberItem.CanClickOK() : Boolean;
  begin
  {
    try
      StrToFloat(Edit.Text);
      Result := True;
    except
      Result := False;
    end;
  }
    Result := True;
    if Edit.Text <> '' then
    try
      StrToFloat(Edit.Text);
    except
      Result := False;
    end;
  end;


  procedure TInputNumberItem.PrepareForInput();
  var
    AttrValue : String;
  begin
    OnGetAttrValue(AttrName, AttrValue);
    try
      StrToFloat(AttrValue);
      Edit.Text := AttrValue;
    except
      Edit.Text := '';
    end;
  end;


  procedure TInputNumberItem.ConfirmInput();
  begin
    OnSetAttrValue(AttrName, Edit.Text);
    //�� ���������, CF=100%...
    OnSetAttrProp(AttrName, constBelief, IntToStr(constBeliefMax));
  end;

end.
