unit DCK_TDialogFormKernel;

interface
  uses
    Menus,
    Classes,
    DCK_Types,
    DCK_TAlternative,
    DCK_TDialogForm;

  type
    TDialogFormKernel = class(TComponent)
      private
        DialogFormList : TList;

        FOnSetAttrValue : TOnSetAttrValue;
        FOnSetAttrProp : TOnSetAttrProp;
        FOnGetAttrValue : TOnGetAttrValue;
        FOnGetAttrProp : TOnGetAttrProp;

        FOnViewHelp : TOnViewHelp;
        FOnCancelStep : TOnCancelStep;
        FOnExecuteSubscenario : TOnExecuteSubscenario;

        procedure SetCaption(Value : String);
        function GetCaption() : String;

        procedure SetHelpFile(Value : String);
        function GetHelpFile() : String;

      protected
        function ItemByName(OwnerItem : TMenuItem; ItemName : String) : TAlternative;

        procedure CreateDialogForm();
        procedure FreeDialogForm();
        procedure ShowDialogForm();
        procedure CloseDialogForms();
        function CurDialogForm() : TDialogForm;

      public
        property Caption : String read GetCaption write SetCaption;
        property HelpFile : String read GetHelpFile write SetHelpFile;

        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;

        procedure AddInputStringItem(Prompt, AttrName : String);
        procedure AddInputNumberItem(Prompt, AttrName : String);
        procedure AddInputInexactNumberItem(Prompt, AttrName : String);
        procedure AddInputCheckedItem(Prompt, AttrName, CheckedValue, UncheckedValue : String);
        procedure AddInputVariantItem(Prompt, AttrName, AvailableValues : String);
        procedure AddInputIndefVariantItem(Prompt, AttrName, AvailableValues : String);
        procedure AddInputSubdefVariantItem(Prompt, AttrName, AvailableValues : String);
        procedure AddOutputStringItem(Text : String; Alignment : TAlignment);
        procedure AddOutputInformationItem(Text : String);
        procedure AddOutputAttentionItem(Text : String);
        procedure AddOutputQuestionItem(Text : String);
        procedure AddAlternative(AlternativePath, SubscenarioName : String);
        procedure Activate();
        procedure Stop();

      published
        property OnSetAttrValue : TOnSetAttrValue read FOnSetAttrValue write FOnSetAttrValue;
        property OnSetAttrProp : TOnSetAttrProp read FOnSetAttrProp write FOnSetAttrProp;
        property OnGetAttrValue : TOnGetAttrValue read FOnGetAttrValue write FOnGetAttrValue;
        property OnGetAttrProp : TOnGetAttrProp read FOnGetAttrProp write FOnGetAttrProp;

        property OnViewHelp : TOnViewHelp read FOnViewHelp write FOnViewHelp;
        property OnCancelStep : TOnCancelStep read FOnCancelStep write FOnCancelStep;
        property OnExecuteSubscenario : TOnExecuteSubscenario read FOnExecuteSubscenario write FOnExecuteSubscenario;
    end;


implementation
  uses
    DCK_Constants,
    DCK_TInputStringItem,
    DCK_TInputNumberItem,
    DCK_TInputInexactNumberItem,
    DCK_TInputCheckedItem,
    DCK_TInputVariantItem,
    DCK_TInputIndefVariantItem,
    DCK_TInputSubdefVariantItem,
    DCK_TOutputStringItem,
    DCK_TOutputAttentionItem,
    DCK_TOutputInformationItem,
    DCK_TOutputQuestionItem, Controls;


  constructor TDialogFormKernel.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    DialogFormList := TList.Create();
    CreateDialogForm();
  end;


  destructor TDialogFormKernel.Destroy();
  begin
    FreeDialogForm();
    DialogFormList.Destroy();
    inherited Destroy();
  end;


  procedure TDialogFormKernel.SetCaption(Value : String);
  begin
    CurDialogForm.Caption := Value;
  end;


  function TDialogFormKernel.GetCaption() : String;
  begin
    Result := CurDialogForm.Caption;
  end;


  procedure TDialogFormKernel.SetHelpFile(Value : String);
  begin
    CurDialogForm.HelpFile := Value;
  end;


  function TDialogFormKernel.GetHelpFile() : String;
  begin
    Result := CurDialogForm.HelpFile;
  end;


  procedure TDialogFormKernel.CreateDialogForm();
  var
    form: TDialogForm;
  begin
    form := TDialogForm.Create(Self);
    form.Width := 500;
    DialogFormList.Add(form);
  end;


  procedure TDialogFormKernel.FreeDialogForm();
  begin
    TDialogForm(DialogFormList.Items[DialogFormList.Count - 1]).Free();
    DialogFormList.Delete(DialogFormList.Count - 1);
  end;


  procedure TDialogFormKernel.ShowDialogForm();
  begin
    CreateDialogForm();
    TDialogForm(DialogFormList.Items[DialogFormList.Count - 2]).ShowModal();

    TDialogForm(DialogFormList.Items[DialogFormList.Count - 2]).Free();
    DialogFormList.Delete(DialogFormList.Count - 2);
  end;


  procedure TDialogFormKernel.CloseDialogForms();
  var
    i : Integer;
  begin
    for i := 0 to DialogFormList.Count - 1 do
      TDialogForm(DialogFormList.Items[i]).Close();
  end;


  function TDialogFormKernel.CurDialogForm() : TDialogForm;
  begin
    Result := TDialogForm(DialogFormList.Last);
  end;


  function TDialogFormKernel.ItemByName(OwnerItem : TMenuItem; ItemName : String) : TAlternative;
  var
    i : Integer;
    Found : Boolean;
  begin
    i := 0;
    Found := False;

    while (i < OwnerItem.Count) and not Found do
    begin
      Found := OwnerItem.Items[i].Caption = ItemName;
      Inc(i);
    end;

    if Found
    then Result := OwnerItem.Items[i - 1] as TAlternative
    else Result := nil;
  end;


  procedure TDialogFormKernel.AddInputStringItem(Prompt, AttrName : String);
  var
    InputStringItem : TInputStringItem;
  begin
    InputStringItem := TInputStringItem.Create(CurDialogForm);

    InputStringItem.Prompt := Prompt;
    InputStringItem.AttrName := AttrName;
  end;


  procedure TDialogFormKernel.AddInputNumberItem(Prompt, AttrName : String);
  var
    InputNumberItem : TInputNumberItem;
  begin
    InputNumberItem := TInputNumberItem.Create(CurDialogForm);

    InputNumberItem.Prompt := Prompt;
    InputNumberItem.AttrName := AttrName;
  end;


  procedure TDialogFormKernel.AddInputInexactNumberItem(Prompt, AttrName : String);
  var
    InputInexactNumberItem : TInputInexactNumberItem;
  begin
    InputInexactNumberItem := TInputInexactNumberItem.Create(CurDialogForm);

    InputInexactNumberItem.Prompt := Prompt;
    InputInexactNumberItem.AttrName := AttrName;
  end;


  procedure TDialogFormKernel.AddInputCheckedItem(Prompt,
                                             AttrName,
                                             CheckedValue,
                                             UncheckedValue : String);
  var
    InputCheckedItem : TInputCheckedItem;
  begin
    InputCheckedItem := TInputCheckedItem.Create(CurDialogForm);
    InputCheckedItem.Prompt := Prompt;
    InputCheckedItem.AttrName := AttrName;
    InputCheckedItem.CheckedValue := CheckedValue;
    InputCheckedItem.UnCheckedValue := UnCheckedValue;
  end;


  procedure TDialogFormKernel.AddInputVariantItem(Prompt, AttrName, AvailableValues : String);
  var
    InputVariantItem : TInputVariantItem;
  begin
    InputVariantItem := TInputVariantItem.Create(CurDialogForm);

    InputVariantItem.Prompt := Prompt;
    InputVariantItem.AttrName := AttrName;
    InputVariantItem.AvailableValues := AvailableValues;
  end;


  procedure TDialogFormKernel.AddInputIndefVariantItem(Prompt, AttrName, AvailableValues : String);
  var
    InputIndefVariantItem : TInputIndefVariantItem;
  begin
    InputIndefVariantItem := TInputIndefVariantItem.Create(CurDialogForm);
    InputIndefVariantItem.Prompt := Prompt;
    InputIndefVariantItem.AttrName := AttrName;
    InputIndefVariantItem.AvailableValues := AvailableValues;
  end;


  procedure TDialogFormKernel.AddInputSubdefVariantItem(Prompt, AttrName, AvailableValues : String);
  var
    InputSubdefVariantItem : TInputSubdefVariantItem;
  begin
    InputSubdefVariantItem := TInputSubdefVariantItem.Create(CurDialogForm);
    InputSubdefVariantItem.Prompt := Prompt;
    InputSubdefVariantItem.AttrName := AttrName;
    InputSubdefVariantItem.AvailableValues := AvailableValues;
  end;


  procedure TDialogFormKernel.AddOutputStringItem(Text : String; Alignment : TAlignment);
  var
    OutputStringItem : TOutputStringItem;
  begin
    OutputStringItem := TOutputStringItem.Create(CurDialogForm);
    OutputStringItem.Text := Text;
    OutputStringItem.Alignment := Alignment;
  end;


  procedure TDialogFormKernel.AddOutputInformationItem(Text : String);
  var
    InformationItem : TOutputInformationItem;
  begin
    InformationItem := TOutputInformationItem.Create(CurDialogForm);
    InformationItem.Text := Text;
  end;


  procedure TDialogFormKernel.AddOutputAttentionItem(Text : String);
  var
    AttentionItem : TOutputAttentionItem;
  begin
    AttentionItem := TOutputAttentionItem.Create(CurDialogForm);
    AttentionItem.Text := Text;
  end;


  procedure TDialogFormKernel.AddOutputQuestionItem(Text : String);
  var
    QuestionItem : TOutputQuestionItem;
  begin
    QuestionItem := TOutputQuestionItem.Create(CurDialogForm);
    QuestionItem.Text := Text;
  end;


  procedure TDialogFormKernel.AddAlternative(AlternativePath, SubscenarioName : String);
  var
    i : Integer;
    AlternativeName : String;
    CurOwnerItem : TMenuItem;
    CurItem : TAlternative;
  begin
    CurOwnerItem := CurDialogForm.PopupMenu.Items;

    i := 0;
    while i <= Length(AlternativePath) do
    begin
      AlternativeName := '';
      Inc(i);
      while (i <= Length(AlternativePath)) and
            (AlternativePath[i] <> constSeparator) do
      begin
        AlternativeName := AlternativeName + AlternativePath[i];
        Inc(i);
      end;

      CurItem := ItemByName(CurOwnerItem, AlternativeName);
      // ���� ����� � ����� ������ ����, �� ������� ��� �������, ����� �������
      //�... ������� �������...
      if CurItem = nil then
      begin
        CurItem := TAlternative.Create(Self);
        CurItem.Caption := AlternativeName;
        CurOwnerItem.Add(CurItem);
      end;
      CurItem.SubscenarioName := SubscenarioName;
      CurItem.OnExecuteSubscenario := OnExecuteSubscenario;
      CurOwnerItem := CurItem;
    end;
  end;


  procedure TDialogFormKernel.Activate();
  begin
    ShowDialogForm();
  end;


  procedure TDialogFormKernel.Stop();
  begin
    CloseDialogForms();
  end;

end.
