unit DCK_TInputVariantItem;

interface
  uses
    Classes,
    StdCtrls,
    CheckLst,
    DCK_TInputItem;

  type
    TInputVariantItem = class(TInputItem)
      private
        procedure CheckListBoxClick(Sender: TObject);

      protected
        PromptLabel : TLabel;
        RadioListBox : TCheckListBox;
        
        function GetPrompt() : String; override;
        procedure SetPrompt(Value : String); override;
        function GetAvailableValues() : String;
        procedure SetAvailableValues(Value : String);

        procedure CreateContents(); override;
        procedure AlignContents(); override;

      public
        function CanClickOK() : Boolean; override;
        procedure PrepareForInput(); override;
        procedure ConfirmInput(); override;

      published
        property AvailableValues : String read GetAvailableValues write SetAvailableValues;
    end;

implementation
  uses
    Controls,
    SysUtils,
    LR_Functions,
    DCK_Constants,
    DCK_Types;


  procedure TInputVariantItem.CreateContents();
  begin
    RadioListBox := TCheckListBox.Create(Self);
    InsertControl(RadioListBox);
    RadioListBox.Ctl3D := constCtl3D;
    RadioListBox.ParentFont := True;
    RadioListBox.OnClick := CheckListBoxClick;
    RadioListBox.OnClickCheck := CheckListBoxClick;

    PromptLabel := TLabel.Create(Self);
    InsertControl(PromptLabel);
    PromptLabel.ParentFont := True;
    PromptLabel.FocusControl := RadioListBox;
  end;


  procedure TInputVariantItem.AlignContents();
  var
    H : Integer;
  begin
    PromptLabel.Left := 0;
    PromptLabel.Top := 0;

    RadioListBox.Left := 0;
    RadioListBox.Width := ClientWidth;

    RadioListBox.Top := PromptLabel.Height;
    H := RadioListBox.Items.Count * RadioListBox.ItemHeight;
    if H > constRadioListBoxMaxHeight then H := constRadioListBoxMaxHeight;
    if RadioListBox.Ctl3D then H := H + 4 else H := H + 2;
    RadioListBox.Height := H;

    if PromptLabel.Caption <> '' then
    begin
      RadioListBox.Top := PromptLabel.Height;
      ClientHeight := PromptLabel.Height + RadioListBox.Height;
    end else
    begin
      RadioListBox.Top := 0;
      ClientHeight := RadioListBox.Height;
    end;
  end;


  procedure TInputVariantItem.CheckListBoxClick(Sender: TObject);
  var
    i : Integer;
  begin
    for i := 0 to (Sender as TCheckListBox).Items.Count - 1
    do (Sender as TCheckListBox).Checked[i] :=
         ((Sender as TCheckListBox).ItemIndex = i) and
         not (Sender as TCheckListBox).Checked[(Sender as TCheckListBox).ItemIndex];
  end;


  function TInputVariantItem.GetPrompt() : String;
  begin
    Result := PromptLabel.Caption;
  end;


  procedure TInputVariantItem.SetPrompt(Value : String);
  begin
    PromptLabel.Caption := Value;
    AlignContents()
  end;

  
  function TInputVariantItem.GetAvailableValues() : String;
  begin
    Result := RadioListBox.Items.Text;
  end;


  procedure TInputVariantItem.SetAvailableValues(Value : String);
  begin
    RadioListBox.Items.Text := Value;
    AlignContents();
  end;


  function TInputVariantItem.CanClickOK() : Boolean;
  {
  var
    i : Integer;
  }
  begin
    Result := True;
    {
    Result := False;
    for i := 0 to RadioListBox.Items.Count - 1 do Result := Result or RadioListBox.Checked[i];
    }
  end;


  procedure TInputVariantItem.PrepareForInput();
  var
    i : Integer;
    AttrValue : String;
  begin
    OnGetAttrValue(AttrName, AttrValue);

    for i := 0 to RadioListBox.Items.Count - 1 do RadioListBox.Checked[i] := False;
    i := 0;
    while (i < RadioListBox.Items.Count) and not AreStringsEqual(RadioListBox.Items[i], AttrValue)
    do Inc(i);
    if i < RadioListBox.Items.Count then RadioListBox.Checked[i] := True;
  end;


  procedure TInputVariantItem.ConfirmInput();
  var
    i : Integer;
    AttrValue : String;
  begin
    AttrValue := '';
    i := 0;
    while (AttrValue = '') and (i < RadioListBox.Items.Count) do
    begin
      if RadioListBox.Checked[i] then AttrValue := RadioListBox.Items[i];
      Inc(i);
    end;

    OnSetAttrValue(AttrName, AttrValue);
    //�� ���������, CF=100%...
    OnSetAttrProp(AttrName, constBelief, IntToStr(constBeliefMax));
  end;

end.
