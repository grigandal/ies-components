unit DCK_TReporterKernel;

interface
  uses
    Classes,
    DCK_Types,
    DCK_TReporterForm;


  type
    TReporterKernel = class(TComponent)
      private
        Form : TReporterForm;
        FCaption : String;
      protected
        procedure CreateFormIfNotExists();
        procedure DestroyFormIfExists();
      public
        property Caption : String read FCaption write FCaption;

        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;

        procedure Clear();
        procedure AddText(Text : String);
        procedure Activate();
        procedure Stop();
    end;


implementation


  constructor TReporterKernel.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Form := nil;
  end;


  destructor TReporterKernel.Destroy();
  begin
    DestroyFormIfExists();
    inherited Destroy();
  end;


  procedure TReporterKernel.CreateFormIfNotExists();
  begin
    if Form = nil then Form := TReporterForm.Create(Self);
  end;


  procedure TReporterKernel.DestroyFormIfExists();
  begin
    if Form <> nil then Form.Destroy();
    Form := nil;
  end;


  procedure TReporterKernel.Clear();
  begin
    CreateFormIfNotExists();
    Form.ReportMemo.Clear();
  end;


  procedure TReporterKernel.AddText(Text : String);
  begin
    CreateFormIfNotExists();
    if Form.ReportMemo.Lines.Text <> ''
    then Form.ReportMemo.Lines.Text := Form.ReportMemo.Lines.Text + #13 + #10;
    Form.ReportMemo.Lines.Text := Form.ReportMemo.Lines.Text + Text;
  end;


  procedure TReporterKernel.Activate();
  begin
    CreateFormIfNotExists();
    if Caption <> '' then Form.Caption := Caption;
    Form.ShowModal();
    Caption := '';
    DestroyFormIfExists();
  end;


  procedure TReporterKernel.Stop();
  begin
    if Form <> nil then Form.Close();
  end;
  
end.
