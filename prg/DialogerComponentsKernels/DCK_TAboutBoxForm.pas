unit DCK_TAboutBoxForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons;

type
  TAboutBoxForm = class(TForm)
    OKSpeedButton: TSpeedButton;
    ImagePanel: TPanel;
    Image: TImage;
    ProgramNameStaticText: TStaticText;
    AboutTextMemo: TMemo;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OKSpeedButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

  procedure TAboutBoxForm.FormKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
  begin
    if (Key = 13) or (Key = 27) then CLose();
  end;


  procedure TAboutBoxForm.OKSpeedButtonClick(Sender: TObject);
  begin
    Close();
  end;

end.
