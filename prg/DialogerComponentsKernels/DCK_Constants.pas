unit DCK_Constants;

interface
  const
    constSeparator = '/';

    constCtl3D =  True;

    constSpaceWidth = 8;
    constSpaceHeight = 8;

    constInputStringHeight = 22;
    constCheckBoxHeight = 22;

    constRadioListBoxMaxHeight = 100;

    constBeliefTrackBarHeight = 30;
    constBeliefTrackBarMin = 0;
    constBeliefTrackBarMax = 10;
    constBeliefTickHeight = 12;

    constButtonHeight = 20;
    constButtonWidth = 50;
    constButtonsItemHeight = 40;

    constIconWidth = 32;
    constIconHeight = 32;

    constAccuracyEditWidth = 50;

    constBelief = 'Belief';
    constMinBelief = 'Belief';
    constMaxBelief = 'MaxBelief';
    constBeliefMin = 0;
    constBeliefMax = 100;

    constAccuracy = 'Accuracy';
    constAccuracyMin = 0;
    constAccuracyMax = 100;

    constTruePropValue = 'T';
    constFalsePropValue = 'F';

    constAskedProp = 'Asked';

implementation

end.
