library BrokerLib;

uses
  ComServ,
  BrokerLib_TLB in 'BrokerLib_TLB.pas',
  BL_TBroker in 'BL_TBroker.pas' {Broker: CoClass},
  BL_Functions in 'BL_Functions.pas',
  BL_TObjectItem in 'BL_TObjectItem.pas',
  BL_TErrorsBox in 'BL_TErrorsBox.pas' {ErrorsBox};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
