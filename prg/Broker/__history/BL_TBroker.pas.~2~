unit BL_TBroker;

interface

  uses
    ComObj,
    ActiveX, Sysutils, Dialogs, Forms,
    BrokerLib_TLB,
    Classes,
    BL_TObjectItem, StdVcl;

  type

    TBroker = class(TAutoObject, IBroker)
      private
        Objects : TCollection;
        BlackBoard : OleVariant;
        Path: WideString;

        procedure GetObject(Name : WideString; var Obj : OleVariant; var Found : Boolean);

      protected
        function Get_BlackBoard () : OleVariant; safecall;
        procedure Set_BlackBoard(Value : OleVariant); safecall;

        procedure RegisterObject(const ObjName, ClassName : WideString); safecall;
        procedure UnregisterAll(); safecall;

        procedure ConfigurateObject(const ObjName, Config : WideString); safecall;

        procedure SendMessage(const SndrName, RcvrName, MsgText : WideString; out Output: OleVariant); safecall;
        procedure SendMessageToAll(const SndrName, MsgText : WideString); safecall;

        procedure StopObject(const ObjName: WideString); safecall;
        procedure StopAll(); safecall;

        procedure ProcessError(const ErrText : WideString); safecall;
    function Get_Path: WideString; safecall;
    procedure Set_Path(const Value: WideString); safecall;
        { Protected declarations }

      public
        procedure Initialize(); override;
        destructor Destroy(); override;
    end;


implementation

  uses
    ComServ,
    Variants,
    BL_Functions,
    BL_TErrorsBox;


  procedure TBroker.Initialize();
  begin
    inherited Initialize();

    Objects := TCollection.Create(TObjectItem);
  end;


  destructor TBroker.Destroy();
  begin
    inherited Destroy();
  end;


  function TBroker.Get_BlackBoard: OleVariant;
  begin
    Result := BlackBoard;
  end;


  procedure TBroker.Set_BlackBoard(Value: OleVariant);
  begin
    BlackBoard := Value;
  end;


  procedure TBroker.GetObject(Name : WideString; var Obj : OleVariant; var Found : Boolean);
  var
    i : Integer;
  begin
    i := 0;
    while (i < Objects.Count) and
          not Equals((Objects.Items[i] as TObjectItem).ObjRef.Name, Name) do Inc(i);
    if i < Objects.Count then
    begin
      Obj := (Objects.Items[i] as TObjectItem).ObjRef;
      Found := True;
    end
    else Found := False;
  end;


  procedure TBroker.RegisterObject(const ObjName, ClassName : WideString);
  var
    Obj : OleVariant;
    ObjItem : TObjectItem;
    Found : Boolean;
  begin
    GetObject(ObjName, Obj, Found);
    if not Found then
    begin
      try
        Application.ProcessMessages;
        Obj := CreateOleObject(ClassName);
        Obj.Name := ObjName;
        Obj.Broker := Self as IDispatch;

        ObjItem := Objects.Add() as TObjectItem;
        ObjItem.ObjRef := Obj;
      except
        on E: Exception do ShowMessage('������ �������� ���������� ' + ClassName + #10#13 + e.Message);
      end
    end;
  end;


  procedure TBroker.UnregisterAll();
  var
    i : Integer;
  begin
    for i := 0 to Objects.Count - 1 do
      (Objects.Items[i] as TObjectItem).ObjRef := Unassigned;
  end;


  procedure TBroker.ConfigurateObject(const ObjName, Config: WideString);
  var
    Obj : OleVariant;
    Found : Boolean;
  begin
    GetObject(ObjName, Obj, Found);
    if Found then
      try
        Obj.Configurate(Config);
      except
        on E: Exception do ShowMessage('������ ������������ ���������� ' + ObjName + #10#13 + e.Message);
      end
  end;


  procedure TBroker.SendMessage(const SndrName, RcvrName, MsgText : WideString; out Output: OleVariant);
  var
    Obj : OleVariant;
    Found : Boolean;
  begin
    GetObject(RcvrName, Obj, Found);
    if Found then
      if not Equals(SndrName, Obj.Name) then
        try
          Obj.ProcessMessage(SndrName, MsgText, Output);
        except
          on E: Exception do ShowMessage('������ ��������� ��������� �������� ' + RcvrName + #10#13 + e.Message);
        end;
  end;


  procedure TBroker.SendMessageToAll(const SndrName, MsgText: WideString);
  var
    i : Integer;
    obj, TempOutput : OleVariant;
  begin
    for i := 0 to Objects.Count - 1 do begin
      obj := (Objects.Items[i] as TObjectItem).ObjRef;
      if not Equals(SndrName, obj.Name)
      then
        try
          obj.ProcessMessage(SndrName, MsgText, TempOutput);
        except
          on E: Exception do ShowMessage('������ ��������� ��������� �������� ' + obj.Name + #10#13 + e.Message);
        end;
    end;
  end;


  procedure TBroker.StopObject(const ObjName: WideString);
  var
    Obj : OleVariant;
    Found : Boolean;
  begin
    GetObject(ObjName, Obj, Found);
    if Found then Obj.Stop;
  end;


  procedure TBroker.StopAll();
  var
    i : Integer;
  begin
    for i := 0 to Objects.Count - 1 do
      (Objects.Items[i] as TObjectItem).ObjRef.Stop;
  end;


  procedure TBroker.ProcessError(const ErrText : WideString);
  var
    ErrBox : TErrorsBox;
  begin
    ErrBox := TErrorsBox.Create(nil);
    ErrBox.ErrTextMemo.Lines.Text := ErrText;
    ErrBox.ShowModal();
    ErrBox.Free();

    StopAll();
  end;


function TBroker.Get_Path: WideString;
begin
  Result := Path;
end;

procedure TBroker.Set_Path(const Value: WideString);
begin
  Path := Value;
end;

initialization

  TAutoObjectFactory.Create(ComServer, TBroker, Class_Broker, ciMultiInstance, tmApartment);

end.
