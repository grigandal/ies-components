unit BL_TObjectItem;

interface
  uses
    Classes;

  type
    TObjectItem = class(TCollectionItem)
      private
//        FObjName : WideString;
        FObjRef : OleVariant;
      public
//        property ObjName : WideString read FObjName write FObjName;
        property ObjRef : OleVariant read FObjRef write FObjRef;
    end;

    
implementation

end.
