unit BL_Functions;

interface

  function GUIDByStr(Str : String) : TGUID;
  function Equals(Str1, Str2 : String) : Boolean;

  
implementation
  uses
    SysUtils;

  function HexByChar(Chr : Char) : Cardinal;
  begin
    case Chr of
      '0' : Result := 0;
      '1' : Result := 1;
      '2' : Result := 2;
      '3' : Result := 3;
      '4' : Result := 4;
      '5' : Result := 5;
      '6' : Result := 6;
      '7' : Result := 7;
      '8' : Result := 8;
      '9' : Result := 9;
      'A' : Result := 10;
      'a' : Result := 10;
      'B' : Result := 11;
      'b' : Result := 11;
      'C' : Result := 12;
      'c' : Result := 12;
      'D' : Result := 13;
      'd' : Result := 13;
      'E' : Result := 14;
      'e' : Result := 14;
      'F' : Result := 15;
      'f' : Result := 16;
    else
      raise Exception.Create('������ "' + Chr + '" �� �������� 16-������ ������');
    end;
  end;


  function GUIDByStr(Str : String) : TGUID;
  var
    i : Integer;
  begin
    if (Length(Str) = 38) and (Str[1] = '{') and (Str[38] = '}') and
       (Str[10] = '-') and (Str[15] = '-') and (Str[20] = '-') and (Str[25] = '-') then
    begin
      Result.D1 := 0;
      for i := 2 to 9 do Result.D1 := Result.D1 * 16 + HexByChar(Str[i]);

      Result.D2 := 0;
      for i := 11 to 14 do Result.D2 := Result.D2 * 16 + HexByChar(Str[i]);

      Result.D3 := 0;
      for i := 16 to 19 do Result.D3 := Result.D3 * 16 + HexByChar(Str[i]);

      Result.D4[0] := 0;
      for i := 21 to 22 do Result.D4[0] := Result.D4[0] * 16 + HexByChar(Str[i]);

      Result.D4[1] := 0;
      for i := 23 to 24 do Result.D4[1] := Result.D4[1] * 16 + HexByChar(Str[i]);

      Result.D4[2] := 0;
      for i := 26 to 27 do Result.D4[2] := Result.D4[2] * 16 + HexByChar(Str[i]);

      Result.D4[3] := 0;
      for i := 28 to 29 do Result.D4[3] := Result.D4[3] * 16 + HexByChar(Str[i]);

      Result.D4[4] := 0;
      for i := 30 to 31 do Result.D4[4] := Result.D4[4] * 16 + HexByChar(Str[i]);

      Result.D4[5] := 0;
      for i := 32 to 33 do Result.D4[5] := Result.D4[5] * 16 + HexByChar(Str[i]);

      Result.D4[6] := 0;
      for i := 34 to 35 do Result.D4[6] := Result.D4[6] * 16 + HexByChar(Str[i]);

      Result.D4[7] := 0;
      for i := 36 to 37 do Result.D4[7] := Result.D4[7] * 16 + HexByChar(Str[i]);
    end
    else raise Exception.Create('������ �� �������� GUID');
  end;


  function UppercaseString(Str : String) : String;
  var
    i : Integer;
  begin
    Result := '';
    for i := 1 to Length(Str) do
      case Str[i] of
        'a'..'z' : Result := Result + UpperCase(Str[i]);
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        else Result := Result + Str[i];
      end;
  end;


  function Equals(Str1, Str2 : String) : Boolean;
  begin
    Result := UppercaseString(Str1) = UppercaseString(Str2);
  end;

end.
