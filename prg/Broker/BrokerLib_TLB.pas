unit BrokerLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 04.11.2007 14:30:44 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\dd\Delphi\prg\Broker\BrokerLib.tlb (1)
// LIBID: {3B54D020-212C-11D6-A03E-EFADC184B341}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\system32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  BrokerLibMajorVersion = 1;
  BrokerLibMinorVersion = 0;

  LIBID_BrokerLib: TGUID = '{3B54D020-212C-11D6-A03E-EFADC184B341}';

  IID_IBroker: TGUID = '{3B54D021-212C-11D6-A03E-EFADC184B341}';
  CLASS_Broker: TGUID = '{3B54D023-212C-11D6-A03E-EFADC184B341}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IBroker = interface;
  IBrokerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Broker = IBroker;


// *********************************************************************//
// Interface: IBroker
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3B54D021-212C-11D6-A03E-EFADC184B341}
// *********************************************************************//
  IBroker = interface(IDispatch)
    ['{3B54D021-212C-11D6-A03E-EFADC184B341}']
    function  Get_BlackBoard: OleVariant; safecall;
    procedure Set_BlackBoard(Value: OleVariant); safecall;
    procedure RegisterObject(const ObjName: WideString; const ClassName: WideString); safecall;
    procedure UnregisterAll; safecall;
    procedure ConfigurateObject(const ObjName: WideString; const Config: WideString); safecall;
    procedure SendMessage(const SndrName: WideString; const RcvrName: WideString; 
                          const MsgText: WideString; out Output: OleVariant); safecall;
    procedure SendMessageToAll(const SndrName: WideString; const MsgText: WideString); safecall;
    procedure StopObject(const ObjName: WideString); safecall;
    procedure StopAll; safecall;
    procedure ProcessError(const ErrText: WideString); safecall;
    function  Get_Path: WideString; safecall;
    procedure Set_Path(const Value: WideString); safecall;
    property BlackBoard: OleVariant read Get_BlackBoard write Set_BlackBoard;
    property Path: WideString read Get_Path write Set_Path;
  end;

// *********************************************************************//
// DispIntf:  IBrokerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3B54D021-212C-11D6-A03E-EFADC184B341}
// *********************************************************************//
  IBrokerDisp = dispinterface
    ['{3B54D021-212C-11D6-A03E-EFADC184B341}']
    property BlackBoard: OleVariant dispid 1;
    procedure RegisterObject(const ObjName: WideString; const ClassName: WideString); dispid 2;
    procedure UnregisterAll; dispid 3;
    procedure ConfigurateObject(const ObjName: WideString; const Config: WideString); dispid 4;
    procedure SendMessage(const SndrName: WideString; const RcvrName: WideString; 
                          const MsgText: WideString; out Output: OleVariant); dispid 5;
    procedure SendMessageToAll(const SndrName: WideString; const MsgText: WideString); dispid 6;
    procedure StopObject(const ObjName: WideString); dispid 7;
    procedure StopAll; dispid 8;
    procedure ProcessError(const ErrText: WideString); dispid 9;
    property Path: WideString dispid 10;
  end;

// *********************************************************************//
// The Class CoBroker provides a Create and CreateRemote method to          
// create instances of the default interface IBroker exposed by              
// the CoClass Broker. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoBroker = class
    class function Create: IBroker;
    class function CreateRemote(const MachineName: string): IBroker;
  end;

implementation

uses ComObj;

class function CoBroker.Create: IBroker;
begin
  Result := CreateComObject(CLASS_Broker) as IBroker;
end;

class function CoBroker.CreateRemote(const MachineName: string): IBroker;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Broker) as IBroker;
end;

end.


