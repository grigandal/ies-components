unit BL_TErrorsBox;

interface

  uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Buttons;

  type
    TErrorsBox = class(TForm)
        ErrTextMemo: TMemo;
        ExitSpeedButton: TSpeedButton;
        procedure ExitSpeedButtonClick(Sender: TObject);
        procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

      private
      { Private declarations }

      public
      { Public declarations }
    end;


implementation

{$R *.dfm}


  procedure TErrorsBox.ExitSpeedButtonClick(Sender: TObject);
  begin
    Close();
  end;

  procedure TErrorsBox.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  begin
    if (Key = 27) then ExitSpeedButtonClick(Self);
  end;

end.
