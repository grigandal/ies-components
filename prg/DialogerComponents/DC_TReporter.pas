unit DC_TReporter;

interface
  uses
    Classes,
    DC_TInteractor,
    DC_TManager,
    DCK_TReporterKernel;


  type
    TReporter = class(TInteractor)
      private
        Kernel : TReporterKernel;
      public
        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

        procedure ProcessMessage(MessageText, SenderName : String); override;
        procedure Stop(); override;
    end;


  procedure Register();


implementation
  uses
    LR_Functions,
    DC_Constants;


  constructor TReporter.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Kernel := TReporterKernel.Create(Self);
  end;


  destructor TReporter.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TReporter.ProcessMessage(MessageText, SenderName : String);
  var
    Id, Val : String;
  begin
    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constActivateKeyWord);
    if not Manager.WasWarning() then Kernel.Activate();

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constAddKeyWord);
      if not Manager.WasWarning() then
      begin
        LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning()
        then Kernel.AddText(Val)
        else Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constSetKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
        Id := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constToKeyWord);
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then
        begin
          if AreStringsEqual(Id, constCaptionId) then Kernel.Caption := Val;
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TReporter.Stop();
  begin
    Kernel.Stop();
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TReporter]);
  end;

end.
