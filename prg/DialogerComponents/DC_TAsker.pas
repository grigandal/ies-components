unit DC_TAsker;

interface
  uses
    Classes,
    DC_TInteractor,
    DC_TManager,
    DCK_TDialogFormKernel;


  type
    TAsker = class(TInteractor)
      private
        Kernel : TDialogFormKernel;

        procedure ViewHelp(HelpFile : String);
        procedure CancelStep();
        procedure ExecuteSubscenario(SubScenarioName : String);

      protected
        procedure SetManager(AManager : TManager); override;

      public
        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

        procedure ProcessMessage(MessageText, SenderName : String); override;
        procedure Stop(); override;
    end;


  procedure Register();



implementation
  uses
    LR_Functions,
    LR_Constants,
    DC_Constants;


  constructor TAsker.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Kernel := TDialogFormKernel.Create(Self);
  end;


  destructor TAsker.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TAsker.SetManager(AManager : TManager);
  begin
    inherited SetManager(AManager);

    Kernel.OnSetAttrValue := AManager.OnSetAttrValue;
    Kernel.OnSetAttrProp := AManager.OnSetAttrProp;
    Kernel.OnGetAttrValue := AManager.OnGetAttrValue;
    Kernel.OnGetAttrProp := AManager.OnGetAttrProp;

    Kernel.OnViewHelp := ViewHelp;
    Kernel.OnCancelStep := CancelStep;
    Kernel.OnExecuteSubscenario := ExecuteSubscenario;
  end;


  procedure TAsker.ProcessMessage(MessageText, SenderName : String);
  var
    Prompt, AttrName, AvailableValues, CheckedValue, UncheckedValue : String;
    AlternativeName, ScenarioName : String;
    Id, Val : String;
    Align : TAlignment;
  begin
    Align := taLeftJustify;
    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constActivateKeyWord);
    if not Manager.WasWarning() then Kernel.Activate();

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constSetKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
        Id := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constToKeyWord);
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then
        begin
          if AreStringsEqual(Id, constCaptionId) then Kernel.Caption := Val;
          if AreStringsEqual(Id, constHelpFileId) then Kernel.HelpFile := Val;
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constInputKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Prompt := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constToKeyWord);

        if not Manager.WasWarning() then LexemsReader.ReadName();
        AttrName := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constAsKeyWord);

        if not Manager.WasWarning() then
        begin
          LexemsReader.ReadKeyWordIfItIs(constStringKeyWord);
          if not Manager.WasWarning()
          then Kernel.AddInputStringItem(Prompt, AttrName);

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constNumberKeyWord);
            if not Manager.WasWarning()
            then Kernel.AddInputNumberItem(Prompt, AttrName);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constInexactNumberKeyWord);
            if not Manager.WasWarning()
            then Kernel.AddInputInexactNumberItem(Prompt, AttrName);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constVariantKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constFromKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadValue();
            AvailableValues := LexemsReader.ReadingResult();
            if not Manager.WasWarning()
            then Kernel.AddInputVariantItem(Prompt, AttrName, AvailableValues);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constIndefVariantKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constFromKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadValue();
            AvailableValues := LexemsReader.ReadingResult();
            if not Manager.WasWarning()
            then Kernel.AddInputIndefVariantItem(Prompt, AttrName, AvailableValues);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constSubdefVariantKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constFromKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadValue();
            AvailableValues := LexemsReader.ReadingResult();
            if not Manager.WasWarning()
            then Kernel.AddInputSubdefVariantItem(Prompt, AttrName, AvailableValues);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constCheckedKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constFromKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadValue();
            CheckedValue := LexemsReader.ReadingResult();
            if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constOrKeyWord);
            if not Manager.WasWarning() then LexemsReader.ReadValue();
            UnCheckedValue := LexemsReader.ReadingResult();
            if not Manager.WasWarning()
            then Kernel.AddInputCheckedItem(Prompt, AttrName, CheckedValue, UnCheckedValue);
          end;
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constOutputKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constAsKeyWord);

        if not Manager.WasWarning() then
        begin
          LexemsReader.ReadKeyWordIfItIs(constStringKeyWord);
          if not Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constOnKeyWord);
            if not Manager.WasWarning() then
            begin
              LexemsReader.ReadKeyWordIfItIs(constLeftKeyWord);
              if not Manager.WasWarning()  then Align := taLeftJustify;

              if Manager.WasWarning() then
              begin
                LexemsReader.ReadKeyWordIfItIs(constRightKeyWord);
                if not Manager.WasWarning() then Align := taRightJustify;
              end;

              if Manager.WasWarning() then
              begin
                LexemsReader.ReadKeyWordIfItIs(constCenterKeyWord);
                if not Manager.WasWarning() then Align := taCenter;
              end;
            end;
            if not Manager.WasWarning() then Kernel.AddOutputStringItem(Val, Align);
            if Manager.WasWarning() then Manager.ProcessError();
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constAttentionKeyWord);
            if not Manager.WasWarning()
            then Kernel.AddOutputAttentionItem(Val);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constQuestionKeyWord);
            if not Manager.WasWarning()
            then Kernel.AddOutputQuestionItem(Val);
          end;

          if Manager.WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(constInformationKeyWord);
            if not Manager.WasWarning()
            then Kernel.AddOutputInformationItem(Val);
          end;

          if Manager.WasWarning() then Manager.ProcessError();
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constOnKeyWord);
      if not Manager.WasWarning() then
      begin
        LexemsReader.ReadValue();
        AlternativeName := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constExecuteKeyWord);

        if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
        ScenarioName := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then Kernel.AddAlternative(AlternativeName, ScenarioName);

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;
    
    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TAsker.Stop();
  begin
    Kernel.Stop();
  end;


  procedure TAsker.ViewHelp(HelpFile : String);
  begin
    Manager.SendMessageTo(constViewKeyWord + ' ' + constStringMarker + HelpFile + constStringMarker,
                          constAskerName, constHelperName);
  end;


  procedure TAsker.CancelStep();
  begin
    Manager.SendMessageTo(constBreakKeyWord, constAskerName, constInterpreterName);
    Manager.StopExtSender();
  end;


  procedure TAsker.ExecuteSubscenario(SubScenarioName : String);
  begin
    Manager.SendMessageTo(constExecuteKeyWord + ' ' + SubScenarioName,
                          constAlternativerName, constInterpreterName);
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TAsker]);
  end;

end.
