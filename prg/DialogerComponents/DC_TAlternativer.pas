unit DC_TAlternativer;

interface
  uses
    Classes,
    LR_TLexemsReader,
    DC_TInteractor,
    DC_TManager,
    DCK_TAlternativerKernel;

  type
    TAlternativer = class(TInteractor)
      private
        Kernel : TAlternativerKernel;
      protected
        procedure SetManager(AManager : TManager); override;
      public
        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;
        procedure ProcessMessage(MessageText, SenderName : String); override;
        procedure ExecuteSubscenario(SubScenarioName : String);
        procedure Stop(); override;
      published
    end;


  procedure Register();


implementation
  uses
    LR_Functions,
    DC_Constants;


  constructor TAlternativer.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Kernel := TAlternativerKernel.Create(Self);
  end;


  destructor TAlternativer.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TAlternativer.SetManager(AManager : TManager);
  begin
    inherited SetManager(AManager);
    Kernel.OnExecuteSubscenario :=ExecuteSubscenario;
    Kernel.OnStopAll := Manager.StopAll;
  end;


  procedure TAlternativer.ProcessMessage(MessageText, SenderName : String);
  var
    AlternativeName, ScenarioName : String;
    Id, Val : String;
  begin
    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constOnKeyWord);
    if not Manager.WasWarning() then
    begin
      LexemsReader.ReadValue();
      AlternativeName := LexemsReader.ReadingResult();

      if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constExecuteKeyWord);

      if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
      ScenarioName := LexemsReader.ReadingResult();

      if not Manager.WasWarning() then Kernel.AddAlternative(AlternativeName, ScenarioName);

      if Manager.WasWarning() then Manager.ProcessError();
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constActivateKeyWord);
      if not Manager.WasWarning() then Kernel.Activate();
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constSetKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
        Id := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constToKeyWord);
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then
        begin
          if AreStringsEqual(Id, constCaptionId) then Kernel.Caption := Val;
          if AreStringsEqual(Id, constPictureFileId) then Kernel.PictureFile := Val;
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TAlternativer.Stop();
  begin
    Kernel.Stop();
  end;


  procedure TAlternativer.ExecuteSubscenario(SubScenarioName : String);
  begin
    Manager.SendMessageTo(constExecuteKeyWord + ' ' + SubScenarioName,
                          constAlternativerName, constInterpreterName);
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TAlternativer]);
  end;

end.
