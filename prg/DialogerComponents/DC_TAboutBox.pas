unit DC_TAboutBox;

interface
  uses
    Classes,
    LR_TLexemsReader,
    DC_TInteractor,
    DC_TManager,
    DCK_TAboutBoxKernel;

  type
    TAboutBox = class(TInteractor)
      private
        Kernel : TAboutBoxKernel;

      public
        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;
        procedure ProcessMessage(MessageText, SenderName : String); override;
        procedure Stop(); override;

      published
    end;

  procedure Register();


implementation
  uses
    LR_Functions,
    DC_Constants;


  constructor TAboutBox.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Kernel := TAboutBoxKernel.Create(Self);
  end;


  destructor TAboutBox.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TAboutBox.ProcessMessage(MessageText, SenderName : String);
  var
    Id, Val : String;
  begin
    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constActivateKeyWord);
    if not Manager.WasWarning() then
    begin
      if not Manager.WasWarning() then Kernel.Activate();
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constSetKeyWord);
      if not Manager.WasWarning() then
      begin
        if not Manager.WasWarning() then LexemsReader.ReadIdentifier();
        Id := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then LexemsReader.ReadKeyWordIfItIs(constToKeyWord);
        if not Manager.WasWarning() then LexemsReader.ReadValue();
        Val := LexemsReader.ReadingResult();

        if not Manager.WasWarning() then
        begin
          if AreStringsEqual(Id, constCaptionId) then Kernel.Caption := Val;
          if AreStringsEqual(Id, constPictureFileId) then Kernel.PictureFile := Val;
          if AreStringsEqual(Id, constProgramNameId) then Kernel.ProgramName := Val;
          if AreStringsEqual(Id, constAboutTextId) then Kernel.AboutText := Val;
        end;

        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TAboutBox.Stop();
  begin
    Kernel.Stop();
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TAboutBox]);
  end;

end.
