unit DC_Types;

interface
  type
    TOnError = procedure (Description : String) of object;
    TOnSendMessage = procedure (SenderName,
                                ReceiverName,
                                MessageText : String) of object;
    TOnSetFact = procedure (AttrName,
                            AttrValue,
                            AttrProperties : String) of object;

    TOnSetAttrValue = procedure(AttrName, AttrValue : String) of object;
    TOnSetAttrProp = procedure(AttrName, PropName, PropValue : String) of object;
    TOnGetAttrValue = procedure(AttrName : String; var AttrValue : String) of object;
    TOnGetAttrProp = procedure(AttrName, PropName : String; var PropValue : String) of object;

    TOnGetFact = procedure (AttrName : String;
                            var AttrValue,
                            AttrProperties : String) of object;
    TOnStopAll = procedure () of object;
    TOnStopObject = procedure (ObjName : String) of object;
    TOnStopExtSender = procedure of object;


implementation

end.
