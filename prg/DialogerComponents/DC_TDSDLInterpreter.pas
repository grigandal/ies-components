unit DC_TDSDLInterpreter;

interface
  uses
    Classes, SysUtils, Forms, Dialogs,
    DC_TInteractor,
    DC_TManager,
    DSDLIK_TDSDLInterpreterKernel;

  type
    TDSDLInterpreter = class(TInteractor)
      private
        Kernel : TDSDLInterpreterKernel;
        FFileName : String;
        FDSDLText : String;
        procedure SetFileName(Value : String);
        procedure SetDSDLText(Value : String);

      protected
        procedure SetManager(Value : TManager);override;

      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;

        procedure Run();
        procedure Test(TestedDSDLText : String; var Description : String);
        procedure GetFact(AttrName : WideString);
        procedure SetFact(AttrName : WideString);

        procedure ProcessMessage(MessageText, SenderName : String);override;
        procedure Stop();override;

        procedure SendMessageTo(MessageText, ReceiverName : String);
        procedure SendMessageToAll(MessageText : String);

        procedure ExecuteScenario(ScenarioName : String);
        procedure SendNamedMessage(MessageName : String);

      published
        property FileName : String read FFileName write SetFileName;
        property DSDLText : String read FDSDLText write SetDSDLText;
    end;


  procedure Register();


implementation
  uses
    DC_Constants;


  constructor TDSDLInterpreter.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);

    Kernel := TDSDLInterpreterKernel.Create(Self);
  end;


  destructor TDSDLInterpreter.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TDSDLInterpreter.SetManager(Value : TManager);
  begin
    inherited SetManager(Value);
    Kernel.OnErrorWithText := Manager.ProcessError;

    Kernel.OnSendMessage := SendMessageTo;
    Kernel.OnSendMessageToAll := SendMessageToAll;
    Kernel.OnStopAll := Manager.StopAll;
    Kernel.OnSetAttrValue := Manager.OnSetAttrValue;
    Kernel.OnGetAttrValue := Manager.OnGetAttrValue;
    Kernel.OnSetAttrProp := Manager.OnSetAttrProp;
    Kernel.OnGetAttrProp := Manager.OnGetAttrProp;
  end;


  procedure TDSDLInterpreter.Run();
  begin
    Kernel.Run();
  end;


  procedure TDSDLInterpreter.Test(TestedDSDLText : String; var Description : String);
  begin
    Kernel.Test(TestedDSDLText, Description);
  end;


  procedure TDSDLInterpreter.GetFact(AttrName : WideString);
  var
    Found : Boolean;
  begin
    Found := False;
    Kernel.SendMessageInputAttribute(AttrName, Found);
    if not Found then Kernel.SendMessageAboutAttribute(AttrName, Found);
  end;


  procedure TDSDLInterpreter.SetFact(AttrName : WideString);
  var
    Found : Boolean;
  begin
    Found := False;
    Kernel.SendMessageOutputAttribute(AttrName, Found);
    if not Found then Kernel.SendMessageAboutAttribute(AttrName, Found);
  end;


  procedure TDSDLInterpreter.SendMessageTo(MessageText, ReceiverName : String);
  begin
    Manager.SendMessageTo(MessageText, constInterpreterName, ReceiverName);
  end;


  procedure TDSDLInterpreter.SendMessageToAll(MessageText : String);
  begin
    Manager.SendMessageTo(MessageText, constInterpreterName, constAll);
  end;


  procedure TDSDLInterpreter.ExecuteScenario(ScenarioName : String);
  begin
    Kernel.ExecuteScenario(ScenarioName);
  end;


  procedure TDSDLInterpreter.SendNamedMessage(MessageName : String);
  begin
    Kernel.SendNamedMessage(MessageName, '');
  end;


  procedure TDSDLInterpreter.ProcessMessage(MessageText, SenderName : String);
  begin
    Kernel.Answer := '';

    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constRunKeyWord);
    if not Manager.WasWarning() then
    begin
      Kernel.DSDLText := DSDLText;
      Kernel.Run();
    end;

    if Manager.WasWarning() then
    begin
      Manager.ClearWarnings();
      LexemsReader.ReadKeyWordIfItIs(constTakeKeyWord);
      if not Manager.WasWarning() then
      begin
        LexemsReader.ReadValue();
        Kernel.Answer := LexemsReader.ReadingResult();
        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      Manager.ClearWarnings();
      Kernel.Execute(MessageText);
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TDSDLInterpreter.Stop();
  begin
    Kernel.Stop();
  end;


  procedure TDSDLInterpreter.SetDSDLText(Value : String);
  begin
    FDSDLText := Value;
    Kernel.DSDLText := Value;
  end;


  procedure TDSDLInterpreter.SetFileName(Value : String);
  var
    InpLine : String;
    InpFile : TextFile;
    FirstLine : Boolean;
  begin
    FFileName := Value;
    if Pos(':', FFileName) = 0 then // ���� ����� ���, �� �������
      FFileName := ExtractFilePath(Application.ExeName) + FFileName;
    FDSDLText := '';
    FirstLine := True;
    try
      AssignFile(InpFile, FFileName);
      Reset(InpFile);
      while not Eof(InpFile) do
      begin
        System.Readln(InpFile, InpLine);
        if not FirstLine then FDSDLText := FDSDLText + #13 + #10;
        FDSDLText := FDSDLText + InpLine;
        FirstLine := False;
      end;
      Close(InpFile);
      Kernel.DSDLText := FDSDLText;
    except
      FFileName := '';
      ShowMessage(constFileReadingError);
    end;
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TDSDLInterpreter]);
  end;

end.
