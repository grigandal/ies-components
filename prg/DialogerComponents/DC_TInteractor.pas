unit DC_TInteractor;

interface
  uses
    Classes,
    LR_TLexemsReader,
    DC_TManager;

  type
    TInteractor = class(TComponent)
      private
        FManager : TManager;

      protected
        LexemsReader : TLexemsReader;
        procedure SetManager(AManager : TManager); virtual;

      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy(); override;

        procedure ProcessMessage(MessageText, SenderName : String); virtual; abstract;
        function CanStop() : Boolean; virtual;
        procedure Stop(); virtual;

      published
        property Manager : TManager read FManager write SetManager;
    end;


implementation
  uses
    DC_Constants;

    
  constructor TInteractor.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);

    LexemsReader := TLexemsReader.Create(Self);
    LexemsReader.SetParameters(constKeyWords, []);
  end;


  destructor TInteractor.Destroy();
  begin
    LexemsReader.Destroy();
    inherited Destroy();
  end;

  
  procedure TInteractor.SetManager(AManager : TManager);
  begin
    FManager := AManager;

    LexemsReader.OnClearWarnings := Manager.ClearWarnings;
    LexemsReader.OnWarning := Manager.AddWarning;
    LexemsReader.OnError := Manager.ProcessError;
    LexemsReader.OnCheckWarning := Manager.CheckWarning;
    LexemsReader.OnCheckError := Manager.CheckError;
  end;


  procedure TInteractor.Stop();
  begin
  end;


  function TInteractor.CanStop() : Boolean;
  begin
    Result := True;
  end;

end.
