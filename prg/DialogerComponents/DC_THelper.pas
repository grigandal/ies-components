unit DC_THelper;

interface
  uses
    Classes,
    DC_TInteractor,
    DCK_TIExplorerForm;

  type
    THelper = class(TInteractor)
      private
        IExplorerForm : TIExplorerForm;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure ProcessMessage(MessageText, SenderName : String);override;
        procedure Stop(); override;
    end;


  procedure Register();


implementation
  uses
    Windows,
    DC_Constants;


  constructor THelper.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    IExplorerForm := TIExplorerForm.Create(Self);
    IExplorerForm.Caption := '�������';
  end;


  destructor THelper.Destroy();
  begin
    IExplorerForm.Destroy();
    inherited Destroy();
  end;


  procedure THelper.ProcessMessage(MessageText, SenderName : String);
  var
    HelpFile : String;
  begin
    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constViewKeyWord);
    if not Manager.WasWarning() then
    begin
      LexemsReader.ReadValue();
      HelpFile := LexemsReader.ReadingResult();
      if not Manager.WasWarning() then
      begin
        IExplorerForm.WebBrowser.Navigate(HelpFile);
        IExplorerForm.Show();
      end;
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure THelper.Stop();
  begin
    IExplorerForm.Close();
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [THelper]);
  end;

end.
