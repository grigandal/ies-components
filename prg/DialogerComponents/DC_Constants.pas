unit DC_Constants;

interface
  const
    constInterpreterName = 'Interpreter';
    constAskerName = 'Asker';
    constHelperName = 'Helper';
    constAlternativerName = 'Alternativer';

    constAll = 'all';

    constRunKeyWord = 'run';
    constBreakKeyWord = 'break';
    constSolveKeyWord = 'solve';
    constSetKeyWord = 'set';
    constToKeyWord = 'to';
    constGetKeyWord = 'get';
    constTakeKeyWord = 'take';
    constOnKeyWord = 'on';
    constExecuteKeyWord = 'execute';
    constActivateKeyWord = 'activate';
    constInputKeyWord = 'input';
    constAsKeyWord = 'as';
    constStringKeyWord = 'string';
    constNumberKeyWord = 'number';
    constInexactNumberKeyWord = 'InexactNumber';
    constCheckedKeyWord = 'checked';
    constFromKeyWord = 'from';
    constOrKeyWord = 'or';
    constOneKeyWord = 'one';
    constVariantKeyWord = 'Variant';
    constIndefVariantKeyWord = 'IndefVariant';
    constSubdefVariantKeyWord = 'SubdefVariant';
    constOfKeyWord = 'of';
    constOutputKeyWord = 'output';
    constLeftKeyWord = 'left';
    constRightKeyWord = 'right';
    constCenterKeyWord = 'center';
    constAddKeyWord = 'add';
    constInformationKeyWord = 'information';
    constAttentionKeyWord = 'attention';
    constQuestionKeyWord = 'question';
    constViewKeyWord = 'view';

    constKeyWords : array[1..32] of String = (constRunKeyWord,
                                              constBreakKeyWord,
                                              constSolveKeyWord,
                                              constSetKeyWord,
                                              constToKeyWord,
                                              constGetKeyWord,
                                              constTakeKeyWord,
                                              constOnKeyWord,
                                              constExecuteKeyWord,
                                              constActivateKeyWord,
                                              constInputKeyWord,
                                              constAsKeyWord,
                                              constStringKeyWord,
                                              constNumberKeyWord,
                                              constInexactNumberKeyWord,
                                              constCheckedKeyWord,
                                              constFromKeyWord,
                                              constOrKeyWord,
                                              constOneKeyWord,
                                              constOfKeyWord,
                                              constVariantKeyWord,
                                              constIndefVariantKeyWord,
                                              constSubdefVariantKeyWord,
                                              constOutputKeyWord,
                                              constLeftKeyWord,
                                              constRightKeyWord,
                                              constCenterKeyWord,
                                              constAddKeyWord,
                                              constInformationKeyWord,
                                              constAttentionKeyWord,
                                              constQuestionKeyWord,
                                              constViewKeyWord);

    constCaptionId = 'Caption';
    constHelpFileId = 'HelpFile';
    constPictureFileId = 'PictureFile';
    constProgramNameId = 'ProgramName';
    constAboutTextId = 'AboutText';

    constFileReadingError = '������ ������ �����';
    constKBNotLoaded = '���� ������ �� ���������';

    constComponent = '���������';
    constMessage = '���������';
    constNotFound = '�� ������';

implementation

end.
