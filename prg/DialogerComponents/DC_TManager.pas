unit DC_TManager;

interface
  uses
    Classes,
    Dialogs,
    DC_Types;

  type
    TManager = class(TComponent)
      private
        FDescription : String;
        FWasWarning : Boolean;
        FWasError : Boolean;
        Stopped : Boolean;

        FOnError : TOnError;
        FOnSendMessage : TOnSendMessage;

        FOnSetAttrValue : TOnSetAttrValue;
        FOnSetAttrProp : TOnSetAttrProp;
        FOnGetAttrValue : TOnGetAttrValue;
        FOnGetAttrProp : TOnGetAttrProp;

        FOnStopAll : TOnStopAll;
        FOnStopObject : TOnStopObject;
        FOnStopExtSender : TOnStopExtSender;

      protected

      public
        constructor Create(AOwner : TComponent);override;

        procedure SendMessageTo(MessageText, SenderName, ReceiverName : String);

        procedure Stop();
        procedure StopObject(ObjName : String);
        procedure StopExtSender;
        procedure StopAll();

        procedure ClearWarnings();
        procedure AddWarning(Description : String);
        procedure ProcessError(); overload;
        procedure ProcessError(Description : String); overload;
        procedure CheckWarning(var WasWarning : Boolean);
        procedure CheckError(var WasError : Boolean);
        function WasWarning() : Boolean;
        function WasError() : Boolean;

      published
        property OnError : TOnError read FOnError write FOnError;
        property OnSendMessage : TOnSendMessage read FOnSendMessage write FOnSendMessage;

        property OnSetAttrValue : TOnSetAttrValue read FOnSetAttrValue write FOnSetAttrValue;
        property OnSetAttrProp : TOnSetAttrProp read FOnSetAttrProp write FOnSetAttrProp;
        property OnGetAttrValue : TOnGetAttrValue read FOnGetAttrValue write FOnGetAttrValue;
        property OnGetAttrProp : TOnGetAttrProp read FOnGetAttrProp write FOnGetAttrProp;

        property OnStopAll : TOnStopAll read FOnStopAll write FOnStopAll;
        property OnStopObject : TOnStopObject read FOnStopObject write FOnStopObject;
        property OnStopExtSender : TOnStopExtSender read FOnStopExtSender write FOnStopExtSender;
    end;

    procedure Register();

implementation
  uses
    SysUtils,
    LR_Functions,
    DC_Constants,
    DC_TInteractor;


  constructor TManager.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    FOnError := nil;
    FOnSendMessage := nil;
    FOnStopAll := nil;
    ClearWarnings();
  end;


  procedure TManager.SendMessageTo(MessageText, SenderName, ReceiverName : String);
  var
    i : Integer;
    Sended : Boolean;
    IsMessageToAll : Boolean;
  begin
    Stopped := False;
    Sended := False;
    IsMessageToAll := AreStringsEqual(ReceiverName, constAll);
    i := 0;
    while (i < Owner.ComponentCount) and
          not Sended and not WasError() and not Stopped do
    begin
      if Owner.Components[i] is TInteractor then
      begin
       if IsMessageToAll
       then (Owner.Components[i] as TInteractor).ProcessMessage(MessageText, SenderName)

       else if AreStringsEqual(ReceiverName, Owner.Components[i].Name) then
       begin
         (Owner.Components[i] as TInteractor).ProcessMessage(MessageText, SenderName);
         Sended := True;
       end;
      end;
      Inc(i);
    end;

    if not Sended and not WasError() and not Stopped then
    begin
      if Assigned(OnSendMessage)
      then OnSendMessage(constInterpreterName, ReceiverName, MessageText)
      else ProcessError('���������� ������� OnSendMessage �� ���������');
    end;
  end;


  procedure TManager.Stop();
  var
    i : Integer;
  begin
    Stopped := True;
    for i := 0 to Owner.ComponentCount - 1 do
      if Owner.Components[i] is TInteractor
      then (Owner.Components[i] as TInteractor).Stop();
  end;


  procedure TManager.StopObject(ObjName : String);
  var
    i : Integer;
    Done : Boolean;
  begin
    i := 0;
    Done := False;
    while (i < Owner.ComponentCount) and not Done do
    begin
      if (Owner.Components[i] is TInteractor) and
         AreStringsEqual(ObjName, Owner.Components[i].Name) then
      begin
        (Owner.Components[i] as TInteractor).Stop;
        Done := True;
      end;
      Inc(i);
    end;
    if not Done then
    begin
      if Assigned(OnStopObject)
      then OnStopObject(ObjName)
      else ProcessError('���������� ������� OnStopObject �� ���������');
    end;
  end;


  procedure TManager.StopExtSender();
  begin
    if Assigned(OnStopExtSender)
    then OnStopExtSender()
    else ProcessError('���������� ������� OnStopExtSender �� ���������');
  end;


  procedure TManager.StopAll();
  begin
//    Stop();
    if Assigned(OnStopAll)
    then OnStopAll()
    else ProcessError('���������� ������� OnStopAll �� ���������');
  end;


  procedure TManager.ClearWarnings();
  begin
    FDescription := '';
    FWasWarning := False;
    FWasError := False;
  end;


  procedure TManager.AddWarning(Description : String);
  begin
    if FDescription = ''
    then FDescription := Description
    else FDescription := FDescription + #13 + #10 + Description;
    FWasWarning := True;
    FWasError := False;
  end;


  procedure TManager.ProcessError();
  begin
    if FWasWarning then
    begin
      FWasError := True;
      FWasWarning := False;

      Stop();

      if Assigned(OnError)
      then OnError(FDescription)
      else ShowMessage('���������� ������� OnError �� ���������');
    end;
  end;


  procedure TManager.ProcessError(Description : String);
  begin
    FWasError := True;
    FWasWarning := False;
    FDescription := Description;

    Stop();

    if Assigned(OnError)
    then OnError(FDescription)
    else ShowMessage('���������� ������� OnError �� ���������');
  end;


  procedure TManager.CheckWarning(var WasWarning : Boolean);
  begin
    WasWarning := FWasWarning;
  end;


  procedure TManager.CheckError(var WasError : Boolean);
  begin
    WasError := FWasError;
  end;


  function TManager.WasWarning() : Boolean;
  begin
    Result := FWasWarning;
  end;


  function TManager.WasError() : Boolean;
  begin
    Result := FWasError;
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TManager]);
  end;

end.
