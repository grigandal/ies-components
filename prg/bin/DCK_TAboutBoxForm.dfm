object AboutBoxForm: TAboutBoxForm
  Left = 208
  Top = 140
  BorderStyle = bsToolWindow
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
  ClientHeight = 281
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object OKSpeedButton: TSpeedButton
    Left = 223
    Top = 249
    Width = 81
    Height = 25
    Caption = 'OK'
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      555555555555555555555555555555555555555555F55555555555555A055555
      55555555588F555555555555AAA05555555555558888F55555555555AAA05555
      555555558888F5555555555AAAAA05555555555888888F55555555AAAAAA0555
      5555558888888F5555555AAA05AAA05555555888858888F55555AA05555AA055
      55558885555888F555555555555AAA05555555555558888F555555555555AA05
      555555555555888F5555555555555AA05555555555555888F5555555555555AA
      05555555555555888F5555555555555AA05555555555555888FF555555555555
      5AA0555555555555588855555555555555555555555555555555}
    NumGlyphs = 2
    OnClick = OKSpeedButtonClick
  end
  object ImagePanel: TPanel
    Left = 8
    Top = 8
    Width = 113
    Height = 113
    BevelOuter = bvLowered
    TabOrder = 0
    object Image: TImage
      Left = 1
      Top = 1
      Width = 111
      Height = 111
      Align = alClient
      Stretch = True
    end
  end
  object ProgramNameStaticText: TStaticText
    Left = 128
    Top = 16
    Width = 177
    Height = 97
    Alignment = taCenter
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object AboutTextMemo: TMemo
    Left = 8
    Top = 128
    Width = 297
    Height = 113
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 2
  end
end
