library DialogerLib;

uses
  ComServ,
  DL_TKernelForm in 'DL_TKernelForm.pas' {KernelForm},
  DialogerLib_TLB in 'DialogerLib_TLB.pas',
  DL_TDialoger in 'DL_TDialoger.pas' {Dialoger: CoClass},
  DL_Constants in 'DL_Constants.pas',
  DL_Functions in 'DL_Functions.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
