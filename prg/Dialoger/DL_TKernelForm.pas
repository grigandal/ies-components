unit DL_TKernelForm;

interface

  uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    DC_TReporter, DC_TInformer, DC_TAsker, DC_TAlternativer,
    DC_TInteractor, DC_TDSDLInterpreter, DC_TManager, ComObj, DC_THelper,
    DC_TAboutBox;

  type
    TKernelForm = class(TForm)
      Manager: TManager;
      Alternativer: TAlternativer;
      Asker: TAsker;
    Informer: TInformer;
      Reporter: TReporter;
      Interpreter: TDSDLInterpreter;
      Helper: THelper;
    OpenDialog: TOpenDialog;
    AboutBox: TAboutBox;

      procedure ManagerSendMessage(SenderName, ReceiverName, MessageText: String);
      procedure ManagerStopAll;
      procedure ManagerError(Description: String);

      procedure ManagerSetAttrValue(AttrName, AttrValue: String);
      procedure ManagerSetAttrProp(AttrName, PropName, PropValue: String);
      procedure ManagerGetAttrProp(AttrName, PropName: String; var PropValue: String);
      procedure ManagerGetAttrValue(AttrName: String; var AttrValue: String);
      procedure ManagerStopObject(ObjName: String);
      procedure ManagerStopExtSender;

    private

    public
      Dialoger : TAutoObject;
    end;


implementation

{$R *.DFM}

  uses
    DCK_Constants,
    DL_TDialoger,
    DL_Constants,
    DL_Functions;


  procedure TKernelForm.ManagerSendMessage(SenderName, ReceiverName, MessageText : String);
  var
    Output : OleVariant;
  begin
    (Dialoger as TDialoger).Broker.SendMessage((Dialoger as TDialoger).Name, ReceiverName, MessageText, Output);
  end;


  procedure TKernelForm.ManagerStopAll;
  begin
    (Dialoger as TDialoger).Broker.StopAll;
  end;


  procedure TKernelForm.ManagerStopObject(ObjName: String);
  begin
    (Dialoger as TDialoger).Broker.StopObject(ObjName);
  end;


  procedure TKernelForm.ManagerStopExtSender;
  begin
    if (Dialoger as TDialoger).ExtSndrName <> ''
    then (Dialoger as TDialoger).Broker.StopObject((Dialoger as TDialoger).ExtSndrName);
  end;


  procedure TKernelForm.ManagerError(Description : String);
  begin
    (Dialoger as TDialoger).Broker.ProcessError(Description);
  end;


  procedure TKernelForm.ManagerSetAttrValue(AttrName, AttrValue: String);
  var
    i : Integer;
    Count : OleVariant;
    Exists : OleVariant;
    CurAttrName : OleVariant;
  begin
    i := 0;
    with (Dialoger as TDialoger) do
    begin
      Broker.BlackBoard.GetChildCount(BBFactsPath, BBFact, Count, Exists);

      if Exists <> 0 then
      begin
        while i < Count do
        begin
          Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrName, CurAttrName, Exists);
          if (Exists <> 0) and Equals(AttrName, CurAttrName) then break;
          Inc(i);
        end;
      end;

      if (i < Count) and (AttrValue = '') then Broker.BlackBoard.RemoveObject(BBFacts+'[' + IntToStr(i) + ']', Exists);

      if (AttrValue <> '') and (i = Count) then Broker.BlackBoard.AddObject(BBFacts, Exists);

      if AttrValue <> '' then
      begin
        Broker.BlackBoard.SetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrName, AttrName, Exists);
        Broker.BlackBoard.SetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrValue, AttrValue, Exists);
        Broker.BlackBoard.SetParamValue(BBFacts + '[' + IntToStr(i) + ']', constAskedProp, constFalsePropValue, Exists);
      end
    end;
  end;


  procedure TKernelForm.ManagerSetAttrProp(AttrName, PropName, PropValue: String);
  var
    i : Integer;
    Count : OleVariant;
    Exists : OleVariant;
    CurAttrName : OleVariant;
  begin
    i := 0;
    with (Dialoger as TDialoger) do
    begin
      Broker.BlackBoard.GetChildCount(BBFactsPath, BBFact, Count, Exists);

      if Exists <> 0 then
      begin
        while i < Count do
        begin
          Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrName, CurAttrName, Exists);
          if (Exists <> 0) and Equals(AttrName, CurAttrName) then break;
          Inc(i);
        end;
      end;

      if i = Count then Broker.BlackBoard.AddObject(BBFacts, Exists);

      Broker.BlackBoard.SetParamValue(BBFacts+'[' + IntToStr(i) + ']', BBAttrName, AttrName, Exists);
      Broker.BlackBoard.SetParamValue(BBFacts+'[' + IntToStr(i) + ']', PropName, PropValue, Exists);
    end;
  end;


  procedure TKernelForm.ManagerGetAttrValue(AttrName: String; var AttrValue: String);
  var
    i : Integer;
    Count : OleVariant;
    Exists : OleVariant;
    CurAttrName : OleVariant;
    CurAttrValue : OleVariant;
    AreEquals : Boolean;
  begin
    AttrValue := '';

    i := 0;
    with (Dialoger as TDialoger) do
    begin
      Broker.BlackBoard.GetChildCount(BBFactsPath, BBFact, Count, Exists);
      if Exists then
      begin
        while i < Count do
        begin
          Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrName, CurAttrName, Exists);
          try
            AreEquals := Equals(AttrName, CurAttrName);
          except
            AreEquals := false;
          end;
          if Exists and AreEquals then
          begin
            Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrValue, CurAttrValue, Exists);
            try
              if Exists then AttrValue := CurAttrValue;
            except
              AttrValue := '';
            end;
            break;
          end;
          Inc(i);
        end;
      end;
    end;
  end;


  procedure TKernelForm.ManagerGetAttrProp(AttrName, PropName: String; var PropValue: String);
  var
    i : Integer;
    Count : OleVariant;
    Exists : OleVariant;
    CurAttrName : OleVariant;
    CurPropValue : OleVariant;
    AreEquals : Boolean;
  begin
    PropValue := '';

    i := 0;
    with (Dialoger as TDialoger) do
    begin
      Broker.BlackBoard.GetChildCount(BBFactsPath, BBFact, Count, Exists);
      if Exists then
      begin
        while i < Count do
        begin
          Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', BBAttrName, CurAttrName, Exists);
          try
            AreEquals := Equals(AttrName, CurAttrName);
          except
            AreEquals := false;
          end;
          if Exists and AreEquals then
          begin
            Broker.BlackBoard.GetParamValue(BBFacts + '[' + IntToStr(i) + ']', PropName, CurPropValue, Exists);
            try
              if Exists then PropValue := CurPropValue;
            except
              PropValue := '';
            end;
            break;
          end;
          Inc(i);
        end;
      end;
    end;
  end;

end.
