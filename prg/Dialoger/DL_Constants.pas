unit DL_Constants;

interface

  const
    namAll = '*';
    namDialoger = 'Dialoger';

    FileNameNode = 'FILENAME';
    DSDLTextNode = 'DSDLTEXT';
    MessageNode = 'MESSAGE';
    ParamNode = 'PARAM';

    ConfigNodeForOutput = 'Config';
    FileNameNodeForOutput = 'FileName';

    ProcNameAttr = 'ProcName';
    ParamNameAttr = 'Name';

    RunProc = 'RUN';
    GetFactProc = 'GETFACT';
    SetFactProc = 'SETFACT';
    ExecuteProc = 'EXECUTE';
    ConfigureDialogerProc = 'CONFIGUREDIALOGER';
    IsComponentConfiguredProc = 'ISCOMPONENTCONFIGURED';
    SendProc = 'SEND';
    AttrNameParam = 'ATTRNAME';
    MessageNameParam = 'MESSAGENAME';
    ScenarioNameParam = 'SCENARIONAME';

    BBFactsPath = 'bb.wm.facts';
    BBFact = 'fact';
    BBFacts = BBFactsPath + '.' + BBFact;
    BBAttrName = 'AttrPath';
    BBAttrValue = 'Value';


implementation

end.
