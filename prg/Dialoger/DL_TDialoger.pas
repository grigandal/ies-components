unit DL_TDialoger;

{$WARN SYMBOL_PLATFORM OFF}

interface

  uses
    ComObj,
    ActiveX,
    DialogerLib_TLB,
    StdVcl,
    Forms,
    DL_TKernelForm;

  type
    TDialoger = class(TAutoObject, IDialoger)
      private
        Kernel : TKernelForm;
        Started : Boolean;
        DSDLFileName, DSDLText : String;
        ConfigText : WideString;

      protected
        function Get_Name : WideString; safecall;
        procedure Set_Name(const Value : WideString); safecall;
        procedure Set_Broker(Value : OleVariant); safecall;
        procedure ProcessMessage(const SndrName, MsgText : WideString; out Output : OleVariant); safecall;
        procedure Run(); safecall;
        procedure Test(const DSDLText: WideString; out ErrText: OleVariant); safecall;
        procedure AssignText(const DSDLText : WideString); safecall;
        procedure LoadText(const FileName : WideString); safecall;
        procedure GetFact(const AttrName: WideString); safecall;
        procedure Configurate(const Config : WideString); safecall;
        procedure Stop(); safecall;
        { Protected declarations }

      public
        Name : WideString;
        Broker : OleVariant;
        ExtSndrName : String;

        procedure Initialize(); override;
        destructor Destroy(); override;
    end;


implementation

  uses
    XMLDoc,
    XMLIntf,
    SysUtils,
    ComServ,
    Variants,
    Dialogs,
    DL_Constants;


  procedure TDialoger.Initialize();
  begin
    inherited Initialize;

    Kernel := TKernelForm.Create(nil);
    Kernel.Dialoger := Self;

    Started := False;
    ExtSndrName := '';

    Broker := Null();
    DSDLFileName := '';
    DSDLText := '';
    ConfigText := '';
  end;


  destructor TDialoger.Destroy();
  begin
    Kernel.Free();

    inherited Destroy();
  end;


  function TDialoger.Get_Name: WideString;
  begin
    Result := Name;
  end;


  procedure TDialoger.Set_Name(const Value: WideString);
  begin
    Name := Value;
  end;


  procedure TDialoger.Set_Broker(Value: OleVariant);
  begin
    Broker := Value;
  end;


  procedure TDialoger.Configurate(const Config : WideString);
  var
    XMLDoc : IXMLDocument;
    i : Integer;
    path: WideString;
  begin
    ConfigText := Config;
    XMLDoc := TXMLDocument.Create(nil);
    XMLDoc.LoadFromXML(Config);

    for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
    begin
      if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = FileNameNode then
      begin
        DSDLFileName := XMLDoc.DocumentElement.ChildNodes[i].NodeValue;
        // ���� ���� �������������, �� ������� ����
        if AnsiPos(':\', DSDLFileName) = 0 then begin
          try
            path := Broker.Path;
          except
            // ���� ��� �������� Path ��� ��� �������
            path := ExtractFilePath(Application.ExeName)
          end;
          DSDLFileName := path + DSDLFileName;
        end;
        Kernel.Interpreter.FileName := DSDLFileName;
      end;

      if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = DSDLTextNode then
      begin
        DSDLText := XMLDoc.DocumentElement.ChildNodes[i].NodeValue;
        if not VarIsNull(Broker)
        then Kernel.Interpreter.DSDLText := DSDLText;
      end;
    end;
  end;


  procedure TDialoger.ProcessMessage(const SndrName, MsgText: WideString; out Output: OleVariant);
  var
    XMLDoc : IXMLDocument;
    ProcName, Config : WideString;
    i : Integer;
    OldExtSndrName : String;
  begin
    XMLDoc := TXMLDocument.Create(nil);
    XMLDoc.LoadFromXML(MsgText);

    OldExtSndrName := ExtSndrName;
    ExtSndrName := SndrName;

    if (UpperCase(XMLDoc.DocumentElement.NodeName) = MessageNode) then
    begin
      ProcName := UpperCase(XMLDoc.DocumentElement.Attributes[ProcNameAttr]);

      if ProcName = ConfigureDialogerProc then
      begin
        Config := ConfigText;
        Kernel.OpenDialog.FileName := DSDLFileName;
        if Kernel.OpenDialog.Execute then
        begin
          Config := '<' + ConfigNodeForOutput + '>';
          Config := Config +
                    '<' + FileNameNodeForOutput + '>' +
                    Kernel.OpenDialog.FileName +
                    '</' + FileNameNodeForOutput + '>';
          Config := Config + '</' + ConfigNodeForOutput + '>';
        end;
        Output := Config;
      end;

      if ProcName =IsComponentConfiguredProc then
      begin
        if (DSDLFileName <> '') or (DSDLText <> '')
        then Output := 1
        else Output := 0;
      end;

      if (ProcName = RunProc) and not Started then
      begin
        Started := True;
        Kernel.Interpreter.Run();
        Started := False;
      end;

      if (ProcName = GetFactProc) then
      begin
        for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
        begin
          //old...
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = ParamNode then
            if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].Attributes[ParamNameAttr]) = AttrNameParam
            then Kernel.Interpreter.GetFact(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);

          //new...
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = AttrNameParam
          then Kernel.Interpreter.GetFact(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);
        end;
      end;

      if (ProcName = SetFactProc) then
      begin
        for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
        begin
          //old...
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = ParamNode then
            if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].Attributes[ParamNameAttr]) = AttrNameParam
            then Kernel.Interpreter.SetFact(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);

          //new...
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = AttrNameParam
          then Kernel.Interpreter.SetFact(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);
        end;
      end;

      if ProcName = ExecuteProc then
      begin
        for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
        begin
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = ScenarioNameParam
          then Kernel.Interpreter.ExecuteScenario(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);
        end;
      end;

      if ProcName = SendProc then
      begin
        for i := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
        begin
          if UpperCase(XMLDoc.DocumentElement.ChildNodes[i].NodeName) = MessageNameParam
          then Kernel.Interpreter.SendNamedMessage(XMLDoc.DocumentElement.ChildNodes[i].NodeValue);
        end;
      end;
    end;
    ExtSndrName := OldExtSndrName;
  end;


  procedure TDialoger.Run();
  begin
    Kernel.Interpreter.Run();
  end;


  procedure TDialoger.Test(const DSDLText: WideString; out ErrText: OleVariant);
  var
    Description : String;
  begin
    Kernel.Interpreter.Test(DSDLText, Description);
    ErrText := Description;
  end;
                           

  procedure TDialoger.AssignText(const DSDLText : WideString);
  begin
    Kernel.Interpreter.DSDLText := DSDLText;
  end;


  procedure TDialoger.LoadText(const FileName : WideString);
  begin
    Kernel.Interpreter.FileName := FileName;
  end;


  procedure TDialoger.GetFact(const AttrName: WideString);
  begin
    Kernel.Interpreter.GetFact(AttrName);
  end;


  procedure TDialoger.Stop();
  begin
    Kernel.Manager.Stop();
  end;


initialization

  TAutoObjectFactory.Create(ComServer, TDialoger, Class_Dialoger, ciMultiInstance, tmApartment);

end.
