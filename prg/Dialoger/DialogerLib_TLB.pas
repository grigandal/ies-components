unit DialogerLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 04.02.2007 21:35:59 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\DD\delphi\prg\Dialoger\DialogerLib.tlb (1)
// LIBID: {5F161C40-26BC-11D6-A03E-A8C8E99B4C5B}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\System32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DialogerLibMajorVersion = 1;
  DialogerLibMinorVersion = 0;

  LIBID_DialogerLib: TGUID = '{5F161C40-26BC-11D6-A03E-A8C8E99B4C5B}';

  IID_IDialoger: TGUID = '{5F161C41-26BC-11D6-A03E-A8C8E99B4C5B}';
  CLASS_Dialoger: TGUID = '{5F161C43-26BC-11D6-A03E-A8C8E99B4C5B}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IDialoger = interface;
  IDialogerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Dialoger = IDialoger;


// *********************************************************************//
// Interface: IDialoger
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F161C41-26BC-11D6-A03E-A8C8E99B4C5B}
// *********************************************************************//
  IDialoger = interface(IDispatch)
    ['{5F161C41-26BC-11D6-A03E-A8C8E99B4C5B}']
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Set_Broker(Param1: OleVariant); safecall;
    procedure ProcessMessage(const SndrName: WideString; const MsgText: WideString; 
                             out Output: OleVariant); safecall;
    procedure Run; safecall;
    procedure Test(const DSDLText: WideString; out ErrText: OleVariant); safecall;
    procedure AssignText(const DSDLText: WideString); safecall;
    procedure LoadText(const FileName: WideString); safecall;
    procedure GetFact(const AttrName: WideString); safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure Stop; safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IDialogerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5F161C41-26BC-11D6-A03E-A8C8E99B4C5B}
// *********************************************************************//
  IDialogerDisp = dispinterface
    ['{5F161C41-26BC-11D6-A03E-A8C8E99B4C5B}']
    property Name: WideString dispid 1;
    property Broker: OleVariant writeonly dispid 2;
    procedure ProcessMessage(const SndrName: WideString; const MsgText: WideString; 
                             out Output: OleVariant); dispid 3;
    procedure Run; dispid 4;
    procedure Test(const DSDLText: WideString; out ErrText: OleVariant); dispid 5;
    procedure AssignText(const DSDLText: WideString); dispid 6;
    procedure LoadText(const FileName: WideString); dispid 7;
    procedure GetFact(const AttrName: WideString); dispid 8;
    procedure Configurate(const Config: WideString); dispid 9;
    procedure Stop; dispid 10;
  end;

// *********************************************************************//
// The Class CoDialoger provides a Create and CreateRemote method to          
// create instances of the default interface IDialoger exposed by              
// the CoClass Dialoger. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDialoger = class
    class function Create: IDialoger;
    class function CreateRemote(const MachineName: string): IDialoger;
  end;

implementation

uses ComObj;

class function CoDialoger.Create: IDialoger;
begin
  Result := CreateComObject(CLASS_Dialoger) as IDialoger;
end;

class function CoDialoger.CreateRemote(const MachineName: string): IDialoger;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Dialoger) as IDialoger;
end;

end.
