unit DSDLEC_TMessagesField;

interface

  uses
    Classes,
    Controls,
    StdCtrls,
    DSDLEC_Types,
    DSDLEC_TDSDLEField;


  type
    TMessagesField = class(TDSDLEField)
      private
        FOnSetPos : TOnSetPos2;

        ListBox : TListBox;

        procedure ListBoxClickHandler(Sender : TObject);

        function GetMessages() : String;
        procedure SetMessages(Value : String);

      public
        property Messages : String read GetMessages write SetMessages;

        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

//        procedure AddMessage(Text : String);
//        procedure Clear();

      published
        property OnSetPos : TOnSetPos2 read FOnSetPos write FOnSetPos;
    end;

    procedure Register();


implementation
  uses
    Sysutils,
    Forms;

  constructor TMessagesField.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);

    ListBox := TListBox.Create(Self);
    ListBox.Parent := Self;

    ListBox.Align := alClient;
    ListBox.BorderStyle := bsNone;

    ListBox.Font.Name := 'Courier New';
    ListBox.Font.Size := 8;
    ListBox.Font.Charset := 204;

    ListBox.OnDblClick := ListBoxClickHandler;
  end;


  destructor TMessagesField.Destroy();
  begin
    ListBox.Destroy();
    inherited Destroy();
  end;

{
  procedure TMessagesField.AddMessage(Text : String);
  begin
    ListBox.Items.Text := ListBox.Items.Text + Text;
  end;
}

  function TMessagesField.GetMessages() : String;
  begin
    Result := ListBox.Items.Text;
  end;


  procedure TMessagesField.SetMessages(Value : String);
  begin
    ListBox.Items.Text := Value;
  end;

{
  procedure TMessagesField.Clear();
  begin
    ListBox.Clear();
  end;
}

  procedure TMessagesField.ListBoxClickHandler(Sender : TObject);
  var
    Line, Col : Integer;
    Mes, Str : String;
  begin
    Mes := ListBox.Items[ListBox.ItemIndex];
    Mes := Copy(Mes, Pos('[', Mes) + 1, Pos(']', Mes) - Pos('[', Mes) - 1);

    Str := Copy(Mes, 1, Pos(':', Mes) - 1);
    while (Length(Str) > 0) and (Str[Length(Str)] = ' ') do Delete(Str, Length(Str), 1);
    Line := StrToIntDef(Str, 0);

    Str := Copy(Mes, Pos(':', Mes) + 1, Length(Mes) - Pos(':', Mes));
    while (Length(Str) > 0) and (Str[Length(Str)] = ' ') do Delete(Str, Length(Str), 1);
    Col := StrToIntDef(Str, 0);

    if Assigned(OnSetPos) and (Line > 0) and (Col > 0)
    then OnSetPos(Line, Col);
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TMessagesField]);
  end;

end.
