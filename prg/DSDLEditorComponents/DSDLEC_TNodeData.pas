unit DSDLEC_TNodeData;

interface

  type
    TNodeData = class(TObject)
      private
        FStartPos : Integer;
        FEndPos : Integer;

      public
        property StartPos : Integer read FStartPos write FStartPos;
        property EndPos : Integer read FEndPos write FEndPos;
    end;


implementation

end.
