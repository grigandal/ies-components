unit DSDLEC_Resources;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList;

type
  TResources = class(TForm)
    TreeIcons: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Resources: TResources;

implementation
{$R *.DFM}


initialization
  Resources := TResources.Create(nil);


finalization
  Resources.Destroy();

end.
