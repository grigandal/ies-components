unit DSDLEC_Types;

interface
  type
    TOnChangeText = procedure(Text : String; CurPos : Integer) of object;
    TOnChangePos = procedure(Pos : Integer) of object;
    TOnSetPos1 = procedure(Pos : Integer) of object;
    TOnSetPos2 = procedure(Line, Col : Integer) of object;

    TItemKind = (ikNoItem, ikScenario, ikMessage);

    
implementation

end.
