unit DSDLEC_TTreeField;

interface
  uses
    Classes,
    Controls,
    ComCtrls,
    DSDLEC_Types,
    DSDLEC_TDSDLEField;


  type
    TTreeField = class(TDSDLEField)
      private
        FOnSetPos : TOnSetPos1;

        Tree : TTreeView;

        Scenarios : TTreeNode;
        Messages : TTreeNode;

        procedure TreeClickHandler(Sender : TObject);

      public
        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

        procedure RefreshTree(Text : String; CurPos : Integer);

        procedure SetActiveItem(Pos : Integer; var ItemName : String; var ItemKind : TItemKind);
        procedure GetActiveItem(var ItemName : String; var ItemKind : TItemKind);

      published
        property OnSetPos : TOnSetPos1 read FOnSetPos write FOnSetPos;
    end;

    procedure Register();


implementation
  uses
    Forms,
    SysUtils,
    DSDLEC_Resources,
    DSDLEC_Constants,
    DSDLEC_TNodeData;


  constructor TTreeField.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);

    Tree := TTreeView.Create(Self);
    Tree.Parent := Self;

    Tree.Align := alClient;
    Tree.BorderStyle := bsNone;
    Tree.ReadOnly := True;

    Tree.Font.Name := 'Courier New';
    Tree.Font.Size := 8;
    Tree.Font.Charset := 204;

    Tree.Images := Resources.TreeIcons;

    Tree.OnDblClick := TreeClickHandler;

    //��� ������ �.�. ����������� ����� (!) ���������� ���� �������� (?)
    Scenarios := Tree.Items.Add(nil, hdScenarios);
    Messages := Tree.Items.Add(nil, hdMessages);
  end;


  destructor TTreeField.Destroy();
  var
    i : Integer;
  begin
    for i := 0 to Tree.Items.Count - 1 do TNodeData(Tree.Items[i].Data).Free();
    Tree.Destroy();

    inherited Destroy();
  end;


  procedure TTreeField.RefreshTree(Text : String; CurPos : Integer);
  var
    i : Integer;
    Node : TTreeNode;
    Id, KW : String;
    Comments : Boolean;
    Str : Boolean;
    Citing : Boolean;
    ScenarioIndex, MessageIndex : Integer;
    Pos : Integer;
    NodeData : TNodeData;
  begin
    Tree.Items.BeginUpdate();

    Id := '';
    KW := '';
    Pos := 0;
    Comments := False;
    Str := False;
    Citing := False;
    ScenarioIndex := -1;
    MessageIndex := -1;
    NodeData := nil;

    for i := 1 to Length(Text) do
    begin
      if not Comments then
      begin
        if not Str then
        begin
          if Text[i] in constIdChars then Id := Id + Text[i];
          if (not (Text[i] in constIdChars) or (i = Length(Text))) and (Id <> '') then
          begin
            if (UpperCase(KW) = kwScenario) or (UpperCase(KW) = kwSubscenario) then
            begin
              if NodeData <> nil then NodeData.EndPos := Pos - 1;
              Inc(ScenarioIndex);
              if ScenarioIndex >= Scenarios.Count then
              begin
                Node := Tree.Items.AddChild(Scenarios, '');
                Node.Data := TNodeData.Create();
              end
              else Node := Scenarios.Item[ScenarioIndex];
              NodeData := Node.Data;
              Node.ImageIndex := 1; //
              Node.Text := Id;
              NodeData.StartPos := Pos;
              NodeData.EndPos := Length(Text) + 1;
            end;

            if UpperCase(KW) = kwMessage then
            begin
              if NodeData <> nil then NodeData.EndPos := Pos - 1;
              Inc(MessageIndex);
              if MessageIndex >= Messages.Count then
              begin
                Node := Tree.Items.AddChild(Messages, '');
                Node.Data := TNodeData.Create();
              end
              else Node := Messages.Item[MessageIndex];
              NodeData := Node.Data;
              Node.ImageIndex := 1; //
              Node.Text := Id;
              NodeData.StartPos := Pos;
              NodeData.EndPos := Length(Text) + 1;
            end;

            if (UpperCase(Id) = kwEnd) and (NodeData <> nil) then
            begin
              NodeData.EndPos := i - 1;
              NodeData := nil;
            end;

            KW := Id;
            Pos := i - Length(Id);
            Id := '';
          end;
          Comments := Text[i] = chCommentsOpen; //Comments begin
          Str := Text[i] = chStringMarker; //Str begin
        end else
        begin
          Str := (Text[i] <> chStringMarker) or Citing;
          Citing := Text[i] = chCitingMarker;
        end;
      end else
      begin
        Comments := Text[i] <> chCommentsClose; //Comments end
      end;
    end;

    //Deleting...
    while ScenarioIndex + 1 < Scenarios.Count do
    begin
      TNodeData(Scenarios.Item[ScenarioIndex + 1].Data).Free();
      Scenarios.Item[ScenarioIndex + 1].Delete();
    end;

    while MessageIndex + 1 < Messages.Count do
    begin
      TNodeData(Messages.Item[MessageIndex + 1].Data).Free();
      Messages.Item[MessageIndex + 1].Delete();
    end;

    Tree.Selected := Scenarios;

    Tree.Items.EndUpdate();
  end;


  procedure TTreeField.SetActiveItem(Pos : Integer; var ItemName : String; var ItemKind : TItemKind);
  var
    i : Integer;
  begin
    ItemName := '';
    ItemKind := ikNoItem;

    for i := 0 to Tree.Items.Count - 1 do
      if (Tree.Items[i] <> Scenarios) and (Tree.Items[i] <> Messages) then
      begin
        if (TNodeData(Tree.Items[i].Data).StartPos <= Pos) and
           (TNodeData(Tree.Items[i].Data).EndPos >= Pos) then
        begin
          Tree.Items[i].ImageIndex := 2;
          Tree.Items[i].SelectedIndex := 2;
          if Tree.Items[i].Parent.Expanded then Tree.Selected := Tree.Items[i];
          ItemName := Tree.Items[i].Text;
          if Tree.Items[i].Parent = Scenarios then ItemKind := ikScenario;
          if Tree.Items[i].Parent = Messages then ItemKind := ikMessage;
        end else
        begin
          Tree.Items[i].ImageIndex := 1;
          Tree.Items[i].SelectedIndex := 1;
        end;
      end;
  end;


  procedure TTreeField.GetActiveItem(var ItemName : String; var ItemKind : TItemKind);
  var
    i : Integer;
  begin
    ItemName := '';
    ItemKind := ikNoItem;

    i := 0;
    while (i < Scenarios.Count) and (Scenarios[i].SelectedIndex <> 2) do Inc(i);
    if i < Scenarios.Count then
    begin
      ItemName := Scenarios[i].Text;
      ItemKind := ikScenario;
    end else
    begin
      i := 0;
      while (i < Messages.Count) and (Messages[i].SelectedIndex <> 2) do Inc(i);
      if i < Messages.Count then
      begin
        ItemName := Messages[i].Text;
        ItemKind := ikMessage;
      end;
    end;
  end;


  procedure TTreeField.TreeClickHandler(Sender : TObject);
  begin
    if (Tree.Selected <> Scenarios) and (Tree.Selected <> Messages) and Assigned(OnSetPos)
    then OnSetPos(TNodeData(Tree.Selected.Data).StartPos);
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TTreeField]);
  end;

end.
