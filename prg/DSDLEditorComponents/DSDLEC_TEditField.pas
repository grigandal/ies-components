unit DSDLEC_TEditField;

interface

  uses
    Classes,
    Controls,
    StdCtrls,
    ExtCtrls,
    DSDLEC_Types,
    DSDLEC_TDSDLEField;


  type
    TEditField = class(TDSDLEField)
      private
        TextPos : Integer;
        TextSelLen : Integer;
        TextSelStart : Integer;

        FOnChangeText : TOnChangeText;
        FOnChangePos : TOnChangePos;

        ChangeTextTimer : TTimer;
        ChangePosTimer : TTimer;

        FMemo : TMemo;

        procedure SetText(Value : String);
        function GetText() : String;

        function GetModified() : Boolean;
        procedure SetModified(Value : Boolean);

        procedure MemoChangeHandler(Sender : TObject);
        procedure TimerChangeTextHandler(Sender : TObject);
        procedure TimerChangePosHandler(Sender : TObject);

      public
        property Text : String read GetText write SetText;
        property Modified : Boolean read GetModified write SetModified;
        property Memo : TMemo read FMemo write FMemo;

        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

        procedure GetTextPos(var Pos : Integer); overload;
        procedure GetTextPos(var Line, Col : Integer); overload;
        procedure SetTextPos(Pos : Integer); overload;
        procedure SetTextPos(Line, Col : Integer); overload;

        procedure Load(FileName : String);
        procedure Save(FileName : String);

      published
        property OnChangeText : TOnChangeText read FOnChangeText write FOnChangeText;
        property OnChangePos : TOnChangePos read FOnChangePos write FOnChangePos;
    end;

    procedure Register();


implementation
  uses
    Forms,
    DSDLEC_Constants;


  constructor TEditField.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);

    TextPos := -1;
    TextSelLen := -1;
    TextSelStart := -1;

    ChangeTextTimer := TTimer.Create(Self);
    ChangeTextTimer.Interval := ChangeTextScanningInterval;

    ChangePosTimer := TTimer.Create(Self);
    ChangePosTimer.Interval := ChangePosScanningInterval;

    Memo := TMemo.Create(Self);
    Memo.Parent := Self;

//    Memo.Lines.Capacity := 

    Memo.Align := alClient;
    Memo.BorderStyle :=bsNone;
    Memo.ScrollBars := ssBoth;

    Memo.Font.Name := 'Courier New';
    Memo.Font.Size := 10;
    Memo.Font.Charset := 204;

    Memo.OnChange := MemoChangeHandler;

    ChangeTextTimer.OnTimer := TimerChangeTextHandler;
    ChangePosTimer.OnTimer := TimerChangePosHandler;

    ChangePosTimer.Enabled := True;
  end;


  destructor TEditField.Destroy();
  begin
    ChangeTextTimer.Enabled := False;
    ChangeTextTimer.Destroy();

    ChangePosTimer.Enabled := False;
    ChangePosTimer.Destroy();

    Memo.Destroy();
    inherited Destroy();
  end;


  procedure TEditField.SetText(Value : String);
  begin
    Memo.OnChange:= nil;

    Memo.Lines.Text := Value;
    Modified := True;
    TimerChangeTextHandler(Self);

    Memo.OnChange:= MemoChangeHandler;
  end;


  function TEditField.GetText() : String;
  begin
    Result := Memo.Lines.Text;
  end;


  function TEditField.GetModified() : Boolean;
  begin
    Result := Memo.Modified;
  end;


  procedure TEditField.SetModified(Value : Boolean);
  begin
    Memo.Modified := Value;
  end;


  procedure TEditField.GetTextPos(var Pos : Integer);
  begin
    Pos := TextPos;
  end;


  procedure TEditField.GetTextPos(var Line, Col : Integer);
  var
    i : Integer;
    Txt : String;
  begin
    Line := 1;
    Col := 1;

    Txt := Text;
    for i := 1 to TextPos - 1 do
    begin
      if Txt[i] = #10 then
      begin
        Inc(Line);
        Col := 1;
      end;

      if (Txt[i] <> #10) and (Txt[i] <> #13) then Inc(Col);
    end;
  end;


  procedure TEditField.SetTextPos(Pos : Integer);
  begin
    Memo.SelStart := Pos - 1;
    Memo.SelLength := 1;
    Memo.SelLength := 0;
    if (Parent as TForm).Active then Memo.SetFocus();
  end;


  procedure TEditField.SetTextPos(Line, Col : Integer);
  var
    Pos : Integer;
    Txt : String;
  begin
    Pos := 1;
    Txt := Text;

    while (Line > 1) and (Pos <= Length(Txt)) do
    begin
      if Txt[Pos] = #10 then Dec(Line);
      Inc(Pos);
    end;

    if Line = 1 then
    begin
      while (Col > 1) and (Pos <= Length(Txt)) and (Txt[Pos] <> #13) and (Txt[Pos] <> #10) do
      begin
        Dec(Col);
        Inc(Pos);
      end;

      if Col = 1 then SetTextPos(Pos);
    end;
  end;


  procedure TEditField.Load(FileName : String);
  begin
    Memo.OnChange:= nil;

    Memo.Lines.LoadFromFile(FileName);
    SetTextPos(1);
    Modified := False;
    TimerChangeTextHandler(Self);

    Memo.OnChange:= MemoChangeHandler;
  end;


  procedure TEditField.Save(FileName : String);
  begin
    Memo.Lines.SaveToFile(FileName);
    Modified := False;
  end;


  procedure TEditField.MemoChangeHandler(Sender : TObject);
  begin
    ChangeTextTimer.Enabled := False;
    ChangeTextTimer.Enabled := True;
  end;


  procedure TEditField.TimerChangeTextHandler(Sender : TObject);
  begin
    ChangeTextTimer.Enabled := False;
    if Assigned(OnChangeText) then OnChangeText(Memo.Lines.Text, TextPos);
    TextSelLen := -1; //to call TimerChangePosHandler
  end;


  procedure TEditField.TimerChangePosHandler(Sender : TObject);
  begin
    ChangePosTimer.Enabled := False;
    if (TextSelStart <> Memo.SelStart) or (TextSelLen <> Memo.SelLength) then
    begin
      if TextSelStart <> Memo.SelStart then TextPos := Memo.SelStart + 1;

      if (TextSelLen <> Memo.SelLength) and (TextSelStart = Memo.SelStart)
      then TextPos := Memo.SelStart + Memo.SelLength + 1;

      if Assigned(OnChangePos) and not ChangeTextTimer.Enabled then
      begin
        OnChangePos(TextPos);
        TextSelLen := Memo.SelLength;
        TextSelStart := Memo.SelStart;
      end;
    end;
    ChangePosTimer.Enabled := True;
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TEditField]);
  end;

end.
