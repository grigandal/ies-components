unit DSDLEC_Constants;

interface
  const
    ChangePosScanningInterval = 50;
    ChangeTextScanningInterval = 500;

    constIdChars = ['0'..'9','A'..'Z','a'..'z','_',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�'];

    hdScenarios = '��������';
    hdMessages = '���������';

    kwScenario = 'SCENARIO';
    kwSubscenario = 'SUBSCENARIO';
    kwMessage = 'MESSAGE';
    kwEnd = 'END';

    chCommentsOpen ='/';
    chCommentsClose = '/';
    chStringMarker = '''';
    chCitingMarker = '$';

implementation

end.
