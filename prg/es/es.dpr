program es;

uses
  Forms,
  ComObj,
  SysUtils,
  Variants, Dialogs;

  {$R *.res}

var
  Starter : OleVariant;

begin
  try
    Application.Initialize;
    Starter := CreateOleObject('StarterLib.Starter');

    Starter.FileName := ExtractFilePath(Application.ExeName) + 'config.xml';
    Starter.Start;

    Starter := Unassigned;
  except
    on e: Exception do
      ShowMessage(e.ClassName + ' ' + e.Message);
  end;                                           
end.
