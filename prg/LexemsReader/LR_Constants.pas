unit LR_Constants;

interface
  const
    constSeparators = [' ', #10, #13, #09];
    constIdChars = ['0'..'9','A'..'Z','a'..'z','_',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�'];

    constStringMarker = #39;
    constTextOpen = '[';
    constTextClose = ']';
    constCitingMarker = '$';
    constNameMarker = '#';
//    constCommentsOpen = '/';
//    constCommentsClose = '/';
    constComma = ',';

    constString = '��������';
    constLine = '������';
    constIdentifier = '�������������';
    constKeyWord = '�������� �����';
    constValue = '��������';
    constName = '��� ��������';
    constMark = '����';

    constNotFound1 = '�� ������';
    constNotFound2 = '�� �������';
    constNotFound3 = '�� �������';

    constStringReadingError = constString + ' ' + constNotFound3;
    constIdentifierReadingError = constIdentifier + ' ' + constNotFound1;
    constKeyWordReadingError = constKeyWord + ' ' + constNotFound3;
    constValueReadingError = constValue + ' ' + constNotFound3;
    constNameReadingError = constName + ' ' + constNotFound3;
    constMarkReadingError = constMark + ' ' + constNotFound1;


implementation

end.

