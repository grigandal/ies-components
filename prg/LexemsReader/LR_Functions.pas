unit LR_Functions;

interface

  function UppercaseString(Str : String) : String;
  function AreStringsEqual(Str1, Str2 : String) : Boolean;
  function StringAfterCiting(Str : String) : String;

implementation
  uses
    Dialogs, //
    SysUtils,
    LR_Constants;

  function UppercaseString(Str : String) : String;
  var
    i : Integer;
  begin
    Result := '';
    for i := 1 to Length(Str) do
      case Str[i] of
        'a'..'z' : Result := Result + UpperCase(Str[i]);
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        else Result := Result + Str[i];
      end;
  end;


  function AreStringsEqual(Str1, Str2 : String) : Boolean;
  begin
    Result := UppercaseString(Str1) = UppercaseString(Str2);
  end;


  function StringAfterCiting(Str : String) : String;
  var
    i : Integer;
    Citing : Boolean;
  begin
    ShowMessage('V2: ' + Str);
    Result := '';
    Citing := False;
    for i := 1 to Length(Str) do
    begin
      if Citing then Citing := False else
      begin
        if Str[i] = constCitingMarker then Citing := True;
        if Str[i] = constStringMarker then Result := Result + constCitingMarker;
      end;
      Result := Result + Str[i];
    end;
  end;

end.
