unit LR_TIdItem;

interface
  uses
    Classes;

  type
    TIdItem = class(TCollectionItem)
      private
        FIdName : String;
        FIdKind : Integer;
      protected
      public
        property IdName : String read FIdName write FIdName;
        property IdKind : Integer read FIdKind write FIdKind;
    end;

    
implementation

end.
