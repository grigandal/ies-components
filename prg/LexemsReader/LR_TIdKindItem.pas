unit LR_TIdKindItem;

interface
  uses
    Classes;

  type
    TIdKindItem = class(TCollectionItem)
      private
        FReadingErrorDescription : String;
        FIdKind : Integer;
      protected
      public
        property IdKind : Integer read FIdKind write FIdKind;
        property ReadingErrorDescription : String
                 read FReadingErrorDescription
                 write FReadingErrorDescription;
    end;



implementation

end.
