unit LR_TLexemsReader;

interface
  uses
    Classes,
    BC_TATataComponent,
    LR_Types,
    LR_TIdKindItem;


  type
    TLexemsReader = class (TATataComponent)
      private
        Identifiers : TCollection;
        Kinds : TCollection;

        FPosition : Integer;
        FResult : String;

        FText : String;
        FTitle : String;

        FKeyWords : array of String;
        FKeyWordsString : String;
        FCommentOpenMarker : Char;
        FCommentCloseMarker : Char;

        FMarks : TMarks;
        FMarksString : String;

        procedure SetText(Value : String);
        procedure SetMarks(Value : String);
        procedure SetKeyWords(Value : String);

      protected
        procedure ReadString();
        procedure ReadText();

        function LineNumber() : Integer;
        procedure GetCurPos(var StrNum, ColNum : Integer);

        function IdKind(IdName : String) : Integer;
        function ExistsId(IdName : String; IdKind : Integer) : Boolean;
        function IdKindItem(IdKind : Integer) : TIdKindItem;

      public
        property Text : String read FText write SetText;
        property Title : String read FTitle write FTitle;
        property Position : Integer read FPosition write FPosition;

        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;

        procedure AddIdKind(IdKind : Integer; ReadingErrorDescription : String);
        procedure AddId(IdName : String; IdKind : Integer);

        procedure AssignText(AText, ATitle : String);
        procedure SetParameters(AKeyWords : array of String; AMarks : TMarks);

        function Prefix() : String;

        function ReadingResult() : String;

        procedure PassSeparators();
        procedure PassOperatorsSeparators();
        procedure ReadIdentifier(); overload;
        procedure ReadIdentifier(IdKind : Integer; IdMustBeKnown : Boolean); overload;
        procedure ReadKeyWord();
        procedure ReadValue();
        procedure ReadName();
        procedure ReadMark();

        procedure ReadKeyWordIfItIs(KeyWord : String);
        procedure ReadMarkIfItIs(Mark : Char);

        function IsKeyWord(Str : String) : Boolean;

        function EndOfText() : Boolean;

      published
        property Marks : String read FMarksString write SetMarks;
        property KeyWords : String read FKeyWordsString write SetKeyWords;
        property CommentOpenMarker : Char read FCommentOpenMarker write FCommentOpenMarker;
        property CommentCloseMarker : Char read FCommentCloseMarker write FCommentCloseMarker;
    end;


  procedure Register();


implementation
  {$R LexemsReader.dcr}

  uses
    SysUtils,
    LR_Constants,
    LR_Functions,
    LR_TIdItem;


  constructor TLexemsReader.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Text := '';
    KeyWords := '';
    Marks := '';
    CommentOpenMarker := '/';
    CommentCloseMarker := '/';

    Identifiers := TCollection.Create(TIdItem);
    Kinds := TCollection.Create(TIdKindItem);
  end;


  destructor TLexemsReader.Destroy();
  begin
    SetLength(FKeyWords, 0);
    Identifiers.Destroy();
    Kinds.Destroy();
    inherited Destroy();
  end;


  procedure TLexemsReader.AddIdKind(IdKind : Integer; ReadingErrorDescription : String);
  var
    IdRec : TIdKindItem;
  begin
    IdRec := Kinds.Add() as TIdKindItem;
    IdRec.IdKind := IdKind;
    IdRec.ReadingErrorDescription := ReadingErrorDescription;
  end;


  procedure TLexemsReader.AddId(IdName : String; IdKind : Integer);
  var
    IdRec : TIdItem;
  begin
    IdRec := Identifiers.Add() as TIdItem;
    IdRec.IdName := IdName;
    IdRec.IdKind := IdKind;
  end;


  function TLexemsReader.IdKindItem(IdKind : Integer) : TIdKindItem;
  var
    i : Integer;
  begin
    i := 0;
    while (i < Kinds.Count) and
          ((Kinds.Items[i] as TIdKindItem).IdKind <> IdKind)
    do Inc(i);

    if i < Kinds.Count
    then Result := Kinds.Items[i] as TIdKindItem
    else Result := nil;
  end;


  function TLexemsReader.IdKind(IdName : String) : Integer;
  var
    i : Integer;
  begin
    i := 0;
    while (i < Identifiers.Count) and
          not AreStringsEqual((Identifiers.Items[i] as TIdItem).IdName, IdName)
    do Inc(i);

    if i < Identifiers.Count
    then Result := (Identifiers.Items[i] as TIdItem).IdKind
    else Result := -1;
  end;


  function TLexemsReader.ExistsId(IdName : String; IdKind : Integer) : Boolean;
  var
    i : Integer;
  begin
    i := 0;
    while (i < Identifiers.Count) and
          (not AreStringsEqual((Identifiers.Items[i] as TIdItem).IdName, IdName) or
           ((Identifiers.Items[i] as TIdItem).IdKind <> IdKind))
    do Inc(i);

    Result := i < Identifiers.Count;
  end;

  
  procedure TLexemsReader.SetText(Value : String);
  begin
    FText := Value;
    FPosition := 1;
  end;


  procedure TLexemsReader.SetMarks(Value : String);
  var
    i : Integer;
  begin
    FMarksString := Value;
    FMarks := [];
    for i := 1 to Length(Value) do FMarks := FMarks + [Value[i]];
  end;


  procedure TLexemsReader.SetKeyWords(Value : String);
  var
    i, CurKeyWordIndex : Integer;
    IsKeyWord : Boolean;
    KeyWord : String;
  begin
    FKeyWordsString := Value;

    IsKeyWord := False;
    CurKeyWordIndex := 0;

    for i := 1 to Length(Value) do
    begin
      if (Value[i] in constIdChars) and not IsKeyWord then Inc(CurKeyWordIndex);
      IsKeyWord := (Value[i] in constIdChars);
    end;

    SetLength(FKeyWords, CurKeyWordIndex);

    KeyWord := '';
    CurKeyWordIndex := 0;
    for i := 1 to Length(Value) do
    begin
      if Value[i] in constIdChars then KeyWord := KeyWord + Value[i];
      if (not (Value[i] in constIdChars) or (i = Length(Value))) and
         (KeyWord <> '') then
      begin
        FKeyWords[CurKeyWordIndex] := KeyWord;
        Inc(CurKeyWordIndex);
        KeyWord := '';
      end;
    end;
  end;


  procedure TLexemsReader.AssignText(AText, ATitle : String);
  begin
    FText := AText;
    FTitle := ATitle;
    FPosition := 1;
  end;


  procedure TLexemsReader.SetParameters(AKeyWords : array of String; AMarks : TMarks);
  var
    i : Integer;
  begin
    SetLength(FKeyWords, Length(AKeyWords));
    for i := 0 to High(AKeyWords) do FKeyWords[i] := AKeyWords[i];

    FMarks := AMarks;
  end;


  function TLexemsReader.LineNumber() : Integer;
  var
    CurPos : Integer;
  begin
    Result := 1;
    CurPos := 1;
    while (CurPos <= FPosition) and (CurPos <= Length(FText)) do
    begin
      if FText[CurPos] = #10 then Inc(Result);
      Inc(CurPos);
    end;
  end;


  procedure TLexemsReader.GetCurPos(var StrNum, ColNum : Integer);
  var
    CurPos : Integer;
  begin
    StrNum := 1;
    ColNum := 1;
    CurPos := 1;
    while (CurPos < FPosition) and (CurPos <= Length(FText)) do
    begin
      if FText[CurPos] = #10 then Inc(StrNum);
      if (FText[CurPos] <> #10) and (FText[CurPos] <> #13) then Inc(ColNum) else ColNum := 1;
      Inc(CurPos);
    end;
  end;


  function TLexemsReader.Prefix() : String;
  var
    StrNum, ColNum : Integer;
  begin
    GetCurPos(StrNum, ColNum);
    Result := FTitle + '[' + IntToStr(StrNum) + ':' + IntToStr(ColNum) + '] ';
  end;


  function TLexemsReader.ReadingResult() : String;
  begin
    Result := FResult;
  end;


  procedure TLexemsReader.PassSeparators();
  var
    Comments : Boolean;
  begin
    Comments := False;
    FResult := '';
    while (FPosition <= Length(FText)) and
          ((FText[FPosition] in constSeparators) or
           (FText[FPosition] = CommentOpenMarker) or
            Comments) do
    begin
      if Comments
      then Comments := FText[FPosition] <> CommentCloseMarker
      else Comments := FText[FPosition] = CommentOpenMarker;
      Inc(FPosition);
    end;
  end;


  procedure TLexemsReader.PassOperatorsSeparators();
  var
    Comments : Boolean;
  begin
    Comments := False;
    FResult := '';
    while (FPosition <= Length(FText)) and
          ((FText[FPosition] in constSeparators) or
           (FText[FPosition] = ';')or
           (FText[FPosition] = CommentOpenMarker) or  Comments) do
    begin
      if Comments
      then Comments := FText[FPosition] <> CommentCloseMarker
      else Comments := FText[FPosition] = CommentOpenMarker;
      Inc(FPosition);
    end;
  end;


  procedure TLexemsReader.ReadIdentifier();
  var
    TempPos : Integer;
  begin
    PassSeparators();
    FResult := '';
    TempPos := FPosition;
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      FResult := FResult + FText[TempPos];
      Inc(TempPos);
    end;
    if (FResult <> '') and not IsKeyWord(FResult) then
    begin
      ClearWarnings();
      FPosition := TempPos;
    end else
    begin
      FResult := '';
      AddWarning(Prefix() + constIdentifierReadingError);
    end;
  end;


  procedure TLexemsReader.ReadIdentifier(IdKind : Integer; IdMustBeKnown : Boolean);
  var
    TempPos : Integer;
    IdKindRec : TIdKindItem;
  begin
    PassSeparators();
    FResult := '';
    TempPos := FPosition;
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      FResult := FResult + FText[TempPos];
      Inc(TempPos);
    end;

    if (FResult <> '') and not IsKeyWord(FResult) and
       (not IdMustBeKnown or ExistsId(FResult, IdKind)) then
    begin
      ClearWarnings();
      FPosition := TempPos;
    end else
    begin
      FResult := '';
      IdKindRec := IdKindItem(IdKind);
      if IdKindRec <> nil
      then AddWarning(Prefix() + IdKindRec.ReadingErrorDescription)
      else AddWarning(Prefix() + constIdentifierReadingError);
    end;
  end;


  procedure TLexemsReader.ReadKeyWord();
  var
    TempPos : Integer;
  begin
    PassSeparators();
    FResult := '';
    TempPos := FPosition;
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      FResult := FResult + FText[TempPos];
      Inc(TempPos);
    end;
    if (FResult <> '') and IsKeyWord(FResult) then
    begin
      ClearWarnings();
      FPosition := TempPos;
    end else
    begin
      FResult := '';
      AddWarning(Prefix() + constKeyWordReadingError);
    end;
  end;


  procedure TLexemsReader.ReadString();
  var
    TempPos : Integer;
    Successful, WasMarker : Boolean;
    Citing : Boolean;
  begin
    PassSeparators();
    FResult := '';
    Successful := False;
    WasMarker := False;
    Citing := False;
    TempPos := FPosition;
    if (TempPos <= Length(FText)) and (FText[TempPos] = constStringMarker) then
    begin
      WasMarker := True;
      Inc(TempPos);
      while (TempPos <= Length(FText)) and
            ((FText[TempPos] <> constStringMarker) or Citing) and
            (FText[TempPos] <> #10) and
            (FText[TempPos] <> #13)
      do
      begin
        if not Citing and (FText[TempPos] = constCitingMarker)
        then Citing := true else
        begin
          FResult := FResult + FText[TempPos];
          Citing := False;
        end;
        Inc(TempPos);
      end;
      if (TempPos <= Length(FText)) and (FText[TempPos] = constStringMarker)
      then Successful := True;
    end;

    if Successful then
    begin
      ClearWarnings();
      Inc(TempPos);
      FPosition := TempPos;
    end;

    if not Successful and not WasMarker then
    begin
      FResult := '';
      AddWarning(Prefix() + constStringReadingError);
    end;

    if not Successful and WasMarker then
    begin
      FResult := '';
      AddWarning(Prefix() +
                 constMark + ' " ' + constStringMarker + ' " ' + constNotFound1);
      ProcessError();
    end;
  end;


  procedure TLexemsReader.ReadText();
  var
    OldPos : Integer;
    Res : String;
    WasStrings : Boolean;
  begin
    PassSeparators();
    OldPos := FPosition;
    Res := '';
    ReadMarkIfItIs(constTextOpen);
    if not WasWarning() then
    begin
      WasStrings := False;
      while not WasWarning() and not WasError() do
      begin
        ReadString();

        if WasWarning() and WasStrings then ProcessError();

        if not WasWarning() and not WasError() then
        begin
          if WasStrings then Res := Res + #13 + #10;
          Res := Res + FResult;
          WasStrings := True;
        end;

        if not WasError() and WasStrings
        then ReadMarkIfItIs(constComma);
      end;

      if not WasError() then
      begin
        ReadMarkIfItIs(constTextClose);
        if WasWarning() then ProcessError();
      end;
    end;

    if not WasWarning() and not WasError() then
    begin
      FResult := Res;
    end else
    begin
      FResult := '';
      FPosition := OldPos;
    end;
  end;


  procedure TLexemsReader.ReadValue();
  begin
    ReadString();
    if WasWarning()
    then ReadText();
  end;


  procedure TLexemsReader.ReadName();
  var
    TempPos : Integer;
    Successful, WasMarker : Boolean;
  begin
    PassSeparators();
    FResult := '';
    Successful := False;
    WasMarker := False;
    TempPos := FPosition;
    if (TempPos <= Length(FText)) and (FText[TempPos] = constNameMarker) then
    begin
      WasMarker := True;
      Inc(TempPos);
      while (TempPos <= Length(FText)) and
            (FText[TempPos] <> constNameMarker) and
            (FText[TempPos] <> #10) and
            (FText[TempPos] <> #13)
      do
      begin
        FResult := FResult + FText[TempPos];
        Inc(TempPos);
      end;
      if (TempPos <= Length(FText)) and (FText[TempPos] = constNameMarker)
      then Successful := True;
    end;

    if Successful then
    begin
      Inc(TempPos);
      FPosition := TempPos;
      ClearWarnings();
    end;

    if not Successful and not WasMarker then
    begin
      FResult := '';
      AddWarning(Prefix() + constNameReadingError);
    end;

    if not Successful and WasMarker then
    begin
      FResult := '';
      AddWarning(Prefix() +
                 constMark + ' " ' + constNameMarker + ' " ' + constNotFound1);
      ProcessError();
    end;
  end;


  procedure TLexemsReader.ReadMark();
  begin
    PassSeparators();
    FResult := '';
    if (FPosition <= Length(FText)) and (FText[FPosition] in FMarks) then
    begin
      FResult := FText[FPosition];
      Inc(FPosition);
      ClearWarnings();
    end
    else AddWarning(Prefix() + constMarkReadingError);
  end;


  procedure TLexemsReader.ReadKeyWordIfItIs(KeyWord : String);
  var
    TempPos : Integer;
  begin
    PassSeparators();
    FResult := '';
    TempPos := FPosition;
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      FResult := FResult + FText[TempPos];
      Inc(TempPos);
    end;
    if AreStringsEqual(FResult, KeyWord) then
    begin
      ClearWarnings();
      FPosition := TempPos;
    end else
    begin
      FResult := '';
      AddWarning(Prefix() +
                 constKeyWord + ' "' + KeyWord + '" ' + constNotFound3);
    end;
  end;


  procedure TLexemsReader.ReadMarkIfItIs(Mark : Char);
  begin
    PassSeparators();
    FResult := '';
    if (FPosition <= Length(FText)) and (FText[FPosition] = Mark) then
    begin
      FResult := FText[FPosition];
      Inc(FPosition);
      ClearWarnings();
    end
    else AddWarning(Prefix() +
                    constMark + ' "' + Mark + '" ' + constNotFound1);
  end;

{
  function TLexemsReader.NextKeyWordIs(KeyWord : String) : Boolean;
  var
    TempPos : Integer;
    TempRes : String;
  begin
    PassSeparators();
    TempPos := FPosition;
    FResult := '';
    TempRes := '';
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      TempRes := TempRes + FText[TempPos];
      Inc(TempPos);
    end;
    Result := AreStringsEqual(TempRes, KeyWord);
  end;


  function TLexemsReader.NextLexemeIsIdentifier() : Boolean;
  var
    TempPos : Integer;
    TempRes : String;
  begin
    PassSeparators();
    TempRes := '';
    TempPos := FPosition;
    while (TempPos <= Length(FText)) and (FText[TempPos] in constIdChars)
    do
    begin
      TempRes := TempRes + FText[TempPos];
      Inc(TempPos);
    end;
    Result := (TempRes <> '') and not IsKeyWord(TempRes);
  end;
}

  function TLexemsReader.IsKeyWord(Str : String) : Boolean;
  var
    i : Integer;
  begin
    i := 0;
    Result := False;
    while (i <= High(FKeyWords)) and not Result do
    begin
      Result := AreStringsEqual(Str, FKeyWords[i]);
      Inc(i);
    end;
  end;


  function TLexemsReader.EndOfText : Boolean;
  begin
    PassSeparators();
    Result := FPosition > Length(FText);
  end;



  procedure Register();
  begin
    RegisterComponents('A-Tata', [TLexemsReader]);
  end;

end.
