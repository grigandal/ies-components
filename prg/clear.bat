del BaseClasses\*.dcu
del BaseClasses\*.~*
del BaseClasses\*.bpl

del bin\*.~*

del Broker\*.dcu
del Broker\*.~*
del Broker\*.bpl

del Client\*.dcu
del Client\*.~*
del Client\*.bpl

del Dialoger\*.dcu
del Dialoger\*.~*
del Dialoger\*.bpl

del DialogerComponents\*.dcu
del DialogerComponents\*.~*
del DialogerComponents\*.bpl

del DialogerComponentsKernels\*.dcu
del DialogerComponentsKernels\*.~*
del DialogerComponentsKernels\*.bpl

del DSDLEditor\*.dcu
del DSDLEditor\*.~*
del DSDLEditor\*.bpl

del DSDLEditorComponents\*.dcu
del DSDLEditorComponents\*.~*
del DSDLEditorComponents\*.bpl

del DSDLInterpreterKernel\*.dcu
del DSDLInterpreterKernel\*.~*
del DSDLInterpreterKernel\*.bpl

del LexemsReader\*.dcu
del LexemsReader\*.~*
del LexemsReader\*.bpl

del Starter\*.dcu
del Starter\*.~*
del Starter\*.bpl