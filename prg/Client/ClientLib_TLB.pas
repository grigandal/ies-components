unit ClientLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 30.03.02 14:39:36 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\WORK\MEPHI\Dialoger\prg\Client\ClientLib.tlb (1)
// LIBID: {82B33840-43EA-11D6-A03E-F65848728159}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\SYSTEM\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINDOWS\SYSTEM\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ClientLibMajorVersion = 1;
  ClientLibMinorVersion = 0;

  LIBID_ClientLib: TGUID = '{82B33840-43EA-11D6-A03E-F65848728159}';

  IID_IClient: TGUID = '{82B33841-43EA-11D6-A03E-F65848728159}';
  CLASS_Client: TGUID = '{82B33843-43EA-11D6-A03E-F65848728159}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IClient = interface;
  IClientDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Client = IClient;


// *********************************************************************//
// Interface: IClient
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {82B33841-43EA-11D6-A03E-F65848728159}
// *********************************************************************//
  IClient = interface(IDispatch)
    ['{82B33841-43EA-11D6-A03E-F65848728159}']
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function  Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SndrName: WideString; const MsgText: WideString; 
                             out Output: OleVariant); safecall;
    procedure Stop; safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IClientDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {82B33841-43EA-11D6-A03E-F65848728159}
// *********************************************************************//
  IClientDisp = dispinterface
    ['{82B33841-43EA-11D6-A03E-F65848728159}']
    property Name: WideString dispid 1;
    property Broker: OleVariant dispid 2;
    procedure Configurate(const Config: WideString); dispid 3;
    procedure ProcessMessage(const SndrName: WideString; const MsgText: WideString; 
                             out Output: OleVariant); dispid 4;
    procedure Stop; dispid 5;
  end;

// *********************************************************************//
// The Class CoClient provides a Create and CreateRemote method to          
// create instances of the default interface IClient exposed by              
// the CoClass Client. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoClient = class
    class function Create: IClient;
    class function CreateRemote(const MachineName: string): IClient;
  end;

implementation

uses ComObj;

class function CoClient.Create: IClient;
begin
  Result := CreateComObject(CLASS_Client) as IClient;
end;

class function CoClient.CreateRemote(const MachineName: string): IClient;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Client) as IClient;
end;

end.
