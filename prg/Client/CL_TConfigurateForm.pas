unit CL_TConfigurateForm;

interface

  uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, ComObj, StdCtrls;

  type
    TConfigurateForm = class(TForm)
    CloseButton: TButton;
    ConfigMemo: TMemo;
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
      private
      { Private declarations }
        FClient : TAutoObject;
      public
      { Public declarations }
        property Client : TAutoObject read FClient write FClient;
    end;


implementation

{$R *.dfm}

  uses
    CL_TClient;


  procedure TConfigurateForm.FormActivate(Sender: TObject);
  begin
    Caption := '������ ' + (Client as TClient).ObjName + '.����������������';
  end;

  
  procedure TConfigurateForm.CloseButtonClick(Sender: TObject);
  begin
    Close();
  end;

end.
