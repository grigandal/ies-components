unit CL_TProcessMessageForm;

interface

  uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
    Dialogs, StdCtrls, ComObj, ExtCtrls, Buttons;

  type
    TProcessMessageForm = class(TForm)
      GroupBox1: TGroupBox;
      ReceivedMessageMemo: TMemo;
      SenderNameEdit: TEdit;
      Label1: TLabel;
      Label3: TLabel;
      GroupBox2: TGroupBox;
      Label2: TLabel;
      Label4: TLabel;
      SendingMessageMemo: TMemo;
      ReceiverNameEdit: TEdit;
      GroupBox3: TGroupBox;
      ObjectNameEdit: TEdit;
      Label5: TLabel;
      StopTimer: TTimer;
      GroupBox4: TGroupBox;
      Label6: TLabel;
      ErrorTextMemo: TMemo;
    ProcessErrorSpeedButton: TSpeedButton;
    SendSpeedButton: TSpeedButton;
    SendToAllSpeedButton: TSpeedButton;
    SetAnableTimer: TTimer;
    CloseSpeedButton: TSpeedButton;
    StopSpeedButton: TSpeedButton;
    StopAllSpeedButton: TSpeedButton;
      procedure CloseButtonClick(Sender: TObject);
      procedure SendButtonClick(Sender: TObject);
      procedure SendToAllButtonClick(Sender: TObject);
      procedure StopButtonClick(Sender: TObject);
      procedure StopAllButtonClick(Sender: TObject);
      procedure StopTimerTimer(Sender: TObject);
      procedure FormActivate(Sender: TObject);
      procedure ProcessErrorButtonClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetAnableTimerTimer(Sender: TObject);

      private
        { Private declarations }
        FClient : TAutoObject;

      public
        { Public declarations }
        property Client : TAutoObject read FClient write FClient;
    end;


implementation

{$R *.dfm}

  uses
    CL_TClient;


  procedure TProcessMessageForm.FormActivate(Sender: TObject);
  begin
    Caption := '������ ' + (Client as TClient).ObjName + '.��������� ���������';
  end;


  procedure TProcessMessageForm.CloseButtonClick(Sender: TObject);
  begin
    Close();
  end;


  procedure TProcessMessageForm.SendButtonClick(Sender: TObject);
  var
    Output : OleVariant;
  begin
    (Client as TClient).Broker.SendMessage((Client as TClient).ObjName,
                                           ReceiverNameEdit.Text,
                                           SendingMessageMemo.Lines.Text,
                                           Output);
  end;


  procedure TProcessMessageForm.SendToAllButtonClick(Sender: TObject);
  begin
    (Client as TClient).Broker.SendMessageToAll((Client as TClient).ObjName,
                                                SendingMessageMemo.Lines.Text);
  end;


  procedure TProcessMessageForm.StopButtonClick(Sender: TObject);
  begin
    (Client as TClient).Broker.StopObject(ObjectNameEdit.Text);
  end;


  procedure TProcessMessageForm.StopAllButtonClick(Sender: TObject);
  begin
    (Client as TClient).Broker.StopAll;
  end;


  procedure TProcessMessageForm.StopTimerTimer(Sender: TObject);
  begin
    if (Client as TClient).Stopped then Close();
  end;


  procedure TProcessMessageForm.ProcessErrorButtonClick(Sender: TObject);
  begin
    (Client as TClient).Broker.ProcessError(ErrorTextMemo.Lines.Text);
  end;


  procedure TProcessMessageForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  begin
    if Key = 27 then CloseButtonClick(Self);
  end;


  procedure TProcessMessageForm.SetAnableTimerTimer(Sender: TObject);
  begin
    //
    ProcessErrorSpeedButton.Enabled := ErrorTextMemo.Lines.Text <> '';
    SendSpeedButton.Enabled := (SendingMessageMemo.Lines.Text <> '') and (ReceiverNameEdit.Text <> '');
    SendToAllSpeedButton.Enabled := SendingMessageMemo.Lines.Text <> '';
    StopSpeedButton.Enabled := ObjectNameEdit.Text <> '';
  end;

end.
