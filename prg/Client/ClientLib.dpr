library ClientLib;

uses
  ComServ,
  ClientLib_TLB in 'ClientLib_TLB.pas',
  CL_TClient in 'CL_TClient.pas' {Client: CoClass},
  CL_TProcessMessageForm in 'CL_TProcessMessageForm.pas' {ProcessMessageForm},
  CL_TConfigurateForm in 'CL_TConfigurateForm.pas' {ConfigurateForm};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
