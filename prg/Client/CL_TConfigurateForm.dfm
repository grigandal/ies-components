object ConfigurateForm: TConfigurateForm
  Left = 282
  Top = 257
  BorderStyle = bsToolWindow
  Caption = 'ConfigurateForm'
  ClientHeight = 218
  ClientWidth = 443
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  DesignSize = (
    443
    218)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 73
    Height = 13
    Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103
  end
  object CloseButton: TButton
    Left = 352
    Top = 189
    Width = 88
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 0
    OnClick = CloseButtonClick
  end
  object ConfigMemo: TMemo
    Left = 0
    Top = 16
    Width = 443
    Height = 166
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
