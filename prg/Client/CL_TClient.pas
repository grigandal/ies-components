unit CL_TClient;

{$WARN SYMBOL_PLATFORM OFF}

interface

  uses
    ComObj,
    ActiveX,
    ClientLib_TLB,
    StdVcl;

  type
    TClient = class(TAutoObject, IClient)
      private
        FObjName : WideString;
        FBroker : OleVariant;
        FStopped : Boolean;

      protected
        function Get_Name() : WideString; safecall;
        procedure Set_Name(const Value : WideString); safecall;
        function Get_Broker() : OleVariant; safecall;
        procedure Set_Broker(Value : OleVariant); safecall;
        procedure Configurate(const Config : WideString); safecall;
        procedure ProcessMessage(const SndrName, MsgText : WideString; out Output : OleVariant); safecall;
        procedure Stop(); safecall;
        { Protected declarations }

      public
        property Stopped : Boolean read FStopped write FStopped;
        property Broker : OleVariant read FBroker write FBroker;
        property ObjName : WideString read FObjName write FObjName;
    end;


implementation

  uses
    ComServ,
    CL_TProcessMessageForm,
    CL_TConfigurateForm;


  function TClient.Get_Name() : WideString;
  begin
    Result := ObjName;
  end;


  procedure TClient.Set_Name(const Value : WideString);
  begin
    ObjName := Value;
  end;


  function TClient.Get_Broker() : OleVariant;
  begin
    Result := Broker;
  end;


  procedure TClient.Set_Broker(Value : OleVariant);
  begin
    Broker := Value;
  end;


  procedure TClient.Configurate(const Config : WideString);
//  var
//    CfgForm : TConfigurateForm;
  begin
    {
    CfgForm := TConfigurateForm.Create(nil);

    CfgForm.Client := Self;
    CfgForm.ConfigMemo.Lines.Text := Config;
    CfgForm.ShowModal();

    CfgForm.Free();
    }
  end;


  procedure TClient.ProcessMessage(const SndrName, MsgText : WideString; out Output : OleVariant);
  var
    ProcMsgForm : TProcessMessageForm;
  begin
    Stopped := False;

    ProcMsgForm := TProcessMessageForm.Create(nil);

    ProcMsgForm.Client := Self;
    ProcMsgForm.SenderNameEdit.Text := SndrName;
    ProcMsgForm.ReceivedMessageMemo.Lines.Text := MsgText;

    ProcMsgForm.ShowModal();

    ProcMsgForm.Free();
  end;


  procedure TClient.Stop();
  begin
    Stopped := True;
  end;


initialization

  TAutoObjectFactory.Create(ComServer, TClient, Class_Client, ciMultiInstance, tmApartment);

end.
