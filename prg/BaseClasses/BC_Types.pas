unit BC_Types;

interface

  type
    TOnError = procedure () of object;
    TOnWarning  = procedure (Description : String) of object;
    TOnClearWarnings = procedure () of object;
    TOnCheckWarning = procedure (var WasWarning : Boolean) of object;
    TOnCheckError = procedure (var WasError : Boolean) of object;

implementation

end.

