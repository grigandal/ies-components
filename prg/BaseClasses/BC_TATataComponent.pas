unit BC_TATataComponent;

interface
  uses
    Classes,
    BC_Types;

  type
    TATataComponent = class(TComponent)
      private
        FOnError : TOnError;
        FOnWarning : TOnWarning;
        FOnClearWarnings : TOnClearWarnings;
        FOnCheckWarning : TOnCheckWarning;
        FOnCheckError : TOnCheckError;

      protected
        procedure SetOnError(Value : TOnError); virtual;
        procedure SetOnWarning(Value : TOnWarning); virtual;
        procedure SetOnClearWarnings(Value : TOnClearWarnings); virtual;
        procedure SetOnCheckWarning(Value : TOnCheckWarning); virtual;
        procedure SetOnCheckError(Value : TOnCheckError); virtual;

      public
        constructor Create(AOwner : TComponent); override;

        procedure ProcessError(); overload; virtual;
        procedure ProcessError(Description : String); overload; virtual;
        procedure AddWarning(Description : String); virtual;
        procedure ClearWarnings(); virtual;
        function WasWarning() : Boolean; virtual;
        function WasError() : Boolean; virtual;

      published
        property OnError : TOnError read FOnError write SetOnError;
        property OnWarning : TOnWarning read FOnWarning write SetOnWarning;
        property OnClearWarnings : TOnClearWarnings read FOnClearWarnings write SetOnClearWarnings;
        property OnCheckWarning : TOnCheckWarning read FOnCheckWarning write SetOnCheckWarning;
        property OnCheckError : TOnCheckError read FOnCheckError write SetOnCheckError;
    end;


  procedure Register();


implementation

  constructor TATataComponent.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    if AOwner is TATataComponent then
    begin
      FOnClearWarnings := (AOwner as TATataComponent).OnClearWarnings;
      FOnWarning := (AOwner as TATataComponent).OnWarning;
      FOnError := (AOwner as TATataComponent).OnError;
      FOnCheckWarning := (AOwner as TATataComponent).OnCheckWarning;
      FOnCheckError := (AOwner as TATataComponent).OnCheckError;
    end else
    begin
      FOnClearWarnings := nil;
      FOnWarning := nil;
      FOnError := nil;
      FOnCheckWarning := nil;
      FOnCheckError := nil;
    end;
  end;


  procedure TATataComponent.SetOnError(Value : TOnError);
  var
    i : Integer;
  begin
    FOnError := Value;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TATataComponent
      then (Components[i] as TATataComponent).OnError := Value;
  end;


  procedure TATataComponent.SetOnWarning(Value : TOnWarning);
  var
    i : Integer;
  begin
    FOnWarning := Value;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TATataComponent
      then (Components[i] as TATataComponent).OnWarning := Value;
  end;


  procedure TATataComponent.SetOnClearWarnings(Value : TOnClearWarnings);
  var
    i : Integer;
  begin
    FOnClearWarnings := Value;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TATataComponent
      then (Components[i] as TATataComponent).OnClearWarnings := Value;
  end;


  procedure TATataComponent.SetOnCheckWarning(Value : TOnCheckWarning);
  var
    i : Integer;
  begin
    FOnCheckWarning := Value;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TATataComponent
      then (Components[i] as TATataComponent).OnCheckWarning := Value;
  end;


  procedure TATataComponent.SetOnCheckError(Value : TOnCheckError);
  var
    i : Integer;
  begin
    FOnCheckError := Value;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TATataComponent
      then (Components[i] as TATataComponent).OnCheckError := Value;
  end;


  procedure TATataComponent.ProcessError();
  begin
    OnError();
  end;


  procedure TATataComponent.ProcessError(Description : String);
  begin
    ClearWarnings();
    AddWarning(Description);
    OnError();
  end;


  procedure TATataComponent.AddWarning(Description : String);
  begin
    OnWarning(Description);
  end;


  procedure TATataComponent.ClearWarnings();
  begin
    OnClearWarnings();
  end;


  function TATataComponent.WasWarning() : Boolean;
  begin
    OnCheckWarning(Result);
  end;


  function TATataComponent.WasError() : Boolean;
  begin
    OnCheckError(Result);
  end;



  procedure Register();
  begin
    RegisterClasses([TATataComponent]);
  end;

end.
