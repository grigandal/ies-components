unit SL_Constants;

interface

  const
    ConfigNode = 'config';
    ObjectNode = 'object';
    MessageNode = 'message';

    ObjNameAttr = 'Name';
    ClassNameAttr = 'Class';
    RcvrNameAttr = 'RcvrName';

    Points = '..........' + '..........' + '..........' + '..........' + '..........' +
             '..........' + '..........' + '..........' + '..........' + '..........';
    LineLen = 50;
    InProcessMsg = '??';
    FinishedMsg = 'OK';

    BBInitTxt='<bb />';

implementation

end.
