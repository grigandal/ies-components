library StarterLib;

uses
  ComServ,
  StarterLib_TLB in 'StarterLib_TLB.pas',
  SL_TStarter in 'SL_TStarter.pas' {Starter: CoClass},
  SL_Constants in 'SL_Constants.pas',
  SL_Functions in 'SL_Functions.pas',
  SL_TMonitor in 'SL_TMonitor.pas' {Monitor};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
