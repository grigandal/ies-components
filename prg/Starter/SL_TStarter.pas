unit SL_TStarter;

{$WARN SYMBOL_PLATFORM OFF}

interface

  uses
    ComObj,
    ActiveX,
    StarterLib_TLB,
    StdVcl,
    SysUtils,
    XMLDoc,
    XMLIntf;


  type
    TStarter = class(TAutoObject, IStarter)
      private
        ConfigXMLDoc : IXMLDocument;

      protected
        function Get_Config() : WideString; safecall;
        procedure Set_Config(const Value : WideString); safecall;
        function Get_FileName() : WideString; safecall;
        procedure Set_FileName(const Value : WideString); safecall;
        procedure Start(); safecall;
       { Protected declarations }

      public
        procedure Initialize(); override;
        destructor Destroy(); override;
    end;


implementation

  uses
    ComServ,
    Controls,
    Forms,
    Variants,
    SL_Constants,
    SL_Functions,
    SL_TMonitor;


  procedure TStarter.Initialize();
  begin
    inherited Initialize();     
    ConfigXMLDoc := TXMLDocument.Create(nil);
    ConfigXMLDoc.FileName := '';
  end;


  destructor TStarter.Destroy();
  begin
    inherited Destroy();
  end;


  function TStarter.Get_Config() : WideString;
  begin
    Result := ConfigXMLDoc.XML.Text;
  end;


  procedure TStarter.Set_Config(const Value : WideString);
  begin
    ConfigXMLDoc.LoadFromXML(Value);
  end;


  function TStarter.Get_FileName() : WideString;
  begin
    Result := ConfigXMLDoc.FileName;
  end;


  procedure TStarter.Set_FileName(const Value : WideString);
  begin
    ConfigXMLDoc.FileName := Value;
    ConfigXMLDoc.Active := True;
  end;


  procedure TStarter.Start();
  var
    OldCursor : TCursor;
    Broker : OleVariant;
    i : Integer;
    TempOutput : OleVariant;
    Config : IXMLNode;
    Monitor : TMonitor;
  begin
    if ConfigXMLDoc.Active then
    begin
      OldCursor := Screen.Cursor;
      Screen.Cursor := crHourGlass;
      Application.ProcessMessages();

      Monitor := TMonitor.Create(nil);
      Monitor.Show();

      Broker := CreateOleObject('BrokerLib.Broker');

      Config := ConfigXMLDoc.DocumentElement;

      Broker.BlackBoard := CreateOleObject('board.Blackboard');

      // by Demidov D.V. to make components know their config files' path
      // �� ��������� - ���� �� exe, �� ���� ����� config.xml, �� ��� ����
      if ConfigXMLDoc.FileName <> '' then
        Broker.Path := ExtractFilePath(ConfigXMLDoc.FileName)
      else
        Broker.Path := ExtractFilePath(Application.ExeName);

      //Registering
      for i := 0 to Config.ChildNodes.Count - 1 do
        if Equals(Config.ChildNodes[i].NodeName, ObjectNode) then
        begin
          Monitor.AddLine('�������� ������� ' + Config.ChildNodes[i].Attributes[ObjNameAttr]);
          Broker.RegisterObject(Config.ChildNodes[i].Attributes[ObjNameAttr],
                                Config.ChildNodes[i].Attributes[ClassNameAttr]);
        end;

      //Configurating
      for i := 0 to Config.ChildNodes.Count - 1 do
        if Equals(Config.ChildNodes[i].NodeName, ObjectNode) then
        begin
          Monitor.AddLine('���������������� ������� ' + Config.ChildNodes[i].Attributes[ObjNameAttr]);
          Broker.ConfigurateObject(Config.ChildNodes[i].Attributes[ObjNameAttr],
                                   Config.ChildNodes[i].ChildNodes[ConfigNode].XML);
        end;

      Monitor.Close();
      Monitor.Free();

      Screen.Cursor := OldCursor;
      Application.ProcessMessages();

      //Start message sending
      Broker.SendMessage('',
                         Config.ChildNodes[MessageNode].Attributes[RcvrNameAttr],
                         Config.ChildNodes[MessageNode].ChildNodes[0].XML,
                         TempOutput);

      Broker.BlackBoard := Unassigned;

      Broker.UnregisterAll;
      Broker := Unassigned;
    end;
  end;


initialization

  TAutoObjectFactory.Create(ComServer, TStarter, Class_Starter, ciMultiInstance, tmApartment);

end.
