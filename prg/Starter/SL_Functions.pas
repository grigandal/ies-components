unit SL_Functions;

interface

  function Equals(Str1, Str2 : String) : Boolean;


implementation
  uses
    SysUtils;


  function UppercaseString(Str : String) : String;
  var
    i : Integer;
  begin
    Result := '';
    for i := 1 to Length(Str) do
      case Str[i] of
        'a'..'z' : Result := Result + UpperCase(Str[i]);
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        '�' : Result := Result + '�';
        else Result := Result + Str[i];
      end;
  end;


  function Equals(Str1, Str2 : String) : Boolean;
  begin
    Result := UppercaseString(Str1) = UppercaseString(Str2);
  end;

end.
