unit SL_TMonitor;

interface

  uses
    Windows,
    Messages,
    SysUtils,
    Variants,
    Classes,
    Graphics,
    Controls,
    Forms,
    Dialogs,
    StdCtrls, ExtCtrls, ComCtrls;

  type
    TMonitor = class(TForm)
      StatusMemo: TMemo;
      private

      public
        procedure AddLine(Line : String);
    end;


implementation

{$R *.dfm}

  uses
    SL_Constants;
    

  procedure TMonitor.AddLine(Line : String);
  begin
    if StatusMemo.Lines.Count > 0
    then StatusMemo.Lines[StatusMemo.Lines.Count - 1] := Copy(StatusMemo.Lines[StatusMemo.Lines.Count - 1], 1, LineLen) + FinishedMsg;
    StatusMemo.Lines.Add(Copy(Line, 1, LineLen - 2) + Copy(Points, 1, LineLen - Length(Line) - 2) + Copy(Points, 1, 2) + InProcessMsg);
  end;

end.
