unit StarterLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 02.04.02 14:47:53 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Saruman\Aspirantura\at\at4\Source\Starter\StarterLib.tlb (1)
// LIBID: {6B399948-43CA-11D6-A03E-8B174D38B059}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  StarterLibMajorVersion = 1;
  StarterLibMinorVersion = 0;

  LIBID_StarterLib: TGUID = '{6B399948-43CA-11D6-A03E-8B174D38B059}';

  IID_IStarter: TGUID = '{6B399949-43CA-11D6-A03E-8B174D38B059}';
  CLASS_Starter: TGUID = '{6B39994B-43CA-11D6-A03E-8B174D38B059}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IStarter = interface;
  IStarterDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Starter = IStarter;


// *********************************************************************//
// Interface: IStarter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6B399949-43CA-11D6-A03E-8B174D38B059}
// *********************************************************************//
  IStarter = interface(IDispatch)
    ['{6B399949-43CA-11D6-A03E-8B174D38B059}']
    function  Get_Config: WideString; safecall;
    procedure Set_Config(const Value: WideString); safecall;
    function  Get_FileName: WideString; safecall;
    procedure Set_FileName(const Value: WideString); safecall;
    procedure Start; safecall;
    property Config: WideString read Get_Config write Set_Config;
    property FileName: WideString read Get_FileName write Set_FileName;
  end;

// *********************************************************************//
// DispIntf:  IStarterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6B399949-43CA-11D6-A03E-8B174D38B059}
// *********************************************************************//
  IStarterDisp = dispinterface
    ['{6B399949-43CA-11D6-A03E-8B174D38B059}']
    property Config: WideString dispid 1;
    property FileName: WideString dispid 2;
    procedure Start; dispid 3;
  end;

// *********************************************************************//
// The Class CoStarter provides a Create and CreateRemote method to          
// create instances of the default interface IStarter exposed by              
// the CoClass Starter. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoStarter = class
    class function Create: IStarter;
    class function CreateRemote(const MachineName: string): IStarter;
  end;

implementation

uses ComObj;

class function CoStarter.Create: IStarter;
begin
  Result := CreateComObject(CLASS_Starter) as IStarter;
end;

class function CoStarter.CreateRemote(const MachineName: string): IStarter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Starter) as IStarter;
end;

end.

