unit DSDLIK_TScenario;

interface
  uses
    Classes,
    DSDLIK_TItem,
    DSDLIK_TItems;

  type
    TScenario = class(TItem)
      private
      protected
        Operators : TItems;
        CurrentOperatorIndex : Integer;
        FName : String;
      public
        property Name : String read FName;
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure DoReading();override;
        procedure ReadHeader();virtual;abstract;
        procedure ReadBody();virtual;
        procedure Execute();
        procedure JumpToLabel(Name, LineNumberInformation : String);
    end;

implementation
  uses
    LR_Functions,
    DSDLIK_Constants,
    DSDLIK_TOperator,
    DSDLIK_TLabel;


  constructor TScenario.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Operators := TItems.Create();
    FName := '';
    CurrentOperatorIndex := 0;
  end;


  destructor TScenario.Destroy();
  begin
    Operators.Destroy();
    inherited Destroy();
  end;


  procedure TScenario.DoReading();
  begin
    FName := '';
    Operators.Clear();
    ReadHeader();
    if not WasWarning() and not WasError() then ReadBody();
    if not WasWarning() and not WasError() then ConfirmReading();
  end;


  procedure TScenario.ReadBody();
  var
    Operator : TOperator;
  begin
    LexemsReader.PassOperatorsSeparators();
    while not WasWarning() and not WasError() do
    begin
      Operator := ReadOperator(Self);
      if not WasWarning() and not WasError() then
      begin
        Operators.Add(Operator);
        LexemsReader.PassOperatorsSeparators();
      end;
    end;

    if not WasError() then
    begin
      LexemsReader.ReadKeyWordIfItIs(kwEnd);
      if WasWarning() then ProcessError();
    end;
  end;


  procedure TScenario.Execute();
  begin
    CurrentOperatorIndex := 0;
    while (CurrentOperatorIndex < Operators.Count()) and CanContinue() do
    begin
      (Operators.Item(CurrentOperatorIndex) as TOperator).Execute();
      Inc(CurrentOperatorIndex);
    end;
  end;


  procedure TScenario.JumpToLabel(Name, LineNumberInformation : String);
  var
    i : Integer;
    Found : Boolean;
  begin
    i := 0;
    Found := False;
    while (i < Operators.Count()) and not Found do
    begin
      Found := (Operators.Item(i) is TLabel) and
               AreStringsEqual((Operators.Item(i) as TLabel).Name, Name);
      Inc(i);
    end;

    if Found then CurrentOperatorIndex := i - 1 else
    begin
      AddWarning(LineNumberInformation + constLabel +
                 ' "' + Name + '" ' + constNotFound2);
      ProcessError();
    end;
  end;

end.
