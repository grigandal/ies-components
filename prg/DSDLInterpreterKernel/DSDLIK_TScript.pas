unit DSDLIK_TScript;

interface
  uses
    Classes,
    DSDLIK_TScenario;

  type
    TScript = class(TScenario)
      public
        procedure ReadHeader(); override;
        procedure ReadBody(); override;
    end;


implementation
  uses
    DSDLIK_TOperator;


  procedure TScript.ReadHeader();
  begin
    FName := '';
    ClearWarnings();
  end;

  
  procedure TScript.ReadBody();
  var
    Operator : TOperator;
  begin
    LexemsReader.PassOperatorsSeparators();
    while not WasWarning() and not WasError() do
    begin
      Operator := ReadOperator(Self);
      if not WasWarning() and not WasError() then
      begin
        Operators.Add(Operator);
        LexemsReader.PassOperatorsSeparators();
      end;
    end;

    if WasWarning() and LexemsReader.EndOfText() then ClearWarnings();
  end;

end.
