unit DSDLIK_TAssignment;

interface
  uses
    Classes,
    DSDLIK_TExpression,
    DSDLIK_TOperator;

  type
    TAssignment = class(TOperator)
      private
        Expression : TExpression;
        AttributeName : String;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TDSDLInterpreterKernel;


  constructor TAssignment.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Expression := nil;
    AttributeName := '';
  end;


  destructor TAssignment.Destroy();
  begin
    if Expression <> nil then Expression.Destroy();
    inherited Destroy();
  end;


  procedure TAssignment.DoReading();
  begin
    AttributeName := '';
    if Expression <> nil then Expression.Destroy();
    Expression := nil;
    LexemsReader.ReadKeyWordIfItIs(kwSet);
    if not WasWarning() then
    begin
      LexemsReader.ReadName();
      if WasWarning() then ProcessError();

      AttributeName := LexemsReader.ReadingResult();

      if not WasError() then
      begin
        LexemsReader.ReadKeyWordIfItIs(kwTo);
        if WasWarning() then ProcessError();
      end;

      if not WasError() then
      begin
        Expression := ReadExpression(Self);
        if not WasWarning() and not WasError()
        then ConfirmReading();
        if WasWarning() then ProcessError();
      end;
    end;
  end;


  procedure TAssignment.Execute();
  var
    ExprValue : String;
  begin
    ExprValue := Expression.Value();
    if CanContinue() then
    begin
      if Pos(constPropSeparator, AttributeName) = 0
      then (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSetAttrValue(AttributeName, ExprValue)
      else (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSetAttrProp
             (
               Copy(AttributeName, 1, Pos(constPropSeparator, AttributeName) - 1),
               Copy(AttributeName, Pos(constPropSeparator, AttributeName) + 1, Length(AttributeName)),
               ExprValue
             );
    end;
  end;

end.
