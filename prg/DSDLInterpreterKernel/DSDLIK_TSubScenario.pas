unit DSDLIK_TSubScenario;

interface
  uses
    DSDLIK_TScenario;

  type
    TSubScenario = class(TScenario)
      private
      public
        procedure ReadHeader();override;
    end;

implementation
  uses
    DSDLIK_Constants;


  procedure TSubScenario.ReadHeader();
  begin
    LexemsReader.ReadKeyWordIfItIs(kwSubscenario);
    if not WasWarning() then
    begin
      LexemsReader.ReadIdentifier(ikSubscenarioName, False);
      if not WasWarning()
      then FName := LexemsReader.ReadingResult()
      else ProcessError();
    end;
  end;

end.
