unit DSDLIK_TJump;

interface
  uses
    DSDLIK_TOperator;

  type
    TJump = class(TOperator)
      private
        LabelName : String;
      public
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation
  uses
    DSDLIK_TScenario,
    DSDLIK_Constants;


  procedure TJump.DoReading();
  begin
    LabelName := '';
    LexemsReader.ReadKeyWordIfItIs(kwGoto);
    if not WasWarning() then
    begin
      LexemsReader.ReadIdentifier(ikLabelName, False);
      if WasWarning() then ProcessError();
      if not WasError() then
      begin
         LabelName := LexemsReader.ReadingResult();
         ConfirmReading();
      end;
    end;
  end;


  procedure TJump.Execute();
  begin
    (OwnerByClass(TScenario) as TScenario).JumpToLabel(LabelName, LineNumberInformation);
  end;

end.
