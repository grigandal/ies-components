unit DSDLIK_TLabel;

interface
  uses
    DSDLIK_TOperator,
    DSDLIK_Constants;

  type
    TLabel = class(TOperator)
      private
        FName : String;
      public
        property Name : String read FName;
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation

  procedure TLabel.DoReading();
  begin
    FName := '';
    LexemsReader.ReadIdentifier(ikLabelName, False);
    if not WasWarning() then
    begin
      FName := LexemsReader.ReadingResult();
      LexemsReader.ReadMarkIfItIs(':');
      if not WasWarning()
      then ConfirmReading()
      else ProcessError();
    end;
  end;


  procedure TLabel.Execute();
  begin
    // � ��� �� ������, �����-�� ?  :)
  end;

end.
