unit DSDLIK_TItems;

interface
  uses
    Classes,
    DSDLIK_TItem;

  type
    TItems = class
      private
        List : TList;
      public
        constructor Create();
        destructor Destroy();override;

        procedure Add(Item : TItem);
        procedure Clear();
        function Count() : Integer;
        function Item(Index : Integer) : TItem;
    end;

    
implementation

  constructor TItems.Create();
  begin
    inherited Create();
    List := TList.Create();
  end;


  destructor TItems.Destroy();
  begin
    Clear();
    List.Destroy();
    inherited Destroy();
  end;


  procedure TItems.Add(Item : TItem);
  begin
    List.Add(Item);
  end;


  procedure TItems.Clear();
  var
    i : Integer;
  begin
    for i := 0 to List.Count - 1 do TItem(List.Items[i]).Destroy();
    List.Clear();
  end;


  function TItems.Count() : Integer;
  begin
    Result := List.Count;
  end;


  function TItems.Item(Index : Integer) : TItem;
  begin
    if (Index >= 0) and (Index < List.Count)
    then Result := List.Items[Index]
    else Result := nil;
  end;
  
end.
