unit DSDLIK_TExpression;

interface
  uses
    Classes,
    DSDLIK_TItem;

  type
    TExpression = class(TItem)
      function Value() : String;virtual;abstract;
    end;

  function ReadExpression(Owner : TComponent) : TExpression;

implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TValue,
    DSDLIK_TAttributeName,
    DSDLIK_TFunctionCall,
    DSDLIK_TAnswer;


  function ReadExpression(Owner : TComponent) : TExpression;
  begin
    Result := TValue.Create(Owner);
    Result.Read();

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TAttributeName.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TFunctionCall.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TAnswer.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() or Result.WasError() then
    begin
      Result.Destroy();
      Result := nil;
    end;
  end;

end.
