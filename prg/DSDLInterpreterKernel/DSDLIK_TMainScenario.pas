unit DSDLIK_TMainScenario;

interface
  uses
    DSDLIK_TScenario;

  type
    TMainScenario = class(TScenario)
      private
      public
        procedure ReadHeader();override;
    end;

implementation
  uses
    DSDLIK_Constants;


  procedure TMainScenario.ReadHeader();
  begin
    LexemsReader.ReadKeyWordIfItIs(kwScenario);
    if not WasWarning() then
    begin
      LexemsReader.ReadIdentifier(ikScenarioName, False);
      if not WasWarning()
      then FName := LexemsReader.ReadingResult()
      else ProcessError();
    end;
  end;

end.
