unit DSDLIK_TAssignedAttribute;

interface
  uses
    DSDLIK_Types,
    DSDLIK_TItem;

  type
    TAssignedAttribute = class (TItem)
      private
        FName : String;
        FAssigningType : TAssigningType;
      protected
        procedure DoReading();override;
      public
        property Name : String read FName;
        property AssigningType : TAssigningType read FAssigningType;
    end;

    
implementation
  uses
    DSDLIK_Constants;

  procedure TAssignedAttribute.DoReading();
  begin
    FName := '';
    LexemsReader.ReadKeyWordIfItIs(kwAbout);
    if not WasWarning() then
    begin
      LexemsReader.ReadName();
      if not WasWarning() and not WasError() then
      begin
        FName := LexemsReader.ReadingResult();
        FAssigningType := atAbout;
        ConfirmReading();
      end
      else ProcessError();
    end;

    if WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(kwInput);
      if not WasWarning() then
      begin
        LexemsReader.ReadName();
        if not WasWarning() and not WasError() then
        begin
          FName := LexemsReader.ReadingResult();
          FAssigningType := atInput;
          ConfirmReading();
        end
        else ProcessError();
      end;
    end;

    if WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(kwOutput);
      if not WasWarning() then
      begin
        LexemsReader.ReadName();
        if not WasWarning() and not WasError() then
        begin
          FName := LexemsReader.ReadingResult();
          FAssigningType := atOutput;
          ConfirmReading();
        end
        else ProcessError();
      end;
    end;
  end;

end.
