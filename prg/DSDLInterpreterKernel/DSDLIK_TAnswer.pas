unit DSDLIK_TAnswer;

interface
  uses
    DSDLIK_TExpression;

  type
    TAnswer = class(TExpression)
      private
      public
        procedure DoReading();override;
        function Value() : String;override;
    end;

implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TDSDLInterpreterKernel;
    

  procedure TAnswer.DoReading();
  begin
    LexemsReader.ReadKeyWordIfItIs(kwAnswer);
    if not WasWarning() then ConfirmReading();
  end;


  function TAnswer.Value() : String;
  begin
    Result := (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).Answer;
  end;

end.
