unit DSDLIK_TMessage;

interface
  uses
    Classes,
    DSDLIK_Types,
    DSDLIK_TItem,
    DSDLIK_TItems;

  type
    TMessage = class(TItem)
      private
        FName : String;
        FAssignedReceiverName : String;
      protected
        Lines : TItems;
        Attributes : TItems;
        procedure DoReading();override;
      public

        property Name : String read FName;
        property AssignedReceiverName : String read FAssignedReceiverName;

        constructor Create(AOwner : TComponent);override;
        destructor Destroy(); override;
        procedure Send(LineNumberInformation : String);
        procedure SendTo(ReceiverName, LineNumberInformation : String);
        function IsAboutAttribute(AttributeName : String) : Boolean;
        function IsInputAttribute(AttributeName : String) : Boolean;
        function IsOutputAttribute(AttributeName : String) : Boolean;
    end;

implementation
  uses
    LR_Functions,
    DSDLIK_Constants,
    DSDLIK_TAssignedAttribute,
    DSDLIK_TMessageLine;


  constructor TMessage.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    FName := '';
    FAssignedReceiverName := '';
    Lines := TItems.Create();
    Attributes := TItems.Create();
  end;


  destructor TMessage.Destroy();
  begin
    Lines.Destroy();
    Attributes.Destroy();
    inherited Destroy();
  end;


  procedure TMessage.DoReading();
  var
    Attribute : TAssignedAttribute;
    Line : TMessageLine;
  begin
    FName := '';
    FAssignedReceiverName := '';
    Attributes.Clear();
    Lines.Clear();
    LexemsReader.ReadKeyWordIfItIs(kwMessage);
    if not WasWarning() then
    begin
      LexemsReader.ReadIdentifier(ikMessageName, False);
      if WasWarning() then ProcessError();

      FName := LexemsReader.ReadingResult();

      if not WasError() then
      begin
        LexemsReader.ReadKeyWordIfItIs(kwTo);
        if not WasWarning() then
        begin
          LexemsReader.ReadIdentifier(ikReceiverName, False);

          if WasWarning() then LexemsReader.ReadKeyWordIfItIs(kwAll);

          if WasWarning() then ProcessError();
          FAssignedReceiverName := LexemsReader.ReadingResult()
        end
      end;

      if not WasError() then
        repeat
          Attribute := TAssignedAttribute.Create(Self);
          Attribute.Read();
          if not WasWarning() and not WasError()
          then Attributes.Add(Attribute)
          else Attribute.Destroy();
        until WasWarning() or
              WasError();

      LexemsReader.PassOperatorsSeparators();

      if not WasError() then
        repeat
          Line := TMessageLine.Create(Self);
          Line.Read();
          if not WasWarning() and not WasError() then
          begin
            Lines.Add(Line);
            LexemsReader.PassOperatorsSeparators();
          end
          else Line.Destroy();
        until WasWarning() or WasError();

      if not WasError() then
      begin
        LexemsReader.PassOperatorsSeparators();
        LexemsReader.ReadKeyWordIfItIs(kwEnd);
        if not WasWarning() then ConfirmReading() else ProcessError();
      end;
    end;
  end;


  procedure TMessage.Send(LineNumberInformation : String);
  begin
    if AssignedReceiverName <> ''
    then SendTo(AssignedReceiverName, LineNumberInformation) else
    begin
      AddWarning(LineNumberInformation +
                 constReceiver + ' ' + constUnknown1);
      ProcessError();
    end;
  end;


  procedure TMessage.SendTo(ReceiverName, LineNumberInformation : String);
  var
    i : Integer;
  begin
    i := 0;
    while (i < Lines.Count()) and CanContinue() do
    begin
      TMessageLine(Lines.Item(i)).SendTo(ReceiverName);
      Inc(i);
    end;
  end;


  function TMessage.IsAboutAttribute(AttributeName : String) : Boolean;
  var
    i : Integer;
  begin
    i := 0;
    Result := False;
    while (i < Attributes.Count()) and not Result do
    begin
      Result := AreStringsEqual((Attributes.Item(i) as TAssignedAttribute).Name, AttributeName) and
                ((Attributes.Item(i) as TAssignedAttribute).AssigningType = atAbout);
      Inc(i);
    end;
  end;


  function TMessage.IsInputAttribute(AttributeName : String) : Boolean;
  var
    i : Integer;
  begin
    i := 0;
    Result := False;
    while (i < Attributes.Count()) and not Result do
    begin
      Result := AreStringsEqual((Attributes.Item(i) as TAssignedAttribute).Name, AttributeName) and
                ((Attributes.Item(i) as TAssignedAttribute).AssigningType = atInput);
      Inc(i);
    end;
  end;


  function TMessage.IsOutputAttribute(AttributeName : String) : Boolean;
  var
    i : Integer;
  begin
    i := 0;
    Result := False;
    while (i < Attributes.Count()) and not Result do
    begin
      Result := AreStringsEqual((Attributes.Item(i) as TAssignedAttribute).Name, AttributeName) and
                ((Attributes.Item(i) as TAssignedAttribute).AssigningType = atOutput);
      Inc(i);
    end;
  end;

end.
