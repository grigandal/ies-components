unit DSDLIK_TMessageLine;

interface
  uses
    Classes,
    DSDLIK_TItem,
    DSDLIK_TExpression,
    DSDLIK_TDSDLInterpreterKernel;

  type
    TMessageLine = class(TItem)
      private
        Expression : TExpression;
      protected
        procedure DoReading();override;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure SendTo(ReceiverName : String);
    end;

implementation
  uses
    DSDLIK_Constants,
    LR_Functions;


  constructor TMessageLine.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Expression := nil;
  end;


  destructor TMessageLine.Destroy();
  begin
    if Expression <> nil then Expression.Destroy();
    inherited Destroy();
  end;


  procedure TMessageLine.DoReading();
  begin
    if Expression <> nil then Expression.Destroy();
    LexemsReader.ReadKeyWordIfItIs(kwLine);
    if not WasWarning() then
    begin
      Expression := ReadExpression(Self);
      if not WasWarning() and not WasError()
      then ConfirmReading();
      if WasWarning() then ProcessError();
    end;
  end;


  procedure TMessageLine.SendTo(ReceiverName : String);
  var
    ExprValue : String;
  begin
    ExprValue := Expression.Value();
    if CanContinue() then
    begin
      if AreStringsEqual(ReceiverName, kwAll)
      then (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSendMessageToAll(ExprValue)
      else (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSendMessage(ExprValue, ReceiverName);
    end
  end;

end.
