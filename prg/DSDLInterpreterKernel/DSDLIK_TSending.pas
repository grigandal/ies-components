unit DSDLIK_TSending;

interface
  uses
    Classes,
    DSDLIK_TExpression,
    DSDLIK_TOperator;

  type
    TSending = class(TOperator)
      private
        MessageText : TExpression;
        ReceiverName : String;
        MessageName : String;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation
  uses
    LR_Functions,
    DSDLIK_Constants,
    DSDLIK_TDSDLInterpreterKernel;


  constructor TSending.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    MessageText := nil;
    ReceiverName := '';
    MessageName := '';
  end;


  destructor TSending.Destroy();
  begin
    if MessageText <> nil then MessageText.Destroy();
    inherited Destroy();
  end;


  procedure TSending.DoReading();
  begin
    if MessageText <> nil then MessageText.Destroy();
    MessageText := nil;
    ReceiverName := '';
    MessageName := '';
    LexemsReader.ReadKeyWordIfItIs(kwSend);
    if not WasWarning() then
    begin
      MessageText := ReadExpression(Self);

      if WasWarning() then
      begin
        LexemsReader.ReadIdentifier(ikMessageName, False);
        MessageName := LexemsReader.ReadingResult();
      end;

      if WasWarning() then ProcessError();

      if not WasError() then
      begin
        LexemsReader.ReadKeyWordIfItIs(kwTo);
        if not WasWarning() then
        begin
          LexemsReader.ReadIdentifier(ikReceiverName, False);
          if not WasWarning() then
          begin
            ReceiverName := LexemsReader.ReadingResult();
            ConfirmReading();
          end;

          if WasWarning() then
          begin
            LexemsReader.ReadKeyWordIfItIs(kwAll);
            if not WasWarning() then
            begin
              ReceiverName := LexemsReader.ReadingResult();
              ConfirmReading();
            end;
          end;

          if WasWarning() then ProcessError();
        end else
        begin
          if MessageText <> nil then ProcessError() else ConfirmReading();
        end;
      end;
    end;
  end;


  procedure TSending.Execute();
  var
    ExprValue : String;
  begin
    if MessageText <> nil then
    begin
      ExprValue := MessageText.Value();
      if CanContinue() then
      begin
        if AreStringsEqual(ReceiverName, kwAll)
        then (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSendMessageToAll(ExprValue)
        else (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnSendMessage(ExprValue, ReceiverName);
      end
    end else
    begin
      if ReceiverName = ''
      then (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).SendNamedMessage(MessageName, LineNumberInformation)
      else (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).SendNamedMessageTo(MessageName, ReceiverName, LineNumberInformation);
    end;
  end;

end.
