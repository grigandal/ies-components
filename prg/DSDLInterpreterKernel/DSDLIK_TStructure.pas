unit DSDLIK_TStructure;

interface
  uses
    Classes,
    DSDLIK_TItem,
    DSDLIK_TItems,
    DSDLIK_TExpression;

  type
    TStructure = class(TItem)
      private
        Expressions : TItems;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure DoReading();override;
        function Count() : Integer;
        function Expression(Index : Integer) : TExpression;
        procedure Clear();
    end;

implementation
  uses
    DSDLIK_Constants;


  constructor TStructure.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Expressions := TItems.Create();
  end;


  destructor TStructure.Destroy();
  begin
    Expressions.Destroy();
    inherited Destroy();
  end;


  procedure TStructure.DoReading();
  var
    Expression : TExpression;
  begin
    Expressions.Clear();
    LexemsReader.ReadMarkIfItIs('(');
    if not WasWarning() then
    begin
      while not WasWarning() and not WasError() do
      begin
        Expression := ReadExpression(Self);

        if WasWarning() and (Expressions.Count() > 0) then ProcessError();

        if not WasWarning() and not WasError() then Expressions.Add(Expression);

        if not WasError() and (Expressions.Count() > 0) then LexemsReader.ReadMarkIfItIs(',');
      end;

      if not WasError() then
      begin
        LexemsReader.ReadMarkIfItIs(')');
        if WasWarning() then ProcessError();
      end;
    end;

    if not WasWarning() and not WasError() then ConfirmReading();
  end;


  function TStructure.Count() : Integer;
  begin
    Result := Expressions.Count();
  end;


  function TStructure.Expression(Index : Integer) : TExpression;
  begin
    Result := Expressions.Item(Index) as TExpression;
  end;


  procedure TStructure.Clear();
  begin
    Expressions.Clear();
  end;

end.
