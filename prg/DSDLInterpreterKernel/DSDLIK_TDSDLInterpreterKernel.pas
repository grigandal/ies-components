unit DSDLIK_TDSDLInterpreterKernel;

interface
  uses
    Classes,
    BC_Types,
    BC_TATataComponent,
    LR_TLexemsReader,
    DSDLIK_Types,
    DSDLIK_TItems,
    DSDLIK_TScenario,
    DSDLIK_TMainScenario;


  type

    TDSDLInterpreterKernel = class(TATataComponent)
      private
        FOnSendMessage : TOnSendMessage;
        FOnSendMessageToAll : TOnSendMessageToAll;

        FOnStopObject : TOnStopObject;
        FOnStopAll : TOnStopAll;

        FOnSetAttrValue : TOnSetAttrValue;
        FOnGetAttrValue : TOnGetAttrValue;
        FOnSetAttrProp : TOnSetAttrProp;
        FOnGetAttrProp : TOnGetAttrProp;

        FOnErrorWithText : TOnErrorWithText;

        FDSDLText : String;

        FStopped : Boolean;
        FScenarioStopped : Boolean;
        FAnswer : String;

        IsTested : Boolean;

        FWasError : Boolean;
        FWasWarning : Boolean;
        FErrorText : String;

        Messages : TItems;
        MainScenario : TMainScenario;
        SubScenarios : TItems;

        property OnWarning;
        property OnCheckWarning;
        property OnCheckError;

        procedure SetDSDLText(Value : String);

      protected
        procedure SetOnError(Value : TOnError); override;
        procedure SetOnWarning(Value : TOnWarning); override;
        procedure SetOnClearWarnings(Value : TOnClearWarnings); override;
        procedure SetOnCheckWarning(Value : TOnCheckWarning); override;
        procedure SetOnCheckError(Value : TOnCheckError); override;

        procedure Clear();
        procedure Convert();

      public
        LexemsReader : TLexemsReader;

        property DSDLText : String read FDSDLText write SetDSDLText;
        property Stopped : Boolean read FStopped;
        property ScenarioStopped : Boolean read FScenarioStopped;
        property Answer : String read FAnswer write FAnswer;

        constructor Create(AOwner : TComponent); override;
        destructor Destroy(); override;

        procedure Test(TestedDSDLText : String; var ErrorText : String);
        procedure Run();

        procedure ExecuteScenario(ScenarioName : String);
        procedure Execute(SubscenarioName, LineNumberInformation : String); overload;
        procedure Execute(ScriptText : String); overload;
        procedure Stop();
        procedure Break();
        procedure SendNamedMessage(MessageName, LineNumberInformation : String);
        procedure SendNamedMessageTo(MessageName, ReceiverName, LineNumberInformation : String);
        procedure SendMessageAboutAttribute(AttributeName : String; Found : Boolean);
        procedure SendMessageInputAttribute(AttributeName : String; Found : Boolean);
        procedure SendMessageOutputAttribute(AttributeName : String; Found : Boolean);

        procedure HandleProcessError();
        procedure HandleAddWarning(Description : String);
        procedure HandleClearWarnings();
        procedure HandleCheckWarning(var WasWarning : Boolean);
        procedure HandleCheckError(var WasError : Boolean);

      published
        property OnSendMessage : TOnSendMessage
                 read FOnSendMessage
                 write FOnSendMessage;

        property OnSendMessageToAll : TOnSendMessageToAll
                 read FOnSendMessageToAll
                 write FOnSendMessageToAll;

        property OnStopObject : TOnStopObject
                 read FOnStopObject
                 write FOnStopObject;

        property OnStopAll : TOnStopAll
                 read FOnStopAll
                 write FOnStopAll;

        property OnSetAttrValue : TOnSetAttrValue
                 read FOnSetAttrValue
                 write FOnSetAttrValue;

        property OnGetAttrValue : TOnGetAttrValue
                 read FOnGetAttrValue
                 write FOnGetAttrValue;

        property OnSetAttrProp : TOnSetAttrProp
                 read FOnSetAttrProp
                 write FOnSetAttrProp;

        property OnGetAttrProp : TOnGetAttrProp
                 read FOnGetAttrProp
                 write FOnGetAttrProp;

        property OnErrorWithText : TOnErrorWithText
                 read FOnErrorWithText
                 write FOnErrorWithText;
    end;


    procedure Register();


implementation
  uses
    LR_Functions,
    DSDLIK_Constants,
    DSDLIK_TMessage,
    DSDLIK_TSubScenario,
    DSDLIK_TScript;


  constructor TDSDLInterpreterKernel.Create(AOwner : TComponent);
  var
    i : Integer;
  begin
    inherited Create(AOwner);

    OnError := HandleProcessError;
    OnWarning := HandleAddWarning;
    OnClearWarnings := HandleClearWarnings;
    OnCheckWarning := HandleCheckWarning;
    OnCheckError := HandleCheckError;
    ClearWarnings();

    Messages := TItems.Create();
    MainScenario := nil;
    SubScenarios := TItems.Create();

    LexemsReader := TLexemsReader.Create(Self);
    LexemsReader.SetParameters(constDialogKeyWords, constMarks);

    //����������� ���� ����� ���������������
    LexemsReader.AddIdKind(ikFunctionName, constFunctionNameReadingError);
    LexemsReader.AddIdKind(ikMessageName, constMessageNameReadingError);
    LexemsReader.AddIdKind(ikScenarioName, constScenarioNameReadingError);
    LexemsReader.AddIdKind(ikSubscenarioName, constSubscenarioNameReadingError);
    LexemsReader.AddIdKind(ikLabelName, constLabelNameReadingError);
    LexemsReader.AddIdKind(ikReceiverName, constReceiverNameReadingError);
    for i :=1 to constFunctionCount do
      LexemsReader.AddId(constFunctionNames[i], ikFunctionName);

    DSDLText := '';
    FAnswer := '';
    IsTested := False;
    FStopped := False;
    FScenarioStopped := False;
  end;


  destructor TDSDLInterpreterKernel.Destroy();
  begin
    Clear();
    LexemsReader.Destroy();
    inherited Destroy();
  end;


  procedure TDSDLInterpreterKernel.SetOnError(Value : TOnError);
  begin
    inherited SetOnError(HandleProcessError);
  end;


  procedure TDSDLInterpreterKernel.SetOnWarning(Value : TOnWarning);
  begin
    inherited SetOnWarning(HandleAddWarning);
  end;


  procedure TDSDLInterpreterKernel.SetOnClearWarnings(Value : TOnClearWarnings);
  begin
    inherited SetOnClearWarnings(HandleClearWarnings);
  end;


  procedure TDSDLInterpreterKernel.SetOnCheckWarning(Value : TOnCheckWarning);
  begin
    inherited SetOnCheckWarning(HandleCheckWarning);
  end;


  procedure TDSDLInterpreterKernel.SetOnCheckError(Value : TOnCheckError);
  begin
    inherited SetOnCheckError(HandleCheckError);
  end;


  procedure TDSDLInterpreterKernel.HandleProcessError();
  begin
    FWasError := True;
    FWasWarning := False;
    if not IsTested and Assigned(OnErrorWithText) then OnErrorWithText(FErrorText);
  end;


  procedure TDSDLInterpreterKernel.HandleAddWarning(Description : String);
  begin
    if FErrorText = ''
    then FErrorText := Description
    else FErrorText := FErrorText + #13 + #10 + Description;
    FWasWarning := True;
    FWasError := False;
  end;


  procedure TDSDLInterpreterKernel.HandleClearWarnings();
  begin
    FErrorText := '';
    FWasWarning := False;
    FWasError := False;
  end;


  procedure TDSDLInterpreterKernel.HandleCheckWarning(var WasWarning : Boolean);
  begin
    WasWarning := FWasWarning;
  end;


  procedure TDSDLInterpreterKernel.HandleCheckError(var WasError : Boolean);
  begin
    WasError := FWasError;
  end;


  procedure TDSDLInterpreterKernel.Convert();
  var
    TempMainScenario : TMainScenario;
    TempSubScenario : TSubScenario;
    TempMessage : TMessage;
  begin
    Clear();
    LexemsReader.PassOperatorsSeparators();
    while not WasError() and not LexemsReader.EndOfText() do
    begin
      TempMessage := TMessage.Create(Self);
      TempMessage.Read();
      if not WasWarning() and not WasError()
      then Messages.Add(TempMessage)
      else TempMessage.Destroy();

      if WasWarning() then
      begin
        TempSubScenario := TSubScenario.Create(Self);
        TempSubScenario.Read();
        if not WasWarning() and not WasError()
        then SubScenarios.Add(TempSubScenario)
        else TempSubScenario.Destroy();
      end;

      if WasWarning() then
      begin
        TempMainScenario := TMainScenario.Create(Self);
        TempMainScenario.Read();
        if not WasWarning() and
           not WasError() and
           (MainScenario <> nil) then
        begin
          AddWarning(LexemsReader.Prefix() + constMainScenarioAlreadyExist);
          ProcessError();
        end;
        if not WasWarning() and
           not WasError() and
           (MainScenario = nil)
        then MainScenario := TempMainScenario
        else TempMainScenario.Destroy();
      end;

      if WasWarning() then ProcessError();

      if not WasError() then LexemsReader.PassOperatorsSeparators();
    end;

    if (MainScenario = nil) and not WasError() then
    begin
      AddWarning(LexemsReader.Prefix() + constMainScenario + ' ' + constNotFound1);
      ProcessError();
    end;

    if WasError() then Clear();
  end;


  procedure TDSDLInterpreterKernel.Clear();
  begin
    Messages.Clear();
    SubScenarios.Clear();
    if MainScenario <> nil then MainScenario.Destroy();
    MainScenario := nil;
  end;


  procedure TDSDLInterpreterKernel.Test(TestedDSDLText : String; var ErrorText : String);
  begin
    IsTested := True;
    LexemsReader.AssignText(TestedDSDLText, constDialogDescription);
    Convert();

    if WasError()
    then ErrorText := FErrorText
    else ErrorText := constErrors + ' ' + constNotFound4;

    Clear();
  end;


  procedure TDSDLInterpreterKernel.Run();
  begin
    IsTested := False;
    FStopped := False;
    FScenarioStopped := False;
    if MainScenario <> nil then MainScenario.Execute();
  end;


  procedure TDSDLInterpreterKernel.ExecuteScenario(ScenarioName : String);
  begin
    if not Stopped then
    begin
      if AreStringsEqual(ScenarioName, MainScenario.Name)
      then Run()
      else Execute(ScenarioName, '');
    end;
  end;


  procedure TDSDLInterpreterKernel.Execute(SubscenarioName, LineNumberInformation : String);
  var
    i : Integer;
    Found : Boolean;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Subscenarios.Count()) and not Found do
      begin
        Found := AreStringsEqual((Subscenarios.Item(i) as TScenario).Name, SubscenarioName);
        Inc(i);
      end;

      if Found then
      begin
        Dec(i);
        (Subscenarios.Item(i) as TScenario).Execute();
        FScenarioStopped := False;
      end else
      begin
        AddWarning(LineNumberInformation +
                   constSubscenario + ' "' + SubscenarioName + '" ' + constNotFound1);
        ProcessError();
      end;
    end;
  end;


  procedure TDSDLInterpreterKernel.Execute(ScriptText : String);
  var
    Script : TScript;
  begin
    if not Stopped then
    begin
      Script := TScript.Create(Self);

      LexemsReader.AssignText(ScriptText, '������');
      Script.Read();
      if not WasWarning() and not WasError() then Script.Execute();

      Script.Destroy();
    end;
  end;


  procedure TDSDLInterpreterKernel.Stop();
  begin
    FStopped := True;
  end;


  procedure TDSDLInterpreterKernel.Break();
  begin
    FScenarioStopped := True;
  end;


  procedure TDSDLInterpreterKernel.SendNamedMessage(MessageName, LineNumberInformation : String);
  var
    i : Integer;
    Found : Boolean;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Messages.Count()) and not Found do
      begin
        Found := AreStringsEqual((Messages.Item(i) as TMessage).Name, MessageName);
        Inc(i);
      end;

      if Found then
      begin
        if (Messages.Item(i - 1) as TMessage).AssignedReceiverName <> ''
        then (Messages.Item(i - 1) as TMessage).Send(LineNumberInformation) else
        begin
          AddWarning(constReceiver + ' "' + MessageName + '" ' + constUnknown1);
          ProcessError();
        end;
      end else
      begin
        AddWarning(constMessage + ' "' + MessageName + '" ' + constNotFound3);
        ProcessError();
      end;
    end;
  end;


  procedure TDSDLInterpreterKernel.SendNamedMessageTo(MessageName, ReceiverName, LineNumberInformation : String);
  var
    i : Integer;
    Found : Boolean;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Messages.Count()) and not Found do
      begin
        Found := AreStringsEqual((Messages.Item(i) as TMessage).Name, MessageName);
        Inc(i);
      end;

      if Found
      then (Messages.Item(i - 1) as TMessage).SendTo(ReceiverName, LineNumberInformation)
      else
      begin
        AddWarning(constMessage + ' "' + MessageName + '" ' + constNotFound3);
        ProcessError();
      end;
    end;
  end;


  procedure TDSDLInterpreterKernel.SendMessageAboutAttribute(AttributeName : String; Found : Boolean);
  var
    i : Integer;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Messages.Count()) and not Found do
      begin
        Found := (Messages.Item(i) as TMessage).IsAboutAttribute(AttributeName);
        Inc(i);
      end;

      if Found then (Messages.Item(i - 1) as TMessage).Send('');
    end;
  end;


  procedure TDSDLInterpreterKernel.SendMessageInputAttribute(AttributeName : String; Found : Boolean);
  var
    i : Integer;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Messages.Count()) and not Found do
      begin
        Found := (Messages.Item(i) as TMessage).IsInputAttribute(AttributeName);
        Inc(i);
      end;

      if Found then (Messages.Item(i - 1) as TMessage).Send('');
    end;
  end;


  procedure TDSDLInterpreterKernel.SendMessageOutputAttribute(AttributeName : String; Found : Boolean);
  var
    i : Integer;
  begin
    if not Stopped then
    begin
      i := 0;
      Found := False;
      while (i < Messages.Count()) and not Found do
      begin
        Found := (Messages.Item(i) as TMessage).IsOutputAttribute(AttributeName);
        Inc(i);
      end;

      if Found then (Messages.Item(i - 1) as TMessage).Send('');
    end;
  end;


  procedure TDSDLInterpreterKernel.SetDSDLText(Value : String);
  begin
    FDSDLText := Value;
    if FDSDLText <> '' then
    begin
      LexemsReader.AssignText(Value, constDialogDescription);
      Convert();
    end;
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TDSDLInterpreterKernel]);
  end;

end.

