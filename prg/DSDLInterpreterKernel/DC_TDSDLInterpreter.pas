unit DC_TDSDLInterpreter;

interface
  uses
    Classes,
    Dialogs,
    DC_TInteractor,
    DC_TManager,
    DSDLI_TDSDLInterpreterKernel;

  type
    TDSDLInterpreter = class(TInteractor)
      private
        Kernel : TDSDLInterpreterKernel;
        FFileName : String;
        FDialogDescription : String;
        procedure SetFileName(AFileName : String);
        procedure SetManager(AManager : TManager);override;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;

        procedure Run();
        procedure ProcessMessage(MessageText, SenderName : String);override;
        procedure Stop();override;

        procedure SetFact(AttrName, AttrValue : String);
        procedure GetFact(AttrName : String; var AttrValue : String);
        procedure SendMessageTo(MessageText, ReceiverName : String);

      published
        property FileName : String read FFileName write SetFileName;
        property DialogDescription : String read FDialogDescription write FDialogDescription;
    end;


  procedure Register();


implementation
  uses
    DC_Constants;


  constructor TDialoger.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Kernel := TDSDLInterpreterKernel.Create(Self);
    Kernel.AssignAttributesAccessorMethods(SetFact, GetFact);
  end;


  destructor TDialoger.Destroy();
  begin
    Kernel.Destroy();
    inherited Destroy();
  end;


  procedure TDialoger.SetManager(AManager : TManager);
  begin
    inherited SetManager(AManager);

    Kernel.OnClearWarnings := Manager.ClearWarnings;
    Kernel.OnWarning := Manager.AddWarning;
    Kernel.OnError := Manager.ProcessError;
    Kernel.OnCheckWarning := Manager.CheckWarning;
    Kernel.OnCheckError := Manager.CheckError;

    Kernel.AssignInteractionsManagerMethods(SendMessageTo,
                                            Manager.StopAll);
  end;


  procedure TDialoger.SetFact(AttrName, AttrValue : String);
  begin
    Manager.SetFact(AttrName, AttrValue, '');
  end;


  procedure TDialoger.GetFact(AttrName : String; var AttrValue : String);
  var
    AttrProp : String;
  begin
    Manager.GetFact(AttrName, AttrValue, AttrProp);
  end;


  procedure TDialoger.Run();
  begin
    Kernel.Run(DialogDescription);
  end;


  procedure TDialoger.SendMessageTo(MessageText, ReceiverName : String);
  begin
    Manager.SendMessageTo(MessageText, constDialogerName, ReceiverName);
  end;


  procedure TDialoger.ProcessMessage(MessageText, SenderName : String);
  var
    SubscenarioName : String;
  begin
    Kernel.Answer := '';

    LexemsReader.AssignText(MessageText, constMessage);

    LexemsReader.ReadKeyWordIfItIs(constRunKeyWord);
    if not Manager.WasWarning() then Kernel.Run(DialogDescription);

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constBreakKeyWord);
      if not Manager.WasWarning() then Kernel.Break();
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constTakeKeyWord);
      if not Manager.WasWarning() then
      begin
        LexemsReader.ReadValue();
        Kernel.Answer := LexemsReader.ReadingResult();
        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then
    begin
      LexemsReader.ReadKeyWordIfItIs(constExecuteKeyWord);
      if not Manager.WasWarning() then
      begin
        LexemsReader.ReadIdentifier(ikSubscenarioName, False);
        SubscenarioName := LexemsReader.ReadingResult();
        if not Manager.WasWarning() then Kernel.Execute(SubscenarioName, '');
        if Manager.WasWarning() then Manager.ProcessError();
      end;
    end;

    if Manager.WasWarning() then Manager.ProcessError();
  end;


  procedure TDialoger.Stop();
  begin
    Kernel.Stop();
  end;


  procedure TDialoger.SetFileName(AFileName : String);
  var
    InpLine : String;
    InpFile : TextFile;
  begin
    FFileName := AFileName;
    FDialogDescription := '';
    try
      AssignFile(InpFile, FFileName);
      Reset(InpFile);
      while not Eof(InpFile) do
      begin
        System.Readln(InpFile, InpLine);
        if FDialogDescription <> '' then FDialogDescription := FDialogDescription + #13 + #10;
        FDialogDescription := FDialogDescription + InpLine;
      end;
      Close(InpFile);
    except
      FFileName := '';
      ShowMessage(constFileReadingError);
    end;
  end;


  procedure Register();
  begin
    RegisterComponents('A-Tata', [TDialoger]);
  end;

end.
