unit DSDLIK_TConditionalJump;

interface
  uses
    Classes,
    DSDLIK_TExpression,
    DSDLIK_TJump;

  type
    TConditionalJump = class(TJump)
      private
        Condition : TExpression;
      public
        constructor Create(AOwner : TComponent);override;
        destructor Destroy();override;
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation
  uses
    LR_Functions,
    DSDLIK_Constants;


  constructor TConditionalJump.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Condition := nil;
  end;


  destructor TConditionalJump.Destroy();
  begin
    if Condition <> nil then Condition.Destroy();
    inherited Destroy();
  end;


  procedure TConditionalJump.DoReading();
  begin
    if Condition <> nil then Condition.Destroy();
    LexemsReader.ReadKeyWordIfItIs(kwWhen);
    if not WasWarning() then
    begin
      Condition := ReadExpression(Self);
      if not WasWarning() and not WasError()
      then inherited DoReading();
    end;
  end;


  procedure TConditionalJump.Execute();
  var
    ExprValue : String;
  begin
    ExprValue := Condition.Value();
    if CanContinue() and AreStringsEqual(ExprValue, constTrue)
    then inherited Execute();
  end;

end.
