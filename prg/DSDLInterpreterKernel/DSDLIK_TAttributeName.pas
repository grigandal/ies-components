unit DSDLIK_TAttributeName;

interface
  uses
    Classes,
    DSDLIK_TExpression,
    DSDLIK_TDSDLInterpreterKernel;

  type
    TAttributeName = class(TExpression)
      private
        FAttributeName : String;
      protected
      public
        property AttributeName : String read FAttributeName;
        procedure DoReading();override;
        function Value() : String;override;
    end;

implementation
  uses
    DSDLIK_Constants;

    
  procedure TAttributeName.DoReading();
  begin
    FAttributeName := '';
    LexemsReader.ReadName();
    if not WasWarning() and not WasError() then
    begin
      FAttributeName := LexemsReader.ReadingResult();
      ConfirmReading();
    end;
  end;


  function TAttributeName.Value() : String;
  var
    Val : String;
  begin
    if Pos(constPropSeparator, AttributeName) = 0
    then (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnGetAttrValue(AttributeName, Val)
    else (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnGetAttrProp
           (
             Copy(AttributeName, 1, Pos(constPropSeparator, AttributeName) - 1),
             Copy(AttributeName, Pos(constPropSeparator, AttributeName) + 1, Length(AttributeName)),
             Val
           );
    Result := Val;
  end;

end.
