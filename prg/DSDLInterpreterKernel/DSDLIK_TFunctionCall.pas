unit DSDLIK_TFunctionCall;

interface
  uses
    Classes,
    DSDLIK_TExpression,
    DSDLIK_TStructure;

  type
    TFunctionCall = class(TExpression)
      private
        Arguments : TStructure;
        FFunctionName : String;

      public
        property FunctionName : String read FFunctionName;

        constructor Create(AOwner : TComponent); override;

        destructor Destroy(); override;
        procedure DoReading(); override;
        function Value() : String; override;
    end;


implementation
  uses
    SysUtils,
    LR_Constants,
    LR_Functions,
    DSDLIK_Constants;

    
  constructor TFunctionCall.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    Arguments := TStructure.Create(Self);
    FFunctionName := '';
  end;


  destructor TFunctionCall.Destroy();
  begin
    Arguments.Destroy();
    inherited Destroy();
  end;


  procedure TFunctionCall.DoReading();
  begin
    FFunctionName := '';
    Arguments.Clear();
    LexemsReader.ReadIdentifier(ikFunctionName, True);
    if not WasWarning() then
    begin
      FFunctionName := LexemsReader.ReadingResult();
      Arguments.Read();
      if not WasWarning() and not WasError() then ConfirmReading();
      if WasWarning() then ProcessError();
    end;
  end;


  function TFunctionCall.Value() : String;
  var
    i : Integer;
    TempValue : String;
    FunctionExists : Boolean;

    function Text(Inp : String) : String;
    var
      i : Integer;
      StartString, FirstString : Boolean;
    begin
      Result := constValueMarker;
      StartString := False;
      FirstString := True;
      for i := 1 to Length(Inp) do
      begin
        if StartString then
        begin
          if not FirstString then Result := Result + constComma;
          Result := Result + constStringMarker;
          StartString := False;
        end;
        if (Inp[i] <> #13) and (Inp[i] <> #10) then Result := Result + Inp[i];
        if (Inp[i] = #10) then
        begin
          StartString := True;
          FirstString := False;
          Result := Result + constValueMarker;
        end;
      end;
      Result := Result + constValueMarker;
    end;

  begin
    Result := '';
    FunctionExists := False;

    if AreStringsEqual(constEqualFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := constTrue;
        i := 0;
        while (i < Arguments.Count()) and CanContinue() and (Result = constTrue) do
        begin
          if i = 0 then TempValue := Arguments.Expression(i).Value() else
            if not AreStringsEqual(TempValue, Arguments.Expression(i).Value())
            then Result := constFalse;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constEqualFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constAndFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := constTrue;
        i := 0;
        while (i < Arguments.Count()) and CanContinue() and (Result = constTrue) do
        begin
          if AreStringsEqual(constTrue, Arguments.Expression(i).Value())
          then Result := constTrue;
          if AreStringsEqual(constFalse, Arguments.Expression(i).Value())
          then Result := constFalse;
          if (Result <> constTrue) and (Result <> constFalse) then
          begin
            AddWarning(LineNumberInformation + constFunction + ' ' + constAndFunctionName + ' : ' +
                       constAllArguments + ' ' + constMustBeBoolean2);
            ProcessError();
          end;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constAndFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constOrFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := constFalse;
        i := 0;
        while (i < Arguments.Count()) and CanContinue() and (Result = constFalse) do
        begin
          if AreStringsEqual(constTrue, Arguments.Expression(i).Value())
          then Result := constTrue;
          if AreStringsEqual(constFalse, Arguments.Expression(i).Value())
          then Result := constFalse;
          if (Result <> constTrue) and (Result <> constFalse) then
          begin
            AddWarning(LineNumberInformation + constFunction + ' ' + constOrFunctionName + ' : ' +
                       constAllArguments + ' ' + constMustBeBoolean2);
            ProcessError();
          end;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constOrFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constNotFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 1 then
      begin
        if AreStringsEqual(constTrue, Arguments.Expression(0).Value())
        then Result := constFalse;
        if AreStringsEqual(constFalse, Arguments.Expression(0).Value())
        then Result := constTrue;
        if (Result <> constTrue) and (Result <> constFalse) then
        begin
          AddWarning(LineNumberInformation + constFunction + ' ' + constNotFunctionName + ' : ' +
                     constArgument + ' ' + constMustBeBoolean1);
          ProcessError();
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constNotFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constConcatFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := '';
        i := 0;
        while (i < Arguments.Count()) and CanContinue() do
        begin
          Result := Result + Arguments.Expression(i).Value();
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constConcatFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constStringFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 1 then
      begin
        Result := constValueMarker +
                  Arguments.Expression(0).Value() +
                  constValueMarker;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constStringFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if AreStringsEqual(constTextFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      Result := constTextOpen;
      i := 0;
      while (i < Arguments.Count()) and CanContinue() do
      begin
        if i > 0 then Result := Result + constComma;
        Result := Result + Text(Arguments.Expression(i).Value());
        Inc(i);
      end;
      Result := Result + constTextClose;
    end;

    if AreStringsEqual(constNameFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 1 then
      begin
        Result := constNameMarker + Arguments.Expression(0).Value() + constNameMarker;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constNameFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumMinus...
    if AreStringsEqual(constNumMinusFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 1 then
      begin
        try
          Result := FloatToStr(-StrToFloat(Arguments.Expression(0).Value()));
        except
          AddWarning(LineNumberInformation + constFunction + ' ' + constNumMinusFunctionName + ' : ' +
                     constStrToIntError);
          ProcessError();
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunction + ' ' + constNumMinusFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumSum...
    if AreStringsEqual(constNumSumFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := '0';
        i := 0;
        while (i < Arguments.Count()) and CanContinue() do
        begin
          try
            Result := FloatToStr(StrToFloat(Result) + StrToFloat(Arguments.Expression(i).Value()));
          except
            AddWarning(LineNumberInformation + constFunction + ' ' + constNumSumFunctionName + ' : ' +
                       constStrToIntError);
            ProcessError();
          end;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumSumFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumDiv...
    if AreStringsEqual(constNumDivFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 2 then
      begin
        try
          if StrToFloat(Arguments.Expression(1).Value()) = 0 then
          begin
            AddWarning(LineNumberInformation + constFunction + ' ' + constNumDivFunctionName + ' : ' +
                       constDivisionByZeroError);
            ProcessError();
          end else
            Result := FloatToStr(StrToFloat(Arguments.Expression(0).Value()) /
                                 StrToFloat(Arguments.Expression(1).Value()));
        except
          AddWarning(LineNumberInformation + constFunction + ' ' + constNumDivFunctionName + ' : ' +
                     constStrToIntError);
          ProcessError();
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumDivFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumMul...
    if AreStringsEqual(constNumMulFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := '1';
        i := 0;
        while (i < Arguments.Count()) and CanContinue() do
        begin
          try
            Result := FloatToStr(StrToFloat(Result) * StrToFloat(Arguments.Expression(i).Value()));
          except
            AddWarning(LineNumberInformation + constFunction + ' ' + constNumMulFunctionName + ' : ' +
                       constStrToIntError);
            ProcessError();
          end;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumMulFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumEqv...
    if AreStringsEqual(constNumEqualFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() > 0 then
      begin
        Result := constTrue;
        i := 0;
        while (i < Arguments.Count()) and CanContinue() and (Result = constTrue) do
        begin
          if i = 0 then TempValue := Arguments.Expression(i).Value() else
          try
            if not (StrToFloat(TempValue) = StrToFloat(Arguments.Expression(i).Value()))
            then Result := constFalse;
          except
            AddWarning(LineNumberInformation + constFunction + ' ' + constNumEqualFunctionName + ' : ' +
                       constStrToIntError);
            ProcessError();
          end;
          Inc(i);
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumEqualFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumLess...
    if AreStringsEqual(constNumLessFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 2 then
      begin
        try
          if StrToFloat(Arguments.Expression(0).Value()) < StrToFloat(Arguments.Expression(1).Value())
          then Result := constTrue
          else Result := constFalse;
        except
          AddWarning(LineNumberInformation + constFunction + ' ' + constNumLessFunctionName + ' : ' +
                     constStrToIntError);
          ProcessError();
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumLessFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    //NumGreater...
    if AreStringsEqual(constNumGreaterFunctionName, FunctionName) then
    begin
      FunctionExists := True;
      if Arguments.Count() = 2 then
      begin
        try
          if StrToFloat(Arguments.Expression(0).Value()) > StrToFloat(Arguments.Expression(1).Value())
          then Result := constTrue
          else Result := constFalse;
        except
          AddWarning(LineNumberInformation + constFunction + ' ' + constNumLessFunctionName + ' : ' +
                     constStrToIntError);
          ProcessError();
        end;
      end else
      begin
        AddWarning(LineNumberInformation + constFunctionName + ' ' + constNumLessFunctionName + ' : ' +
                   constArgumentsNumberError);
        ProcessError();
      end;
    end;

    if not FunctionExists then
    begin
      AddWarning(LineNumberInformation + constFunction + ' ' + FunctionName + ' ' +
                 constUnknown2);
      ProcessError();
    end;
  end;

end.
