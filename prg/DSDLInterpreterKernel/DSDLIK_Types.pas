unit DSDLIK_Types;

interface
  uses
    Classes;


  type
    TOnSendMessage = procedure(MessageText, ReceiverName : String) of object;
    TOnSendMessageToAll = procedure(MessageText : String) of object;

    TOnStopObject = procedure(ObjName : String) of object;
    TOnStopAll = procedure() of object;

    TOnSetAttrValue = procedure(AttrName, AttrValue : String) of object;
    TOnGetAttrValue = procedure(AttrName : String; var AttrValue : String) of object;
    TOnSetAttrProp = procedure(AttrName, PropName, PropValue : String) of object;
    TOnGetAttrProp = procedure(AttrName, PropName : String; var PropValue : String) of object;

    TOnErrorWithText = procedure(ErrorText : String) of object;

    TOwnerClass = class of TComponent;

    TMarks = set of Char;

    TAssigningType = (atAbout, atInput, atOutput);

implementation

end.
