unit DSDLIK_TOperator;

interface
  uses
    Classes,
    DSDLIK_TItem;

  type
    TOperator = class(TItem)
      private
      public
        procedure Execute();virtual;abstract;
    end;

  function ReadOperator(Owner : TComponent) : TOperator;

implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TAssignment,
    DSDLIK_TJump,
    DSDLIK_TConditionalJump,
    DSDLIK_TExecution,
    DSDLIK_TLabel,
    DSDLIK_TSending,
    DSDLIK_TBreak,
    DSDLIK_TStop;


  function ReadOperator(Owner : TComponent) : TOperator;
  begin
    Result := TAssignment.Create(Owner);
    Result.Read();

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TJump.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TConditionalJump.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TExecution.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TSending.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TBreak.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TStop.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() then
    begin
      Result.Destroy();
      Result := TLabel.Create(Owner);
      Result.Read();
    end;

    if Result.WasWarning() or Result.WasError() then
    begin
      Result.Destroy();
      Result := nil;
    end;
  end;

end.
