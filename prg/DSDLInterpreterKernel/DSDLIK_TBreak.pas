unit DSDLIK_TBreak;

interface
  uses
    DSDLIK_TOperator;

  type
    TBreak = class(TOperator)
      private
      public
        procedure DoReading();override;
        procedure Execute();override;
    end;


implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TDSDLInterpreterKernel;


  procedure TBreak.DoReading();
  begin
    LexemsReader.ReadKeyWordIfItIs(kwBreak);
    if not WasWarning() then ConfirmReading();
  end;


  procedure TBreak.Execute();
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).Break();
  end;

end.
