unit DSDLIK_Constants;

interface
  const
    constSeparators = [' ', #10, #13, #09];
    constMarks = ['=', ',', ';', '(', ')', '[', ']'];
    constIdChars = ['0'..'9','A'..'Z','a'..'z','_',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�','�',
                    '�','�','�','�','�','�','�','�','�','�'];
    constPropSeparator = '>';

    kwMessage = 'message';
    kwTo = 'to';
    kwAbout = 'about';
    kwInput = 'input';
    kwOutput = 'output';
    kwEnd = 'end';
    kwAll = 'all';
    kwLine = 'line';
    kwScenario = 'scenario';
    kwSubscenario = 'subscenario';
    kwSend = 'send';
    kwGoto = 'goto';
    kwWhen = 'when';
    kwSet = 'set';
    kwExecute = 'execute';
    kwAnswer = 'answer';
    kwStop = 'stop';
    kwBreak = 'break';

    constDialogKeyWords : array[1..18] of String =
      (kwMessage,
       kwTo,
       kwAbout,
       kwInput,
       kwOutput,
       kwEnd,
       kwAll,
       kwLine,
       kwScenario,
       kwSubscenario,
       kwSend,
       kwGoto,
       kwWhen,
       kwSet,
       kwExecute,
       kwAnswer,
       kwStop,
       kwBreak);

    constValueMarker = #39;
    constCitingMarker = '$';
    constNameMarker = '#';
    constCommentsOpen = '/';
    constCommentsClose = '/';

    //-����� �������------------------------------------------------------------
    constEqualFunctionName = 'eqv';
    constAndFunctionName = 'and';
    constOrFunctionName = 'or';
    constNotFunctionName = 'not';
    constConcatFunctionName = 'concat';
    constStringFunctionName = 'string';
    constTextFunctionName = 'text';
    constNameFunctionName = 'name';
    constNumMinusFunctionName = 'minus';
    constNumSumFunctionName = 'sum';
    constNumDivFunctionName = 'div';
    constNumMulFunctionName = 'mul';
    constNumEqualFunctionName = 'numeqv';
    constNumLessFunctionName = 'less';
    constNumGreaterFunctionName = 'greater';

    //-������ ���� �������------------------------------------------------------
    constFunctionCount = 15;
    constFunctionNames : array[1..constFunctionCount] of String =
      (constEqualFunctionName,
       constAndFunctionName,
       constOrFunctionName,
       constNotFunctionName,
       constConcatFunctionName,
       constStringFunctionName,
       constTextFunctionName,
       constNameFunctionName,
       constNumEqualFunctionName,
       constNumMinusFunctionName,
       constNumSumFunctionName,
       constNumDivFunctionName,
       constNumMulFunctionName,
       constNumLessFunctionName,
       constNumGreaterFunctionName);

    //-��������� ��� ������������ ������� ���������-----------------------------
    //-�������------------------------------------------------------------------
    constDialogDescription = '�������� �������';
    constString = '��������';
    constLine = '������';
    constIdentifier = '�������������';
    constKeyWord = '�������� �����';
    constValue = '��������';
    constName = '��� ��������';
    constMark = '����';
    constReceiver = '���������� ���������';
    constExpression = '���������';
    constOperator = '��������';
    constLabel = '�����';
    constMainScenario = '������� ��������';
    constSubScenario = '�������������� ��������';
    constMessage = '���������';
    constMessageAboutAttribute = '��������� �� �������� � ������';
    constFunction = '�������';
    constArgument = '��������';
    constAllArguments = '��� ���������';

    constFunctionName = '��� �������';
    constMessageName = '��� ���������';
    constScenarioName = '��� ��������';
    constSubscenarioName = '��� �����������';
    constLabelName = '��� �����';
    constReceiverName = '��� ���������� ���������';

    constErrors = '������';

    //-���������----------------------------------------------------------------
    constNotFound1 = '�� ������';
    constNotFound2 = '�� �������';
    constNotFound3 = '�� �������';
    constNotFound4 = '�� �������';

    constUnknown1 = '����������';
    constUnknown2 = '����������';
    constUnknown3 = '����������';

    constAlreadyExist = '��� ����������';

    constMustBeBoolean1 = '������ ���� ����������� ����';
    constMustBeBoolean2 = '������ ���� ����������� ����';

    //-�����--------------------------------------------------------------------
    constStringReadingError = constString + ' ' + constNotFound3;
    constIdentifierReadingError = constIdentifier + ' ' + constNotFound1;
    constKeyWordReadingError = constKeyWord + ' ' + constNotFound3;
    constValueReadingError = constValue + ' ' + constNotFound3;
    constNameReadingError = constName + ' ' + constNotFound3;
    constMarkReadingError = constMark + ' ' + constNotFound1;

    constMainScenarioAlreadyExist = constMainScenario + ' ' + constAlreadyExist;

    constSubscenarioNotFound = constSubscenario + ' ' + constNotFound1;
    constArgumentsNumberError = '�������� ����� ����������';
    constStrToIntError = '���������� �������������� ������ � �����';
    constDivisionByZeroError = '������� �� ����';

    constFunctionNameReadingError = constFunctionName + ' ' + constNotFound3;
    constMessageNameReadingError = constMessageName + ' ' + constNotFound3;
    constScenarioNameReadingError = constScenarioName + ' ' + constNotFound3;
    constSubscenarioNameReadingError = constSubscenarioName + ' ' + constNotFound3;
    constLabelNameReadingError = constLabelName + ' ' + constNotFound3;
    constReceiverNameReadingError = constReceiverName + ' ' + constNotFound3;

    //-����������������� ��������-----------------------------------------------
    constTrue = 'T';
    constFalse = 'F';

    //-Identifier Kinds---------------------------------------------------------
    ikFunctionName = 1;
    ikMessageName = 2;
    ikScenarioName = 3;
    ikSubscenarioName = 4;
    ikLabelName = 5;
    ikReceiverName = 6;

implementation

end.
