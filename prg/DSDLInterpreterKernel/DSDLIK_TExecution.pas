unit DSDLIK_TExecution;

interface
  uses
    DSDLIK_TOperator;

  type
    TExecution = class(TOperator)
      private
        SubscenarioName : String;
      public
        procedure DoReading();override;
        procedure Execute();override;
    end;

implementation
  uses
    DSDLIK_TDSDLInterpreterKernel,
    DSDLIK_Constants;


  procedure TExecution.DoReading();
  begin
    SubscenarioName := '';
    LexemsReader.ReadKeyWordIfItIs(kwExecute);
    if not WasWarning() then
    begin
      LexemsReader.ReadIdentifier(ikSubscenarioName, False);
      if WasWarning() then ProcessError();
      if not WasError() then
      begin
         SubscenarioName := LexemsReader.ReadingResult();
         ConfirmReading();
      end;
    end;
  end;


  procedure TExecution.Execute();
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).Execute(SubscenarioName, LineNumberInformation);
  end;

end.
