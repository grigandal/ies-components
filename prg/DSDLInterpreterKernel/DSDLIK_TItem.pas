unit DSDLIK_TItem;

interface
  uses
    DSDLIK_Types,
    LR_TLexemsReader,
    Classes;

  type
    TItem = class(TComponent)
      private
        FLineNumberInformation : String;
        OldPosition : Integer;
        WasReadingConfirm : Boolean;
        FLexemsReader : TLexemsReader;

      protected
        property LexemsReader : TLexemsReader read FLexemsReader;
        procedure DoReading(); virtual; abstract;

      public
        property LineNumberInformation : String read FLineNumberInformation;

        constructor Create(AOwner : TComponent);override;

        procedure ProcessError();
        procedure AddWarning(Description : String);
        procedure ClearWarnings();
        function WasWarning() : Boolean;
        function WasError() : Boolean;

        procedure Read();
        procedure ConfirmReading();
        function OwnerByClass(OwnerClass : TOwnerClass) : TComponent;
        function CanContinue() : Boolean;

      published
    end;


implementation
  uses
    DSDLIK_TDSDLInterpreterKernel;


  constructor TItem.Create(AOwner : TComponent);
  begin
    inherited Create(AOwner);
    FLexemsReader := (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).LexemsReader;
  end;


  procedure TItem.ProcessError();
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).ProcessError();
  end;


  procedure TItem.AddWarning(Description : String);
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).AddWarning(Description);
  end;


  procedure TItem.ClearWarnings();
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).ClearWarnings();
  end;


  function TItem.WasWarning() : Boolean;
  begin
    Result := (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).WasWarning();
  end;


  function TItem.WasError() : Boolean;
  begin
    Result := (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).WasError();
  end;


  procedure TItem.Read();
  begin
    LexemsReader.PassSeparators();
    OldPosition := LexemsReader.Position;
    WasReadingConfirm := False;
    FLineNumberInformation := LexemsReader.Prefix();

    DoReading();

    if WasReadingConfirm
    then ClearWarnings()
    else LexemsReader.Position := OldPosition;
  end;


  procedure TItem.ConfirmReading();
  begin
    WasReadingConfirm := True;
  end;


  function TItem.OwnerByClass(OwnerClass : TOwnerClass) : TComponent;
  begin
    Result := Owner;
    while not (Result is OwnerClass) do Result := Result.Owner;
  end;


  function TItem.CanContinue() : Boolean;
  begin
    Result := not (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).Stopped and
              not (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).ScenarioStopped and
              not WasError();
  end;

end.
