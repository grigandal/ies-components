unit DSDLIK_TValue;

interface
  uses
    DSDLIK_TExpression;

  type
    TValue = class(TExpression)
      private
        Content : String;
      public
        procedure DoReading();override;
        function Value() : String;override;
    end;


implementation

  procedure TValue.DoReading();
  begin
    Content := '';
    LexemsReader.ReadValue();
    if not WasWarning() and not WasError() then
    begin
      Content := LexemsReader.ReadingResult();
      ConfirmReading();
    end;
  end;


  function TValue.Value() : String;
  begin
    Result := Content;
  end;

end.
