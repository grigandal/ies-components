unit DSDLIK_TStop;

interface
  uses
    DSDLIK_TOperator;

  type
    TStop = class(TOperator)
      private
      public
        procedure DoReading();override;
        procedure Execute();override;
    end;


implementation
  uses
    DSDLIK_Constants,
    DSDLIK_TDSDLInterpreterKernel;


  procedure TStop.DoReading();
  begin
    LexemsReader.ReadKeyWordIfItIs(kwStop);
    if not WasWarning() then ConfirmReading();
  end;


  procedure TStop.Execute();
  begin
    (OwnerByClass(TDSDLInterpreterKernel) as TDSDLInterpreterKernel).OnStopAll();
  end;

end.
