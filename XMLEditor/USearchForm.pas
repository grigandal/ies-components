unit USearchForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TSearchForm = class(TForm)
    Label1: TLabel;
    SearchStringEdit: TEdit;
    OKB: TButton;
    CancelB: TButton;
    SearchGB: TGroupBox;
    NodeNamesCB: TCheckBox;
    AttrNamesCB: TCheckBox;
    AttrValuesCB: TCheckBox;
    NodeValuesCB: TCheckBox;
    FullStrOnlyCB: TCheckBox;
    procedure ValidateSearchParams(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SearchForm: TSearchForm;

implementation

{$R *.dfm}

procedure TSearchForm.ValidateSearchParams(Sender: TObject);
begin
  if (NodeNamesCB.Checked or AttrNamesCB.Checked or NodeValuesCB.Checked or AttrValuesCB.Checked)
  and (Length(SearchStringEdit.Text) > 0) then
    OKB.Enabled := true
  else OKB.Enabled := false;
end;

end.
