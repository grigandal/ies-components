unit UDTDError;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TDTDErrorForm = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    ErrorCodeEdit: TEdit;
    LineEdit: TEdit;
    ErrorMemo: TMemo;
    Label3: TLabel;
    Label4: TLabel;
    LinePositionEdit: TEdit;
    TextMemo: TMemo;
    Label5: TLabel;
    Label6: TLabel;
    FilePositionEdit: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DTDErrorForm: TDTDErrorForm;

implementation

{$R *.dfm}

end.
