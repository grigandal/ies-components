unit XMLEditForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ActnList,  Buttons, ToolWin, Grids,
  ExtCtrls, StdCtrls, xmldom, XMLIntf, XMLDoc, ShellAPI,
  XMLTreeView, XMLAttrView, ValEdit;

const
  Cap = 'XML ��������';

type
  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    LoadMenuItem: TMenuItem;
    SaveMenuItem: TMenuItem;
    N1: TMenuItem;
    ExitMenuItem: TMenuItem;
    NewMenuItem: TMenuItem;
    EditMenu: TMenuItem;
    InsertNodeMenuItem: TMenuItem;
    AddChildMenuItem: TMenuItem;
    ActionList: TActionList;
    NewDoc: TAction;
    OpenDoc: TAction;
    SaveDoc: TAction;
    ExitA: TAction;
    InsertNodeA: TAction;
    OpenDlg: TOpenDialog;
    SaveDlg: TSaveDialog;
    TreePopupMenu: TPopupMenu;
    InsertNodeMI: TMenuItem;
    DeleteNodeMenuItem: TMenuItem;
    AddChildMI: TMenuItem;
    DeleteNodeMI: TMenuItem;
    AddChildA: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton8: TToolButton;
    CutNodeA: TAction;
    HelpMenu: TMenuItem;
    AboutMenuItem: TMenuItem;
    About: TAction;
    SaveAsDoc: TAction;
    SaveAsItem: TMenuItem;
    NewDocSB: TSpeedButton;
    OpenDocSB: TSpeedButton;
    SaveDocSB: TSpeedButton;
    SaveAsDocSB: TSpeedButton;
    ToggleShowAttrSB: TSpeedButton;
    ValidateCB: TSpeedButton;
    ExitCB: TSpeedButton;
    OpenInIECB: TSpeedButton;
    AppendNodeA: TAction;
    AppendNodeMI: TMenuItem;
    AppendNodeMenuItem: TMenuItem;
    TopPanel: TPanel;
    HSplitter: TSplitter;
    XMLEdit: TXMLTreeView;
    VSplitter: TSplitter;
    ToggleShowAttr: TAction;
    ViewMenuItem: TMenuItem;
    ToggleShowAttrItem: TMenuItem;
    LeftPanel: TPanel;
    XMLAttrEdit: TXMLAttrView;
    Splitter1: TSplitter;
    DTDGB: TGroupBox;
    DTDMemo: TMemo;
    NodeTextGB: TGroupBox;
    XMLText: TMemo;
    ToolsMenu: TMenuItem;
    ValidateMenuItem: TMenuItem;
    ValidateA: TAction;
    N5: TMenuItem;
    InsertAttr: TAction;
    AppendAttr: TAction;
    DeleteAttr: TAction;
    InsertAttrMenuItem: TMenuItem;
    AppendAttrMenuItem: TMenuItem;
    DeleteAttrMenuItem: TMenuItem;
    AttrPopupMenu: TPopupMenu;
    AppendAttrMI: TMenuItem;
    InsertAttrMI: TMenuItem;
    DeleteAttrMI: TMenuItem;
    OpenInIE: TAction;
    OpenInIEMenuItem: TMenuItem;
    N2: TMenuItem;
    XMLEditorHelp: TAction;
    XMLEditorHelpMI: TMenuItem;
    SearchA: TAction;
    SearchAgainA: TAction;
    N3: TMenuItem;
    SearchMenuItem: TMenuItem;
    SearchAgainMenuItem: TMenuItem;
    CopyNodeA: TAction;
    PasteNodeA: TAction;
    CopyNodeMenuItem: TMenuItem;
    PasteNodeMenuItem: TMenuItem;
    N4: TMenuItem;
    CopyNodeMI: TMenuItem;
    PasteNodeMI: TMenuItem;
    N6: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ExitAExecute(Sender: TObject);
    procedure OpenDocExecute(Sender: TObject);
    procedure SaveDocExecute(Sender: TObject);
    procedure NewDocExecute(Sender: TObject);
    procedure InsertNodeAExecute(Sender: TObject);
    procedure NodeEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure NodeChange(Sender: TObject; Node: TTreeNode);
    procedure AddChildAExecute(Sender: TObject);
    procedure CutNodeAExecute(Sender: TObject);
    procedure AboutExecute(Sender: TObject);
    procedure SaveAsDocExecute(Sender: TObject);
    procedure AppendNodeAExecute(Sender: TObject);
    procedure XMLEditHint(Sender: TObject);
    procedure ToggleShowAttrExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ValidateAExecute(Sender: TObject);
    procedure InsertAttrExecute(Sender: TObject);
    procedure AppendAttrExecute(Sender: TObject);
    procedure DeleteAttrExecute(Sender: TObject);
    procedure OpenInIEExecute(Sender: TObject);
    procedure XMLAttrEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure XMLEditorHelpExecute(Sender: TObject);
    procedure SearchAExecute(Sender: TObject);
    procedure SearchAgainAExecute(Sender: TObject);
    procedure CopyNodeAExecute(Sender: TObject);
    procedure PasteNodeAExecute(Sender: TObject);
  public
    DTDPresent: boolean;  // ������� ����, ��� DTD ����
    DTDFile: string;      // ���� �� �����, �� ������ �������� �������� doctype.SYSTEM
    DTDSL: TStringList;
    Buffer: IXMLNode;     // ��� ����� ������
    SearchString: string; // ��� ������
    InNodeNames, InAttrNames, InNodeValues, InAttrValues, FullStrOnly: boolean; // ��� ������
    procedure OpenXMLDocument;
    procedure LoadDTD;    // �������� DTD, ���������� ������� ��� ���
    function Valid(ptr: IUnknown): boolean;
    function GetNextNode(n: IXMLNode): IXMLNode; // ���������� ��������� sibling ��� ����
    function Search(n: IXMLNode): IXMLNode; // ���������� ���� � ������� �����, ����� nil
  end;

var
  MainForm: TMainForm;

implementation

uses About, UDTDError, USearchForm;

{$R *.dfm}

//-----------------------------------------------------------------------------
function TMainForm.Valid(ptr: IUnknown): boolean;
begin
  try
    ptr._AddRef;
    ptr._Release;
    Result := true;
  except
    Result := false;
  end;
end;

function TMainForm.GetNextNode(n: IXMLNode): IXMLNode;
begin
  Result := nil;

  if n.ParentNode.NodeType = ntDocument then exit;

  // �������� ��������� ���� � ������
  if n <> n.ParentNode.ChildNodes.Last then begin
    // �������� � ���������� sibling, ���� �� ����
    Result := n.NextSibling;
  end;

  if Result = nil then begin
    // sibling ���, ���������� �� ������� �����
    Result := GetNextNode(n.ParentNode);
  end;
end;

function TMainForm.Search(n: IXMLNode): IXMLNode;
var
  m: IXMLNode;
  i: integer;
  OldCursor: TCursor;
begin
  OldCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;

  // ���� ���������� ��� ������, ������� � ���������� ����, �������� �������� ����� ���������
  // FullStrOnly - ���� ������� ����������, ����� ���������
  Result := nil;
  if n = nil then exit;

  if InNodeNames then begin
    // ���� � ��������� �����
    if FullStrOnly and (AnsiCompareText(n.NodeName, SearchString) = 0) or
    not FullStrOnly and (AnsiPos(n.NodeName, SearchString) > 0) then
      Result := n;
  end;

  if (Result = nil) and InNodeValues and n.IsTextElement then begin
    // ���� � ��������� �����, ���� ����
    //ShowMessage(n.Text);
    if FullStrOnly and (AnsiCompareText(n.Text, SearchString) = 0) or
    not FullStrOnly and (AnsiPos(n.Text, SearchString) > 0) then
      Result := n;
  end;

  if (Result = nil) and (n.AttributeNodes.Count > 0) then begin
    // ���� � ��������� �����
    i:=0;
    while i < n.AttributeNodes.Count do begin
      m := n.AttributeNodes[i];
      if InAttrNames then begin
        // ���� � ��������� ���������
        if FullStrOnly and (AnsiCompareText(m.NodeName, SearchString) = 0) or
        not FullStrOnly and (AnsiPos(m.NodeName, SearchString) > 0) then begin
          Result := n;
          break;
        end
      end;

      if InAttrValues then begin
        // ���� � ��������� ���������
        if FullStrOnly and (AnsiCompareText(m.Text, SearchString) = 0) or
        not FullStrOnly and (AnsiPos(m.Text, SearchString) > 0) then begin
          Result := n;
          break;
        end
      end;
      Inc(i);
    end;
  end;

  if (Result = nil) and n.HasChildNodes then begin
     // ���������� ���������� ����
     Result := Search(n.ChildNodes.First);
  end;

  if (Result = nil) then begin
    Result := Search(GetNextNode(n));
  end;

  Screen.Cursor := OldCursor;
end;

//-----------------------------------------------------------------------------

procedure TMainForm.FormCreate(Sender: TObject);
begin
  DTDSL := TStringList.Create;
  Application.HintHidePause := 10000;
  Caption := Cap;
  XMLEdit.Width := XMLEdit.Parent.ClientWidth div 2;
  XMLEdit.NodeOptions := [];
  XMLEdit.ReadOnly := False;

  // ������������� (���� �� ��������� ������ ���� ���������)
  if ParamCount > 0 then begin
    if FileExists(ParamStr(1)) then begin
      OpenDlg.FileName := ParamStr(1);
      OpenXMLDocument;
    end
  end
  else NewDocExecute(Sender);

  ToggleShowAttrExecute(ToggleShowAttrItem);
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DTDSL.Free;
end;

//-----------------------------------------------------------------------------

procedure TMainForm.ExitAExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.NewDocExecute(Sender: TObject);
begin
  //�������� XMLDocument
  XMLEdit.XMLDocument.FileName := '';
  XMLEdit.XMLDocument.LoadFromXML('<root/>');
  XMLAttrEdit.XMLNode := XMLEdit.XMLDocument.DocumentElement;
  XMLEdit.XMLDocument.Encoding := 'utf-8'; // ��� ��������� �������� �����
  XMLEdit.SynchronizeTree(XMLEdit.XMLDocument.DocumentElement);
  XMLEdit.Items.GetFirstNode.EditText; // ����������
  XMLEdit.Items.GetFirstNode.EndEdit(true);
  Caption := Cap+' - ����������';
  DTDSL.Clear;
  DTDPresent := false;
  DTDFile := '';
end;

procedure TMainForm.OpenDocExecute(Sender: TObject);
begin
  if OpenDlg.Execute then OpenXMLDocument;
end;

procedure TMainForm.OpenXMLDocument;
begin
  XMLEdit.XMLDocument.LoadFromFile(OpenDlg.FileName);
  XMLAttrEdit.XMLNode := XMLEdit.XMLDocument.DocumentElement;
  //if XMLEdit.XMLDocument.Encoding = '' then
    XMLEdit.XMLDocument.Encoding := 'utf-8'; // ��� ��������� �������� �����
  XMLEdit.SynchronizeTree(XMLEdit.XMLDocument.DocumentElement);
  XMLEdit.Items.GetFirstNode.Selected := True;
  Caption := Cap+' - '+ChangeFileExt(ExtractFileName(OpenDlg.FileName),'');
  SaveDlg.FileName := OpenDlg.FileName;
  LoadDTD;
  NodeChange(XMLEdit, XMLEdit.Items.GetFirstNode);
end;

procedure TMainForm.LoadDTD;
var
  i: integer;
  doctype: IXMLNode;
  n: IXMLNodeList;
  DTDFileName: string;
begin
  DTDPresent := false;
  DTDSL.Clear;
  // ������ ���� � ����� ntDoctype
  n := XMLEdit.XMLDocument.DocumentElement.ParentNode.ChildNodes;
  doctype := nil;
  i := 0;
  while i < n.Count do begin
    if n[i].NodeType = ntDocType then begin
      doctype := n[i];
      break;
    end;
    inc(i);
  end;

  if (doctype <> nil) then begin
    if doctype.Attributes['SYSTEM'] <> NULL then begin
      DTDFile := doctype.Attributes['SYSTEM'];
      DTDFileName := DTDFile;
      // ���� ���� �������������, �� ������� ���� � xml �����
      if ExtractFilePath(DTDFileName) = '' then
        DTDFileName := ExtractFilePath(OpenDlg.FileName) + DTDFileName;
      try
        DTDSL.LoadFromFile(DTDFileName);
        DTDPresent := true;
      except
      end
    end
    else begin
      DTDSL.Text := doctype.xml;
      DTDPresent := true;
    end;
  end;
end;

procedure TMainForm.SaveDocExecute(Sender: TObject);
begin
  if SaveDlg.FileName = '' then SaveAsDocExecute(Sender)
  else XMLEdit.XMLDocument.SaveToFile(SaveDlg.FileName);
end;

procedure TMainForm.SaveAsDocExecute(Sender: TObject);
var OldPath, NewPath, msg: string;
begin
  OldPath := ExtractFilePath(SaveDlg.FileName);
  if SaveDlg.Execute then begin
    // ���� ���� ������ �� DTD, �� �������� ��������� �� ���� � DTD (���� ������ �������)
    NewPath := ExtractFilePath(SaveDlg.FileName);
    if (AnsiCompareText(NewPath, OldPath)<>0) and (DTDFile <> '') and (ExtractFileDrive(DTDFile) = '') then begin
      msg := '�������� �������� ������������� ������ �� DTD ����.'+#10;
      msg := msg + '�� ������ ��������� DTD ���� � ��� �� �������?';
      OldPath := OldPath + ExtractFileName(DTDFile) + #0;
      NewPath := NewPath + ExtractFileName(DTDFile) + #0;
      case MessageDlg(msg, mtConfirmation, mbYesNoCancel, 0) of
        mrCancel: System.Exit;
        mrYes: CopyFile(PChar(OldPath), PChar(NewPath), true);
      end;
    end;
    XMLEdit.XMLDocument.SaveToFile(SaveDlg.FileName);
    Caption := Cap+' - '+ChangeFileExt(ExtractFileName(SaveDlg.FileName),'');
  end;
end;

procedure TMainForm.AboutExecute(Sender: TObject);
var AboutBox: TAboutBox;
begin
  AboutBox := TAboutBox.Create(Nil);
  AboutBox.ShowModal;
  AboutBox.Free;
end;


procedure TMainForm.XMLEditorHelpExecute(Sender: TObject);
var
  path: string;
begin
  // ������� ���� ������� � Internet Explorer
  path := ExtractFilePath(Application.ExeName)+'XMLEditorHelp.htm'+#0;
  ShellExecute(Application.Handle, 'open', PChar(path), '', '', SW_SHOW);
end;

//-----------------------------------------------------------------------------

procedure TMainForm.ToggleShowAttrExecute(Sender: TObject);
var
  CurSelected: IXMLNode;
begin
  if Sender is TSpeedButton then begin
    ToggleShowAttrItem.Checked := not ToggleShowAttrItem.Checked;
  end
  else begin
    ToggleShowAttrSB.Down := not ToggleShowAttrSB.Down;
  end;

  // ���� ������� �����, �� ���������� �������� � ����� IE, ����� ������ ����� �����
  if ToggleShowAttrSB.Down then ToggleShowAttrSB.Hint := '�������� ��������'
  else ToggleShowAttrSB.Hint := '���������� ��������';

  if ToggleShowAttrItem.Checked then
    XMLEdit.NodeOptions := XMLEdit.NodeOptions + [noAttrsAsString]
  else
    XMLEdit.NodeOptions := XMLEdit.NodeOptions - [noAttrsAsString];
  CurSelected := XMLEdit.GetXMLNode(XMLEdit.Selected); // ���������� ��� ������
  XMLEdit.SynchronizeTree(XMLEdit.XMLDocument.DocumentElement); // ��������������
  XMLEdit.GetTreeNode(CurSelected).Selected := True;
  XMLAttrEdit.XMLNode := CurSelected;
end;

procedure TMainForm.OpenInIEExecute(Sender: TObject);
var
  path: string;
begin
  // ������� ������������� ���� � Internet Explorer
  if SaveDlg.FileName = '' then ShowMessage('������� ��������� ��������!')
  else begin
    path := SaveDlg.FileName+#0;
    ShellExecute(Application.Handle, 'open', PChar(path), '', '', SW_SHOW);
  end;
end;

//-----------------------------------------------------------------------------

procedure TMainForm.ValidateAExecute(Sender: TObject);
var
  Doc: IXMLDocument;
begin
  // �������� �� ������������ � ������������ � DTD
  Doc := TXMLDocument.Create(nil);
  Doc.ParseOptions := [poValidateOnParse, poResolveExternals]; // ����� ������� dtd �����������
  try
    // ����� ����� ���������� ������ ��� �������� �� ������������
    XMLEdit.XMLDocument.SaveToFile(ExtractFilePath(SaveDlg.FileName)+'~temp.xml');
    Doc.LoadFromFile(ExtractFilePath(SaveDlg.FileName)+'~temp.xml');
    ShowMessage('������ �� ����������!');
  except
    on E: EDOMParseError do begin
       DTDErrorForm.ErrorCodeEdit.Text := IntToStr(E.ErrorCode);
       DTDErrorForm.LineEdit.Text := IntToStr(E.Line);
       DTDErrorForm.LinePositionEdit.Text := IntToStr(E.LinePos);
       DTDErrorForm.FilePositionEdit.Text := IntToStr(E.FilePos);
       DTDErrorForm.ErrorMemo.Text := E.Reason;
       DTDErrorForm.TextMemo.Text := copy(E.SrcText,1,E.LinePos);
       DTDErrorForm.ShowModal;
    end;
  end;
  DeleteFile(ExtractFilePath(SaveDlg.FileName)+'~temp.xml');
  Doc := nil;
end;


procedure TMainForm.SearchAExecute(Sender: TObject);
var n: IXMLNode;
begin
  // ����� �� ���������
  if SearchForm.ShowModal = mrOK then begin
    // �������� ���������
    SearchString  := SearchForm.SearchStringEdit.Text;
    InNodeNames   := SearchForm.NodeNamesCB.Checked;
    InAttrNames   := SearchForm.AttrNamesCB.Checked;
    InNodeValues  := SearchForm.NodeValuesCB.Checked;
    InAttrValues  := SearchForm.AttrValuesCB.Checked;
    FullStrOnly   := SearchForm.FullStrOnlyCB.Checked;
    n := Search(XMLEdit.GetXMLNode(XMLEdit.Selected));
    if n <> nil then begin
      // ������� ���� �/��� ������� ��� ������������ �������� �����
      XMLEdit.GetTreeNode(n).Selected := true;
    end
    else ShowMessage('����� � ��������� ��������.');
  end;
end;

procedure TMainForm.SearchAgainAExecute(Sender: TObject);
var next,n: IXMLNode;
begin
  // ���� ����� ��� �� ���������, �� ������� ���
  if not SearchForm.OKB.Enabled then
    SearchAExecute(Sender)
  else begin

    // ���������� ����� � ����, ���������� �� ���������
    next := XMLEdit.GetXMLNode(XMLEdit.Selected);
    if next.HasChildNodes then begin
      // ���������� ���������� ����
      next := next.ChildNodes.First;
    end
    else
      next := GetNextNode(next);

    if next <> nil then begin
      n := Search(next);
    end;
    if n <> nil then begin
      // ������� ���� �/��� ������� ��� ������������ �������� �����
      // � ��������� ��� ����� � ���� ������
      XMLEdit.GetTreeNode(n).Selected := true;
    end
    else ShowMessage('����� � ��������� ��������.');
  end;
end;

//-----------------------------------------------------------------------------

procedure TMainForm.InsertNodeAExecute(Sender: TObject);
var
  Node, Child: IXMLNode;
  tn: TTreeNode;
begin
  // ������� ���� ����� ������ �����
  Node := XMLEdit.GetXMLNode(XMLEdit.Selected);
  if Node = XMLEdit.XMLDocument.DocumentElement then
    ShowMessage('����� ���� ������ ���� �������� ������')
  else if Node <> NIL then begin
    Child := Node.ParentNode.AddChild('NewObject', Node.ParentNode.ChildNodes.IndexOf(Node));
    tn := XMLEdit.Items.Insert(XMLEdit.Selected, Child.NodeName);
    tn.Selected := true;
    tn.EditText;
  end;
end;

procedure TMainForm.AppendNodeAExecute(Sender: TObject);
var
  Node, Child: IXMLNode;
  tn: TTreeNode;
begin
  // ���������� ���� � �����
  Node := XMLEdit.GetXMLNode(XMLEdit.Selected);
  if Node = XMLEdit.XMLDocument.DocumentElement then
    ShowMessage('����� ���� ������ ���� �������� ������')
  else if Node <> NIL then begin
    Child := Node.ParentNode.AddChild('NewObject');
    tn := XMLEdit.Items.Add(XMLEdit.Selected, Child.NodeName);
    tn.Selected := true;
    tn.EditText;
  end;
end;

procedure TMainForm.AddChildAExecute(Sender: TObject);
var
  Node, Child: IXMLNode;
  TN: TTreeNode;
begin
 // ���������� ��������� ����
 if XMLEdit.Selected <> NIL then begin
    Node := XMLEdit.GetXMLNode(XMLEdit.Selected);
    if Node <> NIL then begin
      Child := Node.AddChild('NewObject');
      tn := XMLEdit.Items.AddChild(XMLEdit.Selected, Child.NodeName);
      tn.Selected := true;
      XMLEdit.Selected.Expand(true);
      tn.EditText;
    end;
  end;
end;

procedure TMainForm.CutNodeAExecute(Sender: TObject);
var Node, Parent: IXMLNode;
begin
  // �������� �������, ������ ������� ������
  if XMLEdit.Selected <> NIL then begin
    Node := XMLEdit.GetXMLNode(XMLEdit.Selected);
    if (Node <> NIL)and(Node <> XMLEdit.XMLDocument.DocumentElement) then begin
      // �������� � ������ ������ ������ ��� �������
      Buffer := Node.CloneNode(true);
      PasteNodeA.Enabled := true;
      // ������ ������
      Parent := Node.ParentNode;
      Parent.ChildNodes.Delete(Parent.ChildNodes.IndexOf(Node));
      XMLEdit.Items.Delete(XMLEdit.Selected);
    end;
  end;
end;


procedure TMainForm.CopyNodeAExecute(Sender: TObject);
begin
  // ��������� ���� � ����� ������
  Buffer := XMLAttrEdit.XMLNode.CloneNode(true);
  PasteNodeA.Enabled := true;
end;

procedure TMainForm.PasteNodeAExecute(Sender: TObject);
var
  Node: IXMLNode;
  tn: TTreeNode;
begin
  // ������� ���� �� ������ ������ ����� ��������� �����
  if Buffer <> nil then begin
    // ������� ���� ����� ���������� �����
    Node := XMLEdit.GetXMLNode(XMLEdit.Selected);
    if Node = XMLEdit.XMLDocument.DocumentElement then
      ShowMessage('����� ���� ������ ���� �������� ������')
    else if Node <> NIL then begin
      Node.ParentNode.ChildNodes.Insert(Node.ParentNode.ChildNodes.IndexOf(Node), Buffer);
      tn := XMLEdit.Items.Insert(XMLEdit.Selected, Buffer.NodeName);
      tn.Selected := true;
      XMLEdit.SynchronizeTree(Buffer);
      Buffer := nil; // �������� ����� ������ ���� ���
      PasteNodeA.Enabled := false;
    end;
  end;
end;


procedure TMainForm.InsertAttrExecute(Sender: TObject);
begin
  // �������� ������� ����� �������
  XMLAttrEdit.InsertRow('', '', false);
end;

procedure TMainForm.AppendAttrExecute(Sender: TObject);
begin
  // �������� ������� � �����
  XMLAttrEdit.InsertRow('', '', true);
end;

procedure TMainForm.DeleteAttrExecute(Sender: TObject);
begin
  // ������� �������
  XMLAttrEdit.XMLNode.AttributeNodes.Delete(XMLAttrEdit.Keys[XMLAttrEdit.Row]);
  XMLAttrEdit.DeleteRow(XMLAttrEdit.Row);
  // ���� �������� ���� �� ������ ���� ��� � ������ �������� ������������
  XMLEdit.GetAttributes(XMLEdit.GetXMLNode(XMLEdit.Selected), XMLEdit.Selected);
end;

//-----------------------------------------------------------------------------
procedure TMainForm.NodeChange(Sender: TObject; Node: TTreeNode);
var
  i,j: integer;
  s,rest: string;
begin
  // �������� ������ ����, ������� ��� ��������
  with XMLAttrEdit do begin
    // ������ ��� ������� ������� XMLNode �������� ������������� ������� �������� ����
    PostAttribute(Keys[Row], Values[Keys[Row]]);
    // � �������� ����� � XMLText ��� �������� ����
    if XMLNode.IsTextElement then begin
      XMLNode.Text := XMLText.Text;
    end
    else if XMLText.Text <> '' then begin
      if XMLNode.ChildNodes.Count = 0 then XMLNode.Text := XMLText.Text
      else begin
        s := '��������� ����� �������� �������� ���� ' + XMLNode.NodeName + '. �� ��������?';
        if Application.MessageBox(PChar(s), PChar(Application.Title), MB_OKCANCEL) = IDOK then
          XMLNode.Text := XMLText.Text;
      end;
    end;

    // ������ ������� ����
    XMLNode := XMLEdit.GetXMLNode(Node);

    // ������� �������� ������ ����
    ShowAttributes;
    // � ������� ����� ������ ����
    if XMLNode.IsTextElement then XMLText.Text := XMLNode.Text
    else XMLText.Text := '';
  end;

  if DTDPresent then begin
    // ������ �������� ���� � ���������� (XMLAttrEdit.XMLNode.NodeName)
    DTDMemo.Lines.Text := '';
    s := '';
    rest := DTDSL.Text;
    while rest <> '' do begin
      i := pos('<!ELEMENT', rest);
      j := pos('<!ATTLIST', rest);
      if (i=0) and (j=0) then break;
      if (j>0) and ((i>j)or(i=0)) then i := j;
      if i>0 then begin
        s := trimleft(copy(rest, i+9, length(rest)));
        j := pos('>', s);
        if (pos(XMLAttrEdit.XMLNode.NodeName, s) = 1) and
           (s[length(XMLAttrEdit.XMLNode.NodeName)+1] in [#10, #13, #9, #32])
        then
          DTDMemo.Lines.Text := DTDMemo.Lines.Text + copy(rest,i,9)+' '+copy(s, 1, j)+#10;
        rest := trimleft(copy(s, j+1, length(s)));
      end;
    end;
  end;
end;

procedure TMainForm.NodeEdited(Sender: TObject; Node: TTreeNode; var S: string);
var
  XMLNode: IXMLNode;
  Kids: IXMLNodeList;
  XMLDoc: IXMLDocument;
  XML, NewName, NewStr: WideString;
  i: integer;
begin
  // �������������� ����
  // � S ����� ���������� ������ � ����������, ������� ���� �������� ����� �� ������� �������, � ��������� ������������
  NewStr := TrimLeft(S);
  i := Pos(' ', NewStr);
  if i = 0 then
    NewName := NewStr
  else
    NewName := copy(NewStr, 1, i-1);

  if RussianInName(NewName) then begin
    ShowMessage('������� ����� � �������� ���� �� ���������: ' + NewName);
    System.Exit;
  end;

  // ���������� ����� �������� ��� ���� � TreeView � ������� TreeView
  XMLNode := XMLEdit.GetXMLNode(XMLEdit.Selected);
  
  NewStr := NewName;
  if noAttrsAsString in XMLEdit.NodeOptions then begin
    for i:=0 to XMLNode.AttributeNodes.Count-1 do begin
      NewStr := NewStr + ' ' + XMLNode.AttributeNodes.Nodes[i].NodeName + ' = "' + XMLNode.AttributeNodes.Nodes[i].Text + '"';
    end;
  end;
  S := NewStr; // S ����� ������������� ���������� � Node.Text

  // ������ �� ������, ���� ��� �� ��������
  if NewName = XMLNode.NodeName then System.exit;

  XML := XMLNode.XML;
  Delete(XML, 2, Length(XMLNode.NodeName)); // �������� ��� ���� � ������
  Insert(NewName, XML, 2);
  if XMLNode.ChildNodes.Count > 0 then begin // ���� ���� �������� ���, �� �������� � � ���
    Delete(XML, Length(XML)-Length(XMLNode.NodeName), Length(XMLNode.NodeName));
    Insert(NewName, XML, Length(XML));
  end;

  // ������ ������� ������ XML
  if XMLNode = XMLEdit.XMLDocument.DocumentElement then begin
    XMLEdit.XMLDocument.LoadFromXML(XML);
    if XMLEdit.XMLDocument.Encoding = '' then
      XMLEdit.XMLDocument.Encoding := 'utf-8'; // ��� ��������� �������� �����
    XMLNode := XMLEdit.XMLDocument.DocumentElement;
  end
  else begin
    XMLDoc := TXMLDocument.Create(NIL);
    XMLDoc.LoadFromXML(XML);
    Kids := XMLNode.ParentNode.ChildNodes;
    Kids.Insert(Kids.Delete(Kids.IndexOf(XMLNode)), XMLDoc.DocumentElement.CloneNode(True));
  end;

  Node.Selected := True;
  if Assigned(XMLEdit.OnChange) then XMLEdit.OnChange(Sender, Node); // ����� dtd ����������
end;

procedure TMainForm.XMLEditHint(Sender: TObject);
var
  h: WideString;
  i: integer;
begin
  // ���������� hint �� ��������� ��� nodevalue
  h := '';
  if XMLEdit.HoverXMLNode.IsTextElement then h := h + 'Value: ' + XMLEdit.HoverXMLNode.Text + #10#13;
  if XMLEdit.HoverXMLNode.AttributeNodes.Count = 0 then h := h + '<no attributes>'
  else begin
    for i:=0 to XMLEdit.HoverXMLNode.AttributeNodes.Count-1 do
      h := h + 'a: ' + XMLEdit.HoverXMLNode.AttributeNodes.Nodes[i].NodeName + ' = ' + XMLEdit.HoverXMLNode.AttributeNodes.Nodes[i].Text + #10#13;
  end;
  XMLEdit.Hint := h;
  Application.ActivateHint(Point(Mouse.CursorPos.X+50, Mouse.CursorPos.Y-15));
end; 

procedure TMainForm.XMLAttrEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  // OnKeyDown: ���������� �������� �� enter
  case Key of
  VK_RETURN, VK_TAB,
  VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN,
  VK_HOME, VK_END, VK_PRIOR, VK_NEXT:
    with XMLAttrEdit do PostAttribute(Keys[Row], Values[Keys[Row]]);
  end;
  // ���� �������� ���� �� ������ ���� ��� � ������ �������� ������������
  XMLEdit.GetAttributes(XMLEdit.GetXMLNode(XMLEdit.Selected), XMLEdit.Selected);
end;

end.
