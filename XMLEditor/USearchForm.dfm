object SearchForm: TSearchForm
  Left = 327
  Top = 276
  Width = 402
  Height = 210
  ActiveControl = SearchStringEdit
  Caption = #1055#1086#1080#1089#1082
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 34
    Height = 13
    Caption = #1053#1072#1081#1090#1080':'
  end
  object SearchStringEdit: TEdit
    Left = 56
    Top = 12
    Width = 241
    Height = 21
    TabOrder = 0
    OnChange = ValidateSearchParams
  end
  object OKB: TButton
    Left = 312
    Top = 12
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080' '#1076#1072#1083#1077#1077
    Default = True
    Enabled = False
    ModalResult = 1
    TabOrder = 1
  end
  object CancelB: TButton
    Left = 312
    Top = 44
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
  end
  object SearchGB: TGroupBox
    Left = 8
    Top = 48
    Width = 289
    Height = 105
    Caption = #1055#1088#1086#1089#1084#1072#1090#1088#1080#1074#1072#1090#1100' '#1087#1088#1080' '#1087#1086#1080#1089#1082#1077
    TabOrder = 3
    object NodeNamesCB: TCheckBox
      Left = 8
      Top = 20
      Width = 97
      Height = 17
      Caption = #1080#1084#1077#1085#1072' '#1091#1079#1083#1086#1074
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = ValidateSearchParams
    end
    object AttrNamesCB: TCheckBox
      Left = 8
      Top = 40
      Width = 121
      Height = 17
      Caption = #1080#1084#1077#1085#1072' '#1072#1090#1088#1080#1073#1091#1090#1086#1074
      TabOrder = 1
      OnClick = ValidateSearchParams
    end
    object AttrValuesCB: TCheckBox
      Left = 8
      Top = 80
      Width = 129
      Height = 17
      Caption = #1079#1085#1072#1095#1077#1085#1080#1103' '#1072#1090#1088#1080#1073#1091#1090#1086#1074
      TabOrder = 2
      OnClick = ValidateSearchParams
    end
    object NodeValuesCB: TCheckBox
      Left = 8
      Top = 60
      Width = 129
      Height = 17
      Caption = #1079#1085#1072#1095#1077#1085#1080#1103' '#1091#1079#1083#1086#1074
      TabOrder = 3
      OnClick = ValidateSearchParams
    end
  end
  object FullStrOnlyCB: TCheckBox
    Left = 8
    Top = 160
    Width = 209
    Height = 17
    Caption = #1048#1089#1082#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1089#1090#1088#1086#1082#1091' '#1094#1077#1083#1080#1082#1086#1084
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
end
