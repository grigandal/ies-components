program XMLEditor;

uses
  Forms,
  XMLEditForm in 'XMLEditForm.pas' {MainForm},
  About in 'About.pas' {AboutBox},
  UDTDError in 'UDTDError.pas' {DTDErrorForm},
  USearchForm in 'USearchForm.pas' {SearchForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'DVD XML Editor';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDTDErrorForm, DTDErrorForm);
  Application.CreateForm(TSearchForm, SearchForm);
  Application.Run;
end.
