object DTDErrorForm: TDTDErrorForm
  Left = 283
  Top = 179
  Width = 496
  Height = 384
  ActiveControl = TextMemo
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1076#1086#1087#1091#1089#1090#1080#1084#1086#1089#1090#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    488
    357)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 64
    Width = 46
    Height = 13
    Caption = #1054#1096#1080#1073#1082#1072' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 12
    Width = 66
    Height = 13
    Caption = #1050#1086#1076' '#1086#1096#1080#1073#1082#1080' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 248
    Top = 12
    Width = 86
    Height = 13
    Caption = #1057#1090#1088#1086#1082#1072' '#1074' '#1092#1072#1081#1083#1077' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 248
    Top = 40
    Width = 97
    Height = 13
    Caption = #1055#1086#1079#1080#1094#1080#1103' '#1074' '#1089#1090#1088#1086#1082#1077' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 136
    Width = 36
    Height = 13
    Caption = #1058#1077#1082#1089#1090' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 40
    Width = 94
    Height = 13
    Caption = #1055#1086#1079#1080#1094#1080#1103' '#1074' '#1092#1072#1081#1083#1077' :'
  end
  object ErrorCodeEdit: TEdit
    Left = 112
    Top = 8
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 0
  end
  object LineEdit: TEdit
    Left = 360
    Top = 8
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 1
  end
  object ErrorMemo: TMemo
    Left = 8
    Top = 80
    Width = 473
    Height = 49
    Anchors = [akLeft, akTop, akRight]
    ReadOnly = True
    TabOrder = 2
  end
  object LinePositionEdit: TEdit
    Left = 360
    Top = 36
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 3
  end
  object TextMemo: TMemo
    Left = 8
    Top = 152
    Width = 473
    Height = 195
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object FilePositionEdit: TEdit
    Left = 112
    Top = 36
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 5
  end
end
