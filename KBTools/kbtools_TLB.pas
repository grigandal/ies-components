unit kbtools_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 01.07.2002 14:02:48 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Dd\Delphi\KBTools\kbtools.tlb (1)
// LIBID: {EC1926E5-3F3B-11D6-AD45-A855175D4C05}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\stdvcl40.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, StdVCL, Variants, Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  kbtoolsMajorVersion = 1;
  kbtoolsMinorVersion = 0;

  LIBID_kbtools: TGUID = '{EC1926E5-3F3B-11D6-AD45-A855175D4C05}';

  IID_IKBEditor: TGUID = '{EC1926E6-3F3B-11D6-AD45-A855175D4C05}';
  CLASS_AutoKBEditor: TGUID = '{EC1926E8-3F3B-11D6-AD45-A855175D4C05}';
  IID_IKBVerifier: TGUID = '{EC1926EA-3F3B-11D6-AD45-A855175D4C05}';
  CLASS_KBVerifier: TGUID = '{EC1926EC-3F3B-11D6-AD45-A855175D4C05}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IKBEditor = interface;
  IKBEditorDisp = dispinterface;
  IKBVerifier = interface;
  IKBVerifierDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  AutoKBEditor = IKBEditor;
  KBVerifier = IKBVerifier;


// *********************************************************************//
// Interface: IKBEditor
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EC1926E6-3F3B-11D6-AD45-A855175D4C05}
// *********************************************************************//
  IKBEditor = interface(IDispatch)
    ['{EC1926E6-3F3B-11D6-AD45-A855175D4C05}']
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); safecall;
    procedure Stop; safecall;
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function  Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IKBEditorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EC1926E6-3F3B-11D6-AD45-A855175D4C05}
// *********************************************************************//
  IKBEditorDisp = dispinterface
    ['{EC1926E6-3F3B-11D6-AD45-A855175D4C05}']
    procedure Configurate(const Config: WideString); dispid 1;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); dispid 2;
    procedure Stop; dispid 3;
    property Name: WideString dispid 4;
    property Broker: OleVariant dispid 5;
  end;

// *********************************************************************//
// Interface: IKBVerifier
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EC1926EA-3F3B-11D6-AD45-A855175D4C05}
// *********************************************************************//
  IKBVerifier = interface(IDispatch)
    ['{EC1926EA-3F3B-11D6-AD45-A855175D4C05}']
    procedure Configurate(const Config: WideString); safecall;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); safecall;
    procedure Stop; safecall;
    function  Get_Name: WideString; safecall;
    procedure Set_Name(const Value: WideString); safecall;
    function  Get_Broker: OleVariant; safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    property Name: WideString read Get_Name write Set_Name;
    property Broker: OleVariant read Get_Broker write Set_Broker;
  end;

// *********************************************************************//
// DispIntf:  IKBVerifierDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EC1926EA-3F3B-11D6-AD45-A855175D4C05}
// *********************************************************************//
  IKBVerifierDisp = dispinterface
    ['{EC1926EA-3F3B-11D6-AD45-A855175D4C05}']
    procedure Configurate(const Config: WideString); dispid 1;
    procedure ProcessMessage(const SenderName: WideString; const MessageText: WideString; 
                             out Output: OleVariant); dispid 2;
    procedure Stop; dispid 3;
    property Name: WideString dispid 4;
    property Broker: OleVariant dispid 5;
  end;

// *********************************************************************//
// The Class CoAutoKBEditor provides a Create and CreateRemote method to          
// create instances of the default interface IKBEditor exposed by              
// the CoClass AutoKBEditor. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoKBEditor = class
    class function Create: IKBEditor;
    class function CreateRemote(const MachineName: string): IKBEditor;
  end;

// *********************************************************************//
// The Class CoKBVerifier provides a Create and CreateRemote method to          
// create instances of the default interface IKBVerifier exposed by              
// the CoClass KBVerifier. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoKBVerifier = class
    class function Create: IKBVerifier;
    class function CreateRemote(const MachineName: string): IKBVerifier;
  end;

implementation

uses ComObj;

class function CoAutoKBEditor.Create: IKBEditor;
begin
  Result := CreateComObject(CLASS_AutoKBEditor) as IKBEditor;
end;

class function CoAutoKBEditor.CreateRemote(const MachineName: string): IKBEditor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoKBEditor) as IKBEditor;
end;

class function CoKBVerifier.Create: IKBVerifier;
begin
  Result := CreateComObject(CLASS_KBVerifier) as IKBVerifier;
end;

class function CoKBVerifier.CreateRemote(const MachineName: string): IKBVerifier;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_KBVerifier) as IKBVerifier;
end;

end.
