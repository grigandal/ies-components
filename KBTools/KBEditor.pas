unit KBEditor;

interface

uses
  ComObj, ActiveX, kbtools_TLB, StdVcl, XMLIntf, XMLDoc, SysUtils,
  Aspr95, KBTypes, EditKB, Dialogs, Variants, Forms;

type
  TAutoKBEditor = class(TAutoObject, IKBEditor)
  protected
    Name: WideString;
    Broker: Variant;
    procedure Configurate(const Config: WideString); safecall;
    function Get_Broker: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure ProcessMessage(const SenderName, MessageText: WideString;
      out Output: OleVariant); safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Stop; safecall;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  private
    KBEditor: TKBEditor;
  end;

implementation

uses ComServ;

procedure TAutoKBEditor.Initialize;
begin
  inherited Initialize;
  KBEditor := TKBEditor.Create(NIL);
  KBEditor.KBControl := TKBControl.Create(NIL);
  KBEditor.KBControl.KB := TKnowledgeBase.Create(NIL);
end;

destructor TAutoKBEditor.Destroy;
begin
  inherited Destroy;
  KBEditor.KBControl.KB.Free;
  KBEditor.KBControl.Free;
  KBEditor.Free;
end;

procedure TAutoKBEditor.Configurate(const Config: WideString);
var
  ConfigDoc: IXMLDocument;
  path, filename: WideString;
begin
  // <config><FileName>medes.kbs</FileName></config>
  ConfigDoc := TXMLDocument.Create(nil);
  ConfigDoc.LoadFromXML(Config);

  if ((ConfigDoc.DocumentElement.ChildNodes.Count>0)
  and (ConfigDoc.DocumentElement.ChildNodes[0].NodeName = 'FileName'))
  then begin
    KBEditor.KBControl.ClearKB(KBEditor.KBControl.KB.KBOwner);
    filename := ConfigDoc.DocumentElement.ChildNodes[0].Text;
    // ���� ���� �������������, �� ������� ����
    if AnsiPos(':\', filename) = 0 then begin
      try
        path := Broker.Path;
      except
        // ���� ��� �������� Path ��� ��� �������
        path := ExtractFilePath(Application.ExeName)
      end;
      filename := path + filename;
    end;

    KBEditor.KBControl.KB.FileName := filename;
    KBEditor.KBControl.KB.Load;
  end;
end;

function TAutoKBEditor.Get_Broker: OleVariant;
begin
  Result := Broker;
end;

function TAutoKBEditor.Get_Name: WideString;
begin
  Result := Name;
end;

procedure TAutoKBEditor.ProcessMessage(const SenderName,
  MessageText: WideString; out Output: OleVariant);
var MessageDoc: IXMLDocument;
begin
  MessageDoc := TXMLDocument.Create(nil);
  MessageDoc.LoadFromXML(MessageText);

  if (AnsiCompareStr(MessageDoc.DocumentElement.Attributes['ProcName'], 'Run') = 0)
  or (AnsiCompareStr(MessageDoc.DocumentElement.Attributes['ProcName'], 'TKBEditor') = 0)
  then begin
    KBEditor.Run(NIL);
  end;
end;

procedure TAutoKBEditor.Set_Broker(Value: OleVariant);
begin
  Broker := Value;
end;

procedure TAutoKBEditor.Set_Name(const Value: WideString);
begin
  Name := Value;
end;

procedure TAutoKBEditor.Stop;
begin

end;

initialization
  TAutoObjectFactory.Create(ComServer, TAutoKBEditor, Class_AutoKBEditor,
    ciMultiInstance, tmApartment);
end.
