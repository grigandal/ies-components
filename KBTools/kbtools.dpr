library kbtools;

uses
  ComServ,
  kbtools_TLB in 'kbtools_TLB.pas',
  KBEditor in 'KBEditor.pas' {AutoKBEditor: CoClass},
  KBVerifier in 'KBVerifier.pas' {KBVerifier: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
