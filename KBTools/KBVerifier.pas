unit KBVerifier;

interface

uses
  ComObj, ActiveX, kbtools_TLB, StdVcl, Controls, XMLIntf, XMLDoc, SysUtils,
  Aspr95, KBTypes, VerifyStart, VerifyReport, VerifyPrint, EditKB, Variants, Forms;

type
  TKBVerifier = class(TAutoObject, IKBVerifier)
  protected
    Name: WideString;
    Broker: Variant;
    procedure Configurate(const Config: WideString); safecall;
    function Get_Broker: OleVariant; safecall;
    function Get_Name: WideString; safecall;
    procedure ProcessMessage(const SenderName, MessageText: WideString;
      out Output: OleVariant); safecall;
    procedure Set_Broker(Value: OleVariant); safecall;
    procedure Set_Name(const Value: WideString); safecall;
    procedure Stop; safecall;
  public
    procedure Initialize; override;
    destructor Destroy; override;
  private
    KBEditor: TKBEditor;
    VerifyStart: TFormVerifyStart;
    VerifyPrint: TFormVerifyPrint;
    VerifyReport: TFormVerifyReport;
  end;

implementation

uses ComServ;

procedure TKBVerifier.Initialize;
begin
  inherited Initialize;
  KBEditor := TKBEditor.Create(NIL);
  KBEditor.KBControl := TKBControl.Create(NIL);
  KBEditor.KBControl.KB := TKnowledgeBase.Create(NIL);
  VerifyStart := TFormVerifyStart.Create(NIL);
  VerifyReport := TFormVerifyReport.Create(NIL);
  VerifyPrint := TFormVerifyPrint.Create(NIL);
  VerifyReport.KB := KBEditor.KBControl.KB;
  VerifyReport.KBEditor := KBEditor;
  VerifyReport.FormPrint := VerifyPrint;
end;

destructor TKBVerifier.Destroy;
begin
  inherited Destroy;
  VerifyPrint.Free;
  VerifyReport.Free;
  VerifyStart.Free;
  KBEditor.KBControl.KB.Free;
  KBEditor.KBControl.Free;
  KBEditor.Free;
end;

procedure TKBVerifier.Configurate(const Config: WideString);
var
  ConfigDoc: IXMLDocument;
  path, filename: WideString;
begin
  // <config><FileName>medes.kbs</FileName></config>
  ConfigDoc := TXMLDocument.Create(nil);
  ConfigDoc.LoadFromXML(Config);

  if ((ConfigDoc.DocumentElement.ChildNodes.Count>0)
  and (ConfigDoc.DocumentElement.ChildNodes[0].NodeName = 'FileName'))
  then begin
    KBEditor.KBControl.ClearKB(KBEditor.KBControl.KB.KBOwner);
    filename := ConfigDoc.DocumentElement.ChildNodes[0].Text;
    // ���� ���� �������������, �� ������� ����
    if AnsiPos(':\', filename) = 0 then begin
      try
        path := Broker.Path;
      except
        // ���� ��� �������� Path ��� ��� �������
        path := ExtractFilePath(Application.ExeName)
      end;
      filename := path + filename;
    end;
    KBEditor.KBControl.KB.FileName := filename;
    KBEditor.KBControl.KB.Load;
  end;
end;

function TKBVerifier.Get_Broker: OleVariant;
begin
  Result := Broker;
end;

function TKBVerifier.Get_Name: WideString;
begin
  Result := Name;
end;

procedure TKBVerifier.ProcessMessage(const SenderName,
  MessageText: WideString; out Output: OleVariant);
var MessageDoc: IXMLDocument;
begin
  MessageDoc := TXMLDocument.Create(nil);
  MessageDoc.LoadFromXML(MessageText);

  if (AnsiCompareStr(MessageDoc.DocumentElement.Attributes['ProcName'], 'Run') = 0)
  or (AnsiCompareStr(MessageDoc.DocumentElement.Attributes['ProcName'], 'TAdition') = 0)
  then begin
    if VerifyStart.ShowModal = mrOK then begin
      VerifyReport.VerifyWhat := VerifyStart.VerifyWhat;
      VerifyReport.Show;
    end;
  end;
end;

procedure TKBVerifier.Set_Broker(Value: OleVariant);
begin
  Broker := Value;
end;

procedure TKBVerifier.Set_Name(const Value: WideString);
begin
  Name := Value;
end;

procedure TKBVerifier.Stop;
begin

end;

initialization
  TAutoObjectFactory.Create(ComServer, TKBVerifier, Class_KBVerifier,
    ciMultiInstance, tmApartment);
end.
