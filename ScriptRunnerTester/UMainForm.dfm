object MainForm: TMainForm
  Left = 253
  Top = 106
  Width = 592
  Height = 501
  Caption = #1058#1077#1089#1090#1077#1088' '#1076#1083#1103' ScriptRunner'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 209
    Height = 20
    Caption = 'ScriptLib.Runner'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Courier'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 164
    Height = 13
    Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1086#1085#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
  end
  object Label3: TLabel
    Left = 328
    Top = 56
    Width = 90
    Height = 13
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1084#1086#1076#1091#1083#1100
  end
  object Label4: TLabel
    Left = 16
    Top = 424
    Width = 52
    Height = 13
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
  end
  object Label5: TLabel
    Left = 16
    Top = 232
    Width = 79
    Height = 13
    Caption = #1042#1099#1079#1086#1074' '#1092#1091#1085#1082#1094#1080#1080
  end
  object Label6: TLabel
    Left = 328
    Top = 232
    Width = 173
    Height = 13
    Caption = #1057#1073#1088#1086#1089' '#1074#1089#1077#1093' '#1079#1072#1075#1088#1091#1078#1077#1085#1085#1099#1093' '#1084#1086#1076#1091#1083#1077#1081
  end
  object RunB: TButton
    Left = 328
    Top = 184
    Width = 153
    Height = 25
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1089#1086#1086#1073#1097#1077#1085#1080#1077
    TabOrder = 0
    OnClick = RunBClick
  end
  object ConfigMemo: TMemo
    Left = 16
    Top = 80
    Width = 297
    Height = 89
    Lines.Strings = (
      '<config>'
      '  <script Timeout="0" Language="VBScript" />'
      '</config>')
    TabOrder = 1
  end
  object AddModuleMemo: TMemo
    Left = 328
    Top = 80
    Width = 241
    Height = 89
    Lines.Strings = (
      '<message ProcName="AddModule">'
      '  <file>unit1.vb</file>'
      '</message>')
    TabOrder = 2
  end
  object ConfigB: TButton
    Left = 16
    Top = 184
    Width = 113
    Height = 25
    Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 3
    OnClick = ConfigBClick
  end
  object ResEdit: TEdit
    Left = 16
    Top = 440
    Width = 553
    Height = 21
    TabOrder = 4
  end
  object RunMemo: TMemo
    Left = 16
    Top = 256
    Width = 297
    Height = 105
    Lines.Strings = (
      '<message ProcName="Run">'
      '  <func name="sum" module="unit1">'
      '    <param type="number">13</param>'
      '    <param type="number">4</param>'
      '  </func>'
      '</message>')
    TabOrder = 5
  end
  object SendRunB: TButton
    Left = 16
    Top = 376
    Width = 137
    Height = 25
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1089#1086#1086#1073#1097#1077#1085#1080#1077
    TabOrder = 6
    OnClick = SendRunBClick
  end
  object ResetMemo: TMemo
    Left = 328
    Top = 256
    Width = 233
    Height = 105
    Lines.Strings = (
      '<message ProcName="Reset" />  ')
    TabOrder = 7
  end
  object ResetB: TButton
    Left = 328
    Top = 376
    Width = 153
    Height = 25
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1089#1086#1086#1073#1097#1077#1085#1080#1077
    TabOrder = 8
    OnClick = ResetBClick
  end
  object DesignB: TButton
    Left = 328
    Top = 16
    Width = 153
    Height = 25
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    TabOrder = 9
    OnClick = DesignBClick
  end
end
