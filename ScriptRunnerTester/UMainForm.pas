unit UMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComObj;

type
  TMainForm = class(TForm)
    RunB: TButton;
    ConfigMemo: TMemo;
    AddModuleMemo: TMemo;
    ConfigB: TButton;
    Label1: TLabel;
    Label2: TLabel;
    ResEdit: TEdit;
    Label3: TLabel;
    RunMemo: TMemo;
    SendRunB: TButton;
    Label4: TLabel;
    Label5: TLabel;
    ResetMemo: TMemo;
    Label6: TLabel;
    ResetB: TButton;
    DesignB: TButton;
    procedure RunBClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ConfigBClick(Sender: TObject);
    procedure SendRunBClick(Sender: TObject);
    procedure ResetBClick(Sender: TObject);
    procedure DesignBClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SR: OleVariant;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}


procedure TMainForm.FormCreate(Sender: TObject);
begin
  SR := CreateOleObject('ScriptLib.Runner');
end;


procedure TMainForm.ConfigBClick(Sender: TObject);
begin
  SR.Configurate(ConfigMemo.Lines.Text);
  ResEdit.Text := 'Компонент сконфигурирован.';
end;


procedure TMainForm.RunBClick(Sender: TObject);
var
  Output: OleVariant;
begin
  SR.ProcessMessage('from me', AddModuleMemo.Lines.Text, Output);
  ResEdit.Text := SR.LastError;
end;

procedure TMainForm.SendRunBClick(Sender: TObject);
var
  Output: OleVariant;
begin
  SR.ProcessMessage('from me', RunMemo.Lines.Text, Output);
  ResEdit.Text := SR.LastError;
  ShowMessage(Output);
end;

procedure TMainForm.ResetBClick(Sender: TObject);
var
  Output: OleVariant;
begin
  SR.ProcessMessage('from me', ResetMemo.Lines.Text, Output);
  ResEdit.Text := SR.LastError;
end;

procedure TMainForm.DesignBClick(Sender: TObject);
var
  Output: OleVariant;
begin
  SR.ProcessMessage('from me', '<message ProcName="Design" />', Output);
end;

end.
